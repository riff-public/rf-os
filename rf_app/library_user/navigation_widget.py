import os 
import sys 
import yaml

from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCore

import shutil
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
moduleDir = os.path.dirname(sys.modules[__name__].__file__)

import app
from rf_utils import file_utils 
from rf_utils import icon 

import browse_item_widget
reload(browse_item_widget)

class NavigationWidget(QtWidgets.QWidget,):
    """docstring for NavigationWidget"""
    sendSignal = QtCore.Signal(str)


    def __init__(self,paths, parent=None):
        super(NavigationWidget, self).__init__(parent=parent)
        # layout
        self.layout = QtWidgets.QVBoxLayout()

        self.BrowseWidget = browse_item_widget.BrowseWidget()
        
        # widget 
        self.label = QtWidgets.QLabel('Folder List')
        self.folder_view = QtWidgets.QTreeWidget()
        self.folder_view.setHeaderLabel('Folder')


        self.layout.addWidget(self.label)
        self.layout.addWidget(self.folder_view)
        self.setLayout(self.layout)

        self.rootPath = paths
        self.directoryPath = ""
        self.init_signals()

    def init_signals(self): 
        self.folder_view.clicked[QtCore.QModelIndex].connect(self.send_path)

    def send_path(self): 
        index = self.folder_view.currentIndex()
        data = index.data( QtCore.Qt.UserRole)
        self.sendSignal.emit(data)

    def list_path(self,root):
        self.directoryPath = root
        self.rootPath = self.directoryPath.replace('\\', '/')
        dirs = file_utils.listFolder(root)
        icon_path = icon.dir
        dirsCheck  = []
        self.folder_view.clear()
        for ix in dirs: 
            display = os.path.join(root, ix).replace('\\', '/')
            item = QtWidgets.QTreeWidgetItem(self.folder_view)
            item.setText(0,ix)
            icons = QtGui.QIcon(icon_path)
            item.setIcon(0,icons)
            item.setData(0,QtCore.Qt.UserRole, display)
            
            folder = file_utils.listFolder(display)
            for fold in folder:
                if fold[0]!='.':
                    pathCheck = os.path.join(display, fold).replace('\\', '/')
                    checkFile = os.listdir(pathCheck)
                    if '_library'in checkFile:
                        pass
                    if not '_library'in checkFile:
                        itemName = os.path.split(pathCheck)[1]
                      #  itemName = os.path.split(list_Check[0][0])
                        childitem = QtWidgets.QTreeWidgetItem(item)
                        childitem.setIcon(0, icons)
                        childitem.setText(0,itemName)
                        childitem.setData(0,QtCore.Qt.UserRole, pathCheck)


    def contextMenuEvent(self,event):
        
        index = self.folder_view.currentIndex()

        data = index.data( QtCore.Qt.UserRole)
        
        contextMenu = QtWidgets.QMenu(self.folder_view)

        rootAct = contextMenu.addAction("Create Root Folder")
        subAct = contextMenu.addAction("Create Sub Folder")
        deleteAct = contextMenu.addAction("delete")


        action = contextMenu.exec_(self.mapToGlobal(event.pos()))

        if (action == rootAct):
            self.rootNewFolder()
        elif (action == subAct):
            self.newFolder(data)
        elif (action == deleteAct):
            self.deletePath(data)


    def rootNewFolder(self):
        text, okPressed = QtWidgets.QInputDialog.getText(self, "Get text","Your name:", QtWidgets.QLineEdit.Normal, "")
        if okPressed and text != '':
            newpath = self.rootPath+'/'+text
            if not os.path.exists(newpath):
                os.makedirs(newpath)
        self.visitTree()        
        self.list_path(self.rootPath)
        self.keepIndex()

    def newFolder(self,data):
        text, okPressed = QtWidgets.QInputDialog.getText(self, "Get text","Your name:", QtWidgets.QLineEdit.Normal, "")
        if okPressed and text != '':
            newpath = data+'/'+text
            if not os.path.exists(newpath):
                os.makedirs(newpath)
        self.visitTree()        
        self.list_path(self.rootPath)
        self.keepIndex()

    def deletePath (self,data):
        quit_msg = "Are you sure deletfile?"
        reply = QtWidgets.QMessageBox.question(self, 'Delete', 
                         quit_msg, QtWidgets.QMessageBox.Yes, QtWidgets.QMessageBox.No)
        if reply == QtWidgets.QMessageBox.Yes:
            shutil.rmtree(data)
            self.visitTree()
            self.list_path(self.rootPath)
            self.BrowseWidget.clearWidget()           
            self.keepIndex()

    def find_checked(self):
        checked = dict()
        root = self.treeWidget.invisibleRootItem()
        signal_count = root.childCount()

        for i in range(signal_count):
            signal = root.child(i)
            checked_sweeps = list()
            num_children = signal.childCount()
            for n in range(num_children):
                child = signal.child(n)
                if child.checkState(0) == QtCore.Qt.Checked:
                    checked_sweeps.append(child.text(0))
            checked[signal.text(0)] = checked_sweeps
        return checked

    def visitTree(self):
        global listExpand,listSelected
        root = self.folder_view.invisibleRootItem()
        child_count = root.childCount()
        listExpand = []
        listSelected = []
        for i in range(child_count):
            item = root.child(i)
            childNode = item.childCount()
            
            if (childNode>0):
                if(item.isExpanded()== 1):
                    listExpand.append(i)
            if (item.isSelected() == 1):
                listSelected.append(i)

            for ix in range(childNode):
                itemChild = item.child(ix)
                if(itemChild.isSelected() == 1):
                    listSelected.append(i)
                    
                    #if (item.text(0) == self.folder_view.currentItem().text(0)) :
                        #listSelected.append(i)
            url = item.text(0) # text at first (0) column
            item.setText(1, 'result from %s' % url) # update result column (1)

    def keepIndex (self):
        global listExpand,listSelected
        for ix in listExpand:
            tLItem = self.folder_view.topLevelItem(ix)
            self.folder_view.expandItem(tLItem)

        for i in listSelected:
            select = self.folder_view.topLevelItem(i)
            self.folder_view.setCurrentItem(select,True)
            



            