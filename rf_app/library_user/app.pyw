# v.0.0.1 polytag switcher
_title = 'RF User Library'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'RFLibraryUserUI'

#Import python modules
import sys
import os 
import json 
import getpass

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# config file 
config_file = '%s/local_config.json' % moduleDir
with open(config_file) as configData: 
    localConfig = json.load(configData)

SCRIPT = localConfig['SCRIPT']
ROOT = os.environ.get(SCRIPT)
core = localConfig['CORE']

# import config
sys.path.append('%s/%s' % (ROOT, core))
import rf_config as config

# framework modules 
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet
from rftool.utils import maya_utils
from rf_utils import icon 
from rf_utils import file_utils
from rf_utils.pipeline import user_pref




user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import navigation_widget
reload(navigation_widget)
import browse_item_widget
reload(browse_item_widget)
import maya_hook 
reload(maya_hook)






class LibraryUser(QtWidgets.QMainWindow):


    def __init__(self, parent=None):
        #Setup Window
        super(LibraryUser, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        if config.isMaya: 
            self.ui = load.setup_ui_maya(uiFile, parent)
        else: 
            self.ui = load.setup_ui(uiFile, self)
        
        self.ui.show()
        self.ui.setWindowTitle('%s %s-%s' % (_title, _version, _des))

        self.rootPath = ""
        self.Private = ""
        self.Public = ""
        self.directoryPath = ""


        self.pref = user_pref.ToolSetting(uiName)

        # setup widget 
        self.setPath()
        self.set_logo()
        self.setup_widget()
        self.init_functions()
        self.default()

    
    def default(self):
        self.ui.public_radioButton.setChecked(True)
        

    def set_logo(self): 
        self.ui.logo_label.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))

    def setup_widget(self): 

        
        self.init_widgets(self.rootPath)
        self.browseWidget = browse_item_widget.BrowseWidget()
        self.ui.browse_layout.addWidget(self.browseWidget)

    def init_widgets(self,rootPath):
        

        self.navWidget = navigation_widget.NavigationWidget(rootPath)
        self.ui.navigation_layout.addWidget(self.navWidget)

        # add browse widget 


    def init_functions(self): 
        """ run functions """ 
        self.ui.public_radioButton.toggled.connect(self.checkStated)
        self.ui.private_radioButton.toggled.connect(self.checkStated)

        self.navWidget.sendSignal.connect(self.navFunc)
        #self.browseWidget.sendData.connect(self.sendPath)

        self.ui.exportButton.clicked.connect(self.exportfile)
        self.ui.importButton.clicked.connect(self.import_count)

        self.browseWidget.sendData.connect(self.print_select)

        self.ui.newPath_pushButton.clicked.connect(self.newPath)
        self.ui.browse_pushButton.clicked.connect(self.browseFile)

    def navFunc(self,root):

        self.directoryPath = root 
        self.browseWidget.clearWidget()

        list_folder = os.listdir(self.directoryPath)
        for ix in list_folder:
            getPath = self.directoryPath+'/'+ix
            for (path, dirs, files) in os.walk(getPath):
                if (files != []):
                    self.browseWidget.new_pic(path,files)
                del dirs[:] # go only one level deep

    def print_select(self,pathRoot):
        global fileMaPath
        fileMaPath = pathRoot


    def checkStated(self):
        
        if self.ui.public_radioButton.isChecked():
            self.rootPath = self.Public
            self.ui.currentPath_label.setText(self.Public)
        elif self.ui.private_radioButton.isChecked():
            self.rootPath = self.Private
            self.ui.currentPath_label.setText(self.Private)
        paths = self.rootPath
        self.navWidget.list_path(paths)

    def setPath (self):

        data = self.pref.read()
        if not data:

            self.rootPath = "P:/Library/GeneralLibrary/default"
            pathData = {'public':self.rootPath,'local':""}  
            writeData = self.pref.write(pathData)
            refreshData = self.pref.read()

            self.Public = refreshData.get('public')
            self.Private = refreshData.get('local')
 
        else:

            self.Public = data['public']
            self.Private = data['local']
            self.rootPath = self.Public
    def newPath(self):

        data = self.pref.read()
        #dialog reqest path
        if self.ui.public_radioButton.isChecked():
            checkP = 'public'
            self.ui.currentPath_label.setText(self.Public)
        elif self.ui.private_radioButton.isChecked():
            checkP = 'private'
        text, okPressed = QtWidgets.QInputDialog.getText(self, "Setpath","%s path:"%(checkP), QtWidgets.QLineEdit.Normal, "")
        if okPressed and text != '':
            if self.ui.public_radioButton.isChecked():
                data['public'] = text
                self.pref.write(data)
                self.Public = data['public']
                self.ui.currentPath_label.setText(self.Public)

            elif self.ui.private_radioButton.isChecked():
                data['local'] = text
                self.pref.write(data)
                self.Private = data['local']
                self.ui.currentPath_label.setText(self.Private)

            self.navWidget.list_path(text)



    def exportfile(self):

        fileName = maya_hook.file_dialog()
        folder = os.path.join(self.directoryPath, fileName).replace('\\', '/')
        exportPath = os.path.join(folder, fileName+'.ma')
        filePng = os.path.join(folder, fileName+'.png')
        verFolder = os.path.join(folder, 'version')
        if not os.path.exists(folder):
             os.makedirs(folder)
        if not os.path.exists(verFolder):
            os.makedirs(verFolder)

        sequencer = False
        formt = 'png'
        st = 1
        w = 900
        h = 900

        maya_hook.exportFiles(exportPath, 'mayaAscii')
        #check version
        list_maya_file = file_utils.listFile(verFolder)
        ver_suffix = '_%03d' % (1)
        if not list_maya_file:
            version_export = os.path.join(verFolder, fileName + ver_suffix + '.ma')
        else:
            ver = int(list_maya_file[-1].split('_0')[1].split('.')[0])
            ver_suffix = '_%03d' % (ver+1)
            version_export = os.path.join(verFolder, fileName + ver_suffix + '.ma')
        file_utils.copy(exportPath, version_export)
        maya_utils.capture_screen(filePng, formt, st, sequencer, w, h)


        files= open(folder+'/_library',"w+")
        files.close()

        inFile = os.listdir(folder)

        self.browseWidget.listOfFiles(folder)

    def importfile(self):
        global fileMaPath
        try:
            for file in os.listdir(fileMaPath):
                if file.endswith(".ma"):
                    impPath = (os.path.join(fileMaPath, file))
                    num = 1
                    name = file.split('.')[0]+'_'+'%02d' % num
                    ver = maya_hook.checkRefVer(name)
                    if(ver == True):
                        num = int(name.split('_0')[1])
                        num+=1
                        name = file.split('.')[0]+'_'+'%02d' % num
                    elif(ver == False):
                        name = file.split('.')[0]+'_'+'%02d' % num
                    maya_hook.importFile(impPath, name)
        except:
            quit_msg = "Select One!"
            reply = QtWidgets.QMessageBox.question(self, 'Error', 
                         quit_msg, QtWidgets.QMessageBox.Yes)
    def import_count(self):
        text, okPressed = QtWidgets.QInputDialog.getInt(self,"Import", "Repeat Count:", 1)

        if okPressed and text != '':
            repeat = int(text)
            for count in xrange(0, repeat):
                self.importfile()
    def browseFile(self):
        if self.ui.public_radioButton.isChecked():
            os.startfile(self.Public)
        elif self.ui.private_radioButton.isChecked():
            os.startfile(self.Private)

def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = LibraryUser(maya_win.getMayaWindow())
        return myApp

    else: 
        logger.info('Run in Standalone\n')
        app = QtWidgets.QApplication.instance()
        if not app: 
            app = QtWidgets.QApplication(sys.argv)
        myApp = LibraryUser()
        stylesheet.set_default(app)
        sys.exit(app.exec_())

if __name__ == '__main__': 
    show()
