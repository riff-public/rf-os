import sys
import os 
import maya.cmds as mc 
import maya.mel as mm

from rf_maya.rftool.clean.ascii_sanitizer import main as ascl

# maya commands 

def file_dialog (): 
    result = mc.promptDialog(
        title='ExportLibrary',
        message='Enter Name:',
        button=['OK', 'Cancel'],
        defaultButton='OK',
        cancelButton='Cancel',
        dismissString='Cancel')
    if result == 'OK':
        text = mc.promptDialog(query=True, text=True)

    return text

def exportFiles (exportPath, typ):
    mc.file( exportPath, exportSelected=True ,typ=typ)

    print 'Sanitizing: %s' %exportPath
    config_path = ascl.get_default_config()
    infected = ascl.clean(config_path=config_path, 
                            input_path=exportPath, 
                            output_path=exportPath)
    print 'Sanitizing Done.'

def importFile(imPath,file):
    mc.file(imPath,i=True,namespace=file)

def checkRefVer(name):
    result = mc.namespace( exists=name )
    return result
