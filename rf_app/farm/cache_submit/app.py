#!/usr/bin/env python
# -- coding: utf-8 --
# v.0.0.1 polytag switcher
_title = 'Cache Submiiter'
_version = 'v.0.0.2'
_des = 'add shot lister'
uiName = 'CacheSubmit'

#Import python modules
import sys
import os 
import json 
import getpass

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

import rf_config as config

# framework modules 
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import ui 
reload(ui)
from rf_utils.pipeline import user_pref
from rf_utils.context import context_info
from rf_utils import file_utils
from rf_utils import register_sg
from rf_app.publish.scene.utils import output_utils 
from rf_app.publish.asset import sg_hook
from rf_app.publish.scene import batch
from rf_utils.pipeline import shot_asset

from rf_utils.sg import sg_process
sg = sg_process.sg 


class Config: 
    fileTypes = ['abc', 'rs']
    fx = 'fx'
    processMap = {'main': 'fx'}


class Color: 
    red = [255, 100, 100]


class Modules: 
    default = ['camera',
         # 'alembic_cache',
         'cache_single',
         'export_grp',
         'export_set',
         'instance_dress',
         'set_asset_list',
         'shotdress',
         'shot_workspace',
         'yeti_bg']


class SubmitterCache(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        #Setup Window
        super(SubmitterCache, self).__init__(parent)

        # ui read
        self.ui = ui.SubmitterCacheUi()
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.statusBar()

        # title
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.resize(750, 650)

        self.init_functions()
        self.init_signals()


    def init_functions(self):
        # set modules 
        self.set_modules()
        # self.ui.pathLineEdit.setText('P:/projectName/scene/work/ep01/pr_ep01_q0010_s0020/anim/main/maya/pr_ep01_q0010_s0020_anim_main.v001.TA.ma')

    def init_signals(self): 
        self.ui.pathButton.clicked.connect(self.set_namespaces)
        self.ui.submitPushButton.clicked.connect(self.submit)
        self.ui.pathLineEdit.textChanged.connect(self.text_changed)
        self.ui.pathLineEdit.returnPressed.connect(self.set_namespaces)
        self.ui.sgRadioButton.clicked.connect(self.text_changed)
        self.ui.maRadioButton.clicked.connect(self.text_changed)
        self.ui.browserWidget.file_listWidget.itemSelected.connect(self.file_selected)

    def set_modules(self): 
        modules = Modules.default

        for module in modules: 
            self.ui.moduleListWidget.add_item(module, module)

    def set_namespaces(self): 
        stdout('Loading ...', self.statusBar)

        path = str(self.ui.pathLineEdit.text())
        readSg = self.ui.sgRadioButton.isChecked()
        readMa = self.ui.maRadioButton.isChecked()

        data = self.read_namespaces(path, sg=readSg, ma=readMa)
        self.ui.namespacesListWidget.clear()

        SubmitterCache.add_cache_list(self.ui.namespacesListWidget, data, path)

        self.ui.pathButton.setEnabled(False)

    @staticmethod
    def add_cache_list(widget, data, path=''): 
        for ns in sorted(data.keys()): 
            color = Color.red
            lock = data[ns]['lock'] 
            dataType = data[ns]['dataType']
            display = '%s \t\t %s' % (ns, dataType) 
            
            if lock: 
                display = '%s - locked \t\t %s' % (ns, dataType)
                lock = True 
            
            item = widget.add_item(display, (ns, path, lock, dataType))
            
            if lock: 
                item.setCheckState(QtCore.Qt.Unchecked)
                item.setForeground(QtGui.QColor(color[0], color[1], color[2]))


    def read_namespaces(self, path, sg=False, ma=False): 
        entity = context_info.ContextPathInfo(path=path)

        if sg: 
            stdout('Reading from Shotgun ...', self.statusBar)
            data = shot_asset.read_asset_elements(entity)
            stdout('Data from Shotgun', self.statusBar) if data else stdout('No Shot Elements data', self.statusBar)
            
            if not data: 
                ma = True

        if ma: 
            stdout('Reading from mayaAscii ...', self.statusBar)
            data = shot_asset.read_ma_asset_namespace(path)
            stdout('Data from mayaAscii', self.statusBar)

        return data

    def submit(self): 
        path = str(self.ui.pathLineEdit.text())
        allModules = self.ui.moduleListWidget.items(all=True)
        allNamespaces = self.ui.namespacesListWidget.items(all=True)

        checkedModuleItems = self.ui.moduleListWidget.checked_items()
        checkedNamespaceItems = self.ui.namespacesListWidget.checked_items()

        checkedModules = [a.data(QtCore.Qt.UserRole) for a in checkedModuleItems]
        checkedNamespaces = [a.data(QtCore.Qt.UserRole)[0] for a in checkedNamespaceItems]

        # if no checked items, insubficient data to submit 
        if not checkedModules or not checkedNamespaces: 
            logger.info('No data to submit')
            print ('No data')
            return

        # empty [] means submit all data 
        modules = [] if len(allModules) == len(checkedModules) else checkedModules
        namespaces = [] if len(allNamespaces) == len(checkedNamespaces) else checkedNamespaces
        assets = []

        batch.submit_queue(path, namespaces=namespaces, modules=modules, assets=assets)

    def text_changed(self): 
        self.ui.namespacesListWidget.clear()
        self.ui.pathButton.setEnabled(True)

    def file_selected(self, path): 
        self.ui.pathLineEdit.setText(path)
            

        

def stdout(message, statusBar=None): 
    """ output stdout """
    logger.info(message)
    print message 
    if statusBar: 
        statusBar().showMessage(message)
        QtWidgets.QApplication.processEvents()


class BlockSignals(): 
    """ temporary block signals """ 
    def __init__(self, widget): 
        self.widget = widget 

    def __enter__(self): 
        self.widget.blockSignals(True)

    def __exit__(self, *args): 
        self.widget.blockSignals(False)


def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        uiName = 'FxPublish'
        maya_win.deleteUI(uiName)
        myApp = SubmitterCache(maya_win.getMayaWindow())
        return myApp

    else: 
        logger.info('Run in standalone\n')
        app = QtWidgets.QApplication.instance()
        if not app: 
            app = QtWidgets.QApplication(sys.argv)
        myApp = SubmitterCache()
        myApp.show()
        stylesheet.set_default(app)
        sys.exit(app.exec_())

if __name__ == '__main__': 
    show()
