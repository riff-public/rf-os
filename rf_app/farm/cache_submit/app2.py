#!/usr/bin/env python
# -- coding: utf-8 --
# v.0.0.1 polytag switcher
_title = 'Cache Submiiter'
_version = 'v.0.0.2'
_des = 'add shot lister'
uiName = 'CacheSubmit'

#Import python modules
import sys
import os 

sys.path.append('{}/core'.format(os.environ['RFSCRIPT']))

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

import rf_config as config

# framework modules 
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet

from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from rf_utils.context import context_info
from rf_utils.widget import project_widget
from rf_utils.widget import export_list_widget
from rf_utils.widget import entity_browse_widget
from rf_utils.widget import file_widget
from collections import OrderedDict
from rf_utils import file_utils
from rf_app.scene.builder.model import scene_builder_model
from rf_utils.widget.status_widget import Icon

from rf_utils.sg import sg_process
sg = sg_process.sg

class SubmitterCacheUi(QtWidgets.QWidget):
    def __init__(self, parent=None) :
        super(SubmitterCacheUi, self).__init__(parent)

        if not config.isMaya:
            self.setStyleSheet("QCheckBox::indicator:checked {image-position: center center;"
                                "background-color:black;"
                                "image: url('D://jittarin//core//rf_utils//styleSheets//maya//images//check_box.png')}")

        self.allLayout = QtWidgets.QVBoxLayout()
        #self.projectWidget = project_widget.SGProjectComboBox(sg=sg_utils.sg, layout=QtWidgets.QHBoxLayout())
        self.projectWidget = project_widget.SGProjectComboBox(sg=sg_process.sg, layout=QtWidgets.QHBoxLayout())

        self.mode_horizontal = QtWidgets.QHBoxLayout()
        self.ep_label = QtWidgets.QLabel('Episode')
        self.ep_comboBox = QtWidgets.QComboBox()
        self.mode_horizontal.addWidget(self.ep_label)
        self.mode_horizontal.addWidget(self.ep_comboBox)

        self.sequence_label =QtWidgets.QLabel('Sequence')
        self.sequence_comboBox = QtWidgets.QComboBox()
        self.mode_horizontal.addWidget(self.sequence_label)
        self.mode_horizontal.addWidget(self.sequence_comboBox)

        self.folderLayout = QtWidgets.QHBoxLayout()
        self.folder_listWidget = QtWidgets.QListWidget()
        self.folder_listWidget.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)

        self.fileLayout = QtWidgets.QVBoxLayout()
        self.file_listWidget = file_widget.FileListWidgetExtended()

        self.settingLayout = QtWidgets.QHBoxLayout()
        self.verticalSpacer = QtWidgets.QSpacerItem(20, 20)
        self.cacheAll_label = QtWidgets.QLabel(" Cache All ")
        self.cacheAll_combobox = QtWidgets.QCheckBox()
        self.build_label = QtWidgets.QLabel(" Build ")
        self.build_combobox = QtWidgets.QCheckBox()
        self.build_combobox.setCheckState(QtCore.Qt.Checked)
        self.settingLayout.addItem(self.verticalSpacer)
        self.settingLayout.addWidget(self.cacheAll_label)
        self.settingLayout.addWidget(self.cacheAll_combobox)
        self.settingLayout.addWidget(self.build_label)
        self.settingLayout.addWidget(self.build_combobox)

        self.settingLayout.setStretch(0, 2)
        self.settingLayout.setStretch(1, 0)
        self.settingLayout.setStretch(2, 0)
        self.settingLayout.setStretch(3, 0)
        self.settingLayout.setStretch(4, 0)

        self.data_listWidget = QtWidgets.QListWidget()
        self.data_listWidget.setSpacing(0)
        self.data_listWidget.setViewMode(QtWidgets.QListView.ListMode)
        self.data_listWidget.setSelectionMode(QtWidgets.QAbstractItemView.NoSelection)

        self.layout_note = QtWidgets.QHBoxLayout()
        self.noteText = QtWidgets.QTextEdit()
        self.noteText.setFixedHeight(45)
        self.layout_note.addWidget(self.noteText)

        self.layout_sumbit = QtWidgets.QHBoxLayout()
        self.optionSelect = QtWidgets.QPushButton()
        self.optionSelect.setText('select all')
        self.optionSelect.setMinimumHeight(30)
        self.layout_sumbit.addWidget(self.optionSelect)

        self.submitButton = QtWidgets.QPushButton()
        self.submitButton.setText('Submit')
        self.submitButton.setMinimumHeight(30)
        self.layout_sumbit.addWidget(self.submitButton)

        self.layout_sumbit.setStretch(0, 0)
        self.layout_sumbit.setStretch(1, 2)

        self.folderLayout.addWidget(self.folder_listWidget)
        self.folderLayout.addLayout(self.fileLayout)
        self.fileLayout.addWidget(self.file_listWidget)
        self.fileLayout.addLayout(self.settingLayout)
        self.fileLayout.addWidget(self.data_listWidget)
        self.fileLayout.addLayout(self.layout_note)
        self.fileLayout.addLayout(self.layout_sumbit)

        self.fileLayout.setStretch(0, 1)
        self.fileLayout.setStretch(1, 0)
        self.fileLayout.setStretch(2, 2)
        self.fileLayout.setStretch(3, 0)
        self.fileLayout.setStretch(4, 0)
        self.folderLayout.setStretch(0, 0)
        self.folderLayout.setStretch(1, 2)

        self.allLayout.addWidget(self.projectWidget)
        self.allLayout.addLayout(self.mode_horizontal)
        self.allLayout.addLayout(self.folderLayout)
        self.setLayout(self.allLayout)


class SubmitterCache(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(SubmitterCache, self).__init__(parent)

        # ui read
        self.ui = SubmitterCacheUi()
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.statusBar()

        # title
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.resize(950, 820)

        self.statusBar = True
        self.shots_data = {}

        self.init_display()
        self.init_signal()
        self.init_selection()


    def init_signal(self):
        self.ui.projectWidget.projectComboBox.currentIndexChanged.connect(self.init_display)
        self.ui.ep_comboBox.currentIndexChanged.connect(self.display_sequence)
        self.ui.sequence_comboBox.currentIndexChanged.connect(self.display_shot)
        self.ui.folder_listWidget.itemSelectionChanged.connect(self.display_lastest_file)
        self.ui.file_listWidget.listWidget.itemSelectionChanged.connect(self.set_namespaces)
        self.ui.submitButton.clicked.connect(self.run_submit)
        #self.ui.submitButton.clicked.connect(self.run_submit_playblast)
        self.ui.optionSelect.clicked.connect(self.select_all_item)

    def init_process(self):
        self.list_projects = project_info.ProjectInfo().list_all(asObject=False)

    def init_display(self):
        self.get_task_staus()
        self.get_shot_data()
        self.display_episode()
        self.display_sequence()

    def init_selection(self):
        if config.isMaya:
            import maya.cmds as mc

            scene = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))
            project = scene.project
            episode = scene.episode
            sequence = scene.sequence

            if project and episode and sequence:
                self.ui.projectWidget.projectComboBox.setCurrentIndex(self.ui.projectWidget.projectComboBox.findText(project))
                self.ui.ep_comboBox.setCurrentIndex(self.ui.ep_comboBox.findText(episode))
                self.ui.sequence_comboBox.setCurrentIndex(self.ui.sequence_comboBox.findText(sequence))

    def display_episode(self):
        self.ui.ep_comboBox.clear()
        self.ui.ep_comboBox.addItems(sorted(self.episode))

    def display_sequence(self):
        act_text = self.ui.ep_comboBox.currentText()
        act_list = [a for a in self.shots_data[act_text]] if act_text in self.shots_data.keys() else []
        self.ui.sequence_comboBox.clear()
        self.ui.sequence_comboBox.addItems(sorted(act_list))
        self.display_shot()

    def get_task_staus(self):
        project = self.ui.projectWidget.current_project()
        taskName = 'cache'
        filters = [['project.Project.name', 'is', project], ['content', 'is', taskName], ['entity.Shot.sg_status_list','is_not', 'omt'], ['sg_status_list', 'is_not', 'omt']]
        fields = ['sg_status_list', 'entity']
        task = sg.find('Task', filters, fields)
        self.cacheList = dict()

        for cache in task:
            name = cache['entity']['name']
            status = cache['sg_status_list']
            self.cacheList[name] = status

    def display_shot(self):

        act_text = self.ui.ep_comboBox.currentText()
        seq_text = self.ui.sequence_comboBox.currentText()
        self.ui.folder_listWidget.clear()

        if act_text and seq_text:
            for shotName, path in sorted(self.shots_data[act_text][seq_text].iteritems()):

                status = self.cacheList[shotName] if shotName in self.cacheList.keys() else 'omt'
                iconPath = Icon.statusMap[status]['icon']
                iconPath = '{0}/{1}'.format(Icon.path, Icon.statusMap[status]['icon'])

                iconWidget = self.icon_widget(iconPath)
                self.add_item(shotName, path, iconWidget)


    def icon_widget(self, iconPath=''):
        """ icon widget """
        iconWidget = QtGui.QIcon()
        iconWidget.addPixmap(QtGui.QPixmap(iconPath), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        return iconWidget

    def add_item(self, text, data, iconWidget):
        """ add item """
        item = QtWidgets.QListWidgetItem(self.ui.folder_listWidget)
        item.setData(QtCore.Qt.UserRole, data)
        item.setText(text)

        if iconWidget:
            item.setIcon(iconWidget)

        return item

    def display_lastest_file(self):
        list_item = self.ui.folder_listWidget.selectedItems()
        self.ui.file_listWidget.clear()

        for item in list_item:
            shot_path = item.data( QtCore.Qt.UserRole)
            files = self.check_lastest_file(shot_path)
            
            for file in files:
                self.ui.file_listWidget.add_file(file)

    def check_lastest_file(self, path):
        step = 'anim'
        entity = context_info.ContextPathInfo(path=path)
        entity.context.update(step=step, process='main', app='maya')
        #entity.context.update(step=step, process='qc', app='maya')
        path = entity.path.workspace().abs_path()
        return self.list_file(path)

    def list_file(self, path):
        files = []
        if os.path.exists(path): 
            files = [os.path.join(path, d) for d in os.listdir(path) if os.path.isfile(os.path.join(path, d))]
        return files

    def set_namespaces(self):
        self.selection = True
        self.ui.data_listWidget.clear()
        self.ui.noteText.clear()
        list_item = self.ui.file_listWidget.listWidget.selectedItems()

        for item in list_item:
            shot_path = item.data( QtCore.Qt.UserRole)
            scene = ''
            scene = context_info.ContextPathInfo(path=shot_path)

            subdir = 'namespaceData'
            dataDir = '{}/{}'.format(scene.path.data().abs_path(), subdir)
            filename = '%s_%s.hero.yml'%(scene.name, 'cacheData')
            datafile = '{}/{}'.format(dataDir, filename)

            if os.path.exists(datafile):
                data = file_utils.ymlLoader(datafile)
                self.add_cache_list(scene, data)

    def add_cache_list(self, scene, data):
        from rf_utils import register_shot
        from rf_app.builder.model import build_cmd
        reg = register_shot.Register(scene)
        assets = reg.get.asset_list()
        heroList = build_cmd.filter_cache(scene, reg, assets)

        from rf_utils.pipeline import cache_list
        cache = cache_list.CacheList(scene)
        datashot = cache.list_keys()

        #project = 'Bikey'
        project = self.ui.projectWidget.current_project()
        shotName = scene.name
        taskName = 'cache'
        sg_data = self.get_one_shot(project, taskName, shotName)
        element_data = self.get_element_shot(project, shotName)

        note = ''
        if sg_data:
            if sg_data['open_notes']:
                note = sg_data['open_notes'][0]['name']

        self.ui.noteText.setText(note)

        dataList = list()
        popList = list()
        for num, ns in enumerate(sorted(data.keys())):
            dataType = data[ns]['type']

            if 'abc' in dataType:
                dataList.append(ns)
            else:
                popList.append(ns)

        for num, ns in enumerate(dataList + popList):
            dataType = data[ns]['type']
            #time = '0000-00-00 00:00:00+00:00'
            time = ""
            status = True if ns in datashot else False
            cache = True if ns in heroList else False
            step = reg.get.assetData[ns]['step'] if ns in reg.get.assetData.keys() else None
            color = [45, 45, 45] if num%2 == 0 else [75, 75, 75]

            if ns in element_data.keys():
                time = element_data[ns]['updated_at']

            self.add_cache_item(ns, dataType, step, color, status, cache, time)


    def add_cache_item(self, display, dataType, step, color, status, cache, time):
        widget = QtWidgets.QWidget()
        widget.setStyleSheet('background-color: rgb(%s, %s, %s);'%(color[0], color[1], color[2]))
        layout = QtWidgets.QHBoxLayout(widget)
        layout_iocn = QtWidgets.QHBoxLayout()

        checkBox = QtWidgets.QCheckBox()

        if self.selection:
            checkBox.setCheckState(QtCore.Qt.Checked)
        else:
            checkBox.setCheckState(QtCore.Qt.Unchecked)

        nslabel = QtWidgets.QLabel(display)
        typelabel = QtWidgets.QLabel(dataType)
        stepbutton = QtWidgets.QPushButton(step)
        statusbutton = QtWidgets.QPushButton('filter')
        cachebutton = QtWidgets.QPushButton('cache')
        timebutton = QtWidgets.QPushButton(str(time))

        typelabel.setFixedWidth(80)
        stepbutton.setFixedWidth(50)
        timebutton.setFixedWidth(140)

        if status:
            statusbutton.setStyleSheet("background-color: rgb(19, 180, 108); color: black")
        else:
            statusbutton.setStyleSheet("background-color: rgb(255, 0, 60); color: black")

        if cache:
            cachebutton.setStyleSheet("background-color: rgb(19, 180, 108); color: black")
        else:
            cachebutton.setStyleSheet("background-color: rgb(255, 0, 60); color: black")

        layout.addWidget(checkBox)
        layout.addWidget(nslabel)
        layout.addWidget(typelabel)
        layout.addLayout(layout_iocn)
        layout_iocn.addWidget(stepbutton)
        layout_iocn.addWidget(statusbutton)
        layout_iocn.addWidget(cachebutton)
        layout_iocn.addWidget(timebutton)

        layout.setStretch(0, 0)
        layout.setStretch(1, 2)
        layout.setStretch(2, 1)
        layout.setStretch(3, 2)

        item = QtWidgets.QListWidgetItem(self.ui.data_listWidget)
        data = (display, dataType, checkBox)
        if config.isMaya:
            item.setSizeHint(QtCore.QSize(50, 35))
        else:
            item.setSizeHint(QtCore.QSize(50, 30))

        item.setData(QtCore.Qt.UserRole, data)
        item.setFlags(item.flags() | QtCore.Qt.ItemIsUserCheckable)
        item.setBackground(QtGui.QColor(color[0], color[1], color[2]))

        self.ui.data_listWidget.setItemWidget(item, widget)

    def select_all_item(self):
        self.selection = False if self.selection else True

        for a in range(self.ui.data_listWidget.count()):
            ns, dataType, checkBoxWidget = self.ui.data_listWidget.item(a).data(QtCore.Qt.UserRole)
            
            if self.selection:
                checkBoxWidget.setCheckState(QtCore.Qt.Checked)
            else:
                checkBoxWidget.setCheckState(QtCore.Qt.Unchecked)

    def run_submit(self):
        from rf_app.publish.scene import batch
        reload(batch)
        not_check = []
        namespaceList = []
        modulesList = []
        assets = []
        path = str(self.ui.file_listWidget.listWidget.selectedItems()[0].data(QtCore.Qt.UserRole))
        path = path.replace('\\','/')
        build_status = True if self.ui.build_combobox.isChecked() == True else False

        for a in range(self.ui.data_listWidget.count()):
            ns, dataType, checkBoxWidget = self.ui.data_listWidget.item(a).data(QtCore.Qt.UserRole)

            if checkBoxWidget.isChecked() == True:
                if not dataType == 'import_data' and not dataType == 'set':
                    namespaceList.append(ns)

                if dataType == 'abc_cache':
                    if 'cache_single' not in modulesList:
                        modulesList.append('cache_single')

                elif dataType == 'abc_tech_cache':
                    if 'yeti_bg' not in modulesList:
                        modulesList.append('yeti_bg')

                elif dataType == 'set':
                    if 'export_set' not in modulesList:
                        modulesList.append('export_set')

                elif dataType == 'shotdress':
                    if 'shotdress' not in modulesList:
                        modulesList.append('shotdress')

                elif dataType == 'import_data':
                    if 'export_grp' not in modulesList:
                        modulesList.append('export_grp')

            else:
                not_check.append(checkBoxWidget)


        if 'import_data' in modulesList:
            self.register_data(scene)

        #print path, namespaceList, modulesList
        if not not_check:
            batch.submit_queue(path)
            
        elif namespaceList and modulesList and path:
            batch.submit_queue(path, namespaces=namespaceList, modules=sorted(modulesList), assets=assets, prefix='cacheSelective', build=build_status)

    def run_submit_playblast(self):
        from rf_maya.rftool.scene.playblast import utils, hook
        path = str(self.ui.file_listWidget.listWidget.selectedItems()[0].data(QtCore.Qt.UserRole))
        taskName = 'cache'
        upload = True
        removeImgs = False
        scene = context_info.ContextPathInfo(path=path)
        shotName = scene.name_code
        camera = '%s_cam:camshotShape'%(shotName)
        startFrame, endFrame, seqStartFrame, seqEndFrame = hook.shot_info(shotName)
        framerate = scene.projectInfo.render.fps()
        sended_job, destination = utils.playblast_to_queue(scene, 
                                                            startFrame, 
                                                            endFrame, 
                                                            camera, 
                                                            shotName, 
                                                            task_name=taskName, 
                                                            sg_upload=upload, 
                                                            framerate=framerate,
                                                            remove_images=removeImgs)

    def register_data(self, scene, namespace='Export_Grp', filetype='import_data'):
        from rf_utils import register_sg
        reload(register_sg)

        shotElm = register_sg.ShotElement(scene)
        taskEntities = shotElm.list_elements()
        task_export = [task['entity']['name'] for task in taskEntities]
        version = 'v001'

        if namespace not in task_export:
            register_sg.update_sg_reg(scene, nsKey=namespace, assetName=namespace, step=scene.step, namespace=namespace, filetype=filetype, version=version)

    def get_one_shot(self, project, taskName, shotName):
        filters = [['project.Project.name', 'is', project], ['content', 'is', taskName], ['entity.Shot.code', 'is', shotName]]
        fields = ['id', 'code', 'open_notes']
        return sg.find_one('Task', filters, fields)

    def get_element_shot(self, project, shotName):
        filters = [['project.Project.name', 'is', project], ['entity.CustomEntity09.sg_shot.Shot.code', 'is', shotName]]
        fields = ['entity','updated_at']
        result = sg.find('Task', filters, fields)

        element_dict = dict()
        for element in result:
            name = element['entity']['name']
            element_dict[name] = element

        return element_dict

    def get_shot_data(self):
        project = self.ui.projectWidget.current_project()
        filters = [['project.Project.name', 'is', project], ['sg_status_list', 'is_not', 'omt'], ['sg_shot_type', 'is', 'Shot']]
        fields = ['sg_sequence', 'code', 'sg_episode.Scene.code', 'sg_sequence_code', 'sg_shot_type']
        shots = sg.find('Shot', filters, fields)
        shots = [shot for shot in shots if shot['sg_episode.Scene.code']]

        self.episode = []
        self.shots_data = OrderedDict()

        if shots:
            context = context_info.Context()
            context.use_sg(sg, project, 'scene', entityName=shots[0]['code'])
            entity = context_info.ContextPathInfo(context=context)

            for i, shot in enumerate(shots): 

                entityGrp = shot['sg_episode.Scene.code']
                if not entityGrp in self.episode:
                    self.episode.append(entityGrp)
                
                entityParent = shot['sg_sequence_code']
                entity.context.update(entity=shot['code'], entityGrp=entityGrp, entityParent=entityParent)
                entity.update_scene_context()
                shot_name = entity.name
                shot_path = entity.path.name().abs_path()
                shot_dict = {shot_name: shot_path}
            
                if not entityGrp in self.shots_data:
                    self.shots_data[entityGrp] = {entityParent: shot_dict}
                else:
                    if not entityParent in self.shots_data[entityGrp]:
                        self.shots_data[entityGrp][entityParent] = shot_dict
                    else:
                        self.shots_data[entityGrp][entityParent].update(shot_dict)


def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        maya_win.deleteUI(uiName)
        myApp = SubmitterCache(maya_win.getMayaWindow())
        myApp.show()
        return myApp


    else: 
        app = QtWidgets.QApplication.instance()
        if not app: 
            app = QtWidgets.QApplication(sys.argv)
        myApp = SubmitterCache()
        myApp.show()
        stylesheet.set_default(app)
        sys.exit(app.exec_())

if __name__ == '__main__': 
    show()
