# v.0.0.1 polytag switcher
_title = 'SubmitterCacheUi'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'SubmitterCache'

import os
import sys
import traceback
from collections import OrderedDict

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils import icon
from rf_utils.widget import user_widget
from rf_utils.widget import project_widget
from rf_utils.widget import entity_browse_widget
from rf_utils.widget import file_widget
from rf_utils.widget import task_widget
from rf_utils.widget import status_widget
from rf_utils.pipeline import user_pref
from rf_utils.widget import export_list_widget
from rf_utils.widget import file_structure_widget
from rf_utils import project_info
from rf_utils.sg import sg_utils



class SubmitterCacheUi(QtWidgets.QWidget):
    status = QtCore.Signal(str)
    def __init__(self, parent=None) :
        super(SubmitterCacheUi, self).__init__(parent)



        # main layout
        self.allLayout = QtWidgets.QVBoxLayout()

        self.centerLayout = QtWidgets.QVBoxLayout()
        self.headerLayout = QtWidgets.QHBoxLayout()
        self.gridLayout = QtWidgets.QGridLayout()

        self.pathLineEdit = QtWidgets.QLineEdit()
        self.pathButton = QtWidgets.QPushButton('List Asset')
        
        self.browserWidget = file_structure_widget.FileStructureWidget()
        self.namespacesListWidget = export_list_widget.ExportListWidget()
        self.moduleListWidget = export_list_widget.ExportListWidget()

        self.nsOptionLayout = QtWidgets.QHBoxLayout()
        self.sgRadioButton = QtWidgets.QRadioButton('Read from Shotgun')
        self.maRadioButton = QtWidgets.QRadioButton('Read from mayaAscii')

        self.moduleCheckBox = QtWidgets.QCheckBox('All')
        self.namespaceCheckBox = QtWidgets.QCheckBox('All')
        self.submitPushButton = QtWidgets.QPushButton('Submit')

        self.nsOptionLayout.addWidget(self.namespaceCheckBox)
        self.nsOptionLayout.addWidget(self.sgRadioButton)
        self.nsOptionLayout.addWidget(self.maRadioButton)

        self.headerLayout.addWidget(self.pathLineEdit)
        self.headerLayout.addWidget(self.pathButton)

        self.centerLayout.addWidget(self.browserWidget)
        self.centerLayout.addLayout(self.headerLayout)
        self.centerLayout.addLayout(self.gridLayout)
        self.centerLayout.addWidget(self.submitPushButton)
        
        self.gridLayout.addWidget(self.moduleListWidget, 0, 0, 1, 1)
        self.gridLayout.addWidget(self.namespacesListWidget, 0, 1, 1, 1)
        self.gridLayout.addWidget(self.moduleCheckBox, 1, 0, 1, 1)
        self.gridLayout.addLayout(self.nsOptionLayout, 1, 1, 1, 1)

        self.allLayout.addLayout(self.centerLayout)

        # header
        self.setLayout(self.allLayout)

        # default value 
        self.set_default()
        self.init_signals()

    def set_default(self): 
        self.gridLayout.setColumnStretch(0, 1)
        self.gridLayout.setColumnStretch(1, 2)
        self.submitPushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.moduleCheckBox.setChecked(True)
        self.namespaceCheckBox.setChecked(True)
        self.sgRadioButton.setChecked(True)


    def init_signals(self): 
        self.moduleCheckBox.stateChanged.connect(self.module_check)
        self.namespaceCheckBox.stateChanged.connect(self.namespace_check)


    def module_check(self, value):
        state = True if value else False
        items = self.moduleListWidget.items(all=True)
        
        for item in items:
            check = QtCore.Qt.Checked if state else QtCore.Qt.Unchecked
            item.setCheckState(check)

    def namespace_check(self, value):
        state = True if value else False
        items = self.namespacesListWidget.items(all=True)
        
        for item in items:
            check = QtCore.Qt.Checked if state else QtCore.Qt.Unchecked
            item.setCheckState(check)


def hline_widget():
    line = QtWidgets.QFrame()
    line.setFrameShape(QtWidgets.QFrame.HLine)
    line.setFrameShadow(QtWidgets.QFrame.Sunken)
    return line





        