import os
import sys
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc
from rf_utils.widget import dialog

from PySide2 import QtWidgets
from PySide2 import QtCore
from PySide2 import QtGui
from functools import partial
import rf_config as config

uiName = 'RENAME'

class Ui(QtWidgets.QWidget): 
    def __init__(self, parent=None): 
        super(Ui, self).__init__(parent)

        self.mainLayout = QtWidgets.QVBoxLayout()
        self.startLayout = QtWidgets.QHBoxLayout()
        self.start_text = QtWidgets.QLabel("start shot")
        self.start_shot = QtWidgets.QLineEdit()
        self.start_shot.setText('s0000')
        self.start_shot.setMinimumHeight(20)
        self.start_shot.setMaxLength(5)
        self.start_text.setStyleSheet('font-size: 12px;')
        self.start_shot.setStyleSheet('font-size: 12px;')

        self.value_text = QtWidgets.QLabel("value")
        self.value_shot = QtWidgets.QLineEdit()
        self.value_shot.setText("10")
        self.value_shot.setMinimumHeight(20)
        self.value_text.setStyleSheet('font-size: 12px;')
        self.value_shot.setStyleSheet('font-size: 12px;')

        self.setting = QtWidgets.QPushButton("SET VALUE")
        self.sorted = QtWidgets.QPushButton("SORT")

        self.shotWidget = QtWidgets.QListWidget()
        self.shotWidget.setSpacing(0)
        self.shotWidget.setViewMode(QtWidgets.QListView.ListMode)
        self.shotWidget.setSelectionMode(QtWidgets.QAbstractItemView.NoSelection)

        self.check_submit = QtWidgets.QPushButton()
        self.check_submit.setText("CHECK")

        self.rename_submit = QtWidgets.QPushButton()
        self.rename_submit.setEnabled(False)
        self.rename_submit.setText("RENAME")
        self.rename_submit.setMinimumHeight(30)

        self.startLayout.addWidget(self.start_text)
        self.startLayout.addWidget(self.start_shot)
        self.startLayout.addWidget(self.value_text)
        self.startLayout.addWidget(self.value_shot)

        self.mainLayout.addLayout(self.startLayout)
        self.mainLayout.addWidget(self.setting)
        self.mainLayout.addWidget(self.sorted)
        self.mainLayout.addWidget(self.shotWidget)
        self.mainLayout.addWidget(self.check_submit)
        self.mainLayout.addWidget(self.rename_submit)
        self.setLayout(self.mainLayout)

class RenameManager(QtWidgets.QMainWindow): 
    def __init__(self, parent=None): 
        super(RenameManager, self).__init__(parent)

        self.ui = Ui()
        self._init_signal()

        self.status_sort = True
        self.seq_shots = mc.sequenceManager(lsh=True)
        self.shots_rename = self.select_obj()
        self.sorted_item()

        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.setWindowTitle('%s' % (uiName))


    def _init_signal(self):
        self.ui.rename_submit.clicked.connect(self.check_rename)
        self.ui.check_submit.clicked.connect(self.check_status)
        self.ui.setting.clicked.connect(self.set_start_shot)
        self.ui.sorted.clicked.connect(self.sorted_item)

    def set_start_shot(self):
        startName = self.ui.start_shot.text()
        value = int(self.ui.value_shot.text())
        if startName:
            for a in range(self.ui.shotWidget.count()):
                lineEdit, shotName = self.ui.shotWidget.item(a).data(QtCore.Qt.UserRole)

                lineEdit.setText(startName)

                shotsplit = startName.split('s')
                startName = '{}{:04}'.format('s',int(shotsplit[1]) + value)

    def set_data_item(self):
        for a in range(self.ui.shotWidget.count()):
            lineEdit, shotName = self.ui.shotWidget.item(a).data(QtCore.Qt.UserRole)
            newName = lineEdit.text()
            data = lineEdit, shotName
            self.ui.shotWidget.item(a).setData(QtCore.Qt.UserRole, data)

    def add_item(self, shotName):
        widget = QtWidgets.QWidget()
        layout = QtWidgets.QHBoxLayout(widget)
        
        shot_text = QtWidgets.QPushButton(shotName)
        shot_text.setFixedSize(80, 20)
        shot_text.setStyleSheet('font-size: 12px;')

        lineEdit = QtWidgets.QLineEdit()
        lineEdit.textChanged.connect(self.set_enabled)
        lineEdit.setMinimumHeight(20)
        lineEdit.setStyleSheet('font-size: 13px;')

        layout.addWidget(shot_text)
        layout.addWidget(lineEdit)

        item = QtWidgets.QListWidgetItem(self.ui.shotWidget)
        item.setSizeHint(widget.sizeHint())

        data = (lineEdit, shotName)
        item.setData(QtCore.Qt.UserRole, data)
        self.ui.shotWidget.setItemWidget(item, widget)

    def set_enabled(self):
        for a in range(self.ui.shotWidget.count()):
            lineEdit, shotName = self.ui.shotWidget.item(a).data(QtCore.Qt.UserRole)
            self.ui.shotWidget.item(a).setBackground(QtGui.QColor(45, 45, 45))

        self.ui.rename_submit.setEnabled(False)

    def select_obj(self):
        shots_rename = []
        sels = mc.ls(sl=True)
        shots_rename = [sel for sel in sels if mc.objectType(sel) == 'shot']

        for shot in shots_rename:
            self.seq_shots.remove(shot)

        return shots_rename

    def create_item(self, items):
        for item in items:
            self.add_item(item)

    def sorted_item(self):
        self.ui.shotWidget.clear()
        item_sort = sorted(self.shots_rename) if self.status_sort else self.shots_rename
        self.status_sort = False if self.status_sort else True
        self.create_item(item_sort)

    def rename_sequencer(self, oldName, newName):
        newNameSpace = '%s_cam' %newName
        sequencer_list = mc.sequenceManager(lsh=True)
        cameraShape = mc.shot(oldName, currentCamera=True, q=True)

        if oldName in sequencer_list:
            mc.rename(oldName, newName)
            mc.shot(newName, shotName=newName, e=True)

        if cameraShape:
            path_reference = mc.referenceQuery(cameraShape, filename=True)
            mc.file(path_reference, e=True, namespace=newNameSpace)
        else:
            dialog.MessageBox.warning('warning', '%s Not Camera'%(newName))

        print ('%s rename ----> %s'%(oldName, newName))
        self.close()

    def check_rename(self):
        tempList = list()
        for a in range(self.ui.shotWidget.count()):
            lineEdit, shotName = self.ui.shotWidget.item(a).data(QtCore.Qt.UserRole)
            oldName = shotName
            newName = lineEdit.text()

            if lineEdit.text() in self.shots_rename:
                temp = '%s_temp'%(lineEdit.text())
                newName = temp
                tempList.append(newName)

            self.rename_sequencer(oldName, newName)

        for temp in tempList:
            oldName = temp
            newName = temp.split('_temp')[0]
            self.rename_sequencer(oldName, newName)

    def check_status(self):
        self.set_data_item()
        seqName = list()
        status_all = list()

        for a in range(self.ui.shotWidget.count()):
            statusList = list()
            lineEdit, shotName = self.ui.shotWidget.item(a).data(QtCore.Qt.UserRole)

            if lineEdit.text() in self.seq_shots:
                statusList.append(False)

            elif lineEdit.text() in seqName:
                statusList.append(False)

            elif not lineEdit.text():
                statusList.append(False)

            elif lineEdit.text() == shotName:
                statusList.append(False)

            else:
                statusList.append(True)
                seqName.append(lineEdit.text())


            if False in statusList:
                #lineEdit.text().setStyleSheet('background-color: rgb(233, 60, 106);')
                self.ui.shotWidget.item(a).setBackground(QtGui.QColor("red"))
                status_all.append(False)
            else:
                self.ui.shotWidget.item(a).setBackground(QtGui.QColor("#13B46C"))
                status_all.append(True)

        if False in status_all:
            self.ui.rename_submit.setEnabled(False)

        else:
            self.ui.rename_submit.setEnabled(True)

def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        maya_win.deleteUI(uiName)
        myApp = RenameManager(maya_win.getMayaWindow())
        myApp.show()
        return myApp

if __name__ == '__main__': 
    show()