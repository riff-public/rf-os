_title = 'Riff Sequencer Manager'
_version = 'v.0.0.1'
_des = 'beta'
uiName = 'RFSequencerManager'

#Import python modules
import sys
import os
import getpass
import json
import logging

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# import config
import rf_config as config
from rf_utils import log_utils
# sg

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import ui
import controls_program
from rf_utils.sg import sg_process
from rf_utils.widget import dialog

class RFSequencerManager(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #SetUp Window
        super(RFSequencerManager, self).__init__(parent)

        #ui read
        self.ui = ui.RFSequencerManagerUI(parent)
        self.cus_wid = ui.QCustomQWidget
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.modules = controls_program
        self.shot_in_scene = self.modules.list_shots()
        self.get_shots()
        self.set_ui_init()
        self.display_shot()
        self.init_state()
        self.resize(360, 750)
        

        # self.set_logo()

    def init_state(self):
        self.ui.refresh_pushbutton.clicked.connect(self.display_shot)
        self.ui.shotListWidget.itemSelectionChanged.connect(self.set_time_ui)
        # self.ui.move_shot_push.clicked.connect(self.sync_shot)
        self.ui.create_shot_pushbutton.clicked.connect(self.create_shot)
        self.ui.sync_sg_pushbutton.clicked.connect(self.sync_sg_one_shot)
        self.ui.sync_sg_all_pushbutton.clicked.connect(self.sync_sg_shot)
        self.ui.delete_shot_pushbutton.clicked.connect(self.delete_shot)
        self.ui.edit_duration_pushbutton.clicked.connect(self.chg_duration)
        self.ui.edit_duration_to_lineEdit.editingFinished.connect(self.update_status_duration)
        self.ui.edit_merge_pushbutton.clicked.connect(self.merge_shot)
        self.ui.rebuild_sound_pushbutton.clicked.connect(self.rebuild_sound)
        self.ui.sendData.connect(self.update_sg_workduration)

    def test(self, data):
        print data,'ssss'

    def set_ui_init(self):
        self.ui.Description_project_label_detail.setText(self.scene.project)
        self.ui.Description_episode_label_detail.setText(self.scene.episode)
        self.ui.Description_sequence_label_detail.setText(self.scene.sequence)
        self.ui.Description_shot_label_detail.setText(self.scene.name)

        # disable modify shot functions 
        self.ui.groupBox_create_shot.setVisible(False)
        self.ui.groupBox_edit_shot.setVisible(False)

    def update_sg_workduration(self, data):
        chk = self.modules.confirm_dialog('')
        print chk
        if not chk:
            return 

        custom_wid, shotName, startTime, duration, endTime = data
        name_code = self.scene.sg_name
        convention = name_code.split('_')
        print convention
        if len(convention) == 4:
            name_code = name_code.replace(convention[-1], shotName)
            print name_code
            extraFields = ['sg_cut_in', 'sg_cut_out', 'sg_cut_duration', 'sg_working_duration']
            shot_entity = sg_process.get_shot_entity(self.scene.project, name_code, extraFields= extraFields)
            sg_process.update_shot_duration(shot_entity.get('id'), duration)
            self.display_shot()
            
            dialog.MessageBox.success('Update Shotgun Complete', 'Update Shotgun Complete')

        else:
            dialog.MessageBox.error('Name Error', 'Name Error >>> %s'%name_code)

    def get_shots(self):
        self.scene = self.modules.get_context_info()
        self.shots_sg = self.modules.get_shot_from_sg(self.scene)
        self.shots_in_scene = self.modules.list_shots()
        self.list_shotName_sg(self.shots_sg)
        self.cam_path = self.modules.get_cam_path(self.scene)

    def display_shot(self):
        self.get_shots()
        # self.shots_in_scene = self.modules.list_shots()
        self.sort_shot()
        self.ui.shotListWidget.clear()
        for shot in self.sort_shot_list:
            self.ui.add_custom_wid('', '', '', shot)
        self.sync_data_sg()

    def get_shot_widget_by_data(self, shot_name=''):
        self.get_list_shot_wid_item()
        for item in self.item_list_widget:
            myQCustomQWidget,shotName, startTime, duration, endTime = item.data(QtCore.Qt.UserRole)
            if shot_name == shotName:
                return item ,myQCustomQWidget

    def get_list_shot_wid_item(self):
        self.item_list_widget = []
        for index in range(0, self.ui.shotListWidget.count()):
            self.item_list_widget.append(self.ui.shotListWidget.item(index))

    def select_list(self):
        print self.ui.shotListWidget.selectedItems()[0]
        print self.ui.shotListWidget.selectedItems()[0].data(QtCore.Qt.UserRole)
        myQCustomQWidget,shotName, startTime, duration, endTime = self.ui.shotListWidget.selectedItems()[0].data(QtCore.Qt.UserRole)
        myQListWidgetItem = QtWidgets.QListWidgetItem(self.ui.shotListWidget)

    def list_shotName_sg(self, shots_sg):
        self.shots_in_sg = []
        for shot in shots_sg:
            self.shots_in_sg.append(shot['sg_shortcode'])

    def sync_data_sg(self):
        for shot in self.sort_shot_list:
            if shot in self.shots_in_sg:
                sg_duration = self.modules.find_sg_duration(shot, self.shots_sg)
                widget_item, cus_wid = self.get_shot_widget_by_data(shot)
                if shot in self.shots_in_scene:
                    curStartFrame, curEndFrame, curSeqStartFrame, curSeqEndFrame = self.modules.shot_info(shot)
                    duration = curEndFrame - curStartFrame +1
                    self.sync_display(cus_wid, shot, curStartFrame, duration, sg_duration,curEndFrame)
                    widget_item.setBackgroundColor( QtGui.QColor('green'))
                    self.sync_data_wid_item(widget_item, cus_wid, shot, int(curStartFrame), int(duration), int(curEndFrame))
                    if duration != sg_duration:
                        widget_item.setBackgroundColor( QtGui.QColor('yellow'))
                else:
                    self.sync_display( cus_wid,  shot, '', 'sync shot gun', sg_duration, '')
                    widget_item.setBackgroundColor( QtGui.QColor('red'))

            elif not shot in self.shots_in_sg:
                widget_item, cus_wid = self.get_shot_widget_by_data(shot)
                curStartFrame, curEndFrame, curSeqStartFrame, curSeqEndFrame = self.modules.shot_info(shot)
                duration = curEndFrame - curStartFrame +1
                self.sync_display( cus_wid,  shot, curStartFrame, 'Not in shot gun', 'Not in shot gun', curEndFrame)
                widget_item.setBackgroundColor( QtGui.QColor('orange'))
                self.sync_data_wid_item(widget_item, cus_wid, shot, int(curStartFrame), '', int(curEndFrame))




    def sync_data_wid_item(self, widget_item , custom_wid_item, shotName='', startTime='', duration='', endTime=''):
        data = [custom_wid_item, shotName, startTime, duration, endTime]
        widget_item.setData(QtCore.Qt.UserRole, data)

    def sync_display(self, custom_wid_item , shotName='', startTime='', duration='', durationSG='', endTime=''):
        custom_wid_item.setShot(shotName)
        custom_wid_item.setStartTime(startTime)
        custom_wid_item.setDurationTime(duration)
        custom_wid_item.setDurationTimeSG(durationSG)
        custom_wid_item.setEndTime(endTime)

    def set_time_ui(self):
        if self.ui.shotListWidget.selectedItems():
            widget_item = self.ui.shotListWidget.selectedItems()[0]
            myQCustomQWidget, shotName, startTime, duration, endTime = widget_item.data(QtCore.Qt.UserRole)
            # if startTime == 1001:
            #     self.ui.move_start_time_lineEdit.setReadOnly(True)
            #     self.ui.move_start_time_lineEdit.clear()
            #     self.ui.move_start_time_lineEdit.setText(str(startTime))
            # else:
            #     self.ui.move_start_time_lineEdit.setReadOnly(False)
            #     self.ui.move_start_time_lineEdit.setText(str(startTime))
            # self.ui.move_duration_time_label_time.setText(str(duration))
            # self.ui.move_end_time_lineEdit.setText(str(endTime))
            # self.ui.move_edit_lineEdit.setPlaceholderText(shotName)
            self.ui.create_shot_to_shot_lineEdit.setPlaceholderText(shotName)
            self.ui.edit_from_lineEdit.setPlaceholderText(shotName)
            self.ui.edit_merge_from_lineEdit.setPlaceholderText(shotName)
            shotName_exists = self.modules.obj_exists(shotName)
            if shotName_exists:
                self.modules.select_seq(shotName)
                self.ui.edit_duration_lineEdit.setPlaceholderText(str(duration))


    def sync_shot(self):
        widget_item = self.ui.shotListWidget.selectedItems()[0]
        myQCustomQWidget, shotName, startTime, duration, endTime = widget_item.data(QtCore.Qt.UserRole)
        curStartFrame, curEndFrame, curSeqStartFrame, curSeqEndFrame = self.modules.shot_info(shotName)
        start_time_ui = int(self.ui.move_start_time_lineEdit.text())
        end_time_ui = int(self.ui.move_end_time_lineEdit.text())
        index = self.modules.find_index_shot(self.shots_in_scene, shotName)
        prv_shot = self.shots_in_scene[index-1]
        if index < len(self.shots_in_scene):
            next_shot = self.shots_in_scene[index+1]
        prvStartFrame, prvEndFrame, prvSeqStartFrame, prvSeqEndFrame = self.modules.shot_info(prv_shot)
        nextStartFrame, nextEndFrame, nextSeqStartFrame, nextSeqEndFrame = self.modules.shot_info(next_shot)
        if shotName in self.shot_in_scene:
            if curStartFrame != start_time_ui:
                offset_time = start_time_ui - curStartFrame  #1155 - #1150
                if offset_time > 0:
                    self.modules.set_start_time_seq(shotName, start_time_ui, curSeqStartFrame + offset_time)
                elif offset_time <= 0:
                    if start_time_ui < (prvEndFrame+100): #1134 - 1135  of 1134 - 1150  = -16
                        prv_end_time = prvEndFrame+100
                        clear_space = curStartFrame - prv_end_time #1150 - 1135= 15
                        self.modules.set_start_time_seq(shotName, prv_end_time, curSeqStartFrame - clear_space)
                        self.modules.add_duration_shot(prv_shot, (offset_time + clear_space))
                        # if mc.objExists('%s_sound'%shotName):
                        #     self.modules.add_duration_sound(prv_shot,(offset_time + clear_space))
                    else:
                        self.modules.set_start_time_seq(shotName, start_time_ui, curSeqStartFrame + offset_time)

            if curEndFrame != end_time_ui:
                offset_time = end_time_ui - curEndFrame #1330 - 1329 =-1
                if offset_time < 0:
                    self.modules.set_end_time_seq(shotName , end_time_ui, curSeqEndFrame - abs(offset_time))
                elif offset_time >= 0:
                    if end_time_ui > (nextStartFrame-100): #1336 - 1335
                        next_start_time = nextStartFrame-100
                        clear_space = next_start_time - curEndFrame #1335 -1318 =
                        self.modules.set_end_time_seq(shotName , next_start_time, curSeqEndFrame + clear_space)
                        self.modules.add_duration_shot(shotName, (offset_time - clear_space))
                        # if mc.objExists('%s_sound'%shotName):
                        #     self.modules.add_duration_sound(shotName,(offset_time - clear_space))
                    else:
                        self.modules.set_end_time_seq(shotName , end_time_ui, curSeqEndFrame - abs(offset_time))
            self.display_shot()
            self.clear_move_shot_ui()

    def rebuild_sound(self):
        self.modules.rebuild_sound(self.scene)


    def clear_move_shot_ui(self):
        self.ui.move_start_time_lineEdit.clear()
        self.ui.move_end_time_lineEdit.clear()
        self.ui.move_edit_lineEdit.clear()
        self.ui.move_start_time_lineEdit.setPlaceholderText('StartTime')
        self.ui.move_end_time_lineEdit.setPlaceholderText('EndTime')
        self.ui.move_duration_time_label_time.setText('0')
        self.ui.move_edit_lineEdit.setPlaceholderText('shot')

    def sort_shot(self):
        self.shot_in_scene = self.modules.list_shots()
        self.sort_shot_list = []
        self.sort_shot_list.extend(self.shots_in_sg)
        for index,shot in enumerate(self.shots_in_scene):
            if not shot in self.shots_in_sg:
                self.sort_shot_list.append(shot)
        self.sort_shot_list = sorted(self.sort_shot_list)
        #print self.sort_shot_list
        return self.sort_shot_list

    def set_cus_wid_shot(self, custom_wid_item, start_time, duration, end_time):
        custom_wid_item.setStartTime(start_time)
        custom_wid_item.setDurationTime(duration)
        custom_wid_item.setEndTime(end_time)
        self.ui.move_duration_time_label_time.setText(str(duration))

    def create_shot(self):
        do_shot = self.ui.create_shot_to_shot_lineEdit.placeholderText()
        print do_shot
        shotName = self.ui.create_shot_lineEdit.text()
        duration = int(self.ui.create_shot_duration_lineEdit.text())
        index = self.modules.find_index_shot(self.shots_in_scene, do_shot)
        all_shot = self.shots_in_scene

        if self.ui.before_radio_button.isChecked():
            shot_shift = all_shot[index]
            status = "before"
        elif self.ui.after_radio_button.isChecked():
            shot_shift = all_shot[index+1]
            status = "after"
        elif self.ui.last_radio_button.isChecked():
            shot_shift = all_shot[-1]
            status = "last"
        else:
            return
        print shot_shift,'-------------'
        seq_start_time = self.modules.create_shot(shotName, duration, shot_shift, status, self.cam_path)
        scene= self.scene
        shot_ent = scene.name.split('_')[3]
        entityName = scene.name.replace(shot_ent, shotName)
        scene.context.update(entity = entityName , step='edit', process='sound')
        path = scene.path.output_hero().abs_path()
        sound_filename = scene.publish_name(True, ext='.wav')
        soundPath = '%s/%s' % (path, sound_filename)
        if os.path.exists(soundPath):
            sound_name ='%s_sound' %shot
            self.modules.create_audio(shotName, soundPath, duration, seq_start_time)
            logger.debug('Recreated audio node %s' % sound_name)

        print shot_shift
        self.shot_in_scene = self.modules.list_shots()
        self.sort_shot()
        self.display_shot()

    def sync_sg_one_shot(self):
        default_duration = 1001
        chk = self.modules.confirm_dialog('Are you sure?\nSync shot')
        if chk == 'Yes':
            self.sort_shot()
            widget_item = self.ui.shotListWidget.selectedItems()[0]
            myQCustomQWidget, shotName, startTime, duration, endTime = widget_item.data(QtCore.Qt.UserRole)
            if not shotName in self.shots_in_scene:
                start_frame, seq_start_time = self.modules.find_frame_shot_sg(self.shots_sg, shotName)
                duration = self.modules.find_sg_duration(shotName, self.shots_sg)
                end_time = start_frame + (duration-1)
                seq_end_frame = seq_start_time + (duration-1)
                self.modules.create_new_shot(shotName, self.cam_path, start_frame, end_time, seq_start_time, seq_end_frame)
                ### create sound
                scene= self.scene
                shot_ent = scene.name.split('_')[3]
                entityName = scene.name.replace(shot_ent, shotName)
                scene.context.update(entity = entityName , step='edit', process='sound')
                path = scene.path.output_hero().abs_path()
                sound_filename = scene.publish_name(True, ext='.wav')
                soundPath = '%s/%s' % (path, sound_filename)
                if os.path.exists(soundPath):
                    sound_name ='%s_sound' %shot
                    self.modules.create_audio(shot, soundPath, duration, seq_start_time)
                    logger.debug('Recreated audio node %s' % sound_name)

            elif shotName in self.shots_in_sg:
                curStartFrame, curEndFrame, curSeqStartFrame, curSeqEndFrame = self.modules.shot_info(shotName)
                in_scene_duration = (curEndFrame - curStartFrame) + 1
                duration = self.modules.find_sg_duration(shotName, self.shots_sg)
                if in_scene_duration != duration:
                    offset_time = duration - in_scene_duration
                    self.modules.add_duration_shot(shotName, offset_time)
                    # if mc.objExists('%s_sound'%shotName):
                    #     self.modules.add_duration_sound(shotName, offset_time)
                    

        self.display_shot()

    def sync_sg_shot(self):
        self.sort_shot()
        self.get_list_shot_wid_item()
        chk = self.modules.confirm_dialog('Are you sure?\nSync all shot')
        if chk == 'Yes':
            for wid_item in self.item_list_widget:
                myQCustomQWidget, shotName, startTime, duration, endTime = wid_item.data(QtCore.Qt.UserRole)
                if not shotName in self.shots_in_scene:
                    start_frame, seq_start_time = self.modules.find_frame_shot_sg(self.shots_sg, shotName)
                    duration = self.modules.find_sg_duration(shotName, self.shots_sg)
                    end_time = start_frame + (duration-1)
                    seq_end_frame = seq_start_time + (duration-1)
                    self.modules.create_new_shot(shotName, self.cam_path, start_frame, end_time, seq_start_time, seq_end_frame)
                    ### create sound
                    scene= self.scene
                    shot_ent = scene.name.split('_')[3]
                    entityName = scene.name.replace(shot_ent, shotName)
                    scene.context.update(entity = entityName , step='edit', process='sound')
                    path = scene.path.output_hero().abs_path()
                    sound_filename = scene.publish_name(True, ext='.wav')
                    soundPath = '%s/%s' % (path, sound_filename)
                    if os.path.exists(soundPath):
                        sound_name ='%s_sound' %shot
                        self.modules.create_audio(shot, soundPath, duration, seq_start_time)
                        logger.debug('Recreated audio node %s' % sound_name)
                elif shotName in self.shots_in_sg:
                    curStartFrame, curEndFrame, curSeqStartFrame, curSeqEndFrame = self.modules.shot_info(shotName)
                    in_scene_duration = (curEndFrame - curStartFrame) + 1
                    duration = self.modules.find_sg_duration(shotName, self.shots_sg)
                    if in_scene_duration != duration:
                        offset_time = duration - in_scene_duration
                        self.modules.add_duration_shot(shotName, offset_time)
                        # if mc.objExists('%s_sound'%shotName):
                        #     self.modules.add_duration_sound(shotName, offset_time)
            self.display_shot()
        else:
            pass

    
    def delete_shot(self):
        chk = self.modules.confirm_dialog('Delete shot ?')
        if chk == 'Yes':
            widget_item = self.ui.shotListWidget.selectedItems()[0]
            myQCustomQWidget, shotName, startTime, duration, endTime = widget_item.data(QtCore.Qt.UserRole) 
            self.modules.delete_shot(shotName)
            self.display_shot()
            

    def chg_duration(self):
        shot_duration = int(self.ui.edit_duration_lineEdit.placeholderText())
        to_duration = int(self.ui.edit_duration_to_lineEdit.text())
        back_condition_chk = self.ui.edit_chkbox_direction_back.isChecked()
        front_condition_chk = self.ui.edit_chkbox_direction_front.isChecked()
        shotName = self.ui.edit_from_lineEdit.placeholderText()
        from_duration = self.ui.edit_duration_lineEdit.placeholderText()
        offset_time = to_duration - shot_duration
        if offset_time > 0:
            value_chg = 'Add'
        elif offset_time < 0:
            value_chg = 'Subtract'
        print value_chg, shotName, from_duration
        if shotName:
            print 'shotName: ',shotName
            if value_chg == 'Add':
                if front_condition_chk == True:
                    print '1'
                    self.modules.add_duration(shotName, offset_time, direction = 'Front')
                elif back_condition_chk == True:      
                    print '2'
                    self.modules.add_duration(shotName, offset_time, direction = 'Back')
            elif value_chg =='Subtract':
                if front_condition_chk == True:
                    self.modules.sub_duration(shotName, offset_time, direction = 'Front')
                elif back_condition_chk == True:
                    self.modules.sub_duration(shotName, offset_time, direction = 'Back')
        print shot_duration, to_duration, back_condition_chk, front_condition_chk, shotName, from_duration
        self.display_shot()

    def update_status_duration(self):
        shot_duration = int(self.ui.edit_duration_lineEdit.placeholderText())
        to_duration = int(self.ui.edit_duration_to_lineEdit.text())
        offset_time = to_duration - shot_duration
        if offset_time < 0:
            self.ui.edit_condition_label.setText("Subtract duration shot")
            self.ui.edit_chkbox_grp_box.setStyleSheet("QGroupBox { background-color: rgb(204, 102, 0); border: 0px solid rgb(0, 0, 0); }")
        elif offset_time > 0:
            self.ui.edit_condition_label.setText("Add duration shot")
            self.ui.edit_chkbox_grp_box.setStyleSheet("QGroupBox { background-color: rgb(153, 0, 76); border: 0px solid rgb(0, 0, 0); }")
        else:
            self.ui.edit_condition_label.setText("Not Change")
            self.ui.edit_chkbox_grp_box.setStyleSheet("QGroupBox { background-color: rgb(76, 153, 0); border: 0px solid rgb(0, 0, 0); }")

    def merge_shot(self):
        src_shot = self.ui.edit_merge_from_lineEdit.placeholderText()
        dst_shot = self.ui.edit_merge_to_lineEdit.text()
        self.modules.merge_shot(dst_shot, src_shot)
def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = RFSequencerManager(maya_win.getMayaWindow())
        myApp.show()
        return myApp
if __name__ == '__main__':
    show()
