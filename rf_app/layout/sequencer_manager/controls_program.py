import maya.cmds as mc
from rf_maya.lib import sequencer_lib
reload(sequencer_lib)
from rf_utils.context import context_info 
from rf_utils.sg import sg_process
from rf_utils.pipeline import camera_sequencer
from rf_maya.lib import camera_lib
import maya.mel as mm
import os
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


def list_shots():
    list_shots = mc.sequenceManager(lsh=True)
    if list_shots:
        return sorted(list_shots)
    else:
        return []

def shot_info(shot):
    return sequencer_lib.shot_info(shot)

def get_shot_from_sg(scene_ctx):
    project = scene_ctx.project 
    episode =scene_ctx.episode
    sequence = scene_ctx.sequence
    projectEntity = sg_process.get_project(project)
    episodeEntity = sg_process.get_one_episode(project, episode)
  
    extraFields = ['sg_cut_in', 'sg_cut_out', 'sg_cut_duration', 'sg_working_duration', 'sg_shortcode', 'sg_sequence_code', 'sg_status_list', 'task_assignees']
    orderOption = [{'field_name':'sg_shortcode', 'direction':'asc'}]
    shots = sg_process.get_shot_entities(projectEntity, episodeEntity, sequence, extraFields=extraFields, order= orderOption)
    return shots

def get_context_info():
    if mc.file(q=True, sn=True):
        return context_info.ContextPathInfo()

def list_shotName_sg( shots_sg):
    self.shots_in_sg = []
    for shot in shots_sg:
        self.shots_in_sg.append(shot)

def get_user(user):
    user_data = sg_process.get_local_user(context.local_user)
    return user_data

def add_duration_shot(shotName, offset_frame):
    all_shots = list_shots()
    index = find_index_shot(all_shots, shotName)
    curStartFrame, curEndFrame, curSeqStartFrame, curSeqEndFrame = sequencer_lib.shot_info(all_shots[index])
    nextStartFrame, nextEndFrame, nextSeqStartFrame, nextSeqEndFrame = sequencer_lib.shot_info(all_shots[index+1])

    if offset_frame > 0:
        sequencer_lib.shift_shot_seq(all_shots[index+1], offset_frame, offset_frame)
        sequencer_lib.shift_key(type='start',frameRange=[nextStartFrame, 100000], frame = offset_frame)
        set_time_shot(shotName, curStartFrame, (curEndFrame+offset_frame), curSeqStartFrame, (curSeqEndFrame + offset_frame))
    elif offset_frame < 0 :
        set_time_shot(shotName, curStartFrame, (curEndFrame+offset_frame), curSeqStartFrame, (curSeqEndFrame + offset_frame))
        sequencer_lib.shift_shot_seq(all_shots[index+1], offset_frame, offset_frame)
        sequencer_lib.shift_key(type='start',frameRange=[nextStartFrame, 100000], frame = offset_frame+1)

def add_duration_sound(shotName, offset_frame):
    all_shots = list_shots()
    index = find_index_shot(all_shots, shotName)
    curStartFrame, curEndFrame, curSeqStartFrame, curSeqEndFrame = sequencer_lib.shot_info(all_shots[index])
    nextStartFrame, nextEndFrame, nextSeqStartFrame, nextSeqEndFrame = sequencer_lib.shot_info(all_shots[index+1])

    if offset_frame > 0:
        shift_audio_seq(all_shots[index+1], offset_frame)
        set_time_sound(shotName, seqStartFrame, duration)
    elif offset_frame < 0 :
        set_time_sound(shotName, seqStartFrame, duration)
        shift_audio_seq(all_shots[index+1], offset_frame)

        
def find_index_shot(list_shot, shot_name):
    if shot_name:
        return list_shot.index(shot_name)


def set_time_shot (shotName, startFrame, endFrame, seqStartFrame, seqEndFrame):
    mc.setAttr('%s.startFrame' % shotName, startFrame)
    mc.setAttr('%s.endFrame' % shotName, endFrame)
    mc.setAttr('%s.sequenceStartFrame' % shotName, seqStartFrame)
    mc.setAttr('%s.sequenceEndFrame' % shotName, seqEndFrame)

def set_time_sound (shotName, seqStartFrame, duration):
    sound_name = '%s_sound'%shotName
    mc.setAttr('%s.sourceStart'%sound_name, 0)
    mc.setAttr('%s.sourceEnd'%sound_name, duration)
    mc.setAttr('%s.offset'%sound_name, seqStartFrame)

def find_sg_duration(shotName, sg_data):
    for shot_data in sg_data:
        if shotName == shot_data['sg_shortcode']:
            return shot_data['sg_working_duration']


def set_start_time_seq (shotName , startFrame, sequenceStartFrame):
    mc.setAttr('%s.startFrame' % shotName, startFrame)
    mc.setAttr('%s.sequenceStartFrame' % shotName, sequenceStartFrame)

def set_end_time_seq (shotName , endFrame, sequenceEndFrame):
    mc.setAttr('%s.endFrame' % shotName, endFrame)
    mc.setAttr('%s.sequenceEndFrame' % shotName, sequenceEndFrame)

def get_cam_path(scene_ctx):
    return scene_ctx.projectInfo.asset.global_asset('cam')

def reference_cam(cam_path, shotName): #return cameraShape
    return camera_sequencer.referenceCam(cam_path, shotName)

def shift_shot_seq(shotName, timeOffset=0, seqOffset=0):
    sequencer_lib.shift_shot_seq(shotName, timeOffset, seqOffset)

def create_shot(shotName, duration, shot_shift, status, rigCamPath):
    cameraShape = reference_cam(rigCamPath, shotName)
    all_shots = list_shots()
    gap_shot = 100

    lastEndFrame, lastSeqStartFrame = find_last_end_frame()
    if status == "before":
        curStartFrame, curEndFrame, curSeqStartFrame, curSeqEndFrame = shot_info(shot_shift)
        shift_shot_seq2(shot_shift, duration-1, duration) #duration = 90 , 190
        sequencer_lib.shift_key(type='start',frameRange=[curStartFrame, 100000], frame = duration+99)
        seqStartFrame = curSeqStartFrame 
        seqEndFrame = seqStartFrame + (duration-1)
        start_time = curStartFrame
        end_time = start_time + (duration-1)
    elif status == "after":
        curStartFrame, curEndFrame, curSeqStartFrame, curSeqEndFrame = shot_info(shot_shift)
        shift_shot_seq2(shot_shift, duration-1, duration) #duration = 90 , 190
        print curStartFrame, duration+100
        sequencer_lib.shift_key(type='start',frameRange=[curStartFrame, 100000], frame = duration+99)
        seqStartFrame = curSeqStartFrame 
        seqEndFrame = seqStartFrame + (duration-1)
        start_time = curStartFrame
        end_time = start_time + (duration-1)
    elif status == "last":
        seqStartFrame = lastSeqStartFrame 
        seqEndFrame = lastSeqStartFrame + (duration-1)
        start_time = lastEndFrame + 100
        end_time = start_time + (duration-1)
    else:
        return
    cam_seq_shot = camera_lib.create_shot(shotName, cameraShape, start_time, end_time, seqStartFrame, seqEndFrame)
    return seqStartFrame


def create_audio(shotName, soundPath, duration, seq_start_time):
    sound_name ='%s_sound' %shotName
    sound_tranform = mc.sound(file = soundPath, n = sound_name, offset = seq_start_time)
    mc.sequenceManager(ata=sound_tranform)
    mc.setAttr('%s.sourceStart'%sound_tranform, 0)
    mc.setAttr('%s.sourceEnd'%sound_tranform, duration)
    mc.setAttr('%s.offset'%sound_tranform, seq_start_time)

def shift_seq(shotName, timeOffset=0, seqOffset=0, mute=False):
    all_shot = list_shots()
    all_shot = sorted(all_shot)
    order_shot = all_shot.index(shotName)
    list_shot = sorted(all_shot)

    for index, ix in enumerate(list_shot):
        if order_shot > index:
            all_shot.remove(ix)
    print all_shot,'--------------------------'
    if seqOffset > 0:
        shotList = sorted(all_shot)[::-1]
    # shift backward, start with first shot
    elif seqOffset < 0:
        shotList = sorted(all_shot)
    for shot in shotList:
        shift_shot(shot, timeOffset, seqOffset, mute)

def shift_shot(shotName, timeOffset=0, seqOffset=0, mute=True):
    currentStartFrame, currentEndFrame, currentSeqStartFrame, currentSeqEndFrame = shot_info(shotName)
    startFrame = currentStartFrame + timeOffset
    endFrame = currentEndFrame + timeOffset
    seqStartFrame = currentSeqStartFrame + seqOffset
    seqEndFrame = currentSeqEndFrame + seqOffset

    if seqOffset > 0:
        mc.setAttr('%s.startFrame' % shotName, startFrame)
        mc.setAttr('%s.endFrame' % shotName, endFrame)
        mc.setAttr('%s.sequenceStartFrame' % shotName, seqStartFrame)
        mc.setAttr('%s.sequenceEndFrame' % shotName, seqEndFrame)

    if seqOffset < 0:
        mc.setAttr('%s.endFrame' % shotName, endFrame)
        mc.setAttr('%s.startFrame' % shotName, startFrame)
        mc.setAttr('%s.sequenceStartFrame' % shotName, seqStartFrame)
        mc.setAttr('%s.sequenceEndFrame' % shotName, seqEndFrame)

def find_frame_shot_sg(sg_data, shotName):
    default_start_frame = 1001
    gap_shot = 100
    frame = default_start_frame
    seq_fram = default_start_frame
    check = True
    for shot in sg_data:
        shotCode = shot['sg_shortcode']
        duration = shot['sg_working_duration'] -1 
        if shotCode == shotName:
            check = False
        if check == True:
            frame = frame + duration
            frame = frame + gap_shot
            seq_fram = seq_fram + duration+1
    return frame, seq_fram

def create_new_shot(shotName, cam_path, start_time, end_time, seqStartFrame=0, seqEndFrame=0):
    cam_shpae = camera_sequencer.referenceCam(cam_path, shotName)
    print shotName, cam_shpae, start_time, end_time, seqStartFrame, seqEndFrame
    camera_lib.create_shot(shotName, cam_shpae, start_time, end_time, seqStartFrame, seqEndFrame)

def find_last_end_frame():
    all_shot = list_shots()
    list_end_frame = []
    list_end_seq_frame = []
    for ix in all_shot:
        end_frame = mc.getAttr("%s.endFrame"%ix)
        end_seq_frame = mc.getAttr("%s.sequenceEndFrame"%ix)
        list_end_frame.append(end_frame)
        list_end_seq_frame.append(end_seq_frame)
    return [max(list_end_frame), max(list_end_seq_frame)+1]

def delete_shot(shotName):
    mc.delete(shotName)

def shift_shot_seq2(shotName, timeOffset=0, seqOffset=0, mute=False):
    all_shot = list_shots()
    all_shot = sorted(all_shot)
    order_shot = all_shot.index(shotName)
    list_shot = sorted(all_shot)

    frameInfo = []
    for index, ix in enumerate(list_shot):
        if order_shot > index:
            all_shot.remove(ix)
    print all_shot,'--------------------------'
    if timeOffset > 0:
        shotList = sorted(all_shot)[::-1]
    # shift backward, start with first shot
    elif timeOffset < 0:
        shotList = sorted(all_shot)
    for shot in shotList:
        sequencer_lib.shift_shot(shot, timeOffset+100, seqOffset, mute)

def select_seq(shotName):
    curStartFrame, curEndFrame, curSeqStartFrame, curSeqEndFrame = sequencer_lib.shot_info(shotName)
    mc.playbackOptions( minTime= curStartFrame, maxTime= curEndFrame )

def confirm_dialog(text):
    chk = mc.confirmDialog( title='Confirm', message=text, button=['Yes','No'], defaultButton='Yes', cancelButton='No', dismissString='No',messageAlign= 'center' )
    return chk

def obj_exists(obj):
    return mc.objExists(obj)

def add_duration(shotName, offset, direction = ''):
    all_shots = list_shots()
    offset = abs(offset)
    index = find_index_shot(all_shots, shotName)
    curStartFrame, curEndFrame, curSeqStartFrame, curSeqEndFrame = sequencer_lib.shot_info(all_shots[index])
    nextStartFrame, nextEndFrame, nextSeqStartFrame, nextSeqEndFrame = sequencer_lib.shot_info(all_shots[index+1])
    exists = obj_exists(all_shots[index+1])
    if obj_exists:
        print direction
        if direction == "Front":
            shift_seq(shotName, 0, offset)
            mc.setAttr('%s.startFrame' % shotName, curStartFrame - offset)
            mc.setAttr('%s.sequenceStartFrame' % shotName, curSeqStartFrame)
            mc.setAttr("%s.track"%shotName, 1)

        elif direction == "Back":
            shift_seq(all_shots[index+1], 0, offset)
            set_time_shot(shotName, curStartFrame, (curEndFrame+offset), curSeqStartFrame, (curSeqEndFrame + offset))

def sub_duration(shotName, offset, direction = ''):
    all_shots = list_shots()
    curStartFrame, curEndFrame, curSeqStartFrame, curSeqEndFrame = sequencer_lib.shot_info(shotName)
    index = find_index_shot(all_shots, shotName)
    if direction == 'Front':
        curStartFrame = curStartFrame + abs(offset) # +
        curSeqStartFrame = curSeqStartFrame +abs(offset)
        mc.setAttr('%s.startFrame' % shotName, curStartFrame)
        mc.setAttr('%s.sequenceStartFrame' % shotName, curSeqStartFrame)
        shift_seq(shotName, 0, offset)
    elif direction == 'Back':
        curEndFrame = curEndFrame + offset # -
        curSeqEndFrame = curSeqEndFrame +offset
        mc.setAttr('%s.endFrame' % shotName, curEndFrame)
        mc.setAttr('%s.sequenceEndFrame' % shotName, curSeqEndFrame)
        shift_seq(all_shots[index+1], 0, offset)

def mute_shot(shotName, mute =True):
    mc.shot(shotName, e=True, mute=mute)

def merge_shot(dst_shot, src_shot):
    mute_shot(src_shot, mute =True)
    mc.setAttr("%s.track"%src_shot, 2)
    srcStartFrame, srcEndFrame, srcSeqStartFrame, srcSeqEndFrame = sequencer_lib.shot_info(src_shot)
    curStartFrame, curEndFrame, curSeqStartFrame, curSeqEndFrame = sequencer_lib.shot_info(dst_shot)
    offset = srcEndFrame - srcStartFrame +1
    mc.setAttr('%s.endFrame' % dst_shot, curEndFrame+ offset)
    mc.setAttr('%s.sequenceEndFrame' % dst_shot, curSeqEndFrame + offset)

def rebuild_sound(scene):
    list_shots = mc.sequenceManager(lsh=True)
    all_audio = mc.ls(type='audio')
    for ix in all_audio:
        mc.delete(ix)
    for shot in list_shots:
        shot_ent = scene.name.split('_')[3]
        entityName = scene.name.replace(shot_ent, shot)
        scene.context.update(entity = entityName , step='edit', process='sound')
        path = scene.path.output_hero().abs_path()
        filename = scene.publish_name(True, ext='.wav')
        soundPath = '%s/%s' % (path, filename)
        seq_start_time = mc.getAttr('%s.sequenceStartFrame' % shot)
        seq_end_time = mc.getAttr('%s.sequenceEndFrame' % shot)
        duration = (seq_end_time - seq_start_time) +1
        #create sound 
        print soundPath
        if os.path.exists(soundPath):
            sound_name ='%s_sound' %shot
            create_audio(shot, soundPath, duration, seq_start_time)
            logger.debug('Recreated audio node %s' % sound_name)

def list_all_sounds():
    list_shots = mc.sequenceManager(lsh=True)
    list_sounds = []
    if list_shots:
        for shot in list_shots:
            if mc.objExists('%s_sound'%shot):
                list_sounds.append('%s_sound'%shot)
        return list_sounds
    else:
        return []

def shift_audio_seq(shotName, seqOffset=0):
    all_sounds = list_all_sounds()
    all_sounds = sorted(all_sounds)
    order_shot = all_sounds.index('%s_sound'%shotName)
    list_sound = sorted(all_sounds)

    frameInfo = []
    for index, ix in enumerate(list_sound):
        if order_shot > index:
            all_sounds.remove(ix)
    print all_sounds,'--------------------------'
    if seqOffset > 0:
        shotList = sorted(all_sounds)[::-1]
    # shift backward, start with first shot
    elif seqOffset < 0:
        shotList = sorted(all_sounds)
    for shot in shotList:
        shift_audio(shotName, seqOffset)


def shift_audio(shotName, seqOffset=0):
    currentStartFrame, currentEndFrame, currentSeqStartFrame, currentSeqEndFrame = sequencer_lib.shot_info(shotName)
    seq_start_time = currentSeqStartFrame + seqOffset
    duration = (currentSeqEndFrame - currentSeqStartFrame) +1
    sound_name ='%s_sound' %shotName
    mc.setAttr('%s.sourceStart'%sound_name, 0)
    mc.setAttr('%s.sourceEnd'%sound_name, duration)
    mc.setAttr('%s.offset'%sound_name, seq_start_time)



########### update shotgun 

def update_task_duration(project, code, duration):
    # projectEntity = sg_process.get_project(project)
    # episodeEntity = sg_process.get_one_episode(project, episode)

    extraFields = ['sg_cut_in', 'sg_cut_out', 'sg_cut_duration', 'sg_working_duration', 'sg_shortcode', 'sg_sequence_code', 'sg_status_list']

    entity = sg_process.get_shot_entity(project, code, extraFields)
    sg_process.update_shot_duration(entity, entity.get('id'), duration)
    return entity

