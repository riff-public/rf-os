# -*- coding: utf-8 -*-
import os
import sys

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_utils import icon
import controls_program 
import maya.cmds as mc

class RFSequencerManagerUI(QtWidgets.QWidget):
    sendData = QtCore.Signal(list)
    def __init__(self, parent=None) :
        super(RFSequencerManagerUI, self).__init__(parent)
        #create listWidget
        self.horizontalSpacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalSpacer = QtWidgets.QSpacerItem(20, 200, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.allLayout = QtWidgets.QHBoxLayout()
        self.logo_layout = QtWidgets.QHBoxLayout()
        self.logo_label = QtWidgets.QLabel()
        self.logo_label.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation))
        self.logo_label.setAlignment(QtCore.Qt.AlignCenter)

        # self.logo_layout.addWidget(self.logo_label)
    
        self.mainLayout = QtWidgets.QGridLayout()
        self.shotName_layout = QtWidgets.QVBoxLayout()
        self.refresh_pushbutton = QtWidgets.QPushButton('refresh')
        self.refresh_pushbutton.setStyleSheet("QPushButton { background-color: rgb(11, 102, 35);solid rgb(0, 0, 0); }")
        self.shotList_label = QtWidgets.QLabel('shotList')
        self.shotListWidget = QtWidgets.QListWidget()
        # self.shotListWidget.setDragEnabled(True)
        # self.shotListWidget.setDragDropMode(QtWidgets.QAbstractItemView.DragDrop )
        self.shotName_layout.addWidget(self.refresh_pushbutton)
        self.shotName_layout.addWidget(self.shotList_label)
        self.shotName_layout.addWidget(self.shotListWidget)


        self.create_shot_layout = QtWidgets.QVBoxLayout()

        self.create_shot_label = QtWidgets.QLabel('ShotName')
        self.create_shot_lineEdit = QtWidgets.QLineEdit()
        self.create_shot_name_layout = QtWidgets.QHBoxLayout()
        self.create_shot_name_layout.addWidget(self.create_shot_label)
        self.create_shot_name_layout.addWidget(self.create_shot_lineEdit)

        self.create_shot_time_layout = QtWidgets.QHBoxLayout()
        self.create_shot_start_time_Label = QtWidgets.QLabel('Start')
        self.create_shot_end_time_Label = QtWidgets.QLabel('End')
        self.create_shot_start_time_lineEdit = QtWidgets.QLineEdit()
        self.create_shot_end_time_lineEdit = QtWidgets.QLineEdit()
        self.create_shot_time_layout.addWidget(self.create_shot_start_time_Label)
        self.create_shot_time_layout.addWidget(self.create_shot_start_time_lineEdit)
        self.create_shot_time_layout.addWidget(self.create_shot_end_time_Label)
        self.create_shot_time_layout.addWidget(self.create_shot_end_time_lineEdit)

        self.create_shot_duration_layout = QtWidgets.QHBoxLayout()
        self.create_shot_duration_label = QtWidgets.QLabel('Duration')
        self.create_shot_duration_lineEdit = QtWidgets.QLineEdit()
        self.create_shot_duration_layout.addWidget(self.create_shot_duration_label)
        self.create_shot_duration_layout.addWidget(self.create_shot_duration_lineEdit)

        self.create_shot_radio_layout = QtWidgets.QHBoxLayout()
        self.before_radio_button = QtWidgets.QRadioButton('before')
        self.after_radio_button = QtWidgets.QRadioButton('after')
        self.last_radio_button = QtWidgets.QRadioButton('last')
        self.create_shot_radio_layout.addWidget(self.before_radio_button)
        self.create_shot_radio_layout.addWidget(self.after_radio_button)
        self.create_shot_radio_layout.addWidget(self.last_radio_button)

        self.create_shot_to_shot_layout = QtWidgets.QHBoxLayout()
        self.create_shot_to_shot_label = QtWidgets.QLabel('From Shot')
        self.create_shot_to_shot_lineEdit = QtWidgets.QLineEdit()
        self.create_shot_to_shot_lineEdit.setReadOnly(True)
        self.create_shot_to_shot_layout.addWidget(self.create_shot_to_shot_label)
        self.create_shot_to_shot_layout.addWidget(self.create_shot_to_shot_lineEdit)


        self.create_shot_pushbutton = QtWidgets.QPushButton('Create Shot')
        self.create_shot_layout.addLayout(self.create_shot_name_layout)
        # self.create_shot_layout.addLayout(self.create_shot_time_layout)
        self.create_shot_layout.addLayout(self.create_shot_duration_layout)
        self.create_shot_layout.addLayout(self.create_shot_radio_layout)
        self.create_shot_layout.addLayout(self.create_shot_to_shot_layout)
        self.create_shot_layout.addWidget(self.create_shot_pushbutton)

        self.groupBox_create_shot = QtWidgets.QGroupBox('Create Shot')
        self.groupBox_create_shot.setLayout(self.create_shot_layout)

        self.edit_layout = QtWidgets.QVBoxLayout()
        self.edit_duration_all_layout = QtWidgets.QVBoxLayout()

        self.edit_from_layout = QtWidgets.QHBoxLayout()
        self.edit_from_label = QtWidgets.QLabel(' Shot')
        self.edit_from_lineEdit = QtWidgets.QLineEdit()
        self.edit_from_lineEdit.setReadOnly(True)
        self.edit_from_layout.addWidget(self.edit_from_label)
        self.edit_from_layout.addWidget(self.edit_from_lineEdit)

        self.edit_duration_layout = QtWidgets.QHBoxLayout()
        self.edit_duration_label = QtWidgets.QLabel('From duration')
        self.edit_duration_lineEdit = QtWidgets.QLineEdit()
        self.edit_duration_lineEdit.setReadOnly(True)
        self.edit_duration_layout.addWidget(self.edit_duration_label)
        self.edit_duration_layout.addWidget(self.edit_duration_lineEdit)

        self.edit_duration_to_layout = QtWidgets.QHBoxLayout()
        self.edit_duration_to_label = QtWidgets.QLabel('To duration')
        self.edit_duration_to_lineEdit = QtWidgets.QLineEdit()
        self.edit_duration_to_layout.addWidget(self.edit_duration_to_label)
        self.edit_duration_to_layout.addWidget(self.edit_duration_to_lineEdit)

        self.edit_chkbox_layout = QtWidgets.QHBoxLayout()
        self.edit_condition_label = QtWidgets.QLabel('Input To duration')
        self.edit_chkbox_layout.addWidget(self.edit_condition_label)
        self.edit_chkbox_grp_box = QtWidgets.QGroupBox()
        self.edit_chkbox_grp_box.setLayout(self.edit_chkbox_layout)
        self.edit_chkbox_grp_box.setStyleSheet("QGroupBox { background-color: rgb(7, 136, 70); border: 0px solid rgb(0, 0, 0); }")

        self.edit_chkbox_direction_layout = QtWidgets.QHBoxLayout()
        self.edit_chkbox_direction_front = QtWidgets.QRadioButton('Front')
        self.edit_chkbox_direction_back = QtWidgets.QRadioButton('Back')
        self.edit_chkbox_direction_layout.addWidget(self.edit_chkbox_direction_front)
        self.edit_chkbox_direction_layout.addWidget(self.edit_chkbox_direction_back)
        self.edit_chkbox_direction_grp_box = QtWidgets.QGroupBox()
        self.edit_chkbox_direction_grp_box.setLayout(self.edit_chkbox_direction_layout)
        self.edit_chkbox_direction_grp_box.setStyleSheet("QGroupBox { background-color: rgb(0, 130, 130); border: 0px solid rgb(0, 0, 0); }")

        self.edit_duration_chg_layout =QtWidgets.QHBoxLayout()
        self.edit_duration_chg_label = QtWidgets.QLabel('Change value')
        self.edit_duration_chg_lineEdit = QtWidgets.QLineEdit()
        self.edit_duration_chg_layout.addWidget(self.edit_duration_chg_label)
        self.edit_duration_chg_layout.addWidget(self.edit_duration_chg_lineEdit)

        self.edit_duration_pushbutton = QtWidgets.QPushButton('Change Duration')

        self.edit_merge_layout = QtWidgets.QGridLayout()
        self.edit_merge_label = QtWidgets.QLabel('Merge Shot')
        self.edit_merge_from_label = QtWidgets.QLabel('From Shot')
        self.edit_merge_from_lineEdit = QtWidgets.QLineEdit()
        self.edit_merge_to_label = QtWidgets.QLabel('To')
        self.edit_merge_to_lineEdit = QtWidgets.QLineEdit()
        self.edit_merge_layout.addWidget(self.edit_merge_label, 0,0,1,1)
        self.edit_merge_layout.addWidget(self.edit_merge_from_label, 1,0,1,1)
        self.edit_merge_layout.addWidget(self.edit_merge_from_lineEdit, 1,1,1,1)
        self.edit_merge_layout.addWidget(self.edit_merge_to_label, 2,0,1,1)
        self.edit_merge_layout.addWidget(self.edit_merge_to_lineEdit, 2,1,1,1)
        self.edit_merge_pushbutton = QtWidgets.QPushButton('Merge')

        self.delete_shot_pushbutton = QtWidgets.QPushButton('Delete Shot')
        self.rebuild_sound_pushbutton = QtWidgets.QPushButton('Rebuild Sound')

        self.h_line = QtWidgets.QFrame()


        self.edit_duration_all_layout.addLayout(self.edit_from_layout)
        self.edit_duration_all_layout.addLayout(self.edit_duration_layout)
        self.edit_duration_all_layout.addLayout(self.edit_duration_to_layout)
        self.edit_duration_all_layout.addWidget(self.edit_chkbox_grp_box)
        self.edit_duration_all_layout.addWidget(self.h_line)
        self.edit_duration_all_layout.addWidget(self.edit_chkbox_direction_grp_box)
        self.edit_duration_all_layout.addLayout(self.edit_chkbox_direction_layout)
        self.edit_duration_all_layout.addWidget(self.edit_duration_pushbutton)
        self.edit_duration_all_layout.addLayout(self.edit_merge_layout)
        self.edit_duration_all_layout.addWidget(self.edit_merge_pushbutton)
        self.edit_duration_all_layout.addWidget(self.delete_shot_pushbutton)
        
        self.groupBox_edit_shot = QtWidgets.QGroupBox('Edit Shot')
        self.groupBox_edit_shot.setLayout(self.edit_duration_all_layout)


        self.edit_layout.addWidget(self.groupBox_create_shot)
        self.edit_layout.addWidget(self.groupBox_edit_shot)
        self.edit_layout.addWidget(self.rebuild_sound_pushbutton)
        self.edit_layout.addWidget(self.delete_shot_pushbutton)



        self.groupBox_Description = QtWidgets.QGroupBox('Shot Description')
        self.groupBox_Description.setAlignment(QtCore.Qt.AlignRight)
        self.groupBox_Description.setMinimumSize(250, 100)
        self.groupBox_Description.setMaximumSize(0, 100)

        self.Description_layout = QtWidgets.QGridLayout()
        self.Description_project_label = QtWidgets.QLabel('Project :')
        self.Description_project_label_detail = QtWidgets.QLabel('-')
        self.Description_episode_label = QtWidgets.QLabel('Episode :')
        self.Description_episode_label_detail = QtWidgets.QLabel('-')
        self.Description_sequence_label = QtWidgets.QLabel('Sequence :')
        self.Description_sequence_label_detail = QtWidgets.QLabel('-')
        self.Description_shot_label = QtWidgets.QLabel('Shot :')
        self.Description_shot_label_detail = QtWidgets.QLabel('-')

        self.Description_layout.addWidget(self.Description_project_label,0,0,1,1)
        self.Description_layout.addWidget(self.Description_project_label_detail,0,1,1,1)
        self.Description_layout.addWidget(self.Description_episode_label,1,0,1,1)
        self.Description_layout.addWidget(self.Description_episode_label_detail,1,1,1,1)
        self.Description_layout.addWidget(self.Description_sequence_label,2,0,1,1)
        self.Description_layout.addWidget(self.Description_sequence_label_detail,2,1,1,1)
        self.Description_layout.addWidget(self.Description_shot_label,3,0,1,1)
        self.Description_layout.addWidget(self.Description_shot_label_detail,3,1,1,1)
        self.groupBox_Description.setLayout(self.Description_layout)

        self.push_sync_layout = QtWidgets.QGridLayout()

        self.sync_sg_pushbutton = QtWidgets.QPushButton('sync one shot')
        self.sync_sg_all_pushbutton = QtWidgets.QPushButton('sync all shot')
        self.push_sync_layout.addWidget(self.sync_sg_pushbutton, 0, 0, 1, 1)
        self.push_sync_layout.addWidget(self.sync_sg_all_pushbutton, 0, 1, 1, 1)
        self.shotName_layout.addLayout(self.push_sync_layout)


        self.left_layout = QtWidgets.QVBoxLayout()
        self.right_layout = QtWidgets.QVBoxLayout()

        self.left_layout.addWidget(self.logo_label)
        self.left_layout.addWidget(self.groupBox_Description)
        self.left_layout.addLayout(self.shotName_layout)

        # self.right_layout.addWidget(self.groupBox_Description)
        # self.right_layout.addLayout(self.edit_layout)
        self.left_layout.addLayout(self.edit_layout)

        self.mainLayout.addLayout(self.left_layout, 0,0,1,1)
        self.mainLayout.addLayout(self.right_layout, 0,1,1,1)

        # self.mainLayout.addWidget(self.logo_label,0,0,1,1,1)
        # self.mainLayout.addWidget(self.groupBox_Description,0,2,1,1,1)
        # self.mainLayout.addLayout(self.shotName_layout, 1,0,1,2,1)
        # self.mainLayout.addLayout(self.edit_layout,1,2,1,2,1)
        

        self.allLayout.addLayout(self.mainLayout)
        self.setLayout(self.allLayout)

    def add_custom_wid(self, startTime, duration, endTime ,shotName, color=''):
        
        self.myQCustomQWidget = QCustomQWidget()
        self.myQCustomQWidget.setShot(shotName)
        self.myQCustomQWidget.setStartTime(startTime)
        self.myQCustomQWidget.setDurationTime(duration)
        self.myQCustomQWidget.setEndTime(endTime)
        myQListWidgetItem = QtWidgets.QListWidgetItem(self.shotListWidget)
        myQListWidgetItem.setSizeHint(self.myQCustomQWidget.sizeHint())
        data = [self.myQCustomQWidget,shotName, startTime, duration, endTime]
        myQListWidgetItem.setData(QtCore.Qt.UserRole, data)
        self.shotListWidget.addItem(myQListWidgetItem)
        self.shotListWidget.setItemWidget(myQListWidgetItem, self.myQCustomQWidget)

    def set_listwidget_color(self, item, color):
        item.setBackgroundColor(color)
    
    def contextMenuEvent(self,event):

        index = self.shotListWidget.currentIndex()
        contextMenu = QtWidgets.QMenu(self.shotListWidget)
        
        openFileAct = contextMenu.addAction("Update Duration Shotgun")

        action = contextMenu.exec_(self.mapToGlobal(event.pos()))

        if action == openFileAct:
            data =  self.shotListWidget.itemFromIndex(index).data(QtCore.Qt.UserRole)
            self.sendData.emit(data)


class QCustomQWidget(QtWidgets.QWidget):
    refresh = QtCore.Signal(bool)
    def __init__(self, parent = None):
        super(QCustomQWidget, self).__init__(parent)

        self.main_layout = QtWidgets.QHBoxLayout()
        self.content_layout = QtWidgets.QVBoxLayout()
        self.time_layout = QtWidgets.QHBoxLayout()

        self.shotName_layout = QtWidgets.QHBoxLayout()
        self.shotName_text_label = QtWidgets.QLabel()
        self.shotName_text_label.setStyleSheet("QLabel {color: #FFFFFF}")
        self.shotName_text_label.setAlignment(QtCore.Qt.AlignCenter)
        self.shotName_layout.addWidget(self.shotName_text_label)

        self.startTime_layout = QtWidgets.QVBoxLayout()
        self.startTime_text_label = QtWidgets.QLabel('startTime')
        self.startTime_text_label.setObjectName('startTime')
        self.startTime_text_label.setStyleSheet("QLabel#startTime {color: #000000}")
        self.startTime_num_label = QtWidgets.QLabel()
        self.startTime_text_label.setAlignment(QtCore.Qt.AlignLeft)
        self.startTime_num_label.setAlignment(QtCore.Qt.AlignLeft)
        self.startTime_num_label.setStyleSheet("QLabel {color: #000000}")
        self.startTime_layout.addWidget(self.startTime_text_label)
        self.startTime_layout.addWidget(self.startTime_num_label)


        self.duration_layout = QtWidgets.QVBoxLayout()
        self.duration_text_label = QtWidgets.QLabel('shot\nduration')
        self.duration_num_label = QtWidgets.QLabel()
        self.duration_text_label.setStyleSheet("QLabel {color: #003366}")
        self.duration_num_label.setStyleSheet("QLabel {color: #003366}")
        self.duration_text_label.setAlignment(QtCore.Qt.AlignCenter)
        self.duration_num_label.setAlignment(QtCore.Qt.AlignCenter)
        self.duration_layout.addWidget(self.duration_text_label)
        self.duration_layout.addWidget(self.duration_num_label)

        self.duration_sg_layout = QtWidgets.QVBoxLayout()
        self.duration_sg_text_label = QtWidgets.QLabel('sg\nduration')
        self.duration_sg_num_label = QtWidgets.QLabel()
        self.duration_sg_text_label.setStyleSheet("QLabel {font-weight:600;}")
        self.duration_sg_num_label.setStyleSheet("QLabel {font-weight:600;}")
        self.duration_sg_text_label.setAlignment(QtCore.Qt.AlignCenter)
        self.duration_sg_num_label.setAlignment(QtCore.Qt.AlignCenter)
        self.duration_sg_layout.addWidget(self.duration_sg_text_label)
        self.duration_sg_layout.addWidget(self.duration_sg_num_label)

        self.endTime_layout = QtWidgets.QVBoxLayout()
        self.endTime_text_label = QtWidgets.QLabel('endTime')
        self.endTime_num_label = QtWidgets.QLabel()
        self.endTime_text_label.setStyleSheet("QLabel {color: #523D03}")
        self.endTime_num_label.setStyleSheet("QLabel {color: #523D03}")
        self.endTime_text_label.setAlignment(QtCore.Qt.AlignRight)
        self.endTime_num_label.setAlignment(QtCore.Qt.AlignRight)
        self.endTime_layout.addWidget(self.endTime_text_label)
        self.endTime_layout.addWidget(self.endTime_num_label)


        self.time_layout.addLayout(self.startTime_layout)
        self.time_layout.addLayout(self.duration_layout)
        self.time_layout.addLayout(self.duration_sg_layout)
        self.time_layout.addLayout(self.endTime_layout)

        self.content_layout.addLayout(self.shotName_layout)
        self.content_layout.addLayout(self.time_layout)

        self.main_layout.addLayout(self.content_layout)

        self.setLayout(self.main_layout)
        
    def setShot(self, shotName):
        self.shotName_text_label.setText(str(shotName))

    def setStartTime(self, time):
        self.startTime_num_label.setText(str(time))

    def setDurationTime(self, time):
        self.duration_num_label.setText(str(time))

    def setDurationTimeSG(self, time):
        self.duration_sg_num_label.setText(str(time))

    def setEndTime(self, time):
        self.endTime_num_label.setText(str(time))

