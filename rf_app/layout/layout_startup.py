import os
import sys
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils.pipeline import camera_sequencer
reload(camera_sequencer)
from rf_utils.context import context_info
from rf_utils.widget import dialog


def build_cam(rebuild=True):
	scene = context_info.ContextPathInfo()
	rigCam = scene.projectInfo.asset.global_asset('cam')
	if scene.project:
		if os.path.exists(rigCam):
			camera_sequencer.create_sequencer_shots(scene.sequence, rigCam)
		else:
			logger.warning('Camera rig not found')
			dialog.MessageBox.warning('Error', 'Camera rig not found')
	else:
		logger.warning('Scene not correct')
		dialog.MessageBox.warning('Error', 'Scene not in the pipeline')
