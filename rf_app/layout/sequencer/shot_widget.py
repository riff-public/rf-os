import os
import sys
import traceback
from collections import OrderedDict
from functools import partial

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

class Label: 
    title = {'Extend': 'Shot Extend Options', 'Trim': 'Shot Trim Options'}
    shiftOption = {'Extend': 'Extend from current frame', 'Trim': 'Trim from current frame'}


class ShotModifyUi(QtWidgets.QDialog):
    currentIndexChanged = QtCore.Signal(str)
    def __init__(self, mode, parent=None):
        super(ShotModifyUi, self).__init__(parent)

        # layout
        self.allLayout = QtWidgets.QVBoxLayout()

        # options 
        # option layout 
        self.optionLayout = QtWidgets.QHBoxLayout()
        self.headRB = QtWidgets.QRadioButton('Start')
        self.tailRB = QtWidgets.QRadioButton('End')
        self.tailRB.setChecked(True)

        self.shiftOptionCB = QtWidgets.QCheckBox(Label.shiftOption.get(mode))
        self.fitRangeCB = QtWidgets.QCheckBox('Fit range to Shot')

        self.optionLayout.addWidget(self.headRB)
        self.optionLayout.addWidget(self.tailRB)

        self.valueLayout = QtWidgets.QHBoxLayout()
        self.label = QtWidgets.QLabel('Value : ')
        self.valueLineEdit = QtWidgets.QLineEdit('0')
        self.valueLayout.addWidget(self.label)
        self.valueLayout.addWidget(self.valueLineEdit)

        self.shiftCB = QtWidgets.QCheckBox('Shift Key')
        self.shiftCB.setChecked(True)


        self.buttonLayout = QtWidgets.QHBoxLayout()
        self.okButton = QtWidgets.QPushButton('OK')
        self.cancelButton = QtWidgets.QPushButton('Cancel')
        self.buttonLayout.addWidget(self.okButton)
        self.buttonLayout.addWidget(self.cancelButton)

        self.okButton.setMinimumSize(QtCore.QSize(0, 26))
        self.cancelButton.setMinimumSize(QtCore.QSize(0, 26))
        self.buttonLayout.setSpacing(4)

        # add layout
        self.allLayout.addLayout(self.optionLayout)
        self.allLayout.addLayout(self.valueLayout)
        self.allLayout.addWidget(self.shiftCB)
        self.allLayout.addWidget(self.shiftOptionCB)
        self.allLayout.addWidget(self.fitRangeCB)
        self.allLayout.addLayout(self.buttonLayout)
        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(8, 8, 8, 8)
        self.setLayout(self.allLayout)

        self.setWindowTitle(Label.title.get(mode))
        self.resize(250, 120)

        self.dictValue = {'head': False, 'tail': False, 'value': 0, 'shift': False, 'shiftCurrent': False, 'fitRange': False}
        self.confirm = False
        self.init_signals()

    def init_signals(self): 
        self.okButton.clicked.connect(self.submit)
        self.cancelButton.clicked.connect(self.close)

    def submit(self): 
        head = self.headRB.isChecked()
        tail = self.tailRB.isChecked()
        shift = self.shiftCB.isChecked()
        value = int(self.valueLineEdit.text())
        currentFrame = int(self.shiftOptionCB.isChecked())
        fitRange = int(self.fitRangeCB.isChecked())

        if value < 0: 
            QtWidgets.QMessageBox.warning(self, 'Error', 'Please enter a positive number')
            return 
        self.dictValue = {'head': head, 'tail': tail, 'value': value, 'shift': shift, 'shiftCurrent': currentFrame, 'fitRange': fitRange}
        self.confirm = True
        self.close()


    @staticmethod
    def show(mode='Extend', parent=None):
        dialog = ShotModifyUi(mode, parent=parent)
        result = dialog.exec_()
        return dialog

