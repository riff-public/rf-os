from rf_utils.pipeline import camera_sequencer
from rf_maya.lib import camera_lib

import maya.cmds as mc
import os

def get_sequencer_in_scene():
    return sorted(mc.sequenceManager(lsh=True)) if mc.sequenceManager(lsh=True) else []

def get_sound_in_scene(soundName):
    return True if mc.objExists(soundName) else False

def create_camera_sequencer(scene, rigCamPath, shotCode, start_frame, end_frame, start_time, end_time):
    if not mc.objExists('Cam_Grp'):
        camera_sequencer.setResolution(scene.project)
        mc.setAttr('defaultResolution.deviceAspectRatio', 2.353)

    cameraShape = camera_sequencer.referenceCam(rigCamPath, shotCode, scene)
    sequencerName = camera_lib.create_shot(shotCode, cameraShape, start_time, end_time)
    mc.setAttr('%s.startFrame'% sequencerName, start_frame)
    mc.setAttr('%s.endFrame'% sequencerName, end_frame)

def set_sequencer(sequencerName, start_frame, end_frame, start_time, end_time):
    mc.setAttr('%s.startFrame'% sequencerName, start_frame)
    mc.setAttr('%s.endFrame'% sequencerName, end_frame)
    mc.setAttr('%s.sequenceStartFrame'% sequencerName, start_time)
    mc.setAttr('%s.scale'% sequencerName, 1)

def create_sound_sequencer(scene, shotCode, start_time, duration):
    path = scene.path.output_hero().abs_path()
    filename = scene.publish_name(True, ext='.wav')
    soundPath = '%s/%s' % (path, filename)

    if os.path.exists(soundPath):
        sound_name ='%s_sound' %shotCode
        sound_tranform = mc.sound(file = soundPath, n = sound_name, offset = start_time)
        mc.sequenceManager(ata=sound_tranform)
        mc.setAttr('%s.sourceStart'%sound_tranform, 0)
        mc.setAttr('%s.sourceEnd'%sound_tranform, duration)
        #mc.setAttr('%s.offset'%sound_tranform, start_time)

def set_sound(soundName, offset, duration):
    mc.setAttr('%s.sourceStart'%soundName, 0)
    mc.setAttr('%s.sourceEnd'%soundName, duration)
    mc.setAttr('%s.offset'%soundName, offset)
    mc.audioTrack(rt=True)

def start_end_sequencer(shots_dict):
    DEFAULT_VALUE = 1
    DEFAULT_START_DURATION_SHOT = 1000
    DEFAULT_SPAN = 100

    time_dict = dict()
    start_time = 0
    end_time = 0
    start_frame = 0
    end_frame = 0
    duration = 0

    for index ,shot in enumerate(sorted(shots_dict)):
        shotCode = shot['sg_shortcode']

        duration = shot['sg_working_duration']
        start_time = shot['sg_cut_in'] + DEFAULT_START_DURATION_SHOT if index == 0 else end_time + DEFAULT_VALUE
        end_time = start_time + duration - DEFAULT_VALUE

        start_frame = shot['sg_cut_in'] + DEFAULT_START_DURATION_SHOT if index == 0 else end_frame + DEFAULT_SPAN
        end_frame = start_frame + duration - DEFAULT_VALUE
            
        if shotCode not in time_dict.keys():
            time_dict[shotCode] = {'start_time':start_time, 'end_time':end_time, 'start_frame':start_frame, 'end_frame':end_frame}
        else:
            time_dict[shotCode]['start_time'] = start_time
            time_dict[shotCode]['end_time'] = end_time
            time_dict[shotCode]['start_frame'] = start_frame
            time_dict[shotCode]['end_frame'] = end_frame

    return time_dict

def clean_track(shots_dict):

    new_track = 0
    tracks = []

    #list track in camera sequencer
    for shotCode in shots_dict:
        track = mc.shot(shotCode, track=True, q=True)
        if track > 1:
            if track not in tracks:
                tracks.append(track)

    #set sequencer to new track
    if tracks:
        new_track = sorted(tracks)[-1] + 1
        tracks.append(new_track)

        for shotCode in sorted(shots_dict)[::-1]:
            mc.shot(shotCode, track=new_track, e=True)

    #set sequencer to track 1
    if tracks:
        for shotCode in shots_dict:
            mc.shot(shotCode, track=1, e=True)

    #remove track
    for num in sorted(tracks)[::-1]:
        mc.shotTrack(rt=num)