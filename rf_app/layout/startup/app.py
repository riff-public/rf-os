from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import sys
import os
import rf_config as config

from rf_app.layout.startup import utils
reload(utils)
from rf_utils.context import context_info 
from rf_utils.sg import sg_process
sg = sg_process.sg

modulePath = sys.modules[__name__].__file__.replace('\\', '/')
moduleDir  = os.path.dirname(modulePath)
uiName = 'Start Up'

class Config:
    bg_green  = 'font-size: 12px; background-color: green;'
    bg_gray = 'font-size: 12px; background-color: rgb(57,61,63); color: gray;'


class CreateSequencer(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(CreateSequencer, self).__init__(parent=parent)

        self.ui()
        self.setup_shots()
        self.signal()

    def ui(self):
        self.widget = QtWidgets.QWidget()
        self.main = QtWidgets.QVBoxLayout(self.widget)
        self.shot_layout = QtWidgets.QHBoxLayout()
        self.info_layout = QtWidgets.QVBoxLayout()

        self.shot_widget = QtWidgets.QListWidget()
        #self.shot_widget.setSpacing(0)
        self.shot_widget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.shot_layout.addWidget(self.shot_widget)

        self.create_button = QtWidgets.QPushButton("Create Sequencer")
        self.create_button.setMinimumHeight(35)
        self.main.addLayout(self.shot_layout)
        self.main.addWidget(self.create_button)

        self.setMinimumSize(370, 600)
        self.setObjectName(uiName)
        self.setWindowTitle(uiName)
        self.setCentralWidget(self.widget)

    def add_item(self, shot, number):
        name = shot['code']
        episode = shot['sg_episode']['name']
        sequence = shot['sg_sequence.Sequence.sg_shortcode']
        shotName = shot['sg_shortcode']
        status = shot['sg_status_list']
        duration = shot['sg_working_duration']

        color_bg1 = Config.bg_green if status != 'omt' else Config.bg_gray
        color_bg2 = ' ' if status != 'omt' else Config.bg_gray

        widget = QtWidgets.QWidget()
        layout = QtWidgets.QHBoxLayout(widget)

        num_text = QtWidgets.QLabel(' %s '%(number + 1))
        num_text.setMinimumWidth(20)
        num_text.setAlignment(QtCore.Qt.AlignCenter)
        num_text.setStyleSheet(color_bg1)
        shot_text = QtWidgets.QLabel(shotName)
        shot_text.setStyleSheet(color_bg1)
        shot_text.setAlignment(QtCore.Qt.AlignCenter)

        infolayout = QtWidgets.QVBoxLayout()
        name_text = QtWidgets.QLabel('%s'%(name))
        name_text.setStyleSheet(color_bg1)
        duration_text = QtWidgets.QLabel('duration: %s'%(duration))
        duration_text.setStyleSheet(color_bg2)
        status_text = QtWidgets.QLabel('status: %s'%(status))
        status_text.setStyleSheet(color_bg2)

        infolayout.addWidget(name_text)
        infolayout.addWidget(duration_text)
        infolayout.addWidget(status_text)
        layout.addWidget(num_text)
        layout.addWidget(shot_text)
        layout.addLayout(infolayout)

        layout.setStretch(0, 0)
        layout.setStretch(1, 1)
        layout.setStretch(2, 0)

        item = QtWidgets.QListWidgetItem(self.shot_widget)
        item.setSizeHint(widget.sizeHint())

        data = shot
        item.setData(QtCore.Qt.UserRole, data)
        self.shot_widget.setItemWidget(item, widget)

    def setup_shots(self):
        self.shots_dict = dict()
        shots_sg = self.list_sg_shot()

        for num, shot in  enumerate(shots_sg):
            self.add_item(shot, num)
            self.shots_dict[shot['sg_shortcode']] = shot

    def list_sg_shot(self):
        self.scene = context_info.ContextPathInfo()
        self.rigCamPath = self.scene.projectInfo.asset.global_asset('cam')
        self.context = context_info.Context()
        self.context.use_sg(sg, project=self.scene.project, entityType='scene', entityName=self.scene.name)
        self.scene = context_info.ContextPathInfo(context=self.context)
        self.sequence = self.scene.sequence

        projectEntity = sg_process.get_project(self.scene.project)
        episodeEntity = sg_process.get_one_episode(self.scene.project, self.scene.episode)

        extraFields = ['sg_cut_in', 'sg_cut_out', 'sg_cut_duration', 'sg_working_duration', 'sg_shortcode', 'sg_sequence_code', 'sg_status_list']
        orderOption = [{'field_name':'sg_shortcode', 'direction':'asc'}]
        shots = sg_process.get_shot_entities(projectEntity, episodeEntity, self.sequence, extraFields=extraFields, order= orderOption)

        return shots

    def signal(self):
        self.create_button.clicked.connect(self.create)

    def create(self):
        shots = list()
        shot_in_scene = utils.get_sequencer_in_scene()

        for item in self.shot_widget.selectedItems():
            shotData = item.data(QtCore.Qt.UserRole)
            shotName = shotData['sg_shortcode']
            shots.append(self.shots_dict[shotName])

        for cam in shot_in_scene:
            if self.shots_dict[cam] not in shots:
                shots.append(self.shots_dict[cam])

        time_dict = utils.start_end_sequencer(shots)

        for shotCode in sorted(time_dict)[::-1]:

            start_time = time_dict[shotCode]['start_time']
            end_time = time_dict[shotCode]['end_time']
            start_frame = time_dict[shotCode]['start_frame']
            end_frame = time_dict[shotCode]['end_frame']
            duration = self.shots_dict[shotCode]['sg_working_duration']
            shotName = self.shots_dict[shotCode]['code']
            soundName = '%s_sound'%(shotCode)

            if shotCode not in shot_in_scene:
                utils.create_camera_sequencer(self.scene, self.rigCamPath, shotCode, start_frame, end_frame, start_time, end_time)
            else:
                utils.set_sequencer(shotCode, start_frame, end_frame, start_time, end_time)

            sound_status = utils.get_sound_in_scene(soundName)
            if not sound_status:
                self.context.update(entity=shotName, step='edit', process='sound')
                utils.create_sound_sequencer(self.scene, shotCode, start_time, duration)
            else:
                utils.set_sound(soundName, start_time, duration)

        utils.clean_track(time_dict)


def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        maya_win.deleteUI(uiName)
        myApp = CreateSequencer(parent=maya_win.getMayaWindow())
        myApp.show()

if __name__ == '__main__':
    show()