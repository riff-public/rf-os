# -*- coding: utf-8 -*-
import sys
import os

core_path = '%s/core' % os.environ.get('RFSCRIPT')
if core_path not in sys.path:
    sys.path.append(core_path)

import rf_config as config
os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])

from Qt import QtWidgets, QtCore, QtGui


class ThumbListWidget(QtWidgets.QListWidget):

    _rows_to_del = []

    def __init__(self, type, parent=None):
        super(ThumbListWidget, self).__init__(parent)
        self.setIconSize(QtCore.QSize(124, 124))

        self.setDragDropMode(QtWidgets.QAbstractItemView.DragDrop)

        self.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.setAcceptDrops(True)
        self._dropping = False

        self.setSelectionRectVisible(True)

        # self.connect(self, QtCore.SIGNAL("dropped"), self.items_dropped)
        print "1111111111111"

    def dragEnterEvent(self, event):
        print "2222222222222"
        if event.mimeData().hasUrls():
            event.accept()
        else:
            super(ThumbListWidget, self).dragEnterEvent(event)

    def dragMoveEvent(self, event):
        print "333333333333"
        if event.mimeData().hasUrls():
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            super(ThumbListWidget, self).dragMoveEvent(event)

    def dropEvent(self, event):
        print "44444444444"
        if event.mimeData().hasUrls():
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            links = []
            for url in event.mimeData().urls():
                links.append(str(url.toLocalFile()))
            self.emit(QtCore.SIGNAL("dropped"), links)
        else:

            if event.source() is self:
                event.setDropAction(QtCore.Qt.MoveAction)
            else:

                event.setDropAction(QtCore.Qt.CopyAction)

            self._dropping = True
            super(ThumbListWidget, self).dropEvent(event)

            self._dropping = False

    def rowsInserted(self, parent, start, end):
        print "55555555555"
        if self._dropping:

            self.emit(QtCore.SIGNAL("dropped"), (start, end))
        super(ThumbListWidget, self).rowsInserted(parent, start, end)

    def dataChanged(self, start, end):
        print "6666666666"
        if self._dropping:
            for row in range(start.row(), end.row() + 1):
                index = self.indexFromItem(self.item(row))
                shot = index.data().toString()
                # print len(self.findItems(shot, Qt.MatchExactly))
                if len(self.findItems(shot, Qt.MatchExactly)) > 1:
                    self._rows_to_del.append(row)

            self._rows_to_del.reverse()

            for row in self._rows_to_del:
                self.takeItem(row)

            self._rows_to_del = []

    def items_dropped(self, arg):
        print "777777777"
        start, end = arg
        print range(start, end + 1)
        for row in range(start, end + 1):
            item = self.item(row)
            item.setFlags(item.flags() | Qt.ItemIsUserCheckable)
            item.setCheckState(Qt.Checked)

    def mousePressEvent(self, event):
        print "888888888"
        item = self.selectedCheckStateItem(event.pos())
        if item:
            new_state = Qt.Unchecked if item.checkState()==Qt.Checked else Qt.Checked
            QtWidgets.QApplication.processEvents()
            self.setSelectedCheckStates(new_state,item)
            QtWidgets.QApplication.processEvents()
            self.viewport().update()
        else:
            QtWidgets.QListWidget.mousePressEvent(self, event)

        self.mousePressPos = (event.pos())

    def setSelectedCheckStates(self, state, click_item):
        print "999999999"
        for item in self.selectedItems():
            if item is not click_item:
                item.setCheckState(state)

    def selectedCheckStateItem(self, pos):
        print "XXXXXXXXX"
        item = self.itemAt(pos)
        if item:
            opt = QtWidgets.QStyleOptionButton()
            opt.rect = self.visualItemRect(item)
            rect = self.style().subElementRect(QtWidgets.QStyle.SE_ViewItemCheckIndicator, opt)
            print "FGFFFFFFFFFFFFFFFFFFFFFFF", self.selectedItems()
            if rect.contains(pos):
                return item
        return 0

    def mouseMoveEvent(self, event):
        if (event.pos() - self.mousePressPos).manhattanLength() > QtWidgets.QApplication.startDragDistance():
            item = self.itemAt(event.pos())
            if item:
                rect = self.visualItemRect(item)
                # offset = 100
                char_width = QtGui.QFontMetricsF(QtGui.QFont(self.font())).width('a')
                offset = len(str(item.data(0))) * char_width
                if rect.translated(offset, 0).contains(event.pos()):
                    self.setState(QtWidgets.QAbstractItemView.DragSelectingState)

        QtWidgets.QListWidget.mouseMoveEvent(self, event)


class Dialog_01(QtWidgets.QMainWindow):

    def __init__(self):
        super(Dialog_01, self).__init__()
        self.listItems = {}

        myQWidget = QtWidgets.QWidget()
        myBoxLayout = QtWidgets.QVBoxLayout()
        myQWidget.setLayout(myBoxLayout)
        self.setCentralWidget(myQWidget)

        self.listWidgetA = ThumbListWidget(self)
        for i in range(12):
            QtWidgets.QListWidgetItem('Item ' + str(i), self.listWidgetA)

        # all_items = self.listWidgetA.findItems(QString('*'), Qt.MatchWrap | Qt.MatchWildcard)
        # for item in all_items:
        #     item.setFlags(item.flags() & ~Qt.ItemIsUserCheckable)

        myBoxLayout.addWidget(self.listWidgetA)

        # self.listWidgetB = ThumbListWidget(self)

        # self.listWidgetA.setAcceptDrops(False)

        # myBoxLayout.addWidget(self.listWidgetB)


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    dialog_1 = Dialog_01()
    dialog_1.show()
    dialog_1.resize(480, 320)
    sys.exit(app.exec_())
