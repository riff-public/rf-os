import sys
import os

core_path = '%s/core' % os.environ.get('RFSCRIPT')
if core_path not in sys.path:
    sys.path.append(core_path)

from rf_app.checkup.models.checkup import CheckUpModel

from Qt import QtWidgets, QtCore, QtGui


class PlaylistCustomWidget(QtWidgets.QListWidget):
    _rows_to_del = []

    def __init__(self, parent=None):
        super(PlaylistCustomWidget, self).__init__(parent)
        self.setIconSize(QtCore.QSize(124, 124))
        self.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self._dropping = False
        self.parent = parent
        # self.setSelectionRectVisible(True)
        self.cansort = False
        # dropped = QtCore.Signal(list)
        # self.dropped.connect(self.items_dropped)
        # self.connect(self, QtCore.SIGNAL("dropped"), self.items_dropped)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls():
            event.accept()
        else:
            super(PlaylistCustomWidget, self).dragEnterEvent(event)

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls():
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            super(PlaylistCustomWidget, self).dragMoveEvent(event)

    def dropEvent(self, event):
        if event.mimeData().hasUrls():
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            links = []
            for url in event.mimeData().urls():
                links.append(str(url.toLocalFile()))
            self.dropped.emit(links)
        else:
            if event.source() is self:
                event.setDropAction(QtCore.Qt.MoveAction)
            else:
                event.setDropAction(QtCore.Qt.CopyAction)
            self._dropping = True
            super(PlaylistCustomWidget, self).dropEvent(event)

            self._dropping = False
        all_items = self.findItems('*', QtCore.Qt.MatchWildcard)
        playlist_title = self.parent.get_playlist_title_selected()
        CheckUpModel().save_order_playlist(playlist_title, all_items)

    def rowsInserted(self, parent, start, end):
        if self._dropping:
            self.dropped.emit((start, end))
            # self.emit(QtCore.SIGNAL("dropped"), (start, end))
        # self.playlist_title = 
        super(PlaylistCustomWidget, self).rowsInserted(parent, start, end)

    def dataChanged(self, start, end):
        if self._dropping:
            for row in range(start.row(), end.row() + 1):
                index = self.indexFromItem(self.item(row))
                shot = index.data().toString()
                if len(self.findItems(shot, Qt.MatchExactly)) > 1:
                    self._rows_to_del.append(row)

            self._rows_to_del.reverse()

            for row in self._rows_to_del:
                self.takeItem(row)

            self._rows_to_del = []

    def items_dropped(self, arg):
        start, end = arg
        for row in range(start, end + 1):
            item = self.item(row)
            item.setFlags(item.flags() | Qt.ItemIsUserCheckable)
            item.setCheckState(Qt.Checked)

    def mousePressEvent(self, event):
        item = self.selectedCheckStateItem(event.pos())
        if event.button() == QtCore.Qt.MouseButton.MiddleButton:
            self.setDragDropMode(QtWidgets.QAbstractItemView.DragDrop)
            self.setAcceptDrops(True)
            self.cansort = True
            if item:
                new_state = Qt.Unchecked if item.checkState() == Qt.Checked else Qt.Checked
                QtCore.QCoreApplication.processEvents()
                self.setSelectedCheckStates(new_state, item)
                QtCore.QCoreApplication.processEvents()
                self.viewport().update()
            else:
                QtWidgets.QListWidget.mousePressEvent(self, event)

            self.mousePressPos = (event.pos())
        elif event.button() == QtCore.Qt.MouseButton.LeftButton:
            self.cansort = False
            self.setDragDropMode(QtWidgets.QAbstractItemView.NoDragDrop)
            self.setAcceptDrops(False)
            if item:
                new_state = Qt.Unchecked if item.checkState() == Qt.Checked else Qt.Checked
                QtCore.QCoreApplication.processEvents()
                self.setSelectedCheckStates(new_state, item)
                QtCore.QCoreApplication.processEvents()
            else:
                QtWidgets.QListWidget.mousePressEvent(self, event)
            self.mousePressPos = (event.pos())
        elif event.button() == QtCore.Qt.MouseButton.RightButton:
            self.cansort = False
            self.setDragDropMode(QtWidgets.QAbstractItemView.NoDragDrop)
            self.setAcceptDrops(False)
            if item:
                new_state = Qt.Unchecked if item.checkState() == Qt.Checked else Qt.Checked
                QtCore.QCoreApplication.processEvents()
                self.setSelectedCheckStates(new_state, item)
                QtCore.QCoreApplication.processEvents()
            else:
                QtWidgets.QListWidget.mousePressEvent(self, event)
            self.mousePressPos = (event.pos())

    def setSelectedCheckStates(self, state, click_item):
        for item in self.selectedItems():
            if item is not click_item:
                item.setCheckState(state)

    def selectedCheckStateItem(self, pos):
        item = self.itemAt(pos)
        if item:
            opt = QtWidgets.QStyleOptionButton()
            opt.rect = self.visualItemRect(item)
            rect = self.style().subElementRect(QtWidgets.QStyle.SE_ViewItemCheckIndicator, opt)
            if rect.contains(pos):
                return item
        return 0

    def mouseMoveEvent(self, event):
        if self.cansort:
            if (event.pos() - self.mousePressPos).manhattanLength() > QtWidgets.QApplication.startDragDistance():
                item = self.itemAt(event.pos())
                if item:
                    rect = self.visualItemRect(item)
                    # offset = 100
                    char_width = QtGui.QFontMetricsF(QtGui.QFont(self.font())).width('a')
                    offset = len(str(item.data(0))) * char_width
                    # if rect.translated(offset, 0).contains(event.pos()):
                    #     self.setState(QtWidgets.QAbstractItemView.DragSelectingState)

        QtWidgets.QListWidget.mouseMoveEvent(self, event)