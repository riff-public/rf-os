import os
import time
import re
import tempfile
from Qt import QtCore
# from publish_directory_model import PublishDirectoryModel
from rv_player import RVPlayer

from rf_app.checkup.utils import checkup_file
from rf_utils.context import context_info
from rf_utils.sg.sg_utils import (
    sg,
    get_all_projects,
    get_episodes_by_project,
    get_char
)
import rf_config as config
from rf_utils.pipeline import user_pref, commands
from rf_utils.file_utils import back_slash_to_forwardslash


class CheckUpModel:
    def __init__(self):
        self.sg = sg
        self.projects = []
        self.project_selected = None
        self.step_selected = ""
        self.episodes = []
        self.assetType = []
        self.steps = ["anim", "animatic", "finalcam", "fx", "layout", "light", "setdress", "sim", "techanim", "comp", "crowd"]
        self.order_options = [{
            'display': 'Date Modified',
            'value': 'date_modified'
        }, {
            'display': 'Date Created',
            'value': 'date_created'
        }, {
            'display': 'File Name',
            'value': 'filename'
        }]
        self.init_state()
        self.seq_dirs = []
        self.data_mapped = {}
        self.publ_data_mapped = {}
        # self.daily_data_mapped = {}
        self.mov_files_display = []
        # self.daily_mov_files_display = []
        self.versions = []
        self.user_pref = user_pref.ToolSetting('check_up')
        self.user_pref_data = self.user_pref.read()
        self.playlist_pref = user_pref.ToolSetting('check_up_playlist')
        self.shots_from_sg = []

    def init_state(self):
        self.set_projects()
        # self.set_episodes()

    def get_projects(self):
        return self.projects

    def get_entityTypes(self):
        return ['Scene', 'Asset']

    def get_project_name_by_code(self, project_code):
        projects = self.get_projects()
        for project in projects:
            if project['sg_project_code'] == project_code:
                return project
        return projects[0]

    def get_episodes(self):
        return self.episodes

    def get_asset_type(self):
        return self.assetType

    def get_steps(self):
        return self.steps

    def get_seq_dirs(self):
        return self.seq_dirs

    def get_data_mapped(self):
        return self.data_mapped

    def get_playlist_data(self):
        return self.playlist_pref.read()

    def get_all_mov_files_V2(self, path, step):
        shot_dirs = list(filter(lambda shot_dir: shot_dir.split("_")[-1] != "all" and len(shot_dir.split("_")) == 4, os.listdir(path)))
        mov_files = []
        for shot_dir in shot_dirs:
            shot_path = "{path}/{shot_dir}/{step}".format(path=path, shot_dir=shot_dir, step=step)
            if os.path.exists(shot_path):
                processes = os.listdir(shot_path)
                for process in processes:
                    if process != "qc":
                        output_work_files = "{shot_path}/{process}/output".format(shot_path=shot_path, process=process)
                        if os.path.exists(output_work_files):
                            list_file = os.listdir(output_work_files)
                            for file in list_file:
                                if len(file.split("_")) == 6:
                                    filename, file_extension = os.path.splitext(file)
                                    if file_extension == ".mov":
                                        mov_files.append(file)
        return mov_files

    def get_all_mov_asset(self, path, step):
        #asset_dirs = list(filter(lambda shot_dir: shot_dir.split("_")[-1] == "Oa", os.listdir(path)))
        step = 'anim'
        asset_dirs = os.listdir(path)
        mov_files = []
        for asset_dir in asset_dirs:
            shot_path = "{path}/{asset_dir}/{step}".format(path=path, asset_dir=asset_dir, step=step)
            if os.path.exists(shot_path):
                processes = os.listdir(shot_path)
                for process in processes:
                    output_work_files = "{shot_path}/{process}/output".format(shot_path=shot_path, process=process) 
                    # P:\SevenChickMovie\asset\work\char\deerBuckDefaultOa\anim\main\output
                    if os.path.exists(output_work_files):
                        list_file = os.listdir(output_work_files)
                        for file in list_file:
                            if len(file.split("_")) == 4:
                                filename, file_extension = os.path.splitext(file)
                                if file_extension == ".mov":
                                    mov_files.append(file)
        return mov_files

    # def get_all_mov_files(self, path, step):
    #     asset_dirs = list(filter(lambda asset_dir: asset_dir.split("_")[-1] != "all" and len(asset_dir.split("_")) == 4, os.listdir(path)))
    #     mov_files = []
    #     for asset_dir in asset_dirs:
    #         shot_path = "{path}/{asset_dir}/{step}".format(path=path, asset_dir=asset_dir, step=step)
    #         for dirName, subdirList, fileList in scandir.walk(shot_path, topdown=False):
    #             if dirName.split("\\")[-1] == "output":
    #                 for fname in fileList:
    #                     filename, file_extension = os.path.splitext(fname)
    #                     if file_extension == ".mov":
    #                         if self.validate_file(fname, step):
    #                             mov_files.append(fname)
    #     return mov_files

    def get_all_animatics_files(self, path):
        mov_files = []
        for shot_path in os.listdir(self.base_path):
            if self.is_valid_shot_folder(shot_path):
                edit_animatic_path = "{base_path}/{shot_path}/edit/animatic/hero/outputMedia".format(
                    base_path=self.base_path,
                    shot_path=shot_path
                )
                if os.path.exists(edit_animatic_path):
                    edit_animatics = os.listdir(edit_animatic_path)
                    for edit_animatic_file in edit_animatics:
                        if len(edit_animatic_file.split("_")) == 6:
                            filename, file_extension = os.path.splitext(edit_animatic_file)
                            if file_extension == ".mov":
                                mov_files.append(edit_animatic_file)

        return mov_files

    def get_mov_files_display(self):
        return self.mov_files_display

    # def get_daily_mov_files_display(self):
    #     return self.daily_mov_files_display

    def get_versions_files(self):
        return self.versions

    def get_all_mov_shot_step(self, shot, department_selected):
        mov_files = []
        shot_path = self.get_base_path_shot(shot, department_selected)
        if os.path.exists(shot_path):
            processes = os.listdir(shot_path)
            for process in processes:
                if process != "qc":
                    output_work_files = "{shot_path}/{process}/output".format(shot_path=shot_path, process=process)
                    if os.path.exists(output_work_files):
                        list_file = os.listdir(output_work_files)
                        for file in list_file:
                            if len(file.split("_")) == 6:
                                filename, file_extension = os.path.splitext(file)
                                if file_extension == ".mov":
                                    file_data = {
                                        "filename": file,
                                        "date_modified": os.path.getmtime(os.path.join(output_work_files, file)),
                                        "date_created": os.path.getctime(os.path.join(output_work_files, file)),
                                        "dir_path": output_work_files,
                                        "file_path": os.path.join(output_work_files, file)
                                    }
                                    mov_files.append(file_data)
        return mov_files

    def get_base_path_shot(self, shot_data, department_selected):
        step = department_selected.split(":")[1].strip()
        shot_name = "_".join(shot_data['filename'].split("_")[0:4])
        project_entity = self.get_project_name_by_code(shot_data['project_name'])
        base_project_path = "P:/{project_name}/scene/work/{ep}/{shot_name}/{step}".format(
            project_name=project_entity['name'],
            ep=shot_data['episode'],
            shot_name=shot_name,
            step=step
        )
        return base_project_path

    def set_projects(self):
        self.projects = get_all_projects()

    def set_project_selected(self, project_selected):
        self.project_selected = project_selected

    def set_episodes(self):
        self.episodes = get_episodes_by_project(self.project_selected)

    def set_asset_type(self):
        self.assetType = get_char()

    def process_check_up_data(self, project, episode, step, order_by_selected):
        self.data_mapped = {}
        # self.daily_data_mapped = {}
        self.step_selected = step
        context = context_info.Context()
        context.update(
            project=project['name'],
            entityType='scene',
            entityGrp=episode['code']
        )
        asset_context = context_info.ContextPathInfo(context=context)
        if step == "animatic":
            self.base_path = '/'.join(asset_context.path.name(workspace=context_info.ContextKey.publish).abs_path().split("/")[:-1])
            animatics_files = self.get_all_animatics_files(self.base_path)
            self.tranform_file_to_data(animatics_files)
        else:
            self.base_path = '/'.join(asset_context.path.name().abs_path().split("/")[:-1])
            start = time.time()
            mov_files = self.get_all_mov_files_V2(self.base_path, step)
            end = time.time()
            print("walk folder time: ", end - start)
            self.tranform_file_to_data(mov_files)

        self.user_pref.write({
            "default_project": project['name'],
            "default_episode": episode['code'],
            "default_step": step,
            "default_order": order_by_selected['display']
        })

    def process_check_up_data_asset(self, project, assetType, step, order_by_selected):
        # use arg : step , project , 
        self.data_mapped = {}
        # self.daily_data_mapped = {}
        self.step_selected = step
        context = context_info.Context()
        context.update(
            project=project['name'],
            entityType='asset',
            entityGrp=assetType
        )
        asset_context = context_info.ContextPathInfo(context=context)
        # if step == "animatic":
        #     self.base_path = '/'.join(asset_context.path.name(workspace=context_info.ContextKey.publish).abs_path().split("/")[:-1])
        #     animatics_files = self.get_all_animatics_files(self.base_path)
        #     self.tranform_file_to_data(animatics_files)
        # else:
        self.base_path = '/'.join(asset_context.path.name().abs_path().split("/")[:-1])
        start = time.time()
        mov_files = self.get_all_mov_asset(self.base_path, step)
        end = time.time()
        print("walk folder time: ", end - start)
        self.tranform_file_to_data_asset(mov_files)

        self.user_pref.write({
            "default_project": project['name'],
            "default_episode": assetType,
            "default_step": step,
            "default_order": order_by_selected['display']
        })

    def set_files_mov_filter(self, shots_selected, isLatest, order_by_selected, publish_dir_model):
        self.mov_files_display = []
        publ_data = publish_dir_model.get_publ_data_mapped()
        if isLatest:
            for seq_shot in shots_selected:
                if seq_shot['workspace_type'] == "wip":
                    mov_files_selected = sorted(self.data_mapped[seq_shot['seq_name']][seq_shot['shot_name']], key=lambda mov_file: mov_file[order_by_selected['value']])
                    data = mov_files_selected[-1]
                elif seq_shot['workspace_type'] == "publ":
                    mov_files_selected = sorted(publ_data[seq_shot['seq_name']][seq_shot['shot_name']], key=lambda mov_file: mov_file[order_by_selected['value']])
                    data = mov_files_selected[-1]

                self.mov_files_display.append(data)
        else:
            for seq_shot in shots_selected:
                if seq_shot['workspace_type'] == "wip":
                    for data in sorted(self.data_mapped[seq_shot['seq_name']][seq_shot['shot_name']], key=lambda mov_file: mov_file[order_by_selected['value']]):
                        self.mov_files_display.append(data)
                elif seq_shot['workspace_type'] == "publ":
                    for data in sorted(publ_data[seq_shot['seq_name']][seq_shot['shot_name']], key=lambda mov_file: mov_file[order_by_selected['value']]):
                        self.mov_files_display.append(data)

    # def set_daily_files_mov_filter(self, shots_selected, isLatest, order_by_selected):
    #     self.daily_mov_files_display = []

    #     if order_by_selected['value'] == "date_modified":
    #         order_by = "created_at"
    #     elif order_by_selected['value'] == "filename":
    #         order_by = "code"

    #     if isLatest:
    #         for seq_shot in shots_selected:
    #             mov_files_selected = sorted(self.daily_data_mapped[seq_shot['seq_name']][seq_shot['shot_name']], key=lambda mov_file: mov_file[order_by])
    #             data = mov_files_selected[-1]
    #             self.daily_mov_files_display.append(data)
    #     else:
    #         for seq_shot in shots_selected:
    #             for data in sorted(self.daily_data_mapped[seq_shot['seq_name']][seq_shot['shot_name']], key=lambda mov_file: mov_file[order_by]):
    #                 self.daily_mov_files_display.append(data)

    # def check_step_filter_dir(self, subdirList):
    #     subdirList = dir_name.split("\\")
    #     if len(split_dir) == 3:
    #         if split_dir[2] == self.step_selected:
    #             return True
    #     return False

    # def validate_file(self, mov_file, step):
    #     if step == mov_file.split("_")[4] and mov_file.split("_")[3] != "all":
    #         data = self.get_file_struct(mov_file)
    #         if os.path.exists(data['file_path']):
    #             return True
    #     return False

    # def filter_sequence(self, path):
    #     filtered_seq = []
    #     for directory_name in os.listdir(path):
    #         seq_name = directory_name.split("_")[2]
    #         if seq_name[0] == "q" and seq_name not in filtered_seq:
    #             filtered_seq.append(seq_name)
    #     return filtered_seq

    # def load_daily(self, project_entity, department):
    #     filtered_versions = [
    #         ["project", "is", project_entity],
    #         ["sg_version_type", "is", "Daily"],
    #         ["sg_department", "is", department]
    #     ]
    #     fields_versions = [
    #         'id',
    #         'code',
    #         'project',
    #         'sg_department',
    #         'playlists',
    #         'entity',
    #         'created_at'
    #     ]
    #     self.versions = self.sg.find('Version', filtered_versions, fields_versions)
    #     self.tranform_daily_file_to_data(self.versions)

    def fetch_all_shots(self, project_entity, episode, department):
        # print "episode : ", episode
        filtered_shots = [
            ["project", "is", project_entity],
            ["sg_episode", "is", episode],
            ["sg_shot_type", "is", "Shot"]
        ]
        fields_shots = [
            'code',
            'sg_status_list',
            'sg_sequence_code',
            'sg_shortcode'
        ]
        self.shots_from_sg = self.sg.find('Shot', filtered_shots, fields_shots)
        # self.tranform_daily_file_to_data(self.versions)

    def tranform_file_to_data(self, mov_files):
        for mov_file in mov_files:
            file_struct = self.get_file_struct(mov_file)
            if os.path.exists(file_struct['file_path']): 
                date_modify = os.path.getmtime(file_struct['file_path'])
                date_created = os.path.getctime(file_struct['file_path'])
                data = {
                    "project_name": file_struct['project_code'],
                    "episode": file_struct['episode'],
                    "sequence": file_struct['seq'],
                    "shot": file_struct['shot'],
                    "step": file_struct['step'],
                    "filename": mov_file,
                    "file_path": file_struct['file_path'],
                    "dir_path": file_struct['dir_path'],
                    "workspace_type": file_struct['workspace_type'],
                    "date_modified": date_modify,
                    "date_created": date_created,
                    "group": "{seq}_{shot}".format(seq=file_struct['seq'], shot=file_struct['shot'])
                }

                if file_struct['seq'] not in self.data_mapped:
                    self.data_mapped.update({file_struct['seq']: {}})

                if file_struct['seq'] in self.data_mapped:
                    if file_struct['shot'] not in self.data_mapped[file_struct['seq']]:
                        self.data_mapped[file_struct['seq']].update({file_struct['shot']: [data]})
                    elif file_struct['shot'] in self.data_mapped[file_struct['seq']]:
                        self.data_mapped[file_struct['seq']][file_struct['shot']].append(data)
            else: 
                print('File name not match with shotName. Skipped "%s"' % file_struct['file_path'])

    def tranform_file_to_data_asset(self, mov_files):
        for mov_file in mov_files:
            file_struct = self.get_file_struct_asset(mov_file)
            date_modify = os.path.getmtime(file_struct['file_path'])
            date_created = os.path.getctime(file_struct['file_path'])
            data = {
                "sequence": file_struct['seq'],
                "shot": file_struct['shot'],
                "step": file_struct['step'],
                "filename": mov_file,
                "file_path": file_struct['file_path'],
                "dir_path": file_struct['dir_path'],
                "workspace_type": file_struct['workspace_type'],
                "date_modified": date_modify,
                "date_created": date_created,
                "group": "{seq}_{shot}".format(seq=file_struct['seq'], shot=file_struct['shot'])
            }

            if file_struct['seq'] not in self.data_mapped:
                self.data_mapped.update({file_struct['seq']: {}})

            if file_struct['seq'] in self.data_mapped:
                if file_struct['shot'] not in self.data_mapped[file_struct['seq']]:
                    self.data_mapped[file_struct['seq']].update({file_struct['shot']: [data]})
                elif file_struct['shot'] in self.data_mapped[file_struct['seq']]:
                    self.data_mapped[file_struct['seq']][file_struct['shot']].append(data)

    def process_play_rv(self, files_selected):
        # RVPlayer().play(files_selected) # play in local
        # for file in os.listdir("C:/Program Files/Shotgun"):
        #     if "RV-" in file:
        #         rv_version = file
        # command = ["C:/Program Files/Shotgun/{rv_version}/bin/rv.exe".format(rv_version=rv_version)]
        app_path = config.Software.rv
        if not os.path.exists(app_path):
            print 'Cannot find application: {}'.foramt(app_path)
            return
        command = [app_path]
        for file_item in files_selected:
            if os.path.exists(file_item['dir_path']): # change os.path.exists(file_item['file_path']). Because filepath version change when publish new version.
                if file_item['step'] == "comp":
                    src_review_file_path, review_file = self.get_review_file_comp(file_item)
                    command.append(src_review_file_path)
                else:
                    command.append(file_item['file_path'])
                
        thread_subprocess = commands.ThreadSubprocess(command)
        thread_subprocess.start()

    def process_play_keyframepro(self, files_selected):
        command = ['python', 
            '{script_root}/core/rf_utils/keyframepro_utils.py'.format(script_root=os.environ['RFSCRIPT']), 
            '-i']
        file_paths = []
        for file_item in files_selected:
            if os.path.exists(file_item['dir_path']): # change os.path.exists(file_item['file_path']). Because filepath version change when publish new version.
                if file_item['step'] == "comp":
                    src_review_file_path, review_file = self.get_review_file_comp(file_item)
                    file_paths.append(src_review_file_path)
                else:
                    file_paths.append(file_item['file_path'])
        fp, tmp = tempfile.mkstemp(suffix='.txt')
        tmp = tmp.replace('\\', '/')
        os.close(fp)
        with open(tmp, 'w') as tf:
            tf.writelines('\n'.join(file_paths))

        command.append(tmp)
                
        thread_subprocess = commands.ThreadSubprocess(command)
        thread_subprocess.start()

        # os.remove(tmp)

    # def remove_tmp_rv_files(self):
    #     RVPlayer().clear_cache_tmp_file()

    def get_review_file_comp(self, file_item):
        files = os.listdir(file_item['dir_path'])
        for file in files:
            if 'review' in file:
                review_file = file

        src_review_file_path = os.path.join(file_item['dir_path'], review_file)
        return src_review_file_path, review_file

    def get_file_struct(self, mov_fle_name):
        project_code, episode, seq, shot, step, process = mov_fle_name.split("_")
        step = self.defend_name_step_possible_error(step)
        seq_shot = "{project_code}_{episode}_{seq}_{shot}".format(
            project_code=project_code,
            episode=episode,
            seq=seq,
            shot=shot
        )
        if step == "edit":
            file_path = "{base_path}/{seq_shot}/edit/animatic/hero/outputMedia/{mov_file}".format(
                base_path=self.base_path,
                seq_shot=seq_shot,
                mov_file=mov_fle_name
            )
            dir_path = "{base_path}/{seq_shot}/edit/animatic/hero/outputMedia".format(
                base_path=self.base_path,
                seq_shot=seq_shot
            )
        else:
            file_path = "{base_path}/{seq_shot}/{step}/{process}/output/{mov_file}".format(
                base_path=self.base_path,
                seq_shot=seq_shot,
                step=step,
                process=process.split(".")[0],
                mov_file=mov_fle_name
            )
            dir_path = "{base_path}/{seq_shot}/{step}/{process}/output".format(
                base_path=self.base_path,
                seq_shot=seq_shot,
                step=step,
                process=process.split(".")[0]
            )

        return {
            'project_code': project_code,
            'episode': episode,
            'seq': seq,
            'shot': shot,
            'step': step,
            'file_path': file_path,
            'dir_path': dir_path,
            'workspace_type': "wip"
        }

    def get_file_struct_asset(self, mov_fle_name):
        seq, step, shot, task = mov_fle_name.split("_")
        step = self.defend_name_step_possible_error(step)
        # seq_shot = "{project_code}_{episode}_{seq}_{shot}".format(
        #     project_code=project_code,
        #     episode=episode,
        #     seq=seq,
        #     shot=shot
        # )
        file_path = "{base_path}/{seq}/{step}/{shot}/output/{mov_file}".format(
            base_path=self.base_path,
            seq=seq,
            step=step,
            shot=shot.split(".")[0],
            mov_file=mov_fle_name
        )
        dir_path = "{base_path}/{seq}/{step}/{shot}/output".format(
            base_path=self.base_path,
            seq=seq,
            step=step,
            shot=shot.split(".")[0]
        )

        return {
            'seq': seq,
            'shot': shot,
            'step': step,
            'file_path': file_path,
            'dir_path': dir_path,
            'workspace_type': "wip"
        }

    # def tranform_daily_file_to_data(self, version_files):
    #     for version_file in version_files:
    #         project, episode, seq_verison, shot_version, step, process, version = version_file['code'].split("_")
    #         if seq_verison not in self.daily_data_mapped:
    #             self.daily_data_mapped.update({seq_verison: {}})

    #         if seq_verison in self.daily_data_mapped:
    #             if shot_version not in self.daily_data_mapped[seq_verison]:
    #                 self.daily_data_mapped[seq_verison].update({shot_version: [version_file]})
    #             elif shot_version in self.daily_data_mapped[seq_verison]:
    #                 self.daily_data_mapped[seq_verison][shot_version].append(version_file)

    def is_valid_shot_folder(self, shot_path):
        shot_path_split = shot_path.split("_")
        if len(shot_path_split) == 4 and shot_path_split[-1] != "all":
            return True
        else:
            return False

    def save_playlist(self, playlist_title, file_selected):
        data = {
            playlist_title: file_selected
        }
        new_data = self.get_playlist_data()
        new_data.update(data)
        self.playlist_pref.write(new_data)

    def update_playlist(self, playlist_title, file_selected):
        data = self.get_playlist_data()
        old_playlist = data.get(playlist_title)
        if old_playlist:
            new_playlist = old_playlist + file_selected
            data[playlist_title] = new_playlist
            self.playlist_pref.write(data)

    def remove_playlist_title(self, playlist_title):
        new_data = self.get_playlist_data()
        for title in playlist_title:
            new_data.pop(title, None)
        self.playlist_pref.write(new_data)

    def remove_playlist_selected(self, playlist_title, playlist_selected):
        new_data = self.get_playlist_data()
        updated_playlist_data = self.remove_shot(playlist_selected, new_data[playlist_title[0]])
        updated_data = {
            playlist_title[0]: updated_playlist_data
        }
        new_data.update(updated_data)
        self.playlist_pref.write(new_data)

    def remove_shot(self, playlist_selected, playlist_data):
        updated_data = []
        for playlist in playlist_data:
            reject = False
            for playlist_remove in playlist_selected:
                if playlist['filename'] == playlist_remove['filename']:
                    reject = True
                    break
            if not reject:
                updated_data.append(playlist)
        return updated_data

    def fetch_versions_shot(self, playlist_selected):
        versions_shot = []
        if len(playlist_selected) != 0:
            shot_versions_files = checkup_file.get_all_files_from_folder(playlist_selected[0]['dir_path'])
            for file in shot_versions_files:
                if len(file.split(".")) == 3:
                    versions_shot.append(file.split(".")[1])
        return versions_shot

    def save_order_playlist(self, playlist_title, all_items):
        playlist_data = self.get_playlist_data()
        playlist_data[playlist_title[0]] = []
        for item in all_items:
            playlist_data[playlist_title[0]].append(item.data(QtCore.Qt.UserRole))
        self.playlist_pref.write(playlist_data)

    def switch_version_shot_playlist(self, playlist_title, playlist_selected, version_switch):
        if len(playlist_selected) != 0:
            playlist_data = self.get_playlist_data()
            for shot in playlist_data[playlist_title[0]]:
                if shot['filename'] == playlist_selected[0]['filename']:
                    new_file_version = self.replace_new_version(
                        playlist_selected[0]['filename'],
                        version_switch
                    )
                    shot['filename'] = new_file_version
                    current_file_path = back_slash_to_forwardslash(shot['file_path']).split("/")
                    current_file_path[-1] = new_file_version
                    new_file_path = "/".join(current_file_path)
                    shot['file_path'] = new_file_path
                    date_modify = os.path.getmtime(shot['file_path'])
                    shot['date_modified'] = date_modify
                    date_created = os.path.getctime(shot['file_path'])
                    shot['date_created'] = date_created

        self.playlist_pref.write(playlist_data)

    def switch_version_shared_playlist(self, playlist_title, playlist_selected, version_switch, playlist_data):
        if len(playlist_selected) != 0:
            for idx, data in enumerate(playlist_data):
                if data['title'] == playlist_title['title']:
                    for shot in data['playlist']:
                        if shot['filename'] == playlist_selected[0]['filename']:
                            new_file_version = self.replace_new_version(
                                playlist_selected[0]['filename'],
                                version_switch
                            )
                            shot['filename'] = new_file_version
                            current_file_path = back_slash_to_forwardslash(shot['file_path']).split("/")
                            current_file_path[-1] = new_file_version
                            new_file_path = "/".join(current_file_path)
                            shot['file_path'] = new_file_path
                            date_modify = os.path.getmtime(shot['file_path'])
                            shot['date_modified'] = date_modify
                            date_created = os.path.getctime(shot['file_path'])
                            shot['date_created'] = date_created
                            break
                    return data['playlist']
    
    def switch_step_shot_playlist(self, playlist_title, playlist_selected, action_text):
        if len(playlist_selected) != 0:
            self.set_shot_step(playlist_selected, playlist_title, action_text)

    def replace_new_version(self, current_filename, new_version):
        filename, version, ext = current_filename.split(".")
        new_file_name = "{filename}.{new_version}.{ext}".format(
            filename=filename,
            new_version=new_version,
            ext=ext
        )
        return new_file_name

    def add_shot_to_playlist(self, title, file_selected, playlist_data):
        for shot_file in file_selected:
            playlist_data[title].append(shot_file)
        self.save_playlist(title, playlist_data[title])

    def defend_name_step_possible_error(self, step):
        STEPS = {
            'ani': 'anim'
        }

        if step in STEPS:
            return STEPS[step]
        return step

    def is_omit_shot(self, seq_name, shot_name):
        for shot in self.shots_from_sg:
            if shot['sg_sequence_code'] == seq_name and shot["sg_shortcode"] == shot_name and shot['sg_status_list'] == "omt":
                return True
        return False

    def check_special_text(self, msg):
        regex = re.compile('[@!#$%^&*()<>?/\|}{~:]')
        if regex.search(msg) is None:
            return False
        else:
            return True

    def set_playlist_lastest_version(self, playlist_title, playlist_selected):
        for shot_playlist_selected in playlist_selected:
            shot_versions_files = checkup_file.get_all_files_from_folder(shot_playlist_selected['dir_path'])
            playlist_data = self.get_playlist_data()
            for shot_playlist_data in playlist_data[playlist_title[0]]:
                if shot_playlist_data['filename'] == shot_playlist_selected['filename']:
                    shot_playlist_data['filename'] = shot_versions_files[-1]
                    current_file_path = back_slash_to_forwardslash(shot_playlist_data['file_path']).split("/")
                    current_file_path[-1] = shot_playlist_data['filename']
                    new_file_path = "/".join(current_file_path)
                    shot_playlist_data['file_path'] = new_file_path
                    date_modify = os.path.getmtime(shot_playlist_data['file_path'])
                    shot_playlist_data['date_modified'] = date_modify
                    date_created = os.path.getctime(shot_playlist_data['file_path'])
                    shot_playlist_data['date_created'] = date_created
            self.playlist_pref.write(playlist_data)

    def set_shared_playlist_lastest_version(self, playlist_title, playlist_selected, playlist_data):
        if len(playlist_selected) == 0:
            return []

        index = None
        for idx, data in enumerate(playlist_data):
            if data['title'] == playlist_title['title']:
                index = idx
                break
        else:
            return []

        title_playlist_data = playlist_data[index]
        for shot_playlist_selected in playlist_selected:
            shot_versions_files = checkup_file.get_all_files_from_folder(shot_playlist_selected['dir_path'])
            for shot in title_playlist_data['playlist']:
                if shot['filename'] == shot_playlist_selected['filename']:
                    shot['filename'] = shot_versions_files[-1]
                    current_file_path = back_slash_to_forwardslash(shot['file_path']).split("/")
                    current_file_path[-1] = shot['filename']
                    new_file_path = "/".join(current_file_path)
                    shot['file_path'] = new_file_path
                    date_modify = os.path.getmtime(shot['file_path'])
                    shot['date_modified'] = date_modify
                    date_created = os.path.getctime(shot['file_path'])
                    shot['date_created'] = date_created
                    break
        return title_playlist_data['playlist']

    def set_shot_step(self, playlist_selected, playlist_title, department_selected):
        updated_palylist_mov_files = []
        for shot in playlist_selected:
            mov_files = self.get_all_mov_shot_step(shot, department_selected)
            if len(mov_files) > 0:
                mov_files.sort(key=lambda file: file['date_modified'])
                mov_files[-1]['sequence'] = shot['sequence']
                mov_files[-1]['shot'] = shot['shot']
                updated_palylist_mov_files.append(mov_files[-1])

        playlist_data = self.get_playlist_data()
        for shot_playlist_data in playlist_data[playlist_title[0]]:
            for updated_shot in updated_palylist_mov_files:
                if shot_playlist_data['shot'] == updated_shot['shot'] and shot_playlist_data['sequence'] == updated_shot['sequence']:
                    shot_playlist_data['filename'] = updated_shot['filename']
                    shot_playlist_data['file_path'] = updated_shot['file_path']
                    shot_playlist_data['dir_path'] = updated_shot['dir_path']
                    shot_playlist_data['date_modified'] = updated_shot['date_modified']
                    shot_playlist_data['date_created'] = updated_shot['date_created']

        self.playlist_pref.write(playlist_data)
