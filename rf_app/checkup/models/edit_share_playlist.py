import os
import json
import shutil
import subprocess
from rf_utils import custom_exception
from rf_utils import file_utils


CHECKUP_EDIT_SHARE_PLAYLIST_PATH = "O:/Pipeline/-TEMP-/checkup"


class EditSharePlaylistModel:
    def __init__(self):
        self.edit_share_playlists = {}
        self.file_path = "{file_path}/edit_share_playlist.json".format(file_path=CHECKUP_EDIT_SHARE_PLAYLIST_PATH)

    def init_state(self):
        pass

    def save_edit_share_playlist(self, title, playlist_data, step, status="Hold"):
        data = {
            "title": title,
            "playlist": playlist_data,
            "status": status,
            "step": step
        }
        if not os.path.exists(CHECKUP_EDIT_SHARE_PLAYLIST_PATH):
            os.makedirs(CHECKUP_EDIT_SHARE_PLAYLIST_PATH)
            open(self.file_path, 'w').close()

        if not os.path.exists(self.file_path):
            open(self.file_path, 'w').close()

        if os.stat(self.file_path).st_size == 0:
            with open(self.file_path, 'w+') as outfile:
                json.dump([data], outfile, indent=4)
        else:
            with open(self.file_path, 'r+') as outfile:
                data_loaded = json.load(outfile)
                data_loaded.append(data)
                outfile.seek(0)
                json.dump(data_loaded, outfile, indent=4)
                outfile.truncate()

    def get_edit_share_playlist(self):
        with open(self.file_path, 'r') as outfile:
            data_loaded = json.load(outfile)
        return data_loaded

    def write_edit_share_playlist(self, data):
        with open(self.file_path, 'w') as outfile:
            json.dump(data, outfile, indent=4)

    def remove_edit_share_playlist_title(self, edit_share_playlist_title):
        new_data = self.get_edit_share_playlist()
        # print "new_data : ", new_data
        for idx, data in enumerate(new_data):
            if data['title'] == edit_share_playlist_title[0]['title']:
                new_data.pop(idx)
                break
        self.write_edit_share_playlist(new_data)
        # print "new_data : ", new_data
        # new_data.pop(edit_share_playlist_title[0], None)

    def copy_to_local(self, playlist_title, file_selected, dst_path):
        sources_edit_path = os.path.join(dst_path, playlist_title)
        if not os.path.exists(sources_edit_path):
            os.makedirs(sources_edit_path)

        subprocess.Popen(r'explorer %s' % sources_edit_path)
        for file in file_selected:
            if file['step'] == "comp":
                src_file_path, filename = self.get_hero_comp_file(file['dir_path'])
                dst_path = os.path.join(sources_edit_path, filename)
                file_utils.xcopy_file(src_file_path, dst_path)
                #shutil.copy2(src_file_path, dst_path)
            else:
                dst_path = os.path.join(sources_edit_path, file['filename'])
                file_utils.xcopy_file(file['file_path'], dst_path)
                #shutil.copy2(file['file_path'], dst_path)

    def add_shot_to_edit_playlist(self, title, file_selected, edit_playlist_data):
        for edit_data in edit_playlist_data:
            if edit_data['title'] == title:
                for shot_file in file_selected:
                    edit_data["playlist"].append(shot_file)
        self.write_edit_share_playlist(edit_playlist_data)

    def set_status_playlist(self, edit_title, edit_playlist_data, status):
        title_selected = edit_title[0]['title']
        for edit_data in edit_playlist_data:
            if edit_data['title'] == title_selected:
                edit_data['status'] = status
        self.write_edit_share_playlist(edit_playlist_data)

    def get_hero_comp_file(self, dir_path):
        files = os.listdir(dir_path)
        hero_file = ''
        for file in files:
            if "hero" in file:
                hero_file = file

        if not hero_file: 
            raise custom_exception.PipelineError('Hero file not found in {}. \n Check if file exists.'.format(dir_path))
        src_hero_file = os.path.join(dir_path, hero_file)
        return src_hero_file, hero_file

    def dst_sources_file_path(self, project, step):
        # P:\SevenChickMovie\edit\sources\anim\q0070_anim_2019-07-17
        dst_path = "P:\\{project_name}\\edit\\sources\\{step}".format(
            project_name=project['name'],
            step=step
        )

        return dst_path
