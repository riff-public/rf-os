import tempfile
import os
import shutil
from rf_utils.pipeline import commands
from rf_utils.file_utils import xcopy_file


class RVPlayer:
    def __init__(self):
        self.base_chkdir_path = ""

    def before_play(self, files_selected):
        self.base_chkdir_path = self.create_tmp_dir_checkup()
        self.store_tmp_checkup(self.base_chkdir_path, files_selected)
        # is_duplicate = self.checksum(self.base_chkdir_path, files_selected)
        # if is_duplicate:
        #     pass
        # else:
        #     self.remove_files(self.base_chkdir_path)
            # self.copy_file_to_local_for_play(self.base_chkdir_path, files_selected)

    def clear_cache_tmp_file(self):
        tmp_dir = tempfile.gettempdir()
        dir_path = "{tmp_dir}\\mov_checkup".format(tmp_dir=tmp_dir)
        if os.path.exists(dir_path):
            shutil.rmtree(dir_path)

    def create_tmp_dir_checkup(self):
        tmp_dir = tempfile.gettempdir()
        dir_path = "{tmp_dir}\\mov_checkup".format(tmp_dir=tmp_dir)
        if not os.path.exists(dir_path):
            os.mkdir(dir_path)
        return dir_path

    def store_tmp_checkup(self, checkup_tmp_dir_path, files_selected):
        chktmpdir = os.listdir(checkup_tmp_dir_path)
        for file in files_selected:
            if file['filename'] not in chktmpdir:
                if file['step'] == "comp":
                    review_file = self.get_review_file_comp(file)
                    local_file = os.path.join(file['dir_path'], review_file)
                    xcopy_file(local_file, checkup_tmp_dir_path)
                else:
                    xcopy_file(file['file_path'], checkup_tmp_dir_path)

    # def checksum(self, checkup_tmp_dir_path, files_selected):
    #     chktmpdir = os.listdir(checkup_tmp_dir_path)
    #     if len(chktmpdir) == len(files_selected):
    #         for file in files_selected:
    #             if file['filename'] not in chktmpdir:
    #                 return False
    #             return True
    #     else:
    #         return False

    # def copy_file_to_local_for_play(self, checkup_tmp_dir_path, files_selected):
    #     for file in files_selected:
    #         src = "{file_path}\\{file}".format(file_path=file['file_path'], file=file)
    #         xcopy_file(file['file_path'], checkup_tmp_dir_path)

    def play(self, files_selected):
        # self.files_selected = files_selected
        self.before_play(files_selected)
        for file in os.listdir("C:/Program Files/Shotgun"):
            if "RV-" in file:
                rv_version = file
        command = ["C:/Program Files/Shotgun/{rv_version}/bin/rv.exe".format(rv_version=rv_version)]
        for file_item in files_selected:
            if os.path.exists(file_item['file_path']):
                if file_item['step'] == "comp":
                    review_file = self.get_review_file_comp(file_item)
                    local_file = os.path.join(self.base_chkdir_path, review_file)
                    command.append(local_file)
                else:
                    local_file = os.path.join(self.base_chkdir_path, file_item['filename'])
                    command.append(local_file)

        thread_subprocess = commands.ThreadSubprocess(command)
        thread_subprocess.start()

    def get_review_file_comp(self, file_item):
        files = os.listdir(file_item['dir_path'])
        for file in files:
            if 'review' in file:
                review_file = file

        # src_review_file_path = os.path.join(file_item['dir_path'], review_file)
        return review_file
