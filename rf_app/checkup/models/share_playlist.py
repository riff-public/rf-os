import os
import json
import shutil
import subprocess
from datetime import datetime


CHECKUP_SHARE_PLAYLIST_PATH = "O:/Pipeline/-TEMP-/checkup"
# CHECKUP_SHARE_PLAYLIST_PATH = 'D:/__playground/checkup'

class SharePlaylistModel:
    def __init__(self):
        self.share_playlists = {}
        self.file_path = "{file_path}/share_playlist.json".format(file_path=CHECKUP_SHARE_PLAYLIST_PATH)

    def init_state(self):
        pass

    def save_share_playlist(self, title, playlist_data):
        creator = os.environ.get('RFUSER', '')
        date = datetime.today().strftime('%y/%m/%d-%H:%M')
        data = {
            "title": title,
            "playlist": playlist_data,
            'creator': creator,
            'updated': date,
            'history': 'Created by: {} {}'.format(creator, date)
        }
        if not os.path.exists(CHECKUP_SHARE_PLAYLIST_PATH):
            os.makedirs(CHECKUP_SHARE_PLAYLIST_PATH)
            open(self.file_path, 'w').close()

        if os.stat(self.file_path).st_size == 0:
            with open(self.file_path, 'w+') as outfile:
                json.dump([data], outfile, indent=4)
        else:
            with open(self.file_path, 'r+') as outfile:
                data_loaded = json.load(outfile)
                data_loaded.append(data)
                outfile.seek(0)
                json.dump(data_loaded, outfile, indent=4)
                outfile.truncate()

    def update_share_playlist(self, title, playlist_data):
        user = os.environ.get('RFUSER', '')
        date = datetime.today().strftime('%y/%m/%d-%H:%M')
        current_data = self.get_share_playlist()
        index = None
        for idx, data in enumerate(current_data):
            if data['title'] == title:
                index = idx
                break

        # update new line of history only if it's different user and different day
        new_history = 'Modified by: {} {}'.format(user, date)
        curr_history = current_data[index].get('history', '').split('\n') if current_data[index].get('history', '') else []
        if curr_history:
            latest_history = curr_history[-1]
            history_parts = latest_history.split(' ')
            latest_date = datetime.strptime(history_parts[3], '%y/%m/%d-%H:%M').date()
            if history_parts[0] == 'Modified' and history_parts[2] == user and latest_date == datetime.today().date():
                curr_history[-1] =  new_history
            else:
                curr_history.append(new_history)
        else:
            curr_history.append(new_history)

        new_data = {
            "title": title,
            "playlist": playlist_data,
            'updated': date,
            'history': '\n'.join(curr_history)
        }
        current_data[index].update(new_data)
        self.write_share_playlist(current_data)

    def get_share_playlist(self):
        with open(self.file_path, 'r') as outfile:
            data_loaded = json.load(outfile)
        return data_loaded

    def write_share_playlist(self, data):
        with open(self.file_path, 'w') as outfile:
            json.dump(data, outfile, indent=4)

    def remove_share_playlist_title(self, share_playlist_title):
        new_data = self.get_share_playlist()
        # print "new_data : ", new_data
        for idx, data in enumerate(new_data):
            if data['title'] == share_playlist_title[0]['title']:
                new_data.pop(idx)
                break
        self.write_share_playlist(new_data)
        # print "new_data : ", new_data
        # new_data.pop(share_playlist_title[0], None)

    def copy_to_local(self, playlist_title, file_selected):
        user_local_path = os.path.join(os.path.expanduser("~"), playlist_title)
        if not os.path.exists(user_local_path):
            os.makedirs(user_local_path)

        subprocess.Popen(r'explorer %s' % user_local_path)
        for file in file_selected:
            dst_path = os.path.join(user_local_path, file['filename'])
            shutil.copy2(file['file_path'], dst_path)
            # file_utils.copy(file['file_path'], dst_path)
