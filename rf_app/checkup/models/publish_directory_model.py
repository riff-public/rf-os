import os
# import time
# import re
import itertools
# from Qt import QtCore

from rf_app.checkup.utils import checkup_file
from rf_utils.context import context_info
from rf_utils.sg.sg_utils import (
    sg,
    get_all_projects,
    get_episodes_by_project
)
import rf_config as config
from rf_utils.pipeline import user_pref
from rf_utils.tech_script import list_pub_media
reload(list_pub_media)


class PublishDirectoryModel:
    def __init__(self):
        self.publ_data_mapped = {}

    def get_all_publ_mov_files(self, path, project_name, episode_code, step):
        mov_files = []
        info = list_pub_media.find_media(project_name, episode_code, step)
        for shot, files in info.iteritems():
            mov_files.append(files)
        flatten_mov_files = list(itertools.chain.from_iterable(mov_files))
        return flatten_mov_files

    def get_publ_data_mapped(self):
        return self.publ_data_mapped

    def process_publish_dir_data(self, project, episode, step, order_by_selected):
        # data_mapped = {}
        # self.daily_data_mapped = {}
        # self.step_selected = step
        context = context_info.Context()
        context.update(
            project=project['name'],
            entityType='scene',
            entityGrp=episode['code']
        )
        scene_context = context_info.ContextPathInfo(context=context)
        base_path = '/'.join(scene_context.path.name(workspace=context_info.ContextKey.publish).abs_path().split("/")[:-1])
        mov_files = self.get_all_publ_mov_files(base_path, project['name'], episode['code'], step)
        mov_mapped = self.tranform_file_to_data(mov_files, base_path)
        self.publ_data_mapped = mov_mapped

    def tranform_file_to_data(self, mov_files, base_path):
        data_mapped = {}
        for mov_file in mov_files:
            print mov_file
            # mov_file_name = os.path.basename(mov_file)
            file_struct = self.get_file_struct(mov_file, base_path)
            date_modify = os.path.getmtime(file_struct['file_path'])
            date_created = os.path.getctime(file_struct['file_path'])
            data = {
                "project_name": file_struct['project_code'],
                "episode": file_struct['episode'],
                "sequence": file_struct['seq'],
                "shot": file_struct['shot'],
                "step": file_struct['step'],
                "filename": os.path.basename(mov_file),
                "file_path": file_struct['file_path'],
                "dir_path": file_struct['dir_path'],
                "workspace_type": file_struct['workspace_type'],
                "date_modified": date_modify,
                "date_created": date_created,
                "group": "{seq}_{shot}".format(seq=file_struct['seq'], shot=file_struct['shot'])
            }

            if file_struct['seq'] not in data_mapped:
                data_mapped.update({file_struct['seq']: {}})

            if file_struct['seq'] in data_mapped:
                if file_struct['shot'] not in data_mapped[file_struct['seq']]:
                    data_mapped[file_struct['seq']].update({file_struct['shot']: [data]})
                elif file_struct['shot'] in data_mapped[file_struct['seq']]:
                    data_mapped[file_struct['seq']][file_struct['shot']].append(data)

        return data_mapped

    def get_file_struct(self, mov_file, base_path):
        # print "mov length:", len(mov_fle_name.split("_"))
        mov_file_name = os.path.basename(mov_file)
        mov_fle_name_split = mov_file_name.split("_")
        # scm_act1_q0090b_s0490_anim_main.hero.mov
        project_code = mov_fle_name_split[0]
        episode = mov_fle_name_split[1]
        seq = mov_fle_name_split[2]
        shot = mov_fle_name_split[3]
        step = mov_fle_name_split[4]

        step = self.defend_name_step_possible_error(mov_fle_name_split[4])
        seq_shot = "{project_code}_{episode}_{seq}_{shot}".format(
            project_code=project_code,
            episode=episode,
            seq=seq,
            shot=shot
        )

        return {
            'project_code': project_code,
            'episode': episode,
            'seq': seq,
            'shot': shot,
            'step': step,
            'file_path': mov_file,
            'dir_path': os.path.dirname(mov_file),
            'workspace_type': "publ"
        }

    def defend_name_step_possible_error(self, step):
        STEPS = {
            'ani': 'anim'
        }

        if step in STEPS:
            return STEPS[step]
        return step
