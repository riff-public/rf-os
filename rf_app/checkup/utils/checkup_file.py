import os


def get_all_files_from_folder(folder_path):
    files = [f for f in os.listdir(folder_path) if os.path.isfile(os.path.join(folder_path, f))]
    filtered_files = filter(lambda file: os.path.splitext(file)[1] == ".mov", files)
    return filtered_files
