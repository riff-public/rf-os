# -*- coding: utf-8 -*-
# v.beta - UI overhaul
# v.1.0.0 - Add Switch Version in Shared Playlist's right-click menu
#         - Make Swich Version menu appear only if there's a version to switch to
# v.1.1.0 - Add Set Latest Version in Shared Playlist's right-click menu
#         - Fix bug in Switch Version
# v.1.1.1 - Add backward compatiblity for P: drive on rf_utils.list_pub_media
# v.1.2.0 - Add creator and date in shared playlist title view
# v.1.3.0 - Fix bug: creator name change, update date/time not updated when switching version in Shared Playlist
#         - Add combined playlists right click menu to My Playlist
#         - Add history in tooltip of Shared Playlist
# v.1.3.1 - Fix bug error when selecting cut order from My Playlist, add color warning for invalid shots
# v.1.4.0 - Add "Export Screenshots" feature for saving a screendshot frame(s) on mulitiple media in My Playlist
# v.1.4.1 - Fix bug wrong playlist displayed in Shared playlist when there are multiple playlist with the exact same name

import sys
import os
import traceback
import getpass
import subprocess
import time
from collections import OrderedDict
from datetime import datetime
from functools import partial
from settings import (
    TITLE_APP,
    VERSION_APP,
    DES,
    UI_NAME
)

core_path = '%s/core' % os.environ.get('RFSCRIPT')
if core_path not in sys.path:
    sys.path.append(core_path)

import rf_config as config
from view.playlist import PlaylistCustomWidget
from models.checkup import CheckUpModel
from models.publish_directory_model import PublishDirectoryModel
from models.share_playlist import SharePlaylistModel
from models.edit_share_playlist import EditSharePlaylistModel
from rf_utils.context import context_info
from rf_utils.ui import load, stylesheet
from rf_utils.widget import browse_widget
from rf_utils import log_utils, project_info, thread_pool
os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide2', 'PySide'])
from Qt import QtWidgets, QtCore, QtGui
from rf_utils import icon

userLog = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(UI_NAME, userLog)
logger = log_utils.init_logger(logFile)

UI_PATH = "{core}/rf_app/checkup/view/checkup.ui".format(core=core_path)
SHOTGUN_URL = "https://riffanimation.shotgunstudio.com"
STATUS_MAPPED = {
    "Hold": icon.sgHold,
    "Approve": icon.sgAprv,
    "Edit": icon.sgPndgPub
}

SHOTGUN_SHOT_CACHE = dict()

class CheckupControl(QtCore.QObject): 
    project_selected = QtCore.Signal(dict)

class CheckUpCtrl(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(CheckUpCtrl, self).__init__(parent)
        # ui read
        if config.isMaya:
            self.ui = load.setup_ui_maya(UI_PATH, parent)
        else:
            # print UI_PATH
            self.ui = load.setup_ui(UI_PATH, self)

        
        self.threadpool = QtCore.QThreadPool()
        self.threadpool.setMaxThreadCount(1)

        self.checkup_control = CheckupControl()
        self.checkup_model = CheckUpModel()
        self.publish_dir_model = PublishDirectoryModel()
        self.share_playlist_model = SharePlaylistModel()
        self.edit_share_playlist_model = EditSharePlaylistModel()
        self.playlist_widget_custom = PlaylistCustomWidget(self)
        # self.shots_from_sg = []
        # self.is_loaded_daily = False
        self.tab_selected = "Check Up"
        self.shot_entities = []
        self.init_state_app()
        self.init_signal()

        # override window title
        self.ui.setWindowTitle('{} {} {}'.format(TITLE_APP, VERSION_APP, DES))
        self.ui.show()

    def init_signal(self):
        self.ui.projects_combobox.currentIndexChanged.connect(self.project_combobox_changed)
        self.ui.entityType_combobox.currentIndexChanged.connect(self.entityType_combobox_changed)
        self.ui.step_combobox.currentIndexChanged.connect(self.step_combobox_changed)
        self.ui.latest_chkbox.stateChanged.connect(self.onchange_latest)
        self.ui.seq_list_widget.itemSelectionChanged.connect(self.seq_list_clicked)
        self.ui.shot_list_widget.itemSelectionChanged.connect(self.shot_list_clicked)
        self.ui.file_list_widget.itemSelectionChanged.connect(self.file_list_clicked)
        self.ui.playMedia_btn.clicked.connect(self.on_play_media)
        self.ui.load_checkup_btn.clicked.connect(self.on_submit)
        self.ui.order_combobox.currentIndexChanged.connect(self.onchange_order)
        self.ui.tabWidget.currentChanged.connect(self.tab_changed)
        # self.ui.load_daily_btn.clicked.connect(self.load_daily)
        # self.ui.daily_seq_list_widget.itemClicked.connect(self.daily_seq_list_clicked)
        # self.ui.daily_shot_list_widget.itemSelectionChanged.connect(self.daily_shot_list_clicked)
        # self.ui.daily_file_list_widget.itemSelectionChanged.connect(self.daily_file_list_clicked)
        self.ui.save_playlist.clicked.connect(self.save_playlist)
        self.ui.save_edit_playlist.clicked.connect(self.save_edit_playlist)
        self.ui.playlist_title_widget.itemSelectionChanged.connect(self.playlist_title_clicked)
        self.ui.share_playlist_title.itemClicked.connect(self.shared_playlist_title_clicked)
        self.ui.search_file_input.textChanged.connect(self.search_file_change)
        # self.ui.playlist_shot_widget.itemSelectionChanged.connect(self.playlist_shot_clicked)
        # self.ui.wip_checkbox.stateChanged.connect(self.onchange_workspace_type)
        # self.ui.publ_checkbox.stateChanged.connect(self.onchange_workspace_type)
        self.ui.edit_share_playlist_title.itemClicked.connect(self.edit_shared_playlist_title_clicked)
        self.ui.omt_chkbox.stateChanged.connect(self.onchange_omt_checkbox)

        self.ui.shared_search.textChanged.connect(self.search_shared_playlist_change)
        self.ui.sort_user_rbtn.toggled.connect(self.set_sort_playlist_shot_widget)
        self.ui.sort_created_rbtn.toggled.connect(self.set_sort_playlist_shot_widget)
        self.ui.sort_modified_rbtn.toggled.connect(self.set_sort_playlist_shot_widget)
        self.ui.sort_cutorder_rbtn.toggled.connect(self.set_sort_playlist_shot_widget)
        self.ui.sort_name_rbtn.toggled.connect(self.set_sort_playlist_shot_widget)
    
    def get_list_files_selected(self):
        index = self.ui.tabWidget.currentIndex()
        self.current_tab = self.ui.tabWidget.tabText(index)
        if self.current_tab == "Check Up":
            self.tab_selected = "Check Up"
            return self.get_files_selected()

        elif self.current_tab == "My PlayList":
            self.tab_selected = "My PlayList"
            return self.get_sort_playlist_shot_selected()

        elif self.current_tab == "Shared Playlist":
            self.tab_selected = "Shared Playlist"
            return self.get_shared_playlist_shot_widget_selected()

        elif self.current_tab == "Edit Playlist":
            self.tab_selected = "Edit Playlist"
            return self.get_edit_shared_playlist_shot_widget_selected()

    def init_state_app(self):
        self.data_mapped = {}
        self.publ_data_mapped = {}
        self.search_file_text = ""
        self.search_shared_playlist_text = ""
        # self.daily_data_mapped = {}
        self.init_projects_combobox()
        self.init_entityType_combobox()
        self.init_steps_combobox()
        self.init_order_combobox()
        if self.checkup_model.user_pref_data:
            self.init_default_setting()
        # self.is_enabled_daily_tab()
        self.set_context_menu()
        self.set_context_menu_edit_playlist()
        self.ui.playlist_shot_widget.setVisible(False)
        self.ui.verticalLayout_5.insertWidget(0, self.playlist_widget_custom)
        self.ui.horizontalLayout.setStretch(0, 0)
        self.ui.horizontalLayout.setStretch(1, 3)
        self.ui.horizontalLayout.setStretch(2, 0)

        header = self.ui.share_playlist_title.headerItem()

        header.setText(0, "Name")
        header.setText(1, "Creator")
        header.setText(2, "Updated")
        self.ui.share_playlist_title.setColumnWidth(0, 190)
        self.ui.share_playlist_title.setColumnWidth(1, 85)
        self.ui.share_playlist_title.sortItems(2, QtCore.Qt.DescendingOrder)  # sort by recorded date

        # title and icons
        app_icon = QtGui.QIcon()
        app_icon.addFile(icon.checkup, QtCore.QSize(16, 16))
        self.ui.setWindowTitle("Check Up")
        self.ui.setWindowIcon(app_icon)

        # paly button
        play_icon = QtGui.QIcon(icon.play_white)
        self.ui.playMedia_btn.setIcon(play_icon)

    def set_context_menu(self):
        self.ui.file_list_widget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.ui.file_list_widget.customContextMenuRequested.connect(self.file_list_widget_right_clicked)

        self.ui.playlist_title_widget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.ui.playlist_title_widget.customContextMenuRequested.connect(self.playlist_title_widget_right_clicked)

        self.playlist_widget_custom.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.playlist_widget_custom.customContextMenuRequested.connect(self.playlist_sort_shot_widget_right_clicked)

        self.ui.share_playlists_shot_widget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.ui.share_playlists_shot_widget.customContextMenuRequested.connect(self.share_playlist_sort_shot_widget_right_clicked)

        self.ui.share_playlist_title.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
        self.remove_share_playlist_title_menu = QtWidgets.QAction("Remove", self.ui.share_playlist_title)
        self.ui.share_playlist_title.addAction(self.remove_share_playlist_title_menu)
        self.remove_share_playlist_title_menu.triggered.connect(self.remove_shared_playlist_title)

        self.copy_share_playlist_title_menu = QtWidgets.QAction("Copy to my playlist", self.ui.share_playlist_title)
        self.ui.share_playlist_title.addAction(self.copy_share_playlist_title_menu)
        self.copy_share_playlist_title_menu.triggered.connect(self.copy_shared_playlist_title)

        self.copy_my_local_computer_menu = QtWidgets.QAction("Copy to my local computer", self.ui.share_playlist_title)
        self.ui.share_playlist_title.addAction(self.copy_my_local_computer_menu)
        self.copy_my_local_computer_menu.triggered.connect(self.copy_my_local_computer)

    def playlist_title_widget_right_clicked(self, position):
        sel_titles = self.ui.playlist_title_widget.selectedItems()
        menu = QtWidgets.QMenu()

        if len(sel_titles) == 1:
            share_action = QtWidgets.QAction("Share", self.ui.playlist_title_widget)
            share_action.triggered.connect(self.share_playlist_clicked)
            menu.addAction(share_action)

            ss_action = QtWidgets.QAction("Export Screenshots", self.ui.playlist_title_widget)
            ss_action.triggered.connect(self.take_screenshots)
            menu.addAction(ss_action)

        elif len(sel_titles) > 1:
            combine_action = QtWidgets.QAction("Combine playlists", self.ui.playlist_title_widget)
            combine_action.triggered.connect(partial(self.combine_playlists, sel_titles))
            menu.addAction(combine_action)

        menu.addSeparator()
        remove_action = QtWidgets.QAction("Remove", self.ui.playlist_title_widget)
        remove_action.triggered.connect(self.remove_playlist_title)
        menu.addAction(remove_action)

        menu.exec_(self.ui.playlist_title_widget.mapToGlobal(position))

    def take_screenshots(self):
        

        playlist_title = self.get_playlist_title_selected()
        playlist_data = self.checkup_model.get_playlist_data()
        try:
            playlists = playlist_data[playlist_title[0]]
        except IndexError:
            return
        
        file_paths = [f.get('file_path') for f in playlists if os.path.exists(f.get('file_path'))]
        if not file_paths:
            return
        output_dir = QtWidgets.QFileDialog.getExistingDirectory(parent=self, 
                                                            caption='Browse Output Directory',
                                                            dir=os.path.expanduser('~'))
        output_dir = output_dir.replace('\\', '/')
        if output_dir:
            self.loading_start()
            worker = thread_pool.Worker(self.export_screenshots, file_paths, output_dir)
            worker.signals.result.connect(self.export_screenshots_finished)
            self.threadpool.start(worker)

    def export_screenshots(self, file_paths, output_dir):
        from rf_utils.media_screenshot import main as media_ss
        ss_paths = media_ss.take_screenshots(input_paths=file_paths,
                                            output_dir=output_dir)
        return output_dir

    def export_screenshots_finished(self, output_dir):
        self.show_message_box("Screenshots exported successfully.")
        self.loading_stop()

        # open dir
        subprocess.Popen(r'explorer %s' % output_dir.replace('/', '\\'))

    def combine_playlists(self, items):
        playlist_title = self.get_playlist_title_selected()
        playlist_data = self.checkup_model.get_playlist_data()

        playlists = []
        for title in playlist_title:
            try:
                playlist = playlist_data[title]
                for sh in playlist:
                    playlists.append(sh)
            except IndexError:
                return

        # suggest playlist name that doesn't crash with exiting ones
        projectName = playlists[0].get('project_name')
        ep = playlists[0].get('episode')
        seq = playlists[0].get('sequence')
        step = playlists[0].get('step')
        playlist_name = "{}_{}_{}_{}_combined".format(projectName,ep,seq,step)
        if playlist_name in playlist_data:
            i = 1
            while True:
                new_name = playlist_name + str(i)
                if new_name in playlist_data:
                    i += 1
                else:
                    playlist_name = new_name
                    break

        combined_playlist_title, ok = QtWidgets.QInputDialog.getText(self, 'Combine playlist', 'Enter combined playlist title:', QtWidgets.QLineEdit.Normal, playlist_name)
        if ok and combined_playlist_title != "":
            has_special_char = self.checkup_model.check_special_text(combined_playlist_title)
            if not has_special_char:
                self.checkup_model.save_playlist(combined_playlist_title, playlists)
                self.playlist_widget_custom.clear()
                self.set_playlist_title()
                for i in range(self.ui.playlist_title_widget.count()): 
                    item = self.ui.playlist_title_widget.item(i)
                    if item.text() == combined_playlist_title:
                        self.ui.playlist_title_widget.setCurrentItem(item)
                        break
            else:
                self.show_message_box("Dont use special character ex. [@!#$%^&*()<>?/\|}{~:].")

    def set_context_menu_edit_playlist(self):
        self.ui.edit_share_playlist_title.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.ui.edit_share_playlist_title.customContextMenuRequested.connect(self.edit_share_playlist_title_right_clicked)

    def edit_share_playlist_title_right_clicked(self, position):
        menu = QtWidgets.QMenu()
        menu.addAction("Remove")
        menu.addAction("Copy to my local computer")

        set_status_menu = menu.addMenu("Set status to")
        icon_approve = QtGui.QIcon(STATUS_MAPPED["Approve"])
        icon_hold = QtGui.QIcon(STATUS_MAPPED["Hold"])
        icon_edit = QtGui.QIcon(STATUS_MAPPED["Edit"])
        set_status_menu.addAction(icon_approve, "Approve")
        set_status_menu.addAction(icon_hold, "Hold")
        set_status_menu.addAction(icon_edit, "Edit Keep")

        edit_playlist_data = self.edit_share_playlist_model.get_edit_share_playlist()
        edit_share_playlist_title = self.get_edit_shared_playlist_title_selected()
        action = menu.exec_(self.ui.edit_share_playlist_title.mapToGlobal(position))
        if action:
            if action.text() == "Approve":
                self.edit_share_playlist_model.set_status_playlist(
                    edit_share_playlist_title,
                    edit_playlist_data,
                    "Approve"
                )
                self.refrest_edit_share_playlist()
            elif action.text() == "Hold":
                self.edit_share_playlist_model.set_status_playlist(
                    edit_share_playlist_title,
                    edit_playlist_data,
                    "Hold"
                )
                self.refrest_edit_share_playlist()
            elif action.text() == "Edit Keep":
                self.edit_share_playlist_model.set_status_playlist(
                    edit_share_playlist_title,
                    edit_playlist_data,
                    "Edit"
                )
                self.refrest_edit_share_playlist()
            elif action.text() == "Remove":
                self.remove_edit_shared_playlist_title()
                self.refrest_edit_share_playlist()
            elif action.text() == "Copy to my local computer":
                self.copy_edit_my_local_computer()

    # def playlist_shot_widget_right_clicked(self, position):
    #     try:
    #         playlist_title = self.get_playlist_title_selected()
    #         playlist_selected = self.get_playlist_shot_selected()
    #         menu = QtWidgets.QMenu()
    #         remove_action = menu.addAction("Remove")

    #         versions_menu = menu.addMenu("Swtich version")
    #         versions_shot = self.checkup_model.fetch_versions_shot(playlist_selected)
    #         for version in versions_shot:
    #             version_action = versions_menu.addAction(version)

    #         action = menu.exec_(self.ui.playlist_shot_widget.mapToGlobal(position))
    #         if action:
    #             if action.text() == "Remove":
    #                 self.remove_playlist_shot()
    #             elif action.text()[0] == "v":
    #                 self.checkup_model.switch_version_shot_playlist(playlist_title, playlist_selected, action.text())
    #                 # self.set_playlist_shot_widget()
    #     except Exception as e:
    #         tb = sys.exc_info()[2]
    #         print e
    #         print "line number : %s", tb.tb_lineno
    #         self.show_critical_box(e)

    def share_playlist_sort_shot_widget_right_clicked(self, position):
        try:
            menu = QtWidgets.QMenu()
            # open in explorer
            open_action = menu.addAction("Open In Explorer")
            # open_action.triggered.connect(self.open_share_playlist_shot)

            menu.addSeparator()

            # set latest version
            all_lastest_version_action = menu.addAction("Set Latest Version")

            # switch version
            playlist_selected = self.get_shared_playlist_shot_widget_selected()
            versions_shot = [v for v in self.checkup_model.fetch_versions_shot(playlist_selected) if v.startswith('v')]
            if versions_shot:
                versions_menu = menu.addMenu("Switch Version")
                for version in versions_shot:
                    version_action = versions_menu.addAction(version)

            action = menu.exec_(self.ui.share_playlists_shot_widget.mapToGlobal(position))
            if action:
                if action.text() == "Open In Explorer":
                    self.open_share_playlist_shot()
                elif action.text() == "Set Latest Version":
                    playlist_title = self.get_shared_playlist_title_selected()
                    playlist_selected = self.get_shared_playlist_shot_widget_selected()
                    playlist_data = self.checkup_model.set_shared_playlist_lastest_version(playlist_title[0], playlist_selected, self.share_playlist)
                    self.share_playlist_model.update_share_playlist(playlist_title[0]['title'], playlist_data)
                    self.update_shares_playlist_title()
                    self.set_shared_playlist_shot_widget()
                elif action.text()[0] == "v":
                    playlist_title = self.get_shared_playlist_title_selected()
                    playlist_selected = self.get_shared_playlist_shot_widget_selected()
                    playlist_data = self.checkup_model.switch_version_shared_playlist(playlist_title[0], playlist_selected, action.text(), self.share_playlist)
                    self.share_playlist_model.update_share_playlist(playlist_title[0]['title'], playlist_data)
                    self.update_shares_playlist_title()
                    self.set_shared_playlist_shot_widget()
                    
        except Exception as e:
            tb = sys.exc_info()[2]
            print e
            print "line number : %s", tb.tb_lineno
            self.show_critical_box(e)

    def playlist_sort_shot_widget_right_clicked(self, position):
        try:
            playlist_title = self.get_playlist_title_selected()
            playlist_selected = self.get_sort_playlist_shot_selected()
            menu = QtWidgets.QMenu()
            
            # switch version
            versions_shot = [v for v in self.checkup_model.fetch_versions_shot(playlist_selected) if v.startswith('v')]
            if versions_shot:
                versions_menu = menu.addMenu("Switch Version")
                for version in versions_shot:
                    version_action = versions_menu.addAction(version)

            # switch department
            department_menu = menu.addMenu("Switch Department")
            steps = self.checkup_model.get_steps()
            for step in steps:
                step_action = department_menu.addAction("step: " + step)

            # add to playlist
            playlist_data = self.checkup_model.get_playlist_data()
            if playlist_data and playlist_title and [p for p in playlist_data.keys() if p not in playlist_title]:
                add_playlist_menu = menu.addMenu("Add To My Existing Playlist")
                for pt in playlist_data.keys():
                    if pt not in playlist_title: 
                        add_playlist_menu.addAction('+ ' + pt)

            # set latest version
            all_lastest_version_action = menu.addAction("Set Latest Version")

            menu.addSeparator()

            save_action = menu.addAction("Save To My Playlist")
            open_action = menu.addAction("Open In Explorer")
            
            menu.addSeparator()
            remove_action = menu.addAction("Remove")

            action = menu.exec_(self.playlist_widget_custom.mapToGlobal(position))
            if action:
                if action.text() == "Remove":
                    self.remove_playlist_shot()
                elif action.text() == "Open In Explorer":
                    self.open_playlist_shot()
                elif action.text() == "Save To My Playlist":
                    self.save_playlist()
                elif action.text() == "Set Latest Version":
                    self.checkup_model.set_playlist_lastest_version(playlist_title, playlist_selected)
                    self.set_sort_playlist_shot_widget()
                elif action.text()[0] == "v":
                    self.checkup_model.switch_version_shot_playlist(playlist_title, playlist_selected, action.text())
                    self.set_sort_playlist_shot_widget()
                elif action.text().startswith('+ '):
                    self.add_to_playlist(action.text().replace('+ ', ''))
                elif "step:" in action.text():
                    self.checkup_model.switch_step_shot_playlist(playlist_title, playlist_selected, action.text())
                    self.set_sort_playlist_shot_widget()

        except Exception as e:
            tb = sys.exc_info()[2]
            print e
            print "line number : %s", tb.tb_lineno
            self.show_critical_box(e)

    def file_list_widget_right_clicked(self, position):
        try:
            file_selected = self.get_files_selected()
            menu = QtWidgets.QMenu()
            menu.addAction("Open Explorer")
            add_playlist_menu = menu.addMenu("Add to playlist")
            playlist_data = self.checkup_model.get_playlist_data()
            for playlist_title in playlist_data.keys():
                add_playlist_menu.addAction("title: " + playlist_title)

            can_edit = self.check_enable_edit_playlist_btn(file_selected)
            if can_edit:
                edit_playlist_data = self.edit_share_playlist_model.get_edit_share_playlist()
                add_edit_playlist_menu = menu.addMenu("Add to Edit")
                for edit_playlist in edit_playlist_data:
                    add_edit_playlist_menu.addAction("edit: " + edit_playlist['title'])

            action = menu.exec_(self.ui.file_list_widget.mapToGlobal(position))
            if action:
                if action.text() == "Open Explorer":
                    self.open_explorer()
                elif "title" in action.text():
                    title = action.text().split(": ")[1]
                    self.checkup_model.add_shot_to_playlist(title, file_selected, playlist_data)
                elif "edit" in action.text():
                    title = action.text().split(": ")[1]
                    self.edit_share_playlist_model.add_shot_to_edit_playlist(title, file_selected, edit_playlist_data)
        except Exception as e:
            tb = sys.exc_info()[2]
            print e
            print "line number : %s", tb.tb_lineno
            self.show_critical_box(e)

    def open_explorer(self):
        files_selected = self.get_files_selected()
        subprocess.Popen(r'explorer %s' % os.path.dirname(files_selected[0]['file_path']).replace('/', '\\'))

    def open_playlist_shot(self):
        playlist_selected = self.get_sort_playlist_shot_selected()
        subprocess.Popen(r'explorer %s' % os.path.dirname(playlist_selected[0]['file_path']).replace('/', '\\'))

    def open_share_playlist_shot(self):
        share_playlist_selected = self.get_shared_playlist_shot_widget_selected()
        subprocess.Popen(r'explorer %s' % os.path.dirname(share_playlist_selected[0]['file_path']).replace('/', '\\'))

    def share_playlist_clicked(self):
        playlist_title = self.get_playlist_title_selected()
        playlist_data = self.checkup_model.get_playlist_data()
        try:
            playlists = playlist_data[playlist_title[0]]
        except IndexError:
            return
        projectName = playlists[0].get('project_name')
        ep = playlist_data[playlist_title[0]][0].get('episode')
        seq = playlist_data[playlist_title[0]][0].get('sequence')
        step = playlist_data[playlist_title[0]][0].get('step')
        share_playlist_title, ok = QtWidgets.QInputDialog.getText(self, 'Save share playlist', 'Enter your share playlist title:', QtWidgets.QLineEdit.Normal, "{}_{}_{}_{}".format(projectName,ep,seq,step))
        if ok and share_playlist_title != "":
            has_special_char = self.checkup_model.check_special_text(share_playlist_title)
            if not has_special_char:
                self.share_playlist_model.save_share_playlist(share_playlist_title, playlist_data[playlist_title[0]])
            else:
                self.show_message_box("Dont use special character ex. [@!#$%^&*()<>?/\|}{~:].")

    def remove_playlist_title(self):
        playlist_title = self.get_playlist_title_selected()
        self.checkup_model.remove_playlist_title(playlist_title)
        self.playlist_widget_custom.clear()
        self.set_playlist_title()

    def remove_shared_playlist_title(self):
        share_playlist_title = self.get_shared_playlist_title_selected()
        self.share_playlist_model.remove_share_playlist_title(share_playlist_title)
        self.ui.share_playlist_title.clear()
        self.ui.share_playlists_shot_widget.clear()
        self.set_shares_playlist_title()

    def remove_edit_shared_playlist_title(self):
        edit_share_playlist_title = self.get_edit_shared_playlist_title_selected()
        self.edit_share_playlist_model.remove_edit_share_playlist_title(edit_share_playlist_title)

    def copy_shared_playlist_title(self):
        shared_playlist_title_selected = self.get_shared_playlist_title_selected()

        self.checkup_model.save_playlist(
            shared_playlist_title_selected[0]['title'],
            shared_playlist_title_selected[0]['playlist']
        )

    # def copy_edit_shared_playlist_title(self):
    #     edit_shared_playlist_title_selected = self.get_edit_shared_playlist_title_selected()
    #     self.edit_share_playlist_model.save_edit_share_playlist(
    #         edit_shared_playlist_title_selected[0]['title'],
    #         edit_shared_playlist_title_selected[0]['playlist']
    #     )

    def copy_my_local_computer(self):
        try:
            shared_playlist_title_selected = self.get_shared_playlist_title_selected()
            self.share_playlist_model.copy_to_local(
                shared_playlist_title_selected[0]['title'],
                shared_playlist_title_selected[0]['playlist']
            )
        except Exception as e:
            tb = sys.exc_info()[2]
            print e
            print "line number : %s", tb.tb_lineno
            self.show_critical_box(e)

    def copy_edit_my_local_computer(self):
        try:
            edit_shared_playlist_title_selected = self.get_edit_shared_playlist_title_selected()
            project = self.get_project_selected()
            dst_path = self.edit_share_playlist_model.dst_sources_file_path(
                project,
                edit_shared_playlist_title_selected[0]['step']
            )
            self.edit_share_playlist_model.copy_to_local(
                edit_shared_playlist_title_selected[0]['title'],
                edit_shared_playlist_title_selected[0]['playlist'],
                dst_path
            )
        except Exception as e:
            tb = sys.exc_info()[2]
            error = traceback.format_exc()
            traceback.print_exc()
            print error
            print "line number : %s", tb.tb_lineno
            self.show_critical_box(e)

    def remove_playlist_shot(self):
        playlist_title = self.get_playlist_title_selected()
        playlist_selected = self.get_sort_playlist_shot_selected()
        self.checkup_model.remove_playlist_selected(playlist_title, playlist_selected)
        self.playlist_widget_custom.clear()
        # self.ui.playlist_shot_widget.clear()
        # self.set_playlist_shot_widget()
        self.set_sort_playlist_shot_widget()

    def init_shotgun_data(self): 
        """ fetch cache shotgun data for later use """ 
        worker = thread_pool.Worker(self.fetch_shot)
        worker.signals.result.connect(self.fetch_shot_finished)
        self.threadpool.start(worker)

    def fetch_shot(self): 
        from rf_utils.sg import sg_process 
        projectEntity = self.get_project_selected()
        project = projectEntity.get('name')

        if not project in SHOTGUN_SHOT_CACHE.keys(): 
            shots = sg_process.get_all_shots(project)
            SHOTGUN_SHOT_CACHE[project] = shots 

        return SHOTGUN_SHOT_CACHE[project]

    def fetch_shot_finished(self, shots): 
        # sorted order by cut order 
        sorted_shots = OrderedDict()
        
        for shot in shots: 
            order = shot.get('sg_cut_order')
            if not order: 
                order = 999
            key = '%03d - %s' % (order, shot.get('code'))
            # print '%s\n' % key
            sorted_shots[key] = shot

        self.shot_entities = [sorted_shots[a] for a in sorted(sorted_shots.keys())]

    def init_projects_combobox(self):
        self.ui.projects_combobox.blockSignals(True)
        self.ui.projects_combobox.clear()
        projects = self.checkup_model.get_projects()
        if len(projects) == 0:
            self.ui.projects_combobox.addItem("Not Found")
        else:
            for row, project in enumerate(projects):
                self.ui.projects_combobox.addItem(project.get('name'))
                self.ui.projects_combobox.setItemData(row, project, QtCore.Qt.UserRole)
        self.ui.projects_combobox.blockSignals(False)
        self.project_combobox_changed()

    def init_entityType_combobox(self):
        #print dir(self.ui)
        self.ui.entityType_combobox.blockSignals(True)
        self.ui.entityType_combobox.clear()
        entityTypes = self.checkup_model.get_entityTypes()
        for row, item in enumerate(entityTypes):
            self.ui.entityType_combobox.addItem(item)
            self.ui.entityType_combobox.setItemData(row, item, QtCore.Qt.UserRole)
        self.ui.entityType_combobox.blockSignals(False)

    def init_episodes_combobox(self):
        self.ui.act_combobox.blockSignals(True)
        self.ui.act_combobox.clear()
        if self.ui.entityType_combobox.currentText() == 'Scene':
            episodes = self.checkup_model.get_episodes()
            if len(episodes) == 0:
                self.ui.act_combobox.addItem("Not Found")
            else:
                for row, episode in enumerate(episodes):
                    self.ui.act_combobox.addItem(episode.get('code'))
                    self.ui.act_combobox.setItemData(row, episode, QtCore.Qt.UserRole)

        elif self.ui.entityType_combobox.currentText() == 'Asset':
            assetTypes = self.checkup_model.get_asset_type()
            for assetType in assetTypes:
                self.ui.act_combobox.addItem(assetType)

        self.ui.act_combobox.blockSignals(False)

    def init_steps_combobox(self):
        self.ui.step_combobox.blockSignals(True)
        self.ui.step_combobox.clear()
        steps = self.checkup_model.get_steps()
        if len(steps) == 0:
            self.ui.step_combobox.addItem("Not Found")
        else:
            for row, steps in enumerate(steps):
                self.ui.step_combobox.addItem(steps)
                self.ui.step_combobox.setItemData(row, steps, QtCore.Qt.UserRole)
        self.ui.step_combobox.blockSignals(False)

    def init_order_combobox(self):
        self.ui.order_combobox.blockSignals(True)
        self.ui.order_combobox.clear()
        options = self.checkup_model.order_options
        if len(options) == 0:
            self.ui.order_combobox.addItem("Not Found")
        else:
            for row, item in enumerate(options):
                self.ui.order_combobox.addItem(item.get('display'))
                self.ui.order_combobox.setItemData(row, item, QtCore.Qt.UserRole)
        self.ui.order_combobox.blockSignals(False)

    def init_default_setting(self):
        index_project = self.ui.projects_combobox.findText(
            self.checkup_model.user_pref_data['default_project'],
            QtCore.Qt.MatchFixedString
        )
        if index_project >= 0:
            self.ui.projects_combobox.setCurrentIndex(index_project)
        self.project_combobox_changed()

        index_episode = self.ui.act_combobox.findText(
            self.checkup_model.user_pref_data['default_episode'],
            QtCore.Qt.MatchFixedString
        )
        if index_episode >= 0:
            self.ui.act_combobox.setCurrentIndex(index_episode)

        index_step = self.ui.step_combobox.findText(
            self.checkup_model.user_pref_data['default_step'],
            QtCore.Qt.MatchFixedString
        )
        if index_step >= 0:
            self.ui.step_combobox.setCurrentIndex(index_step)

        index_order = self.ui.order_combobox.findText(
            self.checkup_model.user_pref_data['default_order'],
            QtCore.Qt.MatchFixedString
        )
        if index_order >= 0:
            self.ui.order_combobox.setCurrentIndex(index_order)

    def project_combobox_changed(self):
        project_selected = self.get_project_selected()
        self.checkup_model.set_project_selected(project_selected)
        self.checkup_model.set_episodes()
        self.checkup_model.set_asset_type()
        self.init_episodes_combobox()
        self.init_shotgun_data()


    def entityType_combobox_changed(self):
        self.checkup_model.set_episodes()
        self.checkup_model.set_asset_type()
        self.init_episodes_combobox()

    def step_combobox_changed(self):
        pass
        # self.is_enabled_daily_tab()

    def get_project_selected(self):
        index = self.ui.projects_combobox.currentIndex()
        project_selected = self.ui.projects_combobox.itemData(index, QtCore.Qt.UserRole)
        return project_selected

    def get_entityType_selected(self):
        index = self.ui.entityType_combobox.currentIndex()
        project_selected = self.ui.projects_combobox.itemData(index, QtCore.Qt.UserRole)
        return project_selected

    def get_episode_selected(self):
        index = self.ui.act_combobox.currentIndex()
        episode_selected = self.ui.act_combobox.itemData(index, QtCore.Qt.UserRole)
        return episode_selected

    def get_assetType_selected(self):
        assetType_selected = self.ui.act_combobox.currentText()
        return assetType_selected

    def get_step_selected(self):
        index = self.ui.step_combobox.currentIndex()
        step_selected = self.ui.step_combobox.itemData(index, QtCore.Qt.UserRole)
        return step_selected

    def get_seqs_selected(self):
        seqs_selected = []
        items = self.ui.seq_list_widget.selectedItems()
        for i in xrange(0, len(items)):
            seq_item = self.ui.seq_list_widget.selectedItems()[i].data(QtCore.Qt.UserRole)
            seqs_selected.append(seq_item)
        return seqs_selected

    # def get_daily_seqs_selected(self):
    #     seqs_selected = []
    #     items = self.ui.daily_seq_list_widget.selectedItems()
    #     for i in xrange(0, len(items)):
    #         seq_item = self.ui.daily_seq_list_widget.selectedItems()[i].data(QtCore.Qt.UserRole)
    #         seqs_selected.append(seq_item)
    #     return seqs_selected

    def get_shots_selected(self):
        shot_selected = []
        items = self.ui.shot_list_widget.selectedItems()
        for i in xrange(0, len(items)):
            shot_item = self.ui.shot_list_widget.selectedItems()[i].data(QtCore.Qt.UserRole)
            shot_selected.append(shot_item)
        return shot_selected

    # def get_daily_shots_selected(self):
    #     shot_selected = []
    #     items = self.ui.daily_shot_list_widget.selectedItems()
    #     for i in xrange(0, len(items)):
    #         shot_item = self.ui.daily_shot_list_widget.selectedItems()[i].data(QtCore.Qt.UserRole)
    #         shot_selected.append(shot_item)
    #     return shot_selected

    def get_files_selected(self):
        files_selected = []
        items = self.ui.file_list_widget.selectedItems()
        for i in xrange(0, len(items)):
            shot_item = self.ui.file_list_widget.selectedItems()[i].data(QtCore.Qt.UserRole)
            files_selected.append(shot_item)
        return files_selected

    # def get_daily_files_selected(self):
    #     files_selected = []
    #     items = self.ui.daily_file_list_widget.selectedItems()
    #     for i in xrange(0, len(items)):
    #         shot_item = self.ui.daily_file_list_widget.selectedItems()[i].data(QtCore.Qt.UserRole)
    #         files_selected.append(shot_item)
    #     return files_selected

    def get_order_by_selected(self):
        index = self.ui.order_combobox.currentIndex()
        order_selected = self.ui.order_combobox.itemData(index, QtCore.Qt.UserRole)
        return order_selected

    # def get_version_daily(self, daily_files_selected):
    #     versions = []
    #     for daily in daily_files_selected:
    #         versions.append(daily['id'])
    #     return versions

    def get_playlist_title_selected(self):
        playlist_title = []
        items = self.ui.playlist_title_widget.selectedItems()
        for item in items:
            data = item.data(QtCore.Qt.UserRole)
            playlist_title.append(data)
        return playlist_title

    def get_sort_playlist_shot_selected(self):
        playlist_shot_selected = []
        items = self.playlist_widget_custom.selectedItems()
        for i in xrange(0, len(items)):
            title_item = self.playlist_widget_custom.selectedItems()[i].data(QtCore.Qt.UserRole)
            playlist_shot_selected.append(title_item)
        return playlist_shot_selected

    def get_shared_playlist_title_selected(self):
        items = self.ui.share_playlist_title.selectedItems()
        title_item = items[0].data(QtCore.Qt.UserRole, 0)
        return [title_item]

    def get_edit_shared_playlist_title_selected(self):
        playlist_title = []
        items = self.ui.edit_share_playlist_title.selectedItems()
        for i in xrange(0, len(items)):
            title_item = self.ui.edit_share_playlist_title.selectedItems()[i].data(QtCore.Qt.UserRole)
            playlist_title.append(title_item)
        return playlist_title

    def get_shared_playlist_shot_widget_selected(self):
        shared_playlist = []
        items = self.ui.share_playlists_shot_widget.selectedItems()
        for i in xrange(0, len(items)):
            title_item = self.ui.share_playlists_shot_widget.selectedItems()[i].data(QtCore.Qt.UserRole)
            shared_playlist.append(title_item)
        return shared_playlist

    def get_edit_shared_playlist_shot_widget_selected(self):
        shared_playlist = []
        items = self.ui.edit_share_playlist_widget.selectedItems()
        for i in xrange(0, len(items)):
            title_item = self.ui.edit_share_playlist_widget.selectedItems()[i].data(QtCore.Qt.UserRole)
            shared_playlist.append(title_item)
        return shared_playlist

    # def get_playlist_shot_selected(self):
    #     playlist_shot_selected = []
    #     items = self.ui.playlist_shot_widget.selectedItems()
    #     for i in xrange(0, len(items)):
    #         title_item = self.ui.playlist_shot_widget.selectedItems()[i].data(QtCore.Qt.UserRole)
    #         playlist_shot_selected.append(title_item)
    #     return playlist_shot_selected

    def set_share_playlist(self):
        self.share_playlist = self.share_playlist_model.get_share_playlist()

    def set_edit_share_playlist(self):
        self.edit_share_playlist = self.edit_share_playlist_model.get_edit_share_playlist()

    def set_seq_list(self):
        self.ui.seq_list_widget.clear()
        for seq_name in sorted(self.data_mapped.keys()):
            widget_item = QtWidgets.QListWidgetItem(self.ui.seq_list_widget)
            widget_item.setText(seq_name + " - wip")
            data = {
                "name": seq_name,
                "workspace_type": "wip"
            }
            widget_item.setData(QtCore.Qt.UserRole, data)
            self.ui.seq_list_widget.addItem(widget_item)

        for seq_name in sorted(self.publ_data_mapped.keys()):
            widget_item = QtWidgets.QListWidgetItem(self.ui.seq_list_widget)
            widget_item.setText(seq_name + " - publ")
            data = {
                "name": seq_name,
                "workspace_type": "publ"
            }
            widget_item.setData(QtCore.Qt.UserRole, data)
            self.ui.seq_list_widget.addItem(widget_item)

    # def set_daily_seq_list(self):
    #     self.ui.daily_seq_list_widget.clear()
    #     for seq_name in sorted(self.daily_data_mapped.keys()):
    #         widget_item = QtWidgets.QListWidgetItem(self.ui.daily_seq_list_widget)
    #         widget_item.setText(seq_name)
    #         widget_item.setData(QtCore.Qt.UserRole, seq_name)
    #         self.ui.daily_seq_list_widget.addItem(widget_item)

    def set_shot_list(self):
        """ shot list """ 
        def set_shot_item(text, data): 
            widget_item = QtWidgets.QListWidgetItem(self.ui.shot_list_widget)
            widget_item.setText(text)
            widget_item.setData(QtCore.Qt.UserRole, data)
            self.ui.shot_list_widget.addItem(widget_item)

        item_wip = OrderedDict()
        item_publ = OrderedDict()
        use_cut_order = self.ui.cut_order_checkBox.isChecked()

        self.ui.shot_list_widget.clear()
        seqs_selected = self.get_seqs_selected()
        for seq in seqs_selected:
            if seq['workspace_type'] == "wip":
                for shot_name in sorted(self.data_mapped[seq['name']].keys()):
                    is_omit = self.checkup_model.is_omit_shot(seq['name'], shot_name)
                    if not is_omit or self.ui.omt_chkbox.isChecked():
                        # widget_item = QtWidgets.QListWidgetItem(self.ui.shot_list_widget)
                        key = '%s - %s' % (seq['name'], shot_name)
                        label = seq['name'] + " - " + shot_name + " - wip"
                        episode_name = self.data_mapped[seq['name']][shot_name][0]['episode']
                        # widget_item.setText(label)
                        data = {
                            "seq_name": seq['name'],
                            "shot_name": shot_name,
                            "episode_name": episode_name,
                            "workspace_type": "wip"
                        }
                        # widget_item.setData(QtCore.Qt.UserRole, data)
                        # self.ui.shot_list_widget.addItem(widget_item)
                        item_wip[key] = {'label': label, 'data': data}

            if seq['workspace_type'] == "publ":
                for shot_name in sorted(self.publ_data_mapped[seq['name']].keys()):
                    is_omit = self.checkup_model.is_omit_shot(seq['name'], shot_name)
                    if not is_omit or self.ui.omt_chkbox.isChecked():
                        key = '%s - %s' % (seq['name'], shot_name)
                        label = seq['name'] + " - " + shot_name + " - publ"
                        episode_name = self.publ_data_mapped[seq['name']][shot_name][0]['episode']
                        # widget_item = QtWidgets.QListWidgetItem(self.ui.shot_list_widget)
                        # widget_item.setText(label)
                        data = {
                            "seq_name": seq['name'],
                            "shot_name": shot_name,
                            "episode_name": episode_name,
                            "workspace_type": "publ"
                        }
                        item_publ[key] = {'label': label, 'data': data}
                        # widget_item.setData(QtCore.Qt.UserRole, data)
                        # self.ui.shot_list_widget.addItem(widget_item)

        if use_cut_order: 
            if self.shot_entities: 
                for shot in self.shot_entities: 
                    seqcode = shot.get('sg_sequence_code')
                    shotcode = shot.get('sg_shortcode')
                    episode = shot.get('sg_episode').get('name') if shot.get('sg_episode') else None
                    order = shot.get('sg_cut_order')
                    key = '%s - %s' % (seqcode, shotcode)

                    if key in item_wip.keys(): 
                        text = '%s [%s]' % (item_wip[key].get('label'), order)
                        data = item_wip[key].get('data')
                        if episode == data['episode_name']:
                            set_shot_item(text, data)

                    if key in item_publ.keys(): 
                        text = '%s [%s]' % (item_publ[key].get('label'), order)
                        data = item_publ[key].get('data')
                        if episode == data['episode_name']:
                            set_shot_item(text, data)

            else: 
                logger.error('No shotgun information. Cannot use shot order')

        else: 
            for key, value in item_wip.items(): 
                text = value.get('label')
                data = value.get('data')
                set_shot_item(text, data)

            for key, value in item_publ.items(): 
                text = value.get('label')
                data = value.get('data')
                set_shot_item(text, data)



    # def set_daily_shot_list(self):
    #     self.ui.daily_shot_list_widget.clear()
    #     seqs_selected = self.get_daily_seqs_selected()
    #     for seq in seqs_selected:
    #         for shot_name in sorted(self.daily_data_mapped[seq].keys()):
    #             widget_item = QtWidgets.QListWidgetItem(self.ui.daily_shot_list_widget)
    #             widget_item.setText(seq + " - " + shot_name)
    #             data = {
    #                 "seq": seq,
    #                 "shot_name": shot_name
    #             }
    #             widget_item.setData(QtCore.Qt.UserRole, data)
    #             self.ui.daily_shot_list_widget.addItem(widget_item)

    def set_display_widget_file_mov(self):
        self.ui.file_list_widget.clear()
        mov_files_display = self.checkup_model.get_mov_files_display()
        if self.search_file_text != "":
            mov_files_display = filter(lambda mov_file: self.search_file_text.lower().decode('utf8') in mov_file['filename'].lower().decode('utf8'), mov_files_display)
        for mov_file in mov_files_display:
            widget_item = QtWidgets.QListWidgetItem(self.ui.file_list_widget)
            widget_item.setText(mov_file['filename'])
            widget_item.setData(QtCore.Qt.UserRole, mov_file)
            self.ui.file_list_widget.addItem(widget_item)

    # def set_daily_display_widget_file_mov(self):
    #     self.ui.daily_file_list_widget.clear()
    #     mov_files_display = self.checkup_model.get_daily_mov_files_display()
    #     for mov_file in mov_files_display:
    #         widget_item = QtWidgets.QListWidgetItem(self.ui.daily_file_list_widget)
    #         widget_item.setText(mov_file['code'])
    #         widget_item.setData(QtCore.Qt.UserRole, mov_file)
    #         self.ui.daily_file_list_widget.addItem(widget_item)

    def set_display_widget_preview_file_mov(self):
        self.ui.preview_file_list_widget.clear()
        files_selected = self.get_files_selected()
        self.check_enable_playlist_btn(files_selected)
        self.check_enable_edit_playlist_btn(files_selected)
        for mov_file in files_selected:
            widget_item = QtWidgets.QListWidgetItem(self.ui.preview_file_list_widget)
            widget_item.setForeground(QtGui.QColor('#f5f5f5'))
            widget_item.setText(mov_file['filename'])
            widget_item.setData(QtCore.Qt.UserRole, mov_file)
            self.ui.preview_file_list_widget.addItem(widget_item)

    # def set_daily_display_widget_preview_file_mov(self):
    #     self.ui.daily_preview_file_list_widget.clear()
    #     daily_files_selected = self.get_daily_files_selected()
    #     for mov_file in daily_files_selected:
    #         widget_item = QtWidgets.QListWidgetItem(self.ui.daily_preview_file_list_widget)
    #         widget_item.setForeground(QtGui.QColor('#f5f5f5'))
    #         widget_item.setText(mov_file['code'])
    #         widget_item.setData(QtCore.Qt.UserRole, mov_file)
    #         self.ui.daily_preview_file_list_widget.addItem(widget_item)

    def set_file_mov_list(self):
        shots_selected = self.get_shots_selected()
        isLatest = self.ui.latest_chkbox.isChecked()
        order_by_selected = self.get_order_by_selected()
        self.checkup_model.set_files_mov_filter(
            shots_selected,
            isLatest,
            order_by_selected,
            self.publish_dir_model
        )

    # def set_daily_file_mov_list(self):
    #     shots_selected = self.get_daily_shots_selected()
    #     isLatest = self.ui.latest_chkbox.isChecked()
    #     order_by_selected = self.get_order_by_selected()
    #     self.checkup_model.set_daily_files_mov_filter(shots_selected, isLatest, order_by_selected)

    def set_playlist_title(self):
        playlist_data = self.checkup_model.get_playlist_data()
        self.ui.playlist_title_widget.clear()
        for title in sorted(playlist_data.keys()):
            widget_item = QtWidgets.QListWidgetItem(self.ui.playlist_title_widget)
            widget_item.setText(title)
            widget_item.setData(QtCore.Qt.UserRole, title)
            self.ui.playlist_title_widget.addItem(widget_item)

    # def set_playlist_shot_widget(self):
    #     playlist_data = self.checkup_model.get_playlist_data()
    #     self.ui.playlist_shot_widget.clear()
    #     title_selected = self.get_playlist_title_selected()
    #     for title in title_selected:
    #         for shot_item in playlist_data[title]:
    #             widget_item = QtWidgets.QListWidgetItem(self.ui.playlist_shot_widget)
    #             widget_item.setText(shot_item.get('filename'))
    #             widget_item.setData(QtCore.Qt.UserRole, shot_item)
    #             self.ui.playlist_shot_widget.addItem(widget_item)

    def set_sort_playlist_shot_widget(self):
        self.playlist_widget_custom.clear()
        playlist_data = self.checkup_model.get_playlist_data()
        title_selected = self.get_playlist_title_selected()
        
        for title in title_selected:
            sorted_data = self.sort_custom_playlist_data(playlist_data[title])
            if not sorted_data:
                continue
            max_len = max([len(s.get('filename')) for s in sorted_data])
            for shot_item in sorted_data:
                date_updated = "Modified:  {}".format(datetime.fromtimestamp(int(shot_item.get('date_modified'))))
                widget_item = QtWidgets.QListWidgetItem(self.playlist_widget_custom)
                filename = shot_item.get('filename')
                space_mult = (100-max_len) + (max_len - len(filename))
                widget_item.setText(filename + space_mult*' ' + '\t' + date_updated)
                if shot_item.get('INVALID'):
                    widget_item.setBackground(QtGui.QBrush(QtGui.QColor(185, 25, 12)))
                widget_item.setData(QtCore.Qt.UserRole, shot_item)
                self.playlist_widget_custom.addItem(widget_item)
                # self.playlist_widget_custom.setItemWidget(widget_item, myQWidget)

    def sort_custom_playlist_data(self, shots):
        if self.ui.sort_user_rbtn.isChecked():  # default order
            return shots
        elif self.ui.sort_name_rbtn.isChecked():  # order by name
            return sorted(shots, key=lambda s: '{}_{}_{}_{}'.format(s['project_name'], s['episode'], s['sequence'], s['shot']))
        elif self.ui.sort_created_rbtn.isChecked():  # order date created
            return sorted(shots, key=lambda s: s.get('date_created'))
        elif self.ui.sort_modified_rbtn.isChecked():  # order date modifed
            return sorted(shots, key=lambda s: s.get('date_modified'))
        elif self.ui.sort_cutorder_rbtn.isChecked():  # order by sg_cut_order
            sorted_shots = [s.get('code') for s in self.shot_entities]
            def append_shot_name(s, sorted_shots):
                sn = '{}_{}_{}_{}'.format(s['project_name'], s['episode'], s['sequence'], s['shot'])
                if sn in sorted_shots:
                    return sorted_shots.index(sn)
                else:
                    s['INVALID'] = True
                    # print('Cannot find {} in SG entity list.'.format(sn))
                    return 0
            return sorted(shots, key=lambda s:append_shot_name(s, sorted_shots))

    # def set_state_daily_tab(self):
    #     self.set_daily_seq_list()

    def set_shares_playlist_title(self):
        self.set_share_playlist()
        self.ui.share_playlist_title.clear()
        for playlist in self.share_playlist:
            if self.search_shared_playlist_text != "":
                if not self.search_shared_playlist_text.lower() in playlist.get('title').lower():
                    continue
            widget_item = QtWidgets.QTreeWidgetItem(self.ui.share_playlist_title)
            widget_item.setText(0, playlist.get('title'))
            widget_item.setData(QtCore.Qt.UserRole, 0, playlist)
            # self.ui.share_playlist_title.addItem(widget_item)

            widget_item.setText(1, playlist.get('creator', ''))
            widget_item.setTextAlignment(1, QtCore.Qt.AlignCenter)
            widget_item.setText(2, playlist.get('updated', ''))
            widget_item.setToolTip(2, playlist.get('history', ''))

    def update_shares_playlist_title(self):
        self.set_share_playlist()
        items = self.ui.share_playlist_title.selectedItems()
        if not items:
            return
        sel_item = items[0]
        playlist_to_update = None
        for playlist in self.share_playlist:
            if playlist.get('title') == sel_item.text(0) and playlist.get('creator') == sel_item.text(1):
                playlist_to_update = playlist
                break
        if playlist_to_update:
            sel_item.setText(2, playlist_to_update.get('updated'))
            sel_item.setToolTip(2, playlist.get('history', ''))

    def set_edit_share_playlist_title(self):
        self.set_edit_share_playlist()
        self.ui.edit_share_playlist_title.clear()
        for playlist in self.edit_share_playlist:
            widget_item = QtWidgets.QListWidgetItem(self.ui.edit_share_playlist_title)
            widget_item.setIcon(QtGui.QIcon(self.get_icon_by_name(playlist.get("status"))))
            widget_item.setText(playlist.get('title'))
            widget_item.setData(QtCore.Qt.UserRole, playlist)
            self.ui.edit_share_playlist_title.addItem(widget_item)

    def set_shared_playlist_shot_widget(self):
        share_playlist_title = self.get_shared_playlist_title_selected()
        self.ui.share_playlists_shot_widget.clear()
        current_share_playlist = []
        for playlist in self.share_playlist:
            if playlist['title'] == share_playlist_title[0]['title'] and \
                playlist['updated'] == share_playlist_title[0]['updated'] and \
                playlist['creator'] == share_playlist_title[0]['creator']:

                current_share_playlist = playlist.get('playlist')
                break
        for playlist in current_share_playlist:
            widget_item = QtWidgets.QListWidgetItem(self.ui.share_playlists_shot_widget)
            widget_item.setText(playlist.get('filename'))
            widget_item.setData(QtCore.Qt.UserRole, playlist)
            self.ui.share_playlists_shot_widget.addItem(widget_item)

    def set_edit_shared_playlist_shot_widget(self):
        edit_share_playlist_title = self.get_edit_shared_playlist_title_selected()
        self.ui.edit_share_playlist_widget.clear()
        current_share_playlist = []
        for playlist in self.edit_share_playlist:
            if playlist['title'] == edit_share_playlist_title[0]['title']:
                current_share_playlist = playlist.get('playlist')
                break
        for playlist in current_share_playlist:
            widget_item = QtWidgets.QListWidgetItem(self.ui.edit_share_playlist_widget)
            widget_item.setText(playlist.get('filename'))
            widget_item.setData(QtCore.Qt.UserRole, playlist)
            self.ui.edit_share_playlist_widget.addItem(widget_item)

    def seq_list_clicked(self):
        self.set_shot_list()

    def shot_list_clicked(self):
        self.set_file_mov_list()
        self.set_display_widget_file_mov()

    def file_list_clicked(self):
        self.set_display_widget_preview_file_mov()
        # self.checkup_model.set_preview_files()

    # def daily_seq_list_clicked(self):
    #     self.set_daily_shot_list()

    # def daily_shot_list_clicked(self):
    #     self.set_daily_file_mov_list()
    #     self.set_daily_display_widget_file_mov()

    # def daily_file_list_clicked(self):
    #     self.set_daily_display_widget_preview_file_mov()

    def playlist_title_clicked(self):
        self.set_sort_playlist_shot_widget()
        # self.set_playlist_shot_widget()

    def shared_playlist_title_clicked(self):
        self.set_shared_playlist_shot_widget()
        # self.set_playlist_shot_widget()

    def search_shared_playlist_change(self, text):
        self.search_shared_playlist_text = text
        self.set_shares_playlist_title()

    def edit_shared_playlist_title_clicked(self):
        self.set_edit_shared_playlist_shot_widget()
        # self.set_playlist_shot_widget()

    def playlist_shot_clicked(self):
        pass
        # self.set_playlist_shot_widget()

    def tab_changed(self, index):
        """ if tab changed, button command changed """
        self.current_tab = self.ui.tabWidget.tabText(index)
        if self.current_tab == "Check Up":
            self.tab_selected = "Check Up"
            # print "select checkup tab"
            self.ui.save_playlist.setEnabled(False)
            self.set_display_widget_preview_file_mov()

        # if self.current_tab == "Daily":
        #     self.tab_selected = "Daily"
        #     if self.data_mapped:
        #         print "select daily tab"
        #         self.set_state_daily_tab()
        #         if not self.is_loaded_daily:
        #             self.load_daily()
        #             self.is_loaded_daily = True

        if self.current_tab == "My PlayList":
            self.tab_selected = "My PlayList"
            self.playlist_widget_custom.clear()
            self.set_playlist_title()
            self.ui.save_playlist.setEnabled(True)
            

        if self.current_tab == "Shared Playlist":
            self.tab_selected = "Shared Playlist"
            self.set_shares_playlist_title()
            self.ui.save_playlist.setEnabled(True)

        if self.current_tab == "Edit Playlist":
            self.tab_selected = "Edit Playlist"
            self.set_edit_share_playlist_title()
            self.ui.save_playlist.setEnabled(True)

            # self.ui.share_playlists_shot_widget.clear()
            # self.set_daily_display_widget_file_mov()

            # self.ui.submit_btn.setText('Checkout as work version')

    # def load_daily(self):
    #     self.loading_start()
    #     QtCore.QCoreApplication.processEvents()
    #     try:
    #         self.ui.daily_seq_list_widget.clear()
    #         self.ui.daily_shot_list_widget.clear()
    #         self.ui.daily_file_list_widget.clear()
    #         self.ui.daily_preview_file_list_widget.clear()

    #         project = self.get_project_selected()
    #         step = self.get_step_selected()
    #         self.checkup_model.load_daily(project, step)
    #         # self.set_state_daily_tab()
    #     except Exception as e:
    #         tb = sys.exc_info()[2]
    #         print e
    #         print "line number : %s", tb.tb_lineno
    #         self.show_critical_box(e)
    #     finally:
    #         self.loading_stop()

    def on_submit(self):
        start = time.time()
        self.loading_start()
        # self.is_loaded_daily = False
        self.data_mapped = {}
        self.publ_data_mapped = {}
        QtCore.QCoreApplication.processEvents()
        try:
            self.ui.seq_list_widget.clear()
            self.ui.shot_list_widget.clear()
            self.ui.file_list_widget.clear()
            self.ui.preview_file_list_widget.clear()

            is_checked_wip = self.ui.wip_checkbox.isChecked()
            is_checked_publ = self.ui.publ_checkbox.isChecked()

            project = self.get_project_selected()
            episode = self.get_episode_selected()
            assetType = self.get_assetType_selected()
            
            step = self.get_step_selected()
            order_by_selected = self.get_order_by_selected()
            self.checkup_model.fetch_all_shots(project, episode, step)

            if is_checked_wip:
                if self.ui.entityType_combobox.currentText() == 'Scene':
                    self.checkup_model.process_check_up_data(project, episode, step, order_by_selected)
                elif self.ui.entityType_combobox.currentText() == 'Asset':
                    self.checkup_model.process_check_up_data_asset(project, assetType, step, order_by_selected)
                self.data_mapped = self.checkup_model.get_data_mapped()
            if is_checked_publ:
                self.publish_dir_model.process_publish_dir_data(
                    project,
                    episode,
                    step,
                    order_by_selected
                )
                self.publ_data_mapped = self.publish_dir_model.get_publ_data_mapped()

            self.set_seq_list()
            self.force_change_order_by()
            # self.checkup_model.remove_tmp_rv_files() # if play file in local should uncomment
        except WindowsError as e:
            print(e) 
            print "possible permission access"
        except Exception as e:
            tb = sys.exc_info()[2]
            print e
            print "line number : %s", tb.tb_lineno
            self.show_critical_box(e)
        finally:
            end = time.time()
            self.loading_stop()
            print "total time: ", end - start

    def on_play_media(self):
        if self.ui.keyframePro_radioButton.isChecked():
            play_func = self.checkup_model.process_play_keyframepro
        elif self.ui.RV_radioButton.isChecked():
            play_func = self.checkup_model.process_play_rv

        self.loading_start()
        QtCore.QCoreApplication.processEvents()
        try:
            if self.tab_selected == "Check Up":
                files_selected = self.get_files_selected()
                if len(files_selected) == 0:
                    self.show_message_box("Please select some file.")
                    return
                play_func(files_selected)
            # elif self.tab_selected == "Daily":
            #     daily_files_selected = self.get_daily_files_selected()
            #     if len(daily_files_selected) == 0:
            #         self.show_message_box("Please select some file.")
            #         return
            #     versions = self.get_version_daily(daily_files_selected)
            #     self.open_shotgun_versions(versions)
            elif self.tab_selected == "My PlayList":
                playlist_shot_selected = self.get_sort_playlist_shot_selected()
                if len(playlist_shot_selected) == 0:
                    self.show_message_box("Please select some file.")
                    return
                play_func(playlist_shot_selected)
            elif self.tab_selected == "Shared Playlist":
                shared_playlist_shot_selected = self.get_shared_playlist_shot_widget_selected()
                if len(shared_playlist_shot_selected) == 0:
                    self.show_message_box("Please select some file.")
                    return
                play_func(shared_playlist_shot_selected)
            elif self.tab_selected == "Edit Playlist":
                edit_shared_playlist_shot_selected = self.get_edit_shared_playlist_shot_widget_selected()
                if len(edit_shared_playlist_shot_selected) == 0:
                    self.show_message_box("Please select some file.")
                    return
                play_func(edit_shared_playlist_shot_selected)
        except Exception as e:
            tb = sys.exc_info()[2]
            print e
            print "line number : %s", tb.tb_lineno
            self.show_critical_box(e)
        finally:
            self.loading_stop()

    def onchange_latest(self):
        if self.tab_selected == "Check Up":
            self.set_file_mov_list()
            self.set_display_widget_file_mov()

        # if self.tab_selected == "Daily":
        #     self.set_daily_file_mov_list()
        #     self.set_daily_display_widget_file_mov()

    def onchange_omt_checkbox(self):
        self.set_shot_list()

    def onchange_order(self):
        if self.tab_selected == "Check Up":
            self.set_file_mov_list()
            self.set_display_widget_file_mov()

    def force_change_order_by(self):
        if self.ui.publ_checkbox.isChecked():
            self.ui.order_combobox.setCurrentIndex(1)
        elif self.ui.wip_checkbox.isChecked():
            self.ui.order_combobox.setCurrentIndex(0)

        # if self.tab_selected == "Daily":
        #     self.set_daily_file_mov_list()
        #     self.set_daily_display_widget_file_mov()

    def loading_start(self):
        # self.ui.loading_txt.setVisible(True)
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        self.ui.loading_txt.setText("Loading, please wait . . .")
        # self.ui.setEnabled(False)

    def loading_stop(self):
        QtWidgets.QApplication.restoreOverrideCursor()
        self.ui.loading_txt.setText("")
        # self.ui.setEnabled(True)

    def show_critical_box(self, msg):
        QtWidgets.QMessageBox.critical(self, "Message", "Error:" + str(msg) + ". Please contact admin.")

    def show_message_box(self, msg):
        QtWidgets.QMessageBox.information(self, "Message", str(msg))

    def open_shotgun_versions(self, versions):
        from PySide import QtGui
        command = """-l -play -eval 'shotgun.theMode().setServerURLValue("%s");shotgun.sessionFromVersionIDs(int[] {%s});""" % (SHOTGUN_URL, ','.join([str(i) for i in versions]))
        baked = "rvlink://baked/%s" % (command.encode("hex"))
        QtGui.QDesktopServices.openUrl(baked)

    # def is_enabled_daily_tab(self):
    #     step_selected = self.get_step_selected()
    #     if step_selected == "animatic":
    #         self.ui.tabWidget.setTabEnabled(1, False)
    #     else:
    #         self.ui.tabWidget.setTabEnabled(1, True)

    def check_enable_playlist_btn(self, files_selected):
        if len(files_selected) == 0:
            self.ui.save_playlist.setEnabled(False)
        else:
            self.ui.save_playlist.setEnabled(True)

    def check_enable_edit_playlist_btn(self, files_selected):
        if self.ui.publ_checkbox.isChecked() and self.ui.wip_checkbox.isChecked() is not True:
            if len(files_selected) == 0:
                self.ui.save_edit_playlist.setEnabled(False)
                return False

            for file in files_selected:
                if file['workspace_type'] == "wip":
                    self.ui.save_edit_playlist.setEnabled(False)
                    return False
            self.ui.save_edit_playlist.setEnabled(True)
            return True
        else:
            self.ui.save_edit_playlist.setEnabled(False)
            return False

    def save_playlist(self):
        file_selected = self.get_list_files_selected()
        playlist_title_input = QtWidgets.QInputDialog()
        playlist_title, ok = playlist_title_input.getText(
            self,
            'Save Playlist',
            'Enter your playlist title:'
        )
        if ok and playlist_title != "":
            has_special_char = self.checkup_model.check_special_text(playlist_title)
            if not has_special_char:
                if playlist_title in self.checkup_model.get_playlist_data():
                    self.checkup_model.update_playlist(playlist_title, file_selected)
                else:
                    self.checkup_model.save_playlist(playlist_title, file_selected)
                    self.playlist_widget_custom.clear()
                    self.ui.playlist_title_widget.clear()
                    self.set_playlist_title()
            else:
                self.show_message_box("Dont use special character ex. [@!#$%^&*()<>?/\|}{~:].")

    def add_to_playlist(self, playlist_title):
        file_selected = self.get_list_files_selected()
        if not file_selected:
            return

        self.checkup_model.update_playlist(playlist_title, file_selected)
        # self.playlist_widget_custom.clear()
        # self.ui.playlist_title_widget.clear()
        # self.set_playlist_title()

    def save_edit_playlist(self):
        file_selected = self.get_files_selected()
        step = self.get_step_selected()
        playlist_title_input = QtWidgets.QInputDialog()

        playlist_title, ok = playlist_title_input.getText(
            self,
            'Save Edit Playlist',
            'Enter your edit playlist title:'
        )
        if ok and playlist_title != "":
            has_special_char = self.checkup_model.check_special_text(playlist_title)

            if not has_special_char:
                self.edit_share_playlist_model.save_edit_share_playlist(
                    playlist_title,
                    file_selected,
                    step
                )
            else:
                self.show_message_box("Dont use special character ex. [@!#$%^&*()<>?/\|}{~:].")

    def search_file_change(self, text):
        self.search_file_text = text
        self.set_display_widget_file_mov()

    def get_icon_by_name(self, status_name):
        return STATUS_MAPPED[status_name]

    def refrest_edit_share_playlist(self):
        self.ui.edit_share_playlist_title.clear()
        self.ui.edit_share_playlist_widget.clear()
        self.set_edit_share_playlist_title()

    # def onchange_workspace_type(self):
    #     can_edit = self.can_edit_playlist()
    #     self.ui.save_edit_playlist.setEnabled(can_edit)


def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(UI_NAME)
        myApp = CheckUpCtrl(maya_win.getMayaWindow())
        return myApp
    else:
        logger.info('Run in Checkup\n')
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        myApp = CheckUpCtrl()
        stylesheet.set_default(app)
        sys.exit(app.exec_())


if __name__ == '__main__':
    show()
