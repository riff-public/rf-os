#!/usr/bin/env python
# -- coding: utf-8 --

# v.0.0.1 polytag switcher
_title = 'Anim Publish Guide'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'AnimPublishGuideUI'

#Import python modules
import sys
import os 
import getpass
from collections import OrderedDict
from functools import partial

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]


# framework modules 
import rf_config as config
from rf_utils import log_utils
from rf_utils.ui import load
user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'

logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import maya.cmds as mc 
from rf_utils.context import context_info
from rftool.utils import pipeline_utils
reload(pipeline_utils)
reload(context_info)



class AnimPublsihGuide(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        #Setup Window
        super(AnimPublsihGuide, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        self.ui = load.setup_ui_maya(uiFile, parent)
        self.setCentralWidget(self.ui)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.setObjectName(uiName)

        # init stuffs
        self.init_signals()

    def init_signals(self): 
        # self.ui.runPlayblast_pushButton.clicked.connect(self.run_playblast)
        self.ui.runGeoCache_pushButton.clicked.connect(partial(self.run_export, 'geo'))
        self.ui.switchYeti_pushButton.clicked.connect(self.switch_yeti)
        self.ui.runYetiCache_pushButton.clicked.connect(partial(self.run_export, 'yeti'))
        self.ui.qcOutput_pushButton.clicked.connect(self.build_qc)
        self.ui.playblastFarm_pushButton.clicked.connect(self.run_playblast)

    def run_playblast(self): 
        from rftool.scene.playblast import pb_app
        reload(pb_app)
        myApp = pb_app.show()

    def run_export(self, preset): 
        if preset == 'geo': 
            presetExport = OrderedDict([('shot_workspace', ['*']), ('camera', ['maya', 'cache']), ('cache', ['*']), ('export_grp', ['*']), ('set_asset_list', ['*']), ('export_set', ['*']), ('shotdress', ['*'])])
        
        if preset == 'yeti': 
            presetExport = OrderedDict([('yeti_groom', ['*'])])
        
        from rf_app.publish.scene import app
        reload(app)
        reload(app.context_info)
        reload(app.context_info.project_info)
        reload(app.output_utils)
        reload(app.register_shot)
        self.exportApp = app.show()
        # override export presets 
        self.exportApp.filterMods = presetExport
        self.exportApp.refresh_shot_list()

        # auto select shot item
        widgetItems = self.exportApp.ui.shotWidget.widget_items()
        if len(widgetItems) == 1: 
            widgetItems[0].set_checked(True)


    def switch_yeti(self): 
        switchDict = self.get_hair_still()
        message = self.check_all_asset_ready(switchDict)
        switch = True
        if message: 
            result = mc.confirmDialog( title='Warning', message=message, button=['Continue Switch','Cancel'])
            switch = True if result == 'Continue Switch' else False 
                
        if switch: 
            # switch
            for name, data in switchDict.iteritems(): 
                dstPath = data['hairStill']
                rnNode = data['rnNode']

                if os.path.exists(dstPath): 
                    mc.file(dstPath, loadReference=rnNode, prompt=False)

    def check_all_asset_ready(self, switchDict): 
        # check if all hair still exists 
        result = []
        missingAssets = []
        validAssets = []
        message = ''
        for name, data in switchDict.iteritems(): 
            missingAssets.append(name) if not data['status'] else validAssets.append(name)

        print ('\n- ').join(missingAssets)
        if missingAssets: 
            message = '%s/%s Hair assets not ready Cache' % (len(missingAssets), len(switchDict.keys()))
            message += '\n- %s\n' % ('\n- ').join(missingAssets)
            message += '\nContact P.Tai or P.Nong'

        return message

    def get_hair_still(self): 
        scene = context_info.ContextPathInfo()
        refPaths = pipeline_utils.collect_asset_by_path(scene)
        mapDict = dict()

        for refPath in refPaths: 
            # rnNode 
            rnNode = mc.referenceQuery(refPath, referenceNode=True)
            asset = context_info.ContextPathInfo(path=refPath)
            asset.context.update(res='md')
            name = asset.library(key=context_info.Lib.rigGrm)
            heroPath = asset.path.hero().abs_path()
            switchPath = '%s/%s' % (heroPath, name)
            mapDict[asset.name] = {'asset': asset, 'rnNode': rnNode, 'hairStill': switchPath, 'status': os.path.exists(switchPath)}

        return mapDict

    def build_qc(self): 
        scene = context_info.ContextPathInfo()
        scene.context.update(process='qc')
        path = scene.path.workspace().abs_path() 
        workfile = scene.work_name()
        savePath = '%s/%s' % (path, workfile)
        message = 'QC need to leave this scene. Continue?'
        result = mc.confirmDialog( title='Warning', message=message, button=['Yes','Cancel'])

        if result == 'Yes': 
            if not os.path.exists(path): 
                os.makedirs(path)
            mc.file(new=True, f=True)
            mc.file(rename=savePath)
            mc.file(save=True, f=True)
            self.build_tool()

    def build_tool(self): 
        from rf_app.scene.builder import app
        reload(app)
        app.show()


    def send_playblast(self): 
        from rftool.scene.playblast import pb_app
        reload(pb_app)
        myApp = pb_app.show() 


def show():
    from rftool.utils.ui import maya_win
    logger.info('Run in Maya\n')
    maya_win.deleteUI(uiName)
    myApp = AnimPublsihGuide(maya_win.getMayaWindow())
    myApp.show()
    return myApp
