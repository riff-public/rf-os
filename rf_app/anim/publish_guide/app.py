#!/usr/bin/env python
# -- coding: utf-8 --

# v.0.0.1 polytag switcher
_title = 'Anim Publish Guide'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'AnimPublishGuideUI'

#Import python modules
import sys
import os 
import getpass
from collections import OrderedDict
from functools import partial
import subprocess

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]


# framework modules 
import rf_config as config
from rf_utils import log_utils
from rf_utils.ui import load
user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'

logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import maya.cmds as mc 
from rf_utils.context import context_info
from rftool.utils import pipeline_utils
from rf_maya.lib import sequencer_lib
from rf_utils import file_utils
reload(pipeline_utils)
reload(context_info)



class AnimPublsihGuide(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        #Setup Window
        super(AnimPublsihGuide, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        self.ui = load.setup_ui_maya(uiFile, parent)
        self.setCentralWidget(self.ui)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.setObjectName(uiName)

        # init stuffs
        self.init_signals()
        self.ui.runYetiCache_pushButton.setVisible(False)
        self.ui.switchYeti_pushButton.setVisible(False)

    def init_signals(self): 
        # self.ui.runPlayblast_pushButton.clicked.connect(self.run_playblast)
        self.ui.runGeoCache_pushButton.clicked.connect(partial(self.run_export, 'all'))
        self.ui.switchYeti_pushButton.clicked.connect(self.switch_yeti)
        self.ui.runYetiCache_pushButton.clicked.connect(partial(self.run_export, 'yeti'))
        self.ui.qcOutput_pushButton.clicked.connect(self.build_qc)
        self.ui.playblastFarm_pushButton.clicked.connect(self.run_playblast)
        self.ui.cacheAprv_pushButton.clicked.connect(partial(self.cache_status, 'apr'))
        self.ui.cacheFailed_pushButton.clicked.connect(partial(self.cache_status, 'pblm'))

    def run_playblast(self): 
        """ send playblast to queue """ 
        # expected arguments 
        # path P:/SevenChickMovie/scene/work/dev/scm_dev_canis_dialog/anim/main/maya/scm_dev_canis_dialog_anim_main.v002.pipe.ma
        # camera dialog_cam:cam
        # dst P:/SevenChickMovie/scene/work/dev/scm_dev_canis_dialog/anim/main/output/scm_dev_canis_dialog_anim_main.v002.wav
        # shotName dialog
        # filename scm_dev_canis_dialog_anim_main.v002.####.png
        # scene SevenChickMovie::scene:dev:canis:scm_dev_canis_dialog:anim:main:maya:scm_dev_canis_dialog_anim_main.v002.pipe.ma:work:v002:::v002:dialog:scm:none:none
        

        # context 
        return 
        scene = context_info.ContextPathInfo()
        scene.context.update(publishVersion=scene.version)

        # args 
        mayaScene = mc.file(q=True, sn=True)
        startFrame, endFrame, seqStartFrame, seqEndFrame = sequencer_lib.shot_info(scene.name_code)
        camera = sequencer_lib.camera_info(scene.name_code)
        shotName = scene.name_code
        dst = scene.path.scheme(key='outputImgPath').abs_path()
        filename = scene.output_name(outputKey='playblastImg')
        audioName = scene.publish_name(ext='.wav')
        fcl = mc.getAttr('%s.focalLength' % camera)
        taskName = 'cache'

        # fix hard coded
        currentEnv = os.environ["PYTHONPATH"]
        from rf_utils.deadline import playblast_queue
        playblast_queue.playblast(mayaScene, int(startFrame), int(endFrame), camera, dst, shotName, filename, audioName, scene, task_name="cache", sg_upload=True, focalLength=fcl)
        os.environ["PYTHONPATH"] = currentEnv

        # from rftool.scene.playblast import pb_app
        # reload(pb_app)
        # myApp = pb_app.show()

    def cache_status(self, status): 
        """ module for export playblast """
        from rf_utils.sg import sg_utils
        sg = sg_utils.sg
        taskName = 'cache'
        scene = context_info.ContextPathInfo()
        scene.context.use_sg(sg=sg, project=scene.project, entityType=scene.entity_type, entityName=scene.name)
        filters = [['project.Project.name', 'is', scene.project], 
                    ['entity', 'is', {'type': 'Shot', 'id': int(scene.id)}], 
                    ['content', 'is', taskName]]
        fields = ['content', 'id']
        taskEntity = sg.find_one('Task', filters, fields)
        data = {'sg_status_list': status}
        result = sg.update('Task', taskEntity['id'], data)
        
        message = 'Update status to %s' % status if result else 'Failed to update status'
        resultStatus = True if result else False 
        
        mc.confirmDialog( title='Status', message=message, button=['OK'])
        return resultStatus, message


    def run_export(self, preset): 
        from rf_app.publish.scene import app
        reload(app)
        reload(app.context_info)
        reload(app.context_info.project_info)
        reload(app.output_utils)
        reload(app.register_shot)
        self.exportApp = app.show()

        if preset == 'geo': 
            presetExport = OrderedDict([('shot_workspace', ['*']), ('camera', ['maya', 'cache']), ('alembic_cache', ['*']), ('export_grp', ['*']), ('set_asset_list', ['*']), ('export_set', ['*']), ('shotdress', ['*'])])
        
        if preset == 'yeti': 
            presetExport = OrderedDict([('yeti_cache', ['*'])])
            self.exportApp.ui.publishCheckBox.setChecked(True)
            
        if preset == 'all': 
            presetExport = OrderedDict([('shot_workspace', ['*']), ('camera', ['maya', 'cache']), ('alembic_cache', ['*']), ('anim_mov', ['*']), ('yeti_bg', ['*']), ('export_grp', ['*']), ('set_asset_list', ['*']), ('export_set', ['*']), ('shotdress', ['*']), ('set_status', ['*'])])
        
        # override export presets 
        self.exportApp.filterMods = presetExport
        self.exportApp.refresh_shot_list()

        # auto select shot item
        widgetItems = self.exportApp.ui.shotWidget.widget_items()
        if len(widgetItems) == 1: 
            widgetItems[0].set_checked(True)

        

    def switch_yeti(self): 
        switchDict = self.get_hair_still()
        message = self.check_all_asset_ready(switchDict)
        switch = True
        if message: 
            result = mc.confirmDialog( title='Warning', message=message, button=['Continue Switch','Cancel'])
            switch = True if result == 'Continue Switch' else False 
                
        if switch: 
            # switch
            for name, data in switchDict.iteritems(): 
                dstPath = data['hairStill']
                rnNode = data['rnNode']

                if os.path.exists(dstPath): 
                    mc.file(dstPath, loadReference=rnNode, prompt=False)

    def check_all_asset_ready(self, switchDict): 
        # check if all hair still exists 
        result = []
        missingAssets = []
        validAssets = []
        message = ''
        for name, data in switchDict.iteritems(): 
            missingAssets.append(name) if not data['status'] else validAssets.append(name)

        print ('\n- ').join(missingAssets)
        if missingAssets: 
            message = '%s/%s Hair assets not ready Cache' % (len(missingAssets), len(switchDict.keys()))
            message += '\n- %s\n' % ('\n- ').join(missingAssets)
            message += '\nContact P.Tai or P.Nong'

        return message

    def get_hair_still(self): 
        scene = context_info.ContextPathInfo()
        refPaths = pipeline_utils.collect_asset_by_path(scene)
        mapDict = dict()

        for refPath in refPaths: 
            # rnNode 
            rnNode = mc.referenceQuery(refPath, referenceNode=True)
            asset = context_info.ContextPathInfo(path=refPath)
            asset.context.update(res='md')
            name = asset.library(key=context_info.Lib.rigGrm)
            heroPath = asset.path.hero().abs_path()
            switchPath = '%s/%s' % (heroPath, name)
            mapDict[asset.name] = {'asset': asset, 'rnNode': rnNode, 'hairStill': switchPath, 'status': os.path.exists(switchPath)}

        return mapDict

    def build_qc(self): 
        scene = context_info.ContextPathInfo()
        scene.context.update(process='qc')
        path = scene.path.workspace().abs_path() 
        workfile = scene.work_name()
        savePath = '%s/%s' % (path, workfile)
        message = 'QC need to leave this scene. Continue?'
        result = mc.confirmDialog( title='Warning', message=message, button=['Yes', 'Open New Maya', 'Cancel'])

        if result == 'Yes': 
            if not os.path.exists(path): 
                os.makedirs(path)

            if os.path.exists(savePath): 
                mc.file(savePath, o=True, f=True)
            else:
                mc.file(new=True, f=True)
                mc.file(rename=savePath)
                mc.file(save=True, f=True)
            self.build_tool()

        if result == 'Open New Maya': 
            templateFile = scene.projectInfo.asset.global_asset(key='mayaTemplate')
            mayaPath = 'C:/Program Files/Autodesk/Maya%s/bin/maya.exe' % mc.about(v=True)

            if not os.path.exists(savePath): 
                file_utils.copy(templateFile, savePath)

            subprocess.Popen([mayaPath, savePath])

    def build_tool(self): 
        from rf_app.scene.builder import app
        reload(app)
        app.show()


    def send_playblast(self): 
        from rftool.scene.playblast import pb_app
        reload(pb_app)
        myApp = pb_app.show() 


def show():
    from rftool.utils.ui import maya_win
    logger.info('Run in Maya\n')
    maya_win.deleteUI(uiName)
    myApp = AnimPublsihGuide(maya_win.getMayaWindow())
    myApp.show()
    return myApp
