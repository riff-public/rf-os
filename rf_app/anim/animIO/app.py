# build-in modules
import sys, os
import shutil
import tempfile
import time
from collections import defaultdict, OrderedDict
from glob import glob
from functools import partial
from datetime import datetime

# logger
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

# maya modules
import maya.cmds as mc
import pymel.core as pm
import maya.OpenMaya as om

import rf_config as config

# UI modules
import Qt
from Qt import wrapInstance, QtGui, QtCore, QtWidgets, QtCompat

# monkey patch for Qt compatibility 
if Qt.__binding__ in ('PySide2', 'PyQt5'):
    Qt.QtWidgets.QHeaderView.setResizeMode = Qt.QtWidgets.QHeaderView.setSectionResizeMode

# custom modules
from rftool.utils.ui import maya_win
from rf_utils.ui import load as loadUi
# from rf_utils.context import context_info

# app module
import core as core
reload(core)

# --- TODO
# //- Import anim with right click and popup
# //- Export date in toolTips
# - Comment section?
# //- Make sure the tool support ZVParentMaster
# //- Set key frames on controller with no key before exporting
# //- Handle blendParent 
# //- Option to replace whole curve/ only replace anim range
# //- in/out table selection update via selection in viewport
# //- Text to remind user name when exporting
# //- reevaluation problem
# //- Flexibility in other pipeline structure
# //- move find all button to right-click menu
# //- warn user when importing if namespace exists with mismatch reference path from .anim metadata 
# //- fix selection bug on 2 files with different time range
# //- right click to reference missing asset + import anim
# //- add multiple root object config
# //- add support for eva
# //- changing multiple version combobox
# //- Rearrange version combobox ---> hero, v009, v008, ...
# //- Upgrade core
# //- Optimize
# //- Manual import/export

# v1.1 - Bug fixes for Eva, listRelatives -ad now lists all types (before it was only "transform" type)
#      - Add support for constraint referenced node
# v1.2 - Add support for moved pivots on transforms

# --- global vars
_uiName = 'animIOWin'
_appTitle = 'Animation I/O'
_version = '1.2'
_dataFolderName = 'animData'

# table font
_tableHeader_font = QtGui.QFont("Arial", 9)
_tableHeader_font.setLetterSpacing(QtGui.QFont.AbsoluteSpacing, 1)
_table_font = QtGui.QFont("Arial", 9)
_table_font.setLetterSpacing(QtGui.QFont.AbsoluteSpacing, 1)
_title_font = QtGui.QFont("Arial", 12, QtGui.QFont.Bold)
_title_font.setLetterSpacing(QtGui.QFont.AbsoluteSpacing, 1)

# colors
_in_cell_color = QtGui.QColor(80, 120, 80)
_in_cell_color_group = QtGui.QColor(140, 100, 10)
_in_cell_missing_color = QtGui.QColor(140, 20, 20)
_out_cell_color = QtGui.QColor(110, 80, 60)

# file naming
_defaultShotName = 'animScene'
_defaultUserName = 'defaultuser'

# directories
_moduleDir = os.path.dirname(sys.modules[__name__].__file__).replace('\\', '/')
_uiFile = '%s/ui.ui' %_moduleDir
_home = os.environ['HOME'].replace('\\', '/')
_appDir = '%s/animIO' %_home
_tempDir = '%s/.animIO_tmp' %_home
_ext = 'ma'

# load UI
form_class, base_class = loadUi.loadUiType(_uiFile)

# pose global vars
POSE_ATTR_TYPES = ('float', 'double', 'double3', 'long', 'doubleLinear', 'doubleAngle', 'bool')

class ImportTabWidget(QtWidgets.QWidget):
    ''' Sub-class QWidget for right click in IN table area '''
    def __init__(self, parent=None):
        super(ImportTabWidget, self).__init__(parent)
        self.parent = parent

        # right click menu
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.generateMenu)

    def generateMenu(self, pos):
        tableWidget = self.findChildren(QtWidgets.QTableWidget)[0]
        gPos = QtGui.QCursor.pos()
        tablePos = tableWidget.viewport().mapFromGlobal(gPos)

        item = tableWidget.itemAt(tablePos)
        if not item:
            return

        curr_row = item.row()
        name = str(tableWidget.item(curr_row, 0).text())

        rightClickMenu = QtWidgets.QMenu(self)
        # import to selection
        importToSelAction = QtWidgets.QAction('Import to selection...', self)
        importToSelAction.triggered.connect(lambda: self.parent.importToSelection(curr_row))
        rightClickMenu.addAction(importToSelAction)

        if not self.parent.convertToRoot(namespace=name):
            # create reference
            createRefAction = QtWidgets.QAction('Build into scene...', self)
            createRefAction.triggered.connect(lambda: self.parent.buildToScene())
            rightClickMenu.addAction(createRefAction)

        rightClickMenu.exec_(self.mapToGlobal(pos))

class VersionComboBox(QtWidgets.QComboBox):
    ''' Sub-class QComboBox for export date tool tip pop-up in version IN comboBox '''
    def __init__(self, filePaths, parent=None):
        super(VersionComboBox, self).__init__(parent)
        self.filePaths = filePaths

    def event(self, event):
        if event.type() == QtCore.QEvent.ToolTip:
            file_path = self.filePaths[self.currentIndex()]
            
            # time ago
            modification_time = os.path.getmtime(file_path)
            dt = datetime.fromtimestamp(modification_time)
            timeAgo = pretty_date(dt)

            # local time
            local_time = time.ctime(modification_time)

            self.setToolTip('%s, %s' %(timeAgo, local_time))
        return QtWidgets.QComboBox.event(self, event)

class ImportTable(QtWidgets.QTableWidget):
    ''' Sub-class QTableWidget for blocking object select signal when right-click '''
    def __init__(self, parent=None):
        super(ImportTable, self).__init__(parent)

    def mousePressEvent(self, event):
        buttonPressed = event.button()
        if buttonPressed == QtCore.Qt.MouseButton.LeftButton:
            return super(ImportTable, self).mousePressEvent(event)
        elif buttonPressed == QtCore.Qt.MouseButton.RightButton:
            gPos = QtGui.QCursor.pos()
            tablePos = self.viewport().mapFromGlobal(gPos)
            item = self.itemAt(tablePos)
            if item:
                self.blockSignals(True)
                index = item.row()
                self.selectRow(index)
                self.blockSignals(False)

class ExportToTimeRangeDialog(QtWidgets.QDialog):
    def __init__(self, parent=None, start=1, end=10):
        super(ExportToTimeRangeDialog, self).__init__(parent)

        self.verticalLayout = QtWidgets.QVBoxLayout(self)
        self.verticalLayout.setSpacing(12)
        self.verticalLayout.setContentsMargins(24, 6, 24, 12)
        
        self.timeRange_label = QtWidgets.QLabel('Time range?')
        self.verticalLayout.addWidget(self.timeRange_label)
        
        self.startEnd_gridLayout = QtWidgets.QGridLayout()
        self.startEnd_gridLayout.setSpacing(6)

        intValidator = QtGui.QIntValidator()
        self.startFrame_lineEdit = QtWidgets.QLineEdit(str(start))
        self.startFrame_lineEdit.setAlignment(QtCore.Qt.AlignCenter)
        self.startFrame_lineEdit.setValidator(intValidator)
        self.startEnd_gridLayout.addWidget(self.startFrame_lineEdit, 0, 1, 1, 1)
        
        self.endFrame_lineEdit = QtWidgets.QLineEdit(str(end))
        self.endFrame_lineEdit.setAlignment(QtCore.Qt.AlignCenter)
        self.endFrame_lineEdit.setValidator(intValidator)
        self.startEnd_gridLayout.addWidget(self.endFrame_lineEdit, 1, 1, 1, 1)
        
        self.endFrame_label = QtWidgets.QLabel('End')
        self.endFrame_label.setAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
        self.startEnd_gridLayout.addWidget(self.endFrame_label, 0, 0, 1, 1)
        
        self.startFrame_label = QtWidgets.QLabel('Start')
        self.startFrame_label.setAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
        self.startEnd_gridLayout.addWidget(self.startFrame_label, 1, 0, 1, 1)
        
        self.startEnd_gridLayout.setColumnStretch(0, 1)
        self.startEnd_gridLayout.setColumnStretch(1, 3)
        self.verticalLayout.addLayout(self.startEnd_gridLayout)
        
        self.buttons = QtWidgets.QDialogButtonBox(self)
        self.buttons.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.verticalLayout.addWidget(self.buttons)
 
        self.setWindowTitle('Export to...')
        self.setFixedSize(180, 130)
        
        # connect 
        self.buttons.accepted.connect(self.accept)
        self.buttons.rejected.connect(self.reject)

class AnimIO(form_class, base_class):
    ''' The application class '''
    def __init__(self, parent, config='default'):
        super(AnimIO, self).__init__(parent)
        self.setupUi(self)

        # --- UI tweaks
        self.setWindowTitle(QtWidgets.QApplication.translate(_uiName, 
            "{0} - v.{1}".format(_appTitle, _version), None))

        # icons
        self.app_icon_path = '%s/icons/app_icon_b.png' %_moduleDir
        self.setWindowIcon(QtGui.QIcon(self.app_icon_path))
        self.inIcon_path = '%s/icons/in_arrow.png' %_moduleDir
        self.outIcon_path = '%s/icons/out_arrow.png' %_moduleDir
        self.refreshIcon_path = '%s/icons/refresh_icon.png' %_moduleDir

        self.import_pushButton.setIcon(QtGui.QIcon(self.inIcon_path))
        self.import_pushButton.setIconSize(QtCore.QSize(75, 75))
        self.import_pushButton.setMaximumWidth(50)
        self.import_pushButton.setMaximumHeight(50)
        # self.import_pushButton.setFlat(True)
        
        self.export_pushButton.setIcon(QtGui.QIcon(self.outIcon_path))
        self.export_pushButton.setIconSize(QtCore.QSize(75, 75))
        self.export_pushButton.setMaximumWidth(50)
        self.export_pushButton.setMaximumHeight(50)
        # self.export_pushButton.setFlat(True)

        self.refresh_pushButton.setIcon(QtGui.QIcon(self.refreshIcon_path))
        self.refresh_pushButton.setMaximumWidth(30)
        self.refresh_pushButton.setMaximumHeight(30)

        # text
        self.exportTxt_label.setFont(_title_font)

        # out table & header
        # self.out_tableWidget.setFont(_table_font)
        out_header = self.out_tableWidget.horizontalHeader()
        out_header.setFont(_tableHeader_font)
        out_header.setResizeMode(0, QtWidgets.QHeaderView.Stretch)

        # --- UI connections
        # button
        self.addOutRow_pushButton.clicked.connect(self.addOutClicked)
        self.removeOutRow_pushButton.clicked.connect(self.removeOutTable)
        self.import_pushButton.clicked.connect(self.importSelected)
        self.export_pushButton.clicked.connect(self.exportSelected)
        self.refresh_pushButton.clicked.connect(self.restart)

        # table
        self.out_tableWidget.itemSelectionChanged.connect(self.selectOutObj)
        self.out_tableWidget.cellChanged.connect(self.outCellChanged)
        self.out_tableWidget.itemDoubleClicked.connect(self.outCellDoubleClicked)

        # action
        self.interTableSel_action.toggled.connect(self.interactiveTableSelectionChecked)
        self.importFrom_action.triggered.connect(self.importFrom)
        self.exportTo_action.triggered.connect(self.exportTo)

        # right click menus
        self.out_tableWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.out_tableWidget.customContextMenuRequested.connect(self.outTableRightClicked)
        
        # show the UI
        self.show()

        # --- class vars
        self.config = config

        # tweakable
        self.shotName = _defaultShotName
        self.userName = _defaultUserName
        self.inDirs = []
        self.outDir = ''
        self.export_default_names = []
        self.ref_path_filters = []

        # internal
        self.out_objs = []
        self.out_names = []
        self.scriptJobs = []
        self.old_out_value = ''
        self._selectedFromTable = False

        # --- init function calls
        self.manageTemp()
        self.setupScriptJob()
        self.restart()

    def manageTemp(self):
        if not os.path.exists(_tempDir):
            os.mkdir(_tempDir)
            return
        for f in os.listdir(_tempDir):
            fp = '%s/%s' %(_tempDir, f)
            try:
                os.remove(fp)
                # print 'Temp removed: %s' %fp
            except:
                pass

    def getSelection(self):
        # get selection
        sels = pm.selected(type='transform')
        if not sels:
            err_msg = 'No selection found!'
            self.messageBox(text=err_msg, title=_appTitle, icon=QtWidgets.QMessageBox.Critical)
            logger.error(err_msg)
            
        return sels

    def importFrom(self):
        sels = self.getSelection()
        if not sels:
            return

        namespace = sels[0].namespace()
        if namespace:
            namespace = namespace[:-1]
        obj = sels[0]
        objName = obj.shortName()

        dialog = QtWidgets.QFileDialog()
        dialog.setWindowTitle('Import from...')
        dialog.setNameFilter('*.ma')
        dialog.setDefaultSuffix('ma')
        dialog.setDirectory(self.inDirs[0])
        dialog.setAcceptMode(QtWidgets.QFileDialog.AcceptOpen)
        anim_path = None
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            anim_path = dialog.selectedFiles()[0]
            if os.path.exists(anim_path):
                animData = core.AnimData(name=namespace, path=anim_path)
                animAsset = core.AnimAsset(root=objName)
                replaceCompletely = self.importReplace_action.isChecked()
                res = animAsset.import_anim(data=animData, replaceCompletely=replaceCompletely)
            else:
                logger.error('Anim path does not exists: %s' %anim_path)

            pm.select(obj, r=True)

            # display to user
            info = 'Imported: '
            if not res:
                info = 'Failed: '
                txt = 'Animation import has failed on:\n%s' %(objName)
                icon = QtWidgets.QMessageBox.Critical
                self.messageBox(text=txt, title=_appTitle, icon=icon)

            logger.info('%sobj=%s, path=%s' %(info, obj, anim_path))

    def exportTo(self):
        sels = self.getSelection()
        if not sels:
            return

        namespace = sels[0].namespace()
        if namespace:
            namespace = namespace[:-1]
        obj = sels[0]
        objName = obj.shortName()

        # pop-up for time range
        start = int(pm.playbackOptions(q=True, min=True))
        end = int(pm.playbackOptions(q=True, max=True))
        timeDialog = ExportToTimeRangeDialog(start=start, end=end)
        if timeDialog.exec_() == QtWidgets.QDialog.Accepted:
            start = int(timeDialog.startFrame_lineEdit.text())
            end = int(timeDialog.endFrame_lineEdit.text())
        else:
            return

        # pop-up for file browse

        dialog = QtWidgets.QFileDialog()
        dialog.setWindowTitle('Export to...')
        dialog.setNameFilter('*.ma')
        dialog.setDefaultSuffix('ma')
        dialog.setDirectory(self.outDir)
        dialog.setAcceptMode(QtWidgets.QFileDialog.AcceptSave)
        export_path = None
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            export_path = dialog.selectedFiles()[0]

            # export anim
            asset = core.AnimAsset(root=objName)
            animData = asset.export_anim(name=namespace, 
                                        path=export_path, 
                                        start=start, 
                                        end=end)

            pm.select(obj, r=True)
            txt, resTxt = '', ''
            icon = None
            if animData: 
                resTxt = 'Exported: '
                txt = 'Animation data exported: %s\n%s' %(objName, time.strftime("%d %b %Y %H:%M", time.localtime()))
                icon = QtWidgets.QMessageBox.Information
            else:
                resTxt = 'Failed: '
                txt = 'Export failed for: %s' %(objName)
                icon = QtWidgets.QMessageBox.Critical
            logger.info('%sobj=%s, path=%s, start=%s, end=%s' %(resTxt, obj, export_path, start, end))
            self.messageBox(text=txt, title=_appTitle, icon=icon)

    def buildToScene(self):
        indices, tableWidget = self.getSelectedInRow()

        for index in indices:
            # find anim path
            name = str(tableWidget.item(index, 0).text())
            start = int(tableWidget.item(index, 1).text())
            end = int(tableWidget.item(index, 2).text())
            verCBox = tableWidget.cellWidget(index, 3)
            version = verCBox.currentText()

            inDir, userName = self.getInDir()
            obj = self.convertToRoot(namespace=name, warn=False)
            if obj:
                logger.warning('Skipping, asset already exists: %s' %name)
                continue

            fn = '%s.%s.%s.%s_%s.%s.%s' %(self.shotName, userName, name, start, end, version, _ext)
            anim_path = '%s/%s' %(inDir, fn)

            if os.path.exists(anim_path):
                metadata = core.readMetaData(anim_path)
                if 'ref_path' in metadata:
                    ref_path = metadata['ref_path']
                    if os.path.exists(ref_path):
                        pm.createReference(ref_path, namespace=name)

                        # import anim
                        obj = self.convertToRoot(namespace=name)
                        if obj:
                            # select and import to selection
                            pm.select(obj, r=True)
                            self.importToSelection(index=index)
                    else:
                        logger.error('Asset path does not exists: %s' %ref_path)
                else:
                    logger.error('No reference path found in anim metadata: %s' %anim_path)
            else:
                logger.error('Animation path does not exist: %s' %anim_path)

        # refresh UI
        oldIndex = self.user_tabWidget.currentIndex()
        self.createInTabs()
        if oldIndex < self.user_tabWidget.count():
            self.user_tabWidget.setCurrentIndex(oldIndex)

    def outTableRightClicked(self, pos):
        tablePos = self.out_tableWidget.viewport().mapToGlobal(pos)
        rightClickMenu = QtWidgets.QMenu(self.out_tableWidget)

        findAllAction = QtWidgets.QAction('Find all...', self.out_tableWidget)
        findAllAction.triggered.connect(lambda: self.populateOutTable())

        clearTableAction = QtWidgets.QAction('Clear', self.out_tableWidget)
        clearTableAction.triggered.connect(lambda: self.clearOutTable())
        
        rightClickMenu.addAction(findAllAction)
        rightClickMenu.addAction(clearTableAction)

        rightClickMenu.exec_(tablePos)
        
    def outCellDoubleClicked(self, item):
        self.old_out_value = str(item.text())

    def outCellChanged(self, row, column):
        item = self.out_tableWidget.item(row, column)
        value = str(item.text())
        if column == 0:  # name change
            # check for same name
            if value in self.out_names:
                # revert to old name
                logger.warning('Same name exists in export list: %s' %value)
                self.out_tableWidget.blockSignals(True)
                item.setText(self.out_names[row])
                self.out_tableWidget.blockSignals(False)
            else:
                self.out_names[row] = value
        elif column == 1:  # start
            if not all([i.isdigit() for i in value]):
                self.out_tableWidget.blockSignals(True)
                item.setText(self.old_out_value)
                self.out_tableWidget.blockSignals(False)
            else:
                end_item = self.out_tableWidget.item(row, 2)
                end_value = int(end_item.text())
                start_value = int(value)
                if start_value > end_value:
                    logger.warning('Start frame must not be higher than end frame!')
                    self.out_tableWidget.blockSignals(True)
                    item.setText(str(end_value))
                    self.out_tableWidget.blockSignals(False)
        elif column == 2:  # end
            if not all([i.isdigit() for i in value]):
                self.out_tableWidget.blockSignals(True)
                item.setText(self.old_out_value)
                self.out_tableWidget.blockSignals(False)
            else:
                start_item = self.out_tableWidget.item(row, 2)
                start_value = int(start_item.text())
                end_value = int(start_value)
                if end_value < start_value:
                    logger.warning('End frame must not be lower than start frame!')
                    self.out_tableWidget.blockSignals(True)
                    item.setText(str(start_value))
                    self.out_tableWidget.blockSignals(False)
    
    def restart(self):
        ''' Called by scriptJob when new scene is opened '''
        # get import/export environment
        self.getEnvironment()
        self.setExportText()

        # reset UI
        self.clearOutTable()
        oldIndex = self.user_tabWidget.currentIndex()
        self.createInTabs()

        # self.updateTableSelection()
        if oldIndex < self.user_tabWidget.count():
            self.user_tabWidget.setCurrentIndex(oldIndex)

    def getEnvironment(self):
        self.outDir = ''
        self.inDirs = []
        self.userName = _defaultUserName
        self.shotName = _defaultShotName
        self.export_default_names = []
        self.ref_path_filters = []

        curr_path = pm.sceneName()   
        # --- read from config
        try:  
            config_data = core.readConfig()
            config_dict = config_data[self.config]

            # get out dir
            outDirSchema = config_dict['outDir']
            outDirs, out_elements = path_from_schema(schema=outDirSchema, path=curr_path)
            self.outDir = outDirs[0]
            if not os.path.exists(self.outDir):
                logger.info('Creating output directory: %s' %self.outDir)
                os.makedirs(self.outDir)

            inDirSchema = config_dict['inDirs']
            inDirs, in_elements = path_from_schema(schema=inDirSchema, path=curr_path)
            # only take existing input directory
            for di in inDirs:
                if os.path.exists(di):
                    self.inDirs.append(di)

            # user name
            self.userName = out_elements['userName']
            # shot name
            self.shotName = out_elements['shotName']
            # obj name
            self.export_default_names = config_dict['rootObj']
            # path filter
            if 'pathFilter' in config_dict:
                self.ref_path_filters = config_dict['pathFilter']

        # --- user has not save scene to a path, use home dir
        except Exception, e:
            logger.warning('Invalid directory structure, using home...')
            logger.error(e)
            dataDir = '%s/%s/%s' %(_appDir, _defaultUserName, _dataFolderName)
            if not os.path.exists(dataDir):
                logger.info('Creating default directory: %s' %dataDir)
                os.makedirs(dataDir)
            self.inDirs = [dataDir]
            self.outDir = dataDir 
        
        logger.info('Shot name: %s' %(self.shotName))
        logger.info('User name: %s' %(self.userName))
        logger.info('Input directories: %s' %(self.inDirs))
        logger.info('Output directory: %s' %(self.outDir))
    
    def interactiveTableSelectionChecked(self):
        # check if option is enabled
        if not self.interTableSel_action.isChecked() and self.selChangedJobId:
            pm.scriptJob(kill=self.selChangedJobId, force=True)
            self.selChangedJobId = None
        else:
            self.selChangedJobId = pm.scriptJob(e=('SelectionChanged', self.updateTableSelection))

    def setupScriptJob(self):
        self.newSceneJobID = pm.scriptJob(e=('NewSceneOpened', self.restart))
        self.sceneOpenedJobID = pm.scriptJob(e=('SceneOpened', self.restart))
        self.sceneSavedJobId = pm.scriptJob(e=('SceneSaved', self.restart))
        self.selChangedJobId = pm.scriptJob(e=('SelectionChanged', self.updateTableSelection))

    def updateTableSelection(self):
        if self._selectedFromTable:
            self._selectedFromTable = False
            return

        # get selection using API for speed
        msel = om.MSelectionList()
        om.MGlobal.getActiveSelectionList(msel)

        selIt = om.MItSelectionList(msel)
        namespaces = []
        while not selIt.isDone():
            mobj = om.MObject()
            selIt.getDependNode(mobj)
            apiType = mobj.apiType()
            if apiType in (110, 121):  # it's a transform/joint
                mdag = om.MDagPath()
                selIt.getDagPath(mdag)
                shortName = mdag.partialPathName()
                name = shortName.split('|')[-1]
                nssplits = name.split(':')
                if len(nssplits) > 1:
                    namespace = ':'.join(nssplits[:-1])
                    namespaces.append(namespace)
            selIt.next()

        # check if we're in IN or OUT tab
        # --- update IN tables
        for ti in xrange(self.user_tabWidget.count()):
            curr_widget = self.user_tabWidget.widget(ti)
            tableWidget = curr_widget.findChildren(QtWidgets.QTableWidget)[0]
            # prev_indices, tableWidget = self.getSelectedInRow()
            # curr_widget = self.user_tabWidget.currentWidget()
            if tableWidget:
                tableWidget.blockSignals(True)
                # no selection, return
                # if not msel.length():

                tableWidget.clearSelection()
                # else:
                # select some rows...
                # names = []
                tableWidget.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
                for index in xrange(tableWidget.rowCount()):
                    nameItem = tableWidget.item(index, 0)
                    name = str(nameItem.text())

                    # startItem = tableWidget.item(index, 1)
                    # endItem = tableWidget.item(index, 2)
                    # verItem = tableWidget.item(index, 3)
                    # if name in namespaces:
                    #     nameItem.setSelected(True)
                    #     startItem.setSelected(True)
                    #     endItem.setSelected(True)
                    #     verItem.setSelected(True)
                    if name in namespaces:
                        tableWidget.selectRow(index)

                tableWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
                tableWidget.blockSignals(False)
        
        # --- update OUT table
        self.out_tableWidget.blockSignals(True)
        # no selection, return
        # if not msel.length():
        self.out_tableWidget.clearSelection()
        # else:
        # select some rows...
        self.out_tableWidget.blockSignals(True)
        for index in xrange(self.out_tableWidget.rowCount()):
            nameItem = self.out_tableWidget.item(index, 0)
            name = str(nameItem.text())
            startItem = self.out_tableWidget.item(index, 1)
            endItem = self.out_tableWidget.item(index, 2)
            if name in namespaces:
                nameItem.setSelected(True)
                startItem.setSelected(True)
                endItem.setSelected(True)
        self.out_tableWidget.blockSignals(False)

    def closeEvent(self, event):
        logger.info('Closing %s' %_appTitle)
        for jid in [self.newSceneJobID, self.sceneOpenedJobID, self.sceneSavedJobId, self.selChangedJobId]:
            if jid and pm.scriptJob(ex=jid):
                pm.scriptJob(kill=jid, force=True)

    def setExportText(self):
        self.exportTxt_label.setText(self.userName)

    def createInTabs(self):
        self.user_tabWidget.blockSignals(True)
        self.user_tabWidget.clear()
        for path in self.inDirs:
            user_path = os.path.dirname(path)
            user = os.path.basename(user_path)
            data = self.getInData(path, user)
            widget, tableWidget = self.createInTable()
            self.fillInTable(tableWidget, data)
            self.user_tabWidget.addTab(widget, user)

        self.user_tabWidget.blockSignals(False)

    def getInData(self, path, user):
        files = glob('%s/%s.%s.*.*_*.*.%s' %(path, self.shotName, user, _ext))
        files = [f.replace('\\', '/') for f in files]
        datas = OrderedDict()  # {name:{range:[(ver, path)]}}

        for path in files:
            basename = os.path.basename(path)
            fn, ext = os.path.splitext(basename)
            splits = fn.split('.')

            name = splits[2]

            rangeStr = splits[3]
            rangeSplits = rangeStr.split('_')
            start = rangeSplits[0]
            end = rangeSplits[1]

            version = splits[4]
            versionPath = (version, path)
            if name not in datas:
                datas[name] = OrderedDict()
            if (start, end) not in datas[name]:
                datas[name][(start, end)] = []
            datas[name][(start, end)].append(versionPath)

        return datas

    def createInTable(self):
        # --- create widget
        widget = ImportTabWidget(parent=self)
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.setContentsMargins(0, 0, 0, 0)
        widget.setLayout(mainLayout)

        # --- create table
        tableWidget = ImportTable()
        tableWidget.setFrameShape(QtWidgets.QFrame.NoFrame)
        tableWidget.setFrameShadow(QtWidgets.QFrame.Plain)
        tableWidget.setDragDropMode(QtWidgets.QAbstractItemView.NoDragDrop)
        tableWidget.setDefaultDropAction(QtCore.Qt.IgnoreAction)
        tableWidget.setAlternatingRowColors(True)
        tableWidget.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        tableWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        tableWidget.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        tableWidget.setGridStyle(QtCore.Qt.DotLine)
        tableWidget.setLineWidth(2)
        tableWidget.setColumnCount(4)
        tableWidget.setHorizontalHeaderLabels(['Name', 'Start', 'End', 'Version'])
        tableWidget.itemClicked.connect(self.selectInObj)
        tableWidget.itemSelectionChanged.connect(self.selectInObj)

        # headers
        header = tableWidget.horizontalHeader()
        header.setResizeMode(0, QtWidgets.QHeaderView.Stretch)
        header.setResizeMode(3, QtWidgets.QHeaderView.Fixed)
        header.setCascadingSectionResizes(False)
        header.setDefaultSectionSize(100)
        header.setHighlightSections(False)
        header.setMinimumSectionSize(30)
        header.setStretchLastSection(False)
        header.setFont(_tableHeader_font)
        tableWidget.verticalHeader().setVisible(False)
        mainLayout.addWidget(tableWidget, 0)

        return widget, tableWidget

    def fillInTable(self, tableWidget, data):
        # --- fill tables
        ri = 0
        i = 0

        # {name:{range:[(ver, path)]}}
        for name, ranges in data.iteritems():
            obj = self.convertToRoot(namespace=name, warn=False)
            if obj:
                if len(ranges) > 1:
                    cell_color = _in_cell_color_group
                else:
                    cell_color = _in_cell_color
                # get cell color
                if i % 2 > 0:
                    cell_color = cell_color.darker(135)
            else:
                cell_color = _in_cell_missing_color

            for frange, versionPaths in ranges.iteritems():
                tableWidget.insertRow(ri)
                 # name
                nameItem = QtWidgets.QTableWidgetItem(name)
                tableWidget.setItem(ri, 0, nameItem)

                start = frange[0]
                end = frange[1]
                # start
                startItem = QtWidgets.QTableWidgetItem(str(start))
                startItem.setTextAlignment(QtCore.Qt.AlignCenter)

                # end
                endItem = QtWidgets.QTableWidgetItem(str(end))
                endItem.setTextAlignment(QtCore.Qt.AlignCenter)

                # add version item
                versionItem =  QtWidgets.QTableWidgetItem()
                versionItem.setFlags(~QtCore.Qt.ItemIsSelectable)

                # set items to table
                tableWidget.setItem(ri, 1, startItem)
                tableWidget.setItem(ri, 2, endItem)
                tableWidget.setItem(ri, 3, versionItem)

                nameItem.setBackground(cell_color)
                startItem.setBackground(cell_color)
                endItem.setBackground(cell_color)

                versions = [v[0] for v in versionPaths]
                paths = [v[1] for v in versionPaths]

                # rearrange version and path, hero, v009, v008 and so on
                arranged_versions, arranged_paths = [], []
                for ver, path in zip(versions[::-1], paths[::-1]):
                    if ver == 'hero':
                        arranged_versions.insert(0, ver)
                        arranged_paths.insert(0, path)
                    else:
                        arranged_versions.append(ver)
                        arranged_paths.append(path)

                # version comboBox
                cbox = VersionComboBox(filePaths=arranged_paths)
                cbox.addItems(arranged_versions)
                cbox.setFocusPolicy(QtCore.Qt.NoFocus)
                cbox.currentIndexChanged.connect(self.versionChanged)
                tableWidget.setCellWidget(ri, 3, cbox)
                ri += 1
            i += 1

    def versionChanged(self):
        selRows, tableWidget = self.getSelectedInRow()
        if len(selRows) < 2:
            return
        cbox = self.sender()
        index  = tableWidget.indexAt(cbox.pos()).row()
        if index not in selRows:
            return

        currVer = str(cbox.currentText())
        for i in selRows:
            verCBox = tableWidget.cellWidget(i, 3)
            verIndx = verCBox.findText(currVer)
            if verIndx != -1:
                verCBox.blockSignals(True)
                verCBox.setCurrentIndex(verIndx)
                verCBox.blockSignals(False)

    def selectOutObj(self):
        indexes = self.getSelectedOutRow()
        if not indexes:
            pm.select(cl=True)
            return

        selections = set()
        for index in indexes:
            selections.add(self.out_objs[index])
        if selections:
            try:
                # logger.info(selections)
                pm.select(list(selections), r=True)
            except pm.MayaNodeError:
                logger.error('Some node does not exists: %s' %selections)

    def selectInObj(self):
        selRowIndices, tableWidget = self.getSelectedInRow()
        if not selRowIndices:
            pm.select(cl=True)
            return

        tabIndex = self.user_tabWidget.currentIndex()
        selections = set()
        names = []
        for index in selRowIndices:
            nameItem = tableWidget.item(index, 0)
            ns = str(nameItem.text())
            obj = self.convertToRoot(namespace=ns)
            if obj:
                selections.add(obj)

            start = int(tableWidget.item(index, 1).text())
            end = int(tableWidget.item(index, 2).text())
            names.append((ns, start, end))

        # select other tab too
        for ti in xrange(self.user_tabWidget.count()):
            if ti == tabIndex:
                continue
            widget = self.user_tabWidget.widget(ti)
            otherTable = widget.findChildren(QtWidgets.QTableWidget)[0]
            otherTable.blockSignals(True)
            otherTable.clearSelection()

            for info in names:
                selName = info[0]
                selStart = info[1]
                selEnd = info[2]
                for ri in xrange(otherTable.rowCount()):
                    otherNameItem = otherTable.item(ri, 0)
                    other_name = str(otherNameItem.text())

                    otherStartItem = otherTable.item(ri, 1)
                    other_start = int(otherStartItem.text())

                    otherEndItem = otherTable.item(ri, 2)
                    other_end = int(otherEndItem.text())
                    if other_name == selName and other_start == selStart and other_end == selEnd:
                        otherNameItem.setSelected(True)
                        otherStartItem.setSelected(True)
                        otherEndItem.setSelected(True)

            otherTable.blockSignals(False)

        self._selectedFromTable = True
        if selections:
            try:
                #logger.info(selections)
                pm.select(list(selections), r=True)
                
            except pm.MayaNodeError:
                logger.error('Some node does not exists: %s' %selections)
        else:
            pm.select(cl=True)
            return

    def addOutClicked(self):
        sels = pm.selected()
        for sel in sels:
            if not isinstance(sel, pm.nt.Transform):
                logger.error('Invalid object type: %s' %sel)
                continue
            name = sel.namespace()[:-1]
            if name:
                obj = self.convertToRoot(namespace=name)
                if not obj:
                    obj = sel
            else:
                name = sel.nodeName()
                obj = sel
            
            self.addOutTable(name=name, obj=obj)

    def addOutTable(self, name='', obj=None, start=None, end=None):
        ''' add a row based on selection to OUT table '''
        if start == None:
            start = pm.playbackOptions(q=True, min=True)
        if end == None:
            end = pm.playbackOptions(q=True, max=True)

        # convert start, end to int
        start = int(start)
        end = int(end)

        if name and obj and start and end:
            # check if name exists
            if name in self.out_names:
                logger.warning('Same name exists in export list: %s' %name)
                existing_index = self.out_names.index(name)
                self.out_tableWidget.selectRow(existing_index)
                self.out_tableWidget.scrollToItem(self.out_tableWidget.item(existing_index, 0))
                return

            self.out_tableWidget.blockSignals(True)
            # insert row
            rowIndex = self.out_tableWidget.rowCount()
            self.out_tableWidget.insertRow(rowIndex)

            # name
            nameItem = QtWidgets.QTableWidgetItem(name)
            nameItem.setToolTip(obj.shortName())
            # start
            startItem = QtWidgets.QTableWidgetItem(str(start))
            startItem.setTextAlignment(QtCore.Qt.AlignCenter)
            # end
            endItem = QtWidgets.QTableWidgetItem(str(end))
            endItem.setTextAlignment(QtCore.Qt.AlignCenter)

            # nameItem.setFont(_table_font)
            # startItem.setFont(_table_font)
            # endItem.setFont(_table_font)

            # set items to table
            self.out_tableWidget.setItem(rowIndex, 0, nameItem)
            self.out_tableWidget.setItem(rowIndex, 1, startItem)
            self.out_tableWidget.setItem(rowIndex, 2, endItem)

            # set color

            if rowIndex % 2 > 0:
                cell_color = _out_cell_color.darker(135)
            else:
                cell_color = _out_cell_color

            nameItem.setBackground(cell_color)
            startItem.setBackground(cell_color)
            endItem.setBackground(cell_color)

            # add to class var
            self.out_objs.append(obj)
            self.out_names.append(name)
            self.out_tableWidget.blockSignals(False)

    def removeOutTable(self):
        selModel = self.out_tableWidget.selectionModel()
        pstMdlIndices = []
        if selModel.hasSelection():
            pstMdlIndices =[QtCore.QPersistentModelIndex(index) for index in self.out_tableWidget.selectionModel().selectedRows()]
        if not pstMdlIndices:
            return
        indicies = [i.row() for i in pstMdlIndices]

        for pmi in pstMdlIndices:
            index = pmi.row()
            self.out_tableWidget.removeRow(index)

        self.out_objs = [obj for i, obj in enumerate(self.out_objs) if i not in indicies]
        self.out_names = [name for i, name in enumerate(self.out_names) if i not in indicies]

    def clearOutTable(self):
        self.out_tableWidget.clearContents()
        self.out_tableWidget.setRowCount(0)
        self.out_objs = []
        self.out_names =[]
        self.old_out_value = ''

    def populateOutTable(self):
        ''' Auto fill OUT table with existing references '''
        self.clearOutTable()
        valid_refs = [r for r in pm.listReferences() if r.namespace and r.isLoaded()]
        for ref in sorted(valid_refs, key=lambda r: r.namespace):
            for fltr in self.ref_path_filters:
                if fltr in ref.withCopyNumber():
                    ns = ref.namespace
                    obj = self.convertToRoot(namespace=ns)
                    allRefNodes = mc.referenceQuery(ref.withCopyNumber(), nodes=True)
                    if obj and allRefNodes and obj.shortName() in allRefNodes:
                        self.addOutTable(name=ns, obj=obj)
                        break

    def exportSelected(self):
        start_time = time.time()
        indexes = self.getSelectedOutRow()
        if not indexes:
            return

        pm.waitCursor(st=True)
        failed_ns = []
        for index in indexes:
            obj = self.out_objs[index]
            name = self.out_names[index]
            start = int(self.out_tableWidget.item(index, 1).text())
            end = int(self.out_tableWidget.item(index, 2).text())

            try:
                pm.PyNode(obj)
            except:
                failed_ns.append(name)
                continue

            # get path
            hero_path = self.getExportPath(name, start, end)
            # copy old hero to local
            bk_hero_path = None
            if os.path.exists(hero_path):
                bkh_handle, bk_hero_path = tempfile.mkstemp(suffix='.%s' %_ext, dir=_tempDir)
                bk_hero_path = bk_hero_path.replace('\\', '/')
                shutil.copy2(hero_path, bk_hero_path)

            # export anim
            asset = core.AnimAsset(root=obj.shortName())
            lch_handle, local_hero_path = tempfile.mkstemp(suffix='.%s' %_ext, dir=_tempDir)
            local_hero_path = local_hero_path.replace('\\', '/')
            
            animData = asset.export_anim(name=name, 
                                        path=local_hero_path, 
                                        start=start, 
                                        end=end)

            # only copy in case export suceeded
            resTxt = ''
            if animData: 
                # move backup hero from local to server as version
                if bk_hero_path:
                    backup_path = self.getVersionPath(bk_hero_path, name, start, end)
                    shutil.copy2(bk_hero_path, backup_path)
                # copy local hero to server
                shutil.copy2(local_hero_path, hero_path)
                resTxt = 'Exported: '
            else:
                failed_ns.append(name)
                resTxt = 'Failed: '

            logger.info('%sname=%s, obj=%s, path=%s, start=%s, end=%s' %(resTxt, name, obj, hero_path, start, end))

        if failed_ns:
            txt = 'Export failed for:\n  %s' %('\n  '.join(failed_ns))
            icon = QtWidgets.QMessageBox.Critical
        else:
            txt = '%s animation data exported.\n%s' %(len(indexes), time.strftime("%d %b %Y %H:%M", time.localtime()))
            icon = QtWidgets.QMessageBox.Information
        pm.waitCursor(st=False)
        self.messageBox(text=txt, title=_appTitle, icon=icon)

    def messageBox(self, text, title, icon=QtWidgets.QMessageBox.Information, buttons=[], detailedText=''):
        qmsgBox = QtWidgets.QMessageBox(self)
        qmsgBox.setText(text)
        if detailedText:
            qmsgBox.setDetailedText(detailedText)
        qmsgBox.setWindowTitle(title)
        if icon:
            qmsgBox.setIcon(icon)
        for text, role in buttons:
            qmsgBox.addButton(text, role)
        results = qmsgBox.exec_()
        return results

    def getVersionPath(self, path, name, start, end):
        backup_version = getVersion(self.outDir, self.shotName, self.userName, name, start, end)
        if not backup_version:
            return
        fn = '%s.%s.%s.%s_%s.%s.%s' %(self.shotName, self.userName, name, start, end, backup_version, _ext)
        backup_path = '%s/%s' %(self.outDir, fn)
        return backup_path

    def getExportPath(self, name, start, end):
        fn = '%s.%s.%s.%s_%s.hero.%s' %(self.shotName, self.userName, name, start, end, _ext)
        path = '%s/%s' %(self.outDir, fn)
        return path

    def getSelectedInRow(self):
        curr_widget = self.user_tabWidget.currentWidget()
        if not curr_widget:
            return [], None
        tableWidget = curr_widget.findChildren(QtWidgets.QTableWidget)[0]
        selRowIndices =[QtCore.QPersistentModelIndex(index).row() for index in tableWidget.selectionModel().selectedRows()]
        return selRowIndices, tableWidget

    def getSelectedOutRow(self):
        selModel = self.out_tableWidget.selectionModel()
        indexes = []
        if selModel.hasSelection():
            indexes =[QtCore.QPersistentModelIndex(index).row() for index in self.out_tableWidget.selectionModel().selectedRows()]
        return indexes

    def convertToRoot(self, namespace, warn=True):
        if namespace:
            namespace += ':'
        for default_name in self.export_default_names:
            grpName = '%s%s' %(namespace, default_name)
            lsObj = pm.ls(grpName, type='transform')
            numFound = len(lsObj)
            if numFound == 1:
                return lsObj[0]
            elif numFound > 1:
                allParents = [o.getAllParents() for o in lsObj]
                minParent = min(allParents)
                minIndex = allParents.index(minParent)
                return lsObj[minIndex]
        if warn:
            logger.warning('Cannot find root object: %s' %namespace)

    def getInDir(self):
        # get input directory, sanity check
        tabIndex = self.user_tabWidget.currentIndex()
        userName = str(self.user_tabWidget.tabText(tabIndex))
        inDir = self.inDirs[tabIndex]
        if not os.path.exists(inDir):
            logger.error('Path does not exists: %s' %inDor)
            return None, None

        # in case tab did not update making user name wrong
        inDirName = os.path.dirname(inDir)
        inDirBaseName = os.path.basename(inDirName)
        if inDirBaseName != userName:
            logger.error('Missing user directory: %s, %s' %(inDirBaseName, userName))
            return None, None

        return inDir, userName

    def importToSelection(self, index):
        curr_widget = self.user_tabWidget.currentWidget()
        tableWidget = curr_widget.findChildren(QtWidgets.QTableWidget)[0]
        name = str(tableWidget.item(index, 0).text())

        # get selection
        sels = self.getSelection()
        if not sels:
            return

        # try to find Rig_Grp
        namespace = sels[0].namespace()
        if namespace:
            namespace = namespace[:-1]
        obj = self.convertToRoot(namespace=namespace)  # get rid of ':' at the end of string
        if not obj:
            obj = sels[0]

        # get input directory
        inDir, userName = self.getInDir()
        if not inDir:
            return 

        pm.waitCursor(st=True)

        # get table values
        start = int(tableWidget.item(index, 1).text())
        end = int(tableWidget.item(index, 2).text())
        verCBox = tableWidget.cellWidget(index, 3)
        version = verCBox.currentText()

        # get object name
        fn = '%s.%s.%s.%s_%s.%s.%s' %(self.shotName, userName, name, start, end, version, _ext)
        anim_path = '%s/%s' %(inDir, fn)
        res = None
        if os.path.exists(anim_path):
            animData = core.AnimData(name=namespace, path=anim_path, start=start, end=end)
            # animData = core.AnimData.init_from_file(path=path)
            animAsset = core.AnimAsset(root=obj.shortName())
            replaceCompletely = self.importReplace_action.isChecked()
            res = animAsset.import_anim(data=animData, replaceCompletely=replaceCompletely)
        else:
            logger.error('Anim path does not exists: %s' %anim_path)

        pm.select(obj, r=True)
        pm.waitCursor(st=False)

        # display to user
        info = 'Imported: '
        if not res:
            info = 'Failed: '
            txt = 'Animation import has failed on:\n%s' %(','.join(failed_ns))
            icon = QtWidgets.QMessageBox.Critical
            self.messageBox(text=txt, title=_appTitle, icon=icon)

        logger.info('%sname=%s, obj=%s, path=%s, start=%s, end=%s' %(info, name, obj, anim_path, start, end))

    def importSelected(self):
        selRowIndices, tableWidget = self.getSelectedInRow()
        if not selRowIndices:
            return

        # get input directory
        inDir, userName = self.getInDir()
        if not inDir:
            return 

        replaceCompletely = self.importReplace_action.isChecked()
        failed_ns = []
        objs = []
        obj = None
        pm.waitCursor(st=True)
        for index in selRowIndices:
            # get table values
            name = str(tableWidget.item(index, 0).text())
            start = int(tableWidget.item(index, 1).text())
            end = int(tableWidget.item(index, 2).text())
            verCBox = tableWidget.cellWidget(index, 3)
            version = verCBox.currentText()

            # get object name
            fn = '%s.%s.%s.%s_%s.%s.%s' %(self.shotName, userName, name, start, end, version, _ext)
            anim_path = '%s/%s' %(inDir, fn)
            res = None
            obj = self.convertToRoot(namespace=name, warn=False)
            pathExists = os.path.exists(anim_path)
            if obj and pathExists:
                metadata = core.readMetaData(anim_path)
                animData = core.AnimData(name=name, path=anim_path, start=start, end=end, metadata=metadata)
                animAsset = core.AnimAsset(root=obj.shortName())

                # check if the path in .anim metadata matches with current object we're importing the data in to...
                if 'ref_path' in metadata and obj.isReferenced():
                    ref_path_anim = metadata['ref_path']
                    ref_path = pm.referenceQuery(obj, f=True, wcn=True)
                    if ref_path_anim != ref_path:
                        if os.path.exists(ref_path_anim):
                            err_txt = 'Mismatch reference path for: %s' %name
                            detailed_txt = 'From file: %s\nIn scene: %s' %(ref_path_anim, ref_path)
                            buttons = (('Import anyway', QtWidgets.QMessageBox.AcceptRole), 
                                    ('Replace reference', QtWidgets.QMessageBox.AcceptRole), 
                                    ('Skip', QtWidgets.QMessageBox.RejectRole))
                            icon = QtWidgets.QMessageBox.Warning
                            result = self.messageBox(text=err_txt, title=_appTitle, icon=icon, buttons=buttons, detailedText=detailed_txt)
                            if result == 1:  # Fix
                                ref = pm.FileReference(pm.referenceQuery(obj, f=True))
                                ref.replaceWith(ref_path_anim)
                                obj = self.convertToRoot(namespace=name, warn=False)
                                if obj:
                                    animAsset = core.AnimAsset(root=obj.shortName())
                                else:
                                    txt = 'Cannot find root object: %s, reverting...' %name
                                    icon = QtWidgets.QMessageBox.Critical
                                    self.messageBox(text=txt, title=_appTitle, icon=icon)
                                    ref.replaceWith(ref_path)
                                    continue
                            elif result == 2:
                                continue
                        else:
                            logger.error('Ignoring, reference path mismatch but metadata path does not exist: %s' %ref_path_anim)

                res = animAsset.import_anim(data=animData, replaceCompletely=replaceCompletely)
                objs.append(obj)
            else:
                if not obj:
                    logger.error('Cannot find root object: %s' %name)
                if not pathExists:
                    logger.error('Anim path does not exists: %s' %anim_path)

            info = 'Imported: '
            if not res:
                info = 'Failed: '
                failed_ns.append(name)
            logger.info('%sname=%s, obj=%s, path=%s, start=%s, end=%s' %(info, name, obj, anim_path, start, end))

        pm.select(objs, r=True)
        pm.waitCursor(st=False)

        # if any failure, pop up details
        if failed_ns:
            txt = 'Animation import has failed on:\n%s' %('\n'.join(failed_ns))
            icon = QtWidgets.QMessageBox.Critical
            self.messageBox(text=txt, title=_appTitle, icon=icon)

def getVersion(directory, shotName, userName, name, start, end):
    files = glob('%s/%s.%s.%s.%s_%s.*.%s' %(directory, shotName, userName, name, start, end, _ext))
    if not files:
        return
    latest_path = files[-1].replace('\\', '/')
    latest_file = os.path.basename(latest_path)
    latest_ver_str = latest_file.split('.')[4]
    if latest_ver_str == 'hero':
        return 'v001'
    else:
        latest_ver_str = latest_ver_str.replace('v', '')
        latest_ver = int(latest_ver_str)
        new_ver = latest_ver + 1
        new_ver_str = 'v%s' %(str(new_ver).zfill(3))
        return new_ver_str

def pretty_date(time=False):
    """
    Get a datetime object or a int() Epoch timestamp and return a
    pretty string like 'an hour ago', 'Yesterday', '3 months ago',
    'just now', etc
    """
    now = datetime.now()
    if type(time) is int:
        diff = now - datetime.fromtimestamp(time)
    elif isinstance(time, datetime):
        diff = now - time
    elif not time:
        diff = now - now
    second_diff = diff.seconds
    day_diff = diff.days

    if day_diff < 0:
        return ''

    if day_diff == 0:
        if second_diff < 10:
            return "just now"
        if second_diff < 60:
            return str(second_diff) + " seconds ago"
        if second_diff < 120:
            return "a minute ago"
        if second_diff < 3600:
            return str(second_diff / 60) + " minutes ago"
        if second_diff < 7200:
            return "an hour ago"
        if second_diff < 86400:
            return str(second_diff / 3600) + " hours ago"
    if day_diff == 1:
        return "Yesterday"
    if day_diff < 7:
        return str(day_diff) + " days ago"
    if day_diff < 31:
        return str(day_diff / 7) + " weeks ago"
    if day_diff < 365:
        return str(day_diff / 30) + " months ago"
    return str(day_diff / 365) + " years ago"

def path_from_schema(schema, path):
    sc_splits = schema.split('/')
    basename = os.path.basename(path)
    dirname = os.path.dirname(path)
    path_splits = dirname.split('/')
    elements = {}
    paths = []

    i = 0
    for sc, pa in zip(sc_splits[::-1], path_splits[::-1]):
        if sc.startswith('<') and sc.endswith('>'):
            elements[sc[1:-1]] = pa
            paths.append(pa)
        else:
            paths.append(sc)
        i += 1
    end_path = '/'.join(paths[::-1])
    base_path = '/'.join(path_splits[:(i*-1)])
    path_pattern = '%s/%s' %(base_path, end_path)

    if '/*/' in schema:
        user_splits = path_pattern.split('/*/')
        result_paths = [str(p.replace('\\', '/')) for p in glob('%s/*' %user_splits[0])]
        result_paths = ['%s/%s' %(e, user_splits[-1]) for e in result_paths]
    else:
        result_paths = [path_pattern]

    return result_paths, elements

def export_sdk(objs=[]):
    if not objs:
        objs = mc.ls(sl=True)
        if not objs:
            logger.error('Select objects to export SDK')
            return

    sn = pm.sceneName()
    path = None
    start_dir = ''
    if sn:
        above_dir = os.path.dirname(sn).replace('\\', '/')
        data_dir = '%s/data' %above_dir
        if not os.path.exists(data_dir):
            os.makedirs(data_dir)
        start_dir = data_dir

    dialog = QtWidgets.QFileDialog()
    dialog.setWindowTitle('Export SDK to...')
    dialog.setNameFilter('*.ma')
    dialog.setDefaultSuffix('.ma')
    if start_dir:
        dialog.setDirectory(start_dir)
    dialog.setAcceptMode(QtWidgets.QFileDialog.AcceptSave)
    if dialog.exec_() == QtWidgets.QDialog.Accepted:
        path = dialog.selectedFiles()[0]
        path = core.export_sdk(path=path, objs=objs)
        logger.info('Export SDK: %s' %path)

def import_sdk():
    sn = pm.sceneName()
    path = None
    start_dir = ''
    if sn:
        above_dir = os.path.dirname(sn).replace('\\', '/')
        data_dir = '%s/data' %above_dir
        if not os.path.exists(data_dir):
            os.makedirs(data_dir)
        start_dir = data_dir

    dialog = QtWidgets.QFileDialog()
    dialog.setWindowTitle('Import SDK from...')
    dialog.setNameFilter('*.ma')
    dialog.setDefaultSuffix('.ma')
    if start_dir:
        dialog.setDirectory(start_dir)
    dialog.setAcceptMode(QtWidgets.QFileDialog.AcceptOpen)
    if dialog.exec_() == QtWidgets.QDialog.Accepted:
        path = dialog.selectedFiles()[0]
        if os.path.exists(path):
            path = core.import_sdk(path=path)
            logger.info('Import SDK: %s' %path)

def show(config='default'):
    ''' Launching function '''
    global animIOApp
    try:
        animIOApp.close()
    except Exception, e:
        pass
    
    animIOApp = AnimIO(parent=maya_win.getMayaWindow(), config=config)
    return animIOApp


'''
# --- LAUNCH APP
from rf_app.anim.animIO import app as animIOApp
reload(animIOApp)
animIOApp.show()


# ---- POSE IMPORT/EXPORT
from rf_app.anim.animIO import app
reload(app)

# project refers to key in animIO's config.yml
project = 'default'

# export pose, RETURN pose dictionary
poses = app.export_pose(project=project,
            namespace='cara_rig_main_md_hero', 
            path='D:/test.ma')

# import pose, RETURN list of set controllers
app.import_pose(namespace='cara_rig_main_md_hero1', 
            path='D:/test.ma')

# ---- SDK IMPORT/EXPORT
from rf_app.anim.animIO import app
reload(app)

# EXPORT SDK
# select some objects and run;
app.export_sdk()

# Just run and browse for your file
app.import_sdk()

'''
print __name__