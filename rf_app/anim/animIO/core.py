import os
import sys
import re
import yaml
from collections import defaultdict, OrderedDict
import time

# logger
import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.ERROR)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc
import maya.mel as mel

_moduleDir = os.path.dirname(sys.modules[__name__].__file__).replace('\\', '/')
ANIM_CURVE_NAME = 'ANIM_CURVE'
CONSTRAINTED_ATTRS = ('tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz')
POSE_ATTR_TYPES = ('long', 'double', 'float', 'doubleLinear', 'doubleAngle', 'enum')
TEMP_NAMESPACE = 'animIOTMP_001'
ANIM_OBJ_ATTR = 'animObj'
ANIM_PIVOT_ATTR = 'animPivot'

def readConfig():
    config_path = '%s/config.yml' %(_moduleDir)
    config_data = None
    with open(config_path, 'r') as f:
        config_data = yaml.load(f)
    return config_data

def attachMetaData(path, metadata):
    data = OrderedDict()
    allowed_cmd = ('requires', 'currentUnit', 'fileInfo', 'createNode')

    # --- read the lines
    with open(path, 'r') as f:
        ascii_lines = f.readlines()
        line_chunk = []
        curr_cmd = ''

        # get the title and end comment first
        data['title_comment'] = []
        title_chunks = []
        i = 0
        for i, line in enumerate(ascii_lines):
            if line.startswith('//'):
                title_chunks.append(line)
            else:
                break
        if title_chunks:
            data['title_comment'].append(title_chunks)

        end_chunks = []
        e = 0
        for e, line in enumerate(ascii_lines[::-1]):
            if line.startswith('//'):
                end_chunks.append(line)
            else:
                break
        if e > 0:
            lines_to_loop = ascii_lines[i:(e*-1)]
        else:
            lines_to_loop = ascii_lines[i:]

        for line in lines_to_loop:
            if line == '\n':
                continue

            is_tabbed = line.startswith('\t')
            if line_chunk:
                this_line_ends = line.endswith(';\n')
                last_line_ends = line_chunk[-1].endswith(';\n')

                # if the current line is tabbed
                if is_tabbed:
                    if this_line_ends and last_line_ends:
                        line_chunk.append(line)
                    elif not this_line_ends and last_line_ends:
                        line_chunk.append(line)
                    else:
                        line_chunk[-1] += line 
                        
                else:  # the current line isn't tabbed
                    data[curr_cmd].append(line_chunk)

            # it's the start of a chunk, ie requires, setAttr, ...
            if not is_tabbed:
                try:
                    curr_cmd = line.split()[0]
                except IndexError:
                    curr_cmd = line
                if curr_cmd not in data:
                    data[curr_cmd] = []
                line_chunk = [line]
        
        if curr_cmd in allowed_cmd:
            data[curr_cmd].append(line_chunk)  # need to add the last line
        data['end_comment'] = []
        if end_chunks:
            data['end_comment'].append(end_chunks)

    # --- remove unneccessary plugins
    new_requires = []
    if 'requires' in data:
        for chunk in data['requires']:
            for line in chunk:
                if line.startswith('requires maya'):
                    new_requires.append(chunk)
    data['requires'] = new_requires

    # --- insert meta data
    data['title_comment'].insert(0, '// %s\n' %(metadata))

    # --- write lines
    result_lines = []
    for cmd, cmd_chunk in data.iteritems():
        for chunk in cmd_chunk:
            for line in chunk:
                result_lines.append(line)

    with open(path, 'w') as f:
        f.writelines(result_lines)

def readMetaData(path):
    metadata = {}
    with open(path, 'r') as f:
        for line in f.readlines():
            if line.startswith('// ') and line.endswith('\n'):
                data = line[3:-1]
                # metadata.append(data)
                exec('metadata = %s' %data)
            else:
                break
    return metadata

def removeDagPathNamespace(path):
    root = False
    if path.startswith('|'):
        root = True
        path = path[1:]
    splits = path.split('|')
    cleaned_elems = []
    for s in splits:
        if '.f[' in s:
            fSplit = s.split('.f[')
            elem = fSplit[0]
            fidx = '.f[%s' %fSplit[1]
        else:
            elem = s
            fidx = ''

        elem = '%s%s' %(elem.split(':')[-1], fidx)
        cleaned_elems.append(elem)

    result = '|'.join(cleaned_elems)
    if root:
        result = '|%s' %(result)
    return result

def addDagPathNamespace(path, namespace):
    root = False
    if path.startswith('|'):
        root = True
        path = path[1:]
    splits = path.split('|')

    result = '|'.join(['%s%s' %(namespace, s) for s in splits])
    if root:
        result = '|%s' %(result)
    return result

def getAllInputs(root):
    children = mc.listRelatives(root, pa=True, ad=True)
    if children:
        children.insert(0, root)
    else:
        children = [root]

    pivots = {}
    poses = defaultdict(dict)
    objWithInputs = defaultdict(list)
    for obj in children:
        # skip constraint or non referenced node (probably constraints too)
        isReferred = mc.referenceQuery(obj, inr=True)
        if mc.objectType(obj, isa='constraint') and not isReferred:
            continue
        if not isReferred:
            continue

        # find unlocked, keyable attributes
        unlocked_attrs = mc.listAttr(obj, k=True, se=True, c=True, u=True, sn=True)  # give translateX, ... <long name>
        if not unlocked_attrs:
            continue
         
        # get pivots
        objName = str(removeDagPathNamespace(obj))
        if mc.objectType(obj, isa='transform'):
            pivot_values = mc.xform(obj, q=True, piv=True)
            # get pivot defaults
            piv_default = mc.attributeQuery('rotatePivot', node=obj, listDefault=True)
            if pivot_values[:3] != piv_default:
                pivots[objName] = ','.join([str(v) for v in pivot_values[:3]])

        for attrName in unlocked_attrs:
            attr = '%s.%s' %(obj, attrName)
            
            # keep try block to avoid non-existing attribute like .a[-1]
            attrInputs = []
            try:
                attrInputs = mc.listConnections(attr, s=True, d=False, c=True, p=True)
            except:
                continue

            attrInputs = attrInputs if attrInputs else []

            # list parent of attribute
            attrParents = mc.attributeQuery(attrName, node=obj, lp=True)
            if attrParents:  # let's include parent attr such as translate being parent of translateX
                parentInputs = mc.listConnections('%s.%s' %(obj, attrParents[0]), s=True, d=False, c=True, p=True)
                parentInputs = parentInputs if parentInputs else []
                attrInputs += parentInputs
            # convert to attr shortName i.e. 'arm.tx'
            attrInputs = ['%s.%s' %(a.split('.')[0], mc.attributeName(a, s=True)) for a in attrInputs] 

            # if attr has connections
            if attrInputs:
                # filter out connections with referenced source node
                for objAttr, srcAttr in zip(attrInputs[0::2], attrInputs[1::2]):
                    srcNode = srcAttr.split('.')[0]
                    if not mc.referenceQuery(srcNode, inr=True):
                        # collect data
                        objWithInputs[obj] += attrInputs

            # no input connections, store as pose
            else:
                try: 
                    value = mc.getAttr(attr)
                    if isinstance(value, (float, int, list)):
                        poses[objName][attrName] = value
                except RuntimeError: 
                    print('{} has no value'.format(attr))

    return objWithInputs, poses, pivots

def get_pose(project, namespace, ignore_exceptions=False):
    # read animIO config
    config_data = readConfig()
    try:
        config_dict = config_data[project]
    except:
        logger.warning('Failed to get project config, using default...')
        config_dict = config_data['default']
    
    # get root object from config
    rootNames = config_dict['rootObj']
    funcName = config_dict['ctrlMatchFunc'][0]
    exec('ctrlMatchFunc = %s' %funcName)
    ctrlMatchArg = config_dict['ctrlMatchFunc'][1]
    ctrlExceptions = config_dict['poseExceptCtrl']

    poses = defaultdict(dict)
    for rootName in rootNames:
        root = '%s:%s' %(namespace, rootName)
        if not mc.objExists(root):
            logger.warning('Cannot find root: %s' %(root))
            continue

        children = mc.listRelatives(root, pa=True, ad=True, type='transform')
        if children:
            children.insert(0, root)
        else:
            children = [root]

        for obj in children:
            # skip constraint or non referenced node (probably constraints too)
            isConstraint = mc.objectType(obj, isa='constraint')
            isRef = mc.referenceQuery(obj, inr=True)
            isCtrl = ctrlMatchFunc(ctrlMatchArg, obj)
            isExcepted = obj.split('|')[-1].split(':')[-1] in ctrlExceptions if not ignore_exceptions else False
            if isConstraint or not isRef or not isCtrl or isExcepted:
                continue

            # find unlocked, keyable attributes
            unlocked_attrs = mc.listAttr(obj, k=True, se=True, c=True, u=True, sn=True)  # give translateX, ... <long name>
            if not unlocked_attrs:
                continue
             
            for attrName in unlocked_attrs:
                attr = '%s.%s' %(obj, attrName)
                isExists = mc.objExists(attr)
                isSettable = mc.getAttr(attr, se=True)
                isAllowType = mc.getAttr(attr, type=True) in POSE_ATTR_TYPES
                if not isExists or not isSettable or not isAllowType:
                    # logger.error('Attribute does not exists: %s' %(attr))
                    continue

                value = mc.getAttr(attr)
                if isinstance(value, (float, int, list)):
                    objName = removeDagPathNamespace(obj)
                    poses[objName][attrName] = value
    return poses

def export_pose(project, namespace, path): 
    poses = get_pose(project, namespace)

    metadata = {}
    metadata['poses'] = dict(poses)
    # write out to file
    with open(path, 'w') as f:
        f.write('// %s\n' %(metadata))

    return poses

def import_pose(namespace, path):
    metadata = readMetaData(path)
    if not 'poses' in metadata:
        logger.error('Cannot find poses in metadata: %s' %path)
        return
    
    poses = metadata['poses']
    poseObjs = set_pose(poses, namespace)

def set_pose(poses, namespace):
    poseObjs = []
    for objName, values in poses.iteritems():
        objNameNs = addDagPathNamespace(path=objName, namespace='%s:' %namespace)

        objNameLs = mc.ls(objNameNs)
        if not objNameLs or len(objNameLs) != 1:
            logger.error('Cannot find: %s' %(objNameNs))
            continue

        poseObj = objNameLs[0]
        poseObjs.append(poseObj)
        for attr, value in values.iteritems():
            attrName ='%s.%s' %(poseObj, attr)
            isExists = mc.objExists(attrName)
            if not isExists or mc.getAttr(attrName) == value:
                continue
            try:
                mc.setAttr(attrName, value)
            except:
                logger.error('Error setting: %s, %s' %(attrName, value))

    return poseObjs

class BakeConstraintsInHierarchy(object):
    ''' Preparation for animation export including setting non-keyed attr on ctrl and bake constrainted channels '''
    def __init__(self, inputs, start, end, *args):
        self.inputs = inputs
        self.start = start
        self.end = end
        self.baked = set()
        
        self.connections = []

    def __enter__(self, *args):
        if not self.inputs:
            return

        # --- find attribute with constraint or pairBlend connections
        attrs = []
        # print self.inputs
        for obj, attrInputs in self.inputs.iteritems():
            for objAttr, srcAttr in zip(attrInputs[0::2], attrInputs[1::2]):
                srcNode = srcAttr.split('.')[0]
                if mc.objectType(srcNode, isa='constraint') or mc.objectType(srcNode, isa='pairBlend'):
                    objAttrName = '.'.join(objAttr.split('.')[1:])
                    if objAttrName in CONSTRAINTED_ATTRS:
                        attrs.append('"%s"' %(objAttr))
                        self.connections.append((srcAttr, objAttr))
                        # stored baked attributes
                        self.baked.add(objAttr)
        # bake it
        if attrs:
            attrStr = ','.join(attrs) 
            cmd = '''bakeResults -simulation true 
                -t "{0}:{1}" 
                -sampleBy 1 
                -oversamplingRate 1 
                -disableImplicitControl true 
                -preserveOutsideKeys false 
                -sparseAnimCurveBake false 
                -removeBakedAttributeFromLayer false 
                -removeBakedAnimFromLayer false 
                -bakeOnOverrideLayer false 
                -minimizeRotation true 
                -controlPoints false 
                -shape true {{{2}}};'''.format(self.start, self.end, attrStr)
            with IsolateNothing() as isolate:
                mel.eval(cmd)

        return self

    def __exit__(self, *args): 
        if self.connections:
            to_del = []
            for src, des in self.connections:
                anim_inputs = mc.listConnections(des, s=True, d=False, type='animCurve')
                if anim_inputs:
                    to_del += list(set(anim_inputs))
                if not mc.isConnected(src, des):
                    mc.connectAttr(src, des, f=True)
            if to_del:
                try:
                    mc.delete(to_del)
                except:
                    pass

class IsolateNothing():
    def __init__(self):
        self.panels = []
        self.selections = []

    def __enter__(self):
        if mc.about(batch=True):
            return
        self.selections = mc.ls(sl=True)
        mc.select(cl=True)
        for panel in mc.lsUI(editors=True):
            if panel.find('modelPanel') != -1:
                try:
                    mc.isolateSelect(panel, state=True)
                except:
                    pass
                self.panels.append(panel)

    def __exit__(self, *args):
        if mc.about(batch=True):
            return
        for panel in self.panels:
            try:
                mc.isolateSelect(panel, state=False)
            except:
                pass
        mc.select(self.selections)

class AnimData(object):
    ''' Anim data class '''
    def __init__(self, name, path=None, start=1, end=10, metadata=[]):
        self.name = name
        self.path = path
        self.start = start
        self.end = end
        self.metadata = metadata

    @classmethod
    def init_from_file(cls, path):
        basename = os.path.basename(path)
        fn, ext = os.path.splitext(basename)
        splits = fn.split('.')
        namespace = ''
        metadata = []
        new_obj = None

        # try to read metadata
        try:
            metadata = readMetaData(path)
        except:
            logger.error('Error reading metadata: %s' %path)

        # split file names for info
        try:
            shotName = splits[0]
            user = splits[1]
            namespace = splits[2]
            start, end = [int(f) for f in splits[3].split('_')]
            new_obj = cls(name=namespace, path=path, start=start, end=end, metadata=metadata)
        except:
            # return default class
            logger.error('Invalid file anim file name: %s' %path)
            basename = os.path.basename(path)
            filename, ext = os.path.splitext(basename)
            new_obj = cls(name=filename, path=path, metadata=metadata)
 
        return new_obj

class AnimAsset(object):
    ''' Core working class for import/export animation '''
    def __init__(self, root):
        self.root = root

    def clearChannels(self, animNodes):
        children = mc.listRelatives(self.root, ad=True, pa=True, type='transform')
        if children:
            children.insert(0, self.root)
        else:
            children = [self.root]

        for obj in children:
            # skip constraint or non referenced node (probably constraints too)
            if mc.objectType(obj, isa='constraint') or not mc.referenceQuery(obj, inr=True):
                continue

            # --- clear anim curves on non animNodes (zero groups)
            if obj not in animNodes:
                anim_inputs = mc.listConnections(obj, s=True, d=False, type='animCurve')
                anim_inputs = list(set(anim_inputs)) if anim_inputs else []
                if anim_inputs:
                    try:
                        mc.delete(anim_inputs)
                    except:
                        logger.error('Cannot delete anim curve on: %s' %(obj))

            # --- disconnect constraints
            cons_inputs = mc.listConnections(obj, s=True, d=False, type='constraint')
            cons_inputs = cons_inputs if cons_inputs else []
            pb_inputs = mc.listConnections(obj, s=True, d=False, type='pairBlend')
            pb_inputs = pb_inputs if pb_inputs else []
            inputs = set(cons_inputs + pb_inputs)
            cons_pb_nodes = []
            for node in inputs:
                # constraint must not be a referenced object
                if mc.referenceQuery(node, inr=True) or mc.lockNode(node, q=True)[0]:
                    continue
                cons_pb_nodes.append(node)
            if cons_pb_nodes:
                try:
                    logger.info('Deleting constraint/pairBlend on: %s' %(obj))
                    mc.delete(cons_pb_nodes)
                except:
                    logger.error('Cannot delete constraint/pairBlend on: %s' %(obj))

    def import_anim(self, data, replaceCompletely=True):
        if replaceCompletely:
            option = 'replaceCompletely'
        else:
            option = 'replace'
        
        namespace = ''
        if data.name:
            namespace = '%s:' %data.name
        start = 1
        if data.start:
            start = data.start

        # import anim curve
        crvs = mc.file(data.path, i=True, returnNewNodes=True, type='mayaAscii')

        # --- collect anim connections and anim nodes
        animNodes = []
        animDict = {}
        for crv in crvs:
            crvAttr = '%s.%s' %(crv, ANIM_OBJ_ATTR)
            if not mc.objExists(crvAttr):
                continue
                
            # find anim controller
            animObjValue = mc.getAttr(crvAttr)
            animObjName = addDagPathNamespace(path=animObjValue, namespace=namespace)
            animAttr = None

            animAttrs = mc.ls(animObjName)
            if not animAttrs or len(animAttrs) != 1:
                logger.error('Cannot find: %s' %(animObjName))
                continue
            animAttr = animAttrs[0]

            animNode = animAttr.split('.')[0]
            animNodes.append(animNode)
            animDict[animAttr] = crv 

        # print animDict
        # clear channels - remove unwanted keys, constraints and pairBlends
        self.clearChannels(animNodes=animNodes)

        # --- Copy keyframes
        for animAttr, crv in animDict.iteritems():
            # make sure attribute exists, in case it got cleared by clearChannels
            animAttrInputs = []
            try:
                # get keys
                animAttrInputs = mc.listConnections(animAttr, type='animCurve')
            except:
                continue
            
            keys = []
            if animAttrInputs:
                keys = [c for c in animAttrInputs if not mc.referenceQuery(c, inr=True) and not mc.lockNode(c, q=True)[0]]

            # set new keyframe
            if not keys:
                key_result = mc.setKeyframe(animAttr, t=start)
                if not key_result:
                    logger.error('Cannot set key: %s' %animAttr)
                    continue
                keys = mc.findKeyframe(animAttr, curve=True)

            # copy + paste the key
            paste_result = 0
            try:
                mc.copyKey(crv, hierarchy=False, controlPoints=False, shape=False, option='curve')
                paste_result = mc.pasteKey(keys[0], option=option, copies=1, connect=False, timeOffset=0, floatOffset=0, valueOffset=0)
            except Exception, e:
                print e

            if not paste_result:
                logger.error('Cannot copy key: %s' %animAttr)
                continue

        metadata = readMetaData(data.path)
        # --- set pivots
        if 'pivots' in metadata:
            pivots = metadata['pivots']
            for objName, valueStr in pivots.iteritems():
                objNameNs = addDagPathNamespace(path=objName, namespace=namespace)

                objNameLs = mc.ls(objNameNs)
                if not objNameLs or len(objNameLs) != 1:
                    logger.error('Cannot find object to set pivot: %s' %(objNameNs))
                    continue
                pivObj = objNameLs[0]

                try:
                    values = [float(v) for v in valueStr.split(',')]
                except:
                    logger.error('Error unpacking pivot value: %s' %(objName))
                    continue

                current_values = mc.xform(pivObj, q=True, piv=True)[:3]
                if current_values != values:
                    mc.xform(pivObj, piv=values)


        # --- set poses
        
        if 'poses' in metadata:
            poses = metadata['poses']
            for objName, values in poses.iteritems():
                objNameNs = addDagPathNamespace(path=objName, namespace=namespace)

                objNameLs = mc.ls(objNameNs)
                if not objNameLs or len(objNameLs) != 1:
                    logger.error('Cannot find object to set pose: %s' %(objNameNs))
                    continue
                poseObj = objNameLs[0]
                    
                for attr, value in values.iteritems():
                    try:
                        mc.setAttr('%s.%s' %(poseObj, attr), value)
                    except:
                        logger.error('Error setting pose: %s %s %s' %(poseObj, attr, value))

        # delete imported anim curves
        mc.delete(crvs)
        return data.path

    def export_anim(self, name, path, start, end, metadata={}):
        objWithInputs, poses, pivots = getAllInputs(root=self.root)
        animCrvs = []
        # bake constraint and pairBlend
        with BakeConstraintsInHierarchy(objWithInputs, start, end) as bakeCons:
            # add ANIM_OBJ_ATTR attribute to anim curves
            for obj, attrInputs in objWithInputs.iteritems():
                for objAttr, srcAttr in zip(attrInputs[0::2], attrInputs[1::2]):
                    # if the object is baked, re-list the connections
                    if objAttr in bakeCons.baked:
                        srcAttrs = mc.listConnections(objAttr, s=True, d=False, p=True)
                        if srcAttrs:
                            srcAttr = srcAttrs[0]
                        else:
                            continue

                    srcNode = srcAttr.split('.')[0]
                    animCrv = None
                    # a key frame is connected
                    if mc.objectType(srcNode, isa='animCurve'):
                        animCrv = srcNode
                    # pairBlend is connected
                    elif mc.objectType(srcNode, isa='pairBlend'):
                        attrName = '.'.join(objAttr.split('.')[1:])
                        pbAttrs = mc.listAttr(srcNode, st='i%s*' %attrName, sn=True)
                        if pbAttrs:
                            for attr in pbAttrs:
                                pbInputs = mc.listConnections('%s.%s' %(srcNode, attr), s=True, d=False, type='animCurve')
                                if pbInputs:
                                    animCrv = pbInputs[0]
                                    break
                
                    if animCrv:
                        # duplcate the anim curve
                        dupCrv = mc.duplicate(animCrv, n='%s1' %ANIM_CURVE_NAME)[0]

                        # set ANIM_OBJ_ATTR attr
                        try:
                            mc.addAttr(dupCrv, ln=ANIM_OBJ_ATTR, dt='string')
                        except:
                            pass
                        animAttr = '.'.join(objAttr.split('.')[1:])
                        objName = removeDagPathNamespace(obj)
                        animName = '%s.%s' %(objName, animAttr)
                        mc.setAttr('%s.%s' %(dupCrv, ANIM_OBJ_ATTR), animName, type='string')

                        # insert key frame on start and end
                        mc.setKeyframe(dupCrv, i=True, t=(start, end))
                        animCrvs.append(dupCrv)

        if not animCrvs:
            logger.warning('Cannot find animation to export: %s' %name)
            with open(path, 'w') as f:
                pass
        else:

            # export anim curves
            mc.select(animCrvs)
            mc.file(path, es=True, type="mayaAscii", f=True)
            # mc.delete(animCrvs)
        
        # attach ref_path and pose metadata
        refPath = ''
        if mc.referenceQuery(self.root, inr=True):
            refPath = mc.referenceQuery(self.root, f=True, wcn=True)
        metadata['ref_path'] = str(refPath)
        metadata['poses'] = dict(poses)
        metadata['pivots'] = dict(pivots)
        attachMetaData(path, metadata)

        if not name:
            name = os.path.basename(path)

        data = AnimData(name=name, path=path, start=start, end=end, metadata=metadata)
        return data

def export_sdk(path, objs):
    sdk_crvs = {}
    sdk_crv_types = ('animCurveUL', 'animCurveUA', 'animCurveUT', 'animCurveUU')
    for obj in objs:
        anim_crvs = []

        # take care of blendWeighted
        bw_inputs = mc.listConnections(obj, type='blendWeighted', scn=True, s=True, d=False, c=True)
        # print obj, bw_inputs
        if bw_inputs:
            for obj_attr, bw in zip(bw_inputs[0::2], bw_inputs[1::2]):
                bw_ac_inputs = mc.listConnections(bw, type='animCurve', scn=True, s=True, d=False)
                if bw_ac_inputs:
                    for ac in bw_ac_inputs:
                        anim_crvs += [obj_attr, ac]
        # print '.. %s' %anim_crvs
        crv_inputs = mc.listConnections(obj, type='animCurve', scn=True, s=True, d=False, c=True, p=True)
        # print obj, crv_inputs
        if crv_inputs:
            for obj_attr, ac_attr in zip(crv_inputs[0::2], crv_inputs[1::2]):
                anim_crvs += [obj_attr, ac_attr.split('.')[0]]
        # print anim_crvs
        if anim_crvs:
            for obj_attr, ac in zip(anim_crvs[0::2], anim_crvs[1::2]):
                if mc.nodeType(ac) in sdk_crv_types:
                    obj_attr_sn = '%s.%s' %(obj, mc.attributeName(obj_attr, s=True))
                    ac_input_attrs = mc.listConnections('%s.input' %ac, scn=True, s=True, d=False, p=True)
                    if ac_input_attrs:
                        driver_attr = ac_input_attrs[0]
                        driver = driver_attr.split('.')[0]
                        driver_attr_sn = '%s.%s' %(driver, mc.attributeName(driver_attr, s=True))
                        sdk_crvs[ac] = (driver_attr_sn, obj_attr_sn)
                    else:
                        logger.warning('Cannot find SDK driver on: %s' %obj_attr)

    animCrvs = []
    for animCrv, connections in sdk_crvs.iteritems():
        # duplcate the anim curve
        dupCrv = mc.duplicate(animCrv, n='%s1' %ANIM_CURVE_NAME)[0]

        # set ANIM_OBJ_ATTR attr
        try:
            mc.addAttr(dupCrv, ln=ANIM_OBJ_ATTR, dt='string')
        except:
            pass

        driver_attr = '.'.join(connections[0].split('.')[1:])
        driver_node = connections[0].split('.')[0]
        driver_node = removeDagPathNamespace(driver_node)

        driven_attr = '.'.join(connections[1].split('.')[1:])
        driven_node = connections[1].split('.')[0]
        driven_node = removeDagPathNamespace(driven_node)

        data = '%s.%s,%s.%s' %(driver_node, driver_attr, driven_node, driven_attr)
        # print data
        mc.setAttr('%s.%s' %(dupCrv, ANIM_OBJ_ATTR), data, type='string')
        animCrvs.append(dupCrv)

    # export anim curves
    mc.select(animCrvs)
    mc.file(path, es=True, type="mayaAscii", f=True)
    mc.delete(animCrvs)

    return path

def import_sdk(path, driver_namesapce='', driven_namespace=''):
    # import anim curve
    imported_objs = mc.file(path, i=True, returnNewNodes=True, type='mayaAscii')

    # --- collect anim connections and anim nodes
    sdk_crv_types = ('animCurveUL', 'animCurveUA', 'animCurveUT', 'animCurveUU')
    sdk_nodes = defaultdict(list)  # animAttr: [anim_crv1, ...]
    for crv in imported_objs:
        data_attr = '%s.%s' %(crv, ANIM_OBJ_ATTR)
        if mc.nodeType(crv) in sdk_crv_types and mc.objExists(data_attr):
            # find anim controller
            data = mc.getAttr(data_attr)
            data_splits = data.split(',')
            driver_data = data_splits[0]
            driven_data = data_splits[1]

            # driver
            driver_attr_name = addDagPathNamespace(path=driver_data, namespace=driver_namesapce)
            driver_attr = None
            driver_attrs = mc.ls(driver_attr_name)
            if not driver_attrs or len(driver_attrs) != 1:
                logger.error('Cannot find driver: %s' %(driver_attr_name))
                continue
            driver_attr = driver_attrs[0]

            # driven
            driven_attr_name = addDagPathNamespace(path=driven_data, namespace=driven_namespace)
            driven_attr = None
            driven_attrs = mc.ls(driven_attr_name)
            if not driven_attrs or len(driven_attrs) != 1:
                logger.error('Cannot find driven: %s' %(driven_attr_name))
                continue
            driven_attr = driven_attrs[0]

            sdk_nodes[driven_attr].append((driver_attr, crv))
    # print sdk_nodes
    for driven_attr, datas in sdk_nodes.iteritems():
        # cut whatever coming into the attribute
        connected_src = mc.connectionInfo(driven_attr, sfd=True)
        if connected_src:
            mc.disconnectAttr(connected_src, driven_attr)

        # if more than one curve have to connect to this attr, create a blendWeighted node
        if len(datas) > 1:
            isLocked = False
            if mc.getAttr(driven_attr, l=True):
                mc.setAttr(driven_attr, l=False)
                isLocked = True
            bw = mc.createNode('blendWeighted')
            try:
                mc.connectAttr('%s.output' %bw, driven_attr, f=True)
            except:
                logger.error('Cannot connect SDK: %s' %driven_attr)

            if isLocked:
                mc.setAttr(driven_attr, l=True)

            for i, data in enumerate(datas):
                driver_attr = data[0]
                crv = data[1]
                mc.connectAttr('%s.output' %crv, '%s.input[%s]' %(bw, i), f=True)
                if not mc.isConnected(driver_attr, '%s.input' %crv):
                    mc.connectAttr(driver_attr, '%s.input' %crv)
        else:
            for data in datas:
                driver_attr = data[0]
                crv = data[1]

                isLocked = False
                if mc.getAttr(driven_attr, l=True):
                    mc.setAttr(driven_attr, l=False)
                    isLocked = True

                try:
                    mc.connectAttr('%s.output' %crv, driven_attr, f=True)
                    mc.connectAttr(driver_attr, '%s.input' %crv)
                except:
                    logger.error('Cannot connect SDK: %s' %driven_attr)

                if isLocked:
                    mc.setAttr(driven_attr, l=True)
        
    return path

def match_ctrl_suffix(matchStr, obj):
    match = re.match(matchStr, obj)
    return match

def match_ctrl_shape_type(shapeTypes, obj):
    shapes = None
    try:
        shapes = [s for s in mc.listRelatives(obj, pa=True, shapes=True) if mc.nodeType(s) in shapeTypes]
    except:
        pass
    return shapes



'''
reload(core)
metadata = core.readMetaData(path)
animData = core.AnimData(name='duckNovice_002',
     path=path, start=start, end=end, metadata=metadata)
animAsset = core.AnimAsset(root=root)
res = animAsset.import_anim(data=animData)
'''