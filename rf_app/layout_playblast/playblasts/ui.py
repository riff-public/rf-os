# -*- coding: utf-8 -*-
import os
import sys

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


class ModelSmoothUI(QtWidgets.QWidget):
    def __init__(self, parent=None) :
        super(ModelSmoothUI, self).__init__(parent)

        self.allLayout = QtWidgets.QVBoxLayout()
        self.mainLayout = QtWidgets.QVBoxLayout()
        self.label = QtWidgets.QLabel('Shot List')
        self.mainLayout.addWidget(self.label)
        self.listWidget = QtWidgets.QListWidget()
        self.listWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.mainLayout.addWidget(self.listWidget)
        self.allLayout.addLayout(self.mainLayout)
        self.playblastLayout = QtWidgets.QVBoxLayout()
        self.playblast_pushButton = QtWidgets.QPushButton('Playblast')
        self.playblast_pushButton.setMinimumSize(QtCore.QSize(0, 50))
        self.syn_shotgun_checkBox = QtWidgets.QCheckBox('SynShotgun')
        self.show_light_checkBox = QtWidgets.QCheckBox('Showlight')
        self.show_texture_checkBox = QtWidgets.QCheckBox('Showtexture')
        self.playblastLayout.addWidget(self.syn_shotgun_checkBox)
        self.playblastLayout.addWidget(self.show_texture_checkBox)
        self.playblastLayout.addWidget( self.show_light_checkBox)
        self.playblastLayout.addWidget(self.playblast_pushButton)
        self.allLayout.addLayout(self.playblastLayout)
        self.setLayout(self.allLayout)


