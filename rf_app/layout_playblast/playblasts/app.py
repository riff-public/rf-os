# v.0.0.1 polytag switcher
_title = 'Riff Model Smooth Data'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'RFModelSmoothUI'


#Import python modules
import sys
import os
import getpass
import logging
from functools import partial
from collections import OrderedDict
import shutil

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

##rf_temp
LocalTemp = os.environ['TEMP']
rf_tmp = os.path.join(LocalTemp,'rf_tmp').replace('\\','/')
# import config
import rf_config as config

if config.isMaya:
    import maya.cmds as mc
    import maya.mel as mm

# framework modules
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from rf_maya.lib import playblast_lib
reload(playblast_lib)
from rf_utils.pipeline import playblast_converter
from rf_utils import convert_timecode
from rf_utils.context import context_info 
from rftool.utils import sg_process
from rf_maya.rftool.fix import sg_tmp 
from rf_utils.pipeline import convert_lib
from rf_utils.sg import sg_utils
sg = sg_utils.sg
from rf_utils import file_utils

import ui
# thread

class Config:
    asset = 'asset'
    scene = 'scene'
    mode = {asset: 'Asset', scene: 'Shot'}

class ModelSmoothData(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(ModelSmoothData, self).__init__(parent)

        # ui read
        self.ui = ui.ModelSmoothUI(parent)
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)

        # smooth
        self.smoothSet = 'smoothPly_set'

        # title
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.resize(300, 300)
        self.init_signals()
        self.display()
    def init_signals(self):
        self.ui.playblast_pushButton.clicked.connect(self.playblast)

    def create_check_box (self, shotName):
        item = QtWidgets.QListWidgetItem(self.ui.listWidget)
        ch = QtWidgets.QCheckBox(shotName)
        self.ui.listWidget.setItemWidget(item, ch)
    
    def display(self):
        list_shot = mc.sequenceManager(lsh=True)
        if list_shot == None:
            logger.debug('not have Camsequence')
            mc.warning('NO Camseq')
        else:    
            for shot in list_shot:
                self.create_check_box(shot)


    def playblast(self):
        
        list_playblast = []
        scene = self.find_entity()
        for index in xrange(self.ui.listWidget.count()):
            check_box = self.ui.listWidget.itemWidget(self.ui.listWidget.item(index))
            state = check_box.checkState()
            shotName = check_box.text()
            scene.context.update(entityCode=shotName, publishVersion='v001')
            if state == QtCore.Qt.CheckState.Checked:
                self.context.update(entity=shotName)
                scene= context_info.ContextPathInfo(context=self.context)
                scene.set_project_code(sg)
                name = scene.output_name(outputKey='playblast')
                filepath = scene.path.app().abs_path()
                dst_path = "%s/playblast/%s"%(filepath, name)
                print dst_path
                list_shot = []
                list_shot.append(shotName)
                #######sound part
                sound_offset = self.find_sound_sequecne(shotName)
                #######sound part
                print list_shot

                startFrame, endFrame, seqStartFrame, seqEndFrame = playblast_lib.get_shot_frame(shotName)
                mc.sequenceManager(currentTime = seqStartFrame)
                cam = mc.shot(shotName,cc=True,q=True)
                cc_modelPanel =  self.getPanelFromCamera(cam)
                mc.modelEditor(cc_modelPanel, e=1, nurbsCurves=0)
                mc.modelEditor(cc_modelPanel, e=1, locators=0)
                show_texture_checkBox = self.ui.show_texture_checkBox.checkState()
                show_light_checkBox = self.ui.show_light_checkBox.checkState()

                if show_texture_checkBox == QtCore.Qt.CheckState.Checked:
                    mc.modelEditor(cc_modelPanel, e=1, displayTextures=1)
                if show_light_checkBox == QtCore.Qt.CheckState.Checked:
                    mc.modelEditor(cc_modelPanel, e=1, displayLights='all')

                playblast_output = playblast_lib.play_sequencer(list_shot, dst_path, projectHud=scene.project, userHud=scene.local_user, departmentHud=scene.step, filenameHud=name, sound_path=sound_offset)
                file_output, basename = os.path.split(dst_path)
                print playblast_output
                mov_fileName = os.path.join(file_output, name).replace('\\','/')
                if os.path.exists(mov_fileName) == True:
                    file_utils.remove(mov_fileName)
                file_utils.rename(playblast_output[0], os.path.join(file_output, name).replace('\\','/'))

                list_playblast.append([shotName, dst_path])


            else:
                pass

        mc.modelEditor(cc_modelPanel, e=1,displayAppearance='smoothShaded')
        mc.modelEditor(cc_modelPanel, e=1, nurbsCurves=1)
        mc.modelEditor(cc_modelPanel, e=1, locators=1)
        sysc_sg_status = self.ui.syn_shotgun_checkBox.checkState()
        if sysc_sg_status == QtCore.Qt.CheckState.Checked:
            for index, shot_data in enumerate(list_playblast):
                shotName, dst_path = shot_data
                sequenceSg = "%s_%s_%s"%(scene.project_code, scene.episode, scene.sequence)
                shotEntity = sg_process.get_one_shot(scene.project, scene.episode, sequenceSg, shotName)
                if shotEntity == None:
                    print scene.project_code, shotName
                    self.create_shot_sg(scene, shotName)
                sg_tmp.send_shot_review(scene.project, scene.episode, scene.sequence, shotName, scene.version, scene.step, 'rev', dst_path, None, user=scene.local_user, step=scene.step)


    def find_entity(self):
        self.context = context_info.Context()
        self.context.use_path(path=mc.file(q=True, sn=True))
        scene= context_info.ContextPathInfo(context=self.context)
        return scene

    def create_shot_sg(self, scene, shotName):
        from rf_utils.sg import sg_process
        sequenceSg = "%s_%s_%s"%(scene.project_code, scene.episode, scene.sequence)
        projectEntity = sg_process.get_project(scene.project)
        episodeEntity = sg_process.get_one_episode(scene.project, scene.episode)
        sequenceEntity = sg_process.get_one_sequence(scene.project, scene.episode, sequenceSg)
        sg_process.create_shot(projectEntity, episodeEntity, sequenceEntity , '%s_%s'%(sequenceSg, shotName), shotName, template='default')

    def find_sound_sequecne(self, shot):
        startFrame, endFrame, seqStartFrame, seqEndFrame = playblast_lib.get_shot_frame(shot)
        sounds = mc.sequenceManager(lsa=True)
        if sounds == None:
            return sounds
        sound_path =  mc.getAttr('%s.filename'%sounds[0])
        sound_output = os.path.join(rf_tmp, os.path.basename(sound_path)).replace('\\', '/')
        start = convert_timecode.frames_to_timecode(seqStartFrame, framerate=24)
        End = convert_timecode.frames_to_timecode(seqEndFrame, framerate=24)
        sound_offset = playblast_converter.offset_audio(sound_path, sound_output, start, End)
        return sound_offset

    def getPanelFromCamera(self, cameraName):
        listPanel=[]
        for panelName in mc.getPanel( type="modelPanel" ):
            if mc.modelPanel( panelName,query=True, camera=True) == cameraName:
                listPanel.append( panelName )
        return listPanel

def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = ModelSmoothData(maya_win.getMayaWindow())
        myApp.show()
        return myApp


##filepath = dst playblast
##synShotgun