### script Rename CamSeq

list_shot = mc.sequenceManager(lsh=True)
new_list_shot = []
for name in list_shot:
    new_list_shot.append(name.lower())
    mc.setAttr('%s.shotName'% name,  name.lower(), type = 'string')
    mc.rename(name, name.lower())
    