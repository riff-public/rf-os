# titles 
uiName = 'CacheCheckOut'
_title = 'Checkout Cache'
_version = 'v.0.0.1'
_des = 'wip'

#Import python modules
import os 
import sys

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from rf_utils.context import context_info
from rf_utils.pipeline import cache_utils


class Color: 
    default = [200, 200, 200]
    grey = [100, 100, 100]
    red = [220, 100, 100]
    green = [100, 220, 100]


class Ui(QtWidgets.QWidget) :

    def __init__(self, parent = None) :
        super(Ui, self).__init__(parent)
        self.allLayout = QtWidgets.QVBoxLayout()
        self.label = QtWidgets.QLabel('Asset List')
        self.listWidget = QtWidgets.QListWidget()
        self.refreshButton = QtWidgets.QPushButton('')
        self.button = QtWidgets.QPushButton('')

        self.allLayout.addWidget(self.label)
        self.allLayout.addWidget(self.listWidget)
        self.allLayout.addWidget(self.refreshButton)
        self.allLayout.addWidget(self.button)

        self.setLayout(self.allLayout)
        self.set_default()

    def set_default(self): 
        self.button.setMinimumSize(QtCore.QSize(0, 30))
        self.button.setText('Checkout')
        self.refreshButton.setText('Refresh')


class Main(QtWidgets.QMainWindow):
    """docstring for Main"""
    def __init__(self, entity=None, parent=None):
        super(Main, self).__init__(parent)
        self.ui = Ui(parent)
        self.entity = entity
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.setWindowTitle('%s %s %s' % (_title, _version, _des))
        self.resize(300, 400)

        self.init_functions()
        self.init_signals()

    def init_functions(self): 
        self.init_context()
        self.set_asset_list() 

    def init_signals(self): 
        self.ui.button.clicked.connect(self.checkout) 
        self.ui.listWidget.itemSelectionChanged.connect(self.item_selected)
        self.ui.refreshButton.clicked.connect(self.set_asset_list)

    def init_context(self): 
        self.scene = context_info.ContextPathInfo()

    def set_asset_list(self): 
        assets = self._list_all_assets()
        cacheAssets = self._list_assets()

        self.ui.listWidget.clear()

        for asset in assets: 
            displayStatus = 'Passive'
            status = False

            if asset in cacheAssets: 
                displayStatus = 'Active'
                status = True

            # item 
            widgetItem = ListItem()

            # set colors 
            colors1 = Color.default if status else Color.grey
            colors2 = Color.green if status else Color.red
            widgetItem.set_text_color(widgetItem.text1, colors1)
            widgetItem.set_text_color(widgetItem.text2, colors2)

            # set info 
            widgetItem.text1.setText(asset)
            widgetItem.text2.setText(displayStatus)
            
            data = {'namespace': asset, 'status': status}
            self.add_widget_item(widgetItem, data)

    def add_widget_item(self, widgetItem, data):
        item = QtWidgets.QListWidgetItem(self.ui.listWidget)
        item.setSizeHint(widgetItem.sizeHint())
        item.setData(QtCore.Qt.UserRole, data)

        self.ui.listWidget.addItem(item)
        self.ui.listWidget.setItemWidget(item, widgetItem)
        return item

    def checkout(self): 
        # checkout asset 
        namespace, active = self._get_item_data()

        if not active: 
            cache = cache_utils.Cache(namespace)
            cache.checkout()

        self.set_asset_list()

    def item_selected(self): 
        namespace, active = self._get_item_data()
        self.ui.button.setEnabled(not active)

    def _get_item_data(self): 
        item = self.ui.listWidget.currentItem()
        data = item.data(QtCore.Qt.UserRole)
        namespace = data['namespace']
        active = data['status']

        return namespace, active


    def _list_assets(self): 
        from rf_app.publish.scene.export import cache 
        reload(cache)
        cacheGrp = self.scene.projectInfo.asset.geo_grp() or 'Geo_Grp'
        cachableList = cache.get_asset_namespace(cacheGrp)

        return cachableList

    def _list_all_assets(self): 
        from rf_app.publish.scene.export import cache 
        reload(cache)
        cacheGrp = self.scene.projectInfo.asset.geo_grp() or 'Geo_Grp'
        assets = cache.get_all_asset_namespace(cacheGrp)
        return assets 


class ListItem(QtWidgets.QWidget):
    """docstring for ListItem"""
    def __init__(self, parent=None):
        super(ListItem, self).__init__(parent=parent)
        self.set_ui()


    def set_ui(self):
        self.layout = QtWidgets.QHBoxLayout()
        self.text1 = QtWidgets.QLabel()
        self.text2 = QtWidgets.QLabel()

        self.layout.addWidget(self.text1)
        self.layout.addWidget(self.text2)

        self.setLayout(self.layout)

        # setStyleSheet
        self.text1.setFont(QtGui.QFont("Arial", 9, QtGui.QFont.Bold))
        self.text2.setFont(QtGui.QFont("Arial", 8))

    def set_text_color(self, widget, colors):
        widget.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))


        

def show(entity=None):
    from rftool.utils.ui import maya_win
    maya_win.deleteUI(uiName)
    myApp = Main(entity, maya_win.getMayaWindow())
    myApp.show()
    return myApp
