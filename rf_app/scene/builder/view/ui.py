# -*- coding: utf-8 -*-
uiName = 'SceneBuilderUI'
import os
import sys
from collections import OrderedDict

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui
from functools import partial

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)


module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.scene.builder.model.scene_builder_model import COLUMNS_INDEX
from rf_utils import icon
from rf_utils.widget import project_widget
from rf_utils.widget import entity_browse_widget 
from rf_utils.pipeline import user_pref

from rf_utils.sg import sg_utils

class TableConfig:
    def column_order():
        column = OrderedDict()
        column['asset'] = 'asset'
        column['type'] = 'type'
        column['status'] = 'status'
        column['material_status'] = 'material status'
        column['current_step'] = 'current step'
        column['current_version'] = 'current version'
        column['department'] = 'department'
        column['output'] = 'output type'
        column['version'] = 'version'
        column['material'] = 'material'
        column['build_shade'] = 'build shade'
        column['action'] = 'Action'
        column['action2'] = 'Action2'
        column['remove'] = 'Remove'
        return column

    column = column_order()


class SceneBuilderUI(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(SceneBuilderUI, self).__init__(parent)

        self.list_wid_items = ['hero', 'layout', 'anim', 'sim', 'finalcam', 'setdress']
        # self.table_wid_columns = [
        #     'asset',
        #     'status',
        #     'material status',
        #     'current step',
        #     'current version',
        #     'department',
        #     'version',
        #     'type',
        #     'material',
        #     'build shade',
        #     'Action',
        #     "Remove"
        # ]

        self.all_wrapper_layout = QtWidgets.QVBoxLayout()
        self.allLayout = QtWidgets.QHBoxLayout()
        self.mainLayout = QtWidgets.QGridLayout()

        self.progres_label_layout = QtWidgets.QHBoxLayout()
        self.progrss_text_label = QtWidgets.QLabel('Loading')
        self.progrss_text_label.setVisible(False)
        self.progres_label_layout.addWidget(self.progrss_text_label, 0, QtCore.Qt.AlignCenter)

        self.logo_label = QtWidgets.QLabel()
        self.logo_layout = QtWidgets.QGridLayout()
        self.space_label = QtWidgets.QLabel()
        self.logo_layout.addWidget(self.logo_label, 0, 0, 1, 12)
        self.logo_layout.addWidget(self.space_label, 0, 1, 1, 12)

        self.navigation_layout = QtWidgets.QVBoxLayout()
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        
        self.add_project_widget(self.navigation_layout)
        self.add_sg_navigation(self.navigation_layout)
        
        self.navigation_layout.addItem(spacerItem)
        self.navigation_layout.setStretch(0, 0)
        self.navigation_layout.setStretch(1, 0)
        self.navigation_layout.setStretch(2, 0)
        self.navigation_layout.setStretch(3, 0)
        self.navigation_layout.setStretch(4, 2)

        # self.entityLabel = QtWidgets.QLabel('Browse')
        # self.sgWidget = entity_browse_widget.SGSceneNavigation2(sg=sg_utils.sg, shotFilter=True)

        self.label = QtWidgets.QLabel('Department')
        self.department_list_view = QtWidgets.QListWidget()
        self.department_list_view.addItems(self.list_wid_items)
        self.department_list_view_layout = QtWidgets.QVBoxLayout()
        self.department_list_view_layout.addWidget(self.label)
        self.department_list_view_layout.addWidget(self.department_list_view)

        self.table_layout = QtWidgets.QHBoxLayout()

        self.table_label = QtWidgets.QLabel('List Asset')
        self.filter_checkBox = QtWidgets.QCheckBox('Filter Data')
        self.reGroup_checkBox = QtWidgets.QCheckBox('Re Group')
        self.table_widget = QtWidgets.QTableWidget(0, len(TableConfig.column.keys()))
        self.table_widget.setHorizontalHeaderLabels(TableConfig.column.keys())
        self.table_widget_layout = QtWidgets.QVBoxLayout()
        self.table_layout.addWidget(self.table_label)
        self.table_layout.addWidget(self.reGroup_checkBox)
        self.table_layout.addWidget(self.filter_checkBox)
        self.table_widget_layout.addLayout(self.table_layout)
        self.table_widget_layout.addWidget(self.table_widget)

        self.table_layout.setStretch(0, 1)
        self.table_layout.setStretch(1, 0)

        self.build_update_layout = QtWidgets.QHBoxLayout()
        self.status_label = QtWidgets.QLabel('')
        self.selected_checkBox = QtWidgets.QCheckBox('Build Selected')
        self.skipSet_checkBox = QtWidgets.QCheckBox('Skip Setdress')
        self.build_update_btn = QtWidgets.QPushButton("Build/Update All")
        self.reload_btn = QtWidgets.QPushButton("Reload All")
        self.remove_btn = QtWidgets.QPushButton("Remove All")
        self.build_update_layout.addWidget(self.status_label)
        self.build_update_layout.addWidget(self.skipSet_checkBox)
        self.build_update_layout.addWidget(self.selected_checkBox)
        self.build_update_layout.addWidget(self.build_update_btn, 0, QtCore.Qt.AlignRight)
        self.build_update_layout.addWidget(self.reload_btn, 0, QtCore.Qt.AlignRight)
        self.build_update_layout.addWidget(self.remove_btn, 0, QtCore.Qt.AlignRight)
        self.build_update_btn.setMinimumSize(QtCore.QSize(200, 30))
        self.reload_btn.setMinimumSize(QtCore.QSize(100, 30))
        self.remove_btn.setMinimumSize(QtCore.QSize(100, 30))

        self.build_update_layout.setStretch(0, 2)
        self.build_update_layout.setStretch(1, 0)
        self.build_update_layout.setStretch(2, 0)
        self.build_update_layout.setStretch(3, 0)
        self.build_update_layout.setStretch(4, 0)

        self.mainLayout.addLayout(self.logo_layout, 0, 0, 1, 1)
        self.mainLayout.addLayout(self.navigation_layout, 1, 0, 1, 1)
        self.mainLayout.addLayout(self.department_list_view_layout, 1, 1, 1, 1)
        self.mainLayout.addLayout(self.table_widget_layout, 1, 2, 1, 1)
        self.mainLayout.setColumnStretch(0, 0)
        self.mainLayout.setColumnStretch(1, 0)
        self.mainLayout.setColumnStretch(2, 5)
        self.allLayout.addLayout(self.mainLayout)
        self.all_wrapper_layout.addLayout(self.allLayout)
        self.all_wrapper_layout.addLayout(self.build_update_layout)
        self.all_wrapper_layout.addLayout(self.progres_label_layout)
        self.setLayout(self.all_wrapper_layout)
        self.set_default()
        self.init_signals()


    def set_default(self):
        self.table_widget.setColumnHidden(TableConfig.column.keys().index('material_status'), True)
        self.table_widget.setColumnHidden(TableConfig.column.keys().index('current_step'), True)
        self.table_widget.setColumnHidden(TableConfig.column.keys().index('current_version'), True)
        self.table_widget.setColumnHidden(TableConfig.column.keys().index('build_shade'), True)
        self.table_widget.setColumnHidden(TableConfig.column.keys().index('version'), True)
        self.table_widget.setColumnHidden(TableConfig.column.keys().index('material'), True)

    def init_signals(self): 
        pass 
        # self.selected_checkBox.stateChanged.connect(self.selected_checked)

    def selected_checked(self, value): 
        state = True if value else False 
        if state: 
            self.build_update_btn.setText('Build Selected')
            self.reload_btn.setText('Reload Selected')
            self.remove_btn.setText('Remove Selected')
        else: 
            self.build_update_btn.setText('Build/Update All')
            self.reload_btn.setText('Reload All')
            self.remove_btn.setText('Remove All')


    def add_project_widget(self, layout):
        """ add project widget """
        self.userPref = user_pref.ToolSetting(uiName)
        self.userData = self.userPref.read()
        project = self.userData.get('project')

        self.projectWidget = project_widget.SGProjectComboBox(sg=sg_utils.sg, defaultProject=project)
        self.projectWidget.allLayout.setSpacing(8)
        layout.addWidget(self.projectWidget)

    def add_sg_navigation(self, layout):
        """ add sg navigation widget """
        self.entityLabel = QtWidgets.QLabel('Browse')
        self.fetchButton = QtWidgets.QPushButton('Next >>')
        self.sgWidget = entity_browse_widget.SGSceneNavigation2(sg=sg_utils.sg, shotFilter=True)
        self.sgWidget.shotExtraFilter = ['sg_shot_type', 'is', 'Shot']
        self.line = hline_widget()
        layout.addWidget(self.sgWidget)
        layout.addWidget(self.line)

        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        layout.addWidget(self.fetchButton)
        layout.addItem(spacerItem)

    def add_row(
        self,
        asset=None,
        status=None,
        mtr_status=None,
        current_step=None,
        current_version=None,
        department=[],
        output_types=[], 
        version=[],
        type_file=[],
        materials=[],
        action=None,
        action_func=None,
        action_func2=None,
        action_remove_func=None,
        action_sync_shade=None,
        disabled=False,
        data=None
    ):
        overall_row = self.table_widget.rowCount()
        version_combo = QtWidgets.QComboBox()
        version_combo.addItems(version)
        # type_combo = QtWidgets.QComboBox()
        # type_combo.addItems(type_file)
        mtr_combo = QtWidgets.QComboBox()
        # mtr_combo.addItems(materials)
        output_combo = QtWidgets.QComboBox()
        output_combo.addItems(output_types)
        step_combo = QtWidgets.QComboBox()
        step_combo.addItems(department)
        action_pushButton = QtWidgets.QPushButton(action)
        action2_pushButton = QtWidgets.QPushButton('Reload')
        remove_pushButton = QtWidgets.QPushButton("Remove")
        build_shade_pushButton = QtWidgets.QPushButton("Build Shade")
        build_shade_pushButton.setEnabled(False)

        if materials:
            for row, mtr in enumerate(materials['texture_path']):
                mtr_combo.addItem(mtr.get('display_name'))
                materials['texture_selected'] = mtr
                mtr_combo.setItemData(row, materials, QtCore.Qt.UserRole)
                build_shade_pushButton.setEnabled(True)

        version_combo.activated.connect(self.version_changed)
        # type_combo.activated.connect(self.type_changed)
        mtr_combo.activated.connect(self.mtr_changed)

        self.table_widget.insertRow(overall_row)

        asset_item = QtWidgets.QTableWidgetItem(asset)
        status_item = QtWidgets.QTableWidgetItem('')
        material_status_item = QtWidgets.QTableWidgetItem(mtr_status)
        current_step_item = QtWidgets.QTableWidgetItem(current_step)
        current_version_item = QtWidgets.QTableWidgetItem(current_version)
        department_item = QtWidgets.QTableWidgetItem(step_combo.currentText())
        version_item = QtWidgets.QTableWidgetItem(version_combo.currentText())
        type_file_item = QtWidgets.QTableWidgetItem(type_file)
        asset_item.setData(QtCore.Qt.UserRole, data)
        output_item = QtWidgets.QTableWidgetItem(output_combo.currentText())

        if self.get_dict_data_combobox(mtr_combo):
            mtr_item = QtWidgets.QTableWidgetItem()
            mtr_item.setData(QtCore.Qt.UserRole, self.get_dict_data_combobox(mtr_combo))
        else:
            mtr_item = QtWidgets.QTableWidgetItem(self.get_dict_data_combobox(mtr_combo))

        if action == "Build":
            remove_pushButton.setEnabled(False)
            build_shade_pushButton.setEnabled(False)

        # action_pushButton.clicked.connect(action_func)
        action_pushButton.clicked.connect(partial(action_func, overall_row))
        action2_pushButton.clicked.connect(partial(action_func2, overall_row))
        remove_pushButton.clicked.connect(partial(action_remove_func, overall_row))
        # build_shade_pushButton.clicked.connect(partial(action_sync_shade, overall_row))

        if disabled:
            self.table_widget.setItem(overall_row, COLUMNS_INDEX['asset'], asset_item)
            version_combo.setEnabled(False)
            # type_combo.setEnabled(False)
            action_pushButton.setEnabled(False)
            action2_pushButton.setEnabled(False)
            build_shade_pushButton.setEnabled(False)
            self.set_disable_row(overall_row)
        # else:
        self.table_widget.setItem(overall_row, TableConfig.column.keys().index('asset'), asset_item)
        self.table_widget.setItem(overall_row, TableConfig.column.keys().index('status'), status_item)
        self.table_widget.setItem(overall_row, TableConfig.column.keys().index('material_status'), material_status_item)
        self.table_widget.setItem(overall_row, TableConfig.column.keys().index('current_step'), current_step_item)
        self.table_widget.setItem(overall_row, TableConfig.column.keys().index('current_version'), current_version_item)
        # self.table_widget.setItem(overall_row, TableConfig.column.keys().index('department'), department_item)
        self.table_widget.setItem(overall_row, TableConfig.column.keys().index('version'), version_item)
        self.table_widget.setItem(overall_row, TableConfig.column.keys().index('type'), type_file_item)
        self.table_widget.setItem(overall_row, TableConfig.column.keys().index('material'), mtr_item)

        self.table_widget.setCellWidget(overall_row, TableConfig.column.keys().index('version'), version_combo)
        self.table_widget.setCellWidget(overall_row, TableConfig.column.keys().index('output'), output_combo)
        self.table_widget.setCellWidget(overall_row, TableConfig.column.keys().index('department'), step_combo)
        # self.table_widget.setCellWidget(overall_row, TableConfig.column.keys().index('type'), type_combo)
        self.table_widget.setCellWidget(overall_row, TableConfig.column.keys().index('material'), mtr_combo)
        self.table_widget.setCellWidget(overall_row, TableConfig.column.keys().index('build_shade'), build_shade_pushButton)
        self.table_widget.setCellWidget(overall_row, TableConfig.column.keys().index('action'), action_pushButton)
        self.table_widget.setCellWidget(overall_row, TableConfig.column.keys().index('action2'), action2_pushButton)
        self.table_widget.setCellWidget(overall_row, TableConfig.column.keys().index('remove'), remove_pushButton)

        self.resize_column()
        # status icon
        self.update_status(overall_row, status)

    def update_status(self, row, statusDict):
        statusIcon = statusDict.get('icon')
        status = statusDict.get('path')
        item = self.table_widget.item(row, TableConfig.column.keys().index('status'))
        item.setData(QtCore.Qt.UserRole, status)
        # statusIcon = icon.sgAprv if status else icon.no

        iconWidget = QtGui.QIcon()
        iconWidget.addPixmap(QtGui.QPixmap(statusIcon),QtGui.QIcon.Normal,QtGui.QIcon.Off)
        item.setIcon(iconWidget)

    def get_status(self, row):
        item = self.table_widget.item(row, TableConfig.column.keys().index('status'))
        return item.data(QtCore.Qt.UserRole)


    def resize_column(self):
        # resize column
        for col in TableConfig.column.keys():
            if not col in ['action', 'remove']:
                self.table_widget.resizeColumnToContents(TableConfig.column.keys().index(col))

    def get_table_data(self, row):
        item = self.table_widget.item(row, TableConfig.column.keys().index('asset'))
        return item.data(QtCore.Qt.UserRole)

    def set_disable_row(self, row):
        # for i in xrange(0, 3):
        table_item = self.table_widget.item(row, 0)
        table_item.setFlags(QtCore.Qt.NoItemFlags)

    def version_changed(self, option):
        combo = self.sender()
        # print('option: %s, "%s"' % (option, combo.itemText(option)))
        index = self.table_widget.indexAt(combo.pos())
        # print('row: %s, column: %s' % (index.row(), index.column()))
        version_item = QtWidgets.QTableWidgetItem(combo.itemText(option))
        self.table_widget.setItem(index.row(), index.column(), version_item)

    def type_changed(self, option):
        type_file = self.sender()
        # print('option: %s, "%s"' % (option, combo.itemText(option)))
        index = self.table_widget.indexAt(type_file.pos())
        # print('row: %s, column: %s' % (index.row(), index.column()))
        type_file_item = QtWidgets.QTableWidgetItem(type_file.itemText(option))
        self.table_widget.setItem(index.row(), index.column(), type_file_item)

    def mtr_changed(self, option):
        mtr_combo_item = self.sender()
        mtr_item_data = self.get_dict_data_combobox(mtr_combo_item)
        # print('option: %s, "%s"' % (option, combo.itemText(option)))
        index = self.table_widget.indexAt(mtr_combo_item.pos())
        # print('row: %s, column: %s' % (index.row(), index.column()))
        mtr_item = QtWidgets.QTableWidgetItem(mtr_combo_item.itemText(option))
        mtr_item.setData(QtCore.Qt.UserRole, mtr_item_data)
        self.table_widget.setItem(index.row(), index.column(), mtr_item)

    def get_dict_data_combobox(self, combobox_obj):
        index = combobox_obj.currentIndex()
        obj_selected = combobox_obj.itemData(index, QtCore.Qt.UserRole) or ""
        return obj_selected


def hline_widget():
    line = QtWidgets.QFrame()
    line.setFrameShape(QtWidgets.QFrame.HLine)
    line.setFrameShadow(QtWidgets.QFrame.Sunken)
    return line

