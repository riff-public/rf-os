import os
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc
import maya.mel as mm
import pymel.core as pm
import re
# from sg_services import ShotgunServices

from rf_utils.context import context_info
from rf_utils.pipeline.scene_builder import SceneBuilder
from rftool.utils import pipeline_utils
from rf_maya.lib import shot_lib
from rftool.utils import yeti_lib
from rf_app.asm import asm_lib
from rf_maya.rftool.shade import shade_utils
from rf_utils import register_shot
from rf_utils import register_entity
from rf_maya.lib import sequencer_lib
from rf_utils import icon 
from rf_utils import file_utils
from rftool.utils import instance_utils
from rf_utils.pipeline import asset_tag
from rf_utils.pipeline import cache_list
from rf_maya.rftool.fix import moveSceneToOrigin as msto
from rf_utils.pipeline import asset_tag
from rf_app.anim.animIO import core as animIOCore
from rf_maya.rftool.utils import maya_utils
from rf_app.publish.scene.utils import maya_hook as hook

reload(animIOCore)
reload(register_shot)
reload(sequencer_lib)
reload(shot_lib)
reload(shade_utils)
reload(yeti_lib)
reload(instance_utils)
reload(cache_list)
reload(asset_tag)
reload(msto)
reload(icon)
from rf_utils import project_info
context_info.logger.setLevel(logging.WARNING)

COLUMNS_INDEX = {
    'asset': 0,
    'status': 1,
    'material_status': 2,
    'current_step': 3,
    'current_version': 4,
    'department': 5,
    'version': 6,
    'type': 7,
    'materials': 8,
    'build_shade': 9,
    'action': 10,
    'remove': 11
}

class CacheStatus: 
    match = 'match'
    nomatch = 'nomatch'
    needUpdate = 'needUpdate'
    remove = 'remove'
    problem = 'problem'


class SceneBuilderModel:
    def __init__(self):
        self.scene_ctx = context_info.ContextPathInfo()
        self.mtr_items = {}
        self.init_state()
        self.departments = ['layout', 'anim', 'sim', 'finalcam', 'setdress']
        self.asset_dirs_filtered = []
        self.reference_assets = []

    def init_state(self):
        # self.scene_data = []
        self.asm_data = []
        self.read_asm_data()
        # self.set_mtr_items()

    def init_reference_scene(self):
        paths = mc.file(q=True, r=True)
        for path in paths:
            namespace = mc.referenceQuery(path, ns=True)
            refPath = path.split('{')[0]
            entity = context_info.ContextPathInfo(path=refPath, schemaKey='cachePath')
            current_step = entity.step
            current_publish_version = entity.publishVersion

            if namespace[1:].split('_')[1] == "cam" and refPath.split('.')[1] == "hero":
                current_step = "-"
                current_publish_version = "-"
            # entity.path.scheme(key='heroPath')
            # scene.path.scheme(key='heroPath')
            data = {
                'namespace': namespace[1:],
                'step': current_step,
                'version': current_publish_version
            }
            self.reference_assets.append(data)

    def get_asm_data_path(self):
        return self.scene_ctx.path.asm_data().abs_path()

    def get_asm_data(self):
        return self.scene_builder.get_asm_data()

    def get_shot_path(self):
        shot_path = self.scene_ctx.path.name(workspace='publish').abs_path()
        return shot_path

    def get_dirs_scene(self):
        shot_path = self.get_shot_path()
        dirs = os.listdir(shot_path)
        return dirs

    def get_dirs_version_asset(self, department, asset_name):
        shot_path = self.get_shot_path()
        dirs = []
        version_path = "{shot_path}/{department}/output/{asset_name}".format(
            shot_path=shot_path,
            department=department,
            asset_name=asset_name
        )
        if os.path.exists(version_path):
            dirs = os.listdir(version_path)
        return dirs

    def get_assets_by_department(self, department_selected):
        shot_path = self.get_shot_path()
        self.asset_dirs_filtered = []

        for department in self.departments:
            department_assets_path = "{shot_path}/{department}/output/".format(
                shot_path=shot_path,
                department=department
            )
            if os.path.exists(department_assets_path):
                dirs = os.listdir(department_assets_path)

                for asset in dirs:
                    data = self.get_filtered_assets(asset, department)
                    self.asset_dirs_filtered.append(data)

        unique_asset_list = self.unique_asset(self.asset_dirs_filtered, department_selected)

        return unique_asset_list

    def get_reference_asset(self, asset_name):
        for reference_asset in self.reference_assets:
            if reference_asset['namespace'] == asset_name:
                return reference_asset

    def get_filtered_assets(self, asset_name, department):
        version_dirs = self.get_dirs_version_asset(
            department,
            asset_name
        )

        current_step = None
        current_version = None
        reference_asset = self.get_reference_asset(asset_name)
        if reference_asset:
            current_step = reference_asset['step']
            current_version = reference_asset['version']

        data = {
            'asset_name': asset_name,
            'current_step': current_step or "",
            'current_version': current_version or "",
            'department': department,
            'versions': version_dirs
        }

        return data

    def get_mtr_items(self):
        return self.mtr_items

    def get_mtr_asset(self, namespace):
        meterials = []
        for key, value in self.mtr_items.iteritems():
            if namespace == key:
                return value
        return meterials

    def set_mtr_items(self):
        asm_data = self.get_asm_data()
        for key, value in asm_data['assets'].iteritems():
            is_cam = self.is_camera_asset(key)
            if is_cam:
                continue
            entity = context_info.ContextPathInfo(path=value['description'])
            # entity.context.update(step='lookdev', look='main')
            # shade_namespace = entity.mtr_namespace
            # print "entity", entity
            # print "type entity", type(entity)
            # print "shade_namespace", shade_namespace
            mtr_idv_files = self.get_mtr_idv_file(entity)
            data = {
                key: {
                    # 'shade_namespace': shade_namespace,
                    'description_path': value['description'],
                    'hero_path': entity.path.hero().abs_path(),
                    'texture_path': mtr_idv_files
                }
            }
            self.mtr_items.update(data)

    def get_mtr_idv_file(self, entity):
        # print "entity", entity
        file_path = entity.path.hero().abs_path()
        list_file = os.listdir(file_path)
        mtr_idv = []
        keyword_mtr = "_mtr_ldv_"
        for file_name in list_file:
            if keyword_mtr in file_name:
                display_name = self.get_mtr_idv_display_name(file_name)
                entity.context.update(step='lookdev', look=display_name)
                shade_namespace = entity.mtr_namespace
                data = {
                    'display_name': display_name,
                    'look': shade_namespace,
                    'file_path': file_name
                }
                mtr_idv.append(data)
        return mtr_idv

    def get_mtr_idv_display_name(self, file_name):
        display_name = file_name.split('_')[3]
        return display_name

    def read_asm_data(self):
        asm_data_path = self.get_asm_data_path()
        scene_dirs = self.get_dirs_scene()
        reg = register_shot.Register(self.scene_ctx)
        asm_file_path = "{asm_data_path}/{file_path}".format(asm_data_path=asm_data_path, file_path=reg.configHero)
        self.scene_builder = SceneBuilder(asm_file_path)

    def is_camera_asset(self, asset_name):
        tail_namespace = asset_name.split('_')[1]
        if tail_namespace == "cam":
            return True
        return False

    def unique_asset(self, assets_filtered, department_selected):
        unique_list_asset = list(filter(lambda asset: asset['department'] == department_selected, assets_filtered))
        list_asset_name = list(map(lambda asset: asset['asset_name'], unique_list_asset))

        for i, asset in enumerate(assets_filtered):
            if asset['department'] != department_selected:
                if asset['asset_name'] not in list_asset_name:
                    unique_list_asset.append(asset)
                    list_asset_name = list(map(lambda asset: asset['asset_name'], unique_list_asset))
        return unique_list_asset

    def build_scene(self, namespace, step, version, type_item):
        # print "namespace", namespace
        # print "department", step
        # print "version", version
        # print "type_item", type_item
        shot_lib.build_abc(self.scene_ctx, step, namespace, version)

    def remove_abc(self, namespace):
        shot_lib.remove_abc(namespace)

    def apply_ref_shade(self, shadeFile, shadeNamespace, geoNamespace):
        shade_utils.apply_ref_shade(shadeFile, shadeNamespace, geoNamespace)
        # connectRef = shade_utils.find_connect_ref_shade(obj)

    def process_build_update_all(self, department_selected):
        if department_selected == "hero":
            asm_data = self.get_asm_data()
            assets = asm_data.get('assets')
        else:
            assets = self.get_assets_by_department(department_selected)



def build(step='', buildType='', filterList=None, buildFilter=[], muteFilter=[]):
    """ build all hero list """
    scene = context_info.ContextPathInfo()
    reg = register_shot.Register(scene)
    assets = reg.get.asset_list()
    logger.debug('Cache found %s' % assets)

    if filterList: 
        assets = filter_cache(scene, reg, assets)
        logger.debug('Cache filter %s' % assets)

    scene.projectInfo.render.set_size(mode='preview')
    logger.info('Build all %s assets' % len(assets))
    logger.debug(assets)
    logger.info('----------------------')


    if buildFilter: 
        assets = [a for a in assets if a in buildFilter]

    if muteFilter:
        assets = [a for a in assets if not reg.get.check_type(a) in muteFilter]

    for asset in assets:
        steps = get_steps(scene, asset)
        prefer_step = steps[0]
        step = prefer_step if not step else step
        # step = ''
        logger.info('** Build %s' % asset)
        # continue
        asset_build(asset, reg, scene, step, buildType)
        step = '' # reset so it won't use in next iteration
        logger.info('\n')

    pipeline_utils.scene_group()
    logger.info('Finished build')


def get_steps(scene, asset): 
    reg = register_shot.Register(scene)
    fileType = reg.get.check_type(asset)   

    # list step chronological order 
    # hero come first
    steps = reg.version.hero_steps(asset)
    steps.reverse()

    # !! Note prefer step here read from config. 
    # in long term it need to read from data attribute that say 
    # prefer: true in yml data 
    # this part need to be replaced some how.

    if fileType in scene.projectInfo.scene.prefer_built_step.keys(): 
        prefer_step = scene.projectInfo.scene.prefer_built_step[fileType]

        if prefer_step in steps: 
            index = steps.index(prefer_step)
            steps.pop(index)
            steps.insert(0, prefer_step)
    # index = steps.index(latest_step)
    # steps.pop(index)
    # steps.insert(0, latest_step)
    return steps


def filter_cache(entity, reg, assets): 
    # fitler cache list 
    cache = cache_list.CacheList(entity)
    heroList = cache.list_keys_status(status=True)

    filterAssets = []

    # if heroList: 
    for asset in assets: 
        fileType = reg.get.assetData[asset]['type']
        namespace = reg.get.assetData[asset]['namespace']
        exportNs = reg.get.assetData[asset].get('exportNamespace')
        checkName = exportNs if exportNs else namespace 

        filterList = ['abc_cache', 'abc_tech_cache', 'yeti_cache', 'set']
        if 'shotdress' in cache.list_keys():
            filterList.append('shotdress')

        if fileType in filterList: 
            if checkName in heroList: 
                filterAssets.append(asset)
        else: 
            filterAssets.append(asset)

    # else: 
    #     filterAssets = assets

    return filterAssets


def ref_shade(proj, geoNamespace, step='ldv'):
    tagGrp = '%s:%s' % (geoNamespace, proj.asset.geo_grp())
    tag = asset_tag.AssetTag(tagGrp)

    if mc.objExists(tagGrp) and mc.referenceQuery(tagGrp, inr=True):
        mtrDict = shade_utils.list_mtr_files(tagGrp, step)
        if not mtrDict:
            step = 'mdl'
            mtrDict = shade_utils.list_mtr_files(tagGrp, step)
        if not mtrDict:
            step = 'tmp'
            mtrDict = shade_utils.list_mtr_files(tagGrp, step)
        # name = mc.getAttr('%s.assetName' % tagGrp)
        # change the way we get name to adPath instead 
        refPath = mc.getAttr('%s.refPath' % tagGrp)
        name = context_info.ContextPathInfo(path=refPath).name

        look = tag.get_look() or 'main'
        shadeFile = mtrDict.get(look)

        # connect shade Connect for stroke
        if shadeFile:
            asset = context_info.ContextPathInfo(path=shadeFile)
            shadeFileExt = os.path.basename(shadeFile)
            shadeFileName, ext = os.path.splitext(shadeFileExt)

            connectionData = []
            dataFile = asset.output_name(outputKey='shdConnectData', hero=True)
            heroPath = asset.path.hero().abs_path()
            dataFile = '%s/%s' % (heroPath, dataFile)
            if os.path.isfile(dataFile):
                print 'found Connection Data assign it!'
                connectionData = file_utils.ymlLoader(dataFile)
            else:
                print 'ConnectionData not found!'
                if mc.objExists('%s.shdConnect' %tagGrp):
                    connectionStr = mc.getAttr('%s.shdConnect' %tagGrp)
                    connectionData = eval(connectionStr)

            shadeFileName = shadeFileName.split('.')[0]  # get rid of .hero
            shadeKey = shadeFileName.split('_mtr_')[-1]
            connectionInfo = []

            if shadeKey in connectionData:
                connectionInfo = connectionData[shadeKey]

        # connect shade finish

        print 'mtrDict', mtrDict

        if shadeFile:
            shadeNamespace = '%s_%s%sMtr' % (name, step, look.capitalize())

            # separate shader 
            separateShaderAttr = '%s.separateShader' % tagGrp
            separateShader = False
            if mc.objExists(separateShaderAttr): 
                separateShader = mc.getAttr(separateShaderAttr)

                if separateShader: 
                    instance_num = re.findall(r'\d{3}', geoNamespace)[0]
                    shadeNamespace = '{}_{}'.format(shadeNamespace, instance_num)
            # separate shader End
            
            try: 
                shade_utils.apply_ref_shade(shadeFile, shadeNamespace, geoNamespace, connectionInfo)
            except Exception as e: 
                print e
        else:
            pass

def asset_build(asset, reg, scene, step='', buildType=''):
    node = asset
    cacheType = reg.get.check_type(asset)
    cachePath = reg.get.cache(asset, step=step, hero=True)
    cachePath = cachePath if cachePath else reg.get.maya(asset, step=step, hero=True)
    if not cachePath: 
        # this mean preferred step is not hero, pick latest version of this step 
        logger.warning('Pick latest version of {}'.format(step))
        cacheFiles = reg.version.caches(asset, step=step, hero=True)
        cachePath = cacheFiles[-1] if cacheFiles else None # lastest 

    mayaPath = reg.get.maya(asset, step=step, hero=True)
    data = reg.get.asset(asset)
    namespace = data.get(register_shot.Config.namespace)
    node = data.get('node')
    assetDescription = reg.get.custom_key(asset, dataType='description')
    print('asset description', assetDescription)
    assetReg = register_entity.RegisterInfo(assetDescription)

    # valid = True if not buildType else True if cacheType == buildType else False
    valid = True if not buildType else True if cacheType == buildType else True if buildType in register_shot.Config.outputTypes else False

    if valid:
        if cacheType == 'abc_cache':
            if buildType == 'cache': 
                if not assetDescription: 
                    logger.error('No asset description found on "%s". Recache and make sure to resolve qc "add_asset_tag".' % asset)
                    return 
                # asset entity 
                assetPath = assetReg.get.hero_path()
                # P:/Bikey/design/publ/_artbook/mattepaint
                if '_artbook' in assetPath:
                    basePath = assetDescription.split('_data')[0]
                    assetPath = '%shero'%(basePath)

                assetEntity = context_info.ContextPathInfo(path=assetPath) 
                logger.debug('Asset path: %s' % assetPath)

                shot_lib.reference_abc(namespace, cachePath, rebuild=False)
                
                step = 'ldv'
                shadeBuildStep = scene.projectInfo.scene.shade_build_step
                if scene.step in ['setdress', 'techanim', 'anim']:
                    step = 'rig'

                    tagGrp = '%s:%s' % (namespace, scene.projectInfo.asset.geo_grp())
                    tag = asset_tag.AssetTag(tagGrp)
                    assetType = tag.get_type()
                    if assetType == 'char':
                        step = shadeBuildStep if shadeBuildStep else step

                ref_shade(scene.projectInfo, namespace, step)

                # set uv tilligMode
                if shadeBuildStep and step == 'mdl':
                    allFileNodes = mc.ls(type="file")
                    for f in allFileNodes:
                        if not mc.getAttr("{}.useFrameExtension".format(f)):
                            continue
                        mc.setAttr("{}.uvTilingMode".format(f), 0)
                
                # hidden geo 
                if scene.step != 'setdress':
                    hiddenGeos = pipeline_utils.read_hiddenGeo_data(assetEntity)
                    shot_lib.hidden_geo(asset, hiddenGeos)

                # shift back for project matched config 
                cacheOrigin = scene.projectInfo.scene.cache_at_origin
                ctrl = '%s:%s' % (asset, scene.projectInfo.asset.geo_grp())
                offsetAttr = mc.objExists('%s.%s' % (ctrl, asset_tag.Attr.cacheOffset))

                if cacheOrigin and offsetAttr: 
                    offsetValue, allTransforms = msto.get_cameraDistanceFromOrigin(scene)
                    msto.offset_asset(obj=ctrl, offset=offsetValue, toOrigin=False, isCache=True)
                    logger.info('Shifting %s "%s"' % (ctrl, offsetValue))
            
            if buildType == 'animCurve': 
                if mc.objExists('BaseAnimation'): 
                    state = mc.animLayer('BaseAnimation', q=True, lock=True)
                    if state: 
                        mc.animLayer('BaseAnimation', e=True, lock=False)

                remove(asset, reg, scene)
                data = reg.get.asset(namespace)
                animFile = data.get('animCurve').get('heroFile')
                sf, ef, ssf, sef = sequencer_lib.shot_info(scene.name_code)
                rootGrp = scene.projectInfo.asset.get('allMoverCtrl') or 'AllMover_Ctrl'
                rootNode = '%s:%s' % (namespace, rootGrp)
                
                metaData = animIOCore.readMetaData(path=animFile)
                rigFile = metaData['ref_path']

                if os.path.exists(animFile): 
                    ref = pm.createReference(rigFile, namespace=namespace)
                    metaData = animIOCore.readMetaData(path=animFile)
                    animData = animIOCore.AnimData(name=namespace, path=animFile, start=sf, end=ef)
                    animAsset = animIOCore.AnimAsset(root=rootNode)
                    import_result = animAsset.import_anim(data=animData, replaceCompletely=True)


        if cacheType == 'ma_rsproxy':
            # asset entity 
            assetPath = assetReg.get.hero_path()
            assetEntity = context_info.ContextPathInfo(path=assetPath) 
            logger.debug('Asset path: %s' % assetPath)
            
            data = reg.get.custom_key(asset, step=step, dataType='maRsproxy')
            cachePath = data[register_shot.Config.heroFile]
            shot_lib.reference_abc(namespace, cachePath, rebuild=False)
            

        if cacheType == 'camera':
            if os.path.exists(cachePath):
                # use maya for these department 
                # if scene.step in ['layout', 'anim', 'finalcam']: 
                #     cachePath = mayaPath

                scene.projectInfo.render.set_size()
                startFrame = scene.projectInfo.render.start_frame()
                # this is a workaround. should be replaced with proper solution later. 
                # this will return a value if it need pre and post roll include in duration 
                inpreRoll, inpostRoll = sequencer_lib.in_pre_post_roll()
                duration = sequencer_lib.sync_range()
                rangeDuration = duration + inpreRoll + inpostRoll
                sequencer_lib.set_range(startFrame, startFrame + rangeDuration - 1)
                camera = shot_lib.build_camera(namespace, cachePath)
                shot_lib.set_camera_value(camera, scene)
                # guess sequence shot by using namespace 
                shotNode = namespace.replace('_cam', '')
                # shot_lib.build_sequencer(scene.name_code, camera, duration)
                shot_lib.build_sequencer(shotNode, camera, duration, inpreRoll, inpostRoll)
                #lock node camera
                cam = hook.find_cam(scene.name_code)
                maya_utils.lock_transform_rotate(cam)


        if cacheType == 'yeti_cache':
            # import cache 
            yeti_lib.import_cache(node, cachePath, namespace=namespace, override=False)
            # apply shade 
            step = 'ldv' 
            if scene.step in ['techanim', 'anim']: 
                step = 'rig'
            ref_shade(scene.projectInfo, namespace, step)
            
            # setup display yeti  
            displayFile = assetReg.get.groom_yeti_display().get('heroFile')
            if displayFile: 
                yetiDisplayData = file_utils.ymlLoader(displayFile) if os.path.exists(displayFile) else dict()
                yeti_lib.set_display(yetiDisplayData, node, namespace)

        if cacheType == 'import_data':
            data = shot_lib.import_reference(cachePath, asset)
            if mc.objExists('Crowd_Grp'):
                allC = mc.listRelatives('Crowd_Grp', allDescendents=True, type='transform', fullPath=True)
                if allC:
                    from rf_maya.rftool.crowd_generate import build
                    mc.select(allC)
                    buildType = 'rsProxy'
                    locs = pm.ls(sl=True)
                    build.build(locs, buildType)

        if cacheType == 'ref_data': 
            shot_lib.reference_abc(namespace, cachePath, rebuild=False)

        if cacheType == 'set':
            setOverride = reg.get.custom_data(asset, step=step, dataType=register_shot.Config.setOverride, hero=True)
            setShot = reg.get.custom_data(asset, step=step, dataType=register_shot.Config.setShot, hero=True)
            setDescription = reg.get.custom_key(asset, step=step, dataType=register_shot.Config.setDescription)
            mayaFile = reg.get.maya(asset, step=step, hero=True)
            # shot_lib.import_reference(mayaFile, asset)
            shot_lib.build_set(asset, setDescription, setOverride, namespace=namespace)

        if cacheType == 'shotdress': 
            shotdress = reg.get.custom_data(asset, step=step, dataType=register_shot.Config.shotDress, hero=True)
            asm_lib.paste('ShotDress_Grp', shotdress, False)
            asm_lib.build('ShotDress_Grp')
            asm_lib.remove_hidden_child('ShotDress_Grp')

        if cacheType == 'instdress': 
            instdressFile = reg.get.custom_data(asset, step=step, dataType=register_shot.Config.maya, hero=True)
            instance_utils.build_instance_shotDress(instdressFile, scene)

        if cacheType == 'fx_cache': 
            if scene.step in ['light']: 
                cachePath = reg.get.maya(asset, step=step, hero=True)
            shot_lib.reference_abc(namespace, cachePath, rebuild=False)


    return cachePath


def remove(asset, reg, scene, step='', buildType=''):
    node = asset
    cacheType = reg.get.check_type(asset)
    cachePath = reg.get.cache(asset, step=step, hero=True)
    data = reg.get.asset(asset)
    namespace = data.get(register_shot.Config.namespace)
    node = data.get('node')

    if cacheType in ['abc_cache', 'fx_cache', 'ma_rsproxy', 'ref_data']:
        shot_lib.remove_abc(namespace)
        # remove other element that share namespace - groom
        assets = [a for a in reg.get.asset_list() if asset in a]
        # for other in assets:
        #     if not other == asset:
        #         remove(other, reg, scene, step, buildType)

    if cacheType in ['camera']:
        shot_lib.remove_abc(namespace)

    if cacheType == 'yeti_cache':
        yeti_lib.remove_cache(node, namespace)
        remove_namespace(namespace)
    if cacheType == 'import_data':
        shot_lib.remove_import_group(asset)

    if cacheType == 'set': 
        asm_lib.mc.select(asset)
        cmd = asm_lib.UiCmd()
        cmd.remove_set()

    if cacheType == 'shotdress': 
        asm_lib.Asm('ShotDress_Grp').remove_root()

    if cacheType == 'instdress': 
        instance_utils.remove_instance_shotDress()


def reload_asset(asset, reg, scene, step='', buildType=''):
    node = asset
    cacheType = reg.get.check_type(asset)
    mayaPath = reg.get.maya(asset, step=step, hero=True)
    data = reg.get.asset(asset)
    namespace = data.get(register_shot.Config.namespace)
    node = data.get('node')
    assetDescription = reg.get.custom_key(asset, dataType='description')
    assetReg = register_entity.RegisterInfo(assetDescription)

    if cacheType in ['camera', 'abc_cache', 'fx_cache', 'ma_rsproxy', 'ref_data']:
        shot_lib.reload_reference(namespace)


def remove_ref_shade(proj, geoNamespace):
    tagGrp = '%s:%s' % (geoNamespace, proj.asset.geo_grp())
    if mc.objExists(tagGrp) and mc.referenceQuery(tagGrp, inr=True):
        mtrDict = shade_utils.list_mtr_files(tagGrp)
        name = mc.getAttr('%s.assetName' % tagGrp)
        step = 'ldv'
        look = 'main'
        # hard code to "main"
        shadeFile = mtrDict.get(look)

        if shadeFile:
            shadeNamespace = '%s_%s%sMtr' % (name, step, look.capitalize())
            shot_lib.remove_abc(shadeNamespace)


def remove_namespace(namespace):
    if not mc.namespaceInfo(namespace, ls=True):
        mc.namespace(rm=namespace)

def check_status(asset, reg, step):
    """ check status of the current cache in scene """ 
    cacheType = reg.get.check_type(asset)
    cachePath = reg.get.cache(asset, step=step, hero=True)
    if not cachePath: 
        # this mean preferred step is not hero, pick latest version of this step 
        logger.warning('Pick latest version of {}'.format(step))
        cacheFiles = reg.version.caches(asset, step=step)
        cachePath = cacheFiles[-1] if cacheFiles else None # lastest 
    mayaPath = reg.get.maya(asset, step=step, hero=True)
    data = reg.get.asset(asset)
    node = data.get('node')
    returnStatus = {'status': CacheStatus.nomatch, 'path': '', 'icon': icon.no}

    if cacheType in ['abc_cache', 'fx_cache', 'ma_rsproxy', 'ref_data']:
        sceneRefs = [a.split('{')[0] for a in mc.file(q=True, r=True)]
        
        if cacheType == 'ma_rsproxy': 
            maRsproxy = reg.get.custom_key(asset, step=step, dataType='maRsproxy')
            cachePath = maRsproxy[register_shot.Config.heroFile]

        if cacheType in ['ref_data']: 
            cachePath = mayaPath

        if cacheType == 'fx_cache': 
            cachePaths = [cachePath, mayaPath]
            if any(a in sceneRefs for a in cachePaths): 
                return {'status': CacheStatus.match, 'path': mayaPath, 'icon': icon.sgAprv}

            # if a for a in sceneRefs
            # return {'status': CacheStatus.match, 'path': cachePath, 'icon': icon.sgAprv}

        if cachePath in sceneRefs:
            return {'status': CacheStatus.match, 'path': cachePath, 'icon': icon.sgAprv}
        else: 
            assetInPath = [a for a in sceneRefs if asset in a]
            if assetInPath: 
                return {'status': CacheStatus.needUpdate, 'path': assetInPath[0], 'icon': icon.needFix}
            return {'status': CacheStatus.nomatch, 'path': assetInPath, 'icon': icon.no}

    if cacheType in ['camera']:
        sceneRefs = [a.split('{')[0] for a in mc.file(q=True, r=True)]
        if cachePath in sceneRefs or mayaPath in sceneRefs:
            return {'status': CacheStatus.match, 'path': cachePath, 'icon': icon.sgAprv}
        else: 
            assetInPath = [a for a in sceneRefs if asset in a]
            if assetInPath: 
                return {'status': CacheStatus.needUpdate, 'path': assetInPath[0], 'icon': icon.needFix}
            return {'status': CacheStatus.nomatch, 'path': assetInPath, 'icon': icon.no}

    if cacheType == 'yeti_cache':
        if cachePath in [mc.getAttr('%s.cacheFileName' % a) for a in mc.ls(type='pgYetiMaya')]:
            return {'status': CacheStatus.match, 'path': cachePath, 'icon': icon.sgAprv}

    if cacheType == 'maya':
        if node:
            if mc.objExists(node):
                return {'status': CacheStatus.match, 'path': node, 'icon': icon.sgAprv}

    if cacheType == 'set': 
        loc = asm_lib.MayaRepresentation(asset)
        if loc.is_asm(): 
            return {'status': CacheStatus.match, 'path': loc.asm_data, 'icon': icon.sgAprv}

    if cacheType == 'shotdress': 
        cachePath = reg.get.custom_data(asset, step=step, dataType=register_shot.Config.shotDress, hero=True)
        scene = context_info.ContextPathInfo(path=cachePath, schemaKey='cachePath')

        if mc.objExists(scene.projectInfo.asset.shotdress_grp()): 
            shotdress_gpr = scene.projectInfo.asset.shotdress_grp()
            loc = asm_lib.MayaRepresentation(shotdress_gpr)
            if len(mc.ls(shotdress_gpr)) > 1: 
                message = 'Error: // More than 1 "{}". Please remove other groups!!'.format(shotdress_gpr)
                return {'status': CacheStatus.problem, 'path': '', 'icon': icon.needFix2, 'message': message}
            
            if cachePath == loc.asm_data: 
                return {'status': CacheStatus.match, 'path': loc.asm_data, 'icon': icon.sgAprv}
    
    if cacheType == 'instdress': 
        cachePath = mayaPath
        scene = context_info.ContextPathInfo(path=cachePath, schemaKey='cachePath')
        
        if mc.objExists(scene.projectInfo.asset.instance_grp()): 
            refs = instance_utils.get_instance_dependency(scene)
            
            if cachePath in refs: 
                return {'status': CacheStatus.match, 'path': cachePath, 'icon': icon.sgAprv}

    if cacheType == 'import_data': 
        cachePath = mayaPath
        scene = context_info.ContextPathInfo(path=cachePath, schemaKey='cachePath')
        
        if asset == scene.projectInfo.asset.export_grp() and mc.objExists(scene.projectInfo.asset.export_grp()): 
            return {'status': CacheStatus.match, 'path': cachePath, 'icon': icon.sgAprv}

        elif asset == scene.projectInfo.asset.crowd_grp() and mc.objExists(scene.projectInfo.asset.crowd_grp()): 
            return {'status': CacheStatus.match, 'path': cachePath, 'icon': icon.sgAprv}

        elif asset == scene.projectInfo.asset.crowd_rs_grp() and mc.objExists(scene.projectInfo.asset.crowd_rs_grp()): 
            return {'status': CacheStatus.match, 'path': cachePath, 'icon': icon.sgAprv}


    return returnStatus


def remove_status(): 
    return {'status': CacheStatus.remove, 'icon': icon.needFix2}

