_title = 'Riff Scene Builder'
_version = 'v.0.1.1'
_des = 'Add skip setdress'
uiName = 'SceneBuilderUI'

import sys
import os
import getpass
import logging
import json
import traceback
from functools import partial

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# import config
import rf_config as config

from rf_utils import log_utils, icon
from rf_utils.ui import load, stylesheet

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import QtWidgets, QtCore, QtGui

from model import scene_builder_model
from model.scene_builder_model import SceneBuilderModel, COLUMNS_INDEX
from rf_utils import register_shot
from rf_utils.context import context_info
from rftool.utils import pipeline_utils
from rf_utils.pipeline import user_pref
from rf_utils import project_info
from rf_utils import file_utils 
from rf_utils.widget import dialog

from view import ui
reload(ui)
reload(scene_builder_model)

UI_PATH = "%s/view/ui.py" % moduleDir
CONFIG_FILE = "%s/config.yml" % moduleDir
CONFIG = file_utils.ymlLoader(CONFIG_FILE)


class ScenceBuilderCtrl(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(ScenceBuilderCtrl, self).__init__(parent)
        self.ui = ui.SceneBuilderUI(parent)
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.set_logo()
        self.resize(1200, 700)
        
        # data 
        self.userPref = user_pref.ToolSetting(uiName)
        
        self.init_state()
        self.init_signal()

    def init_state(self):

        self.scene = context_info.ContextPathInfo()
        if self.scene.path:
            # self.scene_builder_model = SceneBuilderModel()
            # self.scene_builder_model.init_reference_scene()
            self.reg = register_shot.Register(self.scene)
            self.set_FPS()
            # print "mtr_items", mtr_items
            # self.ui.set_material_items(mtr_items)
        else:
            QtWidgets.QMessageBox.critical(self, "Message", "Need import scene file!.")

        # default ui status 
        self.ui.filter_checkBox.setChecked(True)
        self.ui.reGroup_checkBox.setChecked(True)
        self.ui.remove_btn.setVisible(False)

    def init_signal(self):
        # navigation 
        self.ui.projectWidget.projectChanged.connect(self.project_changed)

        # list caches 
        self.ui.department_list_view.itemClicked.connect(self.load_scenes)
        self.ui.build_update_btn.clicked.connect(partial(self.exec_build_update_all, 'build'))
        self.ui.reload_btn.clicked.connect(partial(self.exec_build_update_all, 'reload'))
        self.ui.remove_btn.clicked.connect(partial(self.exec_build_update_all, 'remove'))
        self.ui.table_widget.cellClicked.connect(self.table_activated)
        self.ui.filter_checkBox.stateChanged.connect(self.load_scenes)

    def set_FPS(self):
        scene = self.scene
        self.projectInfo = project_info.ProjectInfo(project=scene.project)
        fps = self.projectInfo.render.fps_unit()
        try: 
            import maya.cmds as mc
            mc.currentUnit(time=fps)
        except: 
            pass 

    def load_scenes(self):
        self.ui.table_widget.clearContents()
        self.ui.table_widget.setRowCount(0)

        department = self.ui.department_list_view.currentItem().text()
        if department == "hero":
            self.list_hero_asset(department)
        else:
            # self.list_active_department_asset(department)
            self.list_department_asset(department)


    def project_changed(self, project): 
        """ what happen when project changed """
        self.ui.sgWidget.list_episodes(project)
        # save ui selection
        self.save_last_selection()

    def save_last_selection(self):
        """ save selection to json file """
        data = dict()
        project = str(self.ui.projectWidget.projectComboBox.currentText())
        episode = str(self.ui.sgWidget.episodeWidget.comboBox.currentText())
        data['project'] = project
        data['episode'] = episode

        result = self.userPref.write(data)

    def build_scene_ctrl(self, row):
        self.loading_start()

        skip = ["shotdress", "set"]

        try:
            # retrieve info
            asset_item = self.ui.table_widget.item(row, ui.TableConfig.column.keys().index('asset')).text()
            data = self.ui.table_widget.item(row, ui.TableConfig.column.keys().index('asset')).data(QtCore.Qt.UserRole)
            # department_item = self.ui.table_widget.item(row, ui.TableConfig.column.keys().index('department')).text()
            department_item = self.ui.table_widget.cellWidget(row, ui.TableConfig.column.keys().index('department')).currentText()
            version_item = self.ui.table_widget.item(row, ui.TableConfig.column.keys().index('version')).text()
            type_item = self.ui.table_widget.item(row, ui.TableConfig.column.keys().index('type')).text()
            build_type = self.ui.table_widget.cellWidget(row, ui.TableConfig.column.keys().index('output')).currentText()
            prefer_step = self.ui.table_widget.cellWidget(row, ui.TableConfig.column.keys().index('department')).itemText(0)

            # check if the skip setdress is checked #
            if self.ui.skipSet_checkBox.isChecked():
                if type_item in skip:
                    return

            # action
            cachePath = scene_builder_model.asset_build(asset_item, self.reg, self.scene, step=department_item, buildType=build_type)
            if self.ui.reGroup_checkBox.isChecked():
                pipeline_utils.scene_group()

        except Exception as e:
            error = traceback.format_exc()
            traceback.print_exc()
            logger.error(error)
            QtWidgets.QMessageBox.critical(self, "Message", "Error:" + str(e) + ". Please contact admin.")

        finally:
            status = scene_builder_model.check_status(asset_item, self.reg, prefer_step)
            self.ui.update_status(row, status)
            self.loading_stop()

    def remove_reference_scene(self, row):
        self.loading_start()
        QtCore.QCoreApplication.processEvents()

        try:
            asset_item = self.ui.table_widget.item(row, 0).text()
            department_item = self.ui.table_widget.item(row, COLUMNS_INDEX['department']).text()
            scene_builder_model.remove(asset_item, self.reg, self.scene, step=department_item)
            # self.scene_builder_model.remove_abc(asset_item)
            # self.refresh_scene_builder()
        except Exception as e:
            QtWidgets.QMessageBox.critical(self, "Message", "Error:" + str(e) + ". Please contact admin.")

        finally:
            status = scene_builder_model.check_status(asset_item, self.reg, department_item)
            # self.ui.update_status(row, status)
            self.check_all_status()
            self.loading_stop()


    def reload_scene_ctrl(self, row):
        self.loading_start()

        try:
            # retrieve info
            asset_item = self.ui.table_widget.item(row, ui.TableConfig.column.keys().index('asset')).text()
            data = self.ui.table_widget.item(row, ui.TableConfig.column.keys().index('asset')).data(QtCore.Qt.UserRole)
            department_item = self.ui.table_widget.item(row, ui.TableConfig.column.keys().index('department')).text()
            version_item = self.ui.table_widget.item(row, ui.TableConfig.column.keys().index('version')).text()
            type_item = self.ui.table_widget.item(row, ui.TableConfig.column.keys().index('type')).text()

            # action
            cachePath = scene_builder_model.reload_asset(asset_item, self.reg, self.scene, step=department_item, buildType='')

        except Exception as e:
            error = traceback.format_exc()
            traceback.print_exc()
            logger.error(error)
            QtWidgets.QMessageBox.critical(self, "Message", "Error:" + str(e) + ". Please contact admin.")

        finally:
            status = scene_builder_model.check_status(asset_item, self.reg, department_item)
            self.ui.update_status(row, status)
            self.loading_stop()

    def check_all_status(self):
        rows = self.ui.table_widget.rowCount()
        for row in range(rows):
            data, asset = self.ui.get_table_data(row)
            department_item = self.ui.table_widget.item(row, COLUMNS_INDEX['department']).text()
            status = scene_builder_model.check_status(asset, self.reg, department_item)
            self.ui.update_status(row, status)

    def sync_shade(self, row):
        self.loading_start()
        QtCore.QCoreApplication.processEvents()
        try:
            asset_item = self.ui.table_widget.item(row, COLUMNS_INDEX['asset']).text()
            mtr_item = self.ui.table_widget.item(row, COLUMNS_INDEX['materials']).data(QtCore.Qt.UserRole)
            shade_file = "{hero_path}/{material_file}".format(
                hero_path=mtr_item['hero_path'],
                material_file=mtr_item['texture_selected']['file_path']
            )
            self.scene_builder_model.apply_ref_shade(
                shade_file,
                mtr_item['texture_selected']['look'],
                asset_item
            )
        except Exception as e:
            QtWidgets.QMessageBox.critical(self, "Message", "Error:" + str(e) + ". Please contact admin.")
        finally:
            self.loading_stop()

        # return mtr_item

    def list_hero_asset2(self, department):
        asm_data = self.scene_builder_model.get_asm_data()
        list_asset_name = asm_data.get('assets')
        for asset_name in list_asset_name:
            version_dirs = self.scene_builder_model.get_dirs_version_asset(
                list_asset_name[asset_name]['step'],
                asset_name
            )
            reference_asset = self.scene_builder_model.get_reference_asset(asset_name)
            is_camera_asset = self.scene_builder_model.is_camera_asset(asset_name)
            if is_camera_asset:
                version_dirs.insert(0, "hero")

            current_step = None
            current_version = None
            if reference_asset:
                current_step = reference_asset['step']
                current_version = reference_asset['version']

            if current_version is None:
                status = "No"
                action_txt = 'Build'
            else:
                status = "Yes"
                action_txt = 'Update'

            materials = self.scene_builder_model.get_mtr_asset(asset_name)

            self.ui.add_row(
                asset=asset_name,
                status=status,
                department=list_asset_name[asset_name]['step'],
                current_step=current_step or "",
                current_version=current_version or "",
                version=version_dirs,
                type_file=['cache', 'rig'],
                materials=materials,
                action=action_txt,
                action_func=self.build_scene_ctrl,
                action_remove_func=self.remove_reference_scene,
                action_sync_shade=self.sync_shade
            )

    def list_hero_asset(self, step):
        def get_steps(latest_step, asset): 
            fileType = self.reg.get.check_type(asset)   

            # list step chronological order 
            # hero come first
            steps = self.reg.version.hero_steps(asset)
            steps.reverse()

            # !! Note prefer step here read from config. 
            # in long term it need to read from data attribute that say 
            # prefer: true in yml data 
            # this part need to be replaced some how.

            if fileType in self.projectInfo.scene.prefer_built_step.keys(): 
                prefer_step = self.projectInfo.scene.prefer_built_step[fileType]

                if prefer_step in steps: 
                    index = steps.index(prefer_step)
                    steps.pop(index)
                    steps.insert(0, prefer_step)
            # index = steps.index(latest_step)
            # steps.pop(index)
            # steps.insert(0, latest_step)
            return steps

        assets = self.reg.get.asset_list()
        filterAssets = assets
        if self.ui.filter_checkBox.isChecked(): 
            filterAssets = scene_builder_model.filter_cache(self.scene, self.reg, assets)
        # assets = filter_cache_list(self.reg, ['anim'])
        problems = list()
        
        for asset in assets:
            # print reg.get.assetData[asset]
            fileType = self.reg.get.assetData[asset]['type']

            step = self.reg.get.assetData[asset]['step']
            steps = get_steps(step, asset)
            prefer_step = steps[0] # prefered step will be 1 st list

            cache = self.reg.get.cache(asset, step=step, hero=True)
            output_types = self.reg.get.output_types(asset)
            data = [self.reg, asset]
            # print 'prefer_step', prefer_step
            status = scene_builder_model.check_status(asset, self.reg, prefer_step)
            ldvShade = True
            reloadStatus = False

            if fileType == 'abc_cache':
                action_txt = 'Build Cache'
                reloadStatus = True

            elif fileType == 'yeti_cache':
                action_txt = 'Build Yeti'

            elif fileType == 'camera':
                action_txt = 'Build Camera'
                reloadStatus = True

            elif fileType == 'set': 
                action_txt = 'Build Set'

            elif fileType == 'shotdress': 
                action_txt = 'Build ShotDress'

            elif fileType == 'instdress': 
                action_txt = 'Build Ins Dress'

            elif fileType == 'fx_cache':  
                action_txt = 'Build fx'
                reloadStatus = True

            elif fileType == 'import_data':  
                action_txt = 'Import data'
                reloadStatus = False

            elif fileType == 'ma_rsproxy':  
                action_txt = 'Build RsProxy'
                reloadStatus = True

            elif fileType == 'ref_data':  
                action_txt = 'Build file'
                reloadStatus = True

            # elif fileType == 'abc_tech_cache': 
            #     action_txt = 'Build Cache'

            else: 
                # others type don't build 
                continue

            add = True if asset in filterAssets else False
            if add == False and status.get('status') == scene_builder_model.CacheStatus.match: 
                if not asset in filterAssets: 
                    removeStatus = scene_builder_model.remove_status()
                    status.update(removeStatus)
                    add = True 

            if add: 
                self.ui.add_row(
                                asset=asset,
                                status=status,
                                department=steps,
                                output_types=output_types,
                                # current_step=current_step or "",
                                # current_version=current_version or "",
                                # version=version_dirs,
                                type_file=fileType,
                                # materials=materials,
                                action=action_txt,
                                action_func=self.build_scene_ctrl,
                                action_func2=self.reload_scene_ctrl,
                                action_remove_func=self.remove_reference_scene,
                                action_sync_shade=self.sync_shade,
                                data=data
                                )

                overallRow = self.ui.table_widget.rowCount()
                currentRow = overallRow - 1
            
                # set reload Button 
                reloadButtonIndex = ui.TableConfig.column.keys().index('action2')
                reloadButton = self.ui.table_widget.cellWidget(currentRow, reloadButtonIndex)
                reloadButton.setEnabled(reloadStatus)

                if status.get('status') == scene_builder_model.CacheStatus.problem: 
                    buildButtonIndex = ui.TableConfig.column.keys().index('action')
                    buildButton = self.ui.table_widget.cellWidget(currentRow, buildButtonIndex)
                    buildButton.setEnabled(False)
                    problems.append(status.get('message'))

        if problems: 
            dialog.ScrollMessageBox.show('Error', 'Please resolve errors before continue', problems)



    def list_department_asset(self, step):
        assets = self.reg.get.asset_list()

        for row, asset in enumerate(assets):
            # print reg.get.assetData[asset]
            fileType = self.reg.get.assetData[asset]['type']
            heroStep = self.reg.get.assetData[asset]['step']
            cache = self.reg.get.cache_local(asset, step=step, hero=True)
            disabled_row = True if not cache else False
            data = [self.reg, asset]
            status = scene_builder_model.check_status(asset, self.reg, step)

            if fileType == 'abc_cache':
                action_txt = 'Build Cache'

            if fileType == 'yeti_cache':
                action_txt = 'Build Yeti'

            if fileType == 'camera':
                action_txt = 'Build Camera'

            if fileType == 'shotdress': 
                action_txt = 'Build ShotDress'

            self.ui.add_row(
                            asset=asset,
                            status=status,
                            department=step,
                            # current_step=current_step or "",
                            # current_version=current_version or "",
                            # version=version_dirs,
                            type_file=fileType,
                            # materials=materials,
                            action=action_txt,
                            action_func=self.build_scene_ctrl,
                            action_remove_func=self.remove_reference_scene,
                            action_sync_shade=self.sync_shade,
                            data=data, 
                            disabled=disabled_row
                            )

            QtWidgets.QApplication.processEvents()


    def table_activated(self, row, column):
        reg, asset = self.ui.get_table_data(row)
        step = reg.get.assetData[asset]['step']
        cache = reg.get.cache(asset, step=step, hero=True)
        data = self.ui.get_status(row)
        self.ui.status_label.setText('Cache - %s' % data)

    def set_logo(self):
        self.ui.logo_label.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))

    def exec_build_update_all(self, mode='build'):
        all_rows = self.ui.table_widget.rowCount()
        rowRange = xrange(0, all_rows)
        selectedRow = self.ui.selected_checkBox.isChecked()

        if selectedRow: 
            items = self.ui.table_widget.selectedItems()
            rowRange = [self.ui.table_widget.row(a) for a in items]

        if rowRange: 
            for row in rowRange:
                if mode == 'build': 
                    self.build_scene_ctrl(row)
                if mode == 'reload': 
                    self.reload_scene_ctrl(row)
                if mode == 'remove': 
                    self.remove_reference_scene(row)

    def refresh_scene_builder(self):
        self.init_state()
        self.load_scenes()

    def loading_start(self):
        self.ui.progrss_text_label.setVisible(True)

    def loading_stop(self):
        self.ui.progrss_text_label.setVisible(False)





def filter_cache_list(reg, steps=['anim']): 
    assets = reg.get.asset_list()
    assetData = reg.get.assetData
    newList = []

    for asset in assets: 
        output = register_shot.Output(assetData.get(asset))
        if output.get_type() == 'abc_cache': 
            if output.get_step() in steps: 
                newList.append(asset)
        else: 
            newList.append(asset)
    return newList


def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = ScenceBuilderCtrl(maya_win.getMayaWindow())
        myApp.show()
        return myApp
    else:
        logger.info('Run in ScenceBuilderCtrl\n')
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        myApp = ScenceBuilderCtrl()
        myApp.show()
        stylesheet.set_default(app)
        sys.exit(app.exec_())


if __name__ == '__main__':
    show()
