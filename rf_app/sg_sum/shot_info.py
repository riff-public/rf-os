from openpyxl import Workbook
from openpyxl.styles import Alignment, colors, fills, Font, Border, Side
from openpyxl.utils import get_column_letter

import os
import sys
from datetime import datetime
sys.path.append('{}/core'.format(os.environ['RFSCRIPT']))

from rf_app.time_logged import sg_time_log_utils as stlg
from collections import OrderedDict
sg = stlg.sg

class Config_STYLES:
	Fill = {'blue': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='333f4f')),
			'green': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='93c47d'))}

	Border = {'white': Border(left=Side(border_style='thin',color=colors.Color(rgb='ffffff')),
                            right=Side(border_style='thin',color=colors.Color(rgb='ffffff')),
                            top=Side(border_style='thin',color=colors.Color(rgb='ffffff')),
                            bottom=Side(border_style='thin',color=colors.Color(rgb='ffffff')))}

	Font = {'white': Font(color=colors.Color(rgb='ffffff'))}

	Alignment = {'center': Alignment(horizontal='center', vertical='center')}

class main:
	def __init__(self):
		desktop_dir = os.path.join(os.environ['HOMEDRIVE'], os.environ["HOMEPATH"], "Desktop")
		today = datetime.today().strftime('%Y-%m-%d_%H-%M-%S')
		namefile = '%s%s%s'%('Hanuman_Anim_',today,'.xlsx')
		path_home = os.path.join(desktop_dir, namefile)

		wb = Workbook()
		ws = wb.create_sheet('shot', 0)
		ws.title = 'shot'
		sumInfo = self.project_sum_info()
		self.set_header(worksheet=ws, data=sumInfo)
		self.set_data_shot(worksheet=ws, data=sumInfo)

		wb.save(path_home)
		print(path_home)

	def set_header(self, worksheet='', data=[], start_col_num=1, start_row_num=1):
		header = ['No.','shot']
		worksheet.freeze_panes = worksheet['A2']
		worksheet.row_dimensions[1].height = 25

		for k in data.keys():
			for i, v in data[k].items():
				if i not in header:
					header.append(i)
		for i, head in enumerate(header):
			col = get_column_letter(start_col_num)
			worksheet[f'{col}{start_row_num}'] = head
			worksheet[f'{col}{start_row_num}'].alignment = Config_STYLES.Alignment['center']
			worksheet[f'{col}{start_row_num}'].fill = Config_STYLES.Fill['blue']
			worksheet[f'{col}{start_row_num}'].font = Config_STYLES.Font['white']
			worksheet[f'{col}{start_row_num}'].border = Config_STYLES.Border['white']

			if start_col_num == 1:
				worksheet.column_dimensions[col].width = 8
			elif start_col_num == 2:
				worksheet.column_dimensions[col].width = 25
			else:
				worksheet.column_dimensions[col].width = 15

			start_col_num += 1

	def set_data_shot(self, worksheet='', data=[], start_col_num=2, start_row_num=2):
		num = 1
		for k in sorted(data.keys()):
			shot_col = get_column_letter(start_col_num)
			worksheet[f'{shot_col}{start_row_num}'] = k
			worksheet[f'A{start_row_num}'] = num
			worksheet[f'A{start_row_num}'].alignment = Alignment(horizontal='center', vertical='center')

			start_col = (start_col_num + 1)
			for i, value in data[k].items():
				col_num = get_column_letter(start_col)
				worksheet[f'{col_num}{start_row_num}'] = value
				worksheet[f'{col_num}{start_row_num}'].alignment = Alignment(horizontal='center', vertical='center')

				if worksheet[f'{col_num}{start_row_num}'].value == 'apr':
					worksheet[f'{col_num}{start_row_num}'].fill = Config_STYLES.Fill['green']
					worksheet[f'{col_num}{start_row_num}'].border = Config_STYLES.Border['white']

				start_col += 1
			start_row_num += 1
			num += 1

	def project_sum_info(self):
		project = 'Hanuman'
		taskname = 'anim'
		filters = [['project.Project.name', 'is', project], ['content', 'is', taskname]]
		fields = ['id', 'due_date', 'sg_status_list', 'entity', 'start_date', 'duration']
		tasks = sg.find('Task', filters, fields)
		success = []

		filters = [['project.Project.name', 'is', 'Hanuman'], ['content', 'is', 'techAnim']]
		techs = sg.find('Task', filters, fields)

		filters = [['project.Project.name', 'is', 'Hanuman'], ['sg_status_list', 'is_not', 'omt'], ['content', 'is', 'sim']]
		sims = sg.find('Task', filters, fields)

		shots = sg.find('Shot', [['project.Project.name', 'is', 'Hanuman'], ['sg_shot_type', 'is', 'Shot']], ['code', 'sg_working_duration'])
		shots = [a for a in shots if 'act1' in a['code']]

		info = OrderedDict()
		techInfo = dict()
		sumInfo = OrderedDict()
		simInfo = dict()

		for task in tasks:
		    if task['due_date']: 
		        info[task['entity']['name']] = task

		for task in techs: 
			if task['due_date']: 
				techInfo[task['entity']['name']] = task

		for task in sims: 
			if task['due_date']: 
				simInfo[task['entity']['name']] = task
		        
		for s in shots: 
		    task = info.get(s['code']) or dict()
		    tech = techInfo.get(s['code']) or dict()
		    sim = simInfo.get(s['code']) or dict()
		    sumInfo[s['code']] = {
		    	'duration': task.get('duration'), 
		    	'start_date': task.get('start_date'), 
		    	'due_date': task.get('due_date'), 
		    	'frame': s['sg_working_duration'], 
		    	'status': task.get('sg_status_list'), 
		    	'tech_finish': tech.get('due_date'), 
		    	'sim_finish': sim.get('due_date')}

		return sumInfo

if __name__ == '__main__':
	main()