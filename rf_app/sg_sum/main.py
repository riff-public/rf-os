import os
import sys
from datetime import datetime

core = '{}/core'.format(os.environ['RFSCRIPT'])
sys.path.append(core) if not core in sys.path else None
import rf_config
try:
    from rf_utils.gsheet import googlesheet as gs
except ImportError:
    pass
from rf_utils.sg import sg_process
sg = sg_process.sg


def run():
    data = [['user', 'entity_type', 'hours', 'date']]
    wsname = datetime.now().strftime('%Y-%m-%d')
    scope = gs.Scope.sheet_full
    client = gs.createAccount(gs.Setting.keyfile, scope)
    service = gs.buildService(gs.Setting.keyfile, scope)
    sheet = gs.openByURL(client, 'https://docs.google.com/spreadsheets/d/1FYPMjZu_u6BlebhNCrVgPZztD3jAOwgXjlA0gkVxXq0/edit#gid=0')
    worksheets = gs.list_worksheet(sheet)

    if not wsname in worksheets:
        worksheet = gs.new_worksheet(sheet, wsname, 100, 100)
    else:
        worksheet = gs.select_worksheetByTitle(sheet, wsname)


    clear_cell(service, sheet, worksheet)
    write_data(service, sheet, wsname, data)


def task_start_end_date(task_id):
    """
    logic
    1. anything to ip -> clock in
    if not 1. anything to fix, rev -> clock in

    anything to apr -> clock out
    apr to other -> start loop
    other to apr -> count interation
    """
    task = sg.find_one('Task', [['id', 'is', task_id]], ['start_date', 'due_date', 'duration'])

    start_condition = ['wtg', 'ip']
    end_condition = ['ip', '*']
    events = sg.find('EventLogEntry', [['entity.Task.id', 'is', task_id]], ['meta', 'created_at'])

    duration = list()
    start = None
    end = None
    accuracy = 0
    # ip?
    if any(a['meta'].get('new_value') == 'ip' for a in events):
        accuracy = 90
    else:


    for ev in events:
        if ev['meta'].get('attribute_name') == 'sg_status_list':
            print ev['meta']['old_value'], ev['meta']['new_value']
            if start:
                if ev['meta']['new_value'] not in ['wtg', 'ip', 'omt']:
                    end = ev['created_at']

            if ev['meta']['new_value'] in ['ip']:
                start = ev['created_at']

    # sg.find_one('Task', )


def write_data(service, sheet, worksheet, data):

    service.spreadsheets().values().append(
            spreadsheetId = sheet.id,
            range = "{}!A:Z".format(worksheet),
            body = {
                    "majorDimension": "ROWS",
                    "values": data,
                    },
            valueInputOption="USER_ENTERED").execute()


def resize_cell(service, sheet, worksheet, col_start, col_end, col_size):
    request_body = {
                      "requests": [
                        {
                          "updateDimensionProperties": {
                            "range": {
                              "sheetId": worksheet.id,
                              "dimension": "COLUMNS",
                              "startIndex": col_start,
                              "endIndex": col_end
                            },
                            "properties": {
                              "pixelSize": col_size
                            },
                            "fields": "pixelSize"
                          }
                        },
                      ]
                    }
    response = service.spreadsheets().batchUpdate(spreadsheetId=sheet.id, body=request_body).execute()


def set_cell_color(service, sheet, worksheet, col_start, col_end, row_start, row_end, red, green, blue):
    #credentials_file = credentials path(str)
    #sheets = google sheet name (str)
    #worksheet = google tab name (str)
    #col_start, col_end = range of columns (int)
    #row_start, row_end = range of row (int)
    #red, green, blue = color (int)

    request_body = {
                      "requests": [
                        {
                          "repeatCell": {
                            "range": {
                              "sheetId": worksheet.id,
                              "startRowIndex": row_start,
                              "endRowIndex": row_end,
                              'startColumnIndex': col_start,
                              'endColumnIndex': col_end
                            },
                            "cell": {
                              "userEnteredFormat": {
                                "backgroundColor": {
                                  "red": red,
                                  "green": green,
                                  "blue": blue
                                },
                              }
                            },
                            "fields": "userEnteredFormat(backgroundColor,textFormat,horizontalAlignment)"
                          }
                        },
                      ]
                    }
    response = service.spreadsheets().batchUpdate(spreadsheetId=sheet.id, body=request_body).execute()


def clear_cell(service, sheet, worksheet, fields='*'):
    request_body = {
                      "requests": [
                        {
                          "updateCells": {
                            "range": {
                              "sheetId": worksheet.id,
                            },
                            "fields": "{}".format(fields)
                          }
                        },
                      ]
                    }
    response = service.spreadsheets().batchUpdate(spreadsheetId=sheet.id, body=request_body).execute()


if __name__ == '__main__':
    run()
