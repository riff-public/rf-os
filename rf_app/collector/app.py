#!/usr/bin/env python
# -- coding: utf-8 --

_title = 'RF Collector'
_version = '1.5.1'
_des = ''
uiName = 'CollectorUi'

#Import python modules
import sys
import os
import logging
import getpass
import traceback
import json
import re
import subprocess
from collections import OrderedDict, defaultdict
from functools import partial
from glob import glob

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '{}/core'.format(os.environ.get('RFSCRIPT'))
sys.path.append(core)

# import config
import rf_config as config

from rf_utils.context import context_info
from rf_utils import log_utils
from rf_utils import user_info
from rf_utils.ui import load
from rf_utils.ui import stylesheet
from rf_utils import thread_pool
from rf_utils import admin

user = '{}-{}'.format(config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

# maya & module function
from . import ui
reload(ui)
# from . import tabs
from rf_utils.sg import sg_process
from rf_utils.context import context_info
from rf_utils import register_entity
from rf_utils.widget import dialog
ui.sg = sg_process.sg

if config.isNuke:
    import nuke
    import nukescripts
elif config.isMaya:
    import maya.cmds as mc

modulePath = sys.modules[__name__].__file__.replace('\\', '/')
moduleDir  = os.path.dirname(modulePath)
appModuleDir = os.path.dirname(moduleDir)

class Dep:
    departmentDict = {'asset':['model', 'rig', 'lookdev'],
                        'scene':['layout', 'anim', 'mocap', 'light']}

class CollectorUi(QtWidgets.QMainWindow):
    tabChanged = QtCore.Signal(bool)
    def __init__(self, parent=None,):
        #Setup Window
        super(CollectorUi, self).__init__(parent)
        self.entity = None
        self.property_win = None
        user = user_info.User()
        if user.is_producer() or user.is_admin() or user.is_supervisor():
            self.is_admin = True
        else:
            self.is_admin = False

        # ui read
        uiFile = '{}/ui.py'.format(moduleDir)
        self.ui = ui.CollectorUi()
        self.w = 1190
        self.h = 685

        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.resize(self.w, self.h)
        self.setWindowTitle('{} {} {}'.format(_title, _version, _des))
        self.setWindowIcon(QtGui.QIcon('{}/icons/app_icon.png'.format(moduleDir)))

        self.setup_ui()
        self.init_functions()
        self.init_signals()
        self.set_default()
        self.set_dep()


    def setup_ui(self):
        # set tabs
        self.ui.sub_layout.setStretch(0, 1)
        self.ui.sub_layout.setStretch(1, 6)


    def init_signals(self):
        self.ui.sort_alp_radioButton.clicked.connect(self.sort_changed)
        self.ui.sort_mod_radioButton.clicked.connect(self.sort_changed)
        self.ui.sort_latestver_radioButton.clicked.connect(self.sort_changed)
        self.ui.asset_widget.entitytype_comboBox.currentIndexChanged.connect(self.set_dep)
        self.ui.add_button.clicked.connect(self.add)
        self.ui.remove_button.clicked.connect(self.remove)
        self.ui.pack_button.clicked.connect(self.pack)

    def set_dep(self):
        entity = str(self.ui.asset_widget.entitytype_comboBox.currentText())
        self.ui.department_comboBox.clear()
        for dep in Dep.departmentDict[entity]:
            self.ui.department_comboBox.addItem(dep)

    def add(self):
        asset_tree_widget = self.ui.asset_widget.entity_widget.treeWidget
        items = asset_tree_widget.selectedItems()
        dep = str(self.ui.department_comboBox.currentText())
        for i in items:
            asset_data = {}
            name = str(dep + ' - '+ i.text(0))
            item = QtWidgets.QListWidgetItem()
            item.setText(name)
            newfont = QtGui.QFont("Times", 10, QtGui.QFont.Bold) 
            item.setFont(newfont)
            asset_data['name'] = i.text(0)
            asset_data['project'] = str(self.ui.asset_widget.project_widget.projectComboBox.currentText())
            asset_data['entity'] = str(self.ui.asset_widget.entitytype_comboBox.currentText())
            asset_data['type'] = str(self.ui.asset_widget.filtertype_comboBox.currentText())
            asset_data['step'] = dep
            item.setData(QtCore.Qt.UserRole,asset_data)
            self.ui.pack_list_widget.addItem(item)

    def remove(self):
        items = self.ui.pack_list_widget.selectedItems()
        for i in items:
            self.ui.pack_list_widget.takeItem(self.ui.pack_list_widget.row(i))

    def set_default(self):
        if  config.isMaya or config.isNuke:
            # try to jump to current scene
            self.jump_to_current_scene()

    def sort_changed(self):
        if self.ui.sort_alp_radioButton.isChecked():
            func = lambda i: i.get('code', None)
            reverse = False
        elif self.ui.sort_mod_radioButton.isChecked():
            func = lambda i: i.get('updated_at', None)
            reverse = True
        elif self.ui.sort_latestver_radioButton.isChecked():
            func = lambda i: i.get('created_at', None)
            reverse = True

        self.ui.asset_widget.sort_items(sort_func=func, sort_reverse=reverse)

    def init_functions(self):
        self.ui.asset_widget.refresh()

    def jump_to_current_scene(self):
        if config.isMaya:
            context = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))
            self.jump_to_context(context=context)
        elif config.isNuke:
            context = context_info.ContextPathInfo(path=nuke.root().name())
            self.jump_to_context(context=context)

    def jump_to_context(self, context):
        # asset contexts
        project = context.project
        entity_type = context.entity_type
        filter_type = context.type
        asset = context.name

        if entity_type == 'asset' and project and filter_type and asset:
            logger.debug('Jump to assset: {} {} {}'.format(project, filter_type, asset))
            # setup app
            self.ui.asset_widget.blockSignals(True)
            # set project
            self.ui.asset_widget.project_widget.set_current_item(project)
            # set entity type
            index = self.ui.asset_widget.entitytype_comboBox.findText('Asset', QtCore.Qt.MatchFixedString)
            self.ui.asset_widget.entitytype_comboBox.setCurrentIndex(index)
            # set type
            index = self.ui.asset_widget.filtertype_comboBox.findText(filter_type, QtCore.Qt.MatchFixedString)
            self.ui.asset_widget.filtertype_comboBox.setCurrentIndex(index)
            self.ui.asset_widget.blockSignals(False)
            # set tag to all tags
            self.ui.asset_widget.tag_widget.set_current_index(0)
            # set asset
            self.ui.asset_widget.entity_widget.blockSignals(True)
            self.ui.asset_widget.entity_widget.set_current(displayText=asset, signal=False)
            self.ui.asset_widget.entity_widget.blockSignals(False)
            # explicit call to asset selected
            entity = self.ui.asset_widget.entity_widget.current_item()
            if entity:
                self.asset_selected(entity=entity)


    def asset_selected(self, entity):
        logger.debug('Asset selected: {}'.format(entity))

    def pack(self):
        pathDict = {}
        entityContext = context_info.ContextPathInfo() 
        for index in range(self.ui.pack_list_widget.count()):
            packData = {}
            item = (self.ui.pack_list_widget.item(index))
            # text = item.text()
            data = item.data(QtCore.Qt.UserRole)
            step = data['step'] # model light lookdev etc.
            if step == 'mocap':
                step = 'anim'
            proj = data['project'] # hanuman bikey etc.
            assetType = data['type'] # act1 ch04 , char prop, etc.
            entityType = data['entity']  # asset scene
            entity = data['name']
            # scene.context.update(res=res, step=step, process=process)
            entityContext.context.update(project=proj, step=step, entity=entity, entityGrp=assetType, entityType=entityType)
            if step == 'lookdev':
                scene = entityContext.copy()
                scene.context.update(process='outsource',app='maya')
                path = scene.path.workspace().abs_path()
                files = None
                if os.path.exists(path):
                    files = os.listdir(path)
                newVersion = 'v001'
                if files:
                    verList = []
                    for f in files:
                        ver = re.findall('[vV]{1}[0-9]{3}', f)
                        if ver:
                            version = ver[0].lower().split('v')[-1]
                            verList.append(version)
                    if verList:
                        newVersion = 'v{:03d}'.format(int(verList[-1]) + 1)
                scene.context.update(version=newVersion)
                workfile = scene.work_name()
                elem = '{}_autoBuild' .format(workfile.split('.')[0])
                filename = '{}.{}'.format(elem, '.'.join(workfile.split('.')[1:]))
                path = '{}/{}'.format(path, filename)
                packData['step'] = step
                pathDict[path] = packData

            elif step == 'rig':
                scene = entityContext.copy()
                scene.context.update(project=proj , step=step, entity=entity, entityGrp=assetType, entityType=entityType)
                scene.context.update(process='main',app='maya', res='md')
                heroPath = scene.path.hero().abs_path()
                # heroRelPath = scene.path.hero()
                mbFileName = scene.output_name(outputKey='rig', hero=True)
                path = '{}/{}'.format(heroPath, mbFileName)
                packData['step'] = step
                path = path.replace('\\', '/')
                if not os.path.exists(path):
                    scene.context.update(process='main',app='maya', res='pr')
                    heroPath = scene.path.hero().abs_path()
                    # heroRelPath = scene.path.hero()
                    mbFileName = scene.output_name(outputKey='rig', hero=True)
                    path = '{}/{}'.format(heroPath, mbFileName)
                    packData['step'] = step
                    path = path.replace('\\', '/')
                pathDict[path] = packData

                looks = self.list_look(scene)
                for look in looks:
                    scene.context.update(look=look)
                    mtrFile = scene.library(context_info.Lib.rigMtr)
                    mtr = '{}/{}'.format(scene.path.hero().abs_path(), mtrFile)
                    mtr = mtr.replace('\\', '/')
                    pathDict[mtr] = packData
                # fileName = scene.publish_name(hero=True)

            elif step == 'model':
                scene = entityContext.copy()
                scene.context.update(project=proj , step=step, entity=entity, entityGrp=assetType, entityType=entityType)
                scene.context.update(process='main',app='maya', res='md')
                heroPath = scene.path.hero().abs_path()
                # heroRelPath = scene.path.hero()
                fileName = scene.publish_name(hero=True)
                path = '{}/{}'.format(heroPath, fileName)
                packData['step'] = step
                path = path.replace('\\', '/')
                if not os.path.exists(path):
                    scene.context.update(process='main',app='maya', res='pr')
                    heroPath = scene.path.hero().abs_path()
                    # heroRelPath = scene.path.hero()
                    fileName = scene.publish_name(hero=True)
                    path = '{}/{}'.format(heroPath, fileName)
                    packData['step'] = step
                    path = path.replace('\\', '/')
                pathDict[path] = packData

                looks = self.list_look(scene)
                for look in looks:
                    scene.context.update(look=look)
                    mtrFile = scene.library(context_info.Lib.modelMtr)
                    mtr = '{}/{}'.format(scene.path.hero().abs_path(), mtrFile)
                    mtr = mtr.replace('\\', '/')
                    pathDict[mtr] = packData

                adPath = '%s/%s' % (scene.path.output_hero().abs_path(), scene.output_name(outputKey='cache', hero=True))
                adPath = adPath.replace('\\', '/')
                pathDict[adPath] = packData


            elif step == 'light':
                scene = entityContext.copy()
                proj = entity.split('_')[0]
                seq = entity.split('_')[2]
                sh = entity.split('_')[3]
                scene.context.update(process='outsource',app='maya', entityCode=sh, projectCode=proj, entityParent=seq)
                path = scene.path.workspace().abs_path()
                files = None
                if os.path.exists(path):
                    files = os.listdir(path)
                newVersion = 'v001'
                if files:
                    verList = []
                    for f in files:
                        ver = re.findall('[vV]{1}[0-9]{3}', f)
                        if ver:
                            version = ver[0].lower().split('v')[-1]
                            verList.append(version)
                    if verList:
                        newVersion = 'v{:03d}'.format(int(verList[-1]) + 1)
                scene.context.update(version=newVersion)
                workfile = scene.work_name()
                elem = '{}_autoBuild' .format(workfile.split('.')[0])
                filename = '{}.{}'.format(elem, '.'.join(workfile.split('.')[1:]))
                path = '{}/{}'.format(path, filename)
                packData['step'] = step
                path = path.replace('\\', '/')
                pathDict[path] = packData

            elif data['step'] == 'mocap':
                scene = entityContext.copy()
                proj = entity.split('_')[0]
                seq = entity.split('_')[2]
                sh = entity.split('_')[3]
                scene.context.update(process='Mocap',app='maya', entityCode=sh, projectCode=proj, entityParent=seq)
                path = scene.path.workspace().abs_path()
                scene.context.update(process='mocap')
                pathLower = scene.path.workspace().abs_path()
                if os.path.exists(path):
                    files = None
                    if os.path.exists(path):
                        files = os.listdir(path)
                    if files:
                        fileList = []
                        for f in files:
                            if 'Mocap' in f or 'mocap' in f:
                                fileList.append(f)
                        if fileList:
                            filename = fileList[-1]
                        else:
                            filename = files[-1]
                        path = '{}/{}'.format(path, filename)
                        path = path.replace('\\', '/')
                        packData['step'] = step
                        pathDict[path] = packData
                elif os.path.exists(pathLower):
                    path = pathLower
                    files = None
                    if os.path.exists(path):
                        files = os.listdir(path)
                    if files:
                        fileList = []
                        for f in files:
                            if 'Mocap' in f or 'mocap' in f:
                                fileList.append(f)
                        if fileList:
                            filename = fileList[-1]
                        else:
                            filename = files[-1]
                        path = '{}/{}'.format(path, filename)
                        path = path.replace('\\', '/')
                        packData['step'] = step
                        pathDict[path] = packData
                else:
                    self.show_message_box("Shot : {}_{}_{} does not have mocap scene.".format(proj, seq, sh))
                    return


            elif entityType == 'scene':
                scene = entityContext.copy()
                proj = entity.split('_')[0]
                seq = entity.split('_')[2]
                sh = entity.split('_')[3]
                scene.context.update(process='main',app='maya', entityCode=sh, projectCode=proj, entityParent=seq)
                path = scene.path.workspace().abs_path()
                files = None
                if os.path.exists(path):
                    files = os.listdir(path)
                if files:
                    fileList = []
                    for f in files:
                        if 'main' in f:
                            fileList.append(f)
                    if fileList:
                        filename = fileList[-1]
                    else:
                        filename = files[-1]
                    path = '{}/{}'.format(path, filename)
                    path = path.replace('\\', '/')
                    packData['step'] = step
                    pathDict[path] = packData

            # print 'PACK'
            # print entity
            # print step
            # for key,v in pathDict.items():
            #     print key
            # print 'END \n'

            dst = self.ui.dst_line.text()
            dst = dst.replace('\\', '/')
            # print dst

        if dst:
            if os.path.exists(dst):
                # pass
                self.pack_farm(pathDict, dst)
            else:
                self.show_message_box("Your Destination doesn't exists")
                return
        else:
            self.show_message_box("Please fill your Destination")
            return

        dialog.MessageBox.success('Confirm', 'Submit success')



    def show_message_box(self, msg):
        QtWidgets.QMessageBox.information(self, "Message", str(msg))

    def build_scene_name(self, scene): 
        path = scene.path.workspace().abs_path()
        workfile = scene.work_name()
        elem = '%s_autoBuild' % workfile.split('.')[0]
        filename = '%s.%s' % (elem, '.'.join(workfile.split('.')[1:]))
        return '%s/%s' % (path, filename)

    def list_look(self, entity): 
        res = 'md'
        regInfo = register_entity.Register(entity)
        looks = regInfo.get_looks(step='texture', res=res)
        if not looks:
            looks = ['main']
        return looks

    def pack_farm(self, allItems, dst):
        # allItems = []
        # dst = 'P:/Hanuman/ftp/to_OS/Chaya/20220601'
        from rf_utils.deadline import batch_collector
        reload(batch_collector)
        from rf_utils import project_info
        proj = project_info.ProjectInfo(str(self.ui.asset_widget.project_widget.projectComboBox.currentText()))
        executable = proj.software.mayapy()
        version = str(proj.software.version("Maya"))

        print proj, executable

        script = 'O:/Pipeline/core/rf_app/sceneCollector_scm/sceneCollecter_batch.py'
        if dst:
            if os.path.exists(dst):
                for path,data in allItems.items():
                    print 'send'
                    step = data['step']
                    cmds = [script, '-p', path, '-d', dst, '-m', 'scene', '-k', str(True), '-s', str(False), '-st', step, '-ver', version]
                    jobname = "Pack %s" % path.split('/')[-1]
                    #executable = 'C:/Program Files/Autodesk/Maya2020/bin/mayapy.exe'
                    batch_collector.process_send_to_farm(cmds, jobname=jobname, executable= executable)


def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        parent = maya_win.getMayaWindow()
        myApp = CollectorUi(parent=parent)
        myApp.show()
    elif config.isNuke:
        from rf_nuke import nuke_win 
        # reload(nuke_win)
        logger.info('Run in Nuke\n')
        parent = nuke_win._nuke_main_window()
        nuke_win.deleteUI(uiName)
        myApp = CollectorUi(parent=parent)
        myApp.show()
    else:
        logger.info('Run in standalone\n')
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        myApp = CollectorUi(parent=None)
        myApp.show()
        stylesheet.set_default(app)
        sys.exit(app.exec_())
    return myApp


if __name__ == '__main__':
    show()
