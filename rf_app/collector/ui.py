#!/usr/bin/env python
# -- coding: utf-8 --

import os
import sys
import traceback
from datetime import datetime
from collections import OrderedDict, defaultdict
from functools import partial

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCompat

module_path = sys.modules[__name__].__file__
module_dir = os.path.dirname(module_path)
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils.widget import project_widget
from rf_utils.widget import entity_browser
from rf_utils.widget import hashtag_widget
from rf_utils import icon
sg = None

class Setting:
    refreshIcon = '{}/icons/refresh.png'.format(module_dir)
    currentIcon = '{}/icons/current_icon.png'.format(module_dir)

class CollectorUi(QtWidgets.QWidget):
    status = QtCore.Signal(str)

    def __init__(self, parent=None):
        super(CollectorUi, self).__init__(parent)
        self.layout = QtWidgets.QVBoxLayout()
        self.sub_layout = QtWidgets.QHBoxLayout()
        self.layout.addLayout(self.sub_layout)
        self.setLayout(self.layout)

        self.setup_widget()
        self.init_signals()
        self.init_functions()

    def setup_widget(self):
        # # logo
        # self.logo = QtWidgets.QLabel()
        # self.logo.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))
        # self.layout.insertWidget(0, self.logo)

        
        # splitter
        self.mainSplitter = QtWidgets.QSplitter()
        self.mainSplitter.setOrientation(QtCore.Qt.Horizontal)

        self.navSplitWidget = QtWidgets.QWidget(self.mainSplitter)
        self.viewSplitWidget = QtWidgets.QWidget(self.mainSplitter)
        self.mainSplitter.setSizes([100, 300])
        self.mainSplitter.setStretchFactor(0, 0)
        self.mainSplitter.setStretchFactor(1, 1)

        self.sub_layout.addWidget(self.mainSplitter)

        # navigation bar
        self.nav_layout = QtWidgets.QVBoxLayout(self.navSplitWidget)
        self.nav_layout.setSpacing(7)
        
        # asset widget
        self.asset_widget = entity_browser.EntityTreeBrowser()
        self.asset_widget.search_lineEdit.setPlaceholderText('Search by name, tag or chapter...')
        self.asset_widget.filtertype_comboBox.setMaximumWidth(80)
        self.asset_widget.entitytype_comboBox.setMaximumWidth(80)
        self.asset_widget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)

        self.asset_widget.entity_widget.treeWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        # hide entity type, only asset
        # self.asset_widget.entitytype_label.hide()
        # self.asset_widget.entitytype_comboBox.hide()

        # set margins
        self.asset_widget.layout.setContentsMargins(0, 0, 0, 0)
        self.nav_layout.addWidget(self.asset_widget)

        # options layout
        self.options_layout = QtWidgets.QVBoxLayout()
        self.options_layout.setSpacing(3)
        self.options_layout.setContentsMargins(0, 0, 0, 0)
        self.nav_layout.addLayout(self.options_layout)

        # sort layout
        self.sort_groupBox = QtWidgets.QGroupBox()
        self.options_layout.addWidget(self.sort_groupBox)

        # sort radio buttons
        self.sort_layout = QtWidgets.QHBoxLayout(self.sort_groupBox)
        self.sort_layout.setSpacing(25)
        self.sort_layout.setContentsMargins(0, 0, 0, 0)

        spacerItem1 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.sort_layout.addItem(spacerItem1)

        self.sort_alp_radioButton = QtWidgets.QRadioButton('A-Z')
        self.sort_alp_radioButton.setChecked(True)
        self.sort_layout.addWidget(self.sort_alp_radioButton)

        self.sort_mod_radioButton = QtWidgets.QRadioButton('Modified')
        self.sort_layout.addWidget(self.sort_mod_radioButton)

        self.sort_latestver_radioButton = QtWidgets.QRadioButton('Created')
        self.sort_layout.addWidget(self.sort_latestver_radioButton)

        spacerItem2 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.sort_layout.addItem(spacerItem2)

        # pack
        self.pack_main_layout = QtWidgets.QHBoxLayout(self.viewSplitWidget)

        self.pack_button_layout = QtWidgets.QVBoxLayout()
        self.add_button = QtWidgets.QPushButton('>>')
        self.add_button.setMaximumSize(32, 32)
        self.remove_button = QtWidgets.QPushButton('<<')
        self.remove_button.setMaximumSize(32, 32)
        spacerItemtop = QtWidgets.QSpacerItem(0, 200 ,QtWidgets.QSizePolicy.Minimum)
        spacerItembottom = QtWidgets.QSpacerItem(0, 200, QtWidgets.QSizePolicy.Minimum)
        self.pack_button_layout.addItem(spacerItemtop)
        self.pack_button_layout.addWidget(self.add_button)
        self.pack_button_layout.addWidget(self.remove_button)
        self.pack_button_layout.addItem(spacerItembottom)

        self.pack_layout = QtWidgets.QVBoxLayout()
        # pack widget
        # self.pack_H_layout = QtWidgets.QHBoxLayout()

        self.dep_label = QtWidgets.QLabel('Department')
        self.department_comboBox = QtWidgets.QComboBox()
        self.department_comboBox.setMaximumWidth(80)

        self.pack_list_widget = QtWidgets.QListWidget()
        self.pack_list_widget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        # self.pack_H_layout.addWidget(self.dep_label)
        # self.pack_H_layout.addWidget(self.department_comboBox)
        spacerItem3 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        # self.pack_H_layout.addItem(spacerItem3)

        self.asset_widget.filter_layout.insertWidget(5,self.dep_label)
        self.asset_widget.filter_layout.insertWidget(6,self.department_comboBox)

        self.pack_label = QtWidgets.QLabel('Pack List')

        self.dst_layout = QtWidgets.QHBoxLayout()
        self.dst_label = QtWidgets.QLabel('Destination')
        self.dst_line = QtWidgets.QLineEdit()
        self.pack_button = QtWidgets.QPushButton('Pack')
        self.pack_button.setMaximumSize(100, 100)
        # spacerItem4 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.dst_layout.addWidget(self.dst_label)
        self.dst_layout.addWidget(self.dst_line)
        self.dst_layout.addWidget(self.pack_button)
        self.dst_layout.setStretch(0, 0)
        self.dst_layout.setStretch(1, 0)
        self.dst_layout.setStretch(2, 1)
        # self.dst_layout.addItem(spacerItem4)

        # self.pack_layout.addLayout(self.pack_H_layout)
        self.pack_layout.addWidget(self.pack_label)
        self.pack_layout.addWidget(self.pack_list_widget)
        self.pack_layout.addLayout(self.dst_layout)
        self.pack_main_layout.addLayout(self.pack_button_layout)
        self.pack_main_layout.addLayout(self.pack_layout)

        self.nav_layout.setStretch(0, 1)
        self.nav_layout.setStretch(1, 0)
        self.layout.setStretch(0, 0)
        self.layout.setStretch(1, 0)
        self.layout.setStretch(2, 1)

    def init_signals(self):
        pass

    def init_functions(self):
        pass