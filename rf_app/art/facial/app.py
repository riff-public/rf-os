# v.0.0.1 Ftp sync
_title = 'Facial Tool'
_version = 'v.0.0.1'
_des = 'Beta'
uiName = 'FacialUi'

#Import python modules
import sys
import os
import getpass
import logging
from datetime import datetime
from collections import OrderedDict
from functools import partial
# import config
import rf_config as config
from rf_utils import log_utils
from rf_utils.ui import stylesheet

module_dir = os.path.dirname(sys.modules[__name__].__file__)
appname = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]
user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)
uifile = '{}/{}'.format(module_dir, 'ui.ui')
global ui

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from rf_utils import load 
from . import facial_grid
from . import app_config


class FacialToolUi(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(FacialToolUi, self).__init__(parent)

        # ui read
        ui = load.setup_ui(uifile)
        self.ui = ui
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.resize(300, 200)
        self.init_signals()

    def init_signals(self): 
        self.ui.facial_button.clicked.connect(self.run_facial_tool)

    def run_facial_tool(self): 
        import importlib
        importlib.reload(facial_grid)
        importlib.reload(app_config)
        importlib.reload(facial_grid.img_utils)
        facial_grid.show(self) 



def show(): 
    if config.isMaya: 
        from rftool.utils.ui import maya_win
        maya_win.deleteUI(uiName)
        app = FacialToolUi(maya_win.getMayaWindow())
        app.show() 
        return app
    else: 
        # app = QtWidgets.QApplication.instance()
        main_app = QtWidgets.QApplication(sys.argv)
        app = FacialToolUi()
        app.show()
        stylesheet.set_default(main_app)
        sys.exit(main_app.exec_())


if __name__ == '__main__':
    show()


""" 
Cloud status condition 
1. no sync 
    1.1 cloud not exists 
    1.2 md5 not match 
2. sync 

md5 matching strategy 
- file count follow local
- cloud less count -> not match 
- read file md5 from json compare json from cloud 
"""

""" 
Download condition 
Zings asset - publ/hero 
Riff asset - publ/hero_zings 
    1. Ftp No - Nothing 
    2. Ftp yes Local Yes No json - Download 
    3. Ftp yes Local Yes Json Yes - Compare - Download 
    4. Ftp yes Local Yes Json Yes - Compare - Yes - Sync 
"""
