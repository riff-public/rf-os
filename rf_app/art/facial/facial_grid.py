# v.0.0.1 Ftp sync
# v.0.0.2 create resize version 
# v.0.0.3 Add canvas comboBox, fix bug alpha, asset searchable
# v.0.0.4 Add Support Ui drop files
# v.0.0.5 Add auto fill PNG-A (checkBox and menu)
# v.0.0.6 Add clear button 
# v.0.0.7 Set Status
# v.0.0.8 Add still publish
# v.0.0.9 Add SG Upload version


_title = 'Rename Tool'
_version = 'v.0.0.8'
_des = 'Add still image'
uiName = 'RenameFacialUi'

#Import python modules
import sys
import os
import getpass
import logging
import subprocess
from datetime import datetime
from collections import OrderedDict
from functools import partial
# import config
import rf_config as config
from rf_utils import file_utils
from rf_utils.ui import stylesheet
from rf_utils.context import context_info
from rf_utils import admin
from rf_utils.sg import sg_process
from rf_utils import user_info
from rf_app.publish.asset import sg_hook
sg = sg_process.sg

module_dir = os.path.dirname(sys.modules[__name__].__file__)
appname = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from . import app_config
from rf_utils import img_utils

from rf_utils.pipeline.notification import noti_module_cliq
# reload(noti_module_cliq)


class RenameTool(QtWidgets.QMainWindow):
    """docstring for RenameTool"""
    def __init__(self, parent=None):
        super(RenameTool, self).__init__(parent)
        # ui read
        self.ui = RenameUi(parent)
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)

        # title
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.resize(700, 700)

        # fill ui 
        self.project = 'CA'
        self.char_path = 'P:/{}/asset/work/char'.format(self.project)
        self.set_ui()
        self.init_signals()
        self.init_data()

    def set_ui(self): 
        # hardcode asset list 
        assets = file_utils.list_folder(self.char_path)
        self.ui.asset_combo.addItems(sorted(assets))
        self.ui.element_combo.addItems(app_config.Config.element_preset)
        self.ui.file_element_combo.addItems(app_config.Config.file_element_preset)
        self.ui.still_element_combo.addItems(list(app_config.Config.still_preset.keys()))
        self.still_element_changed(0)

        # add canvas 
        self.ui.canvas_combo.clear()
        for i, canvas in enumerate(app_config.Config.canvas_size): 
            w, h = canvas
            self.ui.canvas_combo.addItem('{} x {}'.format(w, h))
            self.ui.canvas_combo.setItemData(i, canvas, QtCore.Qt.UserRole)

    def init_signals(self): 
        self.ui.drop_area.multipleDropped.connect(self.dir_drop)
        self.ui.drop_files.multipleDropped.connect(self.file_drop)
        self.ui.export_button.clicked.connect(self.export)
        self.ui.asset_combo.currentIndexChanged.connect(self.asset_changed)
        self.ui.element_combo.currentIndexChanged.connect(self.element_changed)
        self.ui.clear_button.clicked.connect(self.clear_buttons)
        self.ui.status_button.clicked.connect(self.set_status)
        self.ui.still_element_combo.currentIndexChanged.connect(self.still_element_changed)

        # set button signals 
        for num, button in self.ui.button_dict.items(): 
            button.customContextMenuRequested.connect(partial(self.button_context_menu, button))

        for num, button in self.ui.still_button_dict.items(): 
            button.droppedUrl.connect(self.still_dropped)


    def init_data(self): 
        path = '{}/{}'.format(self.char_path, self.ui.asset_combo.currentText())
        self.asset = context_info.ContextPathInfo(path=path)
        self.asset.context.update(step='rig', process='main', look='main', app='maya', res='md')

    def asset_changed(self): 
        asset_name = self.ui.asset_combo.currentText()
        self.asset.context.update(entity=asset_name)
        self.element_changed()

    def element_changed(self): 
        self.clear_buttons()

    def still_element_changed(self, index): 
        key = self.ui.still_element_combo.itemText(index)
        elements = app_config.Config.still_preset[key]

        for key, button in self.ui.still_button_dict.items(): 
            button.clear_display(stylesheet=False)
            button.set_color(200, 140, 120)
            button.setEnabled(False)

        for i, element in enumerate(elements): 
            padding = '%03d' % i
            button = self.ui.still_button_dict[padding]
            button.setText(element)
            button.set_data(element)
            button.setEnabled(True)

    def still_dropped(self, obj): 
        urls, button = obj
        filepath = urls[0].toLocalFile()
        button.set_display(filepath)

                # source.clear_display()

    def clear_buttons(self): 
        for key, button in self.ui.button_dict.items(): 
            button.clear_display()

    def dir_drop(self, dirs): 
        # check folder format. only accept 3 digits 010, 020 
        canvas_size = self.ui.canvas_combo.itemData(self.ui.canvas_combo.currentIndex(), QtCore.Qt.UserRole)
        file_dict = OrderedDict()
        for d in dirs: 
            if os.path.isdir(d): 
                path, dirname = os.path.split(d)
                if dirname in app_config.Config.allow_seq: 
                    files = file_utils.list_file(d)
                    filter_files = [a for a in files if os.path.splitext(a)[-1] in app_config.Config.allow_exts]

                    i = 0 
                    for file in sorted(filter_files): 
                        filepath = '{}/{}'.format(d, file)
                        if i < app_config.Config.num_row: 
                            key = '%03d' % (int(dirname) + i)
                            print(key)
                            if key in self.ui.button_dict.keys(): 
                                button = self.ui.button_dict[key]
                                button.set_display(filepath)
                                w, h = img_utils.get_image_size(filepath)
                                pw, ph = canvas_size

                                if w > pw or h > ph: 
                                    button.set_warning()

                            i+=1 
                        else: 
                            print('discard {}'.format(filepath))

                    if self.ui.autofill_cb.isChecked(): 
                        if i < (app_config.Config.num_row - 1): 
                            for ii in range(i, (app_config.Config.num_row)): 
                                key = '%03d' % (int(dirname) + ii)
                                button = self.ui.button_dict[key]
                                button.set_display(app_config.Config.transparency_png)



    def file_drop(self, files): 
        for i, file in enumerate(files): 
            if not os.path.isdir(file): 
                key = 'ui%03d' % i
                button = self.ui.file_button_dict[key] 
                button.set_display(file)

    def export(self): 
        # list all buttons  
        element_name = self.ui.element_combo.currentText()
        texture_path = self.asset.path.scheme(key='texturePath').abs_path()
        lo = app_config.Config.lod_prefix.get('low')
        hi = app_config.Config.lod_prefix.get('high')
        root = texture_path
        canvas_size = self.ui.canvas_combo.itemData(self.ui.canvas_combo.currentIndex(), QtCore.Qt.UserRole)

        for key, button in self.ui.button_dict.items(): 
            if button.filepath: 
                src = button.filepath
                name, ext = os.path.splitext(src)
                filename = os.path.split(element_name)[-1]
                newname = '{}.{}{}'.format(filename, button.num, ext)
                dst_path = '{}/{}/{}'.format(root, hi, element_name)
                dst = '{}/{}'.format(dst_path, newname)
                dst_resize_path = '{}/{}/{}'.format(root, lo, element_name)
                dst_resize = '{}/{}'.format(dst_resize_path, newname)
                
                print('Processing {}'.format(src))
                self.ui.status_label.setText('Processing {}'.format(src))
                QtWidgets.QApplication.processEvents()
                
                if not src in app_config.Config.skip_process_files: 
                    # expand canvas 
                    new_width = canvas_size[0]
                    new_height = canvas_size[1]
                    img = img_utils.expand_png_canvas(src, new_width, new_height)

                    # save to destination 
                    # texren = self.asset.path.scheme(key='publishTexturePath').abs_path()
                    
                    # save file to dst
                    print('Hires save to {} ...'.format(dst))
                    img_utils.save_to_file(img, dst)

                    # resize 
                    resize_w, resize_h = app_config.Config.resize_wh
                    print('Resizing to {} ...'.format(dst_resize))
                    # re_img = img_utils.resize_image(img, app_config.Config.resize_scale)
                    re_img = img_utils.resize_image(img, resize_w=resize_w, resize_h=resize_h)
                    img_utils.save_to_file(re_img, dst_resize)

                else: 
                    file_utils.copy(src, dst)
                    file_utils.copy(src, dst_resize)

                button.set_complete()
                print('Copy to {}'.format(dst))
                self.ui.status_label.setText('Copy to {}'.format(dst))
                QtWidgets.QApplication.processEvents()

        # file buttons 
        file_element = self.ui.file_element_combo.currentText()
        for key, button in self.ui.file_button_dict.items(): 
            if button.filepath: 
                src = button.filepath
                filename = os.path.basename(src)
                dst = '{}/{}/{}/{}'.format(root, lo, file_element, filename)

                # copy to dir 
                result = file_utils.copy(src, dst)

                if result: 
                    button.set_complete()
                    self.ui.status_label.setText('Copy to {}'.format(dst))
                    QtWidgets.QApplication.processEvents()

        # still files 
        element_dir = self.ui.still_element_combo.currentText()

        for key, button in self.ui.still_button_dict.items(): 
            if button.filepath: 
                src = button.filepath 
                srcname, ext = os.path.splitext(os.path.basename(src))
                filename = '{}{}'.format(button.data(), ext)
                dst = '{}/{}/{}/{}'.format(root, hi, element_dir, filename)
                result = file_utils.copy(src, dst)

                # resize 
                dst_resize = '{}/{}/{}/{}'.format(root, lo, element_dir, filename)
                resize_w, resize_h = app_config.Config.resize_wh
                print('Resizing to {} ...'.format(dst_resize))
                # re_img = img_utils.resize_image(img, app_config.Config.resize_scale)
                img_utils.resize_from_file(src, dst_resize, resize_w=resize_w, resize_h=resize_h)

                if result: 
                    button.set_complete()
                    self.ui.status_label.setText('Copy to {}'.format(dst))
                    QtWidgets.QApplication.processEvents()

        # open explorer 
        self.open_explorer(root)
        self.ui.status_label.setText('Complete')

    def clear_dir(self, path): 
        files = file_utils.list_file(path)

        for file in files: 
            filepath = '{}/{}'.format(path, file)
            try: 
                admin.remove(filepath)
            except Exception as e: 
                print(e)
                print('Failed to remove file {}'.format(filepath))

    def img_clicked(self, button): 
        filepath = button.filepath
        if os.path.exists(filepath): 
            output_path = filepath.replace('/', '\\')
            subprocess.Popen(r'explorer /select,"%s"' % output_path)

    def open_explorer(self, path): 
        output_path = '{}\\'.format(path.replace('/', '\\'))
        subprocess.Popen(r'explorer /select,"%s"' % output_path)
        self.ui.status_label.setText('Complete')

    def button_context_menu(self, button, position): 
        menu = QtWidgets.QMenu(self)
        action1 = QtWidgets.QAction("Fill PNG-Alpha column", self)
        action1.triggered.connect(partial(self.fill_column, button))
        menu.addAction(action1)
        global_position = button.mapToGlobal(position)
        menu.exec_(self.mapToGlobal(position + button.pos()))

    def fill_column(self, button): 
        # find button in columns 
        col = str(button.num)[:-1]
        for i in range(app_config.Config.num_row): 
            key = '{}{}'.format(col, i)
            col_button = self.ui.button_dict[key]
            if not col_button.filepath: 
                col_button.set_display(app_config.Config.transparency_png)

    def set_status(self): 
        task_name = app_config.Config.task
        status = app_config.Config.task_status
        target_task = app_config.Config.target_task
        asset_name = self.ui.asset_combo.currentText()

        # find task id on "Design step (Asset)"
        # print(task_name, status, self.asset.name)
        project_entity = sg.find_one('Project', [['name', 'is', self.project]], ['name', 'id'])
        step_entity = sg.find_one('Step', 
            [['short_name', 'is', 'design'], ['entity_type', 'is', 'Asset']], 
            ['short_name', 'entity_type'])
        entity = sg.find_one('Asset', 
            [['project', 'is', project_entity], ['code', 'is', asset_name]], 
            ['code', 'id'])
        target_step_entity = sg.find_one('Step', 
            [['short_name', 'is', 'rig'], ['entity_type', 'is', 'Asset']], 
            ['short_name', 'entity_type'])

        task_entity = sg.find_one('Task', 
            [['project', 'is', project_entity], 
            ['entity.Asset.code', 'is', asset_name], 
            ['step', 'is', step_entity], 
            ['content', 'is', task_name]], 
            ['content', 'sg_status_list', 'project'])

        target_task_entity = sg.find_one('Task', 
            [['project', 'is', project_entity], 
            ['entity.Asset.code', 'is', asset_name], 
            ['step', 'is', target_step_entity], 
            ['content', 'is', target_task]], 
            ['content', 'sg_status_list'])

        if not task_entity: 
            data = {
                'project': project_entity, 
                'step': step_entity, 
                'entity': entity, 
                'content': task_name, 
                'sg_status_list': status}
            task_entity = sg.create('Task', data)
            task_entity.update({'project': project_entity})

        else: 
            # update status 
            task_data = {'sg_status_list': status}
            sg.update('Task', task_entity['id'], task_data)

        # if target_task_entity: 
        #     target_data = {'sg_status_list': app_config.Config.target_status}
        #     sg.update('Task', target_task_entity['id'], target_data)
        
        # create version
        imgs = list()
        for key, button in self.ui.file_button_dict.items(): 
            if button.filepath: 
                imgs.append(button.filepath)
                break

        if imgs: 
            design = self.asset.copy()
            design.context.update(step='design', process=app_config.Config.task)
            user = user_info.User()
            sg_hook.publish_version(design, task_entity, status, imgs, user.sg_user(), description='FclSeq')

        else: 
            if target_task_entity: 
                target_data = {'sg_status_list': app_config.Config.target_status}
                sg.update('Task', target_task_entity['id'], target_data)

        design = self.asset.copy()
        design.context.update(step='design', process=app_config.Config.task)
        taskFromId = sg_process.get_task_from_id(task_entity['id'])
        result = [taskFromId]
        user = user_info.User()
        userEntity = user.sg_user()

        task_entity['step'] = step_entity
        task_entity['step']['name'] = 'design'

        task_entity['entity'] = taskFromId['entity']
        # noti_module_cliq.send_scene_notification(result, task_entity, design, userEntity, description='FclSeq')
        description = 'FclSeq'

        emails = noti_module_cliq.get_notification_data(result, task_entity, description, userEntity)
        noti_module_cliq.send_notification(emails, task_entity, urlImage=imgs)

        color = QtGui.QColor(120, 160, 100)
        self.ui.status_button.setStyleSheet("background-color: {};" .format(color.name()))


class RenameUi(QtWidgets.QWidget):
    """docstring for RenameUi"""
    def __init__(self, parent=None):
        super(RenameUi, self).__init__(parent)
        self.parent = parent
        self.button_dict = dict()
        self.file_button_dict = dict()
        self.still_button_dict = dict()
        self.setup_ui()
        self.setAcceptDrops(True)

    def setup_ui(self): 
        self.layout = QtWidgets.QVBoxLayout()

        # header 
        self.header_layout = QtWidgets.QHBoxLayout()

        # asset list 
        self.asset_label = QtWidgets.QLabel('Asset name : ')
        self.asset_combo = QtWidgets.QComboBox()
        self.set_asset_searchable()

        self.element_label = QtWidgets.QLabel('Dir Element name : ')
        self.element_combo = QtWidgets.QComboBox()

        self.canvas_label = QtWidgets.QLabel('Canvas size : ')
        self.canvas_combo = QtWidgets.QComboBox()

        self.autofill_cb = QtWidgets.QCheckBox('Autofill Transparency')
        self.autofill_cb.setChecked(True)

        self.file_element_label = QtWidgets.QLabel('Drop File to : ')
        self.file_element_combo = QtWidgets.QComboBox()

        self.still_element_label = QtWidgets.QLabel('Drop Still to : ')
        self.still_element_combo = QtWidgets.QComboBox()

        spacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.header_layout.addWidget(self.asset_label)
        self.header_layout.addWidget(self.asset_combo)
        self.header_layout.addWidget(self.element_label)
        self.header_layout.addWidget(self.element_combo)
        self.header_layout.addWidget(self.canvas_label)
        self.header_layout.addWidget(self.canvas_combo)
        self.header_layout.addWidget(self.autofill_cb)
        self.header_layout.addItem(spacer)
        self.header_layout.addWidget(self.file_element_label)
        self.header_layout.addWidget(self.file_element_combo)
        self.header_layout.addWidget(self.still_element_label)
        self.header_layout.addWidget(self.still_element_combo)

        self.layout.addLayout(self.header_layout)
        self.sub_layout = QtWidgets.QHBoxLayout()
        self.layout.addLayout(self.sub_layout)

        # line 
        line = QtWidgets.QFrame(self.parent)
        line.setFrameShape(QtWidgets.QFrame.HLine)
        line.setFrameShadow(QtWidgets.QFrame.Sunken)

        self.layout.insertWidget(1, line)

        # master drop 
        self.drop_layout = QtWidgets.QHBoxLayout()
        self.layout.insertLayout(2, self.drop_layout)

        # drop widget 
        self.drop_area = DropArea('-- Drop folders here --')
        self.drop_area.setAlignment(QtCore.Qt.AlignCenter)
        self.drop_area.setMinimumSize(QtCore.QSize(0, 60))
        color = QtGui.QColor(100, 100, 100)  # create a QColor object with RGB values
        self.drop_area.setStyleSheet("background-color: {};" .format(color.name()))
        
        self.drop_files = DropArea('-- Drop files here --')
        self.drop_files.setAlignment(QtCore.Qt.AlignCenter)
        self.drop_files.setMinimumSize(QtCore.QSize(0, 60))
        color = QtGui.QColor(100, 140, 200)  # create a QColor object with RGB values
        self.drop_files.setStyleSheet("background-color: {};" .format(color.name()))

        self.drop_still = DropArea('-- Drop Still below --')
        self.drop_still.setAlignment(QtCore.Qt.AlignCenter)
        self.drop_still.setMinimumSize(QtCore.QSize(0, 60))
        color = QtGui.QColor(200, 140, 120)  # create a QColor object with RGB values
        self.drop_still.setStyleSheet("background-color: {};" .format(color.name()))
        
        self.drop_layout.addWidget(self.drop_area)
        self.drop_layout.addWidget(self.drop_files)
        self.drop_layout.addWidget(self.drop_still)

        self.drop_layout.setStretch(0, 6)
        self.drop_layout.setStretch(1, 1)
        self.drop_layout.setStretch(2, 1)

        # line 
        line = QtWidgets.QFrame(self.parent)
        line.setFrameShape(QtWidgets.QFrame.HLine)
        line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.layout.insertWidget(3, line)

        row = app_config.Config.num_row
        col = app_config.Config.num_col 

        self.generate_buttons(row, col)

        # generate ui 
        ui_row = app_config.Config.ui_num_row
        ui_col = app_config.Config.ui_num_col 
        self.generate_ui_buttons(ui_row, ui_col)

        still_row = app_config.Config.still_num_row
        still_col = app_config.Config.still_num_col 
        self.generate_still_buttons(still_row, still_col)

        # line 
        line = QtWidgets.QFrame(self.parent)
        line.setFrameShape(QtWidgets.QFrame.HLine)
        line.setFrameShadow(QtWidgets.QFrame.Sunken)

        self.publish_layout = QtWidgets.QHBoxLayout()
        spacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.status_label = QtWidgets.QLabel('')
        self.clear_button = QtWidgets.QPushButton('Clear')
        self.clear_button.setMinimumSize(QtCore.QSize(0, 40))
        self.export_button = QtWidgets.QPushButton('Send to Rig')


        # 
        self.publish_layout.addWidget(self.clear_button)
        self.publish_layout.addWidget(self.status_label)
        self.publish_layout.addItem(spacer)
        self.publish_layout.addWidget(self.export_button)
        

        self.layout.addWidget(line)
        self.layout.addLayout(self.publish_layout)
        self.setLayout(self.layout)

        self.sub_layout.setContentsMargins(0, 0, 0, 0)
        self.sub_layout.setSpacing(0)
        self.export_button.setMinimumSize(QtCore.QSize(120, 40))
        self.publish_layout.setStretch(0, 1)
        self.publish_layout.setStretch(1, 0)
        self.publish_layout.setStretch(2, 4)
        self.publish_layout.setStretch(3, 3)

        # add status buttons 
        self.button_task_dict = dict()
        
        task_name = app_config.Config.task
        self.status_button = QtWidgets.QPushButton('Set {}'.format(task_name))
        self.status_button.setMinimumSize(QtCore.QSize(0, 40))
        self.status_button.setMaximumSize(QtCore.QSize(200, 40))
        self.button_task_dict[task_name] = self.status_button
        self.publish_layout.addWidget(self.status_button)
        self.publish_layout.setStretch(4, 2)

        self.header_layout.setStretch(0, 0)
        self.header_layout.setStretch(1, 1)
        self.header_layout.setStretch(2, 0)
        self.header_layout.setStretch(3, 1)
        self.header_layout.setStretch(4, 0)
        self.header_layout.setStretch(5, 1)
        self.header_layout.setStretch(6, 0)
        self.header_layout.setStretch(7, 2)
        self.header_layout.setStretch(8, 0)
        self.header_layout.setStretch(9, 1)
        self.header_layout.setStretch(10, 0)
        self.header_layout.setStretch(11, 1)

    def generate_buttons(self, row, col): 
        for i in range(1, col + 1): 
            layout = QtWidgets.QVBoxLayout()
            layout.setContentsMargins(0, 0, 0, 0)
            layout.setSpacing(0)
            self.sub_layout.addLayout(layout)

            # if i % 2 == 0: 
            #     line = QtWidgets.QFrame(self.parent)
            #     line.setFrameShape(QtWidgets.QFrame.VLine)
            #     line.setFrameShadow(QtWidgets.QFrame.Sunken)
            #     self.sub_layout.addWidget(line)

            for v in range(row): 
                padding = '%03d' % (int('{}{}'.format(i, v)))
                button = Button()
                button.num = padding
                button.setObjectName('{}_button'.format(padding))
                size = app_config.Config.preview_size
                button.setMinimumSize(QtCore.QSize(size, size))
                button.setMaximumSize(QtCore.QSize(size, size))
                layout.addWidget(button)
                button.clicked.connect(partial(self.open_explorer, button))
                button.dropped.connect(self.dropped_button)

                self.button_dict[padding] = button

    def generate_ui_buttons(self, row, col): 
        count = 0 
        for i in range(1, col + 1): 
            layout = QtWidgets.QVBoxLayout()
            layout.setContentsMargins(0, 0, 0, 0)
            layout.setSpacing(0)
            self.sub_layout.addLayout(layout)

            # if i % 2 == 0: 
            #     line = QtWidgets.QFrame(self.parent)
            #     line.setFrameShape(QtWidgets.QFrame.VLine)
            #     line.setFrameShadow(QtWidgets.QFrame.Sunken)
            #     self.sub_layout.addWidget(line)

            for v in range(row): 
                padding = 'ui%03d' % (count)
                button = Button()
                button.num = padding
                button.setObjectName('{}_button'.format(padding))
                size = app_config.Config.ui_preview_size
                button.setMinimumSize(QtCore.QSize(size, size))
                button.setMaximumSize(QtCore.QSize(size, size))
                layout.addWidget(button)
                button.clicked.connect(partial(self.open_explorer, button))
                # button.dropped.connect(self.dropped_button)
                self.file_button_dict[padding] = button
                button.set_color(100, 140, 200)
                count += 1

    def generate_still_buttons(self, row, col): 
        count = 0 
        for i in range(1, col + 1): 
            layout = QtWidgets.QVBoxLayout()
            layout.setContentsMargins(0, 0, 0, 0)
            layout.setSpacing(0)
            self.sub_layout.addLayout(layout)

            if i % 2 == 0: 
                line = QtWidgets.QFrame(self.parent)
                line.setFrameShape(QtWidgets.QFrame.VLine)
                line.setFrameShadow(QtWidgets.QFrame.Sunken)
                self.sub_layout.addWidget(line)

            for v in range(row): 
                padding = '%03d' % (count)
                button = Button()
                button.num = padding
                button.setObjectName('{}_button'.format(padding))
                size = app_config.Config.still_preview_size
                button.setMinimumSize(QtCore.QSize(size, size))
                button.setMaximumSize(QtCore.QSize(size, size))
                layout.addWidget(button)
                button.clicked.connect(partial(self.open_explorer, button))
                self.still_button_dict[padding] = button
                # button.dropped.connect(self.dropped_button)
                button.set_color(200, 140, 120)
                count += 1


    def set_asset_searchable(self): 
        self.asset_combo.setEditable(True)
        self.searchCompleter = QtWidgets.QCompleter()
        self.searchCompleter.setFilterMode(QtCore.Qt.MatchContains)

        self.searchCompleter.setCompletionMode(QtWidgets.QCompleter.UnfilteredPopupCompletion)
        self.searchCompleter.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self.searchCompleter.setModel(self.asset_combo.model())
        self.asset_combo.setCompleter(self.searchCompleter)
        self.asset_combo.setInsertPolicy(QtWidgets.QComboBox.NoInsert) # add this so recent search doesn't appear in combobox

        # focus setup
        # have to set focus to ClickFocus or it will ALWAYS recieve focus
        lineedit = self.asset_combo.lineEdit()
        lineedit.setFocusPolicy(QtCore.Qt.ClickFocus) 

    def open_explorer(self, button): 
        path = button.filepath
        if path: 
            if os.path.exists(path): 
                print('Opening in explorer "{}"'.format(path))
                output_path = '{}\\'.format(path.replace('/', '\\'))
                subprocess.Popen(r'explorer /select,"%s"' % output_path)

    def dropped_button(self, obj): 
        source, target = obj
        if not source == target: 
            if source.filepath: 
                target.set_display(source.filepath)
                source.clear_display()


class Button(QtWidgets.QPushButton):
    """docstring for Button"""
    dropped = QtCore.Signal(object)
    droppedUrl = QtCore.Signal(object)

    def __init__(self, parent=None):
        super(Button, self).__init__(parent)
        self._data = None
        self.num = None
        self.filepath = None
        self.setAcceptDrops(True)
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)

    def mouseMoveEvent(self, event): 
        if event.buttons() == QtCore.Qt.LeftButton: 
            mimeData = QtCore.QMimeData()
            drag = QtGui.QDrag(self)
            drag.setMimeData(mimeData)
            dropAction = drag.exec_(QtCore.Qt.MoveAction)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls:
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            self.dropped.emit((event.source(), self))
            self.droppedUrl.emit((event.mimeData().urls(), self))

        else:
            event.ignore()

    def set_display(self, imagefile): 
         # create a QPixmap object with the path to the icon file
        size = app_config.Config.preview_size
        pixmap = QtGui.QPixmap(imagefile)

        # scale the pixmap to the desired size
        # pixmap = pixmap.scaled(1000, 1000)

        # create a QIcon object from the QPixmap
        icon = QtGui.QIcon(pixmap)

        # icon = QtGui.QIcon(imagefile)
        self.setIcon(icon)
        self.setIconSize(QtCore.QSize(size, size)) 
        self.filepath = imagefile
        self.set_occupied()  

    def clear_display(self, icon=True, stylesheet=True, text=True, data=True): 
        self.setIcon(QtGui.QIcon(None)) if icon else None
        self.filepath = None 
        self.setStyleSheet("") if stylesheet else None
        self.setText('') if text else None
        self.set_data(None) if data else None

    def set_data(self, data): 
        self._data = data

    def data(self): 
        return self._data

    def set_occupied(self): 
        color = QtGui.QColor(60, 60, 60)  # create a QColor object with RGB values
        self.setStyleSheet("background-color: {};" .format(color.name()))

    def set_complete(self): 
        color = QtGui.QColor(100, 200, 100)  # create a QColor object with RGB values
        self.setStyleSheet("background-color: {};" .format(color.name()))

    def set_warning(self): 
        color = QtGui.QColor(200, 100, 100)  # create a QColor object with RGB values
        self.setStyleSheet("background-color: {};" .format(color.name()))

    def set_color(self, *color): 
        color = QtGui.QColor(*color)
        self.setStyleSheet("background-color: {};" .format(color.name()))


class DropArea(QtWidgets.QLabel): 
    multipleDropped = QtCore.Signal(list)

    def __init__(self, parent=None):
        super(DropArea, self).__init__(parent)
        self.setAcceptDrops(True)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls:
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            links = []
            for url in event.mimeData().urls():
                links.append(str(url.toLocalFile()))

            self.multipleDropped.emit(links)
            # self.addWidgetItems(links)

        else:
            event.ignore()
        

def show(parent=None): 
    if not parent: 
        main_app = QtWidgets.QApplication(sys.argv)
        app = RenameTool()
        app.show()
        stylesheet.set_default(main_app)
        sys.exit(main_app.exec_())
    else: 
        app = RenameTool(parent)
        app.show()


if __name__ == '__main__':
    show()

        
