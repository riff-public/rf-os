import os 
import sys 


class Config: 
    facial_grid = [10, 10]
    element_preset = [
        'CheekSeq',
        'EyebrowSeq',
        'EyeSeq',
        'EyeBall/IrisAlphaSeq',
        'JawSeq',
        'MouthSeq',
        'WrinkleSeq',
        'MouthEffDnSeq',
        'MouthEffUpSeq',
        'TearSeq',
        'SweatSeq',
        'MouthHole/MouthHoleAlphaSeq',
        'EyeRSeq',
        'EyeBallR/IrisAlphaSeq', 
        'EyeBall/PupilAlphaSeq',
        'GBfullFace']

    canvas_size = [(2500, 2500), (4096, 4096)]
    preview_size = 50
    allow_exts = ['.png', '.jpg', '.PNG', '.JPG']
    resize_scale = 0.1 #
    resize_wh = [128, 128]
    lod_prefix = {'low': 'preview', 'high': 'hi'}

    num_row = 10 
    num_col = 24 
    allow_seq = ['%03d' % int(a*10) for a in range(1, num_col + 1)]

    # ui 
    ui_num_row = 5
    ui_num_col = 2
    file_element_preset = ['Ui']
    ui_preview_size = 100

    # still 
    still_num_row = 5
    still_num_col = 2
    still_preview_size = 100
    still_preset = {'EyeBall': ['BlinkCor', 'IrisAlpha', 'IrisCor', 'PupilCor', 'SpecularCor', 'SpecularSpACor', 'SpecularSpBCor'], 
        '/': ['EyeMask', 'WhiteLine', 'WhiteLineDn', 'WhiteLineMask'],
                    'EyeBall/IrisSeq':['IrisSeq.001','IrisSeq.002','IrisSeq.003'],
                    'EyeBall/BlinkSeq':['BlinkSeq.001','BlinkSeq.002','BlinkSeq.003'],
                    'MouthHole':['TeethOneCor','TeethTwoCor','TeethThreeCor.png']}

    # transparency png 
    transparency_png = '{}/transparency_png.png'.format(os.path.dirname(__file__).replace('\\', '/'))
    skip_process_files = [transparency_png]

    # task status
    task = 'fclSeq'
    target_task = 'rigFcl'
    task_status = 'apr'
    target_status = 'rdy'
