# v.0.0.1 polytag switcher
_title = 'Riff Art Publish'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'RFArtPublishUI'

#Import python modules
import sys
import os , fnmatch
import getpass
import json 
from shutil import copy2

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path: 
    sys.path.append(core)

# import config
import rf_config as config
from PIL import Image
from rf_utils.ui import load, stylesheet
from rf_utils import log_utils
from rf_utils import project_info
from rf_utils.context import context_info
from rf_utils.widget import display_widget

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import QtWidgets, QtCore, QtGui


TEMPLATE_PSD_FILE = '%s/core/rf_template/CloudMaker/design/psd.psd' % os.environ.get('RFSCRIPT')
UI_PATH = "%s/core/rf_app/art/app2.ui" % os.environ.get('RFSCRIPT')

PROCESS_OPTION_FILE = "process_options.json"


def append_path(current_path, append_text):
    path = "{current_path}/{append_text}".format(current_path=current_path, append_text=append_text)
    return path


class RFArtApp(QtWidgets.QMainWindow):
    def __init__(self, args=None):
        # super(RFArtApp, self).__init__(args)
        self.ui = load.setup_ui(UI_PATH, self)
        self.ui.show()
        self.ui.tabWidget.setCurrentIndex(0)
        self.process = ""
        self.process_from_client = ""
        self.file_path_from_client = ""
        self.project_name = args[1]
        self.type_name = args[2]
        self.subtype_name = args[3]
        self.asset_name = args[4]
        self.projectInfo = project_info.ProjectInfo(project=self.project_name)
        self.projectInfo.set_project_env()
        self.init_preview_image()
        self.init_signal()
        self.init_design_app()
        self.app_process()

    def init_signal(self):
        self.ui.new_file_btn.clicked.connect(self.handle_new_file)
        self.ui.new_file_from_client.clicked.connect(self.handle_new_file_from_client)
        self.ui.clear_btn.clicked.connect(self.clear_state)
        self.displayWidget.dropped.connect(self.display_trigger)

    def init_design_app(self):
        self.ui.msg_new_file.setVisible(False)
        self.ui.msg_from_client.setVisible(False)
        self.ui.msg_from_client.setVisible(False)
        self.ui.width_label.setVisible(False)
        self.ui.multiply.setVisible(False)
        self.ui.height_label.setVisible(False)

    def init_preview_image(self):
        self.displayWidget = display_widget.Display()
        self.displayWidget.set_label('Image here')
        self.displayWidget.set_style("border: 1px dashed #000;")
        self.ui.browse_layout.addWidget(self.displayWidget)

    def clear_state(self):
        self.displayWidget.clear()
        self.displayWidget.set_label('Image here')
        self.init_design_app()
        self.file_path_from_client = ""
        print("clear.")

    def display_trigger(self, file_path):
        if file_path:
            self.file_path_from_client = file_path
            picture = Image.open(file_path)
            width, height = picture.size
            if width and height:
                print("picture.size width :%s , height: %s", width, height)
                self.displayWidthHeight(width, height)

    def displayWidthHeight(self, width, height):
        self.ui.width_label.setVisible(True)
        self.ui.height_label.setVisible(True)
        self.ui.multiply.setVisible(True)
        self.ui.width_label.setText(str(width))
        self.ui.height_label.setText(str(height))

    def get_project_path(self):
        project_path = ('/').join([self.projectInfo.rootProject, self.project_name])
        return project_path

    def get_asset_data_path(self):
        context = context_info.Context()
        context.update(
            project=self.project_name,
            entityType='design',
            workspace='work',
            entityGrp=self.type_name,
            entityParent=self.subtype_name,
            entity=self.asset_name
        )
        asset = context_info.ContextPathInfo(context=context)
        asset_path = asset.path.data().abs_path()
        return asset_path

    def get_asset_workspace(self):
        context = context_info.Context()
        context.update(
            project=self.project_name,
            entityType='design',
            workspace='work',
            entityGrp=self.type_name,
            entityParent=self.subtype_name,
            entity=self.asset_name
        )
        asset = context_info.ContextPathInfo(context=context)
        asset_path = asset.path.workspace().abs_path()
        return asset_path

    def get_process_options_file_path(self):
        asset_data_path = self.get_asset_data_path()
        process_options_file_path = "{asset_data_path}/{process_options_path}".format(
            asset_data_path=asset_data_path,
            process_options_path=PROCESS_OPTION_FILE
        )
        return process_options_file_path

    def create_process_options_if_not_exists(self):
        asset_path = self.get_asset_data_path()
        process_options_path = "{asset_path}/{process_options}".format(
            asset_path=asset_path,
            process_options=PROCESS_OPTION_FILE
        )
        if not os.path.exists(asset_path):
            os.makedirs(asset_path)
        if not os.path.exists(process_options_path):
            open("{process_options_path}".format(process_options_path=process_options_path), 'w')
        # self.create_process_options_file()

    # def create_process_options_file(self):
    #     asset_path = self.get_asset_data_path()

    def get_process_options(self):
        process_options = []
        process_options_file_path = self.get_process_options_file_path()
        try:
            file = open(process_options_file_path, 'r')
            data = json.load(file)
            for options in data['process_options']:
                process_options.append(options['name'])
            return process_options
        except ValueError:
            return []

    def init_process_combobox(self, current_process=None):
        self.ui.process_combobox.clear()
        self.ui.process_combobox_client.clear()
        process_options = self.get_process_options()
        self.ui.process_combobox.addItems(process_options)
        self.ui.process_combobox_client.addItems(process_options)
        if current_process is not None:
            index = self.ui.process_combobox.findText(
                current_process,
                QtCore.Qt.MatchFixedString
            )
            if index >= 0:
                self.ui.process_combobox.setCurrentIndex(index)
                self.ui.process_combobox_client.setCurrentIndex(index)

    def save_process_options(self):
        process_options_file_path = self.get_process_options_file_path()
        if os.stat(process_options_file_path).st_size == 0:
            initial_process_options = {
                'process_options': [{
                    'name': self.process
                }]
            }
            with open(process_options_file_path, mode='w') as f:
                f.write(json.dumps(initial_process_options, indent=2))
        else:
            with open(process_options_file_path) as process_options_json:
                process_options = json.load(process_options_json)

            process_options['process_options'].append({'name': self.process})
            with open(process_options_file_path, mode='w') as f:
                f.write(json.dumps(process_options, indent=2))

    def save_process_from_client_options(self):
        process_options_file_path = self.get_process_options_file_path()
        if os.stat(process_options_file_path).st_size == 0:
            initial_process_options = {
                'process_options': [{
                    'name': self.process_from_client
                }]
            }
            with open(process_options_file_path, mode='w') as f:
                f.write(json.dumps(initial_process_options, indent=2))
        else:
            with open(process_options_file_path) as process_options_json:
                process_options = json.load(process_options_json)

            process_options['process_options'].append({'name': self.process_from_client})
            with open(process_options_file_path, mode='w') as f:
                f.write(json.dumps(process_options, indent=2))

    def check_can_new_file(self):
        if self.project_name != "" and self.type_name != "" and self.asset_name != "" and self.process != "":
            return True
        return False

    def check_can_new_file_from_client(self):
        if self.project_name != "" and self.type_name != "" and self.asset_name != "" and self.process_from_client != "" and self.file_path_from_client != "":
            return True
        return False

    def loading_msg(self):
        self.ui.msg_new_file.setVisible(True)
        self.ui.msg_new_file.setStyleSheet('color: blue')
        self.ui.msg_new_file.setText("Processing file ...")
        print("Processing file")

    def loading_msg_from_client(self):
        self.ui.msg_from_client.setVisible(True)
        self.ui.msg_from_client.setStyleSheet('color: blue')
        self.ui.msg_from_client.setText("Processing file ...")
        print("Processing file")

    def show_error_msg_box(self, msg):
        msg_box = QtWidgets.QMessageBox()
        msg_box.setIcon(QtWidgets.QMessageBox.Critical)
        msg_box.setText(msg)
        msg_box.setWindowTitle("MessageBox")
        msg_box.exec_()

    def show_file_already_exists_msg_box(self, msg):
        new_version = self.count_version_file()
        self.new_version = str(new_version).zfill(3)
        msg_box = QtWidgets.QMessageBox()
        msg_box.setIcon(QtWidgets.QMessageBox.Warning)
        msg_box.setText(msg)
        msg_box.setInformativeText("Do you want create file '{}_v{}' ?".format(self.file_already_exists, self.new_version))
        msg_box.setWindowTitle("MessageBox")
        msg_box.setStandardButtons(QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)
        msg_box.buttonClicked.connect(self.handle_already_msg_box)
        msg_box.exec_()

    def show_success_msg_box(self):
        msg_box = QtWidgets.QMessageBox()
        msg_box.setIconPixmap(QtGui.QPixmap("O:\\Pipeline\\core\\rf_app\\art\\check-icon.png"))
        msg_box.setText("Create file successfully.")
        msg_box.setWindowTitle("MessageBox")
        msg_box.exec_()

    def handle_already_msg_box(self, event):
        if event.text() == "OK":
            client_file_name = os.path.basename(self.file_path_from_client)
            file_name, ext = os.path.splitext(client_file_name)
            new_file_name = "{asset_name}_{process_from_client}_v{version}_001".format(
                asset_name=self.asset_name,
                process_from_client=self.process_from_client,
                version=self.new_version
            )
            asset_workspace_path = self.get_asset_workspace()
            file_path = "{asset_workspace_path}/{new_file_name}{ext}".format(
                asset_workspace_path=asset_workspace_path,
                new_file_name=new_file_name,
                ext=ext
            )
            # self.save_process_from_client_options()
            copy2(self.file_path_from_client, file_path)
            self.init_process_combobox(self.process_from_client)

            self.ui.msg_new_file.setStyleSheet('color: green')
            self.ui.msg_new_file.setText("Create file successfully.")
            self.show_success_msg_box()
            self.clear_state()
            print("Create file successfully.")
        elif event.text() == "Cancel":
            print "Cancle :", event.text()

    def handle_new_file(self):
        try:
            self.loading_msg()
            self.process = self.ui.process_combobox.currentText()
            print("File information : {project_name}/{type_name}/{subtype_name}/{asset_name}/version/{process}".format(
                project_name=self.project_name,
                type_name=self.type_name,
                subtype_name=self.subtype_name,
                asset_name=self.asset_name,
                process=self.process
            ))
            if self.check_can_new_file():
                self.ui.msg_new_file.setVisible(True)
                msg = self.create_file()
                if msg == "File already exists.":
                    self.ui.msg_new_file.setStyleSheet('color: red')
                    self.ui.msg_new_file.setText(msg)
                    self.show_error_msg_box("File already exists.")
                    print("File already exists.")
                else:
                    self.ui.msg_new_file.setStyleSheet('color: green')
                    self.ui.msg_new_file.setText(msg)
                    self.show_success_msg_box()
                    print("Create file successfully.")
            else:
                self.ui.msg_new_file.setVisible(True)
                self.ui.msg_new_file.setStyleSheet('color: red')
                self.ui.msg_new_file.setText("Failed create file. Please enter the information.")
                self.show_error_msg_box("Failed create file. Please enter the information.")
                print("Cannot create file.")
        except Exception as e:
            self.ui.msg_new_file.setVisible(True)
            self.ui.msg_new_file.setStyleSheet('color: red')
            self.ui.msg_new_file.setText("Failed create file.")
            self.show_error_msg_box("Failed create file.")
            print("Cannot create file. %s", e)

    def handle_new_file_from_client(self):
        try:
            self.loading_msg_from_client()
            self.process_from_client = self.ui.process_combobox_client.currentText()
            print("File information : {project_name}/{type_name}/{subtype_name}/{asset_name}/version/{process_from_client}".format(
                project_name=self.project_name,
                type_name=self.type_name,
                subtype_name=self.subtype_name,
                asset_name=self.asset_name,
                process_from_client=self.process_from_client
            ))
            if self.check_can_new_file_from_client():
                self.ui.msg_from_client.setVisible(True)
                msg = self.create_file_from_client()
                if msg == "File already exists.":
                    self.ui.msg_from_client.setStyleSheet('color: red')
                    self.ui.msg_from_client.setText(msg)
                    self.show_file_already_exists_msg_box("File already exists.")
                    print("File already exists.")
                else:
                    self.ui.msg_from_client.setStyleSheet('color: green')
                    self.ui.msg_from_client.setText(msg)
                    self.show_success_msg_box()
                    self.clear_state()
                    print("Create file successfully.")
            else:
                self.ui.msg_from_client.setVisible(True)
                self.ui.msg_from_client.setStyleSheet('color: red')
                self.ui.msg_from_client.setText("Failed create file. Please enter the information.")
                self.show_error_msg_box("Failed create file. Please enter the information.")
                print("Cannot create file. Please enter the information.")
        except Exception as e:
            self.ui.msg_from_client.setVisible(True)
            self.ui.msg_from_client.setStyleSheet('color: red')
            self.ui.msg_from_client.setText("Failed create file.")
            self.show_error_msg_box("Failed create file.")
            print("Failed create file. %s", e)

    def create_file(self):
        file_name = "{asset_name}_{process}_v001_001".format(asset_name=self.asset_name, process=self.process)
        asset_workspace_path = self.get_asset_workspace()
        file_path = "{asset_workspace_path}/{file_name}.psd".format(
            asset_workspace_path=self.get_asset_workspace(),
            file_name=file_name
        )
        try:
            open(file_path, 'r')
            return "File already exists."
        except IOError:
            self.save_process_options()
            copy2(TEMPLATE_PSD_FILE, file_path)
            self.init_process_combobox(self.process)
            return "Create file successfully."

    def create_file_from_client(self):
        client_file_name = os.path.basename(self.file_path_from_client)
        file_name, ext = os.path.splitext(client_file_name)
        new_file_name = "{asset_name}_{process_from_client}_v001_001".format(asset_name=self.asset_name, process_from_client=self.process_from_client)
        asset_workspace_path = self.get_asset_workspace()
        file_path = "{asset_workspace_path}/{new_file_name}{ext}".format(
            asset_workspace_path=asset_workspace_path,
            new_file_name=new_file_name,
            ext=ext
        )
        try:
            open(file_path, 'r')
            self.file_already_exists = "{asset_name}_{process_from_client}".format(asset_name=self.asset_name, process_from_client=self.process_from_client)
            return "File already exists."
        except IOError:
            self.save_process_from_client_options()
            copy2(self.file_path_from_client, file_path)
            self.init_process_combobox(self.process_from_client)
            return "Create file successfully."

    def count_version_file(self):
        version_file = 0
        asset_workspace_path = self.get_asset_workspace()
        dir_assets = os.listdir(asset_workspace_path)
        for asset in dir_assets:
            if self.file_already_exists in asset:
                asset_name, process, version, ext = asset.split("_")
                version_number = int(version[1:])
                if version_number > version_file:
                    version_file = version_number
        return version_file + 1

    def app_process(self):
        self.create_process_options_if_not_exists()
        self.init_process_combobox()


if __name__ == '__main__':
    app = QtWidgets.QApplication.instance()
    if not app:
        app = QtWidgets.QApplication(sys.argv)
        stylesheet.set_default(app)
    myApp = RFArtApp(sys.argv)
    # stylesheet.set_default(app)
    sys.exit(app.exec_())
