# v.0.0.1 polytag switcher
_title = 'Riff Art Publish'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'RFArtPublishUI'

#Import python modules
import sys
import os , fnmatch
import getpass
import json 
from shutil import copy2

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path: 
    sys.path.append(core)

# import config
import rf_config as config
from rf_utils.ui import load
from rf_utils import log_utils
from rf_utils import project_info
from rf_utils.context import context_info

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import QtWidgets, QtCore


TEMPLATE_PSD_FILE = '../../rf_template/CloudMaker/design/psd.psd'


def append_path(current_path, append_text):
    path = "{current_path}/{append_text}".format(current_path=current_path, append_text=append_text)
    return path


class RFArtApp(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(RFArtApp, self).__init__(parent)
        self.ui = load.setup_ui('C:/Users/teeruk.i/projects/rf_pipleline_tools/core/rf_app/art/ui.ui', self)
        self.ui.show()

        self.project_name = ""
        self.type_name = ""
        self.subtype_name = ""
        self.asset_name = ""
        self.process = ""

        projectInfo = project_info.ProjectInfo()
        projects = projectInfo.list_all(False)
        self.ui.project_comboBox.addItem("------ Please Select ------")
        self.ui.project_comboBox.addItems(projects)
        self.ui.new_file_btn.clicked.connect(self.handle_new_file)
        self.ui.msg_new_file.setVisible(False)
        self.init_signals()

    def get_project_path(self):
        projectInfo = project_info.ProjectInfo(project=self.project_name)
        projectInfo.set_project_env()
        project_path = ('/').join([projectInfo.rootProject, self.project_name])
        return project_path

    def get_type_path(self):
        project_path = self.get_project_path()
        type_path = "{project_path}/design/{type_name}".format(
            project_path=project_path,
            type_name=self.type_name
        )
        return type_path

    def get_subtype_path(self):
        project_path = self.get_project_path()
        type_path = "{project_path}/design/{type_name}/{subtype_name}".format(
            project_path=project_path,
            type_name=self.type_name,
            subtype_name=self.subtype_name,
        )
        return type_path

    def get_asset_data_path(self):
        context = context_info.Context()
        context.update(
            project=self.project_name,
            entityType='design',
            workspace='work',
            entityGrp=self.type_name,
            entityParent=self.subtype_name,
            entity=self.asset_name
        )
        asset = context_info.ContextPathInfo(context=context)
        asset_path = asset.path.data().abs_path()
        return asset_path

    def get_process_options_file_path(self):
        asset_data_path = self.get_asset_data_path()
        process_options_file_path = "{asset_data_path}/process_options.json".format(
            asset_data_path=asset_data_path
        )
        return process_options_file_path

    def get_process_options(self):
        process_options = []
        asset_path = self.get_asset_data_path()
        if not os.path.exists(asset_path):
            os.makedirs(asset_path)
        process_options_file_path = self.get_process_options_file_path()
        try:
            file = open(process_options_file_path, 'r')
            data = json.load(file)
            for options in data['process_options']:
                process_options.append(options['name'])
            return process_options
        except IOError:
            open(process_options_file_path, 'w')
            return []

    def filter_directory(self, dirs):
        directories_filtered = []
        for directory in dirs:
            if directory[0] is not '_':
                directories_filtered.append(directory)
        return directories_filtered

    def save_process_options(self):
        process_options_file_path = self.get_process_options_file_path()
        if os.stat(process_options_file_path).st_size == 0:
            initial_process_options = {
                'process_options': [{
                    'name': self.process
                }]
            }
            with open(process_options_file_path, mode='w') as f:
                f.write(json.dumps(initial_process_options, indent=2))
        else:
            with open(process_options_file_path) as process_options_json:
                process_options = json.load(process_options_json)

            process_options['process_options'].append({'name': self.process})
            with open(process_options_file_path, mode='w') as f:
                f.write(json.dumps(process_options, indent=2))

    def loadingMsg(self):
        self.ui.msg_new_file.setVisible(True)
        self.ui.msg_new_file.setStyleSheet('color: blue')
        self.ui.msg_new_file.setText("Processing file ...")
        print("Processing file")

    def handle_new_file(self):
        self.loadingMsg()
        self.process = self.ui.process_comboBox.currentText()
        print("File information : {project_name}/{type_name}/{subtype_name}/{asset_name}/_data/{process}".format(
            project_name=self.project_name,
            type_name=self.type_name,
            subtype_name=self.subtype_name,
            asset_name=self.asset_name,
            process=self.process
        ))
        if self.check_can_new_file():
            self.ui.msg_new_file.setVisible(True)
            msg = self.create_file()
            self.ui.msg_new_file.setStyleSheet('color: green')
            self.ui.msg_new_file.setText(msg)
            print("Create file successfully.")
        else:
            self.ui.msg_new_file.setVisible(True)
            self.ui.msg_new_file.setStyleSheet('color: red')
            self.ui.msg_new_file.setText("Failed create file. Please enter the information.")
            print("Cannot create file.")

    def init_signals(self):
        self.ui.project_comboBox.currentIndexChanged.connect(self.project_callback)
        self.ui.type_comboBox.currentIndexChanged.connect(self.type_selected_callback)
        self.ui.subtype_comboBox.currentIndexChanged.connect(self.subtype_selected_callback)
        self.ui.asset_comboBox.currentIndexChanged.connect(self.asset_selected_callback)

    def init_type_box(self):
        project_path = self.get_project_path()
        types_directorys = self.filter_directory(os.listdir("{project_path}/design".format(project_path=project_path)))
        self.ui.type_comboBox.addItems(types_directorys)

    def init_subtype_box(self):
        type_path = self.get_type_path()
        subtype_directory = self.filter_directory(os.listdir(type_path))
        self.ui.subtype_comboBox.addItems(subtype_directory)

    def init_asset_box(self):
        subtype_path = self.get_subtype_path()
        assets_directory = self.filter_directory(os.listdir(subtype_path))
        self.ui.asset_comboBox.addItems(assets_directory)

    def init_process_combobox(self, current_process=None):
        self.ui.process_comboBox.clear()
        process_options = self.get_process_options()
        self.ui.process_comboBox.addItems(process_options)
        if current_process is not None:
            index = self.ui.process_comboBox.findText(
                current_process,
                QtCore.Qt.MatchFixedString
            )
            if index >= 0:
                self.ui.process_comboBox.setCurrentIndex(index)

    def project_callback(self, args):
        self.ui.type_comboBox.clear()
        self.type_name = ""
        self.project_name = self.ui.project_comboBox.currentText()
        self.init_type_box()

    def type_selected_callback(self, args):
        self.subtype_name = ""
        self.ui.subtype_comboBox.clear()
        self.type_name = self.ui.type_comboBox.currentText()
        self.init_subtype_box()

    def subtype_selected_callback(self, args):
        self.asset_name = ""
        self.ui.asset_comboBox.clear()
        self.subtype_name = self.ui.subtype_comboBox.currentText()
        self.init_asset_box()

    def asset_selected_callback(self, args):
        self.asset_name = self.ui.asset_comboBox.currentText()
        self.init_process_combobox()

    def check_can_new_file(self):
        if self.project_name != "" and self.type_name != "" and self.asset_name != "" and self.process != "":
            return True
        return False

    def create_file(self):
        file_name = "{asset_name}_{process}_001".format(asset_name=self.asset_name, process=self.process)
        file_path = "{project_path}/design/{type_name}/{subtype_name}/{asset_name}/version/{file_name}.psd".format(
            project_path=self.get_project_path(),
            type_name=self.type_name,
            subtype_name=self.subtype_name,
            asset_name=self.asset_name,
            file_name=file_name
        )
        try:
            open(file_path, 'r')
            return "File already exists."
        except IOError:
            self.save_process_options()
            copy2(TEMPLATE_PSD_FILE, file_path)
            self.init_process_combobox(self.process)
            return "Create file successfully."

        # self.ui.new_file_btn.setEnabled(True)
    def test (self):
        print "aaaa"

if __name__ == '__main__':
    app = QtWidgets.QApplication.instance()
    if not app:
        app = QtWidgets.QApplication(sys.argv)
    myApp = RFArtApp()
    # stylesheet.set_default(app)
    sys.exit(app.exec_())
