import sys
import getpass
from Qt import QtCore
from Qt import QtGui
from Qt import QtWidgets
import rf_config as config 
from rf_utils.ui import stylesheet


# GUI FILE
from ui_main import Ui_MainWindow

class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)      
        self.init_signals()

    def init_signals(self): 
        self.ui.assetButton.clicked.connect(self.runAssetPublish)
        self.ui.shotButton.clicked.connect(self.runShotPublish)
        self.ui.layoutButton.clicked.connect(self.runMattepaintDomePublish)
        self.ui.compButton.clicked.connect(self.runRenderPublish)

    def runAssetPublish(self): 
        from rf_app.publish.art import design_app
        design_app.show2(0, self)

    def runShotPublish(self): 
        # colorscript
        from rf_app.publish.seq import shot_app
        shot_app.show2(self)

    def runMattepaintDomePublish(self): 
        # change tab to mattepaint layout
        from rf_app.publish.art import design_app
        design_app.show2(1, self)

    def runRenderPublish(self): 
        from rf_app.publish.scene.render.browser import app
        myapp = app.show2(self)



def show(mode='default'):
    app = QtWidgets.QApplication.instance()
    if not app:
        app = QtWidgets.QApplication(sys.argv)
    myApp = MainWindow()
    myApp.show()
    stylesheet.set_default(app)
    sys.exit(app.exec_())


# if __name__ == "__main__":
#     app = QApplication(sys.argv)
#     window = MainWindow()
#     window.show()
#     sys.exit(app.exec_())
