from rf_utils.sg import sg_process
sg = sg_process.sg 


def get_subtype():
    return sg.schema_field_read('Asset', 'sg_subtype')['sg_subtype']["properties"]["valid_values"]["value"]


def get_type():
    return sg.schema_field_read('Asset', 'sg_asset_type')['sg_asset_type']["properties"]["valid_values"]["value"]
