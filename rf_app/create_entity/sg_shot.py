from rf_utils.sg import sg_process
from rf_utils import custom_exception
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
sg = sg_process.sg 
CACHE_PROJECT = dict()


def find_project(project): 
	if not project in CACHE_PROJECT.keys(): 
		project_entity = sg.find_one('Project', [['name', 'is', project]], ['name', 'id'])
		if project_entity: 
			CACHE_PROJECT[project] = project_entity
	project_entity = CACHE_PROJECT.get(project)
	if project_entity: 
		return project_entity
	else: 
		raise custom_exception.PipelineError('Error', 'Project "{}" not found'format(project))


def find_episode(project, episode): 
	project_entity = find_project(project)
	key = '{}:{}'format(project, episode)
	if not key in CACHE_EPISODE.keys(): 
		episode_entity = sg.find_one('Scene', [['project', 'is', project_entity], ['code', 'is', episode]], ['code', 'id'])
		if episode_entity: 
			CACHE_EPISODE[key] = episode_entity
	episode_entity = CACHE_EPISODE.get(key)
	if episode_entity: 
		return episode_entity
	else: 
		raise custom_exception.PipelineError('Error', 'Episode "{}" not found on project {}'format(episode, project))


def find_sequence(project, episode, sequence): 
	project_entity = find_project(project)
	episode_entity = find_episode(project, episode)
	key = '{}:{}:{}'.format(project, episode, sequence)
	if not key in CACHE_SEQUENCE.keys(): 
		sequence_entity = sg.find_one('Sequence', [['project', 'is', project_entity], ['code', 'is', sequence]], 
			['code', 'id', 'sg_shortcode', 'sg_episodes'])
		if sequence_entity: 
			CACHE_SEQUENCE[key] = sequence_entity

	sequence_entity = CACHE_SEQUENCE.get(key)
	if sequence_entity: 
		return sequence_entity
	else: 
		raise custom_exception.PipelineError('Error', 'Sequence "{}" not found on project {}'format(sequence, project))


def create_episode(project, episode): 
	""" create an episode 
	Args: 
		project (str): "Hanuman"
		episode (str): "ch01"
	Returns: 
		dict : {'code': episode, 'type': 'Scene'}
	""" 
	project_entity = find_project(project)
	episode_entity = sg.create('Scene', {'project': project_entity, 'code': episode})
	return episode_entity


def create_sequence(project, episode, sequence, shortcode): 
	""" create sequence 
	Args: 
		project (str): "Hanuman"
		episode (str): "ch01"
		sequence (str): hmn_ch01_q0010
		shortcode (str): q0010 
	Returns: 
		dict : {'code': 'Sequence', 'type': 'Sequence'}
	""" 
	project_entity = find_project(project)
	episode_entity = find_episode(project, episode)
	data = {
		'project': project_entity, 
		'sg_episodes': episode_entity, 
		'code': sequence, 
		'sg_shortcode': shortcode}
    sequence_entity = sg.create('Sequence', data)
    return sequence_entity


def create_shot(project, episode, sequence, shotname, shortcode, template): 
	project_entity = find_project(project)
	episode_entity = find_episode(project, episode)
	sequence_entity = find_sequence(project, episode, sequence)
	data = {
		'project': projectEntity, 
		'code': name, 
		'sg_episode': episodeEntity,
            'sg_sequence_code': sequence_entity['sg_shortcode'], 
            'sg_sequence': sequence_entity, 
            'sg_shortcode': shortcode, 
            'sg_shot_type': 'Shot', 
            'task_template': template
            }

    if not entity:
        result = sg.create('Shot', data)
    return sequence_entity
