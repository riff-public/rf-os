_UI = 'CreateEntity'
import sys
import os
import time
import subprocess
import rf_config as config 
from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCore
from rf_utils.ui import stylesheet
from functools import partial
from rf_utils import thread_pool
from rf_utils import user_info
from rf_utils.widget import project_widget
from rf_utils.widget import user_widget
from rf_utils.sg import sg_process
from rf_utils.pipeline import create_scene
from rf_utils.context import context_info
from rf_utils.widget import dialog
DIRNAME = os.path.dirname(__file__).replace('/', '\\')


class CreateEntity(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(CreateEntity, self).__init__(parent=parent)
        self.setObjectName(_UI)
        self.threadpool = QtCore.QThreadPool()
        self.type_ep_row = 0 
        self.subtype_seq_row = 0 
        self.setup_ui()
        self.init_signals()
        self.init_functions()

    def setup_ui(self):
        self.widget = QtWidgets.QWidget()
        self.main_layout = QtWidgets.QVBoxLayout(self.widget)
        self.top_layout = QtWidgets.QGridLayout()
        self.type_layout = QtWidgets.QHBoxLayout()
        self.mid_layout = QtWidgets.QGridLayout()
        self.end_layout = QtWidgets.QGridLayout()
        self.button_layout = QtWidgets.QHBoxLayout()

        self.label = QtWidgets.QLabel('Create Asset / Shot')
        self.main_layout.addWidget(self.label)

        # project asset scene user 
        self.project_widget = project_widget.SGProjectComboBox(sg=sg_process.sg, layout=QtWidgets.QHBoxLayout())
        self.user_widget = user_widget.UserComboBox()

        self.spacer1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        
        self.top_layout.addWidget(self.project_widget, 0, 0)
        self.top_layout.addItem(self.spacer1, 0, 1)
        self.top_layout.addWidget(self.user_widget, 0, 2)

        self.top_layout.setColumnStretch(0, 0)
        self.top_layout.setColumnStretch(1, 2)
        self.top_layout.setColumnStretch(2, 3)
        self.line(self.main_layout)
        
        # entity type 
        self.asset_rb = QtWidgets.QRadioButton('Asset')
        self.scene_rb = QtWidgets.QRadioButton('Scene')
        self.asset_rb.setChecked(True)
        self.spacer2 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.type_layout.addWidget(self.asset_rb)
        self.type_layout.addWidget(self.scene_rb)
        self.type_layout.addItem(self.spacer2)

        self.type_layout.setStretch(0, 1)
        self.type_layout.setStretch(1, 1)
        self.type_layout.setStretch(2, 2)

        # type subtype 
        self.type_ep_label = QtWidgets.QLabel('Type')
        self.type_ep_widget = QtWidgets.QListWidget()
        self.subtype_seq_label = QtWidgets.QLabel('SubType')
        self.subtype_seq_widget = QtWidgets.QListWidget()
        self.type_ep_input = QtWidgets.QLineEdit()
        self.type_ep_button = QtWidgets.QPushButton('+')
        self.subtype_seq_input = QtWidgets.QLineEdit()
        self.subtype_seq_button = QtWidgets.QPushButton('+')

        self.type_ep_widget.setMinimumSize(QtCore.QSize(0, 160))
        self.subtype_seq_widget.setMinimumSize(QtCore.QSize(0, 160))

        self.mid_layout.addWidget(self.type_ep_label, 0, 0)
        self.mid_layout.addWidget(self.type_ep_widget, 1, 0, 1, 2)
        self.mid_layout.addWidget(self.subtype_seq_label, 0, 2)
        self.mid_layout.addWidget(self.subtype_seq_widget, 1, 2, 1, 2)
        self.mid_layout.addWidget(self.type_ep_input, 2, 0)
        self.mid_layout.addWidget(self.type_ep_button, 2, 1)
        self.mid_layout.addWidget(self.subtype_seq_input, 2, 2)
        self.mid_layout.addWidget(self.subtype_seq_button, 2, 3)

        self.mid_layout.setColumnStretch(0, 1)
        self.mid_layout.setColumnStretch(1, 1)
        self.mid_layout.setColumnStretch(2, 1)
        self.mid_layout.setColumnStretch(3, 1)

        self.type_ep_button.setMaximumSize(QtCore.QSize(30, 30))
        self.subtype_seq_button.setMaximumSize(QtCore.QSize(30, 30))

        # name 
        self.name_label = QtWidgets.QLabel('Asset name : ')
        self.name_lineEdit = QtWidgets.QLineEdit()
        self.preview = QtWidgets.QLabel('Preview : ')
        self.preview_label = QtWidgets.QLabel('e.g. budsaba')
        self.override_cb = QtWidgets.QCheckBox('Manual name')

        self.description_label = QtWidgets.QLabel('Description : ')
        self.description_widget = QtWidgets.QPlainTextEdit()
        self.description_label.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)

        self.end_layout.addWidget(self.preview, 0, 0)
        self.end_layout.addWidget(self.preview_label, 0, 1)
        self.end_layout.addWidget(self.name_label, 1, 0)
        self.end_layout.addWidget(self.name_lineEdit, 1, 1)
        self.end_layout.addWidget(self.override_cb, 1, 2)
        self.end_layout.addWidget(self.description_label, 2, 0)
        self.end_layout.addWidget(self.description_widget, 2, 1, 1, 3)

        # button 
        self.create_button = QtWidgets.QPushButton('Create')
        self.spacer3 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.create_button.setMinimumSize(QtCore.QSize(120, 30))
        
        self.button_layout.addItem(self.spacer3)
        self.button_layout.addWidget(self.create_button)
        self.button_layout.setStretch(0, 1)
        self.button_layout.setStretch(1, 1)

        self.status = QtWidgets.QLabel()
        self.main_layout.addLayout(self.top_layout)
        self.line(self.main_layout)
        self.main_layout.addLayout(self.type_layout)
        self.main_layout.addLayout(self.mid_layout)
        self.line(self.main_layout)
        self.main_layout.addLayout(self.end_layout)
        self.main_layout.addLayout(self.button_layout)
        self.spacer = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        # self.main_layout.addItem(self.spacer)
        self.main_layout.addWidget(self.status)
        self.setCentralWidget(self.widget)
        self.resize(440, 560)

        self.main_layout.setStretch(0, 0)
        self.main_layout.setStretch(1, 0)
        self.main_layout.setStretch(2, 0)
        self.main_layout.setStretch(3, 0)
        self.main_layout.setStretch(4, 0)
        self.main_layout.setStretch(5, 3)
        self.main_layout.setStretch(6, 1)
        self.main_layout.setStretch(7, 0)
        self.main_layout.setStretch(8, 0)

        self.top_layout.setSpacing(0)
        self.type_layout.setSpacing(0)
        self.setWindowTitle('RF Asset - Shot Creator')

    def line(self, layout=None):
        line = QtWidgets.QFrame()
        line.setFrameShape(QtWidgets.QFrame.HLine)
        line.setFrameShadow(QtWidgets.QFrame.Sunken)
        layout.addWidget(line) if layout else None

    def init_functions(self): 
        project = self.project_widget.current_item()
        # setup context 
        context = context_info.Context()
        context.update(project=project['name'], projectCode=project['sg_project_code'])
        self.entity = context_info.ContextPathInfo(context=context)

        self.set_mode()
        if self.is_asset: 
            self.entity.context.update(entityType='asset')
            self.init_asset()

        if self.is_scene: 
            self.entity.context.update(entityType='scene')
            self.init_scene(project)

    def init_signals(self): 
        self.asset_rb.clicked.connect(self.init_functions)
        self.scene_rb.clicked.connect(self.init_functions)
        self.project_widget.projectChanged.connect(self.init_functions)
        self.type_ep_widget.itemSelectionChanged.connect(self.type_ep_changed)
        self.subtype_seq_widget.itemSelectionChanged.connect(self.name_changed)
        self.name_lineEdit.textChanged.connect(self.name_changed)
        self.override_cb.stateChanged.connect(self.name_changed)
        self.create_button.clicked.connect(self.create)

        # create button 
        self.type_ep_button.clicked.connect(self.create_type_ep)
        self.subtype_seq_button.clicked.connect(self.create_subtype_seq)

    def set_mode(self): 
        self.is_asset = True if self.asset_rb.isChecked() else False 
        self.is_scene = not self.is_asset
        self.type_ep_widget.clear()
        self.subtype_seq_widget.clear()

        if self.is_asset: 
            self.type_ep_label.setText('Type')
            self.subtype_seq_label.setText('SubType')
            self.name_label.setText('Asset name : ')
            self.preview_label.setText('e.g. budsaba')

        elif self.is_scene: 
            self.type_ep_label.setText('Episode')
            self.subtype_seq_label.setText('Sequence')
            self.name_label.setText('Shot name : ')
            self.preview_label.setText('e.g. s0010')

    def init_asset(self): 
        worker = thread_pool.Worker(self.fetch_asset)
        worker.signals.result.connect(self.fetch_asset_finished)
        self.threadpool.start(worker)

    def fetch_asset(self): 
        types = sg_process.get_type()
        subtypes = sg_process.get_subtype()
        return (types, subtypes)

    def fetch_asset_finished(self, data): 
        types, subtypes = data
        self.type_ep_widget.clear()
        self.type_ep_widget.addItems(sorted(types))
        self.subtype_seq_widget.clear()

        for s in subtypes: 
            item = QtWidgets.QListWidgetItem(self.type_ep_widget)
            item.setText(s)
            item.setData(QtCore.Qt.UserRole, s)

    def init_scene(self, project_entity): 
        worker = thread_pool.Worker(self.fetch_scene, project_entity['name'])
        worker.signals.result.connect(self.fetch_scene_finished)
        self.threadpool.start(worker)

    def fetch_scene(self, project): 
        eps = sg_process.get_episodes(project)
        seqs = sg_process.get_all_sequences(project)
        return (eps, seqs)

    def fetch_scene_finished(self, data): 
        self.scene_dict = self.pack_scene_dict(data)
        self.type_ep_widget.clear()
        for text in sorted(self.scene_dict.keys()): 
            item = QtWidgets.QListWidgetItem(self.type_ep_widget)
            item.setText(text)
            item.setData(QtCore.Qt.UserRole, text)

        if self.type_ep_row: 
            self.type_ep_widget.setCurrentRow(self.type_ep_row)

    def type_ep_changed(self): 
        type_ep = self.type_ep_widget.currentItem().text()
        self.entity.context.update(entityGrp=type_ep)

        if self.is_scene: 
            self.subtype_seq_widget.clear() 

            for seq in sorted(self.scene_dict[type_ep]): 
                item = QtWidgets.QListWidgetItem(self.subtype_seq_widget)
                item.setText(seq['sg_shortcode'])
                item.setData(QtCore.Qt.UserRole, seq)
                item.setToolTip(seq['code'])

    def name_changed(self): 
        type_ep = None 
        subtype_seq = None
        if self.type_ep_widget.currentItem(): 
            type_ep = self.type_ep_widget.currentItem().data(QtCore.Qt.UserRole)
        if self.subtype_seq_widget.currentItem(): 
            subtype_seq = self.subtype_seq_widget.currentItem().data(QtCore.Qt.UserRole)
        text = self.name_lineEdit.text()
        if type_ep and subtype_seq and text: 
            if not self.override_cb.isChecked(): 
                self.entity.context.update(entityParent=subtype_seq['sg_shortcode'], entityCode=text)
                self.entity_name = self.entity.sg_name
            else: 
                self.entity_name = text
            self.preview_label.setText(self.entity_name)

    def create_type_ep(self): 
        if self.is_scene: 
            project = self.project_widget.current_item()
            episode = self.type_ep_input.text()
            if project and episode: 
                result = sg_process.create_episode(project['name'], episode)
                self.init_scene(project)

        if self.is_asset: 
            logger.warning('Not supported yet')

    def create_subtype_seq(self): 
        if self.is_scene: 
            project = self.project_widget.current_item()
            episode = self.type_ep_widget.currentItem().text()
            sequence = self.subtype_seq_input.text()
            shortcode = self.entity.projectInfo.scene.sequence_key # all 
            self.type_ep_row = self.type_ep_widget.currentRow()

            if project and episode and sequence: 

                self.entity.context.update(entityParent=sequence, entityCode=shortcode)
                print self.entity.sg_seq_name
                return 
                name = self.entity.sg_name
                dialog.CustomMessageBox.show('Create Sequence', 'Create Sequence "{}"?'.format(self.entity.sg_seq_name))
                self.status.setText('Creating Sequence {} ...'.format(sequence))
                self.subtype_seq_button.setEnabled(False)
                self.subtype_seq_input.setEnabled(False)
                # print (entity.sg_seq_name)
                # print(entity.sg_name)
                worker = thread_pool.Worker(create_scene.create, project['name'], name, episode, sequence, shortcode, 'Sequence', True)
                worker.signals.result.connect(self.create_subtype_seq_finished)
                self.threadpool.start(worker)
                # create_scene.create(project['name'], name, episode, sequence, shortcode, shotType='Sequence', link=True)

    def create_subtype_seq_finished(self, result): 
        dirs, sg_entity = result 
        project = self.project_widget.current_item()
        self.init_scene(project)
        # self.type_ep_changed()
        self.status.setText('Sequence created')
        self.subtype_seq_button.setEnabled(True)
        self.subtype_seq_input.setEnabled(True)

    def create(self): 
        if self.is_scene: 
            self.status.setText('Creating shot {} ...'.format(self.entity_name))
            self.create_button.setEnabled(False)
            # print self.entity.project, self.entity_name, self.entity.episode, self.entity.sequence, self.entity.name_code
            worker = thread_pool.Worker(
                create_scene.create, 
                self.entity.project, 
                self.entity_name, 
                self.entity.episode, 
                self.entity.sequence, 
                self.entity.name_code, 'Shot', True)
            worker.signals.result.connect(self.create_finished)
            self.threadpool.start(worker)

    def create_finished(self, result): 
        self.create_button.setEnabled(True)
        self.status.setText('Create shot finished')

    def pack_scene_dict(self, data): 
        scene_dict = dict()
        eps, seqs = data 
        for ep in eps: 
            scene_dict[ep['code']] = list()

        for seq in seqs: 
            if seq['sg_episode']: 
                ep = seq['sg_episode']['name']
                if ep in scene_dict.keys(): 
                    scene_dict[ep].append(seq)
                else: 
                    scene_dict[ep] = [seq]
        return scene_dict

def show():
    if config.isMaya: 
        from rftool.utils.ui import maya_win
        maya_win.deleteUI(_UI)
        myApp = CreateEntity(maya_win.getMayaWindow())
        myApp.show()
        return myApp
    else: 
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        myApp = CreateEntity()
        myApp.show()
        stylesheet.set_default(app)
        sys.exit(app.exec_())
