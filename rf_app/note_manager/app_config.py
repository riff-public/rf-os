import os
import sys 
from collections import OrderedDict

MODULE_DIR = os.path.dirname(sys.modules[__name__].__file__)

class Icon: 
    pencil = '{}/icons/pencil_icon.png'.format(MODULE_DIR)
    attachment = '{}/icons/paperclip_icon.png'.format(MODULE_DIR)
    snap = '{}/icons/snap.png'.format(MODULE_DIR)
    inbox = '{}/icons/email_icon.png'.format(MODULE_DIR)
    compose = '{}/icons/compose_icon.png'.format(MODULE_DIR)


class StatusConfig: 
    path = '%s/icons/status' % MODULE_DIR
    setting = OrderedDict()
    setting['All'] = {'display': 'All', 'icon': ''} 
    setting['wtg'] = {'display': 'Wating to Start', 'icon': '{}/wtg_icon.png'.format(path)} 
    setting['rdy'] = {'display': 'Ready to Start', 'icon': '{}/rdy_icon.png'.format(path)} 
    setting['ip'] = {'display': 'In Progress', 'icon': '{}/ip_icon.png'.format(path)} 
    setting['rev'] = {'display': 'Daily', 'icon': '{}/review_icon.png'.format(path)} 
    setting['pub'] = {'display': 'Pending Publish', 'icon': '{}/pub_icon.png'.format(path)} 
    setting['fix'] = {'display': 'Need Fix', 'icon': '{}/fix_icon.png'.format(path)} 
    setting['hld'] = {'display': 'On Hold', 'icon': '{}/hld_icon.png'.format(path)} 
    setting['apr'] = {'display': 'Approved', 'icon': '{}/aprv_icon.png'.format(path)} 
    setting['p_aprv'] = {'display': 'Pending Approve', 'icon': '{}/p_aprv_icon.png'.format(path)} 
    setting['omt'] = {'display': 'Omit', 'icon': '{}/omt_icon.png'.format(path)}
    setting['ardy'] = {'display': 'Anim Ready', 'icon': '{}/ardy_icon.png'.format(path)}
    setting['clsd'] = {'display': 'Closed', 'icon': '{}/clsd_icon.png'.format(path)}
    setting['res'] = {'display': 'Resolved', 'icon': '{}/clsd_icon.png'.format(path)}
    setting['cmpt'] = {'display': 'Complete', 'icon': '{}/cmpt_icon.png'.format(path)}
    setting['opn'] = {'display': 'Open', 'icon': '{}/rdy_icon.png'.format(path)}
    setting['wip'] = {'display': 'Work In Progress', 'icon': '{}/wip_icon.png'.format(path)}
