#!/usr/bin/env python
# -- coding: utf-8 --

import os
import sys
import traceback
from collections import OrderedDict

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir = os.path.dirname(module_path)
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils.widget import project_widget
from rf_utils.widget import entity_browse_widget
from rf_utils.widget import sg_entity_widget
from rf_utils.widget import entity_step_widget
from rf_utils.widget import filter_widget
from rf_utils.widget import register_file_widget
from rf_utils.widget import status_widget
from rf_utils.widget import user_widget
from rf_utils import icon
from . import app_config
from rf_app.note_manager.widget import inbox_widget
from rf_app.note_manager.widget import content_widget
from rf_utils.widget.ticket import ticket_form
# reload(ticket_form)
from rf_app.note_manager.widget import tab_preset
# reload(tab_preset)
sg = None


class NoteManagerUi(QtWidgets.QWidget):
    status = QtCore.Signal(str)

    def __init__(self, threadpool=None, parent=None):
        super(NoteManagerUi, self).__init__(parent)
        self.tab_index = {'inbox': 0, 'ticket_note_form': 1}
        self.threadpool = threadpool
        self.form_init = False
        self.setup_ui()

    def setup_ui(self): 
        self.layout = QtWidgets.QVBoxLayout()
        self.top_layout = QtWidgets.QGridLayout()
        self.tab_widget = QtWidgets.QTabWidget()

        # inbox 
        self.splitter = QtWidgets.QSplitter(QtCore.Qt.Horizontal)
        self.bottom_layout = QtWidgets.QHBoxLayout()

        # outbox 
        self.splitter_outbox = QtWidgets.QSplitter(QtCore.Qt.Horizontal)

        self.tab_widget.addTab(self.splitter, '')
        self.tab_widget.setTabIcon(self.tab_index.get('inbox'), QtGui.QIcon(app_config.Icon.inbox))
        self.tab_widget.setIconSize(QtCore.QSize(32, 32)) 

        self.tab_widget.addTab(self.splitter_outbox, '')
        self.tab_widget.setTabIcon(self.tab_index.get('ticket_note_form'), QtGui.QIcon(app_config.Icon.compose))
        self.tab_widget.setIconSize(QtCore.QSize(32, 32))

        self.layout.addLayout(self.top_layout)
        self.layout.addWidget(self.tab_widget)
        self.layout.addLayout(self.bottom_layout)

        self.layout.setStretch(0, 0)
        self.layout.setStretch(1, 1)
        self.layout.setStretch(2, 0)

        self.setLayout(self.layout)

        self.setup_widget()
        self.set_default()
        self.init_signals()
        self.init_functions()

    def setup_widget(self):
        # logo
        self.logo = QtWidgets.QLabel()
        self.logo.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))

        spacer_horizontal = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        spacer_vertical = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.tab_inbox_widget, self.tab_modules = tab_preset.setup_tabs(self)
        self.user_widget = user_widget.UserComboBox()
        # access each filter widget
        # self.tab_modules['note'].filters['project']

        self.top_layout.addWidget(self.user_widget, 0, 0, 1, 1)
        self.top_layout.addItem(spacer_horizontal, 0, 1, 1, 1)
        self.top_layout.addWidget(self.logo, 0, 3, 1, 1)

        self.filter_splitter = QtWidgets.QSplitter(QtCore.Qt.Vertical)
        self.filter_layout = QtWidgets.QVBoxLayout(self.filter_splitter)

        self.top_layout.setColumnStretch(0, 1)
        self.top_layout.setColumnStretch(1, 1)
        self.top_layout.setColumnStretch(2, 4)
        self.top_layout.setColumnStretch(3, 0)

        self.display_widget = inbox_widget.NoteList(self.splitter)
        self.display_widget.label.setText('Note List')

        self.content_widget = content_widget.NoteContent(self.splitter)
        # self.content_widget.label.setText('Details')

        self.form_widget = ticket_form.TicketUi(threadpool=self.threadpool)

        self.splitter.addWidget(self.tab_inbox_widget)
        self.splitter.addWidget(self.display_widget)
        self.splitter.addWidget(self.content_widget)

        self.splitter_outbox.addWidget(self.form_widget)

        self.splitter.setStretchFactor(0, 30)
        self.splitter.setStretchFactor(1, 40)
        self.splitter.setStretchFactor(2, 60)

    def init_signals(self):
        pass
        # self.project_widget.projectChanged.connect(self.project_changed)

    def init_functions(self):
        pass
        # this will trigger current project to list episode, sequence and shots
        # self.project_widget.emit_project_changed()

    def complete_filter(self, data):
        pass

    def project_changed(self, project): 
        self.entity_step_widget.set_project(project['name']) 
        self.entity_step_widget.entity_selected()

    def set_default(self): 
        pass
        # self.entity_step_widget.scene_radioButton.setChecked(True)
    

class RegFileWidget(QtWidgets.QWidget):
    """docstring for RegFileWidget"""
    def __init__(self, parent=None):
        super(RegFileWidget, self).__init__(parent=parent)
        self.layout = QtWidgets.QVBoxLayout()
        self.label = QtWidgets.QLabel()
        self.widget = register_file_widget.RegisterFileWidget()
        self.layout.addWidget(self.label)
        self.layout.addWidget(self.widget)
        self.setLayout(self.layout)
        self.layout.setContentsMargins(0, 0, 0, 0)

        self.layout.insertWidget(0, self.label)

    def add_file(self, display, data): 
        item = QtWidgets.QListWidgetItem()
        item.setText(display)
        item.setData(QtCore.Qt.UserRole, data)
        self.widget.addItem(item)
        
        
        


# splitter example
# from PySide2 import QtWidgets
# from PySide2 import QtCore
# widget = QtWidgets.QWidget()
# layout = QtWidgets.QHBoxLayout()
# splitter = QtWidgets.QSplitter(QtCore.Qt.Horizontal)
# layout.addWidget(splitter)
# splitter.addWidget(QtWidgets.QPushButton())
# splitter.addWidget(QtWidgets.QPushButton())
# widget.setLayout(layout)
# widget.show()


