#!/usr/bin/env python
# -- coding: utf-8 --
import sys 
import os 
import time
from datetime import datetime
from functools import partial
from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCore

from . import note_utils
from rf_utils.sg import sg_note
from rf_utils import thread_pool
from rf_app.note_manager import app_config
from rf_utils.widget import display_snap
from rf_utils.widget import snap_widget
# reload(app_config)
# reload(note_utils)

MODULE_PATH = sys.modules[__name__].__file__
MODULE_DIR  = os.path.dirname(MODULE_PATH)

class Icon:
    user = '{}/icons/user.png'.format(os.path.split(MODULE_DIR)[0])


class Color:
    date = 'color: rgb(100, 140, 200);'
    link = 'color: rgb(100, 200, 100);'
    green = 'color: rgb(40, 200, 40);'
    light_green_bg = 'background-color: rgb(60, 160, 60);'
    reply = 'background-color: rgb(80, 160, 240);'
    red = 'color: rgb(200, 70, 70);'
    progress = 'color: rgb(100, 200, 100);'
    version = 'color: rgb(220, 160, 100)'
    button = 'background-color: rgb(40, 40, 40)'
    status = 'color: rgb(40, 40, 40);'
    snap = 'background-color: rgb(200, 200, 200);'


class Config:
    status_color = {'opn': Color.status}
    button_progress = {'opn': 'ip', 'ip': 'cmp', 'wtg': 'ip'}
    button_progress_label = {
        'opn': 'Start fixing note', 
        'ip': 'Complete this note',  
        'wtg': 'Start working on ticket'}


class Cache:
    entity = dict()


class NoteContent(QtWidgets.QWidget):
    """docstring for NoteContent"""
    item_selected = QtCore.Signal(object)

    def __init__(self, parent=None):
        super(NoteContent, self).__init__(parent=parent)
        self.ui = None
        self.threadpool = None
        self.running_worker = False
        self.stop_thread = False
        self.delay_timer = 40
        self.setup_ui()
        self.init_signals()

    def setup_ui(self):
        self.layout = QtWidgets.QVBoxLayout()
        # decoration
        self.frame = QtWidgets.QFrame()
        self.frame.setFrameShape(QtWidgets.QFrame.Box)
        self.frame.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.frame_layout = QtWidgets.QVBoxLayout(self.frame)

        self.progress_label = QtWidgets.QLabel()
        self.progress_label.hide()
        # user area
        self.title_detail = TitleDetail()
        # main detail
        self.note_body = NoteBodyDisplay()
        # reply area
        self.reply = ReplyDisplay()
        # complete button 
        self.status_button = QtWidgets.QPushButton()
        self.status_button.hide()
        # submit reply
        self.submit = SubmitReply()
        # spacer

        self.splitter = QtWidgets.QSplitter(QtCore.Qt.Vertical)
        self.splitter_layout = QtWidgets.QVBoxLayout(self.splitter)
        self.splitter_layout.addWidget(self.note_body)
        self.splitter_layout.addWidget(self.reply)

        self.splitter.setStretchFactor(0, 0)
        self.splitter.setStretchFactor(1, 1)
        self.splitter.setStretchFactor(2, 2)

        self.frame_layout.addWidget(self.progress_label)
        self.frame_layout.addWidget(self.title_detail)
        self.frame_layout.addWidget(self.splitter)
        self.layout.addWidget(self.frame)
        self.layout.addWidget(self.submit)
        self.layout.addWidget(self.status_button)

        self.layout.setStretch(0, 1)
        self.layout.setStretch(1, 0)
        self.layout.setStretch(2, 0)

        self.setLayout(self.layout)
        self.layout.setContentsMargins(0, 0, 0, 0)

    def init_signals(self): 
        self.submit.reply_button.clicked.connect(self.reply_submit)
        self.submit.img_button.clicked.connect(self.add_attachments)

    def add_attachments(self): 
        self.submit.snap_widget.show()
        self.submit.snap_button.show()

    def reply_submit(self): 
        self.submit.reply_button.setEnabled(False)
        # get inputs 
        content = self.submit.reply_widget.text()
        user_entity = self.ui.user_widget.data()
        attachments = self.submit.snap_widget.get_images()

        # thread
        worker = thread_pool.Worker(sg_note.add_reply, self.note_entity, content, user_entity, attachments=attachments)
        worker.signals.result.connect(self.reply_complete)
        self.threadpool.start(worker)

    def reply_complete(self, reply_entity): 
        self.submit.reply_button.setEnabled(True)
        self.fetch_data(self.note_entity)

    def add_note(self, note_entity):
        self.clear()
        self.fetch_data(note_entity)
        self.set_progress(True, note_entity)
        self.note_entity = note_entity

        # self.title_detail.set_data(note_entity)
        # self.note_body.note.set_data(note_entity)
        # self.note_body.note.add_attachments([])
        # self.reply.add_reply([])

    def set_progress(self, status, note_entity=None): 
        display = 'Loading ...'
        if note_entity: 
            display = 'Loading {} {} ...'.format(note_entity['type'], note_entity['id'])

        if status: 
            font =QtGui.QFont()
            font.setPointSize(11)
            font.setBold(True)
            self.progress_label.show()
            self.progress_label.setFont(font)
            self.progress_label.setText(display)
            self.progress_label.setStyleSheet(Color.progress)
            self.title_detail.hide()

        else: 
            self.title_detail.show()
            self.progress_label.hide()

    def fetch_data(self, note_entity):
        # print 'start fetching ...'
        if self.threadpool.activeThreadCount():
            self.stop_thread = True
            if self.running_worker:
                # print 'too late to stop. Queue'
                pass

        worker = thread_pool.Worker(self.fetch_data_call, note_entity)
        worker.signals.result.connect(self.fetch_data_finished)
        self.threadpool.start(worker)

    def fetch_data_call(self, note_entity):
        """
        get following extra data
        user thumbnail
        attachment thumbnails
        reply thread
        """
        # call threads
        self.stop_thread = False
        for i in range(self.delay_timer):
            time.sleep(0.01)
            if self.stop_thread == True:
                return []

        self.running_worker = True
        print('fetching ...')

        note_entity = sg_note.add_entity_to_note(note_entity)
        print('note_entity', note_entity)
        self.attachment_dict = sg_note.get_file_attachments(note_entity)
        self.attachment_dict = note_utils.download_attachment_thumbnails(self.attachment_dict)

        # grouping by chronological order
        self.threads = sg_note.get_note_thread(note_entity, self.attachment_dict)

        # download user thumbnail
        self.user_dict = sg_note.get_user_info(self.threads)
        self.user_dict = note_utils.download_user_preview(self.user_dict)
        self.threads = note_utils.add_user_information(self.threads, self.user_dict)

        # arrange in format widget will use
        self.thread_order_list = note_utils.grouping_note_display(self.threads, self.attachment_dict)

        print('updating ui ...')

        # set note button
        self.set_button_status(note_entity)

        return self.thread_order_list

    def fetch_data_finished(self, data):
        self.running_worker = False
        if data:
            self.title_detail.set_data(data[0])
            self.set_progress(False)
            self.thread_order_list = data
            self.set_data(self.thread_order_list)
            print('Completed!!')

        else:
            print('no data')
        # if not data:
        #     print 'Stop complete'
        # print 'job finished. thread running {}'.format(self.threadpool.activeThreadCount())

    def set_data(self, thread_order_list):
        # self.title_detail.set_data(note_entity)
        # self.note_body.note.set_data(note_entity)
        # self.note_body.note.add_attachments([])

        self.title_detail.set_data(thread_order_list[0])
        self.note_body.add_note(thread_order_list[0])

        if len(thread_order_list) > 1:
            self.reply.add_reply(thread_order_list[1:])

    def clear(self):
        # self.title_detail.clear()
        self.note_body.clear()
        self.reply.clear()

    def set_button_status(self, note_entity): 
        status = note_utils.get_status(note_entity)
        next_status = Config.button_progress.get(status)
        display = Config.button_progress_label.get(status)

        if display: 
            self.status_button.show()
            self.status_button.setText(display)
            self.status_button.setStyleSheet(Color.light_green_bg)
        else: 
            self.status_button.hide()


class TitleDetail(QtWidgets.QWidget):
    """docstring for TitleDetail"""
    def __init__(self, parent=None):
        super(TitleDetail, self).__init__(parent=parent)
        self.setup_ui()

    def setup_ui(self):
        self.layout = QtWidgets.QVBoxLayout()
        # self.set_detail_ui(self.layout)
        self.grid_layout = None
        self.setLayout(self.layout)
        self.set_default()

    def clear_layout(self): 
        clear_layout(self.grid_layout)

    def set_detail_ui(self, layout): 
        # elements
        self.grid_layout = QtWidgets.QGridLayout()
        self.status = QtWidgets.QLabel('-')
        self.address_to = SGUserWidget()
        self.address_cc = SGUserWidget()
        self.entity_name = QtWidgets.QLabel('-')
        self.task_name = QtWidgets.QLabel('-')
        # self.due_date = QtWidgets.QLabel('-')

        font = QtGui.QFont()
        font.setPointSize(9)
        font.setBold(True)

        status_label = QtWidgets.QLabel('Status : ')
        to_label = QtWidgets.QLabel('To : ')
        cc_label = QtWidgets.QLabel('Cc : ')
        entity_label = QtWidgets.QLabel('Asset / Shot : ')
        task_label = QtWidgets.QLabel('Task name : ')
        due_date = QtWidgets.QLabel('Due date : ')

        status_label.setFont(font)
        to_label.setFont(font)
        cc_label.setFont(font)
        entity_label.setFont(font)
        task_label.setFont(font)
        due_date.setFont(font)
        # self.due_date.setFont(font)

        self.layout_list = [
                (to_label, self.address_to), 
                (cc_label, self.address_cc), 
                (entity_label, self.entity_name), 
                (task_label, self.task_name), 
                (status_label, self.status), 
                (due_date, None)
            ]

        for row, widgets in enumerate(self.layout_list): 
            for column, widget in enumerate(widgets): 
                if widget: 
                    self.grid_layout.addWidget(widget, row, column, 1, 1)

        # self.layout.addWidget(to_label, 1, 0, 1, 1)
        # self.layout.addWidget(cc_label, 2, 0, 1, 1)
        # self.layout.addWidget(entity_label, 3, 0, 1, 1)
        # self.layout.addWidget(task_label, 4, 0, 1, 1)
        # self.layout.addWidget(status_label, 5, 0, 1, 1)

        # self.layout.addWidget(self.address_to, 1, 1, 1, 1)
        # self.layout.addWidget(self.address_cc, 2, 1, 1, 1)
        # self.layout.addWidget(self.entity_name, 3, 1, 1, 1)
        # self.layout.addWidget(self.task_name, 4, 1, 1, 1)
        # self.layout.addWidget(self.status, 5, 1, 1, 1)

        self.grid_layout.setColumnStretch(0, 1)
        self.grid_layout.setColumnStretch(1, 3)

        self.grid_layout.setContentsMargins(0, 0, 0, 0)
        layout.addLayout(self.grid_layout)
        return self.grid_layout

    def clear_layout(self): 
        if self.grid_layout: 
            clear_layout(self.grid_layout)


    def set_data(self, note_entity):
        self.clear_layout()
        self.set_detail_ui(self.layout)
        self.note_entity = note_entity
        self.set_status(note_entity)
        self.set_to(note_entity)
        self.set_cc(note_entity)
        self.set_entity(note_entity)
        self.set_tasks(note_entity)
        self.set_due_date(note_entity)

    def set_type(self, note_entity):
        display = '-'
        if note_entity:
            display = note_utils.get_type(note_entity)
        self.entity_type.setText(display)

    def set_due_date(self, note_entity): 
        today = datetime.today()
        strf = '%Y-%b-%d'
        due_date_dict = note_utils.get_due_date(note_entity)
        display_list = list()

        i = 0
        for name, due_date in due_date_dict.items(): 
            label = QtWidgets.QLabel()
            color = Color.red
            font = QtGui.QFont()
            font.setBold(True)
            label.setFont(font)
            item = '{} - No set'.format(name)

            if due_date:
                due_date_object = datetime.strptime(due_date, "%Y-%m-%d")
                due_date = due_date_object.strftime(strf)
                days = (due_date_object - today).days

                if days > 0: 
                    color = Color.green

                item = '{} - {} [{} days]'.format(name, due_date, days)
            
            label.setText(item)
            label.setStyleSheet(Color.red)
            display_list.append(item)
            row = len(self.layout_list) - 1
            self.grid_layout.addWidget(label, row + i, 1, 1, 1)

            i += 1

        # display = '\n'.join(display_list)
        # self.due_date.setText(display)



    def set_default(self):
        pass

    def set_status(self, note_entity):
        display = '-'
        if note_entity:
            display = note_utils.get_status(note_entity)
        self.status.setText(display)
        # color = '%s' % (Color.status)
        # self.status.setText(status)
        # self.status.setStyleSheet(color)

    def set_to(self, note_entity):
        self.address_to.clear()
        if note_entity:
            users = note_utils.get_to(note_entity)
            self.address_to.add_users(users)

    def set_cc(self, note_entity):
        self.address_cc.clear()
        if note_entity:
            users = note_utils.get_cc(note_entity)
            self.address_cc.add_users(users)

    def set_entity(self, note_entity):
        display = '-'
        if note_entity:
            display = note_utils.get_entity(note_entity).get('name')
        self.entity_name.setText(display)

    def set_tasks(self, note_entity):
        display = '-'
        if note_entity:
            tasks = note_utils.get_tasks(note_entity)
            if tasks:
                display = ','.join(tasks)

            if len(display) > 20:
                display = '{} ...'.format(display[0: 21])

        self.task_name.setText(display)

    def clear(self):
        self.set_status(None)
        self.set_to(None)
        self.set_cc(None)
        self.set_entity(None)
        self.set_tasks(None)

    def loading(self):
        self.entity_type.setText('Loading ...')


class UserDisplay(QtWidgets.QWidget):
    """docstring for UserDisplay"""
    def __init__(self, parent=None):
        super(UserDisplay, self).__init__(parent=parent)
        self.layout = QtWidgets.QVBoxLayout()
        self.user = self.user_widget()
        self.user_name = QtWidgets.QLabel('User')
        self.layout.addWidget(self.user)
        self.layout.addWidget(self.user_name)
        self.setLayout(self.layout)

    def user_widget(self):
        self.user = QtWidgets.QLabel()
        self.user.setMaximumSize(QtCore.QSize(60, 60))
        self.user.setMinimumSize(QtCore.QSize(60, 60))
        self.user.setPixmap(QtGui.QPixmap(Icon.user).scaled(60, 60, QtCore.Qt.KeepAspectRatio))
        return self.user

    def set_user(self, user_entity):
        path = user_entity.get('preview_path') or Icon.user
        self.user_name.setText(user_entity['name'])
        self.user.setPixmap(QtGui.QPixmap(path).scaled(60, 60, QtCore.Qt.KeepAspectRatio))

    def clear(self):
        self.user_name.setText('')
        self.user.setPixmap(QtGui.QPixmap(Icon.user).scaled(60, 60, QtCore.Qt.KeepAspectRatio))


class NoteBody(QtWidgets.QWidget):
    """docstring for NoteBody"""
    def __init__(self, parent=None):
        super(NoteBody, self).__init__(parent=parent)
        self.threadpool = None
        self.setup_ui()

    def setup_ui(self):
        self.layout = QtWidgets.QHBoxLayout()
        self.widget = QtWidgets.QWidget()

        self.left_layout = QtWidgets.QVBoxLayout()
        self.user_widget = UserDisplay()
        self.user_spacer = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.left_layout.addWidget(self.user_widget)
        self.left_layout.addItem(self.user_spacer)
        self.left_layout.setStretch(0, 0)
        self.left_layout.setStretch(1, 1)

        self.right_layout = QtWidgets.QVBoxLayout()
        self.attachment_grid = QtWidgets.QHBoxLayout()
        self.text_widget = QtWidgets.QPlainTextEdit()
        self.text_widget.setMaximumSize(QtCore.QSize(400, 100))
        self.text_widget.setReadOnly(True)
        self.right_layout.addWidget(self.text_widget)
        self.right_layout.addLayout(self.attachment_grid)

        self.right_layout.setStretch(0, 0)
        self.right_layout.setStretch(1, 1)

        self.layout.addLayout(self.left_layout)
        self.layout.addLayout(self.right_layout)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setStretch(0, 0)
        self.layout.setStretch(1, 1)
        self.setLayout(self.layout)

    def add_attachments(self, attachments):
        clear_layout(self.attachment_grid)
        for i, entity in enumerate(attachments):
            path = entity['preview_path']
            button = QtWidgets.QPushButton()
            button.setMinimumSize(QtCore.QSize(50, 50))
            button.setMaximumSize(QtCore.QSize(50, 50))
            button.setIcon(QtGui.QIcon(path))
            button.setIconSize(QtCore.QSize(100, 100))
            button.clicked.connect(partial(self.attachment_clicked, entity))
            # button.setFlat(True)
            self.attachment_grid.addWidget(button)

        spacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.attachment_grid.addItem(spacer)

    def attachment_clicked(self, entity):
        local_dst_file = note_utils.get_temp_dir_path(id=entity['id'], ext='.jpg', entity_type=entity['type'], filetype='hires')
        if not os.path.exists(local_dst_file):
            self.download_attachment(entity, local_dst_file)
        else:
            self.download_finished(local_dst_file)

    def download_attachment(self, entity, dst):
        worker = thread_pool.Worker(sg_note.download_attachment, entity, dst)
        worker.signals.result.connect(self.download_finished)
        self.threadpool.start(worker)

    def download_finished(self, dst):
        if os.path.exists(dst):
            os.startfile(dst)

        else:
            print('failed to download')

    def set_data(self, note_entity):
        """ start function """
        # basic data - user, description, attachments
        self.set_username(note_entity)
        self.set_description(note_entity)

        attachments = note_entity['attachments']
        self.add_attachments(attachments)

    def set_username(self, note_entity):
        user = note_utils.get_sender(note_entity)
        self.user_widget.set_user(user)

    def set_description(self, note_entity):
        description = note_utils.get_content(note_entity)
        self.text_widget.setPlainText(description)

    def clear(self):
        clear_layout(self.attachment_grid)
        self.user_widget.clear()
        self.text_widget.setPlainText('')


class NoteBodyDisplay(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(NoteBodyDisplay, self).__init__(parent=parent)
        self.threadpool = None
        self.setup_ui()

    def setup_ui(self):
        self.layout = QtWidgets.QVBoxLayout()
        self.widget = QtWidgets.QWidget()
        self.scroll_layout = QtWidgets.QVBoxLayout(self.widget)
        self.scroll = QtWidgets.QScrollArea()
        self.scroll.setWidgetResizable(True)
        self.scroll.setWidget(self.widget)
        # self.scroll.setFixedHeight(160)
        self.scroll.setMinimumSize(QtCore.QSize(100, 200))
        self.scroll.setFrameShape(QtWidgets.QFrame.Box)
        self.scroll.setFrameShadow(QtWidgets.QFrame.Sunken)

        self.layout.addWidget(self.scroll)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

    def clear(self):
        clear_layout(self.scroll_layout)

    def add_note(self, note_entity):
        self.clear()
        self.note = NoteBody()
        self.note.threadpool = self.threadpool
        self.scroll_layout.addWidget(self.note)
        self.note.set_data(note_entity)


class ReplyDisplay(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(ReplyDisplay, self).__init__(parent=parent)
        self.threadpool = None
        self.setup_ui()

    def setup_ui(self):
        self.layout = QtWidgets.QVBoxLayout()
        self.widget = QtWidgets.QWidget()
        self.scroll_layout = QtWidgets.QVBoxLayout(self.widget)
        self.scroll = QtWidgets.QScrollArea()
        self.scroll.setWidgetResizable(True)
        self.scroll.setWidget(self.widget)
        # self.scroll.setFixedHeight(200)
        self.scroll.setFrameShape(QtWidgets.QFrame.Box)
        self.scroll.setFrameShadow(QtWidgets.QFrame.Sunken)

        self.layout.addWidget(self.scroll)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

    def add_reply(self, replies):
        self.clear()
        for note_entity in replies:
            reply = NoteBody()
            reply.threadpool = self.threadpool
            reply.set_data(note_entity)
            self.scroll_layout.addWidget(reply)
            self.scroll_layout.addWidget(add_line())

    def clear(self):
        clear_layout(self.scroll_layout)


class SubmitReply(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(SubmitReply, self).__init__(parent=parent)
        self.setup_ui()
        self.init_signals()

    def setup_ui(self): 
        self.layout = QtWidgets.QVBoxLayout()

        self.attachment_layout = QtWidgets.QHBoxLayout()
        self.reply_layout = QtWidgets.QHBoxLayout()

        # attachment section 
        self.snap_widget = display_snap.DisplaySnap()
        self.snap_widget.setMaximumSize(QtCore.QSize(2000, 40))
        self.snap_widget.hide()

        self.snap_button = snap_widget.SnapButton()
        self.snap_button.hide()
        self.snap_button.button.setIcon(QtGui.QPixmap(app_config.Icon.snap))
        self.snap_button.button.setIconSize(QtCore.QSize(32, 32))
        self.snap_button.setMaximumSize(QtCore.QSize(64, 64))
        self.snap_button.button.setStyleSheet(Color.reply)

        self.attachment_layout.addWidget(self.snap_widget)
        self.attachment_layout.addWidget(self.snap_button)

        self.attachment_layout.setStretch(0, 1)
        self.attachment_layout.setStretch(1, 0)

        # reply section 
        self.reply_widget = QtWidgets.QLineEdit()
        self.img_button = create_button_image(app_config.Icon.attachment, border=False)

        self.reply_button = QtWidgets.QPushButton('Reply')
        self.reply_button.setMinimumSize(QtCore.QSize(48, 20))
        self.reply_button.setStyleSheet(Color.reply)

        self.layout.addLayout(self.attachment_layout)
        self.layout.addLayout(self.reply_layout)

        self.reply_layout.addWidget(self.reply_widget)
        self.reply_layout.addWidget(self.img_button)
        self.reply_layout.addWidget(self.reply_button)
        self.reply_layout.setContentsMargins(0, 0, 0, 0)
        self.reply_layout.setStretch(0, 3)
        self.reply_layout.setStretch(1, 0)
        self.reply_layout.setStretch(2, 0)

        self.layout.setContentsMargins(0, 0, 0, 0)
        # self.layout.setSpacing(0)

        self.setLayout(self.layout)

    def init_signals(self): 
        self.snap_button.captured.connect(self.snap_widget.add_item)



class SGUserWidget(QtWidgets.QWidget):
    """docstring for SGUserWidget"""
    def __init__(self, parent=None):
        super(SGUserWidget, self).__init__(parent=parent)
        self.name_list = list()
        self.setup_ui()

    def setup_ui(self): 
        self.layout = QtWidgets.QHBoxLayout()
        self.name_layout = QtWidgets.QHBoxLayout()
        spacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.name_layout.addItem(spacer)
        
        self.edit_button = create_button_image(app_config.Icon.pencil, size=[16, 16], border=False)

        self.layout.addLayout(self.name_layout)
        self.layout.addWidget(self.edit_button)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.name_layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

    def add_users(self, users, display_key='name'): 
        for user in users: 
            self.add_user(user, display_key=display_key)
        spacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.name_layout.addItem(spacer)
        self.name_layout.setStretch(self.name_layout.count(), 1)

    def add_user(self, user_entity, display_key='name'): 
        font = QtGui.QFont()
        font.setBold(True)
        label = QtWidgets.QLabel(user_entity[display_key])
        label.setFont(font)

        index = self.layout.count() - 1
        self.name_layout.insertWidget(index, label)
        self.name_list.append(label)

    def clear(self): 
        clear_layout(self.name_layout)


def create_button_image(path, size=[16, 16], border=True): 
    button = QtWidgets.QPushButton()
    button.setMinimumSize(QtCore.QSize(size[0], size[1]))
    button.setMaximumSize(QtCore.QSize(size[0], size[1]))
    button.setIcon(QtGui.QIcon(path))
    button.setIconSize(QtCore.QSize(size[0], size[1]))
    button.setFlat(not border)
    return button

def clear_layout(layout):
    if layout is not None:
        while layout.count():
            child = layout.takeAt(0)
            if child.widget() is not None:
                child.widget().deleteLater()
            elif child.layout() is not None:
                clear_layout(child.layout())
        

def add_line(parent=None):
    line = QtWidgets.QFrame(parent)
    line.setFrameShape(QtWidgets.QFrame.HLine)
    line.setFrameShadow(QtWidgets.QFrame.Sunken)
    return line
