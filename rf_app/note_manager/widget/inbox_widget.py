#!/usr/bin/env python
# -- coding: utf-8 --
from Qt import QtWidgets
from Qt import QtCore
from Qt import QtGui
from functools import partial
from . import stylesheet


class Color:
    ticket = 'background-color: rgb(160, 70, 70)'
    note = 'background-color: rgb(60, 100, 160)'
    date = 'color: rgb(100, 140, 200);'
    link = 'color: rgb(100, 200, 100);'
    version = 'color: rgb(220, 160, 100)'
    button = 'background-color: rgb(40, 40, 40)'
    status = 'color: rgb(40, 40, 40);'


class NoteList(QtWidgets.QWidget):
    """docstring for NoteList"""
    note_selected = QtCore.Signal(object)

    def __init__(self, parent=None):
        super(NoteList, self).__init__(parent=parent)
        self.setup_ui()
        self.limit_display = 50
        self.context_menu_display = {'text': None}
        self.root_item = self.tree_widget.invisibleRootItem()
        self.init_signals()

    def setup_ui(self): 
        self.layout = QtWidgets.QVBoxLayout()
        self.tree_widget = QtWidgets.QTreeWidget()
        self.tree_widget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.header_layout = QtWidgets.QHBoxLayout()
        self.label = QtWidgets.QLabel()
        self.search_lineEdit = QtWidgets.QLineEdit()
        self.search_lineEdit.setMaximumSize(QtCore.QSize(2000, 20))

        self.bottom_layout = QtWidgets.QHBoxLayout()
        self.sort_label = QtWidgets.QLabel('Sorted order: ')
        self.sort_combobox = QtWidgets.QComboBox()
        self.page_label = QtWidgets.QLabel('Show: ')
        self.page_combobox = QtWidgets.QComboBox()
        self.spacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.bottom_layout.addWidget(self.sort_label)
        self.bottom_layout.addWidget(self.sort_combobox)
        self.bottom_layout.addItem(self.spacer)
        self.bottom_layout.addWidget(self.page_label)
        self.bottom_layout.addWidget(self.page_combobox)

        self.header_layout.addWidget(self.label)
        self.header_layout.addWidget(self.search_lineEdit)
        self.layout.addLayout(self.header_layout)
        self.layout.addWidget(self.tree_widget)
        self.layout.addLayout(self.bottom_layout)
        self.header_layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)
        self.layout.setContentsMargins(0, 0, 0, 0)

    def init_signals(self): 
        self.search_lineEdit.textChanged.connect(self.search)
        self.page_combobox.currentIndexChanged.connect(self.item_page_signal)
        self.tree_widget.currentItemChanged.connect(self.item_selected)
        self.tree_widget.customContextMenuRequested.connect(self.show_context_menu)

    def item_selected(self, item):
        self.note_selected.emit(item.data(0, QtCore.Qt.UserRole))

    def show_context_menu(self, pos): 
        menu = QtWidgets.QMenu(self)
        item = self.tree_widget.currentItem()
        data = item.data(0, QtCore.Qt.UserRole)

        # create_menu 
        for text, func in self.context_menu_display.items(): 
            create_note = menu.addAction(text)
            if func: 
                create_note.triggered.connect(partial(func, data))
        menu.popup(self.tree_widget.mapToGlobal(pos))


    def search(self): 
        key = self.search_lineEdit.text()
        items = self.tree_widget.all_items()

        for item in items: 
            if key: 
                hidden = True 
                if key in item.text(): 
                    hidden = False
            else: 
                hidden = False
            item.setHidden(hidden)

    def clear(self): 
        self.tree_widget.blockSignals(True)
        self.tree_widget.clear()
        self.tree_widget.blockSignals(False)

    def add_items(self, entities): 
        """ 
        Args: 
            entities (list): {'entity': note_entity, 'child': []}
        """ 
        for entity in entities: 
            content = entity.get('note', dict()).get('content') 
            if content: 
                item = QtWidgets.QTreeWidgetItem(self.root_item)
                note_widget = NoteWidget(entity, self.tree_widget)
                self.tree_widget.setItemWidget(item, 0, note_widget)
                QtWidgets.QApplication.processEvents()

    def add_item_pages(self, entities, root): 
        
        if len(entities) >= self.limit_display: 
            pages = len(entities) / self.limit_display
            fractal_page = len(entities) % self.limit_display

        else: 
            pages = 1 
            fractal_page = 0

        with PauseSignals([self.page_combobox]): 
            self.page_combobox.clear()

            for i in range(pages + 1): 
                start = (self.limit_display * i) + 1
                end = (start + self.limit_display - 1)
                end = len(entities) if end > len(entities) else end
                display = '{}-{}'.format(start, end)
                self.page_combobox.addItem(display)
                data = {'entities': entities[(self.limit_display * i):  (start + self.limit_display)], 'root': root}
                self.page_combobox.setItemData(i, data, QtCore.Qt.UserRole)

        self.item_page_signal(0)

    def item_page_signal(self, row): 
        data = self.page_combobox.itemData(row, QtCore.Qt.UserRole)
        self.tree_widget.clear()
        self.add_tree_items(data['entities'], data['root'])

    def add_tree_items(self, entities, root): 
        """ 
        Args: 
            entities (list): {'note': note_entity, 'child': []}
        """ 
        root = self.root_item if not root else root
        for entity in entities:
            # if entity['id'] == 822: 
            #     print 'found %s' %  entity
            content = get_content(entity) or ''
            childs = entity.get('children', list())
            item = QtWidgets.QTreeWidgetItem(root)
            note_widget = NoteWidget(entity, self.tree_widget)
            self.tree_widget.setItemWidget(item, 0, note_widget)
            item.setData(0, QtCore.Qt.UserRole, entity)

            if childs: 
                self.add_tree_items(childs, root=item)

        self.tree_widget.expandAll()
                    

class NoteWidget(QtWidgets.QWidget): 
    def __init__(self, entity, parent=None): 
        super(NoteWidget, self).__init__(parent=parent)
        self.setup_ui()
        self.set_data(entity)
        self.setStyleSheet(stylesheet.StyleConfig.listwidget_item_style)

    def setup_ui_debug(self): 
        self.layout = QtWidgets.QGridLayout()
        self.update_at = QtWidgets.QLabel()
        self.subject = QtWidgets.QLabel()
        self.content = QtWidgets.QLabel()
        self.sender = QtWidgets.QLabel()
        self.receivers = QtWidgets.QLabel()
        self.status = QtWidgets.QLabel()
        self.project = QtWidgets.QLabel()
        self.spacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.layout.addWidget(self.update_at, 0, 0, 1, 1)
        self.layout.addWidget(self.subject, 1, 0, 1, 1)
        self.layout.addWidget(self.content, 2, 0, 1, 1)
        self.layout.addWidget(self.sender, 0, 1, 1, 1)
        self.layout.addWidget(self.receivers, 1, 1, 1, 1)
        self.layout.addWidget(self.status, 2, 1, 1, 1)
        self.layout.addWidget(self.project, 3, 0, 1, 1)
        self.layout.addItem(self.spacer, 3, 1, 1, 2)

        self.setLayout(self.layout)
        self.layout.setContentsMargins(0, 0, 0, 0)

        content = get_content(entity)
        subject = get_title(entity)
        update_at = entity.get('global_updated_at')

        sender = get_sender(entity).get('name')
        receivers = [a.get('name') for a in get_receivers(entity)]
        status = entity.get('sg_status_list')
        project = entity.get('project').get('name')

        self.update_at.setText('{} - {}'.format(entity.get('type'), entity.get('id')))
        self.subject.setText(subject)
        self.content.setText(content)
        self.sender.setText(sender)
        self.receivers.setText(str(receivers))
        self.content.setText(content)
        self.status.setText(status)
        self.project.setText(project)

    def setup_ui(self): 
        self.layout = QtWidgets.QGridLayout()
        self.entity_type = QtWidgets.QLabel()
        self.update_at = QtWidgets.QLabel()
        self.content = QtWidgets.QLabel()
        self.status = QtWidgets.QLabel()
        self.project = QtWidgets.QLabel()
        self.task = QtWidgets.QLabel()
        self.spacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.layout.addWidget(self.entity_type, 0, 0, 1, 1)
        self.layout.addWidget(self.status, 0, 1, 1, 1)
        self.layout.addWidget(self.update_at, 0, 2, 1, 1)
        self.layout.addWidget(self.project, 1, 0, 1, 1)
        self.layout.addWidget(self.task, 1, 1, 1, 1)
        self.layout.addWidget(self.content, 2, 0, 1, 3)
        self.layout.addItem(self.spacer, 3, 1, 1, 2)

        self.layout.setColumnStretch(0, 2)
        self.layout.setColumnStretch(1, 3)
        self.layout.setColumnStretch(2, 3)

        self.setLayout(self.layout)
        self.layout.setContentsMargins(0, 0, 0, 0)

    def set_data(self, entity): 
        subject = get_title(entity)
        update_at = entity.get('updated_at')

        sender = get_sender(entity).get('name')
        receivers = [a.get('name') for a in get_receivers(entity)]
        status = entity.get('sg_status_list')
        project = entity.get('project').get('name')

        font = QtGui.QFont()
        font.setPointSize(9)
        font.setBold(True)

        self.set_entity_type(entity)
        self.update_at.setText(update_at.strftime('%Y-%b-%d %H:%M:%S'))
        self.set_content(entity)
        self.status.setText(status)
        self.project.setText(project)
        self.set_task(entity)

        # bold 
        self.project.setFont(font)
        self.update_at.setFont(font)

    def set_entity_type(self, entity): 
        font = QtGui.QFont()
        font.setPointSize(9)
        font.setBold(True)
        entity_type = entity['type']

        if entity_type == 'Note': 
            color = '%s' % (Color.note)

        if entity_type == 'Ticket': 
            color = '%s' % (Color.ticket)

        display = '{} - {}'.format(entity_type, entity['id'])
        self.entity_type.setText(display)
        self.entity_type.setStyleSheet(color)
        self.entity_type.setFont(font)

    def set_task(self, entity): 
        if entity['type'] == 'Note': 
            tasks = entity['tasks']
            task = '-'
            if tasks: 
                task = ', '.join([a['name'] for a in tasks])
            self.task.setText(task)

    def set_content(self, entity): 
        limit_preview = 150
        content = get_content(entity)
        if len(content) > limit_preview: 
            content = '{}...'.format(content.encode('utf-8')[0: limit_preview])
        self.content.setText(content)


class PauseSignals(object): 
    def __init__(self, widgets): 
        self.widgets = widgets

    def __enter__(self, *args): 
        for widget in self.widgets: 
            widget.blockSignals(True)

    def __exit__(self, *args): 
        for widget in self.widgets: 
            widget.blockSignals(False)


def get_title(entity):
    if entity['type'] == 'Ticket':
        text = entity['title']
    if entity['type'] == 'Note':
        text = entity['subject']
    return text


def get_content(entity):
    content = 'No content'
    if entity['type'] == 'Ticket':
        text = entity.get('description') or content
    if entity['type'] == 'Note':
        text = entity.get('content') or content
    # text.decode(encoding='UTF-8', errors='strict')
    return text


def get_sender(entity):
    if entity['type'] == 'Ticket':
        text = entity['created_by']
    if entity['type'] == 'Note':
        text = entity['user']
    return text


def get_receivers(entity):
    if entity['type'] == 'Ticket':
        return entity['addressings_to'] + entity['addressings_cc']

    if entity['type'] == 'Note':
        return entity['addressings_to'] + entity['addressings_cc']
""" {'note': 
    {
        'tasks': [{'template_task_id': 39282, 'type': 'Task', 'id': 283373, 'name': 'anim'}], 
        'note_links': [{'type': 'Shot', 'id': 9828, 'name': 'hnm_act1_q0030a_s0010'}], 
        'attachments': [], 
        'user.HumanUser.image': 'https://sg-media-tokyo.s3-accelerate.amazonaws.com', 
        'created_at': datetime.datetime(2020, 8, 25, 20, 20, 48, ), 
        'addressings_to': [], 
        'updated_at': datetime.datetime(2020, 11, 6, 16, 28, 29, ), 
        'content': '', 
        'sg_note_type': 'Fix', 
        'user': {'type': 'HumanUser', 'id': 263, 'name': 'Banmu'}, 
        'sg_modified_by': None, 
        'sg_status_list': 'clsd', 
        'sg_brief': None, 
        'type': 'Note', 
        'id': 14144, 
        'subject': "Riff's Note on hnm_act1_q0030a_s0010"
    }, 
    'status': 'clsd', 
    'sender': {'type': 'HumanUser', 'id': 263, 'name': 'Banmu'}, 
    'receivers': [{'type': 'HumanUser', 'id': 548, 'name': 'To'}]

    }
    """
