import sys 
import os 
from collections import OrderedDict
from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCore

from rf_utils.widget import project_widget
from rf_utils.widget import user_widget
from rf_utils.widget import entity_step_widget
from rf_app.note_manager.widget import note_filter
from rf_app.note_manager import app_config
# reload(app_config)
# reload(note_filter)

MODULE_PATH = sys.modules[__name__].__file__
MODULE_DIR  = os.path.dirname(MODULE_PATH)


class Setting: 
    filters = {
        'note': ['project', 'task', 'status'],
        'team': ['project', 'users', 'status'],
        'ticket': ['project', 'task', 'users', 'status']
        }

    filter_config = {'status': app_config.StatusConfig.setting}

    filter_map = {
            'project': '$', # project
            'sender': '#', # sender
            'users': '@', # receiver
            'step': '!', # step
            'status': '&', # status
            'task': '*'} # task


class TabSetting: 
    """ name of tabs module """ 
    activeTab = 'note'
    allowedTabs = ['note', 'team', 'ticket']
    tabNameMap = {'note': 'My Note', 'team': 'My Team', 'ticket': 'My Ticket'}


def setup_tabs(self): 
    self.tabWidget = QtWidgets.QTabWidget()
    modules = TabSetting.allowedTabs
    activeIndex = 0 
    self.tabModules = dict()
    tabNames = []

    for tabName in TabSetting.allowedTabs: 
        for index, name in enumerate(modules): 
            title = TabSetting.tabNameMap.get(name) or name.capitalize()
            if name == tabName: 
                widget = Ui(name=name, parent=self)
                self.tabWidget.addTab(widget, title)

                if name == TabSetting.activeTab: 
                    activeIndex = len(self.tabModules.keys())
                
                self.tabModules[name] = widget
                widget.last_filter_selected.connect(self.complete_filter)

    # active tab 
    self.tabWidget.setCurrentIndex(activeIndex)
    return self.tabWidget, self.tabModules


class Ui(QtWidgets.QWidget):
    """docstring for Tabz"""
    last_filter_selected = QtCore.Signal(object)

    def __init__(self, name, parent=None):
        super(Ui, self).__init__(parent)
        self.name = name
        self.layout = QtWidgets.QVBoxLayout()
        # self.entity_step_widget = entity_step_widget.EntityStepWidget2(sg=sg)
        self.submit_button = QtWidgets.QPushButton('Get Notes')
        self.submit_button.setMinimumSize(QtCore.QSize(120, 40))

        self.filter_splitter = QtWidgets.QSplitter(QtCore.Qt.Vertical)
        self.filter_layout = QtWidgets.QVBoxLayout(self.filter_splitter)
        self.filters = OrderedDict()
        self.widgets = list()

        # loop each filter to generate widget
        for filter_name in Setting.filters[name]: 
            config = Setting.filter_config.get(filter_name)
            filter_widget = note_filter.FilterWidget(name=filter_name, config=config)
            filter_widget.prefix = Setting.filter_map[filter_name]

            self.filter_splitter.addWidget(filter_widget)
            self.filters[filter_name] = filter_widget
            self.widgets.append(filter_widget)
            exec('self.{}_{}_filter = filter_widget'.format(self.name, filter_name))

        # connect widget signal together 
        for i, widget in enumerate(self.widgets[:-1]): 
            next_widget = self.widgets[i+1]
            widget.filtered_data.connect(next_widget.add_input)

        # last widget emit signal to this widget 
        self.widgets[-1].filtered_data.connect(self.filter_complete)

        self.layout.addWidget(self.submit_button)
        self.layout.addWidget(self.filter_splitter)
        self.setLayout(self.layout)

    def input_data(self, note_list): 
        self.note_list = note_list

        # fill only first filter
        widget = self.widgets[0]
        widget.add_input(note_list)
        
    def filter_complete(self, input_data): 
        self.last_filter_selected.emit(input_data)
