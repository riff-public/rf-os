# -*- coding: utf-8 -*-
import os
import sys
from rf_utils.sg import sg_process
from rf_utils import user_info
try:
    from urllib import urlopen, urlretrieve
except ImportError:
    from urllib.request import urlopen, urlretrieve

import urllib
from datetime import datetime
from collections import OrderedDict
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


def extract_data(note_list, prefix, separator):
    filters = list()
    keyword_dict = dict()

    for note in note_list:
        value = note.get('keyword')
        if value:
            for v in value.split(separator):
                if v.startswith(prefix):
                    keyword = v[1:]

                    if not keyword in keyword_dict.keys():
                        keyword_dict[keyword] = 1
                    else:
                        keyword_dict[keyword] += 1

    return keyword_dict


def create_tree_list(note_list, extra_list): 
    """ 
    repack entity format to dict 
    add {global_updated_at: datetime, children: [], parents: []}
    tree = {12: [{1: []}, {2: []}], 13: []}
    """ 

    def find_children(tree_dict, children, parent=None): 
        if children: 
            parent_id = get_id(parent) if parent else None
            for entity in children: 
                grand_children = get_children(entity)
                grand_children = [note_dict.get(get_id(a)) for a in grand_children]
                id = get_id(entity)
                entity_dict = dict()
                entity_dict[id] = dict()
                tree_dict.update(entity_dict)

                if grand_children: 
                    entity_dict = find_children(tree_dict[id], grand_children, entity)
                    tree_dict.update(entity_dict)

        return tree_dict

    note_dict = dict()
    tree_dict = dict()
    extra_dict = dict()

    for item in (note_list + extra_list): 
        key = get_id(item)
        note_dict[key] = item

    for item in extra_list:
        key = get_id(item)
        extra_dict[key] = item

    tree_dict = find_children(tree_dict, note_list)

    # remove extra dict key
    for k, v in tree_dict.copy().items():
        if k in [get_id(a) for a in extra_list]:
            tree_dict.pop(k)

    return tree_dict, note_dict


def sort_group_by_date(note_list, extra_list): 
    def find_all_decendant(children_list, v_dict): 
        if v_dict: 
            for k, v in v_dict.items(): 
                children_list.append(k)
                children_list = find_all_decendant(children_list, v)
        return children_list

    order_tree = OrderedDict() # order by latest date
    date_list = OrderedDict()
    tmp = dict()
    tree_dict, note_dict = create_tree_list(note_list, extra_list)

    for k, v in tree_dict.items(): 
        children_list = [k]
        children_list = find_all_decendant(children_list, v)

        all_date = sorted([note_dict[a]['updated_at'] for a in children_list])
        latest = all_date[-1]
        key = '{}-{}'.format(str(latest), k)
        date_list[key] = k
        note_dict[k].update({'global_updated_at': latest})

    for k in sorted(date_list.keys())[::-1]: 
        id = date_list[k]
        order_tree[id] = tree_dict[id]

    # for k, v in order_tree.items(): 
    #     print k, v, str(note_dict[k]['updated_at']), str(note_dict[k]['global_updated_at'])

    # add data into tree
    return order_tree, note_dict


def reformat_to_list(order_tree, note_dict): 
    """ 
    data = [item1, item2]
    """ 
    def set_all_decendant(children_list, v_dict): 
        if v_dict: 
            for k, v in v_dict.items(): 
                # print 'k', k, len(child)
                children = list()
                entity = note_dict[k]
                children = set_all_decendant(children, v)
                entity.update({'children': children})
                children_list.append(entity)

        return children_list

    order_tree_list = list()
    order_tree_list = set_all_decendant(order_tree_list, order_tree)

    # for k, v_dict in order_tree.items(): 
    #     entity = note_dict[k]
    #     children = list()
    #     children = set_all_decendant(children, v_dict)
    #     entity.update('children': children)
    #     order_tree_list.append(entity)

    # for each in order_tree_list:
    #     print each['id'], len(each['children'])

    return order_tree_list


def grouping_note_display(threads, attachment_dict):
    """ re grouping to this format
    [user - description - attachment]
    """
    note_list = list()
    active_user = None
    note_dict = dict()

    # for a in threads:
    #     if a['type'] in ['Note', 'Reply']:
    #         print '\ngrouping_note_display {} {}'.format(a['type'], a['content'])

    for i, entity in enumerate(threads):
        if entity['type'] == 'Note':
            user =  entity['created_by']
            entity['attachments'] = list()
            note_dict = dict(entity)
            note_list.append(note_dict)
            active_user = user['id']

        if entity['type'] == 'Reply':
            entity['attachments'] = list()
            note_dict = dict(entity)
            note_list.append(note_dict)
            note_dict['user'] = entity['user']
            note_dict['content'] = get_content(entity)
            active_user = user['id']

        if entity['type'] == 'Ticket':
            user =  entity['created_by']
            entity['attachments'] = list()
            note_dict = dict(entity)
            note_list.append(note_dict)
            active_user = user['id']

        if entity['type'] == 'Attachment':
            user = entity['created_by']
            if user['id'] == active_user:
                if 'attachments' in note_dict.keys():
                    note_dict['attachments'].append(attachment_dict[entity['id']])
                else:
                    note_dict['attachments'] = [attachment_dict[entity['id']]]
                # if entity['id'] == 479179:
                #     print note_dict, attachment_dict[entity['id']]
            else:
                note_list.append(note_dict)
                note_dict = dict(entity)
                note_list.append(note_dict)
                note_dict['user'] = user
                if 'attachments' in note_dict.keys():
                    note_dict['attachments'].append(attachment_dict[entity['id']])
                else:
                    note_dict['attachments'] = [attachment_dict[entity['id']]]
                active_user = user['id']

    # for i, a in enumerate(note_list):
    #     if a['type'] in ['Note', 'Reply']:
    #         print '{} note_list {} {}'.format(i, a['type'], a['content'])


    return note_list


def add_user_information(threads, user_dict):
    for thread in threads:
        user = get_sender(thread)
        if user['type'] == 'HumanUser':
            uid = user['id']
        if user['type'] == 'ClientUser':
            uid = 'c{}'.format(user['id'])
        if user['type'] == 'ApiUser': 
            uid = 'a{}'.format(user['id'])

        if 'user' in thread.keys():
            thread['user'].update(user_dict[uid])
        else:
            thread['user'] = user_dict[uid]

    return threads


def get_id(entity): 
    id = str()
    if entity['type'] == 'Ticket': 
        id = 'T{}'.format(entity['id'])
    if entity['type'] == 'Note': 
        id = 'N{}'.format(entity['id'])
    return id


def get_children(entity): 
    children = get_note_ticket_attr(entity, note_key='sg_subnotes', ticket_key='sg_note')
    return children or list()


def get_parents(entity): 
    parents = list()
    if entity['type'] == 'Note': 
        parents = entity['ticket_sg_note_tickets']
    return parents


def get_title(entity):
    return get_note_ticket_attr(entity, note_key='title', ticket_key='subject')


def get_type(entity):
    return entity['type']


def get_status(entity):
    return entity['sg_status_list']


def get_content(entity):
    text = get_note_ticket_attr(entity, note_key='content', ticket_key='description')
    # if text: 
    #     text.decode(encoding='UTF-8', errors='strict')
    return text


def get_to(entity):
    return entity['addressings_to']


def get_cc(entity):
    return entity['addressings_cc']


def get_entity(entity):
    return entity.get('entity', dict())

def get_sender(entity):
    return get_note_ticket_attr(entity, note_key='user', ticket_key='created_by')


def get_due_date(entity): 
    due_date_dict = dict()
    if entity['type'] == 'Ticket': 
        due_date = entity['sg_due_date']
        due_date_dict['ticket'] = due_date
    if entity['type'] == 'Note': 
        tasks = entity['tasks']

        for task in tasks: 
            due_date = task['due_date']
            due_date_dict[task['name']] = due_date
    return due_date_dict


def get_tasks(entity):
    tasks = list()
    if entity['type'] == 'Note':
        tasks = entity['tasks']
        if tasks:
            tasks = [a['name'] for a in tasks]
    return tasks


def get_note_ticket_attr(entity, note_key, ticket_key):
    if entity['type'] in ['Ticket', 'Attachment']:
        text = entity.get(ticket_key)
    if entity['type'] in ['Note', 'Reply']:
        text = entity.get(note_key)
    return text


def download_thumbnail(url, dst):
    dirname = os.path.dirname(dst)
    if not os.path.exists(dirname):
        os.makedirs(dirname)

    urllib.urlretrieve(url, dst)

    if os.path.exists(dst):
        return dst


def download_attachment_thumbnails(attachment_dict):
    for k, entity in attachment_dict.items():
        url = entity['image']
        if url: 
            # filename = entity['filename']
            # name, ext = os.path.splitext(filename)
            ext = '.jpg' # bug PNG
            dst = download_to_cache(url, entity['id'], ext, entity['type'])
            entity['preview_path'] = dst
        else: 
            entity['preview_path'] = ''
            print('No url found %s' % entity)

    return attachment_dict


def download_user_preview(user_dict):
    for k, entity in user_dict.items():
        url = entity.get('image')
        ext = '.jpg' # bug PNG
        if url:
            dst = download_to_cache(url, entity['id'], ext, entity['type'])
            entity['preview_path'] = dst

    return user_dict


def download_to_cache(url, entity_id, ext, entity_type):
    dst = get_temp_dir_path(entity_id, ext, entity_type=entity_type, filetype='thumbnail')

    if not os.path.exists(os.path.dirname(dst)):
        os.makedirs(os.path.dirname(dst))

    if not os.path.exists(dst):
        dst = download_thumbnail(url, dst)
        print('download {}'.format(dst))

    return dst


def get_temp_dir_path(id, ext, entity_type, filetype='thumbnail'):
    app = 'note_manager'
    tmp = '{}/{}'.format(os.environ['TEMP'], app)
    if filetype == 'thumbnail':
        name = 'thumb'
    elif filetype == 'hires':
        name = 'hires'
    tmp_file = '{}_{}-{}{}'.format(name, entity_type, id, ext)
    path = '{}/{}'.format(tmp, tmp_file)
    return path
