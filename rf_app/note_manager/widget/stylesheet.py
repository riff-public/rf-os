

class StyleConfig:
    # colors
    black = (0, 0, 0)
    white = (255, 255, 255)

    darker_gray = (30, 30, 30)
    dark_gray = (35, 35, 35)
    mid_lower_gray = (52, 52, 52)
    mid_gray = (68, 68, 68)
    upper_mid_gray = (80, 80, 80)
    lower_light_gray = (93, 93, 93)
    light_gray = (140, 140, 140)
    bright_gray = (190, 190, 190)
    
    dark_yellow = (112, 92, 63)
    yellow = (255, 220, 64)
    pale_yellow = (210, 160, 60)
    bright_yellow = (224, 173, 50)
    hilight_yellow = (235, 185, 42)

    dark_gray_green = (52, 56, 52)
    gray_green = (62, 69, 62)
    light_gray_green = (70, 77, 70)
    dark_green = (30, 37, 30)
    mid_green = (100, 175, 78)
    green = (110, 185, 88)

    red = (240, 80, 80)
    bright_red = (240, 50, 50)

    bright_brown = (176, 127, 35)
    upper_mid_brown = (175, 150, 110)
    mid_brown = (145, 120, 80)
    dark_brown = (123, 103, 73)

    # styles
    menu_style = 'QMenu {{ background-color: rgb{dark_gray}; color: white; padding: 4px; }}'.format(dark_gray=dark_gray)
    listwidget_item_style = 'background-color: rgba(0, 0, 0, 0);'
    title_text_style = 'color: white; font-size: 16px; font: bold;'
    searchbar_style = ''' QLineEdit {{ background-color: rgb{dark_gray}; color: white; border: 0px; border-radius: 6px; }}'''.format(dark_gray=dark_gray)
    content_text_style = 'color: white; font-size: 14px;'
    viewer_widget_style = '''QWidget {{ background-color: rgb{darker_gray}; border-width: 0px; border-style: solid; }}
    QToolTip {{ background-color: rgb{bright_yellow}; color: black; }}'''.format(darker_gray=darker_gray, bright_yellow=bright_yellow)
    viewer_font_style = '''background-color: rgba(0, 0, 0, 0); 
    border: 1px rgba(255, 255, 255, 128); border-radius: 6px; 
    color: rgba(255, 255, 255, 128); font-size: 18px;'''
    creator_label_style = '''QToolTip {{ background-color: rgb{bright_yellow}; color: black; }}
    QLabel {{ color: white; font: bold; }}'''.format(bright_yellow=bright_yellow, white=white)
    date_label_style = '''QToolTip {{ background-color: rgb{bright_yellow}; color: black; }}
    QLabel {{ color: rgb{bright_gray}; }}'''.format(bright_yellow=bright_yellow, bright_gray=bright_gray)

    # content text
    content_style = '''QMenu {{ background-color: rgb{mid_gray}; }}
    QLabel {{ color: rgb{white}; }}'''.format(mid_gray=mid_gray, white=white)
    no_content_style = '''QMenu {{ background-color: rgb{mid_gray}; }}
    QLabel {{ font: italic; color: rgb{light_gray}; }}'''.format(mid_gray=mid_gray, light_gray=light_gray)
    
    status_toolbutton_off_style = '''QToolTip {{ background-color: rgb{bright_yellow}; color: black; }}
    QToolButton {{ background-color: rgb{dark_gray}; color: rgb{light_gray}; border-radius: 6px; }}
    QToolButton:hover {{ background-color: rgb{dark_yellow}; color: white; }}
    QToolButton::menu-indicator {{ image: none; }}'''.format(bright_yellow=bright_yellow, dark_yellow=dark_yellow, light_gray=light_gray, dark_gray=dark_gray)
    
    status_toolbutton_on_style = '''QToolTip {{ background-color: rgb{bright_yellow}; color: black; }}
    QToolButton {{ background-color: rgb{pale_yellow}; color: {bright_gray}; border-radius: 6px; }}
    QToolButton:hover {{ background-color: rgb{hilight_yellow}; color: white; }}
    QToolButton::menu-indicator {{ image: none; }}'''.format(pale_yellow=pale_yellow, hilight_yellow=hilight_yellow, bright_yellow=bright_yellow, bright_gray=bright_gray)
    
    msgbox_style = 'background-color: rgb{mid_gray}'.format(mid_gray=mid_gray)
    thumbnail_style = '''QListWidget {{ padding: 0px 0px 6px 6px; border: 0px; }}
    QListWidget::item {{ border: none; border-radius: 6px; }}'''

    job_listWidget_style = '''QListWidget {{ background-color: rgb{dark_green}; border-radius: 6px; padding: 6px;}}
    QListWidget::item {{ background-color: rgb{mid_gray}; border-radius: 6px;}}
    QListWidget::item:selected {{ background-color: rgb{gray_green}; border: 2px solid rgb{mid_green}; border-radius: 6px; }}
    QListWidget::item:hover {{ background-color: rgb{gray_green}; border-radius: 6px;}}
    QToolTip {{ background-color: rgb{bright_yellow}; color: black; }}
    '''.format(bright_yellow=bright_yellow, gray_green=gray_green, mid_gray=mid_gray, mid_green=mid_green, dark_green=dark_green)

    feed_listWidget_style = '''QListWidget {{ background-color: rgb{dark_green}; padding: 6px; border-radius: 6px; }}
    QListWidget::item {{ background-color: rgb{gray_green}; border-radius: 6px;}}
    QListWidget::item:selected {{ background-color: rgb{gray_green}; border: 2px solid rgb{gray_green}; border-radius: 6px;}}
    QListWidget::item:hover {{ background-color: rgb{gray_green}; border-radius: 6px;}}
    '''.format(dark_green=dark_green, gray_green=gray_green)