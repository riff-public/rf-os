import os 
import sys 
from collections import OrderedDict 
from Qt import QtWidgets
from Qt import QtCore 
from Qt import QtGui

from . import note_utils
# reload(note_utils)


class Config: 
    all_filter = '- All -'
    filter_map = {
            'project': '$', # project
            'sender': '#', # sender
            'users': '@', # receiver
            'step': '!', # step
            'status': '&', # status
            'task': '*'} # task


class FilterWidget(QtWidgets.QWidget):
    """docstring for FilterWidget"""
    item_selected = QtCore.Signal(object)
    filtered_data = QtCore.Signal(object)

    def __init__(self, name='', config=dict(), parent=None):
        super(FilterWidget, self).__init__(parent=parent)
        self.config = config
        self.name = name
        self.setup_ui()

    def setup_ui(self): 
        self.layout = QtWidgets.QVBoxLayout()
        self.name_label = QtWidgets.QLabel(self.name)
        self.filter = QtWidgets.QListWidget()
        self.filter.setSortingEnabled(True)
        self.layout.addWidget(self.name_label)
        self.layout.addWidget(self.filter)
        self.setLayout(self.layout)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.filter.itemSelectionChanged.connect(self.filter_selected)
        self.prefix = ''
        self.separator = ':'

    def set_multiple_selection(self): 
        self.filter.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

    def set_title(self, text): 
        self.label.setText(text)

    def add_input(self, note_list):
        self.note_list = note_list

        self.filter.blockSignals(True)

        self.filter.clear()

        # for note in note_list: 
        #     if note['id'] == 822: 
        #         print '%s noteeeee %s' % (self.prefix, note)

        data = self.extract_data(note_list)
        # count only note with keyword
        all_display = '{} [{}]'.format(Config.all_filter, len(note_list))
        self.add_filter(all_display, data=Config.all_filter)

        for i, v in enumerate(sorted(data.keys())):
            iconpath = None
            map_display = v
            if self.config: 
                map_display = self.config.get(v, dict()).get('display', v)
                iconpath = self.config.get(v, dict()).get('icon')

            display = '{} [{}]'.format(map_display, data[v])
            self.add_filter(display, data=v, iconpath=iconpath)

        self.filter.blockSignals(False)
        self.select_item(all_display)

    def extract_data(self, note_list):
        return note_utils.extract_data(note_list, self.prefix, self.separator)

    def add_filter(self, display, data=None, iconpath=None): 
        data = display if not data else data
        item = QtWidgets.QListWidgetItem()
        item.setText(display)
        item.setData(QtCore.Qt.UserRole, data)
        if iconpath and os.path.exists(iconpath): 
            iconWidget = QtGui.QIcon()
            iconWidget.addPixmap(QtGui.QPixmap(iconpath),QtGui.QIcon.Normal,QtGui.QIcon.Off)
            # font = QtGui.QFont("Arial", 8)
            item.setIcon(iconWidget)
        self.filter.addItem(item)

    def filter_selected(self): 
        filtered_list = list()
        for note in self.note_list:
            keyword = note.get('keyword')
            if not keyword:
                filtered_list.append(note)
            if keyword:
                if Config.all_filter in self.selected_filters():
                    filtered_list.append(note)
                elif any(a in keyword for a in self.selected_filters()):
                    filtered_list.append(note)

        # print '%s filter %s to %s' % (self.label.text(), len(self.note_list), len(filtered_list))
        self.filtered_data.emit(filtered_list)

    def input_data(self, input_list): 
        self.input_list = input_list

    def selected_filters(self): 
        return [a.data(QtCore.Qt.UserRole) for a in self.filter.selectedItems()]
        
    def clear(self): 
        self.filter.clear()

    def all_items(self, *kwargs): 
        return [self.filter.item(a) for a in range(self.filter.count())]

    def set_all_item(self): 
        if self.all_items(): 
            all_item = self.init_all_item()
            self.filter.insertItem(0, all_item)

    def set_all_on_top(self): 
        all_items = self.all_items()
        if all_items: 
            if Config.all_filter in [str(a.text()) for a in all_items]: 
                row = self.filter.row(self.all_item)
                self.filter.takeItem(row)
                self.filter.insertItem(0, self.all_item)

        # self.filter.insertItem(0, Config.all_filter)

    def sort(self): 
        self.filter.sortItems() 
        self.set_all_on_top()
        # ascending by default
        # self.listWidget.sortItems(QtCore.Qt.DescendingOrder)
        # items = [self.filter.item(a) for a in range(self.filter.count())]
        # key = dict()
        # for item in items: 
        #     key[item.text()] = item 

    def select_item(self, keyword): 
        items = self.all_items()
        datas = [a.data(QtCore.Qt.UserRole) for a in items]
        labels = [str(a.text()) for a in items]
        if keyword in labels: 
            index = labels.index(keyword)
            self.filter.setCurrentRow(index)
        elif keyword in datas: 
            index = datas.index(keyword)
            self.filter.setCurrentRow(index)
