# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:\Dropbox\script_server\core\rf_app\note_manager\ui2.ui'
#
# Created: Mon Jan 31 13:50:35 2022
#      by: pyside2-uic  running on PySide2 2.0.0~alpha0
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(1070, 715)
        self.verticalLayout = QtWidgets.QVBoxLayout(Form)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.userLabel = QtWidgets.QLabel(Form)
        self.userLabel.setObjectName("userLabel")
        self.horizontalLayout_3.addWidget(self.userLabel)
        self.userCombo = QtWidgets.QComboBox(Form)
        self.userCombo.setObjectName("userCombo")
        self.userCombo.addItem("")
        self.horizontalLayout_3.addWidget(self.userCombo)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem)
        self.logo = QtWidgets.QLabel(Form)
        self.logo.setObjectName("logo")
        self.horizontalLayout_3.addWidget(self.logo)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.line_4 = QtWidgets.QFrame(Form)
        self.line_4.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_4.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_4.setObjectName("line_4")
        self.verticalLayout.addWidget(self.line_4)
        self.main_splitter = QtWidgets.QSplitter(Form)
        self.main_splitter.setOrientation(QtCore.Qt.Horizontal)
        self.main_splitter.setObjectName("main_splitter")
        self.tab_widget = QtWidgets.QTabWidget(self.main_splitter)
        self.tab_widget.setObjectName("tab_widget")
        self.tab_3 = QtWidgets.QWidget()
        self.tab_3.setObjectName("tab_3")
        self.tab1_splitter = QtWidgets.QSplitter(self.tab_3)
        self.tab1_splitter.setGeometry(QtCore.QRect(9, 9, 256, 576))
        self.tab1_splitter.setOrientation(QtCore.Qt.Vertical)
        self.tab1_splitter.setObjectName("tab1_splitter")
        self.filter1 = QtWidgets.QListWidget(self.tab1_splitter)
        self.filter1.setObjectName("filter1")
        self.filter2 = QtWidgets.QListWidget(self.tab1_splitter)
        self.filter2.setObjectName("filter2")
        self.filter3 = QtWidgets.QListWidget(self.tab1_splitter)
        self.filter3.setObjectName("filter3")
        self.tab_widget.addTab(self.tab_3, "")
        self.tab_4 = QtWidgets.QWidget()
        self.tab_4.setObjectName("tab_4")
        self.splitter_3 = QtWidgets.QSplitter(self.tab_4)
        self.splitter_3.setGeometry(QtCore.QRect(9, 9, 256, 576))
        self.splitter_3.setOrientation(QtCore.Qt.Vertical)
        self.splitter_3.setObjectName("splitter_3")
        self.listWidget_6 = QtWidgets.QListWidget(self.splitter_3)
        self.listWidget_6.setObjectName("listWidget_6")
        self.listWidget_4 = QtWidgets.QListWidget(self.splitter_3)
        self.listWidget_4.setObjectName("listWidget_4")
        self.listWidget_5 = QtWidgets.QListWidget(self.splitter_3)
        self.listWidget_5.setObjectName("listWidget_5")
        self.tab_widget.addTab(self.tab_4, "")
        self.inbox_widget = QtWidgets.QListWidget(self.main_splitter)
        self.inbox_widget.setObjectName("inbox_widget")
        self.detail_widget = QtWidgets.QListWidget(self.main_splitter)
        self.detail_widget.setObjectName("detail_widget")
        self.verticalLayout.addWidget(self.main_splitter)
        self.verticalLayout.setStretch(2, 1)

        self.retranslateUi(Form)
        self.tab_widget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(QtWidgets.QApplication.translate("Form", "Form", None, -1))
        self.userLabel.setText(QtWidgets.QApplication.translate("Form", "User", None, -1))
        self.userCombo.setItemText(0, QtWidgets.QApplication.translate("Form", "Hanuman", None, -1))
        self.logo.setText(QtWidgets.QApplication.translate("Form", "Logo", None, -1))
        self.tab_widget.setTabText(self.tab_widget.indexOf(self.tab_3), QtWidgets.QApplication.translate("Form", "Tab 1", None, -1))
        self.tab_widget.setTabText(self.tab_widget.indexOf(self.tab_4), QtWidgets.QApplication.translate("Form", "Tab 2", None, -1))

