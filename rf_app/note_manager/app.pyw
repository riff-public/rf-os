#!/usr/bin/env python
# -- coding: utf-8 --

_title = 'RF Note Manager'
_version = 'v.0.0.1'
_des = 'Wip'
uiName = 'NoteManagerUi'

#Import python modules
import sys
import os
import logging
import getpass
import traceback
from collections import OrderedDict
import os
import json
from datetime import datetime

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '%s/core' % os.environ.get('RFSCRIPT')
sys.path.append(core)

# import config
import rf_config as config

try: 
    reload
except: 
    from importlib import reload

from rf_utils import log_utils
from rf_utils.ui import load
reload(load)
from rf_utils.ui import stylesheet
# maya & module function
from . import ui
reload(ui)
reload(ui.inbox_widget)
from rf_utils.widget import dialog
from rf_utils.widget import widget_utils
from rf_utils.context import context_info
from rf_utils import thread_pool
from rf_utils import file_utils
from rf_utils.sg import sg_note
from rf_utils.sg import sg_process
from rf_app.note_manager.widget import note_utils
reload(note_utils)
ui.sg = sg_note.sg

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui


isNuke = True
try:
    import nuke
    import nukescripts
except ImportError:
    isNuke = False

modulePath = sys.modules[__name__].__file__.replace('\\', '/')
moduleDir  = os.path.dirname(modulePath)
appModuleDir = os.path.dirname(moduleDir)


class NoteManager(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(NoteManager, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.py' % moduleDir
        self.w = 1080
        self.h = 680
        self.threadpool = QtCore.QThreadPool()
        self.threadpool.setMaxThreadCount(1)
        self.ui = ui.NoteManagerUi(threadpool=self.threadpool)
        self.ui.form_widget.threadpool = self.threadpool
        self.ui.content_widget.threadpool = self.threadpool
        self.ui.content_widget.note_body.threadpool = self.threadpool
        self.ui.content_widget.reply.threadpool = self.threadpool
        self.ui.content_widget.ui = self.ui

        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.resize(self.w, self.h)
        self.setWindowTitle('%s %s %s' % (_title, _version, _des))
        self.setWindowIcon(QtGui.QIcon('{}/icons/app_icon.png'.format(moduleDir)))
        self.init_functions()

    def closeEvent(self, event):
        '''
        Closes all tab widgets before actually close itself triggering each tab closeEvents
        '''
        pass 

    def init_functions(self): 
        self.ui.note = self.ui.tab_modules['note']
        self.ui.display_widget.context_menu_display = {'Create Note': self.create_note}
        self.ui.tab_widget.currentChanged.connect(self.tab_changed)
        # self.filter_widgets = [self.ui.sender_filter.filter, self.ui.user_filter.filter, self.ui.status_filter.listWidget]
        self.init_signals()

    def init_signals(self): 
        self.ui.note.submit_button.clicked.connect(self.fetch_note)
        self.ui.note.last_filter_selected.connect(self.filter_complete)
        self.ui.display_widget.note_selected.connect(self.note_selected)

    def fetch_note(self): 
        print('start fetching ...')
        worker = thread_pool.Worker(self.fetch_note_call)
        worker.signals.result.connect(self.fetch_note_finished)
        self.threadpool.start(worker)
        self.start_counter = datetime.now()
        self.ui.note.submit_button.setEnabled(False)

    def fetch_note_call(self): 
        user_entity = self.ui.user_widget.data()
        note_list = sg_note.fetch_notes_tickets(user_entity=user_entity)
        return note_list 

    def fetch_note_finished(self, data_list):
        # convert this to tree relation 
        self.ui.note.submit_button.setEnabled(True)
        print('finished in %s' % (datetime.now() - self.start_counter))
        self.note_list, self.extra_note_list = data_list

        self.ui.note.input_data(self.note_list)

    def filter_complete(self, note_list):
        self.filtered_list = note_list
        self.set_note_display()

    def note_selected(self, entity):
        self.ui.content_widget.add_note(entity)

    def tab_changed(self, index): 
        return 

    def set_note_display(self): 
        """ this method will be triggered when status filter is selected """ 
        # find current selection status filter 
        # sort data by date

        # for note in self.filtered_list: 
        #     print(note['id'])
        #     if note['id'] == 25270: 
        #         self.ui.display_widget.label.setText(note['content'].decode(encoding='utf-8'))
        self.order_tree, self.note_dict = note_utils.sort_group_by_date(self.filtered_list, self.extra_note_list)
        self.order_list = note_utils.reformat_to_list(self.order_tree, self.note_dict)
        # for a in self.order_list: 
        #     if a['id'] == 822: 
        #         print( 'found %s' % a)
        # set display 
        self.ui.display_widget.clear()
        # self.ui.content_widget.clear()
        self.ui.display_widget.add_item_pages(self.order_list, root=None)

    def create_note(self, data): 
        index = self.ui.tab_index.get('ticket_note_form')
        self.ui.tab_widget.setCurrentIndex(index)
        print('index', index)


def show(mode='default'):
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        if mode == 'default':
            maya_win.deleteUI(uiName)
            myApp = NoteManager(parent=maya_win.getMayaWindow())
        myApp.show()
    elif config.isNuke:
        from rf_nuke import nuke_win 
        logger.info('Run in Nuke\n')
        if mode == 'default':
            nuke_win.deleteUI(uiName)
            myApp = NoteManager(parent=nuke_win._nuke_main_window())
        myApp.show()
    else:
        logger.info('Run in standalone\n')
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        if mode == 'default':
            myApp = NoteManager(parent=None)
        myApp.show()
        stylesheet.set_default(app)
        sys.exit(app.exec_())
    return myApp


if __name__ == '__main__':
    show()
