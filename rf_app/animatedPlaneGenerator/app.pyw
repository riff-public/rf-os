#!/usr/bin/env python
# -- coding: utf-8 --

# v.0.0.1 polytag switcher
# v.0.0.2 fig bugs
# v.1.0.0 add current scene button

_title = 'RF Anim Plane Generator'
_version = 'v.1.0.0'
_des = ''
uiName = 'AnimPlaneGenerator'

#Import python modules
import sys
import os
import logging
import getpass
import traceback
from collections import OrderedDict
import os
import json
import pickle
from glob import glob
import subprocess
from functools import partial

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '{}/core'.format(os.environ.get('RFSCRIPT'))
sys.path.append(core)

# import config
import rf_config as config

from rf_utils import log_utils
from rf_utils.ui import load
# reload(load)
from rf_utils.ui import stylesheet

user = '{}-{}'.format(config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from rf_utils import thread_pool

# maya & module function
from . import ui
reload(ui)
# from . import tabs
from rf_utils.widget import media_widget
reload(media_widget)
from rf_maya.rftool.utils import animation_plane
reload(animation_plane)

from rf_utils.sg import sg_process
from rf_utils.context import context_info
ui.sg = sg_process.sg

if config.isNuke:
    import nuke
    import nukescripts
elif config.isMaya:
    import maya.cmds as mc
    import pymel.core as pm

modulePath = sys.modules[__name__].__file__.replace('\\', '/')
moduleDir  = os.path.dirname(modulePath)
appModuleDir = os.path.dirname(moduleDir)

class KeyConfig:
    word = 'Ui'

class AnimPlaneGenerator(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(AnimPlaneGenerator, self).__init__(parent)
        self.entity = None

        # ui read
        uiFile = '{}/ui.py'.format(moduleDir)
        self.ui = ui.AnimPlaneGeneratorUi()
        self.w = 1190
        self.h = 685
        # self.tab_config = tab_config

        self._default_quality = 30
        self.__default_render_size = (1920, 1080)
        self.__default_fps = 24
        self._load_size_mult = 0.5
        self._minimum_grid_size = 64
        self._maximum_grid_size = 640
        self._default_grid_size = 215

        self.assetList = []

        self.description_display_font = QtGui.QFont()
        self.description_display_font.setItalic(True)
        self.description_display_font.setPointSize(8)

        self.grey_font_palette = QtGui.QPalette()
        self.grey_font_palette.setBrush(QtGui.QPalette.Text, QtGui.QBrush(QtGui.QColor(128, 128, 128)))


        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.resize(self.w, self.h)
        self.setWindowTitle('{} {} {}'.format(_title, _version, _des))
        # self.setWindowIcon(QtGui.QIcon('{}/icons/app_icon.png'.format(moduleDir)))
        self.setup_ui()
        self.set_default()
        self.init_signals()
        self.init_functions()

    def init_signals(self):
        self.ui.project_widget.projectChanged.connect(self.project_changed)
        self.ui.refresh_button.clicked.connect(self.refresh)
        self.ui.search_lineEdit.textChanged.connect(self.search_update_viewer)
        self.gridsize_slider.valueChanged.connect(self.update_item_size)
        self.ui.generate_button.clicked.connect(self.generate_click)

        self.media_viewer.customContextMenuRequested.connect(self.item_right_clicked)
        self.media_viewer.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)

    def item_right_clicked(self, pos):
        # right click menu
        item = self.media_viewer.itemAt(pos)
        sel_items = self.media_viewer.selectedItems()
        if item and [i for i in sel_items if i is item]:
            rightClickMenu = QtWidgets.QMenu(self.media_viewer)
            openMenu = rightClickMenu.addAction('Open in explorer')
            openMenu.triggered.connect(partial(self.open_in_explorer))

        rightClickMenu.exec_(self.media_viewer.mapToGlobal(pos))

    def open_in_explorer(self):
        sel_items = self.media_viewer.selectedItems()
        if not sel_items:
            return
        for item in sel_items:
            item_info = item.data(QtCore.Qt.UserRole)
            if 'movPath' in item_info and item_info['movPath'] and os.path.exists(item_info['movPath']):
                path = os.path.dirname(item_info['movPath'])
                subprocess.Popen(r'explorer {}'.format(path.replace('/', '\\')))

    def init_functions(self):
        # this will trigger current project to list episode, sequence and shots
        self.ui.project_widget.emit_project_changed()

    def setup_ui(self):
        # -------------------
        # media layout
        self.media_layout = QtWidgets.QVBoxLayout()
        self.media_layout.setSpacing(3)

        # media viewer
        self.media_viewer = media_widget.MediaGridViewer(quality=self._default_quality*0.01)
        self.media_layout.addWidget(self.media_viewer)
        self.ui.display_layout.addLayout(self.media_layout)

        # setting form layout
        self.thumbsize_layout = QtWidgets.QFormLayout()
        self.thumbsize_layout.setLabelAlignment(QtCore.Qt.AlignLeft)
        self.thumbsize_layout.setFormAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)
        self.thumbsize_layout.setSpacing(3)
        self.ui.display_layout.addLayout(self.thumbsize_layout)

        # thumbnail size slider
        self.gridsize_slider = QtWidgets.QSlider()
        self.gridsize_slider.setMinimum(self._minimum_grid_size)
        self.gridsize_slider.setMaximum(self._maximum_grid_size)
        self.gridsize_slider.setOrientation(QtCore.Qt.Horizontal)
        self.thumbsize_layout.addRow('Size: ', self.gridsize_slider)


        self.threadpool = QtCore.QThreadPool()
        self.threadpool.setMaxThreadCount(1)

        if not config.isMaya:
            self.ui.generate_button.setEnabled(0)

    def set_default(self):

        self.gridsize_slider.setValue(self._default_grid_size)

        self.media_viewer.auto_play = True

    def project_changed(self):
        self.project = self.ui.project_widget.current_project()
        context = context_info.Context()
        context.update(
            project=self.project,
            entityType='scene')
        self.entity = context_info.ContextPathInfo(context=context)
        self.refresh()

    def refresh(self):
        self.media_viewer.clear()
        if not self.entity:
            return
            
        if self.entity:
            # update media widget settings everytime entity changes
            self.media_viewer.fps = self.entity.projectInfo.render.fps()
            w, h = self.entity.projectInfo.render.final_outputH()
            self._render_size = (w * self._load_size_mult, h * self._load_size_mult)  # multply the load size for performance
        else:
            self.media_viewer.fps = self.__default_fps
            self._render_size = self.__default_render_size

        self.thread_fetch_data(self.entity)


        self.update_item_size()

    def thread_fetch_data(self, entity): 
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        worker = thread_pool.Worker(self.fetch_data, entity)
        worker.signals.result.connect(self.list_media)
        self.threadpool.start(worker)

    def fetch_data(self, entity): 
        fetch_results = []
        sg_data = sg_process.get_assets(entity.project)

        fetch_results = sg_data
        return fetch_results

    def list_media(self, fetch_results):
        self.assetList = []

        for sg_asset in fetch_results:
            assetName = sg_asset['code']
            tags = sg_asset['tags']
            tagsList = []
            for tag in tags:
                tagsList.append(tag['name'])
            if KeyConfig.word in tagsList:
                self.assetList.append(assetName)
        self.update_viewer(self.assetList)

    def update_item_bg(self):
        for x in range(self.media_viewer.count()-1):
            item = (self.media_viewer.item(x))
            itemData = item.data(QtCore.Qt.UserRole)
            print itemData
            if itemData:
                globPath = itemData['globPath']
                if globPath:
                    print globPath
                # item.setBackgroundColor(bgWarnColor)


    def update_item_size(self):
        size = self.gridsize_slider.value() * 0.001
        self.media_viewer.item_size = (self._render_size[0], self._render_size[1])
        self.media_viewer.setIconSize(QtCore.QSize(self._render_size[0]*size, self._render_size[1]*size))

    def search_update_viewer(self):
        key_search = self.ui.search_lineEdit.text()
        for index in xrange(self.media_viewer.count()):
            item = self.media_viewer.item(index)
            itemData = item.data(QtCore.Qt.UserRole)
            if key_search.lower() in itemData['name'].lower():
                item.setHidden(False)
            else:
                item.setHidden(True)
        # self.update_viewer(self.assetList)

    def get_design_mov_path(self,assetName):
        mov = ''
        # project = self.ui.project_widget.current_project()
        assetContext = context_info.Context()
        assetContext.update(
            project=self.project,
            entityType='design',
            entityGrp='prop',
            step='design',
            task='expression',
            look = 'main',
            process = 'expression',
            entity=assetName)
            # entityGrp=episode,
            # entityParent=sequence,
            # entityCode=shot)
        assetEntity = context_info.ContextPathInfo(context=assetContext)
        movPath = assetEntity.path.output_hero().abs_path()
        if os.path.exists(movPath):
            mov = os.listdir(movPath)
            if mov:
                mov = os.path.join(movPath, mov[0])

        return mov


    def update_viewer(self, assetList):
        self.media_viewer.clear()

        key_search = self.ui.search_lineEdit.text()

        # self.project = self.ui.project_widget.current_project()

        view_data = []
        for asset in assetList:
            assetContext = context_info.Context()
            assetContext.update(
                project=self.project,
                entityType='asset',
                entityGrp='prop',
                look = 'main',
                entity=asset)
            assetEntity = context_info.ContextPathInfo(context=assetContext)
            texturePath = assetEntity.path.publish_texture().abs_path()
            if os.path.exists(texturePath):
                globPath = texturePath + '/*.png'
                # print globPath
            else:
                globPath = ''

            movPath = self.get_design_mov_path(asset)
            if movPath:

                text = asset
                
                user_data = {'globPath':globPath , 'name':asset, 'movPath':movPath}

                if os.path.exists(texturePath):
                    user_data['__backgroundColor'] = (15, 80, 138)

                if key_search:
                    if key_search.lower() in asset.lower():
                        view_data.append((movPath, text, user_data))
                else:
                    view_data.append((movPath, text, user_data))

                view_data = sorted(view_data, key=lambda i: i[2])


        if view_data:
            self.media_viewer.add_items(media_info=view_data)
        QtWidgets.QApplication.restoreOverrideCursor()

    # def autoplay_toggled(self):
    #     if self.autoplay_checkbox.isChecked():
    #         self.media_viewer.auto_play = True
    #         self.set_item_toolTips(text='')
    #     else:
    #         self.media_viewer.auto_play = False
    #         self.set_item_toolTips(text='Middle mouse drag to flip through frames')

    def generate_click(self):
        # pop up user to choose application to play media
        qmsgBox = QtWidgets.QMessageBox(self)
        # set tile, icon
        qmsgBox.setWindowTitle('Generate Plane')
        qmsgBox.setIcon(QtWidgets.QMessageBox.Question)

        # set text
        text = 'Are you sure to generate animated plane?\n'
        sel_items = self.media_viewer.selectedItems()
        if not sel_items:
            qmsgBox.setIcon(QtWidgets.QMessageBox.Warning)
            qmsgBox.setText("No selected asset , Can't generate.")
            qmsgBox.exec_()
            return
        for item in sel_items:
            itemData = item.data(QtCore.Qt.UserRole)
            if itemData:
                globPath = itemData['globPath']
                if not globPath:
                    qmsgBox.setIcon(QtWidgets.QMessageBox.Warning)
                    qmsgBox.setText("Some selected asset don't have Image Sequence , Can't generate.")
                    qmsgBox.exec_()
                    return
                name = itemData['name']
                text += ':' + name + '\n'
        qmsgBox.setText(text)

        qmsgBox.addButton('OK', QtWidgets.QMessageBox.AcceptRole)
        cancel_button = qmsgBox.addButton('Cancel', QtWidgets.QMessageBox.RejectRole)
        qmsgBox.exec_()
        clicked_button = qmsgBox.clickedButton()
        if clicked_button != cancel_button:
            self.generate_plane()

    def generate_plane(self):
        exportGrp = self.entity.projectInfo.asset.get('exportGrp') or 'Export_Grp'

        planeSize = self.ui.planeSize_spinBox.value()
        sel_items = self.media_viewer.selectedItems()
        for item in sel_items:
            itemData = item.data(QtCore.Qt.UserRole)
            if itemData:
                globPath = itemData['globPath']
                # movPath = itemData['movPath']
                assetName = itemData['name']
                placement = animation_plane.generate(globPath, name=assetName, scale=planeSize)
                if not pm.objExists(exportGrp):
                    pm.group(placement ,n=exportGrp)
                pm.parent(placement ,exportGrp)




def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = AnimPlaneGenerator(parent=maya_win.getMayaWindow())
        myApp.show()
    else:
        logger.info('Run in standalone\n')
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        myApp = AnimPlaneGenerator(parent=None)
        myApp.show()
        stylesheet.set_default(app)
        sys.exit(app.exec_())
    return myApp


if __name__ == '__main__':
    show()
