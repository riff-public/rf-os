#!/usr/bin/env python
# -- coding: utf-8 --

import os
import sys
import traceback
from collections import OrderedDict

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir = os.path.dirname(module_path)
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils.widget import project_widget
# from rf_utils.widget import entity_browse_widget
from rf_utils import icon
sg = None


class Setting:
    refreshIcon = '%s/icons/refresh.png' % module_dir
    # currentIcon = '%s/icons/current_icon.png' % module_dir


class AnimPlaneGeneratorUi(QtWidgets.QWidget):
    status = QtCore.Signal(str)

    def __init__(self, parent=None):
        super(AnimPlaneGeneratorUi, self).__init__(parent)
        self.layout = QtWidgets.QVBoxLayout()
        self.sub_layout = QtWidgets.QHBoxLayout()
        self.layout.addLayout(self.sub_layout)
        self.setLayout(self.layout)

        self.setup_widget()
        # self.init_signals()
        self.init_functions()

    def setup_widget(self):
        # logo
        self.logo = QtWidgets.QLabel()
        self.logo.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))
        self.layout.insertWidget(0, self.logo)

        # button layout
        self.button_layout = QtWidgets.QHBoxLayout()
        self.layout.insertLayout(1, self.button_layout)
        # spacer
        button_spacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.button_layout.addItem(button_spacer)

        self.refresh_button = QtWidgets.QPushButton()
        self.refresh_button.setMaximumSize(32, 32)
        self.refresh_button.setIcon(QtGui.QIcon(Setting.refreshIcon))
        self.button_layout.addWidget(self.refresh_button)

        # navigation bar
        self.nav_layout = QtWidgets.QVBoxLayout()
        self.project_widget = project_widget.SGProjectComboBox(sg=sg)

        # search bar 
        self.search_lineEdit = QtWidgets.QLineEdit()
        self.search_lineEdit.setPlaceholderText('Search by name...')

        spacerItem_nav = QtWidgets.QSpacerItem(25, 25, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)

        self.nav_layout.addWidget(self.project_widget)
        self.nav_layout.addWidget(self.search_lineEdit)
        self.nav_layout.addItem(spacerItem_nav)
        self.sub_layout.addLayout(self.nav_layout)

        self.display_layout = QtWidgets.QVBoxLayout()
        self.sub_layout.addLayout(self.display_layout)

        self.button_layout2 = QtWidgets.QVBoxLayout()
        self.button_layout2.setSpacing(5)
        self.generate_button = QtWidgets.QPushButton('Generate')
        self.generate_button.setFixedSize(QtCore.QSize(150, 30))
        # self.generate_button.setIcon(QtGui.QIcon(Setting.refreshIcon))

        self.planeSize_label = QtWidgets.QLabel('Plane Size :')
        self.planeSize_spinBox = QtWidgets.QDoubleSpinBox()
        self.planeSize_spinBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.planeSize_spinBox.setButtonSymbols(QtWidgets.QAbstractSpinBox.UpDownArrows)
        self.planeSize_spinBox.setMinimum(1.0)
        self.planeSize_spinBox.setMaximum(30.0)
        self.planeSize_spinBox.setValue(1.0)
        self.planeSize_layout = QtWidgets.QHBoxLayout()
        self.planeSize_layout.addWidget(self.planeSize_label, QtCore.Qt.AlignLeft)
        self.planeSize_layout.addWidget(self.planeSize_spinBox, QtCore.Qt.AlignLeft)

        spacerItem_2 = QtWidgets.QSpacerItem(25, 25, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)

        self.button_layout2.addLayout(self.planeSize_layout)
        self.button_layout2.addWidget(self.generate_button)
        self.button_layout2.addItem(spacerItem_2)
        self.sub_layout.addLayout(self.button_layout2)

        self.sub_layout.setStretch(0, 0)
        self.sub_layout.setStretch(1, 1)
        self.sub_layout.setStretch(2, 0)

        # spacer
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.nav_layout.addItem(spacerItem)

        self.nav_layout.setStretch(0, 0)
        self.nav_layout.setStretch(1, 1)
        # self.nav_layout.setStretch(2, 2)

    # def init_signals(self):
    #     self.project_widget.projectChanged.connect(self.project_changed)

    def init_functions(self):
        # this will trigger current project to list episode, sequence and shots
        self.project_widget.emit_project_changed()

    # def project_changed(self, project):
    #     pass
    #     # self.sg_nav_widget.list_episodes(project)



