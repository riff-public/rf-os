from rf_maya.rftool.layout.split_shot import split_shot_cmd
from rf_maya.rftool.utils import maya_utils
import maya_hook as hook
reload (hook)
def export_shot(shot, dstPath):

    # go to current shot, set start & end frame each shot
    # shift key 

    split_shot_cmd.shiftShot(shot)
    camRig = hook.find_cam_rig(shot)
    hook.set_seq_current_time(101)
    start, end = hook.shot_time(shot)
 
    hook.look_thru(camRig)
    hook.clearSelection()


    #select Object
    list_frame_selection = listFrame(int(start), int(end), 5)
    for frame in list_frame_selection:
        print frame
        hook.changeFrame(frame)
        hook.select_viewport_objects() 
        selection = hook.listSelection()
        hook.addSelection(selection)

    # GET A LIST OF REFERENCES
    hook.addSelection(shot)
    hook.addSelection(camRig)  
    # import audio
    #
    ##
    #
    mayaFile = hook.get_file_format(shot)
    dstPath = hook.makedirs(dstPath)
    pathFile = '%s/%s' %(dstPath, mayaFile)
    hook.exportSelected(pathFile)

    writeFileMaSenceConfig(pathFile, start, end)
    writeFileMaShot(pathFile, shot)

def listFrame(start, end, skipFrameCount):
    frameRange = list(range(start, end+1))
    list_frame_selection = []
    for index, frame in enumerate(frameRange):
        if (index % skipFrameCount == 0):
            list_frame_selection.append(frame)
        else:
            pass
    return list_frame_selection


def writeFileMaSenceConfig(pathFile, start, end):
    file = open(pathFile,"a") 
    senceConfig = 'createNode script -name "sceneConfigurationScriptNode";setAttr ".before" -type "string" "playbackOptions -min %s -max %s -ast %s -aet %s ";setAttr ".scriptType" 6;' % (start, end, start, end)
    file.write(senceConfig) 
    file.close()

def writeFileMaShot(pathFile, shotName):
    file = open(pathFile,"a") 
    createNode = '\ncreateNode sequencer -n "sequencer1";'
    senceConfig = '\nconnectAttr "%s.se" "sequencer1.shts" -na;'%(shotName)
    mayaNode = '\nconnectAttr "%s.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[2].dn";'%(shotName)
    file.write(createNode)
    file.write(mayaNode)
    file.write(senceConfig) 
    file.close()

