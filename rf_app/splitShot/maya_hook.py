import maya.cmds as mc
import pymel.core as pm
import maya.OpenMaya as om
import maya.OpenMayaUI as omUI
import os


def makedirs(path): 
    os.makedirs(path) if not os.path.exists(path) else None 
    return path

def exportSelected(pathFile):
    pm.exportSelected(pathFile, preserveReferences = True)  
          
def listSelection():
    return mc.ls(sl=True)

def select_viewport_objects(): 
    view = omUI.M3dView.active3dView()
    om.MGlobal.selectFromScreen(0,0,view.portWidth(),view.portHeight(),om.MGlobal.kAddToList)
    sel = mc.ls(sl=True)
    return sel

def find_cam_rig(shot):
    camera = mc.shot(shot, q=True, currentCamera=True)
    parentGrp = mc.listRelatives(camera, parent = True)
    return parentGrp[0] if parentGrp else ''

def set_seq_current_time(value):
    return mc.sequenceManager(currentTime=value)

def shot_time(shot):
    startTime = mc.shot(shot, q=True, startTime=True)
    endTime = mc.shot(shot, q=True, endTime=True)
    return startTime, endTime

def look_thru(cam):
    """ look through this camera """
    mc.lookThru(cam)

def changeFrame(frame):
    mc.currentTime(frame)

def clearSelection():
    mc.select(clear = True)

def addSelection(obj):
    return mc.select(obj, add = True)

def parentNode(obj):
    return mc.listRelatives(obj, p = True)

def get_file_format(shot): 
    return '%s_test.ma' % shot