#!/usr/bin/env python
# -- coding: utf-8 --

import os
import sys
import traceback
from collections import OrderedDict

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir = os.path.dirname(module_path)
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils.widget import project_widget
from rf_utils.widget import entity_browse_widget
from rf_utils import icon
sg = None


class Setting:
    refreshIcon = '%s/icons/refresh.png' % module_dir
    currentIcon = '%s/icons/current_icon.png' % module_dir


class SceneMonitorUi(QtWidgets.QWidget):
    status = QtCore.Signal(str)

    def __init__(self, parent=None):
        super(SceneMonitorUi, self).__init__(parent)
        self.layout = QtWidgets.QVBoxLayout()
        self.sub_layout = QtWidgets.QHBoxLayout()
        self.layout.addLayout(self.sub_layout)
        self.setLayout(self.layout)

        self.setup_widget()
        self.init_signals()
        self.init_functions()

    def setup_widget(self):
        # logo
        self.logo = QtWidgets.QLabel()
        self.logo.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))
        self.layout.insertWidget(0, self.logo)

        # button layout
        self.button_layout = QtWidgets.QHBoxLayout()
        self.layout.insertLayout(1, self.button_layout)
        # spacer
        button_spacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.button_layout.addItem(button_spacer)
        # current button
        self.current_button = QtWidgets.QPushButton()
        self.current_button.setMaximumSize(32, 32)
        self.current_button.setIcon(QtGui.QIcon(Setting.currentIcon))
        self.button_layout.addWidget(self.current_button)
        # refresh button
        self.refresh_button = QtWidgets.QPushButton()
        self.refresh_button.setMaximumSize(32, 32)
        self.refresh_button.setIcon(QtGui.QIcon(Setting.refreshIcon))
        self.button_layout.addWidget(self.refresh_button)

        # navigation bar
        self.nav_layout = QtWidgets.QVBoxLayout()
        self.project_widget = project_widget.SGProjectComboBox(sg=sg)
        self.nav_layout.addWidget(self.project_widget)
        self.sub_layout.addLayout(self.nav_layout)

        """ add sg navigation widget """
        self.sg_nav_widget = entity_browse_widget.SGSceneNavigation(sg=sg, shotFilter=True)
        self.sg_nav_widget.shotExtraFilter = ['sg_shot_type', 'is', 'Shot']
        self.sg_nav_widget.shotWidget.displayAttr = 'sg_shortcode'
        self.sg_nav_widget.shotWidget.set_searchable(True)

        self.nav_layout.addWidget(self.sg_nav_widget)

        # spacer
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.nav_layout.addItem(spacerItem)

        self.nav_layout.setStretch(0, 0)
        self.nav_layout.setStretch(1, 0)
        self.nav_layout.setStretch(2, 2)

    def init_signals(self):
        self.project_widget.projectChanged.connect(self.project_changed)

    def init_functions(self):
        # this will trigger current project to list episode, sequence and shots
        self.project_widget.emit_project_changed()

    def project_changed(self, project):
        self.sg_nav_widget.list_episodes(project)



