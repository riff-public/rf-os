#!/usr/bin/env python
# -- coding: utf-8 --

# v.0.0.1 - Polytag switcher
# v.0.0.2 - Fig bugs
# v.1.0.0 - Add current scene button
# v.1.1.0 - Add storyboard tab
# v.1.1.1 - Fix bug storyboard tab: item data not updating when editting description and status
# v.1.2.0 - Add Brief tab
# v.1.3.0 - Add tag and description editing in media tab
#         - Make AddBriefWin stored last step and task selection
# v.1.3.1 - Fix entity not update in AddBriefWin
#         - Make AddBriefWin disabled when no task available
# v.1.4.0 - Add brief attachment image save
# v.1.4.1 - Add app icon to brief save dialog
# v.1.4.2 - Stop showing omitted shots in media tab
# v.1.5.0 - Add download media button to media tab

_title = 'RF Scene Monitor'
_version = 'v.1.5.0'
_des = ''
uiName = 'SceneMonitorUi'

#Import python modules
import sys
import os
import logging
import getpass
import traceback
from collections import OrderedDict
import os
import json
import pickle

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '{}/core'.format(os.environ.get('RFSCRIPT'))
sys.path.append(core)

# import config
import rf_config as config

from rf_utils import log_utils
from rf_utils.ui import load
# reload(load)
from rf_utils.ui import stylesheet

user = '{}-{}'.format(config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

# maya & module function
from . import ui
from . import tabs
from rf_utils.sg import sg_process
from rf_utils.context import context_info
ui.sg = sg_process.sg

if config.isNuke:
    import nuke
    import nukescripts
elif config.isMaya:
    import maya.cmds as mc

modulePath = sys.modules[__name__].__file__.replace('\\', '/')
moduleDir  = os.path.dirname(modulePath)
appModuleDir = os.path.dirname(moduleDir)

class FullConfig:
    """ name of tabs module """
    activeTab = 'builder'
    allowedTabs = ['builder', 'storyboard', 'colorscript', 'mastershot', 'lookdev', 'media', 'brief']
    tabNameMap = {'builder': 'Render Builder'}

class ViewerConfig:
    activeTab = 'colorscript'
    allowedTabs = ['mastershot', 'colorscript', 'storyboard', 'lookdev', 'media', 'brief']
    tabNameMap = {}

class SceneMonitor(QtWidgets.QMainWindow):
    def __init__(self, tab_config=FullConfig, parent=None):
        #Setup Window
        super(SceneMonitor, self).__init__(parent)
        self.entity = None

        # ui read
        uiFile = '{}/ui.py'.format(moduleDir)
        self.ui = ui.SceneMonitorUi()
        self.w = 1190
        self.h = 685
        self.tab_config = tab_config

        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.resize(self.w, self.h)
        self.setWindowTitle('{} {} {}'.format(_title, _version, _des))
        self.setWindowIcon(QtGui.QIcon('{}/icons/app_icon.png'.format(moduleDir)))
        self.setup_ui()
        self.init_signals()
        self.init_functions()

        self.set_default()

    def closeEvent(self, event):
        '''
        Closes all tab widgets before actually close itself triggering each tab closeEvents
        '''
        for i in xrange(self.ui.tab_widget.count()):
            widget = self.ui.tab_widget.widget(i)
            widget.close()

    def setup_ui(self):
        # set tabs
        self.ui.tab_widget = self.add_tab_widget()
        self.ui.sub_layout.setStretch(0, 1)
        self.ui.sub_layout.setStretch(1, 6)

        # toolTips
        self.ui.current_button.setToolTip('Jump to current scene.')
        self.ui.refresh_button.setToolTip('Refresh current tab.')

    def init_signals(self):
        self.ui.sg_nav_widget.completed.connect(self.shot_selected)
        self.ui.refresh_button.clicked.connect(self.refresh_tab)
        self.ui.current_button.clicked.connect(self.jump_to_current_scene)
        self.ui.tab_widget.currentChanged.connect(self.tab_changed)

    def init_functions(self):
        self.ui.sg_nav_widget.emit_entity()

    def set_default(self):
        # hide current scene button in standalone mode
        if not config.isMaya and not config.isNuke:
            self.ui.current_button.hide()
        else:
            # try to jump to current scene
            self.jump_to_current_scene()

    def jump_to_current_scene(self):
        # try to find context from current Nuke path
        curr_scene_path = None
        if config.isNuke:
            try:
                curr_scene_path = nuke.scriptName()
            except:
                return
        elif config.isMaya:
            try:
                curr_scene_path = mc.file(q=True, sn=True)
            except:
                return

        if curr_scene_path:
            entity = context_info.ContextPathInfo(path=curr_scene_path)

            try:
                # project
                self.ui.project_widget.set_current_item(project=entity.project)
                # ep
                self.ui.sg_nav_widget.episodeWidget.set_current_item(text=entity.episode)
                # seq
                items = [self.ui.sg_nav_widget.sequenceFilter.itemText(a) for a in range(self.ui.sg_nav_widget.sequenceFilter.count())]
                index = items.index(entity.sequence) if entity.sequence in items else 0
                self.ui.sg_nav_widget.sequenceFilter.setCurrentIndex(index)
                # shot
                self.ui.sg_nav_widget.shotWidget.set_current_item(text=entity.name_code)
            except Exception, e:
                pass

    def add_tab_widget(self):
        """ looping for each tabs modules and compare to allowedTab setting,
            add to tab widgets """
        self.ui.tab_widget = QtWidgets.QTabWidget()
        modules = tabs.get_module()
        activeIndex = 0
        self.tabModules = dict()
        self.threadpool = QtCore.QThreadPool()
        self.threadpool.setMaxThreadCount(1)
        tabNames = []

        for tabName in self.tab_config.allowedTabs:
            for index, name in enumerate(modules):
                title = self.tab_config.tabNameMap.get(name) or name.capitalize()
                if name == tabName:
                    func = tabs.get_func(name)
                    reload(func)
                    widget = func.Ui(thread=self.threadpool, parent=self)
                    self.ui.tab_widget.addTab(widget, title)
                    if hasattr(widget, 'refresh'):
                        widget.refresh
                    if name == self.tab_config.activeTab:
                        activeIndex = len(self.tabModules.keys())

                    self.tabModules[name] = widget
        # active tab
        self.ui.tab_widget.setCurrentIndex(activeIndex)
        self.ui.sub_layout.addWidget(self.ui.tab_widget)

        return self.ui.tab_widget

    def shot_selected(self, sg_entity):
        self.entity = None
        if sg_entity:
            """ shot selected, context changed """
            project = sg_entity.get('project').get('name')
            episode = sg_entity.get('sg_episode').get('name')
            sequence = sg_entity.get('sg_sequence_code')
            shot = sg_entity.get('sg_shortcode')
            
            # setup entity 
            context = context_info.Context()
            context.update(
                project=project,
                entityType='scene',
                entity=sg_entity.get('code'))
                # entityParent=sequence,
                # entityCode=shot)
            self.entity = context_info.ContextPathInfo(context=context)

        # set unsync for all tabs
        self.unsync_tabs()
        # refresh current tab 
        self.refresh_tab()

    def refresh_tab(self):
        """ refresh current tab """
        widget = self.ui.tab_widget.currentWidget()
        widget.refresh(self.entity)
        widget.context_sync = True
        logger.debug('Refresh')

    def tab_changed(self): 
        """ tab only refresh if context has changed """
        widget = self.ui.tab_widget.currentWidget()
        if not widget.context_sync: 
            # print 'Refreshing...'
            self.refresh_tab()
        else: 
            # print 'Not refresh'
            logger.debug('Not refresh')
        self.setup_restrict_navigation(widget)

    def setup_restrict_navigation(self, widget):
        if hasattr(widget, 'restrict_navigation'):
            widget.restrict_navigation(self)
        else:
            self.ui.sg_nav_widget.episodeWidget.setEnabled(True)
            self.ui.sg_nav_widget.sequenceFilter.setEnabled(True)
            self.ui.sg_nav_widget.shotWidget.setEnabled(True)

    def unsync_tabs(self): 
        """ unsync all tabs so it will refresh when tab changed """ 
        tab_count = self.ui.tab_widget.count()

        for i in range(tab_count): 
            widget = self.ui.tab_widget.widget(i)
            widget.context_sync = False 

def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = SceneMonitor(tab_config=FullConfig, parent=maya_win.getMayaWindow())
        myApp.show()
    elif config.isNuke:
        from rf_nuke import nuke_win 
        # reload(nuke_win)
        logger.info('Run in Nuke\n')
        nuke_win.deleteUI(uiName)
        myApp = SceneMonitor(tab_config=FullConfig, parent=nuke_win._nuke_main_window())
        myApp.show()
    else:
        logger.info('Run in standalone\n')
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        myApp = SceneMonitor(tab_config=ViewerConfig, parent=None)
        myApp.show()
        stylesheet.set_default(app)
        sys.exit(app.exec_())
    return myApp


if __name__ == '__main__':
    show()
