#!/usr/bin/env python
# -- coding: utf-8 --

import os
import sys
import subprocess
from collections import OrderedDict
from datetime import datetime
from functools import partial
import time
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from Qt import QtCore
from Qt import QtGui
from Qt import QtWidgets

from rf_utils import thread_pool
from rf_utils import user_info
# reload(user_info)


modulePath = sys.modules[__name__].__file__
moduleDir  = os.path.dirname(modulePath)


class Color:
    projectLabel = 'color: rgb(220, 220, 220);'
    entityLabel = 'color: rgb(180, 220, 240);'
    grpLabel = 'color: rgb(160, 160, 160);'
    submitButton = 'background-color: rgb(80, 160, 240);'
    snapButton = 'background-color: rgb(200, 200, 200);'


class Default:
    userIcon = '%s/icons/user.png' % os.path.split(moduleDir)[0]
    refreshIcon = '%s/icons/refresh.png' % os.path.split(moduleDir)[0]
    captureIcon = '%s/icons/snap.png' % os.path.split(moduleDir)[0]


class Ui(QtWidgets.QWidget):
    """docstring for Ui"""
    def __init__(self, thread=None, parent=None):
        super(Ui, self).__init__(parent)
        self.parent = parent
        self.context_sync = False
        self.entity = None
        
         # setup thread
        self.threadpool = QtCore.QThreadPool() if not thread else thread
        self.setup_ui()

    def setup_ui(self):
        self.layout = QtWidgets.QVBoxLayout()
        self.frame = QtWidgets.QFrame(self)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.main_layout = QtWidgets.QVBoxLayout(self.frame)
        self.layout.addWidget(self.frame)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

    def refresh(self, entity):
        """ all command start here """
        self.entity = entity

