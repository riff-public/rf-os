# -*- coding: utf-8 -*-
import os
import sys
import subprocess
from glob import glob
from collections import OrderedDict
from datetime import datetime
from functools import partial
import time
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from Qt import QtCore
from Qt import QtGui
from Qt import QtWidgets

import rf_config as config
from rf_utils import thread_pool
from . import master
from rf_utils.sg import sg_process
from rf_utils.widget import image_widget
from rf_utils.widget import textEdit
from rf_utils.widget.status_widget import Icon
from rf_utils.widget import collapse_widget
from rf_utils import user_info


modulePath = sys.modules[__name__].__file__
moduleDir  = os.path.dirname(modulePath)
appModuleDir = os.path.dirname(moduleDir)

class PartTreeWidget(QtWidgets.QTreeWidget):
    def __init__(self, parent=None):
        super(PartTreeWidget, self).__init__(parent)
        self.setStyleSheet('QTreeWidget::item{padding:2px 0}')

class Ui(master.Ui):
    """docstring for Ui"""
    def __init__(self, thread=None, parent=None):
        super(Ui, self).__init__(thread=thread, parent=parent)
        self.taskname = 'storyboard'
        self.bg_col = (43, 43, 43)
        self.edit_col = (100, 50, 50)

        self.active_font = QtGui.QFont()
        self.active_font.setItalic(False)
        self.active_font.setPointSize(8)

        self.display_font = QtGui.QFont()
        self.display_font.setItalic(True)
        self.display_font.setPointSize(8)

        self.grey_font_palette = QtGui.QPalette()
        self.grey_font_palette.setBrush(QtGui.QPalette.Text, QtGui.QBrush(QtGui.QColor(128, 128, 128)))

        self.white_font_palette = QtGui.QPalette()
        self.white_font_palette.setBrush(QtGui.QPalette.Text, QtGui.QBrush(QtGui.QColor(255, 255, 255)))

        self.player_app = {'Keyframe Pro': self.play_keyframepro, 'RV': self.play_rv}
        self.chosen_app = None

        self.setupUi()
        self.init_signal()
        self.set_default()

    def setupUi(self):
        self.mainSplitter = QtWidgets.QSplitter()
        self.mainSplitter.setOrientation(QtCore.Qt.Horizontal)

        self.partSplitWidget = QtWidgets.QWidget(self.mainSplitter)
        self.viewSplitWidget = QtWidgets.QWidget(self.mainSplitter)
        self.mainSplitter.setSizes([85, 330])

        self.main_layout.addWidget(self.mainSplitter)

        # shotlist layout
        self.part_layout = QtWidgets.QVBoxLayout(self.partSplitWidget)
        self.part_layout.setContentsMargins(6, 0, 6, 0)

        # asset label
        self.info_layout = QtWidgets.QVBoxLayout()
        self.part_layout.addLayout(self.info_layout)

        self.status_layout = QtWidgets.QFormLayout()
        self.status_layout.setLabelAlignment(QtCore.Qt.AlignLeft)
        self.status_layout.setFormAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)
        self.status_layout.setSpacing(3)
        self.info_layout.addLayout(self.status_layout)
        self.creator_value_label = QtWidgets.QLabel()
        self.status_layout.addRow('Creator: ', self.creator_value_label)
        self.date_value_label = QtWidgets.QLabel()
        self.status_layout.addRow('Updated: ', self.date_value_label)

        # asset list widget
        self.part_treeWidget = PartTreeWidget()
        self.part_treeWidget.setColumnCount(2)
        self.part_treeWidget.setSortingEnabled(False)
        self.part_treeWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.part_treeWidget.setHeaderHidden(True)
        self.part_treeWidget.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.part_layout.addWidget(self.part_treeWidget)

        # options
        self.display_option_groupBox = collapse_widget.CollapsibleBox(title='Display options')
        self.part_layout.addWidget(self.display_option_groupBox)
        self.display_option_layout = QtWidgets.QVBoxLayout(self.display_option_groupBox)

        # show episode
        self.filter_layout = QtWidgets.QHBoxLayout()
        self.display_option_layout.addLayout(self.filter_layout)
        self.filter_label = QtWidgets.QLabel('Show')
        self.filter_layout.addWidget(self.filter_label)
        self.filter_comboBox = QtWidgets.QComboBox()
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.filter_comboBox.setSizePolicy(sizePolicy)
        self.filter_layout.addWidget(self.filter_comboBox)
        self.display_option_groupBox.setContentLayout(self.display_option_layout)

        # view layout 
        self.view_layout = QtWidgets.QVBoxLayout(self.viewSplitWidget)
        self.view_layout.setContentsMargins(0, 0, 0, 0)

        # media layout
        self.media_layout = QtWidgets.QHBoxLayout()
        self.image_viewer = image_widget.ImageSlideViewer()
        self.image_viewer.reelWidget.allow_delete = False
        self.image_viewer.reelWidget.setStyleSheet('background-color: rgb{};'.format(self.bg_col))
        self.image_viewer.allow_image_drop = False
        self.image_viewer.drop_add_image = False
        self.image_viewer.paste_add_image = False
        self.image_viewer.show_last_image = False  # show the first image
        self.image_viewer.viewer.setBackgroundBrush(QtGui.QColor(self.bg_col[0], self.bg_col[1], self.bg_col[2]))
        # self.image_viewer.setImage("P:/Hanuman/scene/publ/act1/hnm_act1_q0030g_all/colorscript/main/hero/outputMedia/hnm_act1_q0030g_all_clrscpt_main.hero.jpg")

        self.media_layout.addWidget(self.image_viewer)
        self.view_layout.addLayout(self.media_layout)

        # media button layout
        self.media_button_layout = QtWidgets.QVBoxLayout()
        self.media_button_layout.setSpacing(5)
        # play button
        self.view_media_button = QtWidgets.QPushButton('')
        self.view_media_button.setFixedSize(QtCore.QSize(40, 22))
        self.view_media_button.setIcon(QtGui.QIcon('{}/icons/eye_icon.png'.format(appModuleDir)))
        # dir button
        self.open_media_dir_button = QtWidgets.QPushButton('')
        self.open_media_dir_button.setFixedSize(QtCore.QSize(40, 22))
        self.open_media_dir_button.setIcon(QtGui.QIcon('{}/icons/folder_icon.png'.format(appModuleDir)))
        spacerItem1 = QtWidgets.QSpacerItem(25, 25, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        
        self.media_button_layout.addWidget(self.view_media_button)
        self.media_button_layout.addWidget(self.open_media_dir_button)
        self.media_button_layout.addItem(spacerItem1)
        self.media_layout.addLayout(self.media_button_layout)

        # description layout
        self.description_layout = QtWidgets.QVBoxLayout()
        self.description_layout.setSpacing(3)
        self.view_layout.addLayout(self.description_layout)

        # description button layout
        self.description_button_layout = QtWidgets.QHBoxLayout()
        self.description_label = QtWidgets.QLabel('Description: ')
        spacerItem2 = QtWidgets.QSpacerItem(25, 25, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.edit_description_button = QtWidgets.QPushButton('')
        self.edit_description_button.setCheckable(True)
        self.edit_description_button.setFixedSize(QtCore.QSize(40, 22))
        self.edit_description_button.setIcon(QtGui.QIcon('{}/icons/pencil_icon.png'.format(appModuleDir)))
        self.description_button_layout.addWidget(self.description_label)
        self.description_button_layout.addItem(spacerItem2)
        self.description_button_layout.addWidget(self.edit_description_button)
        self.description_layout.addLayout(self.description_button_layout)

        # description box
        self.description_textEdit = textEdit.TextEditWithPlaceHolder()
        self.description_textEdit.setPlaceholderText('< No description >')
        self.description_textEdit.setWordWrapMode(QtGui.QTextOption.WrapAtWordBoundaryOrAnywhere)
        self.description_textEdit.setTextInteractionFlags(QtCore.Qt.NoTextInteraction)
        self.description_textEdit.setFont(self.display_font)
        self.description_textEdit.setPalette(self.grey_font_palette)
        self.description_layout.addWidget(self.description_textEdit)

        # setup sizes
        self.media_layout.setStretch(0, 15)
        self.media_layout.setStretch(1, 1)
        self.view_layout.setStretch(0, 10)
        self.view_layout.setStretch(1, 2)
        self.description_layout.setStretch(0, 1)
        self.description_layout.setStretch(1, 3)

        # toolTips
        self.view_media_button.setToolTip('Open image in media viewer')
        self.open_media_dir_button.setToolTip('Open file explorer at image directory')
        self.edit_description_button.setToolTip('Edit storyboard description (Supervisor/Producer only)')

    def restrict_navigation(self, app):
        app.ui.sg_nav_widget.episodeWidget.setEnabled(False)
        app.ui.sg_nav_widget.sequenceFilter.setEnabled(False)
        app.ui.sg_nav_widget.shotWidget.setEnabled(False)

    def set_default(self):
        self.part_treeWidget.clear()
        self.reset_info()

        # only allow description edit button for sup, producer, admin
        user = user_info.User()
        if user.is_producer() or user.is_admin() or user.is_supervisor():
            self.edit_description_button.setVisible(True)
        else:
            self.edit_description_button.setVisible(False)

    def init_signal(self):
        self.part_treeWidget.currentItemChanged.connect(self.update_view)
        self.part_treeWidget.customContextMenuRequested.connect(self.part_right_clicked)
        self.edit_description_button.toggled.connect(self.edit_description_toggled)
        self.description_textEdit.editorLostFocus.connect(lambda: self.disable_description_edit(force=False))
        self.view_media_button.clicked.connect(self.view_media)
        self.open_media_dir_button.clicked.connect(self.open_media_dir)
        self.filter_comboBox.currentIndexChanged.connect(self.episode_filter_changed)
        self.image_viewer.reelWidget.itemDoubleClicked.connect(self.doubleclick_view_media)

    def doubleclick_view_media(self, item_data):
        if isinstance(item_data, dict) and os.path.exists(item_data):
            self.launch_viewer_app(paths=[item_data])

    def view_media(self):
        currentItems = self.image_viewer.selected_items()
        if not currentItems:
            self.error_no_selection('View Media', 'Please select at least 1 image!')
            return
        media_paths = []
        for item in currentItems:
            data = item.data(QtCore.Qt.UserRole)
            if os.path.exists(data):
                media_paths.append(data)
        if media_paths:
            self.launch_viewer_app(paths=media_paths)

    def launch_viewer_app(self, paths):
        mods = QtWidgets.QApplication.keyboardModifiers()
        if not self.chosen_app or mods & QtCore.Qt.ControlModifier:
            # pop up user to choose application to play media
            qmsgBox = QtWidgets.QMessageBox(self)
            # set tile, icon
            qmsgBox.setWindowTitle('View Media')
            qmsgBox.setIcon(QtWidgets.QMessageBox.Question)

            # set text
            text = 'Open media with?'
            qmsgBox.setText(text)

            # set detailed text
            detailedText = 'Playlist: \n- '
            detailedText += '\n- '.join([os.path.basename(path) for path in paths])
            qmsgBox.setDetailedText(detailedText)

            # add buttons
            for app_name in self.player_app:
                qmsgBox.addButton(app_name, QtWidgets.QMessageBox.AcceptRole)
            cancel_button = qmsgBox.addButton('Cancel', QtWidgets.QMessageBox.RejectRole)
            qmsgBox.exec_()
            clicked_button = qmsgBox.clickedButton()
            if clicked_button != cancel_button:
                app_name = clicked_button.text()
                self.chosen_app = app_name
                func = self.player_app[app_name]
                func(paths)
        else:
            func = self.player_app[self.chosen_app]
            func(paths)

    def play_keyframepro(self, paths):
        app_path = config.Software.pythonwPath
        if not os.path.exists(app_path):
            print 'Cannot find application: {}'.foramt(app_path)
            return
        script_path = '{}/rf_utils/keyframepro_utils.py'.format(config.Env.core)
        input_paths = ['-i'] + paths
        commands = [app_path, script_path] + input_paths
        kfp = subprocess.Popen(commands, 
                    stdout=subprocess.PIPE)

    def play_rv(self, paths):
        app_path = config.Software.rv
        if not os.path.exists(app_path):
            print 'Cannot find application: {}'.foramt(app_path)
            return
        commands = [app_path] + paths
        subprocess.Popen(commands)

    def open_media_dir(self):
        current_path = self.image_viewer.current_image_path()
        if not current_path or not os.path.exists(current_path):
            return
        dir_name = os.path.dirname(current_path)
        subprocess.Popen(r'explorer {}'.format(dir_name.replace('/', '\\')))

    def refresh(self, entity): 
        super(Ui, self).refresh(entity)
        self.part_treeWidget.clear()
        self.reset_info()
        self.reset_view()
        if not entity:
            return
        # self.entity inherit from parent class
        self.thread_fetch_data(self.entity)

    def thread_fetch_data(self, entity): 
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        worker = thread_pool.Worker(self.fetch_data, entity)
        worker.signals.result.connect(self.list_parts)
        self.threadpool.start(worker)

    def fetch_data(self, entity): 
        project_name = entity.project
        step_name = self.taskname
        filters = [['sg_shot_type', 'is', 'Preproduction']]
        prepro_shots = sg_process.get_all_shots(project_name, filters=filters)
        tasks = sg_process.list_tasks(project_name, step_name, entities=[])
        versions = sg_process.get_versions_from_entities_tasks(entities=prepro_shots, 
                                                        tasks=[t.get('content') for t in tasks],
                                                        extraFields=['sg_modified_by', 'updated_at'])

        part_id = dict([(s.get('id'), s) for s in prepro_shots])
        board_data = OrderedDict()  # {shot_name: {'shot':..., 'tasks': [...]}}
        for task in tasks:
            task_entity = task.get('entity')
            if task_entity and task_entity.get('id') in part_id:
                shot = part_id[task_entity.get('id')]
                shot_id = shot.get('id')
                shot_name = shot.get('code')
                taskname = task.get('content')
                
                task_versions = []
                remaining_versions = []
                task_id = task.get('id')
                for version in versions:
                    if version['entity']['id'] == shot_id and version['sg_task']['id'] == task_id:
                        # decode Thai
                        desc_text = version.get('description')
                        if desc_text:
                            desc_text = desc_text.decode(encoding='UTF-8', errors='strict')
                            version['description'] = desc_text
                        task_versions.append(version)
                    else:
                        remaining_versions.append(version)
                versions = remaining_versions

                if task_versions:
                    task['versions'] = task_versions
                    if shot_name not in board_data:
                        board_data[shot_name] = {'shot': shot, 'tasks': []}
                    board_data[shot_name]['tasks'].append(task)

        return board_data

    def list_parts(self, board_data):
        self.part_treeWidget.clear()
        self.filter_comboBox.clear()

        # print board_data
        episode_names = set()
        for shot_name in sorted(board_data.keys()):
            data = board_data[shot_name]
            shot = data['shot']
            tasks = data['tasks']

            # episode names
            ep_name = shot['sg_episode'].get('name')
            episode_names.add(ep_name)

            # part item
            root_item = QtWidgets.QTreeWidgetItem(self.part_treeWidget)
            root_item.setText(0, shot.get('code'))
            root_item.setData(QtCore.Qt.UserRole, 0, shot)

            # create process item
            for task in tasks:
                # update 'current' key to data
                task.update({'current': len(task['versions'])-1})
                # create new treeWidget item and set data
                item = QtWidgets.QTreeWidgetItem(root_item)
                item.setData(QtCore.Qt.UserRole, 0, task)
                # setup the item
                self.setup_task_item(item=item, task=task)

        self.filter_comboBox.addItems(['All'] + sorted(list(episode_names)))
        QtWidgets.QApplication.restoreOverrideCursor()

    def setup_task_item(self, item, task):
        item.setText(0, task.get('content'))
        
        # set status icon
        index = task['current']
        selected_version = task['versions'][index]
        status = selected_version['sg_status_list']
        icon = QtGui.QIcon('{0}/{1}'.format(Icon.path, Icon.statusMap[status]['icon']))
        item.setIcon(0, icon)

    def episode_filter_changed(self):
        sels = [s for s in self.part_treeWidget.selectedItems() if s.parent()]
        self.apply_filter()
        if sels:
            curr_item = sels[0]
            parent_item = curr_item.parent()
            if parent_item.isHidden():
                curr_item.setSelected(False)
                self.reset_info()
                self.reset_view()

    def apply_filter(self):
        selected_filter = self.filter_comboBox.currentText()
        rootItem = self.part_treeWidget.invisibleRootItem()
        for i in range(rootItem.childCount()):
            item = rootItem.child(i)
            shot = item.data(QtCore.Qt.UserRole, 0)
            hidden = False
            if selected_filter != 'All':
                ep_name = shot['sg_episode'].get('name')
                if ep_name != selected_filter:
                    hidden = True
            item.setHidden(hidden)

    def part_right_clicked(self, pos):
        item = self.part_treeWidget.itemAt(pos)
        # right clicked on an item
        if item:
            shot_item = item.parent()
            # allow right click only on child item (board item)
            if shot_item:
                rightClickMenu = QtWidgets.QMenu(self)

                task_info = item.data(QtCore.Qt.UserRole, 0)
                index = task_info['current']
                versions = task_info['versions']
                version_actions = []
                for i, version in enumerate(versions):
                    action = QtWidgets.QAction(version['code'], self)
                    action.setCheckable(True)
                    if i == index:  # set checked for current index version
                        action.setChecked(True)
                    action.triggered.connect(partial(self.update_board_version, item, i))
                    version_actions.append(action)
                # add actions
                for action in version_actions:
                    rightClickMenu.addAction(action)

                # status widget for producer and only with selected asset only
                status_actions = []
                user = user_info.User()
                has_right = True if user.is_producer() or user.is_admin() or user.is_supervisor() else False
                itemData = item.data(QtCore.Qt.UserRole, 0)
                if has_right:
                    # add separator
                    rightClickMenu.addSeparator()
                    # add submenu
                    status_menu = QtWidgets.QMenu('Set status...',  self)
                    rightClickMenu.addMenu(status_menu)
                    
                    isLatest = True if index == len(versions)-1 else False
                    task_status = [s for s in Icon.statusMap.keys() if s in Icon.availableMaps['Task']]
                    version_status = [s for s in Icon.statusMap.keys() if s in Icon.availableMaps['Version']]
                    available_status = task_status if isLatest else version_status
                    # add status actions
                    for status in available_status:
                        action = QtWidgets.QAction(Icon.statusMap[status]['display'], self)
                        icon = QtGui.QIcon('{0}/{1}'.format(Icon.path, Icon.statusMap[status]['icon']))
                        action.setIcon(icon)
                        
                        action.triggered.connect(partial(self.update_status, task_info, status, isLatest, index, item))
                        status_actions.append(action)
                    # add actions
                    for status_action in status_actions:
                        status_menu.addAction(status_action)
                
                if version_actions or status_actions:
                    rightClickMenu.exec_(self.part_treeWidget.mapToGlobal(pos))

    def update_status(self, task_info, newStatus, isLatest, version_index, item):
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        version = task_info['versions'][version_index]
        versionId = version.get('id')
        # print 'versionId', versionId
        # set version status, if newStatus is not 'apr', 'rev' or 'fix', use 'wip'
        if newStatus in Icon.availableMaps['Version']:
            versionStatus = newStatus
        else:
            versionStatus = 'wip'
        sg_process.set_version_status(versionId, versionStatus)

        # set Task status if the version is latest version
        task = version.get('sg_task')
        entity = version.get('entity')
        taskname = task.get('name')
        if isLatest:
            
            taskId = task.get('id')
            # print 'isLatest', isLatest, taskId
            sg_process.set_task_status(taskId, newStatus)

        QtWidgets.QApplication.restoreOverrideCursor()

        # update UI
        new_versions = sg_process.get_versions_from_entity(entity, taskname, extraFields=['sg_status_list'])
        new_current_version = new_versions[version_index]
        if new_current_version.get('sg_status_list') == versionStatus:
            # decode thai description
            desc_text = new_current_version.get('description')
            if desc_text:
                desc_text = desc_text.decode(encoding='UTF-8', errors='strict')
                new_current_version['description'] = desc_text
            # update item data
            task_info['versions'][version_index] = new_current_version
            item.setData(QtCore.Qt.UserRole, 0, task_info)
            self.setup_task_item(item=item, task=task_info)
            self.update_info(task_info)

    def update_view(self, *args, **kwargs):
        self.reset_info()
        self.reset_view()

        item = self.part_treeWidget.currentItem()
        shot_item = item.parent()
        if not item or not shot_item:
            return

        # get item data
        task_info = item.data(QtCore.Qt.UserRole, 0)

        self.update_info(data=task_info)

        # show
        versions = task_info.get('versions')
        if versions:
            QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
            current_index = task_info.get('current')
            curr_version = versions[current_index]
            # load images
            media_path = curr_version['sg_path_to_movie']
            if media_path:
                image_paths = glob(media_path.replace('#', '*'))  # replace with * to use glob
                if image_paths:
                    self.image_viewer.add_images(image_paths)
            QtWidgets.QApplication.restoreOverrideCursor()

    def update_info(self, data):
        if not data['versions']:
            return

        index = data['current']
        version = data['versions'][index]
        # creator
        user = version.get('sg_modified_by') if version.get('sg_modified_by') else version.get('user')
        if user:
            user = user.get('name')
            self.creator_value_label.setFont(self.active_font)
            self.creator_value_label.setStyleSheet('color: white')
            self.creator_value_label.setText(user)
        # date
        date = version.get('updated_at') if version.get('updated_at') else version.get('created_at')
        if date:
            self.date_value_label.setFont(self.active_font)
            self.date_value_label.setStyleSheet('color: white')
            self.date_value_label.setText(str(datetime.strftime(date, '%y/%m/%d-%H:%M')))
        # note
        description_text = version.get('description')
        if description_text:
            self.description_textEdit.setPlainText(description_text)

    def update_board_version(self, item, index):
        task_info = item.data(QtCore.Qt.UserRole, 0)
        task_info['current'] = index  # update the current index
        item.setData(QtCore.Qt.UserRole, 0, task_info)

        # update
        self.setup_task_item(item=item, task=task_info)
        self.update_info(task_info)
        self.update_view()

    def reset_view(self):
        self.image_viewer.clear()

    def reset_info(self):
        self.description_textEdit.clear()

        self.creator_value_label.setFont(self.display_font)
        self.creator_value_label.setStyleSheet('color: grey')
        self.creator_value_label.setText('N/A')

        self.date_value_label.setFont(self.display_font)
        self.date_value_label.setStyleSheet('color: grey')
        self.date_value_label.setText('N/A')

    def disable_description_edit(self, force=True):
        # set toggle off
        self.edit_description_button.blockSignals(True) if force else None
        self.edit_description_button.setChecked(False)
        self.edit_description_button.blockSignals(False) if force else None

        # set non editable
        self.description_textEdit.setTextInteractionFlags(QtCore.Qt.NoTextInteraction)
        # set look
        self.description_textEdit.setFont(self.display_font)
        self.description_textEdit.setPalette(self.grey_font_palette)
        self.description_textEdit.setStyleSheet('background-color: rgb{};'.format(self.bg_col))

    def enable_description_edit(self):
        # set editable
        self.description_textEdit.setTextInteractionFlags(QtCore.Qt.TextEditorInteraction)
        # set look
        self.description_textEdit.setPalette(self.white_font_palette)
        self.description_textEdit.setFont(self.active_font)
        self.description_textEdit.setFocus()
        self.description_textEdit.moveCursor(QtGui.QTextCursor.End)
        self.description_textEdit.setStyleSheet('background-color: rgb{};'.format(self.edit_col))

    def edit_description_toggled(self):
        item = self.part_treeWidget.currentItem()
        shot_item = item.parent()
        if not item or not shot_item:
            self.error_no_selection('Edit Description', 'Please select a Storyboard!')
            self.disable_description_edit()
            return

        board_data = item.data(QtCore.Qt.UserRole, 0)
        index = board_data['current']
        versions = board_data['versions']
        version_info = versions[index]

        # start editing
        if self.edit_description_button.isChecked():
            self.enable_description_edit()
        else: # finish editing
            current_text = self.description_textEdit.toPlainText()
            version_desc = version_info['description'] if version_info['description'] else ''
            if current_text != version_desc:
                # popup question to user
                qmsgBox = QtWidgets.QMessageBox(self)
                qmsgBox.setText('Edit storyboard description?')
                qmsgBox.setWindowTitle('Confirm')
                qmsgBox.setIcon(QtWidgets.QMessageBox.Question)
                qmsgBox.addButton('  Yes  ', QtWidgets.QMessageBox.AcceptRole)
                qmsgBox.addButton('  No  ', QtWidgets.QMessageBox.RejectRole)
                answer = qmsgBox.exec_()
                if answer == 0: 
                    try:
                        # update shotgun data
                        update_description(taskid=version_info['id'], description=current_text)
                        
                        # also update local data stored inside listwidget item
                        version_info['description'] = current_text
                        board_data['versions'][index] = version_info
                        item.setData(QtCore.Qt.UserRole, 0, board_data)
                    except Exception, e:
                        self.description_textEdit.setPlainText(version_desc)
                else:
                    # set text back to previous text
                    self.description_textEdit.setPlainText(version_desc)
            # disable edit UI
            self.disable_description_edit()

    def error_no_selection(self, title, message):
        # pop up user to choose application to play media
        qmsgBox = QtWidgets.QMessageBox(self)
        # set tile, icon
        qmsgBox.setWindowTitle(title)
        qmsgBox.setText(message)
        qmsgBox.setIcon(QtWidgets.QMessageBox.Critical)

        # add button
        qmsgBox.addButton('  OK  ', QtWidgets.QMessageBox.AcceptRole)
        qmsgBox.exec_()

def update_description(taskid, description): 
    """ this function update description from task id 
    get task id from shot[self.taskname]['id']
    """ 
    data = {'description': description}
    return sg_process.sg.update('Version', taskid, data)
