#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import subprocess
import glob
from collections import OrderedDict
from datetime import datetime
from functools import partial
import time
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from Qt import QtCore
from Qt import QtGui
from Qt import QtWidgets

from rf_utils import thread_pool
from rf_utils import register_render
# reload(register_render)
from . import master
# reload(master)

isNuke = True
try:
    import nuke
    import nukescripts
except ImportError:
    isNuke = False

_title = 'Render Builder'
modulePath = sys.modules[__name__].__file__
moduleDir  = os.path.dirname(modulePath)

class CustomDelegate(QtWidgets.QStyledItemDelegate):

    def sizeHint(self, option, index):
        default = QtWidgets.QStyledItemDelegate.sizeHint(self, option, index)
        return QtCore.QSize(default.width(), default.height() + 12)

    def createEditor(self, parent, option, index):
        editor = QtWidgetsQTextEdit(parent)
        return editor

    def setEditorData(self, editor, index):
        text = index.model().data(index, QtCore.Qt.DisplayRole).toString()
        editor.setText(text)

    def setModelData(self, editor, model, index):
        model.setData(index, QtCore.QVariant(editor.toPlainText()))

    # def paint(self, painter, option, index):
    #     option.state = QtWidgets.QStyle.State_None
    #     super(CustomDelegate, self).paint(painter, option, index)

class CustomTreeView(QtWidgets.QTreeWidget):

    def mousePressEvent(self, event):
        clicked_item = self.itemAt(event.pos())
        if not clicked_item:
            self.clearSelection()
        else:
            QtWidgets.QTreeView.mousePressEvent(self, event)
            # self.setCurrentItem(clicked_item)
        self.clearFocus()

class CustomPlainTextEdit(QtWidgets.QPlainTextEdit):
    def mousePressEvent(self, event):
        treeWidget = self.parent().parentWidget()
        pPos = self.mapToParent(event.pos())
        item = treeWidget.itemAt(pPos)
        treeWidget.setCurrentItem(item)

class Ui(master.Ui):
    """ Comp builder UI class """
    def __init__(self, thread=None, parent=None):
        super(Ui, self).__init__(thread=thread, parent=parent)
        self.grey = QtGui.QColor(128, 128, 128)
        self.light_grey = QtGui.QColor(80, 80, 80)
        self.dark_grey = QtGui.QColor(40, 40, 40)
        self.black = QtGui.QColor(0, 0, 0)
        self.red = QtGui.QColor(255, 0, 25)
        self.default_font_color = QtGui.QColor(255, 255, 255)

        self.cryptomatte = 'Cryptomatte'
        self.color_code_dict = {'Shadow': QtGui.QColor(48, 46, 115),
                                'Beauty': QtGui.QColor(84, 43, 66),
                                'Light': QtGui.QColor(115, 103, 46),
                                'Outline': QtGui.QColor(20, 20, 20),
                                'ID': QtGui.QColor(57, 115, 46)}

        self.setupUi()
        self.init_signal()
        self.init_callbacks()

    def init_callbacks(self):
        if not isNuke:
            return
        nuke.addOnDestroy(self.delete_node_callback, nodeClass='Read')

    def closeEvent(self, event):
        if not isNuke:
            return
        nuke.removeOnDestroy(self.delete_node_callback, nodeClass='Read')

    def delete_node_callback(self):
        if not isNuke:
            return
        deleted_node = nuke.thisNode()
        rootItem = self.treeWidget.invisibleRootItem()
        for i in xrange(rootItem.childCount()):
            item = rootItem.child(i)
            del_node_items = []
            for i in xrange(item.childCount()):
                node_item = item.child(i)
                node = node_item.data(QtCore.Qt.UserRole, 0)
                if node == deleted_node:
                    del_node_items.append(node_item)
            for node_item in del_node_items:
                node_item_index = item.indexOfChild(node_item)
                item.takeChild(node_item_index)
            self.check_outdated_version(item=item)

    def setupUi(self):
        # --- top layout
        self.top_layout = QtWidgets.QHBoxLayout()
        self.top_layout.setContentsMargins(5, 5, 5, 5)
        self.main_layout.addLayout(self.top_layout)

        # filter label
        self.filter_label = QtWidgets.QLabel('Filter: ')
        self.top_layout.addWidget(self.filter_label)
        # filter line edit
        self.filter_lineEdit = QtWidgets.QLineEdit()
        self.filter_lineEdit.setMinimumHeight(25)
        self.filter_lineEdit.setMaximumSize(QtCore.QSize(200, 30))
        self.filter_lineEdit.setPlaceholderText('<Render pass name>')
        self.top_layout.addWidget(self.filter_lineEdit)

        # spacer
        top_spacerItem1 = QtWidgets.QSpacerItem(0, 0, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.top_layout.addItem(top_spacerItem1)

        # buttons
        self.buildAll_button = QtWidgets.QPushButton('Build all')
        self.top_layout.addWidget(self.buildAll_button)
        self.updateAll_button = QtWidgets.QPushButton('Update all to latest')
        self.top_layout.addWidget(self.updateAll_button)

        # ---- tree widget
        self.treeWidget = CustomTreeView()
        self.main_layout.addWidget(self.treeWidget)
        # set custom delegate to fix item size
        delegate = CustomDelegate()
        self.treeWidget.setItemDelegate(delegate)

        # adjust scroll
        # self.treeWidget.setVerticalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
        # self.treeWidget.verticalScrollBar().setSingleStep(1)

        # set background
        black_palette = QtGui.QPalette()
        black_palette.setBrush(QtGui.QPalette.Base, QtGui.QBrush(self.light_grey))
        self.treeWidget.setPalette(black_palette)

        # set sorting
        self.treeWidget.setSortingEnabled(True)

        # set style sheet
        # self.treeWidget.setStyleSheet("QTreeWidget: {outline: 0;}")
        # self.treeWidget.setStyleSheet('QTreeView::branch{background: white;}')

        # set header
        header = self.treeWidget.headerItem()

        header.setTextAlignment(0, QtCore.Qt.AlignCenter)
        header.setTextAlignment(1, QtCore.Qt.AlignCenter)
        header.setTextAlignment(2, QtCore.Qt.AlignCenter)
        header.setTextAlignment(3, QtCore.Qt.AlignCenter)
        header.setTextAlignment(4, QtCore.Qt.AlignCenter)
        header.setTextAlignment(5, QtCore.Qt.AlignCenter)
        header.setTextAlignment(6, QtCore.Qt.AlignCenter)
        header.setTextAlignment(7, QtCore.Qt.AlignCenter)

        header.setText(0, "Pass name")
        header.setText(1, "Version")
        header.setText(2, "Creator")
        header.setText(3, "Step")
        header.setText(4, "Create date")
        header.setText(5, "Modified")
        header.setText(6, "Action")
        header.setText(7, "History")
        
        self.treeWidget.setColumnWidth(0, 210)
        self.treeWidget.setColumnWidth(1, 60)
        self.treeWidget.setColumnWidth(2, 70)
        self.treeWidget.setColumnWidth(3, 50)
        self.treeWidget.setColumnWidth(4, 85)
        self.treeWidget.setColumnWidth(5, 60)
        self.treeWidget.setColumnWidth(6, 55)
        self.treeWidget.setColumnWidth(7, 200)
        
    def init_signal(self):
        self.filter_lineEdit.textChanged.connect(self.filter_edited)
        self.buildAll_button.clicked.connect(self.build_all)
        self.updateAll_button.clicked.connect(self.update_all)
        self.treeWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.treeWidget.customContextMenuRequested.connect(self.item_right_clicked)
        self.treeWidget.itemDoubleClicked.connect(partial(self.item_double_clicked))

    def item_double_clicked(self, item, column):
        item_parent = item.parent()
        # double click on top level item, select all read nodes
        if not item_parent:
            child_items = []
            for i in xrange(item.childCount()):
                child_item = item.child(i)
                child_items.append(child_item)
            self.select_readnode(node_items=child_items, frame=True)
            item.setExpanded(False)
        else:  # else it's a child item, select only that read node
            self.select_readnode(node_items=[item], frame=True)

    def build_all(self):
        rootItem = self.treeWidget.invisibleRootItem()
        new_node_items = []
        for i in xrange(rootItem.childCount()):
            item = rootItem.child(i)
            if not item.isHidden():
                nuke_node, node_item = self.new_readnode(item=item)
                new_node_items.append(node_item)
        
        self.select_readnode(node_items=new_node_items)

    def update_all(self):
        rootItem = self.treeWidget.invisibleRootItem()
        for i in xrange(rootItem.childCount()):
            item = rootItem.child(i)
            if not item.isHidden():
                self.update_item_readnodes(item=item, latest_version=True)

    def filter_edited(self):
        filter_text = self.filter_lineEdit.text().lower()
        rootItem = self.treeWidget.invisibleRootItem()
        for i in xrange(rootItem.childCount()):
            item = rootItem.child(i)
            name = item.text(0).lower()
            if filter_text not in name:
                item.setHidden(True)
            else:
                item.setHidden(False)

    def refresh(self, entity): 
        # this will inherit from master 
        super(Ui, self).refresh(entity)

        # clear old items
        self.treeWidget.clear()
        if not entity:
            return
            
        # read render register yml
        reg = register_render.Register(ContextPathInfo=entity)
        render_data = reg.get_data()

        # populate tree widget from data
        dark_grey_brush = QtGui.QBrush(self.dark_grey)
        grey_palette = QtGui.QPalette()
        grey_palette.setBrush(QtGui.QPalette.Text, QtGui.QBrush(self.grey))
        his_font = QtGui.QFont()
        his_font.setItalic(True)
        his_font.setPointSize(8)

        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        for elem_name, elem in render_data.elements.iteritems():
            # new element item
            elem_item = QtWidgets.QTreeWidgetItem(self.treeWidget)
            elem_item.setData(QtCore.Qt.UserRole, 0, elem)

            # set background for pass items
            elem_item.setBackground(0, dark_grey_brush)
            elem_item.setBackground(1, dark_grey_brush)
            elem_item.setBackground(2, dark_grey_brush)
            elem_item.setBackground(3, dark_grey_brush)
            elem_item.setBackground(4, dark_grey_brush)
            elem_item.setBackground(5, dark_grey_brush)
            elem_item.setBackground(6, dark_grey_brush)
            elem_item.setBackground(7, dark_grey_brush)

            # set alignment
            elem_item.setTextAlignment(0, QtCore.Qt.AlignCenter)
            elem_item.setTextAlignment(1, QtCore.Qt.AlignCenter)
            elem_item.setTextAlignment(2, QtCore.Qt.AlignCenter)
            elem_item.setTextAlignment(3, QtCore.Qt.AlignCenter)
            elem_item.setTextAlignment(4, QtCore.Qt.AlignCenter)
            elem_item.setTextAlignment(5, QtCore.Qt.AlignCenter)
            elem_item.setTextAlignment(6, QtCore.Qt.AlignCenter)

            # ----- set render pass name
            elem_item.setText(0, elem_name)
            # font
            pass_font = QtGui.QFont()
            pass_font.setPointSize(8)
            pass_font.setLetterSpacing(QtGui.QFont.AbsoluteSpacing, 1)
            # pass_font.setBold(True)
            elem_item.setFont(0, pass_font)
            # background color
            brush = elem_item.background(0)

            # rainbow color for crypetomatte
            if elem_name.endswith(self.cryptomatte):
                gradient = QtGui.QLinearGradient()
                gradient.setStart(0, 0)
                gradient.setFinalStop(190, 0)
                gradient.setColorAt(0, QtGui.QColor(77, 38, 38))
                gradient.setColorAt(0.1667, QtGui.QColor(77, 77, 38))
                gradient.setColorAt(0.3334, QtGui.QColor(37, 77, 37))
                gradient.setColorAt(0.5, QtGui.QColor(37, 77, 77))
                gradient.setColorAt(0.6667, QtGui.QColor(37, 37, 77))
                gradient.setColorAt(0.8335, QtGui.QColor(77, 37, 77))
                gradient.setColorAt(1, self.dark_grey)

                brush = QtGui.QBrush(gradient)
                elem_item.setBackground(0, brush)
            else:
                for key, color in self.color_code_dict.iteritems():
                    if key in elem_name:
                        
                        gradient = QtGui.QLinearGradient()
                        gradient.setStart(0, 0)
                        gradient.setFinalStop(192, 0)
                        gradient.setColorAt(0, color.lighter(f=135))
                        gradient.setColorAt(0.334, color)
                        gradient.setColorAt(1, self.dark_grey)

                        brush = QtGui.QBrush(gradient)
                        elem_item.setBackground(0, brush)
                        break

            # populate versions to combobox
            ver_widget = QtWidgets.QWidget(self.treeWidget)
            ver_layout = QtWidgets.QVBoxLayout(ver_widget)
            ver_layout.setContentsMargins(0, 0, 0, 0)
            # spaceer1
            ver_spacerItem1 = QtWidgets.QSpacerItem(0, 0, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
            ver_layout.addItem(ver_spacerItem1)
            # combobox
            ver_combobox = QtWidgets.QComboBox(ver_widget)
            ver_combobox.setObjectName("version_comboBox")
            ver_combobox.setMaximumSize(QtCore.QSize(80, 30))
            ver_combobox.setFocusPolicy(QtCore.Qt.NoFocus)
            ver_combobox.addItems(elem.get_version_names())
            ver_combobox.setCurrentIndex(ver_combobox.count() - 1)
            ver_layout.addWidget(ver_combobox)
            # spacer2
            ver_spacerItem2 = QtWidgets.QSpacerItem(0, 0, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
            ver_layout.addItem(ver_spacerItem2)
            # add to treeWidget
            self.treeWidget.setItemWidget(elem_item, 1, ver_widget)
            
            # ---- add New button
            button_widget = QtWidgets.QWidget(self.treeWidget)
            button_layout = QtWidgets.QVBoxLayout(button_widget)
            button_layout.setContentsMargins(0, 0, 0, 0)
            # spacer1
            button_spacerItem1 = QtWidgets.QSpacerItem(0, 0, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
            button_layout.addItem(button_spacerItem1)
            # button 
            add_button = QtWidgets.QPushButton("New")
            add_button.clicked.connect(partial(self.new_readnode, elem_item))
            add_button.setMaximumSize(QtCore.QSize(100, 30))
            button_layout.addWidget(add_button)
            # spacer2
            button_spacerItem2 = QtWidgets.QSpacerItem(0, 0, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
            button_layout.addItem(button_spacerItem2)
            # add to treeWidget
            self.treeWidget.setItemWidget(elem_item, 6, button_widget)

            # ----- history section
            his_textEdit = CustomPlainTextEdit(parent=self.treeWidget)
            his_textEdit.setObjectName("history_textEdit")
            his_textEdit.viewport().setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))  # get rid of "I" cursor when mouse hover 
            his_textEdit.setMaximumHeight(60)
            his_textEdit.setStyleSheet("* { background-color: rgba(0, 0, 0, 0); }")
            his_textEdit.setWordWrapMode(QtGui.QTextOption.WrapAtWordBoundaryOrAnywhere)
            his_textEdit.setTextInteractionFlags(QtCore.Qt.NoTextInteraction)
            his_textEdit.setPalette(grey_palette)
            his_textEdit.setFont(his_font)

            self.treeWidget.setItemWidget(elem_item, 7, his_textEdit)
            
            # ----- refresh info
            # update tree
            self.update_version_data(item=elem_item)

            # update sub tree
            self.update_scene_item_data(item=elem_item)

            # init signal
            # ver_combobox.currentIndexChanged.connect(partial(self.update_version_data, elem_item))

        # call filter edited in case there're left over filter text
        self.filter_edited()
        QtWidgets.QApplication.restoreOverrideCursor()

    def get_current_render_version(self, item):
        elem = item.data(QtCore.Qt.UserRole, 0)
        ver_widget = self.treeWidget.itemWidget(item, 1)
        ver_combobox = ver_widget.findChild(QtWidgets.QComboBox, 'version_comboBox')
        version_name = ver_combobox.currentText()

        render_version = elem.get_version_by_name(version_name)
        return render_version

    def update_version_data(self, item, *args):
        render_version = self.get_current_render_version(item=item)

        # set creator
        item.setText(2, render_version.creator)
        # set step
        item.setText(3, render_version.step)
        # set create date
        item.setText(4, '\n'.join(render_version.create_date.split('-')))
        # set modified
        bool_dict = {True: 'Yes', False: 'No'}
        item.setText(5, bool_dict[render_version.overwritten])
        # set history
        history_texts = []
        for his in render_version.histories:
            comment_header = his['modified_by'] + ' ' + his['create_date'] + ':' 
            history_texts.append(comment_header + ' ' + his['comment'])
        history_text = '\n'.join(history_texts)
        his_textEdit = self.treeWidget.itemWidget(item, 7)
        his_textEdit.setPlainText(history_text)

    def get_latest_version(self, item):
        elem = item.data(QtCore.Qt.UserRole, 0)
        all_versions = elem.get_version_names()
        if all_versions:
            latest_version = all_versions[-1]
        else:
            latest_version = 'v001'
        return latest_version

    def update_scene_item_data(self, item):
        if not isNuke:
            return
        # remove all children first
        item.takeChildren()
        render_version = self.get_current_render_version(item=item)

        version_path = render_version.path.split('/{}/'.format(render_version.name))[0]
        for node in nuke.allNodes(filter='Read'):
            filePath = node['file'].value()
            # found a read node pointing to this path
            if filePath.startswith(version_path +  '/'): # needs to add / to the ends so "SET_FG_Beauty_Cryptomatte" is not startswith "SET_FG_Beauty"
                # print node.name(), filePath, version_path
                node_item, curr_version = self.create_node_item(node=node, item=item, version_path=version_path)

        self.check_outdated_version(item=item)

    def check_outdated_version(self, item):
        latest_version = self.get_latest_version(item=item)
        has_outdated = False
        for i in xrange(item.childCount()):
            node_item = item.child(i)
            curr_ver_text = node_item.text(1)
            if curr_ver_text != latest_version:
                has_outdated = True
                break

        ver_widget = self.treeWidget.itemWidget(item, 1)
        ver_combobox = ver_widget.findChild(QtWidgets.QComboBox, 'version_comboBox')
        if has_outdated:
            ver_combobox.setStyleSheet("QComboBox {border-color: red; border-width: 1px; border-style: solid;}")
        else:
            ver_combobox.setStyleSheet("")

    def create_node_item(self, node, item, version_path):
        # create child item
        node_item = QtWidgets.QTreeWidgetItem(item)
        
        filePath = node['file'].value()
        curr_version = self.set_readnode_version(node=node, 
                    node_item=node_item, 
                    item=item, 
                    version_path=version_path, 
                    filePath=filePath)

        # update button
        update_button = QtWidgets.QPushButton("Update", self.treeWidget)
        # update_button.setMaximumHeight(20)
        update_button.clicked.connect(partial(self.update_readnode, item, node_item, True))
        self.treeWidget.setItemWidget(node_item, 6, update_button)
        return node_item, curr_version

    def set_readnode_version(self, node, node_item, item, version_path, filePath):
        latest_version = self.get_latest_version(item=item)

        # set item data as the read node itself
        node_item.setData(QtCore.Qt.UserRole, 0, node)
        # set tooltip as full path
        node_item.setToolTip(0, filePath)  

        node_item.setTextAlignment(0, QtCore.Qt.AlignCenter)
        node_item.setTextAlignment(1, QtCore.Qt.AlignCenter)

        # ----- set name
        filename = os.path.basename(filePath)
        if not filename:
            filename = 'N/A'
        node_item.setText(0, filename)

        # ----- set version
        try:
            version_path = filePath.replace(version_path, '')
            version_path_splits = [p for p in version_path.split('/') if p != '']
            curr_version = version_path_splits[0]
        except Exception, e:
            curr_version = 'N/A'
        node_item.setText(1, curr_version)

        # ----- setup color coded display
        # if current version is not latest, set font to red
        if curr_version != latest_version:
            red_brush = QtGui.QBrush(self.red)
            node_item.setForeground(1, red_brush)
            curr_ver_text = node_item.text(1)
            node_item.setText(1, '{}*'.format(curr_ver_text))
        else:
            # reset font color to default
            node_item.setData(1, QtCore.Qt.ForegroundRole, None)

        return curr_version

    def item_right_clicked(self, pos):
        # right click menu
        item = self.treeWidget.itemAt(pos)
        
        actions = []
        # empty area right clicked
        if not item:
            # expand all
            expand_all_action = QtWidgets.QAction('Expand all', self)
            expand_all_action.triggered.connect(partial(self.expand_items, True))
            actions.append(expand_all_action)
            # collapse all
            collapse_all_action = QtWidgets.QAction('Collapse all', self)
            collapse_all_action.triggered.connect(partial(self.expand_items, False))
            actions.append(collapse_all_action)

            # sep ---
            sep = QtWidgets.QAction(self)
            sep.setSeparator(True)
            actions.append(sep)

            # set all to latest version
            latest_ver_all_action = QtWidgets.QAction('Latest version all', self)
            latest_ver_all_action.triggered.connect(partial(self.set_latest_version_all))
            actions.append(latest_ver_all_action)

        else:
            item_parent = item.parent()
            # it's a child item
            if item_parent:
                # select node
                select_node_action = QtWidgets.QAction('Select node', self)
                select_node_action.triggered.connect(partial(self.select_readnode, [item], False))
                actions.append(select_node_action)
                
                # goto node
                goto_node_action = QtWidgets.QAction('Go to node', self)
                goto_node_action.triggered.connect(partial(self.select_readnode, [item], True))
                actions.append(goto_node_action)

                # remove node
                remove_node_action = QtWidgets.QAction('Remove node', self)
                remove_node_action.triggered.connect(partial(self.remove_readnode, item))
                actions.append(remove_node_action)
            else: # parent item
                # update all
                update_read_nodes_action = QtWidgets.QAction('Update all', self)
                update_read_nodes_action.triggered.connect(partial(self.update_item_readnodes, item, False))
                actions.append(update_read_nodes_action)

                # remove all
                remove_read_nodes_action = QtWidgets.QAction('Remove all', self)
                remove_read_nodes_action.triggered.connect(partial(self.remove_item_readnodes, item))
                actions.append(remove_read_nodes_action)

        if actions:
            rightClickMenu = QtWidgets.QMenu(self)
            for action in actions:
                rightClickMenu.addAction(action)

            rightClickMenu.exec_(self.treeWidget.mapToGlobal(pos))

    def remove_item_readnodes(self, item):
        if not isNuke:
            return
        nodes_to_delete = []
        for i in xrange(item.childCount()):
            node_item = item.child(i)
            node = node_item.data(QtCore.Qt.UserRole, 0)
            nodes_to_delete.append(node)
        item.takeChildren()
        for node in nodes_to_delete:
            nuke.delete(node)
        self.check_outdated_version(item=item)

    def update_item_readnodes(self, item, latest_version=False):
        num_children = item.childCount()
        if num_children < 1:
            return 
        if latest_version:
            ver_widget = self.treeWidget.itemWidget(item, 1)
            ver_combobox = ver_widget.findChild(QtWidgets.QComboBox, 'version_comboBox')
            ver_combobox.setCurrentIndex(ver_combobox.count() - 1)

        updated_node_items = []
        for i in xrange(num_children):
            node_item = item.child(i)
            nuke_node = self.update_readnode(item=item, 
                                            node_item=node_item,
                                            setSelected=False)
            updated_node_items.append(node_item)
        self.select_readnode(node_items=updated_node_items)

    def expand_items(self, value):
        rootItem = self.treeWidget.invisibleRootItem()
        for i in xrange(rootItem.childCount()):
            item = rootItem.child(i)
            if not item.isHidden():
                item.setExpanded(value)

    def set_latest_version_all(self):
        rootItem = self.treeWidget.invisibleRootItem()
        for i in xrange(rootItem.childCount()):
            item = rootItem.child(i)
            if not item.isHidden():
                ver_widget = self.treeWidget.itemWidget(item, 1)
                ver_combobox = ver_widget.findChild(QtWidgets.QComboBox, 'version_comboBox')
                ver_combobox.setCurrentIndex(ver_combobox.count() - 1)

    def remove_readnode(self, node_item):
        if not isNuke:
            return
        node = node_item.data(QtCore.Qt.UserRole, 0)

        # # update UI
        item = node_item.parent()
        # node_item_index = item.indexOfChild(node_item)
        # item.takeChild(node_item_index)

        nuke.delete(node)
        self.check_outdated_version(item=item)

    def select_readnode(self, node_items, frame=False):
        if not isNuke:
            return
        # deselect all
        nukescripts.clear_selection_recursive()
        positions = []
        for node_item in node_items:
            node = node_item.data(QtCore.Qt.UserRole, 0)
            node.setSelected(True)

            if frame:
                positions.append([node.xpos(), node.ypos()])
        
        if frame and positions:
            center = getCenterOfPositions(positions)
            nuke.zoom( 1, center)

    def new_readnode(self, item):
        if not isNuke:
            return

        selected_node = None
        try:
            selected_node = nuke.selectedNode()
        except ValueError:
            pass

        # create read node
        render_version = self.get_current_render_version(item=item)
        if '%04d' in render_version.path:
            st, ef = get_image_sequence_range(path=render_version.path)
            read_arg = 'file {' + str(render_version.path) + ' ' + str(st) + '-' + str(ef) + '}'
        else:
            read_arg = 'file {' + str(render_version.path) + '}'

        node = nuke.createNode('Read', read_arg, inpanel=False) 

        if selected_node:
            node_width = selected_node.screenWidth()
            xPos = int(selected_node['xpos'].getValue(0))
            yPos = int(selected_node['ypos'].getValue(0))
            node.setXYpos(xPos+node_width+10, yPos)

        # update UI
        version_path = render_version.path.split('/{}/'.format(render_version.name))[0]
        # print node.name(), item.text(0), render_version.path, version_path
        node_item, curr_version = self.create_node_item(node=node, item=item, version_path=version_path)
        item.setExpanded(True)
        self.treeWidget.setCurrentItem(node_item)
        self.check_outdated_version(item=item)
        
        return node, node_item
    
    def update_readnode(self, item, node_item, setSelected=False):
        # set read node Knob
        render_version = self.get_current_render_version(item=item)

        node = node_item.data(QtCore.Qt.UserRole, 0)
        node['file'].setValue(render_version.path)
        version_path = render_version.path.split('/{}/'.format(render_version.name))[0]

        # update UI
        self.set_readnode_version(node=node, 
                                node_item=node_item, 
                                item=item,
                                version_path=version_path,
                                filePath=render_version.path)
        if setSelected:
            self.treeWidget.setCurrentItem(node_item)
            self.select_readnode(node_items=[node_item])

        self.check_outdated_version(item=item)
        return node

def get_image_sequence_range(path):
    path = path.replace('.%04d.', '.[0-9][0-9][0-9][0-9].')
    files = glob.glob(path)
    startframe, endframe = 1001, 1001
    try:
        startframe = os.path.basename(files[0]).split('.')[-2]
        endframe = os.path.basename(files[-1]).split('.')[-2]
    except:
        pass
    return startframe, endframe

def getCenterOfPositions(positions):
    x, y = [], []
    for pos in positions:
        x.append(pos[0])
        y.append(pos[1])

    mix, miy = min(x), min(y)
    mxx, mxy = max(x), max(y)
    center = [((mxx-mix)*0.5)+mix, ((mxy-miy)*0.5)+miy]
    return center

def find_builder():
    from rf_nuke import nuke_win
    reload(nuke_win)
    app = nuke_win.findUI('SceneMonitorUi')
    if app:
        builder = None
        for i in xrange(app.ui.tab_widget.count()):
            widget = app.ui.tab_widget.widget(i)
            if isinstance(widget, Ui):
                builder = app.ui.tab_widget.widget(0)
                return builder
