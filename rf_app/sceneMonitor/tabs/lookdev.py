#!/usr/bin/env python
# -- coding: utf-8 --

import os
import sys
import subprocess
from collections import OrderedDict
from datetime import datetime
from functools import partial
from glob import glob
import time
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from Qt import QtCore
from Qt import QtGui
from Qt import QtWidgets

from rf_utils import thread_pool
from . import master
# reload(master)
from rf_utils.sg import sg_process
# reload(sg_process)
from rf_utils.widget import image_widget
# reload(image_widget)
from rf_utils.widget import textEdit
from rf_utils.widget.status_widget import Icon
from rf_utils.widget import collapse_widget
from rf_utils import user_info

isNuke = True
try:
    import nuke
    import nukescripts
    from rf_nuke.utils import shared_comp
    reload(shared_comp)
except ImportError:
    isNuke = False

modulePath = sys.modules[__name__].__file__.replace('\\', '/')
moduleDir  = os.path.dirname(modulePath)
appModuleDir = os.path.dirname(moduleDir)

class Ui(master.Ui):
    """docstring for Ui"""
    def __init__(self, thread=None, parent=None):
        super(Ui, self).__init__(thread=thread, parent=parent)
        self.taskname = 'lookdev'
        self.nopreivew_path = '{}/icons/nopreview_icon.png'.format(appModuleDir)
        self.bg_col = (43, 43, 43)
        self.edit_col = (100, 50, 50)
        
        self.description_active_font = QtGui.QFont()
        self.description_active_font.setItalic(False)
        self.description_active_font.setPointSize(8)

        self.description_display_font = QtGui.QFont()
        self.description_display_font.setItalic(True)
        self.description_display_font.setPointSize(8)

        self.grey_font_palette = QtGui.QPalette()
        self.grey_font_palette.setBrush(QtGui.QPalette.Text, QtGui.QBrush(QtGui.QColor(128, 128, 128)))

        self.white_font_palette = QtGui.QPalette()
        self.white_font_palette.setBrush(QtGui.QPalette.Text, QtGui.QBrush(QtGui.QColor(255, 255, 255)))

        self.setupUi()
        self.init_signal()
        self.set_default()

    def setupUi(self):
        self.mainSplitter = QtWidgets.QSplitter()
        self.mainSplitter.setOrientation(QtCore.Qt.Horizontal)

        self.shotSplitWidget = QtWidgets.QWidget(self.mainSplitter)
        self.viewSplitWidget = QtWidgets.QWidget(self.mainSplitter)
        self.mainSplitter.setSizes([85, 330])

        self.main_layout.addWidget(self.mainSplitter)

        # shotlist layout
        self.asset_layout = QtWidgets.QVBoxLayout(self.shotSplitWidget)
        self.asset_layout.setContentsMargins(6, 0, 6, 0)

        # asset label
        self.asset_label = QtWidgets.QLabel('Assets')
        self.asset_layout.addWidget(self.asset_label)
        # asset list widget
        self.asset_listwidget = QtWidgets.QListWidget()
        self.asset_layout.addWidget(self.asset_listwidget)

        # options
        self.display_option_groupBox = collapse_widget.CollapsibleBox(title='Display options')
        self.asset_layout.addWidget(self.display_option_groupBox)
        self.display_option_layout = QtWidgets.QVBoxLayout(self.display_option_groupBox)


        # filter layout
        self.filter_layout = QtWidgets.QHBoxLayout()
        self.display_option_layout.addLayout(self.filter_layout)

        # label
        self.filter_label = QtWidgets.QLabel('Filter:')
        self.filter_label.setMinimumHeight(22)
        self.filter_layout.addWidget(self.filter_label)

        # search line
        self.search_line = QtWidgets.QLineEdit()
        self.filter_layout.addWidget(self.search_line)

        # radio buttons
        self.display_radiobutton_layout = QtWidgets.QHBoxLayout()
        self.display_option_layout.addLayout(self.display_radiobutton_layout)
        self.char_radio_button = QtWidgets.QRadioButton('Char')
        self.prop_radio_button = QtWidgets.QRadioButton('Prop')
        self.all_radio_button = QtWidgets.QRadioButton('All')
        self.display_radiobutton_layout.addWidget(self.char_radio_button)
        self.display_radiobutton_layout.addWidget(self.prop_radio_button)
        self.display_radiobutton_layout.addWidget(self.all_radio_button)

        self.display_option_groupBox.setContentLayout(self.display_option_layout)

        # view layout 
        self.view_layout = QtWidgets.QVBoxLayout(self.viewSplitWidget)
        self.view_layout.setContentsMargins(0, 0, 0, 0)

        # media layout
        self.media_layout = QtWidgets.QHBoxLayout()
        self.image_viewer = image_widget.ImageSlideViewer()
        # self.image_viewer.setImage("P:/Hanuman/scene/publ/act1/hnm_act1_q0030g_all/colorscript/main/hero/outputMedia/hnm_act1_q0030g_all_clrscpt_main.hero.jpg")

        self.media_layout.addWidget(self.image_viewer)
        self.view_layout.addLayout(self.media_layout)

        # media button layout
        self.media_button_layout = QtWidgets.QVBoxLayout()
        self.media_button_layout.setSpacing(5)
        # play button
        self.view_media_button = QtWidgets.QPushButton('')
        self.view_media_button.setFixedSize(QtCore.QSize(40, 22))
        self.view_media_button.setIcon(QtGui.QIcon('{}/icons/eye_icon.png'.format(appModuleDir)))
        # dir button
        self.open_media_dir_button = QtWidgets.QPushButton('')
        self.open_media_dir_button.setFixedSize(QtCore.QSize(40, 22))
        self.open_media_dir_button.setIcon(QtGui.QIcon('{}/icons/folder_icon.png'.format(appModuleDir)))
        spacerItem1 = QtWidgets.QSpacerItem(25, 25, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        
        self.media_button_layout.addWidget(self.view_media_button)
        self.media_button_layout.addWidget(self.open_media_dir_button)
        self.media_button_layout.addItem(spacerItem1)
        self.media_layout.addLayout(self.media_button_layout)

        # description layout
        self.description_layout = QtWidgets.QVBoxLayout()
        self.description_layout.setSpacing(3)
        self.view_layout.addLayout(self.description_layout)

        # description button layout
        self.description_button_layout = QtWidgets.QHBoxLayout()
        self.description_label = QtWidgets.QLabel('Description: ')
        spacerItem2 = QtWidgets.QSpacerItem(25, 25, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.edit_description_button = QtWidgets.QPushButton('')
        self.edit_description_button.setCheckable(True)
        self.edit_description_button.setFixedSize(QtCore.QSize(40, 22))
        self.edit_description_button.setIcon(QtGui.QIcon('{}/icons/pencil_icon.png'.format(appModuleDir)))
        self.description_button_layout.addWidget(self.description_label)
        self.description_button_layout.addItem(spacerItem2)
        self.description_button_layout.addWidget(self.edit_description_button)
        self.description_layout.addLayout(self.description_button_layout)

        # description box
        self.description_textEdit = textEdit.TextEditWithPlaceHolder()
        self.description_textEdit.setPlaceholderText('< No description >')
        self.description_textEdit.setWordWrapMode(QtGui.QTextOption.WrapAtWordBoundaryOrAnywhere)
        self.description_textEdit.setTextInteractionFlags(QtCore.Qt.NoTextInteraction)
        self.description_textEdit.setFont(self.description_display_font)
        self.description_textEdit.setPalette(self.grey_font_palette)
        self.description_layout.addWidget(self.description_textEdit)

        # setup sizes
        self.media_layout.setStretch(0, 15)
        self.media_layout.setStretch(1, 1)
        self.view_layout.setStretch(0, 10)
        self.view_layout.setStretch(1, 2)
        self.description_layout.setStretch(0, 1)
        self.description_layout.setStretch(1, 3)

        # toolTips
        self.view_media_button.setToolTip('Open image in media viewer')
        self.open_media_dir_button.setToolTip('Open file explorer at image directory')
        self.edit_description_button.setToolTip('Edit lookdev description (Supervisor/Producer only)')
        self.char_radio_button.setToolTip('Show only characters')
        self.prop_radio_button.setToolTip('Show only props')
        self.all_radio_button.setToolTip('Show all types of assets')
        self.search_line.setToolTip('Search for specific asset by name')


    def set_default(self):
        # only allow description edit button for sup, producer, admin
        user = user_info.User()
        if user.is_producer() or user.is_admin() or user.is_supervisor():
            self.edit_description_button.setVisible(True)
        else:
            self.edit_description_button.setVisible(False)

        # filter
        self.all_radio_button.setChecked(True)
        # sorting
        self.asset_listwidget.setSortingEnabled(True)
        self.search_line.setPlaceholderText('Search...')

    def init_signal(self):
        self.asset_listwidget.itemClicked.connect(self.update_view)
        self.asset_listwidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.asset_listwidget.customContextMenuRequested.connect(self.asset_right_clicked)
        self.search_line.textChanged.connect(self.apply_filter)
        self.char_radio_button.clicked.connect(self.apply_filter)
        self.prop_radio_button.clicked.connect(self.apply_filter)
        self.all_radio_button.clicked.connect(self.apply_filter)
        self.edit_description_button.toggled.connect(self.edit_description_toggled)
        self.description_textEdit.editorLostFocus.connect(lambda: self.disable_description_edit(force=False))
        self.view_media_button.clicked.connect(self.view_media)
        self.open_media_dir_button.clicked.connect(self.open_media_dir)

    def view_media(self):
        current_path = self.image_viewer.current_image_path()
        if not current_path or not os.path.exists(current_path):
            return
        os.startfile(current_path)

    def open_media_dir(self):
        current_path = self.image_viewer.current_image_path()
        if not current_path or not os.path.exists(current_path):
            return
        dir_name = os.path.dirname(current_path)
        subprocess.Popen(r'explorer {}'.format(dir_name.replace('/', '\\')))

    def refresh(self, entity): 
        super(Ui, self).refresh(entity)
        if not entity:
            return
        # self.entity inherit from parent class
        self.thread_fetch_data(self.entity)

    def thread_fetch_data(self, entity): 
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        worker = thread_pool.Worker(self.fetch_data, entity)
        worker.signals.result.connect(self.list_assets)
        self.threadpool.start(worker)

    def fetch_data(self, entity): 
        shot_entity = sg_process.get_shot_entity(project=entity.project, code=entity.name, extraFields=['assets'])
        typed_assets = sg_process.get_assets(project=entity.project, filters=[['shots', 'is', shot_entity]])
        versions = sg_process.get_versions_from_entities(entities=shot_entity['assets'], task=self.taskname, extraFields=['sg_status_list', 'sg_task.Task.task_assignees', 'sg_task.Task.entity'])

        shot_assets = []
        for asset_entity in shot_entity['assets']:
            asset_version = [a for a in versions if a['entity']['id'] == asset_entity['id']]
            # asset_version = sg_process.get_versions_from_entity(entity=asset_entity, task=self.taskname, extraFields=['sg_status_list', 'sg_task.Task.task_assignees', 'sg_task.Task.entity'])
            if asset_version:
                shot_assets.append(asset_version)
        # print assets
        return shot_assets, typed_assets

    def list_assets(self, asset_data):
        shot_assets, typed_assets  = asset_data
        asset_info = OrderedDict()
        for asset in shot_assets:
            for version in asset:
                asset_name = version['sg_task.Task.entity']['name']
                
                if asset_name and asset_name not in asset_info:
                    asset_type = None
                    for typed_asset in typed_assets:
                        if typed_asset['code'] == asset_name:
                            asset_type = typed_asset['sg_asset_type']
                            break
                    asset_info[asset_name] = {'asset_type': asset_type, 'versions':[]}
                asset_info[asset_name]['versions'].append(version)

        # update UI
        self.update_assetlist(asset_info=asset_info)
        QtWidgets.QApplication.restoreOverrideCursor()

    def update_assetlist(self, asset_info):
        # clear ui first
        self.asset_listwidget.clear()

        # add master shot to list
        for asset_name, data in asset_info.iteritems():
            asset_type = data['asset_type']
            versions = data['versions']
            # get the latest version
            latest_version = versions[-1]

            # create list widget item
            item = QtWidgets.QListWidgetItem(asset_name)
            # attach data into item
            item.setData(QtCore.Qt.UserRole, {'current':len(versions)-1, 'versions':versions, 'asset_type':asset_type})

            # set sg status icon
            status = latest_version['sg_status_list']
            icon = QtGui.QIcon('{0}/{1}'.format(Icon.path, Icon.statusMap[status]['icon']))
            item.setIcon(icon)

            # set toolTip as assignee names
            toolTipTxt = ', '.join([a['name'] for a in latest_version['sg_task.Task.task_assignees']])
            item.setToolTip('Created by: {}'.format(toolTipTxt))

            self.asset_listwidget.addItem(item)

        # apply type filter
        self.apply_filter()
        # update view
        self.update_view()

    def get_current_type_filter(self):
        if self.all_radio_button.isChecked():
            return None
        elif self.char_radio_button.isChecked():
            return 'char'
        elif self.prop_radio_button.isChecked():
            return 'prop'

    def apply_filter(self):
        current_filter = self.get_current_type_filter()
        search_text = self.search_line.text()
        selection_changed = False
        for i in range(self.asset_listwidget.count()):
            item = self.asset_listwidget.item(i)
            assetName = item.text()
            asset_info = item.data(QtCore.Qt.UserRole)
            hidden = False

            # apply filter
            if current_filter:
                asset_type = asset_info['asset_type']
                if asset_type != current_filter:
                    hidden = True
            # apply search
            if search_text and search_text.lower() not in assetName.lower():
                hidden = True

            # if selected item is filtered out, deselect it
            if hidden and item.isSelected():
                item.setSelected(False)
                selection_changed = True  
            item.setHidden(hidden)

        # if filter is causing selection to change(hide smth currenly selected), clear the view
        if selection_changed:
            self.clear_view()

    def update_view(self, *args, **kwargs):
        self.disable_description_edit()
        self.clear_view()
        item = self.asset_listwidget.currentItem()
        if not item:
            return

        asset_info = item.data(QtCore.Qt.UserRole)

        # get version
        index = asset_info['current']
        versions = asset_info['versions']
        asset_version = versions[index]

        # load images
        media_path = asset_version['sg_path_to_movie']
        if media_path:
            image_paths = glob(media_path.replace('#', '*'))  # replace with * to use glob
            if image_paths:
                self.image_viewer.add_images(image_paths)

        # load description
        text = u''
        if asset_version['description']:
            text = asset_version['description']
        self.description_textEdit.setPlainText(text)

    def asset_right_clicked(self, pos):
        item = self.asset_listwidget.itemAt(pos)
        # right clicked on an item
        if item:
            asset_info = item.data(QtCore.Qt.UserRole)
            current = asset_info['current']
            versions = asset_info['versions']
            actions = []
            for i, version in enumerate(versions):
                action = QtWidgets.QAction(version['code'], self)
                action.setCheckable(True)
                if i == current:  # set checked for current version
                    action.setChecked(True)
                action.triggered.connect(partial(self.update_asset_version, item, i))
                actions.append(action)

            if actions:
                rightClickMenu = QtWidgets.QMenu(self)
                for action in actions:
                    rightClickMenu.addAction(action)
                rightClickMenu.exec_(self.asset_listwidget.mapToGlobal(pos))

    def update_asset_version(self, item, index):
        asset_info = item.data(QtCore.Qt.UserRole)
        asset_info['current'] = index  # update the current index
        item.setData(QtCore.Qt.UserRole, asset_info)

        # update view
        self.update_view()

    def clear_view(self):
        self.image_viewer.clear()
        self.description_textEdit.clear()

    def disable_description_edit(self, force=True):
        # set toggle off
        self.edit_description_button.blockSignals(True) if force else None
        self.edit_description_button.setChecked(False)
        self.edit_description_button.blockSignals(False) if force else None

        # set non editable
        self.description_textEdit.setTextInteractionFlags(QtCore.Qt.NoTextInteraction)
        # set look
        self.description_textEdit.setFont(self.description_display_font)
        self.description_textEdit.setPalette(self.grey_font_palette)
        self.description_textEdit.setStyleSheet('background-color: rgb{};'.format(self.bg_col))

    def enable_description_edit(self):
        # set editable
        self.description_textEdit.setTextInteractionFlags(QtCore.Qt.TextEditorInteraction)
        # set look
        self.description_textEdit.setPalette(self.white_font_palette)
        self.description_textEdit.setFont(self.description_active_font)
        self.description_textEdit.setFocus()
        self.description_textEdit.moveCursor(QtGui.QTextCursor.End)
        self.description_textEdit.setStyleSheet('background-color: rgb{};'.format(self.edit_col))

    def edit_description_toggled(self):
        currentItem = self.asset_listwidget.currentItem()
        if not currentItem:
            self.disable_description_edit()
            return

        asset_data = currentItem.data(QtCore.Qt.UserRole)
        index = asset_data['current']
        versions = asset_data['versions']
        asset_info = versions[index]

        # start editing
        if self.edit_description_button.isChecked():
            self.enable_description_edit()
        else: # finish editing
            current_text = self.description_textEdit.toPlainText()

            if current_text != asset_info['description']:
                # popup question to user
                qmsgBox = QtWidgets.QMessageBox(self)
                qmsgBox.setText('Edit lookdev description?')
                qmsgBox.setWindowTitle('Confirm')
                qmsgBox.setIcon(QtWidgets.QMessageBox.Question)
                qmsgBox.addButton('  Yes  ', QtWidgets.QMessageBox.AcceptRole)
                qmsgBox.addButton('  No  ', QtWidgets.QMessageBox.RejectRole)
                answer = qmsgBox.exec_()
                if answer == 0: 
                    try:
                        # update shotgun data
                        update_description(taskid=asset_info['id'], description=current_text)
                        
                        # also update local data stored inside listwidget item
                        asset_info['description'] = current_text
                        asset_data['versions'][index] = asset_info
                        currentItem.setData(QtCore.Qt.UserRole, asset_data)
                    except Exception, e:
                        self.description_textEdit.setPlainText(asset_info['description'])
                else:
                    # set text back to previous text
                    self.description_textEdit.setPlainText(asset_info['description'])
            # disable edit UI
            self.disable_description_edit()

def update_description(taskid, description): 
    """ this function update description from task id 
    get task id from shot[self.taskname]['id']
    """ 
    data = {'description': description}
    return sg_process.sg.update('Version', taskid, data)
