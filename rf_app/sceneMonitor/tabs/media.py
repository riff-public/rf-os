#!/usr/bin/env python
# -- coding: utf-8 --

import os
import sys
import subprocess
from collections import OrderedDict
from datetime import datetime
from functools import partial
import time
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from Qt import QtCore
from Qt import QtGui
from Qt import QtWidgets

import rf_config as config
from rf_utils.widget import textEdit
from rf_utils.widget import status_widget
from rf_utils.sg import sg_process
# reload(sg_process)
from rf_utils import thread_pool
from rf_utils.widget import media_widget
# reload(media_widget)
from rf_utils.widget import image_widget
# reload(image_widget)
from rf_utils.widget import hashtag_widget
from rf_utils.widget import collapse_widget
from rf_utils import user_info
from rf_utils import file_utils
from rf_utils.pipeline import watermark
from . import master


modulePath = sys.modules[__name__].__file__.replace('\\', '/')
moduleDir  = os.path.dirname(modulePath)
appModuleDir = os.path.dirname(moduleDir)

class Snapshot(QtWidgets.QMainWindow):
    def __init__(self, item_size, entity, parent=None):
        #Setup Window
        super(Snapshot, self).__init__(parent)
        self.item_size = item_size
        self.entity = entity
        self.viewer_background_color = QtGui.QColor(50, 50, 50)
        self.shotname_font = QtGui.QFont('Calibri', 22)

        self.setupUi()
        self.init_signal()
        self.set_default()

    def setupUi(self):
        # setup UI
        # main win
        self.main_widget = QtWidgets.QWidget()
        self.setCentralWidget(self.main_widget)
        self.resize(850, 500)
        self.setWindowModality(QtCore.Qt.ApplicationModal)
        self.setWindowTitle('Take Snapshot')
        self.setWindowIcon(QtGui.QIcon('{}/icons/app_icon.png'.format(appModuleDir)))

        # main layout
        self.main_layout = QtWidgets.QVBoxLayout(self.main_widget)

        # setup splitter
        self.mainSplitter = QtWidgets.QSplitter()
        self.mainSplitter.setOrientation(QtCore.Qt.Horizontal)

        self.settingSplitWidget = QtWidgets.QWidget(self.mainSplitter)
        self.viewSplitWidget = QtWidgets.QWidget(self.mainSplitter)
        self.mainSplitter.setSizes([80, 320])
        self.main_layout.addWidget(self.mainSplitter)

        # ------ setting layout
        self.setting_layout = QtWidgets.QVBoxLayout(self.settingSplitWidget)
        self.setting_layout.setContentsMargins(3, 3, 3, 3)
        self.setting_layout.setSpacing(3)

        self.snapshot_layout = QtWidgets.QGridLayout()
        self.snapshot_layout.setRowMinimumHeight(0, 20)
        self.snapshot_layout.setRowMinimumHeight(1, 20)
        self.snapshot_layout.setRowMinimumHeight(2, 20)
        self.snapshot_layout.setRowMinimumHeight(3, 20)
        self.snapshot_layout.setHorizontalSpacing(10)
        self.setting_layout.addLayout(self.snapshot_layout)

        # images per row setting
        self.grid_label = QtWidgets.QLabel('Images per row')
        self.snapshot_layout.addWidget(self.grid_label, 0, 0, 1, 1, QtCore.Qt.AlignLeft)
        self.imagePerRow_spinBox = QtWidgets.QSpinBox()
        self.imagePerRow_spinBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.imagePerRow_spinBox.setButtonSymbols(QtWidgets.QAbstractSpinBox.UpDownArrows)
        self.imagePerRow_spinBox.setMinimum(1)
        self.imagePerRow_spinBox.setMaximum(30)
        self.snapshot_layout.addWidget(self.imagePerRow_spinBox, 0, 1, 1, 1, QtCore.Qt.AlignLeft)

        # font size
        self.fontsize_label = QtWidgets.QLabel('Font size')
        self.snapshot_layout.addWidget(self.fontsize_label, 1, 0, 1, 1, QtCore.Qt.AlignLeft)
        self.fontsize_spinBox = QtWidgets.QSpinBox()
        self.fontsize_spinBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.fontsize_spinBox.setButtonSymbols(QtWidgets.QAbstractSpinBox.UpDownArrows)
        self.fontsize_spinBox.setMinimum(8)
        self.fontsize_spinBox.setMaximum(128)
        self.snapshot_layout.addWidget(self.fontsize_spinBox, 1, 1, 1, 1, QtCore.Qt.AlignLeft)

        # row spacing 
        self.rowSpacing_label = QtWidgets.QLabel('Row gap')
        self.snapshot_layout.addWidget(self.rowSpacing_label, 2, 0, 1, 1, QtCore.Qt.AlignLeft)
        self.rowSpacing_spinBox = QtWidgets.QSpinBox()
        self.rowSpacing_spinBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.rowSpacing_spinBox.setButtonSymbols(QtWidgets.QAbstractSpinBox.UpDownArrows)
        self.rowSpacing_spinBox.setMinimum(0)
        self.rowSpacing_spinBox.setMaximum(2000)
        self.snapshot_layout.addWidget(self.rowSpacing_spinBox, 2, 1, 1, 1, QtCore.Qt.AlignLeft)

        # column spacing 
        self.columnSpacing_label = QtWidgets.QLabel('Column gap')
        self.snapshot_layout.addWidget(self.columnSpacing_label, 3, 0, 1, 1, QtCore.Qt.AlignLeft)
        self.columnSpacing_spinBox = QtWidgets.QSpinBox()
        self.columnSpacing_spinBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.columnSpacing_spinBox.setButtonSymbols(QtWidgets.QAbstractSpinBox.UpDownArrows)
        self.columnSpacing_spinBox.setMinimum(0)
        self.columnSpacing_spinBox.setMaximum(2000)
        self.snapshot_layout.addWidget(self.columnSpacing_spinBox, 3, 1, 1, 1, QtCore.Qt.AlignLeft)

        # ------------------------------------------
        self.render_option_groupBox = collapse_widget.CollapsibleBox('Render settings')
        self.setting_layout.addWidget(self.render_option_groupBox)

        self.render_option_layout = QtWidgets.QGridLayout()
        self.render_option_layout.setRowMinimumHeight(0, 20)
        self.render_option_layout.setRowMinimumHeight(1, 20)
        self.render_option_layout.setRowMinimumHeight(2, 20)

        # render size
        self.width_label = QtWidgets.QLabel('Width')
        self.render_option_layout.addWidget(self.width_label, 0, 0, 1, 1, QtCore.Qt.AlignLeft)

        self.render_width_spinBox = QtWidgets.QSpinBox()
        self.render_width_spinBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.render_width_spinBox.setButtonSymbols(QtWidgets.QAbstractSpinBox.UpDownArrows)
        self.render_width_spinBox.setMinimum(500)
        self.render_width_spinBox.setMaximum(12000)
        self.render_option_layout.addWidget(self.render_width_spinBox, 0, 1, 1, 1, QtCore.Qt.AlignLeft)

        self.height_label = QtWidgets.QLabel('Height')
        self.render_option_layout.addWidget(self.height_label, 1, 0, 1, 1, QtCore.Qt.AlignLeft)

        self.render_height_spinBox = QtWidgets.QSpinBox()
        self.render_height_spinBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.render_height_spinBox.setButtonSymbols(QtWidgets.QAbstractSpinBox.UpDownArrows)
        self.render_height_spinBox.setMinimum(500)
        self.render_height_spinBox.setMaximum(12000)
        self.render_option_layout.addWidget(self.render_height_spinBox, 1, 1, 1, 1, QtCore.Qt.AlignLeft)

        self.border_label = QtWidgets.QLabel('Border')
        self.render_option_layout.addWidget(self.border_label, 2, 0, 1, 1, QtCore.Qt.AlignLeft)

        self.border_spinBox = QtWidgets.QSpinBox()
        self.border_spinBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.border_spinBox.setButtonSymbols(QtWidgets.QAbstractSpinBox.UpDownArrows)
        self.border_spinBox.setMinimum(0)
        self.border_spinBox.setMaximum(100)
        
        self.border_spinBox.setSuffix('%')
        self.render_option_layout.addWidget(self.border_spinBox, 2, 1, 1, 1, QtCore.Qt.AlignLeft)

        self.render_option_groupBox.setContentLayout(self.render_option_layout)

        spacerItem1 = QtWidgets.QSpacerItem(25, 25, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.setting_layout.addItem(spacerItem1)

        self.setting_layout.setStretch(0, 1)
        self.setting_layout.setStretch(1, 1)
        self.setting_layout.setStretch(2, 10)

        # ------ view layout
        self.view_layout = QtWidgets.QVBoxLayout(self.viewSplitWidget)
        self.image_viewer = image_widget.ImageGridViewer(imagePerRow=4) 
        self.view_layout.addWidget(self.image_viewer)

        # buttons
        self.buttonBox = QtWidgets.QDialogButtonBox(QtCore.Qt.Horizontal)
        ok_button = QtWidgets.QPushButton('Save image')
        cancel_button = QtWidgets.QPushButton('Cancel')
        self.buttonBox.addButton(ok_button, QtWidgets.QDialogButtonBox.AcceptRole)
        self.buttonBox.addButton(cancel_button, QtWidgets.QDialogButtonBox.RejectRole)
        self.main_layout.addWidget(self.buttonBox)

    def init_signal(self):
        self.rowSpacing_spinBox.valueChanged.connect(self.rowSpacing_changed)
        self.columnSpacing_spinBox.valueChanged.connect(self.columnSpacing_changed)
        self.imagePerRow_spinBox.valueChanged.connect(self.imagePerRow_changed)
        self.fontsize_spinBox.valueChanged.connect(self.fontSize_changed)
    
        # button signals
        self.buttonBox.accepted.connect(self.take_snapshot)
        self.buttonBox.rejected.connect(self.close)

    def set_default(self):
        # image viewer setup
        self.image_viewer.gridScale = self.item_size
        self.image_viewer._item_is_selectable = False
        self.image_viewer.setBackgroundBrush(self.viewer_background_color)
        self.image_viewer.borderWidth = 5

        # setting spinboxes setup
        self.imagePerRow_spinBox.setValue(4)
        self.rowSpacing_spinBox.setValue(50)
        self.fontsize_spinBox.setValue(self.shotname_font.pointSize())
        self.columnSpacing_spinBox.setValue(50)
        self.render_width_spinBox.setValue(4096)
        self.render_height_spinBox.setValue(2048)
        self.border_spinBox.setValue(5)
        
    def show_images(self, image_datas):
        self.image_viewer.add_ui_items(item_datas=image_datas)
        i = 0
        for pixmap, userdata in image_datas:
            self.image_viewer.onFrameLoaded(loadData=(i, pixmap))
            i += 1
        self.image_viewer.set_item_decorations()
        self.update_render_size()

    def rowSpacing_changed(self):
        self.image_viewer.rowSpacing = self.rowSpacing_spinBox.value()
        self.image_viewer.arrangeImages()
        self.update_render_size()

    def columnSpacing_changed(self):
        self.image_viewer.columnSpacing = self.columnSpacing_spinBox.value()
        self.image_viewer.arrangeImages()
        self.update_render_size()

    def imagePerRow_changed(self):
        self.image_viewer.imagePerRow = self.imagePerRow_spinBox.value()
        self.image_viewer.arrangeImages()
        self.update_render_size()

    def fontSize_changed(self):
        self.shotname_font.setPointSize(self.fontsize_spinBox.value())
        # set data attached to itmes
        for item in self.image_viewer.items:
            item_data = item.data(QtCore.Qt.UserRole).copy()
            if '__text' in item_data:
                new_datas = []
                for data in item_data['__text']:
                    new_data = [data[0], data[1] ,data[2], self.shotname_font, data[4]]
                    new_datas.append(new_data)
                item_data['__text'] = new_datas
                item.setData(QtCore.Qt.UserRole, item_data)
        self.image_viewer.set_item_decorations()
        self.update_render_size()

    def update_render_size(self, *args):
        if not self.image_viewer.scene():
            return
        # update settings
        allRect = self.image_viewer.getItemsRect()
        self.render_width_spinBox.setValue(int(allRect.width()*0.5))
        self.render_height_spinBox.setValue(int(allRect.height()*0.5))

    def take_snapshot(self):
        render_size = (self.render_width_spinBox.value(), self.render_height_spinBox.value())
        verticalBorder = 1.0 - ((self.border_spinBox.value() * 0.01) * 0.1)  # slimmer side border
        horizontalBorder = 1.0 - (self.border_spinBox.value() * 0.01)
        chosen_path, temp_path = self.image_viewer.browseSaveImage(renderSize=render_size, 
                                                                verticalBorder=horizontalBorder,  
                                                                horizontalBorder=horizontalBorder,
                                                                writeToTemp=True)
        # create burn in
        if chosen_path:
            user = user_info.User()
            # convert shot entity to parent sequence
            entity = self.entity.copy()
            entity.context.update(entityCode='all', entity='none', process=self.entity.process)
            entity.update_scene_context()
            result = watermark.add_border_hud_to_image(input_path=temp_path, 
                                                        output_path=chosen_path,  
                                                        topLeft=[entity.project],
                                                        topMid=[entity.name],
                                                        topRight=['Date: {}'.format(time.strftime('%d %b %Y - %H:%M:%S'))],
                                                        bottomLeft=['user: {}'.format(user.name()), 
                                                                'file: {}'.format(os.path.basename(chosen_path))],
                                                        bottomMid=[],
                                                        bottomRight=[],
                                                        fontScaleMultiplier={'topMid':1.25}, # make sequence name bigger
                                                        thickness=2)  
            # cleanup temp
            os.remove(temp_path)
            # popup to confirm success
            qmsgBox = QtWidgets.QMessageBox(self)
            qmsgBox.setText('Snapshot success:\n{}'.format(chosen_path))
            qmsgBox.setWindowTitle('Confirm')
            qmsgBox.setIcon(QtWidgets.QMessageBox.Information)
            qmsgBox.addButton('OK', QtWidgets.QMessageBox.AcceptRole)
            qmsgBox.exec_()

            # close Snapshot UI
            self.close()
            
        return chosen_path

class Ui(master.Ui):
    """docstring for Ui"""
    def __init__(self, thread=None, parent=None):
        super(Ui, self).__init__(thread=thread, parent=parent)
        self.shot_data = OrderedDict()
        self.departments = OrderedDict([('animatic', []),
                                        ('layout', []), 
                                        ('anim', []), 
                                        ('finalCam', []), 
                                        ('techAnim', []), 
                                        ('light_still', []), 
                                        ('comp_still', ['_preview', '_review']), 
                                        ('light_full', []), 
                                        ('comp_full', ['_preview', '_review'])])
        self.player_app = {'Keyframe Pro': self.play_keyframepro, 'RV': self.play_rv}
        self.download_path = None
        self.chosen_app = None
        self.nopreivew_path = '{}/icons/nopreview_icon.png'.format(appModuleDir)
        self.multiple_value_str = '< Multiple values >'
        self.no_description_str = '< No description >'
        user = user_info.User()
        self.is_admin = True if user.is_producer() or user.is_admin() or user.is_supervisor() else False

        # internal vars
        self.__default_render_size = (1920, 1080)
        self.__default_fps = 24

        # UI vars
        self._render_size = self.__default_render_size
        self._minimum_grid_size = 64
        self._maximum_grid_size = 640
        self._default_grid_size = 215
        self._default_quality = 30
        self._load_size_mult = 0.5

        self.bg_col = (43, 43, 43)
        self.edit_col = (100, 50, 50)

        self.active_font = QtGui.QFont()
        self.active_font.setItalic(False)
        self.active_font.setPointSize(8)

        self.display_font = QtGui.QFont()
        self.display_font.setItalic(True)
        self.display_font.setPointSize(8)

        self.grey_font_palette = QtGui.QPalette()
        self.grey_font_palette.setBrush(QtGui.QPalette.Text, QtGui.QBrush(QtGui.QColor(128, 128, 128)))

        self.white_font_palette = QtGui.QPalette()
        self.white_font_palette.setBrush(QtGui.QPalette.Text, QtGui.QBrush(QtGui.QColor(255, 255, 255)))

        self.setupUi()
        self.init_signal()
        self.set_default()

    def take_snapshot(self):
        item_count = self.media_viewer.count()
        if not item_count:
            return 

        images = []
        for i in xrange(item_count):
            item = self.media_viewer.item(i)
            if item.current_frame < len(item.pixmaps):
                pixmap = item.pixmaps[item.current_frame]
                font_data = [(item.text(), 'bottomCenter', 0, QtGui.QFont('Calibri', 22), QtGui.QColor('white'))]
                images.append((pixmap, {'__text': font_data}))

        item_size = (self.media_viewer.item_size.width(), self.media_viewer.item_size.height())
        snap = Snapshot(item_size=item_size, entity=self.entity , parent=self)
        snap.show()
        snap.show_images(image_datas=images)
        
    def setupUi(self):
        # setup splitter
        self.mainSplitter = QtWidgets.QSplitter()
        self.mainSplitter.setOrientation(QtCore.Qt.Horizontal)

        self.infoSplitWidget = QtWidgets.QWidget(self.mainSplitter)
        self.viewSplitWidget = QtWidgets.QWidget(self.mainSplitter)
        self.mainSplitter.setSizes([95, 300])

        # info layout
        self.info_layout = QtWidgets.QVBoxLayout(self.infoSplitWidget)
        self.info_layout.setContentsMargins(6, 6, 6, 6)
        self.info_layout.setSpacing(3)

        # view layout 
        self.view_layout = QtWidgets.QHBoxLayout(self.viewSplitWidget)
        self.view_layout.setContentsMargins(0, 0, 0, 0)

        self.main_layout.addWidget(self.mainSplitter)

        # -------------------
        # media layout
        self.media_layout = QtWidgets.QVBoxLayout()
        self.media_layout.setSpacing(3)

        # media viewer
        self.media_viewer = media_widget.MediaGridViewer(quality=self._default_quality*0.01)
        self.media_viewer.auto_play = True
        self.media_layout.addWidget(self.media_viewer)

        # department combobox
        self.dept_layout = QtWidgets.QHBoxLayout()
        self.dept_layout.setSpacing(5)
        self.dept_layout.setContentsMargins(0, 0, 0, 0)
        self.media_layout.addLayout(self.dept_layout)

        spacerItem1 = QtWidgets.QSpacerItem(25, 25, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.dept_layout.addItem(spacerItem1)

        # media from combobox
        self.dept_label = QtWidgets.QLabel('Show from')
        self.dept_layout.addWidget(self.dept_label)

        self.dept_combobox = QtWidgets.QComboBox()
        self.dept_combobox.setMinimumWidth(100)
        self.dept_combobox.addItems(self.departments.keys())
        self.dept_layout.addWidget(self.dept_combobox)

        # only checkbox
        self.only_checkbox = QtWidgets.QCheckBox('only')
        self.dept_layout.addWidget(self.only_checkbox)

        spacerItem2 = QtWidgets.QSpacerItem(25, 25, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.dept_layout.addItem(spacerItem2)

        self.view_layout.addLayout(self.media_layout)

        # -------------------
        # button layout
        self.button_layout = QtWidgets.QVBoxLayout()
        self.button_layout.setSpacing(5)
        # view button
        self.play_button = QtWidgets.QPushButton('')
        self.play_button.setFixedSize(QtCore.QSize(40, 22))
        self.play_button.setIcon(QtGui.QIcon('{}/icons/playlist_play_icon.png'.format(appModuleDir)))
        # dir button
        self.open_dir_button = QtWidgets.QPushButton('')
        self.open_dir_button.setFixedSize(QtCore.QSize(40, 22))
        self.open_dir_button.setIcon(QtGui.QIcon('{}/icons/folder_icon.png'.format(appModuleDir)))
        # snapshot button
        self.snapshot_button = QtWidgets.QPushButton('')
        self.snapshot_button.setFixedSize(QtCore.QSize(40, 25))
        self.snapshot_button.setIcon(QtGui.QIcon('{}/icons/camera_icon.png'.format(appModuleDir)))
        # download button
        self.download_button = QtWidgets.QPushButton('')
        self.download_button.setFixedSize(QtCore.QSize(40, 25))
        self.download_button.setIcon(QtGui.QIcon('{}/icons/import2_icon.png'.format(appModuleDir)))
        
        spacerItem3 = QtWidgets.QSpacerItem(25, 25, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)

        self.button_layout.addWidget(self.play_button)
        self.button_layout.addWidget(self.open_dir_button)
        self.button_layout.addWidget(self.snapshot_button)
        self.button_layout.addWidget(self.download_button)
        self.button_layout.addItem(spacerItem3)
        self.view_layout.addLayout(self.button_layout)

        # -------------------
        # status layout
        self.status_layout = QtWidgets.QFormLayout()
        self.status_layout.setLabelAlignment(QtCore.Qt.AlignLeft)
        self.status_layout.setFormAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)
        self.status_layout.setSpacing(3)
        self.info_layout.addLayout(self.status_layout)
        
        self.shot_value_label = QtWidgets.QLabel()
        self.creator_value_label = QtWidgets.QLabel()
        self.date_value_label = QtWidgets.QLabel()
        self.status_value_label = status_widget.StatusLabel()

        self.status_layout.addRow('Shot:', self.shot_value_label)
        self.status_layout.addRow('Creator:', self.creator_value_label)
        self.status_layout.addRow('Updated:', self.date_value_label)
        self.status_layout.addRow('Status:', self.status_value_label)

        # Tags
        self.tag_layout = QtWidgets.QVBoxLayout()
        self.tag_layout.setContentsMargins(0, 0, 0, 0)
        self.tag_layout.setSpacing(0)
        self.info_layout.addLayout(self.tag_layout)

        self.tag_title_layout = QtWidgets.QHBoxLayout()
        self.tag_title_layout.setContentsMargins(0, 0, 0, 0)
        self.tag_layout.addLayout(self.tag_title_layout)

        self.tag_label = QtWidgets.QLabel('Tags:')
        self.tag_title_layout.addWidget(self.tag_label)

        spacerItem43 = QtWidgets.QSpacerItem(25, 25, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.tag_title_layout.addItem(spacerItem43)

        self.hashtagWidget = hashtag_widget.SGTagWidget(inLine=False)
        self.hashtagWidget.use_hash = True
        self.hashtagWidget.allow_creation = True
        self.hashtagWidget.allow_rename = True
        self.hashtagWidget.allLayout.setSpacing(0)
        self.hashtagWidget.line_edit.setStyleSheet('background-color: rgb{}; border-radius: 0px'.format(self.bg_col))
        self.hashtagWidget.label.hide()
        self.tag_layout.addWidget(self.hashtagWidget)

        # description 
        self.description_layout = QtWidgets.QVBoxLayout()
        self.description_layout.setContentsMargins(0, 0, 0, 0)
        self.description_layout.setSpacing(0)
        self.info_layout.addLayout(self.description_layout)

        self.description_title_layout = QtWidgets.QHBoxLayout()
        self.description_title_layout.setContentsMargins(0, 0, 0, 0)
        self.description_layout.addLayout(self.description_title_layout)

        self.description_label = QtWidgets.QLabel('Description:')
        self.description_title_layout.addWidget(self.description_label)

        spacerItem33 = QtWidgets.QSpacerItem(25, 25, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.description_title_layout.addItem(spacerItem33)

        self.edit_description_button = QtWidgets.QPushButton('')
        self.edit_description_button.setCheckable(True)
        self.edit_description_button.setFixedSize(QtCore.QSize(30, 20))
        self.edit_description_button.setIcon(QtGui.QIcon('{}/icons/pencil_icon.png'.format(appModuleDir)))
        self.description_title_layout.addWidget(self.edit_description_button)

        self.description_textEdit = textEdit.TextEditWithPlaceHolder()
        self.description_textEdit.setPlaceholderText(self.no_description_str)
        self.description_textEdit.setWordWrapMode(QtGui.QTextOption.WrapAtWordBoundaryOrAnywhere)
        self.description_textEdit.setTextInteractionFlags(QtCore.Qt.NoTextInteraction)
        self.description_textEdit.setFont(self.display_font)
        self.description_textEdit.setPalette(self.grey_font_palette)
        self.description_layout.addWidget(self.description_textEdit)

        # settings
        self.grid_option_groupBox = collapse_widget.CollapsibleBox('Display options')
        self.info_layout.addWidget(self.grid_option_groupBox)
        self.grid_option_layout = QtWidgets.QVBoxLayout(self.grid_option_groupBox)
        
        # setting form layout
        self.thumbsize_layout = QtWidgets.QFormLayout()
        self.thumbsize_layout.setLabelAlignment(QtCore.Qt.AlignLeft)
        self.thumbsize_layout.setFormAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)
        self.thumbsize_layout.setSpacing(3)
        self.grid_option_layout.addLayout(self.thumbsize_layout)
        
        # Sort combobox
        self.sort_combobox = QtWidgets.QComboBox()
        self.sort_combobox.addItems(['None', 'Shot name', 'Cut order'])
        self.thumbsize_layout.addRow('Sort by: ', self.sort_combobox)

        # thumbnail size slider
        self.gridsize_slider = QtWidgets.QSlider()
        self.gridsize_slider.setMinimum(self._minimum_grid_size)
        self.gridsize_slider.setMaximum(self._maximum_grid_size)
        self.gridsize_slider.setOrientation(QtCore.Qt.Horizontal)
        self.thumbsize_layout.addRow('Size: ', self.gridsize_slider)

        # # quality slider
        # self.quality_slider = QtWidgets.QSlider()
        # self.quality_slider.setMinimum(1)
        # self.quality_slider.setMaximum(100)
        # self.quality_slider.setValue(self._default_quality)
        # self.quality_slider.setOrientation(QtCore.Qt.Horizontal)
        # self.thumbsize_layout.addRow('Quality: ', self.quality_slider)

        # # autoplay checkbox
        # self.autoplay_checkbox = QtWidgets.QCheckBox('Auto play')
        # self.grid_option_layout.addWidget(self.autoplay_checkbox)

        self.grid_option_groupBox.setContentLayout(self.grid_option_layout)

        # setup sizes
        self.view_layout.setStretch(0, 9)
        self.view_layout.setStretch(1, 3)

        self.info_layout.setStretch(0, 0)
        self.info_layout.setStretch(1, 1)
        self.info_layout.setStretch(2, 3)
        self.info_layout.setStretch(3, 0)

        # toolTips
        self.play_button.setToolTip('Create playlist from selected items in a media player')
        self.open_dir_button.setToolTip('Open file explorer at selected media directories')
        self.snapshot_button.setToolTip('Take snapshot of current display')
        self.download_button.setToolTip('Download selected media to local')
        self.sort_combobox.setToolTip('Rearrange items by sorting order')
        self.gridsize_slider.setToolTip('Adjust how big/small the size of each item in the viewer')
        # self.quality_slider.setToolTip('Adjust the resolution of each media loaded to an item')
        # self.autoplay_checkbox.setToolTip('''Turn auto play ON/Off.
        # - If checked, media will play as soon as you put your mouse on an item.
        # - If not, use middle-mouse button to drag through each frame.''')
        self.dept_combobox.setToolTip('Select department to populate media')
        self.only_checkbox.setToolTip('''Turn only selected department ON/OFF.
        - If checked, will only fetch media from selected department.
        - If not, and no media is available for the selected department, 
            will include media from upstream department.''')
        self.edit_description_button.setToolTip('Edit shot description (Supervisor/Producer only)')

    def refresh(self, entity): 
        super(Ui, self).refresh(entity)

        # clear items first so user won't trigger anymore thread runs
        self.media_viewer.clear()
        self.reset_info_panel()
        if not entity:
            return
            
        if self.entity:
            # update media widget settings everytime entity changes
            self.media_viewer.fps = self.entity.projectInfo.render.fps()
            w, h = self.entity.projectInfo.render.final_outputH()
            self._render_size = (w * self._load_size_mult, h * self._load_size_mult)  # multply the load size for performance
        else:
            self.media_viewer.fps = self.__default_fps
            self._render_size = self.__default_render_size
        self.update_item_size()

        # self.entity inherit from parent class
        self.thread_fetch_data(self.entity)

    def set_default(self):
        self.reset_info_panel()  # reset info panel
        # self.autoplay_checkbox.setChecked(True)  # auto play enabled by default
        self.gridsize_slider.setValue(self._default_grid_size)
        self.sort_combobox.setCurrentIndex(2)  # sort by cut order
        self.dept_combobox.setCurrentIndex(8)  # set to comp_full

        if self.is_admin:
            self.edit_description_button.setVisible(True)
            self.hashtagWidget.line_edit.setReadOnly(False)
            self.hashtagWidget.allow_rename = True
            self.hashtagWidget.allow_creation = True
            self.hashtagWidget.allow_delete = True
        else:
            self.edit_description_button.setVisible(False)
            self.hashtagWidget.line_edit.setReadOnly(True)
            self.hashtagWidget.line_edit.hide()
            self.hashtagWidget.allow_rename = False
            self.hashtagWidget.allow_creation = False
            self.hashtagWidget.allow_delete = False

    def init_signal(self):
        self.sort_combobox.currentIndexChanged.connect(self.update_viewer)
        # self.autoplay_checkbox.toggled.connect(self.autoplay_toggled)
        self.gridsize_slider.valueChanged.connect(self.update_item_size)
        # self.quality_slider.sliderMoved.connect(self.editing_quality)
        # self.quality_slider.sliderReleased.connect(self.quality_changed)
        self.edit_description_button.toggled.connect(self.edit_description_toggled)
        self.play_button.clicked.connect(self.play_media)
        self.open_dir_button.clicked.connect(self.open_media_dir)
        self.snapshot_button.clicked.connect(self.take_snapshot)
        self.download_button.clicked.connect(self.download_media)
        self.media_viewer.itemSelectionChanged.connect(self.update_info)
        self.media_viewer.itemDoubleClicked.connect(self.play_media)
        self.dept_combobox.currentIndexChanged.connect(self.update_viewer)
        self.only_checkbox.toggled.connect(self.update_viewer)
        self.description_textEdit.editorLostFocus.connect(lambda: self.disable_description_edit(force=False))
        self.hashtagWidget.edited.connect(self.tag_edited)

        # right clicked menu
        self.media_viewer.customContextMenuRequested.connect(self.item_right_clicked)

    def download_media(self):
        items = self.media_viewer.selectedItems()
        if not items:
            return

        mods = QtWidgets.QApplication.keyboardModifiers()
        if not self.download_path or mods & QtCore.Qt.ControlModifier:
            # choose path
            download_dir = QtWidgets.QFileDialog.getExistingDirectory(parent=self, 
                                                    caption='Set download directory',
                                                    dir=os.path.expanduser('~'))
            if download_dir:
                self.download_path = download_dir
        # get folder name
        proj = self.entity.context.projectCode
        date = str(datetime.strftime(datetime.today().date(), '%Y%m%d'))
        episode = self.entity.episode
        dept = self.dept_combobox.currentText()
        folder_path = '{}/{}_{}_{}_{}'.format(self.download_path, proj, date, episode, dept)
        if not os.path.exists(folder_path):
            os.makedirs(folder_path)

        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        for item in items:
            shot_info = item.data(QtCore.Qt.UserRole)
            path = shot_info['filepath']
            fn = os.path.basename(path)
            file_utils.copy(path, '{}/{}'.format(folder_path, fn))
        QtWidgets.QApplication.restoreOverrideCursor()

        # # open dir
        # subprocess.Popen(r'explorer {}'.format(folder_path.replace('/', '\\')))

    def sort_data(self):
        '''
        {shotName: {cut_order:n, 
                    data:{
                        departmentA: {...}, 
                        departmentB: {...}
                        }
                    }
        }
        '''
        curr_index = self.sort_combobox.currentIndex()
        if curr_index == 0:
            sorted_data = self.shot_data.copy()
        elif curr_index == 1:  # sort by shot name
            sorted_data = OrderedDict(sorted(self.shot_data.items()))
        elif curr_index == 2:  # sort by cut order
            sorted_data = OrderedDict(sorted(self.shot_data.items(), key=lambda s: s[1]['cut_order']))

        return sorted_data

    def item_right_clicked(self, pos):
        # right click menu
        item = self.media_viewer.itemAt(pos)
        sel_items = self.media_viewer.selectedItems()
        if item and [i for i in sel_items if i is item]:
            actions = OrderedDict()
            # if more than 1 item selected, will display all possible departments
            if len(sel_items) > 1:
                switch_selected_actions = []
                for dept in self.departments.keys():
                    action = QtWidgets.QAction(dept, self.media_viewer)
                    action.triggered.connect(partial(self.switch_items_to, sel_items, dept))
                    switch_selected_actions.append(action)
                actions['Switch selected to'] = switch_selected_actions
            else:
                item_data = item.data(QtCore.Qt.UserRole)
                shotname = item_data['shotname']

                # switch menu
                switch_actions = []
                departments = self.get_departments_from_shot(shotname=shotname)
                for dept in departments:
                    media_path = self.shot_data[shotname]['data'][dept]['filepath']
                    if os.path.exists(media_path):
                        new_shot_data = self.shot_data[shotname]['data'][dept]
                        action = QtWidgets.QAction(dept, self.media_viewer)
                        action.triggered.connect(partial(self.switch_item, item, new_shot_data))
                        switch_actions.append(action)
                if switch_actions:
                    actions['Switch to'] = switch_actions

            if actions:
                rightClickMenu = QtWidgets.QMenu(self.media_viewer)
                for title, actions in actions.iteritems():
                    subMenu = rightClickMenu.addMenu(title)
                    for action in actions:
                        subMenu.addAction(action)

            rightClickMenu.exec_(self.media_viewer.mapToGlobal(pos))

    def switch_item(self, item, data):
        path = data['filepath']
        text = data['display_name']
        user_data = data
        update_data = [(item, {'filepath': path, 'text': text, 'user_data': user_data})]
        self.media_viewer.update_items(update_data=update_data)

    def switch_items_to(self, items, department):
        update_data = []
        for item in items:
            item_data = item.data(QtCore.Qt.UserRole)
            shotname = item_data['shotname']
            if department in self.shot_data[shotname]['data']:
                new_shot_data = self.shot_data[shotname]['data'][department]
                if os.path.exists(new_shot_data['filepath']):
                    new_data = (item, {'filepath': new_shot_data['filepath'], 'text': new_shot_data['display_name'], 'user_data': new_shot_data})
                    update_data.append(new_data)
        if update_data:       
            self.media_viewer.update_items(update_data=update_data)

    def get_departments_from_shot(self, shotname):
        if self.shot_data[shotname]:
            data = self.shot_data[shotname]['data']
            return data.keys()

    def play_media(self, items=None):
        if not items:
            items = self.media_viewer.selectedItems()
            if not items:
                return
        elif not isinstance(items, (list, tuple)):
            items = [items]

        media_paths = []
        for item in items:
            shot_info = item.data(QtCore.Qt.UserRole)
            path = shot_info['filepath']
            dept = shot_info['department']
            if os.path.exists(path):

                # try search and replace the path using self.departments
                if dept in self.departments.keys() and self.departments[dept]:
                    dirname = os.path.dirname(path)
                    basename = os.path.basename(path)
                    new_path = '{}/{}'.format(dirname, basename.replace(self.departments[dept][0], self.departments[dept][1]))
                    if os.path.exists(new_path):
                        path = new_path
                media_paths.append(path)
            else:
                print 'Cannot find media: {}'.format(path)

        mods = QtWidgets.QApplication.keyboardModifiers()
        if media_paths:
            if not self.chosen_app or mods & QtCore.Qt.ControlModifier:
                # pop up user to choose application to play media
                qmsgBox = QtWidgets.QMessageBox(self)
                # set tile, icon
                qmsgBox.setWindowTitle('Play Media')
                qmsgBox.setIcon(QtWidgets.QMessageBox.Question)

                # set text
                text = 'Open media with?'
                qmsgBox.setText(text)

                # set detailed text
                detailedText = 'Playlist: \n- '
                detailedText += '\n- '.join([os.path.basename(path) for path in media_paths])
                qmsgBox.setDetailedText(detailedText)

                # add buttons
                for app_name in self.player_app:
                    qmsgBox.addButton(app_name, QtWidgets.QMessageBox.AcceptRole)
                cancel_button = qmsgBox.addButton('Cancel', QtWidgets.QMessageBox.RejectRole)
                qmsgBox.exec_()
                clicked_button = qmsgBox.clickedButton()
                if clicked_button != cancel_button:
                    self.chosen_app = clicked_button.text()
                    func = self.player_app[clicked_button.text()]
                    func(media_paths)
            else:
                func = self.player_app[self.chosen_app]
                func(media_paths)
    
    def play_keyframepro(self, paths):
        app_path = config.Software.pythonPath
        if not os.path.exists(app_path):
            print 'Cannot find application: {}'.foramt(app_path)
            return
        script_path = '{}/rf_utils/keyframepro_utils.py'.format(config.Env.core)
        input_paths = ['-i']
        for path in paths:
            input_paths.append(path)
        commands = [app_path, script_path] + input_paths
        subprocess.Popen(commands)

    def play_rv(self, paths):
        app_path = config.Software.rv
        if not os.path.exists(app_path):
            print 'Cannot find application: {}'.foramt(app_path)
            return
        commands = [app_path] + paths
        subprocess.Popen(commands)

    def open_media_dir(self):
        selItems = self.media_viewer.selectedItems()
        if not selItems:
            return
        for item in selItems:
            shot_info = item.data(QtCore.Qt.UserRole)
            if 'publish_dir' in shot_info and shot_info['publish_dir'] and os.path.exists(shot_info['publish_dir']):
                subprocess.Popen(r'explorer {}'.format(shot_info['publish_dir'].replace('/', '\\')))
    
    # def autoplay_toggled(self):
    #     if self.autoplay_checkbox.isChecked():
    #         self.media_viewer.auto_play = True
    #         self.set_item_toolTips(text='')
    #     else:
    #         self.media_viewer.auto_play = False
    #         self.set_item_toolTips(text='Middle mouse drag to flip through frames')

    def set_item_toolTips(self, text):
        # set tool tip for items
        for i in xrange(self.media_viewer.count()):
            item = self.media_viewer.item(i)
            item.setToolTip(text)
 
    def update_item_size(self):
        size = self.gridsize_slider.value() * 0.001
        self.media_viewer.item_size = (self._render_size[0], self._render_size[1])
        self.media_viewer.setIconSize(QtCore.QSize(self._render_size[0]*size, self._render_size[1]*size))

    # def editing_quality(self):
    #     pass
    #     # self.media_viewer.clear()

    # def quality_changed(self):
    #     quality = self.quality_slider.value() * 0.01
    #     self.media_viewer.quality = quality
    #     # self.media_viewer.clear()
    #     self.update_item_size()
    #     self.update_viewer()

    def thread_fetch_data(self, entity): 
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        worker = thread_pool.Worker(self.fetch_data, entity)
        worker.signals.result.connect(self.list_media)
        self.threadpool.start(worker)

    def fetch_data(self, entity): 
        fetch_results = OrderedDict()
        for dept in self.departments.keys():
            sg_data = sg_process.get_versions_from_sequence(entity.project, 
                                                            entity.episode, 
                                                            entity.sequence, 
                                                            dept, 
                                                            extraFields=['entity.Shot.tags'])
            # print sg_data
            fetch_results[dept] = sg_data 

        return fetch_results

    def list_media(self, fetch_results):
        self.shot_data = OrderedDict()   # {shotName: {cut_order:n, data:{departmentA: {...}, departmentB: {...}}}}
        for dept, dept_data in fetch_results.iteritems():
            for shot, data in dept_data.iteritems():
                if shot not in self.shot_data:
                    self.shot_data[shot] = OrderedDict()
                if 'data' not in self.shot_data[shot]:
                    self.shot_data[shot]['data'] = OrderedDict()
                if dept not in self.shot_data[shot]['data']:
                    self.shot_data[shot]['data'][dept] = OrderedDict()

                # get cut order
                if 'cut_order' not in self.shot_data[shot]:
                    self.shot_data[shot]['cut_order'] = data['sg_cut_order']
                local_data = OrderedDict({'shotname': shot})
                # get only the latest version
                latest_version = data['versions'][-1]
                # get version name
                local_data['version_name'] = latest_version['code']
                # tags
                local_data['tags'] = latest_version['entity.Shot.tags']
                # shot id
                local_data['entity'] = latest_version['entity']
                # get ID
                local_data['id'] = latest_version['id']
                # get path
                media_path = latest_version['sg_path_to_movie']
                if media_path and os.path.exists(media_path):
                    local_data['filepath'] = media_path
                    local_data['publish_dir'] = os.path.dirname(media_path)
                else:
                    local_data['filepath'] = ''
                    local_data['publish_dir'] = ''

                # get user
                user = ''
                if latest_version['user']:
                    user = latest_version['user']['name']
                local_data['user'] = user
                # get date
                local_data['date'] = latest_version['created_at']
                # get description
                description = u''
                if latest_version['description']:
                    description = latest_version['description'].decode(encoding='UTF-8', errors='strict')
                local_data['description'] = description
                # get status
                local_data['status'] = latest_version['sg_task.Task.sg_status_list']

                # make display name for UI
                local_data['display_name'] = '{}_{}'.format(data['sg_shortcode'], dept)
                # department
                local_data['department'] = dept

                self.shot_data[shot]['data'][dept] = local_data

        # update UI
        self.update_viewer()
        # QtWidgets.QApplication.restoreOverrideCursor()

    def update_viewer(self):
        self.media_viewer.clear()
        sorted_data = self.sort_data()
        current_department_selected = self.dept_combobox.currentText()
        if self.only_checkbox.isChecked():
            sel_departments = [current_department_selected]
        else:
            top_department = current_department_selected
            departments = self.departments.keys()
            top_index = departments.index(top_department)
            sel_departments = []
            for i in xrange(top_index):
                sel_departments.append(departments[i])
            sel_departments.append(top_department)

        view_data = []
        for shot_name, shot_data in sorted_data.iteritems():
            for dept in sel_departments[::-1]:  # inverse the list so later dept comes first
                if not dept in shot_data['data']:
                    continue
                path = shot_data['data'][dept]['filepath']
                if path:
                    text = shot_data['data'][dept]['display_name']
                    user_data = shot_data['data'][dept]
                    view_data.append((path, text, user_data))
                    break

        if view_data:
            self.media_viewer.add_items(media_info=view_data)
        QtWidgets.QApplication.restoreOverrideCursor()

    def update_info(self):
        self.reset_info_panel()
        sel_items = self.media_viewer.selectedItems()
        num_items = len(sel_items)
        if num_items == 1:
            itemData = sel_items[0].data(QtCore.Qt.UserRole)
            if itemData:
                # update shot nasme
                shotname = itemData['shotname']
                self.shot_value_label.setStyleSheet('color: white')
                self.shot_value_label.setFont(self.active_font)
                self.shot_value_label.setText(shotname)

                # update creator
                creator = itemData['user']
                self.creator_value_label.setStyleSheet('color: white')
                self.creator_value_label.setFont(self.active_font)
                self.creator_value_label.setText(creator)

                # update date
                date = itemData['date']
                self.date_value_label.setStyleSheet('color: white')
                self.date_value_label.setFont(self.active_font)
                self.date_value_label.setText(str(datetime.strftime(date, '%Y/%m/%d - %H:%M:%S')))

                # update status
                status = itemData['status']
                self.status_value_label.text_label.setStyleSheet('color: white')
                self.status_value_label.text_label.setFont(self.active_font)
                self.status_value_label.set_status(status)

        elif num_items > 1:
            self.set_multiple_info()

        if num_items > 0:
            # update description ,tag
            tags = OrderedDict()
            desc_texts = OrderedDict()
            for item in sel_items:
                itemData = item.data(QtCore.Qt.UserRole)
                # description
                # update description
                desc = itemData.get('description')
                if desc:
                    desc_texts[itemData['shotname']] = desc

                # tags
                item_tags = itemData['tags']
                for tag in item_tags:
                    tag_name = tag.get('name')
                    if tag_name not in tags:
                        tags[tag_name] = tag

            if desc_texts:
                if len(desc_texts) > 1:
                    description = '\n\n'.join(['- {}:\n"{}"'.format(i[0], i[1]) for i in desc_texts.items()])
                else:
                    description = desc_texts.values()[0]

                self.description_textEdit.setStyleSheet('color: white; background-color: rgb{};'.format(self.bg_col))
                self.description_textEdit.setFont(self.display_font)
                self.description_textEdit.setPlainText(description)
            if tags:
                self.hashtagWidget.blockSignals(True)
                self.hashtagWidget.add_items(tags.items(), check=False)
                self.hashtagWidget.blockSignals(False)

    def reset_info_panel(self):
        self.shot_value_label.setStyleSheet('color: grey')
        self.shot_value_label.setFont(self.display_font)
        self.shot_value_label.setText('N/A')

        self.creator_value_label.setText('N/A')
        self.creator_value_label.setStyleSheet('color: grey')
        self.creator_value_label.setFont(self.display_font)

        self.status_value_label.text_label.setStyleSheet('color: grey')
        self.status_value_label.text_label.setFont(self.display_font)
        self.status_value_label.set_status('N/A')

        self.date_value_label.setStyleSheet('color: grey')
        self.date_value_label.setFont(self.display_font)
        self.date_value_label.setText('N/A')
        
        self.description_textEdit.setPalette(self.grey_font_palette)
        self.description_textEdit.setFont(self.display_font)
        self.description_textEdit.clear()

        self.hashtagWidget.clear()

    def set_multiple_info(self):
        self.shot_value_label.setStyleSheet('color: grey')
        self.shot_value_label.setFont(self.display_font)
        self.shot_value_label.setText(self.multiple_value_str)

        self.creator_value_label.setStyleSheet('color: grey')
        self.creator_value_label.setFont(self.display_font)
        self.creator_value_label.setText(self.multiple_value_str)

        self.date_value_label.setStyleSheet('color: grey')
        self.date_value_label.setFont(self.display_font)
        self.date_value_label.setText(self.multiple_value_str)
        
        self.status_value_label.text_label.setStyleSheet('color: grey')
        self.status_value_label.text_label.setFont(self.display_font)
        self.status_value_label.set_status(self.multiple_value_str)

        self.description_textEdit.setStyleSheet('color: grey')
        self.description_textEdit.setFont(self.display_font)
        self.description_textEdit.setPlainText(self.multiple_value_str)

    def disable_description_edit(self, force=True):
        # set toggle off
        self.edit_description_button.blockSignals(True) if force else None
        self.edit_description_button.setChecked(False)
        self.edit_description_button.blockSignals(False) if force else None

        # set non editable
        self.description_textEdit.setTextInteractionFlags(QtCore.Qt.NoTextInteraction)
        # set look
        self.description_textEdit.setFont(self.display_font)
        self.description_textEdit.setPalette(self.grey_font_palette)
        self.description_textEdit.setStyleSheet('background-color: rgb{};'.format(self.bg_col))

    def enable_description_edit(self):
        # set editable
        self.description_textEdit.setTextInteractionFlags(QtCore.Qt.TextEditorInteraction)
        # set look
        self.description_textEdit.setPalette(self.white_font_palette)
        self.description_textEdit.setFont(self.active_font)
        self.description_textEdit.setFocus()
        self.description_textEdit.moveCursor(QtGui.QTextCursor.End)
        self.description_textEdit.setStyleSheet('background-color: rgb{};'.format(self.edit_col))

    def edit_description_toggled(self):
        items = self.media_viewer.selectedItems()
        if not items:
            self.error_no_selection('Edit Description', 'Please select a shot media!')
            self.disable_description_edit()
            return

        num_items = len(items)
        if num_items == 1:
            version = items[0].data(QtCore.Qt.UserRole)
            version_desc = version['description'] if version['description'] else ''
        else:
            version_desc = self.multiple_value_str

        # start editing
        if self.edit_description_button.isChecked():
            # clear < multple values > text before allowing user to edit
            if num_items > 1:
                self.description_textEdit.clear()
            self.enable_description_edit()
        else: # finish editing
            current_text = self.description_textEdit.toPlainText()
            if current_text != self.multiple_value_str:
                # popup question to user
                qmsgBox = QtWidgets.QMessageBox(self)
                qmsgBox.setText('Edit shot media description?')
                qmsgBox.setWindowTitle('Confirm')
                qmsgBox.setIcon(QtWidgets.QMessageBox.Question)
                qmsgBox.addButton('  Yes  ', QtWidgets.QMessageBox.AcceptRole)
                qmsgBox.addButton('  No  ', QtWidgets.QMessageBox.RejectRole)
                answer = qmsgBox.exec_()
                if answer == 0:
                    for item in items:
                        version = item.data(QtCore.Qt.UserRole)
                        try:
                            # update shotgun data
                            update_description(version_id=version['id'], description=current_text)
                            
                            # also update local data stored inside item
                            version['description'] = current_text
                            item.setData(QtCore.Qt.UserRole, version)
                        except Exception, e:
                            self.description_textEdit.setPlainText(version_desc)
            else:
                # set text back to previous text
                self.description_textEdit.setPlainText(version_desc)
            # disable edit UI
            self.disable_description_edit()

    def tag_edited(self):
        sel_items = self.media_viewer.selectedItems()
        if not sel_items:
            return

        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)

        # get tag SG data to update
        tags = self.hashtagWidget.get_all_item_data()
        existing_tags = sg_process.sg.find('Tag', [], ['name'])
        existing_tag_names = [str(t['name']) for t in existing_tags]
        valid_tags = []
        for tagName, tag in tags:
            if str(tagName) not in existing_tag_names:
                tag = sg_process.sg.create('Tag', {'name': tagName})
            valid_tags.append(tag)
        tags_data = {'tags': valid_tags}

        # batch update SG
        batch_data = []
        for item in sel_items: 
            item_data = item.data(QtCore.Qt.UserRole)
            shot_id = item_data.get('entity')['id']
 
            data = {"request_type": "update", 
                    "entity_type": "Shot", 
                    "entity_id": shot_id, 
                    "data": tags_data}
            batch_data.append(data)
        result = sg_process.sg.batch(batch_data)

        # update UI
        if result:
            result_ids = [r.get('id') for r in result]
            for item in sel_items:
                item_data = item.data(QtCore.Qt.UserRole)
                shot_id = item_data.get('entity')['id']
                if shot_id in result_ids:
                    item_data['tags'] = valid_tags
                    item.setData(QtCore.Qt.UserRole, item_data)
            self.update_info()
        QtWidgets.QApplication.restoreOverrideCursor()

    def error_no_selection(self, title, message):
        # pop up user to choose application to play media
        qmsgBox = QtWidgets.QMessageBox(self)
        # set tile, icon
        qmsgBox.setWindowTitle(title)
        qmsgBox.setText(message)
        qmsgBox.setIcon(QtWidgets.QMessageBox.Critical)

        # add button
        qmsgBox.addButton('  OK  ', QtWidgets.QMessageBox.AcceptRole)
        qmsgBox.exec_()

def update_description(version_id, description): 
    """ this function update description from task id 
    get task id from shot[self.taskname]['id']
    """ 
    data = {'description': description}
    return sg_process.sg.update('Version', version_id, data)