#!/usr/bin/env python
# -- coding: utf-8 --

import os
import sys
import subprocess
import glob
import re
from collections import OrderedDict
from datetime import datetime
from functools import partial
import time
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from Qt import QtCore
from Qt import QtGui
from Qt import QtWidgets

import rf_config as config
from rf_utils import thread_pool
from . import master
from rf_utils.widget import image_widget
# reload(image_widget)
from rf_utils.widget import textEdit
from rf_utils.widget import status_widget
from rf_utils.widget import collapse_widget
from rf_utils.sg import sg_process
from rf_utils import user_info
from rf_utils.pipeline import watermark
# reload(watermark)
# from rf_utils.widget.status_widget import Icon

isNuke = True
try:
    import nuke
    import nukescripts
    from rf_nuke.utils import shared_comp
    reload(shared_comp)
except ImportError:
    isNuke = False

modulePath = sys.modules[__name__].__file__.replace('\\', '/')
moduleDir  = os.path.dirname(modulePath)
appModuleDir = os.path.dirname(moduleDir)

class Ui(master.Ui):
    """docstring for Ui"""
    def __init__(self, thread=None, parent=None):
        super(Ui, self).__init__(thread=thread, parent=parent)
        self.mastershots = []
        self.player_app = {'Keyframe Pro': self.play_keyframepro, 'RV': self.play_rv}
        self.chosen_app = None

        self.item_icon_size = (128, 128)
        self.shotname_font = QtGui.QFont('Calibri', 32)
        self.shotname_color = QtGui.QColor('white')
        self.viewer_background_color = QtGui.QColor(50, 50, 50)

        self.colorscript_data = {'Shot':{}, 'Sequence':{}}
        self.new_item_duration = 3  # anything newer than 3 days will have "new" icon

        self.nopreivew_path = '{}/icons/nopreview_icon.png'.format(appModuleDir)
        self.star_icon = '{}/icons/star_icon.png'.format(appModuleDir)
        self.new_icon = '{}/icons/new_icon.gif'.format(appModuleDir)

        self.description_display_font = QtGui.QFont()
        self.description_display_font.setItalic(True)
        self.description_display_font.setPointSize(8)

        self.approved_border_color = QtGui.QColor('green')
        self.inprogress_border_color = QtGui.QColor('red')

        self.grey_font_palette = QtGui.QPalette()
        self.grey_font_palette.setBrush(QtGui.QPalette.Text, QtGui.QBrush(QtGui.QColor(128, 128, 128)))

        self.setupUi()
        self.init_signal()
        self.reset_info_panel()
        self.set_default()

    def setupUi(self):
        # setup splitter
        self.mainSplitter = QtWidgets.QSplitter()
        self.mainSplitter.setOrientation(QtCore.Qt.Horizontal)

        self.infoSplitWidget = QtWidgets.QWidget(self.mainSplitter)
        self.viewSplitWidget = QtWidgets.QWidget(self.mainSplitter)
        self.mainSplitter.setSizes([95, 300])

        # info layout
        self.info_layout = QtWidgets.QVBoxLayout(self.infoSplitWidget)
        self.info_layout.setContentsMargins(6, 6, 6, 6)
        self.info_layout.setSpacing(3)

        # view layout 
        self.view_layout = QtWidgets.QHBoxLayout(self.viewSplitWidget)
        self.view_layout.setContentsMargins(0, 0, 0, 0)

        self.main_layout.addWidget(self.mainSplitter)

        # -------------------
        # media layout
        self.media_layout = QtWidgets.QHBoxLayout()
        # self.image_viewer = image_widget.ImageSingleViewer()
        # self.image_viewer.setImage("P:/Hanuman/scene/publ/act1/hnm_act1_q0030g_all/colorscript/main/hero/outputMedia/hnm_act1_q0030g_all_clrscpt_main.hero.jpg")

        self.image_viewer = image_widget.ImageGridViewer(imagePerRow=4)    
        self.media_layout.addWidget(self.image_viewer)
        self.view_layout.addLayout(self.media_layout)

        # button layout
        self.button_layout = QtWidgets.QVBoxLayout()
        self.button_layout.setSpacing(5)
        # view button
        self.view_button = QtWidgets.QPushButton('')
        self.view_button.setFixedSize(QtCore.QSize(40, 22))
        self.view_button.setIcon(QtGui.QIcon('{}/icons/eye_icon.png'.format(appModuleDir)))
        # dir button
        self.open_dir_button = QtWidgets.QPushButton('')
        self.open_dir_button.setFixedSize(QtCore.QSize(40, 22))
        self.open_dir_button.setIcon(QtGui.QIcon('{}/icons/folder_icon.png'.format(appModuleDir)))
        spacerItem1 = QtWidgets.QSpacerItem(25, 25, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        # import button
        self.import_button = QtWidgets.QPushButton('')
        self.import_button.setIcon(QtGui.QIcon('{}/icons/import2_icon.png'.format(appModuleDir)))
        self.import_button.setFixedSize(QtCore.QSize(40, 36))

        self.button_layout.addWidget(self.view_button)
        self.button_layout.addWidget(self.open_dir_button)
        self.button_layout.addWidget(self.import_button)

        self.button_layout.addItem(spacerItem1)
        self.media_layout.addLayout(self.button_layout)

        # status layout
        self.status_layout = QtWidgets.QFormLayout()
        self.status_layout.setLabelAlignment(QtCore.Qt.AlignLeft)
        self.status_layout.setFormAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)
        self.status_layout.setSpacing(3)
        self.info_layout.addLayout(self.status_layout)
        
        self.shot_value_label = QtWidgets.QLabel()
        self.creator_value_label = QtWidgets.QLabel()
        self.date_value_label = QtWidgets.QLabel()
        self.status_value_label = status_widget.StatusLabel()

        self.status_layout.addRow('Shot: ', self.shot_value_label)
        self.status_layout.addRow('Creator: ', self.creator_value_label)
        self.status_layout.addRow('Updated: ', self.date_value_label)
        self.status_layout.addRow('Status: ', self.status_value_label)
        # self.status_layout.addRow('Description:', None)

        # description 
        self.description_label = QtWidgets.QLabel('Description')
        self.info_layout.addWidget(self.description_label)

        self.description_textEdit = textEdit.TextEditWithPlaceHolder()
        # self.description_textEdit.setMinimumHeight(50)
        self.description_textEdit.setPlaceholderText('< No description >')
        self.description_textEdit.setWordWrapMode(QtGui.QTextOption.WrapAtWordBoundaryOrAnywhere)
        self.description_textEdit.setTextInteractionFlags(QtCore.Qt.NoTextInteraction)
        self.description_textEdit.setFont(self.description_display_font)
        self.description_textEdit.setPalette(self.grey_font_palette)
        self.info_layout.addWidget(self.description_textEdit)

        # ----- display settings
        # self.grid_option_groupBox = QtWidgets.QGroupBox()
        # self.grid_option_groupBox.setTitle('Display')
        self.grid_option_groupBox = collapse_widget.CollapsibleBox('Display options')
        self.info_layout.addWidget(self.grid_option_groupBox)
        self.grid_option_layout = QtWidgets.QVBoxLayout(self.grid_option_groupBox)

        # sequence mode checkbox
        self.seq_mode_checkbox = QtWidgets.QCheckBox('Sequence mode')
        self.seq_mode_checkbox.setChecked(False)  # disable shot mode
        self.grid_option_layout.addWidget(self.seq_mode_checkbox)
        
        # hide blank image checkbox
        self.hide_blank_checkbox = QtWidgets.QCheckBox('Hide blank image')
        self.grid_option_layout.addWidget(self.hide_blank_checkbox)
        self.hide_blank_checkbox.setVisible(False)  # hide it for now
        
        # hide borders checkbox
        self.hide_borders_checkbox = QtWidgets.QCheckBox('Hide color frame')
        self.grid_option_layout.addWidget(self.hide_borders_checkbox)

        # hide text checkbox
        self.hide_text_checkbox = QtWidgets.QCheckBox('Hide texts')
        self.grid_option_layout.addWidget(self.hide_text_checkbox)

        # hide icon checkbox
        self.hide_icon_checkbox = QtWidgets.QCheckBox('Hide icons')
        self.grid_option_layout.addWidget(self.hide_icon_checkbox)

        self.gap_layout = QtWidgets.QGridLayout()
        self.gap_layout.setRowMinimumHeight(0, 20)
        self.gap_layout.setRowMinimumHeight(1, 20)
        # self.gap_layout.setColumnStretch(0, 1)
        # self.gap_layout.setColumnStretch(1, 5)
        self.grid_option_layout.addLayout(self.gap_layout)

        # images per row setting
        self.grid_label = QtWidgets.QLabel('Images per row')
        self.gap_layout.addWidget(self.grid_label, 0, 0, 1, 1, QtCore.Qt.AlignLeft)
        self.imagePerRow_spinBox = QtWidgets.QSpinBox()
        self.imagePerRow_spinBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.imagePerRow_spinBox.setButtonSymbols(QtWidgets.QAbstractSpinBox.UpDownArrows)
        self.imagePerRow_spinBox.setMinimum(1)
        self.imagePerRow_spinBox.setMaximum(30)
        self.gap_layout.addWidget(self.imagePerRow_spinBox, 0, 1, 1, 1, QtCore.Qt.AlignLeft)

        # font size
        self.fontsize_label = QtWidgets.QLabel('Font size')
        self.gap_layout.addWidget(self.fontsize_label, 1, 0, 1, 1, QtCore.Qt.AlignLeft)
        self.fontsize_spinBox = QtWidgets.QSpinBox()
        self.fontsize_spinBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.fontsize_spinBox.setButtonSymbols(QtWidgets.QAbstractSpinBox.UpDownArrows)
        self.fontsize_spinBox.setMinimum(8)
        self.fontsize_spinBox.setMaximum(128)
        self.gap_layout.addWidget(self.fontsize_spinBox, 1, 1, 1, 1, QtCore.Qt.AlignLeft)

        # row spacing 
        self.rowSpacing_label = QtWidgets.QLabel('Row gap')
        self.gap_layout.addWidget(self.rowSpacing_label, 2, 0, 1, 1, QtCore.Qt.AlignLeft)
        self.rowSpacing_spinBox = QtWidgets.QSpinBox()
        self.rowSpacing_spinBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.rowSpacing_spinBox.setButtonSymbols(QtWidgets.QAbstractSpinBox.UpDownArrows)
        self.rowSpacing_spinBox.setMinimum(0)
        self.rowSpacing_spinBox.setMaximum(2000)
        self.gap_layout.addWidget(self.rowSpacing_spinBox, 2, 1, 1, 1, QtCore.Qt.AlignLeft)

        # column spacing 
        self.columnSpacing_label = QtWidgets.QLabel('Column gap')
        self.gap_layout.addWidget(self.columnSpacing_label, 3, 0, 1, 1, QtCore.Qt.AlignLeft)
        self.columnSpacing_spinBox = QtWidgets.QSpinBox()
        self.columnSpacing_spinBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.columnSpacing_spinBox.setButtonSymbols(QtWidgets.QAbstractSpinBox.UpDownArrows)
        self.columnSpacing_spinBox.setMinimum(0)
        self.columnSpacing_spinBox.setMaximum(2000)
        self.gap_layout.addWidget(self.columnSpacing_spinBox, 3, 1, 1, 1, QtCore.Qt.AlignLeft)
        self.grid_option_groupBox.setContentLayout(self.grid_option_layout)

        # ----- render settings
        # self.render_option_groupBox = QtWidgets.QGroupBox()
        # self.render_option_groupBox.setTitle('Snapshot')
        self.render_option_groupBox = collapse_widget.CollapsibleBox('Snapshot')
        self.info_layout.addWidget(self.render_option_groupBox)
        self.render_option_layout = QtWidgets.QVBoxLayout(self.render_option_groupBox)
        
        # render size
        self.snapshot_layout = QtWidgets.QGridLayout()
        self.snapshot_layout.setRowMinimumHeight(0, 20)
        self.snapshot_layout.setRowMinimumHeight(1, 20)
        # self.snapshot_layout.setColumnStretch(0, 3)
        # self.snapshot_layout.setColumnStretch(1, 7)
        self.render_option_layout.addLayout(self.snapshot_layout)

        self.width_label = QtWidgets.QLabel('Width')
        self.snapshot_layout.addWidget(self.width_label, 0, 0, 1, 1, QtCore.Qt.AlignLeft)

        self.render_width_spinBox = QtWidgets.QSpinBox()
        self.render_width_spinBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.render_width_spinBox.setButtonSymbols(QtWidgets.QAbstractSpinBox.UpDownArrows)
        self.render_width_spinBox.setMinimum(500)
        self.render_width_spinBox.setMaximum(12000)
        self.snapshot_layout.addWidget(self.render_width_spinBox, 0, 1, 1, 1, QtCore.Qt.AlignLeft)

        self.height_label = QtWidgets.QLabel('Height')
        self.snapshot_layout.addWidget(self.height_label, 1, 0, 1, 1, QtCore.Qt.AlignLeft)

        self.render_height_spinBox = QtWidgets.QSpinBox()
        self.render_height_spinBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.render_height_spinBox.setButtonSymbols(QtWidgets.QAbstractSpinBox.UpDownArrows)
        self.render_height_spinBox.setMinimum(500)
        self.render_height_spinBox.setMaximum(12000)
        self.snapshot_layout.addWidget(self.render_height_spinBox, 1, 1, 1, 1, QtCore.Qt.AlignLeft)

        self.border_label = QtWidgets.QLabel('Border')
        self.snapshot_layout.addWidget(self.border_label, 2, 0, 1, 1, QtCore.Qt.AlignLeft)

        self.border_spinBox = QtWidgets.QSpinBox()
        self.border_spinBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.border_spinBox.setButtonSymbols(QtWidgets.QAbstractSpinBox.UpDownArrows)
        self.border_spinBox.setMinimum(0)
        self.border_spinBox.setMaximum(100)
        
        self.border_spinBox.setSuffix('%')
        self.snapshot_layout.addWidget(self.border_spinBox, 2, 1, 1, 1, QtCore.Qt.AlignLeft)

        # snapshot button
        self.snapshot_button = QtWidgets.QPushButton('')
        self.snapshot_button.setIcon(QtGui.QIcon('{}/icons/camera_icon.png'.format(appModuleDir)))
        # self.snapshot_button.setFixedSize(QtCore.QSize(60, 25))
        self.render_option_layout.addWidget(self.snapshot_button)
        self.render_option_groupBox.setContentLayout(self.render_option_layout)

        # setup sizes
        self.media_layout.setStretch(0, 15)
        self.media_layout.setStretch(1, 1)
        self.view_layout.setStretch(0, 9)
        self.view_layout.setStretch(1, 3)
        self.view_layout.setStretch(2, 3)

        # toolTips
        self.view_button.setToolTip('Open with media viewer')
        self.open_dir_button.setToolTip('Open file explorer at color script directory')
        self.snapshot_button.setToolTip('Take snapshot of current display')
        self.import_button.setToolTip('Import selected color script into this session')

        self.seq_mode_checkbox.setToolTip('Display colorscript from sequence shot')
        self.hide_borders_checkbox.setToolTip('Hide colored coded frame on images')
        self.hide_text_checkbox.setToolTip('Hide text overlays on images')
        self.hide_icon_checkbox.setToolTip('Hide icon overlays on images')
        self.imagePerRow_spinBox.setToolTip('How many images you want in 1 row?')
        self.rowSpacing_spinBox.setToolTip('Gap in pixels between each row')
        self.columnSpacing_spinBox.setToolTip('Gap in pixels between each column')
        self.render_width_spinBox.setToolTip('Width of the snapshot image in pixels')
        self.render_height_spinBox.setToolTip('Height of the snapshot image in pixels')
        self.border_spinBox.setToolTip('Border area of the snapshot image in pixels')

    def refresh(self, entity): 
        super(Ui, self).refresh(entity)
        if not entity:
            return
        # self.entity inherit from parent class
        self.thread_fetch_data(self.entity)

        # set image viewer grid size
        render_size = self.entity.projectInfo.render.final_outputH()
        self.image_viewer.gridScale = render_size
        # set overlay icon size according to render size
        icon_size = int(((render_size[0] + render_size[1])*0.5) * 0.1)  # start icon at 10% the averaged render size
        self.item_icon_size = (icon_size, icon_size)

    def set_default(self):
        # background color
        self.image_viewer.setBackgroundBrush(self.viewer_background_color)
        # self.hide_text_checkbox.setChecked(True)
        self.imagePerRow_spinBox.setValue(4)
        self.rowSpacing_spinBox.setValue(50)
        self.fontsize_spinBox.setValue(16)
        self.columnSpacing_spinBox.setValue(50)
        self.render_width_spinBox.setValue(5000)
        self.render_height_spinBox.setValue(8000)
        self.border_spinBox.setValue(5)
        # border width
        self.image_viewer.borderWidth = 5
        # multiplier for displaying fullsize image
        self.image_viewer.fullSizeMultiplier = 1.0

        # hide nuke buttons
        if not isNuke:
            self.import_button.setVisible(False)

    def reset_info_panel(self):
        self.shot_value_label.setText('N/A')
        self.status_value_label.set_status('N/A')
        self.date_value_label.setText('N/A')
        self.creator_value_label.setText('N/A')
        
    def thread_fetch_data(self, entity): 
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        worker = thread_pool.Worker(self.fetch_data, entity)
        worker.signals.result.connect(self.list_colorscript)
        self.threadpool.start(worker)

    def fetch_data(self, entity): 
        version_dict = sg_process.get_versions_from_sequence(project=entity.project, 
                                                        episode=entity.episode, 
                                                        sequence=entity.sequence, 
                                                        taskname='colorscript')

        # query mastershot names
        self.mastershots = []
        mastershots = sg_process.get_mastershot(entity.project, entity.episode, entity.sequence)
        if mastershots:
            self.mastershots = [s['code'] for s in mastershots]
        return version_dict
        
    def list_colorscript(self, version_dict):
        temp_data = {'Shot':{}, 'Sequence':{}}
        self.colorscript_data = temp_data.copy()

        # sort_cut_order = self.sort_cutorder_checkbox.isChecked():
        for shot, data in version_dict.items(): 
            # print shot
            shot_name = shot
            
            # print 'Cut order', data['sg_cut_order']
            cut_order = data['sg_cut_order']

            # print 'Shot Type', data['sg_shot_type']
            shot_type = data['sg_shot_type']
            version_data = {}
            if data['versions']:
                latest_version = data['versions'][-1]
                version_data['shotname'] = shot_name

                # print '\tname', latest_version['code']
                version_data['name'] = latest_version['code']

                # print '\tpath', latest_version['sg_path_to_movie']
                version_data['path'] = latest_version['sg_path_to_movie']

                # print '\tuser', latest_version['user']['name']
                version_data['user'] = latest_version['user']['name']

                # print '\tdescription', latest_version['description']
                description = u''
                if latest_version['description']:
                    description = latest_version['description'].decode(encoding='UTF-8', errors='strict')
                version_data['description'] = description
                
                # print '\tdate', latest_version['created_at']
                version_data['date'] = latest_version['created_at']

                # print '\tstatus', latest_version['sg_task.Task.sg_status_list']
                version_data['status'] = latest_version['sg_task.Task.sg_status_list']

            temp_data[shot_type][shot] = version_data

        self.colorscript_data['Sequence'] = sorted(temp_data['Sequence'].items())
        self.colorscript_data['Shot'] = sorted(temp_data['Shot'].items())

        # update UI
        self.update_viewer()
        QtWidgets.QApplication.restoreOverrideCursor()

    def update_viewer(self):
        if self.seq_mode_checkbox.isChecked():
            shot_data = self.colorscript_data['Sequence']
        else:
            shot_data = self.colorscript_data['Shot']
        hide_blank_img = self.hide_blank_checkbox.isChecked()
        # self.image_viewer.useFullSize = True
        show_images = []
        if shot_data:
            # extract all paths
            all_paths = []
            all_datas = []
            for shot_name, version_data in shot_data:
                path = version_data['path'] if version_data['path'] else ''
                if '#' in path:
                    images = [im.replace('\\', '/') for im in glob.glob(path.replace('#', '*'))]
                    all_paths.extend(images)
                    for im in xrange(len(images)):
                        itemData = version_data.copy()
                        all_datas.append(itemData)
                else:
                    itemData = version_data.copy()
                    if not os.path.exists(path):
                        if hide_blank_img:
                            continue
                        else:
                            path = self.nopreivew_path
                            itemData = None
                    all_paths.append(path)
                    all_datas.append(itemData)

            overlayBorder = self.image_viewer.borderWidth
            datenow = datetime.now()

            for path, itemData in zip(all_paths, all_datas):
                basename = os.path.basename(path)
                fn, ext = os.path.splitext(basename)

                if itemData:
                    # toolTip data
                    itemData['__toolTip'] = fn

                    # frame border data
                    if itemData['status'] == 'apr':
                        color = self.approved_border_color
                    else:
                        color = self.inprogress_border_color
                    itemData['__border_color'] = color

                    # set texts data
                    itemData['__text'] = []
                    itemData['__pixmap'] = []
                    itemData['__text'].append((itemData['name'], 'bottomCenter', overlayBorder, self.shotname_font, self.shotname_color))

                    # show mastershot icon
                    if itemData['shotname'] in self.mastershots:
                        itemData['__pixmap'].append((self.star_icon, self.item_icon_size, 'topLeft', overlayBorder))

                    # show new icon
                    time_since_updated = datenow - itemData['date'].replace(tzinfo=None)
                    if time_since_updated.days <= self.new_item_duration:
                        itemData['__pixmap'].append((self.new_icon, self.item_icon_size, 'topRight', overlayBorder))

                    # actual file path attached to the item
                    itemData['filepath'] = path

                # data to be sent to viewer
                show_images.append((path, itemData))
        else:
            show_images = [(self.nopreivew_path, None)]

        # show all images
        self.update_grid_settings()
        self.image_viewer.showImages(images=show_images)
        self.reset_info_panel() 

        # connect loading finish signal to update UI
        self.image_viewer.loadFinished.connect(self.update_render_size)
        self.image_viewer.loadFinished.connect(self.hide_borders_toggled)
        self.image_viewer.loadFinished.connect(self.hide_text_toggled)
        self.image_viewer.loadFinished.connect(self.hide_icon_toggled)

    def update_render_size(self, *args):
        if not self.image_viewer.scene():
            return
        # update settings
        allRect = self.image_viewer.getItemsRect()
        self.render_width_spinBox.setValue(allRect.width())
        self.render_height_spinBox.setValue(allRect.height())

    def update_grid_settings(self):
        self.image_viewer.rowSpacing = self.rowSpacing_spinBox.value()
        self.image_viewer.columnSpacing = self.columnSpacing_spinBox.value()
        self.image_viewer.imagePerRow = self.imagePerRow_spinBox.value()

    def update_info(self, itemData=None):
        self.reset_info_panel()
        if itemData:
            # update shot nasme
            shotname = itemData['shotname']
            self.shot_value_label.setText(shotname)

            # update creator
            creator = itemData['user']
            self.creator_value_label.setText(creator)

            # update date
            date = itemData['date']
            self.date_value_label.setText(str(datetime.strftime(date, '%Y/%m/%d - %H:%M:%S')))

            # update status
            status = itemData['status']
            self.status_value_label.set_status(status)

            # update description
            if itemData['description']:
                self.description_textEdit.setPlainText(itemData['description'])

    def init_signal(self):
        self.view_button.clicked.connect(self.view_image)
        self.open_dir_button.clicked.connect(self.open_image_dir)
        self.snapshot_button.clicked.connect(self.take_snapshot)
        self.import_button.clicked.connect(self.import_colorscript)

        self.seq_mode_checkbox.toggled.connect(self.sequence_mode_toggled)
        self.hide_blank_checkbox.toggled.connect(self.update_viewer)
        self.hide_borders_checkbox.toggled.connect(self.hide_borders_toggled)
        self.hide_text_checkbox.toggled.connect(self.hide_text_toggled)
        self.hide_icon_checkbox.toggled.connect(self.hide_icon_toggled)
        self.image_viewer.itemSelected.connect(self.update_info)
        self.image_viewer.itemDoubleClicked.connect(self.view_image)

        # settings
        self.rowSpacing_spinBox.valueChanged.connect(self.rowSpacing_changed)
        self.columnSpacing_spinBox.valueChanged.connect(self.columnSpacing_changed)
        self.imagePerRow_spinBox.valueChanged.connect(self.imagePerRow_changed)
        self.fontsize_spinBox.valueChanged.connect(self.fontSize_changed)

    def rowSpacing_changed(self):
        self.image_viewer.rowSpacing = self.rowSpacing_spinBox.value()
        self.image_viewer.arrangeImages()
        self.update_render_size()

    def columnSpacing_changed(self):
        self.image_viewer.columnSpacing = self.columnSpacing_spinBox.value()
        self.image_viewer.arrangeImages()
        self.update_render_size()

    def imagePerRow_changed(self):
        self.image_viewer.imagePerRow = self.imagePerRow_spinBox.value()
        self.image_viewer.arrangeImages()
        self.update_render_size()

    def fontSize_changed(self):
        self.shotname_font.setPointSize(self.fontsize_spinBox.value())
        # set data attached to itmes
        for item in self.image_viewer.items:
            item_data = item.data(QtCore.Qt.UserRole).copy()
            if '__text' in item_data:
                new_datas = []
                for data in item_data['__text']:
                    new_data = [data[0], data[1] ,data[2], self.shotname_font, data[4]]
                    new_datas.append(new_data)
                item_data['__text'] = new_datas
                item.setData(QtCore.Qt.UserRole, item_data)
        self.image_viewer.set_item_decorations()
        self.update_render_size()

    def hide_borders_toggled(self):
        if self.hide_borders_checkbox.isChecked():
            self.image_viewer.hideBorders()
        else:
            self.image_viewer.showBorders()
    
    def hide_text_toggled(self):
        if self.hide_text_checkbox.isChecked():
            self.image_viewer.hideTexts()
        else:
            self.image_viewer.showTexts()

    def hide_icon_toggled(self):
        if self.hide_icon_checkbox.isChecked():
            self.image_viewer.hideOverlays()
        else:
            self.image_viewer.showOverlays()

    def view_image(self):
        currentItems = self.image_viewer.scene().selectedItems()
        if not currentItems:
            self.error_no_selection()
            return
        media_paths = []
        for item in currentItems:
            data = item.data(QtCore.Qt.UserRole)
            if isinstance(data, dict) and 'filepath' in data and os.path.exists(data['filepath']):
                media_paths.append(data['filepath'])
        if media_paths:
            self.launch_viewer_app(paths=media_paths)

    def open_image_dir(self):
        currentItems = self.image_viewer.scene().selectedItems()
        if not currentItems:
            self.error_no_selection()
            return
        open_dirs = set()
        for item in currentItems:
            data = item.data(QtCore.Qt.UserRole)
            if isinstance(data, dict) and 'filepath' in data:
                dirname = os.path.dirname(data['filepath'])
                if os.path.exists(dirname) and dirname not in open_dirs:
                    open_dirs.add(dirname)
                    subprocess.Popen(r'explorer {}'.format(dirname.replace('/', '\\')))

    def error_no_selection(self):
        # pop up user to choose application to play media
        qmsgBox = QtWidgets.QMessageBox(self)
        # set tile, icon
        qmsgBox.setWindowTitle('View Media')
        qmsgBox.setText('Please select at least 1 image!')
        qmsgBox.setIcon(QtWidgets.QMessageBox.Critical)

        # add button
        qmsgBox.addButton('  OK  ', QtWidgets.QMessageBox.AcceptRole)
        qmsgBox.exec_()

    def launch_viewer_app(self, paths):
        if not self.chosen_app:
            # pop up user to choose application to play media
            qmsgBox = QtWidgets.QMessageBox(self)
            # set tile, icon
            qmsgBox.setWindowTitle('View Media')
            qmsgBox.setIcon(QtWidgets.QMessageBox.Question)

            # set text
            text = 'Open media with?'
            qmsgBox.setText(text)

            # set detailed text
            detailedText = 'Playlist: \n- '
            detailedText += '\n- '.join([os.path.basename(path) for path in paths])
            qmsgBox.setDetailedText(detailedText)

            # add buttons
            for app_name in self.player_app:
                qmsgBox.addButton(app_name, QtWidgets.QMessageBox.AcceptRole)
            cancel_button = qmsgBox.addButton('Cancel', QtWidgets.QMessageBox.RejectRole)
            qmsgBox.exec_()
            clicked_button = qmsgBox.clickedButton()
            if clicked_button != cancel_button:
                app_name = clicked_button.text()
                self.chosen_app = app_name
                func = self.player_app[app_name]
                func(paths)
        else:
            func = self.player_app[self.chosen_app]
            func(paths)

    def play_keyframepro(self, paths):
        app_path = config.Software.pythonwPath
        if not os.path.exists(app_path):
            print 'Cannot find application: {}'.foramt(app_path)
            return
        script_path = '{}/rf_utils/keyframepro_utils.py'.format(config.Env.core)
        input_paths = ['-i']

        # check for image sequence pattern of keyframe Pro  name.####.ext
        prefixes = set()
        for path in paths:
            basename = os.path.basename(path)
            fn, ext = os.path.splitext(basename)
            m = re.match(r'(.+)\.([0-9]+)$', fn)
            if m:
                if m.group(1) not in prefixes:
                    prefixes.add(m.group(1))
                    input_paths.append(path)
            else:
                input_paths.append(path)
        commands = [app_path, script_path] + input_paths
        kfp = subprocess.Popen(commands, 
                    stdout=subprocess.PIPE)

    def play_rv(self, paths):
        app_path = config.Software.rv
        if not os.path.exists(app_path):
            print 'Cannot find application: {}'.foramt(app_path)
            return
        commands = [app_path] + paths
        subprocess.Popen(commands)

    def take_snapshot(self):
        render_size = (self.render_width_spinBox.value(), self.render_height_spinBox.value())
        verticalBorder = 1.0 - ((self.border_spinBox.value() * 0.01) * 0.1)  # slimmer side border
        horizontalBorder = 1.0 - (self.border_spinBox.value() * 0.01)
        chosen_path, temp_path = self.image_viewer.browseSaveImage(renderSize=render_size, 
                                                                verticalBorder=horizontalBorder,  
                                                                horizontalBorder=horizontalBorder,
                                                                writeToTemp=True)

        if chosen_path:
            # create burn in
            user = user_info.User()
            # convert shot entity to parent sequence
            entity = self.entity.copy()
            entity.context.update(entityCode='all', entity='none', step='colorscript', process=self.entity.process)
            entity.update_scene_context()
            result = watermark.add_border_hud_to_image(input_path=temp_path, 
                                                        output_path=chosen_path,  
                                                        topLeft=[entity.project],
                                                        topMid=[entity.name],
                                                        topRight=['Date: {}'.format(time.strftime('%d %b %Y - %H:%M:%S'))],
                                                        bottomLeft=['user: {}'.format(user.name()), 
                                                                'department: colorscript', 
                                                                'file: {}'.format(os.path.basename(chosen_path))],
                                                        bottomMid=[],
                                                        bottomRight=['type: {}'.format(entity.type),
                                                                'LOD: {}'.format(entity.res), 
                                                                'process: {}'.format(entity.process)],
                                                        fontScaleMultiplier={'topMid':2.0}, # make sequence name bigger
                                                        thickness=2)  
            # cleanup temp
            os.remove(temp_path)

            # popup to confirm success
            qmsgBox = QtWidgets.QMessageBox(self)
            qmsgBox.setText('Snapshot success:\n{}'.format(chosen_path))
            qmsgBox.setWindowTitle('Confirm')
            qmsgBox.setIcon(QtWidgets.QMessageBox.Information)
            qmsgBox.addButton('OK', QtWidgets.QMessageBox.AcceptRole)
            qmsgBox.exec_()

    def import_colorscript(self):
        if isNuke:
            currentItem = self.image_viewer.scene().selectedItems()
            if not currentItem:
                return
            data = currentItem[0].data(QtCore.Qt.UserRole)
            if isinstance(data, dict) and 'filepath' in data and os.path.exists(data['filepath']):
                read_arg = 'file {' + data['filepath'] + '}'
                node = nuke.createNode('Read', read_arg, inpanel=False) 

    def sequence_mode_toggled(self):
        # in sequence mode, we want full size image cuz user will zoom in a lot
        if self.seq_mode_checkbox.isChecked():
            self.image_viewer.useFullSize = True
        else:
            self.image_viewer.useFullSize = False

        self.update_viewer()

