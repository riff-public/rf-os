#!/usr/bin/env python
# -- coding: utf-8 --

import os
import sys
import subprocess
from collections import OrderedDict
from datetime import datetime
from functools import partial
import time
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from Qt import QtCore
from Qt import QtGui
from Qt import QtWidgets

from rf_utils import thread_pool
from . import master
# reload(master)
from rf_utils.sg import sg_process
# reload(sg_process)
from rf_utils import file_utils
from rf_utils.widget import media_widget
# reload(media_widget)
from rf_utils.widget import textEdit
from rf_utils.widget.status_widget import Icon
from rf_utils import user_info

isNuke = True
try:
    import nuke
    import nukescripts
    from rf_nuke.utils import shared_comp
    reload(shared_comp)
except ImportError:
    isNuke = False

modulePath = sys.modules[__name__].__file__.replace('\\', '/')
moduleDir  = os.path.dirname(modulePath)
appModuleDir = os.path.dirname(moduleDir)

class Ui(master.Ui):
    """docstring for Ui"""
    def __init__(self, thread=None, parent=None):
        super(Ui, self).__init__(thread=thread, parent=parent)
        # default target task for mastershot 
        self.taskname = 'comp_still'
        self.nopreivew_path = '{}/icons/nopreview_icon.png'.format(appModuleDir)

        self.description_active_font = QtGui.QFont()
        self.description_active_font.setItalic(False)
        self.description_active_font.setPointSize(8)

        self.description_display_font = QtGui.QFont()
        self.description_display_font.setItalic(True)
        self.description_display_font.setPointSize(8)

        self.grey_font_palette = QtGui.QPalette()
        self.grey_font_palette.setBrush(QtGui.QPalette.Text, QtGui.QBrush(QtGui.QColor(128, 128, 128)))

        self.white_font_palette = QtGui.QPalette()
        self.white_font_palette.setBrush(QtGui.QPalette.Text, QtGui.QBrush(QtGui.QColor(255, 255, 255)))

        self.setupUi()
        self.init_signal()
        self.set_default()

    def setupUi(self):
        self.mainSplitter = QtWidgets.QSplitter()
        self.mainSplitter.setOrientation(QtCore.Qt.Horizontal)

        self.shotSplitWidget = QtWidgets.QWidget(self.mainSplitter)
        self.viewSplitWidget = QtWidgets.QWidget(self.mainSplitter)
        self.mainSplitter.setSizes([85, 330])

        self.main_layout.addWidget(self.mainSplitter)

        # shotlist layout
        self.shotlist_layout = QtWidgets.QVBoxLayout(self.shotSplitWidget)
        self.shotlist_layout.setContentsMargins(6, 0, 6, 0)

        # label
        self.mastershot_label = QtWidgets.QLabel('Master shots')
        self.shotlist_layout.addWidget(self.mastershot_label)
        # list widget
        self.mastershot_listwidget = QtWidgets.QListWidget()
        self.shotlist_layout.addWidget(self.mastershot_listwidget)

        # view layout 
        self.view_layout = QtWidgets.QVBoxLayout(self.viewSplitWidget)
        self.view_layout.setContentsMargins(0, 0, 0, 0)

        # media layout
        self.media_layout = QtWidgets.QHBoxLayout()
        self.preview_label = media_widget.PreviewMedia()
        self.preview_label._loadMedia(self.nopreivew_path)
        self.media_layout.addWidget(self.preview_label)
        self.view_layout.addLayout(self.media_layout)

        # media button layout
        self.media_button_layout = QtWidgets.QVBoxLayout()
        self.media_button_layout.setSpacing(5)
        # play button
        self.play_media_button = QtWidgets.QPushButton('')
        self.play_media_button.setFixedSize(QtCore.QSize(40, 22))
        self.play_media_button.setIcon(QtGui.QIcon('{}/icons/play_icon.png'.format(appModuleDir)))
        # dir button
        self.open_media_dir_button = QtWidgets.QPushButton('')
        self.open_media_dir_button.setFixedSize(QtCore.QSize(40, 22))
        self.open_media_dir_button.setIcon(QtGui.QIcon('{}/icons/folder_icon.png'.format(appModuleDir)))
        spacerItem1 = QtWidgets.QSpacerItem(25, 25, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        # import button
        self.import_button = QtWidgets.QPushButton('')
        self.import_button.setIcon(QtGui.QIcon('{}/icons/import2_icon.png'.format(appModuleDir)))
        self.import_button.setFixedSize(QtCore.QSize(40, 36))

        self.import_browse_button = QtWidgets.QPushButton('')
        self.import_browse_button.setIcon(QtGui.QIcon('{}/icons/dot_icon.png'.format(appModuleDir)))
        self.import_browse_button.setFixedSize(QtCore.QSize(40, 22))

        self.media_button_layout.addWidget(self.play_media_button)
        self.media_button_layout.addWidget(self.open_media_dir_button)
        self.media_button_layout.addWidget(self.import_button)
        self.media_button_layout.addWidget(self.import_browse_button)
        self.media_button_layout.addItem(spacerItem1)
        self.media_layout.addLayout(self.media_button_layout)

        # description layout
        self.description_layout = QtWidgets.QVBoxLayout()
        self.description_layout.setSpacing(3)
        self.view_layout.addLayout(self.description_layout)

        # description button layout
        self.description_button_layout = QtWidgets.QHBoxLayout()
        self.description_label = QtWidgets.QLabel('Description: ')
        spacerItem2 = QtWidgets.QSpacerItem(25, 25, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.edit_description_button = QtWidgets.QPushButton('')
        self.edit_description_button.setCheckable(True)
        self.edit_description_button.setFixedSize(QtCore.QSize(40, 22))
        self.edit_description_button.setIcon(QtGui.QIcon('{}/icons/pencil_icon.png'.format(appModuleDir)))
        self.description_button_layout.addWidget(self.description_label)
        self.description_button_layout.addItem(spacerItem2)
        self.description_button_layout.addWidget(self.edit_description_button)
        self.description_layout.addLayout(self.description_button_layout)

        # description box
        self.description_textEdit = textEdit.TextEditWithPlaceHolder()
        self.description_textEdit.setPlaceholderText('< No description >')
        self.description_textEdit.setWordWrapMode(QtGui.QTextOption.WrapAtWordBoundaryOrAnywhere)
        self.description_textEdit.setTextInteractionFlags(QtCore.Qt.NoTextInteraction)
        self.description_textEdit.setFont(self.description_display_font)
        self.description_textEdit.setPalette(self.grey_font_palette)
        self.description_layout.addWidget(self.description_textEdit)

        # setup sizes
        self.media_layout.setStretch(0, 15)
        self.media_layout.setStretch(1, 1)
        self.view_layout.setStretch(0, 10)
        self.view_layout.setStretch(1, 2)
        self.description_layout.setStretch(0, 1)
        self.description_layout.setStretch(1, 3)

        # toolTips
        self.play_media_button.setToolTip('Play video with media player')
        self.open_media_dir_button.setToolTip('Open file explorer at media directory')
        self.import_button.setToolTip('Import selected mastershot into this session')
        self.import_browse_button.setToolTip('Browse mastershot directory for this sequence')
        self.edit_description_button.setToolTip('Edit mastershot description (Supervisor/Producer only)')

    def set_default(self):
        # only allow description edit button for sup, producer, admin
        user = user_info.User()
        if user.is_producer() or user.is_admin() or user.is_supervisor():
            self.edit_description_button.setVisible(True)
        else:
            self.edit_description_button.setVisible(False)

        if not isNuke:
            self.import_button.setVisible(False)
            self.import_browse_button.setVisible(False)

    def init_signal(self):
        self.mastershot_listwidget.currentItemChanged.connect(self.update_view)
        self.edit_description_button.toggled.connect(self.edit_description_toggled)
        self.play_media_button.clicked.connect(self.play_media)
        self.open_media_dir_button.clicked.connect(self.open_media_dir)
        self.import_button.clicked.connect(self.import_mastershot)
        self.import_browse_button.clicked.connect(self.browse_import_mastershot)

    def play_media(self):
        currentItem = self.mastershot_listwidget.currentItem()
        if not currentItem:
            return
        shot_info = currentItem.data(QtCore.Qt.UserRole)
        if os.path.exists(shot_info['fullres_path']):
            os.startfile(shot_info['fullres_path'])

    def open_media_dir(self):
        currentItem = self.mastershot_listwidget.currentItem()
        if not currentItem:
            return
        shot_info = currentItem.data(QtCore.Qt.UserRole)
        if os.path.exists(shot_info['publish_dir']):
            subprocess.Popen(r'explorer {}'.format(shot_info['publish_dir'].replace('/', '\\')))

    def browse_import_mastershot(self):
        if isNuke:
            entity = self.entity.copy()
            entity.context.update(entityCode='all', entity='none', step='comp', process='main', app='nuke')
            entity.update_scene_context()
            seq_all_path = entity.path.workspace().abs_path()
            browse_dir = None
            if os.path.exists(seq_all_path):
                browse_dir = seq_all_path

            shared_comp.import_comp(browse_dir=browse_dir)            

    def import_mastershot(self):
        if isNuke:
            currentItem = self.mastershot_listwidget.currentItem()
            selected_mastershot_name = ''
            if currentItem:
                # get seq all path
                entity = self.entity.copy()
                entity.context.update(entityCode='all', entity='none', step='comp', process='main', app='nuke')
                entity.update_scene_context()
                seq_all_path = entity.path.workspace().abs_path()

                # get mastershot filename
                shot_info = currentItem.data(QtCore.Qt.UserRole)
                selected_mastershot_name = '{}_master.nk'.format(shot_info['shot_code'])
                mastershot_fp = '{}/{}'.format(seq_all_path, selected_mastershot_name)
                if os.path.exists(mastershot_fp):
                    nuke.nodePaste(mastershot_fp)
                else:
                    # popup question to user
                    qmsgBox = QtWidgets.QMessageBox(self)
                    qmsgBox.setText('No mastershot comp found:\n{}'.format(mastershot_fp))
                    qmsgBox.setWindowTitle('Error')
                    qmsgBox.setIcon(QtWidgets.QMessageBox.Critical)
                    qmsgBox.addButton('OK', QtWidgets.QMessageBox.AcceptRole)
                    qmsgBox.exec_()

    def refresh(self, entity): 
        super(Ui, self).refresh(entity)
        if not entity:
            return
            
        # self.entity inherit from parent class
        self.thread_fetch_data(self.entity)

         # set media widget fps
        fps = self.entity.projectInfo.render.fps()
        self.preview_label.fps = fps
        self.preview_label.updateInfo()

    def thread_fetch_data(self, entity): 
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        worker = thread_pool.Worker(self.fetch_data, entity)
        worker.signals.result.connect(self.list_mastershot)
        self.threadpool.start(worker)

    def fetch_data(self, entity): 
        # raw data from Shotgun 
        shots = sg_process.get_mastershot(entity.project, entity.episode, entity.sequence)
        tasks = sg_process.get_task_from_entities(self.taskname, shots)

        # combine data to shot entity 
        for shot in shots: 
            for task in tasks: 
                if task['entity']['id'] == shot['id']: 
                    shot[self.taskname] = task
                    break 
        return shots 
    
    def list_mastershot(self, shots): 
        mastershot_info = []
        for shot in shots: 
            info = {}

            scene = self.entity.copy()
            scene.context.update(entityCode=shot['sg_shortcode'], entity='none')
            scene.update_scene_context()
            info['shot_code'] = shot['code']
            # print 'shot', shot['code']

            # ----- mov path
            scene.context.update(step='comp', process='main', app='nuke')
            publishdir = scene.path.scheme(key='outputHeroImgPath').abs_path()
            info['publish_dir'] = publishdir

            previewfile = scene.output_name(outputKey='previewOutput', hero=True)
            preview_filepath = '{}/{}'.format(publishdir, previewfile)
            info['preview_path'] = preview_filepath
            # print 'preview_filepath', preview_filepath, os.path.exists(preview_filepath)

            # optional for other 2 files 
            # scene.output_name(outputKey='reviewOutput', hero=True)
            # full rez path
            fullresfile = scene.output_name(outputKey='editOutput', hero=True)
            fullres_filepath = '{}/{}'.format(publishdir, fullresfile)
            info['fullres_path'] = fullres_filepath
            # print 'fullres_path', fullres_filepath, os.path.exists(fullres_filepath)

            # ----- nuke script path
            nukedir = scene.path.scheme(key='publishHeroWorkspacePath').abs_path()
            # this will generate name <shot>_comp_main.hero.nk which is the correct way 
            nukescript = scene.output_name(outputKey='workspace', hero=True) 
            # but nuke file in hero is directly copy from workspace
            # version name still in the file. we will list_lastest file instead 
            files = file_utils.list_file(nukedir)
            latest_nk = sorted(files)[-1] if files else ''
            nuke_filepath = '{}/{}'.format(nukedir, latest_nk)
            info['nuke_path'] = nuke_filepath
            # print 'nuke_filepath', nuke_filepath, os.path.exists(nuke_filepath)

            # ----- description
            description = shot[self.taskname]['sg_description']
            # needs to decode to support Thai
            if description:
                description = description.decode(encoding='UTF-8', errors='strict')

            # ----- assignees
            assignees = [i['name'] for i in shot[self.taskname]['task_assignees']]
            info['assignees'] = assignees
            # print 'assignees', assignees

            info['description'] = description
            # print 'description', description

            # ----- sg status
            status = shot[self.taskname]['sg_status_list']
            info['status'] = status
            # print 'sg status', status

            # ----- task id, this will be used later when updating description
            taskid = shot[self.taskname]['id']
            info['taskid'] = taskid
            # print 'taskid', taskid 
            # print '----------'

            mastershot_info.append(info)

        # update UI
        self.update_shotlist(mastershot_info=mastershot_info)
        QtWidgets.QApplication.restoreOverrideCursor()

    def update_shotlist(self, mastershot_info):
        # clear ui first
        self.mastershot_listwidget.clear()

        # add master shot to list
        for shot_info in mastershot_info:
            item = QtWidgets.QListWidgetItem(shot_info['shot_code'])
            # attach data into item
            item.setData(QtCore.Qt.UserRole, shot_info)

            # set sg status icon
            icon = QtGui.QIcon('{0}/{1}'.format(Icon.path, Icon.statusMap[shot_info['status']]['icon']))
            item.setIcon(icon)

            # set toolTip as assignee names
            toolTipTxt = ', '.join(shot_info['assignees'])
            item.setToolTip('Created by: {}'.format(toolTipTxt))

            self.mastershot_listwidget.addItem(item)

    def update_view(self):
        self.disable_description_edit()
        currentItem = self.mastershot_listwidget.currentItem()
        if not currentItem:
            self.clear_view()
            return

        shot_info = currentItem.data(QtCore.Qt.UserRole)

        # load video
        if os.path.exists(shot_info['preview_path']):
            self.preview_label.loadMedia([shot_info['preview_path']])
        else:
            self.preview_label.mediaplayer.clear()
            self.preview_label.clear_view()
            self.preview_label.loadMedia(self.nopreivew_path)

        # load description
        text = u''
        if shot_info['description']:
            text = shot_info['description']
        self.description_textEdit.setPlainText(text)

    def clear_view(self):
        self.preview_label.mediaplayer.clear()
        self.preview_label.clear_view()
        self.preview_label.loadMedia(self.nopreivew_path)
        self.description_textEdit.clear()

    def disable_description_edit(self):
        # set toggle off
        self.edit_description_button.blockSignals(True)
        self.edit_description_button.setChecked(False)
        self.edit_description_button.blockSignals(False)

        # set non editable
        self.description_textEdit.setTextInteractionFlags(QtCore.Qt.NoTextInteraction)
        # set look
        self.description_textEdit.setFont(self.description_display_font)
        self.description_textEdit.setPalette(self.grey_font_palette)

    def enable_description_edit(self):
        # set editable
        self.description_textEdit.setTextInteractionFlags(QtCore.Qt.TextEditorInteraction)
        # set look
        self.description_textEdit.setPalette(self.white_font_palette)
        self.description_textEdit.setFont(self.description_active_font)
        self.description_textEdit.setFocus()
        self.description_textEdit.moveCursor(QtGui.QTextCursor.End)

    def edit_description_toggled(self):
        currentItem = self.mastershot_listwidget.currentItem()
        if not currentItem:
            self.disable_description_edit()
            return

        shot_info = currentItem.data(QtCore.Qt.UserRole)
        # start editing
        if self.edit_description_button.isChecked():
            self.enable_description_edit()
        else: # finish editing
            current_text = self.description_textEdit.toPlainText()

            if current_text != shot_info['description']:
                # popup question to user
                qmsgBox = QtWidgets.QMessageBox(self)
                qmsgBox.setText('Edit master shot description?')
                qmsgBox.setWindowTitle('Confirm')
                qmsgBox.setIcon(QtWidgets.QMessageBox.Question)
                qmsgBox.addButton('  Yes  ', QtWidgets.QMessageBox.AcceptRole)
                qmsgBox.addButton('  No  ', QtWidgets.QMessageBox.RejectRole)
                answer = qmsgBox.exec_()
                if answer == 0: 
                    try:
                        # update shotgun data
                        update_description(taskid=shot_info['taskid'], description=current_text)
                        
                        # also update local data stored inside listwidget item
                        shot_info['description'] = current_text
                        currentItem.setData(QtCore.Qt.UserRole, shot_info)
                    except Exception, e:
                        self.description_textEdit.setPlainText(shot_info['description'])
                else:
                    # set text back to previous text
                    self.description_textEdit.setPlainText(shot_info['description'])
            # disable edit UI
            self.disable_description_edit()

def update_description(taskid, description): 
    """ this function update description from task id 
    get task id from shot[self.taskname]['id']
    """ 
    data = {'sg_description': description}
    return sg_process.sg.update('Task', taskid, data)
