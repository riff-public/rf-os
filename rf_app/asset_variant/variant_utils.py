from rf_utils.sg import sg_process
from rf_utils.context import context_info
sg = sg_process.sg 

ATTRS = ['asset_sg_parent_variants_assets', 'sg_parent_variants']
standard_attrs = ['sg_asset_type', 'sg_subtype', 'project', 'code', 'id']


def find_variants(project, asset_name): 
    variants = list()
    fields = ATTRS
    fields += standard_attrs

    # find variants or parent variants 
    entity = sg.find_one('Asset', [['project.Project.name', 'is', project], ['code', 'is', asset_name]], fields)
    variants = entity['asset_sg_parent_variants_assets']
    if variants: 
        variants = [entity] + variants
        return variants

    # find parent variants 
    if not variants: 
        parent_vars = entity['sg_parent_variants']
        if parent_vars: 
            variants = parent_vars
            filters = [['project', 'is', entity['project']]]
            advancedFilter1 = {
                                "filter_operator": "any",
                                "filters": [['id', 'is', a['id']] for a in parent_vars]
                            }
        
            filters.append(advancedFilter1)
            parent_vars = sg.find('Asset', filters, fields)

            for entity in parent_vars: 
                variants += entity['asset_sg_parent_variants_assets']
        else:
            variants = [entity]
        return variants


def variant_info(project, entities): 
    filters = [['project.Project.name', 'is', project]]
    fields = standard_attrs
    advancedFilter1 = {
                                "filter_operator": "any",
                                "filters": [['id', 'is', a['id']] for a in entities]
                            }
    filters.append(advancedFilter1)
    return sg.find('Asset', filters, fields)


def entity_to_context(project, entities): 
    contexts = list() 
    sg_entities = variant_info(project, entities)

    for entity in sg_entities: 
        context = context_info.Context()
        context.update(
            project=project, 
            entityType='asset', 
            entityGrp=entity['sg_asset_type'], 
            entity=entity['code'], 
            entityParent=entity['sg_subtype'])
        asset = context_info.ContextPathInfo(context=context)
        contexts.append(asset)

    return contexts


def test(): 
    va = find_variants('Bikey', 'tsumikiCargoMiniB')
    for a in va: 
        print(a)

    # {'type': 'Asset', 'id': 21349, 'name': 'tsumikiCargoMiniA'}
    # {'type': 'Asset', 'id': 21350, 'name': 'tsumikiCargoMiniB'}
    # {'type': 'Asset', 'id': 21351, 'name': 'tsumikiCargoMiniC'}
    # {'type': 'Asset', 'id': 21352, 'name': 'tsumikiCargoMiniD'}

    entities = entity_to_context('Bikey', va)
    for asset in entities: 
        asset.context.update(step='rig', process='main', res='md')
        # print(asset.path.hero())
        # print(asset.path.icon())
        print(asset.path.hero())
        print(asset.output_name(outputKey='rig', hero=True))

    # $RFPUBL/Bikey/asset/publ/prop/tsumikiCargoMiniA/hero
    # $RFPUBL/Bikey/asset/publ/prop/tsumikiCargoMiniB/hero
    # $RFPUBL/Bikey/asset/publ/prop/tsumikiCargoMiniC/hero
    # $RFPUBL/Bikey/asset/publ/prop/tsumikiCargoMiniD/hero
