#!/usr/bin/env python
# -*- coding: utf-8 -*-
# beta   - under development
# 1.0.0  - add file selection combobox
# 1.1.0  - add shader switch

_title = 'Asset Variant'
_version = '1.1.0'
_des = ''
uiName = 'AssetVariant'

#Import python modules
import sys
import os
import logging
import getpass
from functools import partial
from collections import OrderedDict, defaultdict

core = '{}/core'.format(os.environ.get('RFSCRIPT'))
sys.path.append(core)

# import config
import rf_config as config
from rf_utils import log_utils
from rf_utils.ui import stylesheet

user = '{}-{}'.format(config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

# QT
os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

# Maya modules
import pymel.core as pm
import maya.cmds as mc
import maya.mel as mel

import variant_utils
from rf_utils.context import context_info
from rf_utils.widget import file_widget
from rf_utils import file_utils
from rf_utils.pipeline import asset_tag
from rf_app.asm import asm_lib
from rftool.shade import shade_utils

moduleDir = os.path.dirname(sys.modules[__name__].__file__).replace('\\', '/')
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]
ORIGINAL_VARIANT_ATTR = asm_lib.ConfigAttr.originalVariant

class AssetVariant(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(AssetVariant, self).__init__(parent)
        # app vars
        self.asset = None
        self.is_asm = False
        self.__thumbnail_step = 'model'
        self.__thumbnail_process = 'main'
        self.__thumbnail_ext = '.png'

        # ui vars
        self.w = 500
        self.h = 400
        self.valid_color = 'yellow'
        self.__icon_size = 128
        self.no_preview = QtGui.QIcon('{}/icons/nopreview_icon.png'.format(moduleDir))

        # init functions
        self.setup_ui()
        self.init_signals()
        self.set_default()

    def setup_ui(self):
        self.setObjectName(uiName)
        self.setWindowTitle('{} {} {}'.format(_title, _version, _des))
        self.setWindowModality(QtCore.Qt.ApplicationModal)
        self.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))

        self.centralwidget = QtWidgets.QWidget(self)
        self.setCentralWidget(self.centralwidget)
        self.resize(self.w, self.h)

        # main layout
        self.main_layout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.main_layout.setContentsMargins(9, 9, 9, 9)
        self.main_layout.setSpacing(9)

        # ----- header layout
        self.header_layout = QtWidgets.QHBoxLayout()
        self.header_layout.setSpacing(5)
        self.main_layout.addLayout(self.header_layout)

        # asset label
        spacerItem1 = QtWidgets.QSpacerItem(24, 24, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.header_layout.addItem(spacerItem1)
        self.asset_label = QtWidgets.QLabel()
        self.header_layout.addWidget(self.asset_label)
        spacerItem2 = QtWidgets.QSpacerItem(24, 24, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.header_layout.addItem(spacerItem2)

        # ----- view layout
        # view splitter
        self.viewSplitter = QtWidgets.QSplitter()
        self.viewSplitter.setOrientation(QtCore.Qt.Horizontal)
        
        self.main_layout.addWidget(self.viewSplitter)

        self.namespaceSplitWidget = QtWidgets.QWidget(self.viewSplitter)
        self.variantSplitWidget = QtWidgets.QWidget(self.viewSplitter)

        self.view_layout = QtWidgets.QVBoxLayout(self.namespaceSplitWidget)
        self.view_layout.setSpacing(5)

        # namespace list
        self.name_listwidget = QtWidgets.QListWidget()
        self.name_listwidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.view_layout.addWidget(self.name_listwidget)

        self.variant_layout = QtWidgets.QVBoxLayout(self.variantSplitWidget)
        self.variant_layout.setSpacing(5)

        # variant list
        self.variant_listwidget = file_widget.ImageListWidget()
        self.variant_listwidget.listWidget.setIconSize(QtCore.QSize(self.__icon_size, self.__icon_size))
        self.variant_listwidget.listWidget.setMovement(QtWidgets.QListView.Static)
        self.variant_listwidget.listWidget.setResizeMode(QtWidgets.QListView.Adjust)
        self.variant_layout.addWidget(self.variant_listwidget)

        # file combobox
        self.file_layout = QtWidgets.QFormLayout()
        self.variant_layout.addLayout(self.file_layout)
        self.file_combobox = QtWidgets.QComboBox()
        self.file_combobox.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))
        self.file_layout.addRow('Target', self.file_combobox)

        # ----- bottom layout
        self.bottom_layout = QtWidgets.QHBoxLayout()
        self.bottom_layout.setSpacing(5)
        self.main_layout.addLayout(self.bottom_layout)

        # switch button
        spacerItem3 = QtWidgets.QSpacerItem(24, 24, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.bottom_layout.addItem(spacerItem3)
        self.switch_button = QtWidgets.QPushButton('Switch')
        self.switch_button.setFixedSize(QtCore.QSize(65, 25))
        self.bottom_layout.addWidget(self.switch_button)

        # cancle button
        self.close_button = QtWidgets.QPushButton('Close')
        self.close_button.setFixedSize(QtCore.QSize(65, 25))
        self.bottom_layout.addWidget(self.close_button)

        self.main_layout.setStretch(0, 0)
        self.main_layout.setStretch(1, 5)
        self.main_layout.setStretch(2, 0)
        self.viewSplitter.setSizes([300, 500])

    def init_signals(self):
        self.name_listwidget.itemSelectionChanged.connect(self.name_selection_changed)
        self.variant_listwidget.itemSelected.connect(self.variant_selected)
        self.switch_button.clicked.connect(self.switch_variant)
        self.close_button.clicked.connect(self.close)

    def set_default(self):
        self.populate()
        self.switch_button.setEnabled(False)
        
    def populate(self):
        # get selection
        sels = mc.ls(sl=True, type='transform')
        if not sels:
            self.asset = None
            return

        sels, asms = asm_lib.UiCmd().list_selection()
        input_path = None
        asset = None
        if asms:
            self.is_asm = True
            loc = asm_lib.MayaRepresentation(asms[0])
            context_info.logger.disabled = True
            asset = context_info.ContextPathInfo(path=loc.gpu_path)
            context_info.logger.disabled = False
            if asset.valid:
                input_path = loc.gpu_path
            else:
                self.asset = None
                return
            sels = [asm_lib.MayaRepresentation(a).shortname for a in asms]

        else:
            self.is_asm = False
            # figure out asset from selection
            context_info.logger.disabled = True  # disable logger in context
            for sel in sels:
                if mc.referenceQuery(sel, inr=True):
                    path = mc.referenceQuery(sel, f=True, withoutCopyNumber=True)
                    asset = context_info.ContextPathInfo(path=path)
                    if asset.valid:
                        input_path = path
                        break
            context_info.logger.disabled = False  # enable logger in context

            # return if there's no asset found
            if not asset:
                self.asset = None
                return

        self.asset = asset
        self.asset.input_path = input_path
        self.set_asset_label()
        self.populate_names()
        
        self.select_names(sels)
        self.name_selection_changed()
        self.populate_variant()

    def set_asset_label(self):
        if self.asset:
            self.asset_label.setText('{} - {}'.format(self.asset.type, self.asset.name))
            self.asset_label.setStyleSheet('color: {}'.format(self.valid_color))

    def populate_names(self):
        if self.is_asm:
            self.populate_setdress()
        else:
            self.populate_namespace()

    def populate_namespace(self):
        ref_nodes = mc.ls(type='reference')
        for rn in ref_nodes:
            try:
                input_path = mc.referenceQuery(rn, f=True, withoutCopyNumber=True)
            except RuntimeError:
                continue
            if input_path == self.asset.input_path:
                namespace = mc.referenceQuery(rn, namespace=True)[1:]
                topGrp = '{}:{}'.format(namespace, self.asset.projectInfo.asset.geo_grp())
                if mc.objExists(topGrp):
                    item = QtWidgets.QListWidgetItem(namespace)
                    item.setData(QtCore.Qt.UserRole, (rn, topGrp))
                    self.name_listwidget.addItem(item)

    def populate_setdress(self):
        all_asms = asm_lib.list_asms()
        for asm in all_asms:
            loc = asm_lib.MayaRepresentation(asm)
            if loc.gpu_path == self.asset.input_path:
                name = loc.shortname
                if mc.objExists(name):
                    item = QtWidgets.QListWidgetItem(name)
                    item.setData(QtCore.Qt.UserRole, (asm, name))
                    self.name_listwidget.addItem(item)

    def select_names(self, selections):
        self.name_listwidget.clearSelection()
        if self.is_asm:
            self.select_setdress_name(selections)
        else:
            self.select_namespace(selections)

    def select_namespace(self, selections):
        sel_namespaces = list(set([':'.join(s.split(':')[:-1]) for s in selections]))
        for i in range(self.name_listwidget.count()):
            item = self.name_listwidget.item(i)
            if item.text() in sel_namespaces:
                item.setSelected(True)

    def select_setdress_name(self, selections):
        for i in range(self.name_listwidget.count()):
            item = self.name_listwidget.item(i)
            if item.text() in selections:
                item.setSelected(True)

    def populate_variant(self):
        variants = variant_utils.find_variants(self.asset.project, self.asset.name)
        if variants:
            variant_entities = variant_utils.entity_to_context(self.asset.project, variants)
            for variant in variant_entities: 
                variant.context.update(step=self.__thumbnail_step, process=self.__thumbnail_process)
                img_dir = variant.path.output_hero_img().abs_path()
                thumbnail_img = self.no_preview
                if os.path.exists(img_dir):
                    thumbnail_path = file_utils.get_latest_file(img_dir, ext=self.__thumbnail_ext)
                    if thumbnail_path:
                        thumbnail_img = QtGui.QIcon(thumbnail_path.replace('\\', '/'))
                self.variant_listwidget.add_item(text=variant.name, data=variant, iconWidget=thumbnail_img)

    def name_selection_changed(self):
        objs = []
        if self.is_asm:
            for item in self.name_listwidget.selectedItems():
                asm, shotname = item.data(QtCore.Qt.UserRole)
                loc = asm_lib.MayaRepresentation(asm)
                objs.append(loc.longname)
        else:
            for item in self.name_listwidget.selectedItems():
                rn, topGrp = item.data(QtCore.Qt.UserRole)
                objs.append(topGrp)
        mc.select(objs, r=True)
        self.check_selection()

    def variant_selected(self):
        self.file_combobox.clear()
        # populate file
        try:
            item = self.name_listwidget.selectedItems()[0]
            selected_variant = self.variant_listwidget.selected_files()[0]
        except:
            return

        if self.is_asm:
            asm, shortname = item.data(QtCore.Qt.UserRole)
            loc = asm_lib.MayaRepresentation(asm)
            path = loc.gpu_path
            fn = os.path.basename(path)
        else:
            rn, topGrp = item.data(QtCore.Qt.UserRole)
            path = mc.referenceQuery(rn, f=True, withoutCopyNumber=True)
            fn = os.path.basename(path)

        # guess and distribute context value from filename
        asset = context_info.ContextPathInfo(path=path)
        outputList = asset.guess_outputKey(fn)
        outputKey, step = outputList[0]
        asset.context.update(step=step)
        asset.extract_name(fn, outputKey=outputKey)
        asset.context.update(entity=selected_variant.name)
        asset.output_name(outputKey=outputKey, hero=True)
        output_dir = asset.path.hero().abs_path()
        output_file = asset.output_name(outputKey=outputKey, hero=True)

        i = 0
        sel_index = None
        for f in os.listdir(output_dir):
            fp = '{}/{}'.format(output_dir, f)
            fn, ext = os.path.splitext(f)
            if not os.path.isfile(fp):
                continue
            if self.is_asm:
                if ext != '.abc' and '_gpu_' not in f:
                    continue
            else:
                if ext not in ('.ma', '.mb') or '_mtr_' in f or '_rig_' not in f:
                    continue
            self.file_combobox.addItem(f, fp)
            if f == output_file:
                sel_index = i
            i += 1
        if sel_index != None:
            self.file_combobox.setCurrentIndex(sel_index)

        self.check_selection()

    def check_selection(self):
        variants = self.variant_listwidget.selected_files()
        item = self.name_listwidget.selectedItems()
        file = self.file_combobox.currentText()
        if variants and item and file:
            self.switch_button.setEnabled(True)
        else:
            self.switch_button.setEnabled(False)

    def switch_variant(self):
        # get selected variant
        selected_variant = self.variant_listwidget.selected_files()[0]
        if self.is_asm:
            error = self.switch_setdress_variant(selected_variant=selected_variant)
        else:
            error = self.switch_ref_variant(selected_variant=selected_variant)

        # show pop-up if there's failed asset
        if error:
            detailedText = '===== Error List =====\n'
            to_sel = []
            for old, new, topGrp in error:
                detailedText += '\nCannot locate:\n{}\nto replace\n{}\n'.format(new, old)
                to_sel.append(topGrp)
            qmsgBox = QtWidgets.QMessageBox(self)
            qmsgBox.setText('Switch asset variant failed on {} asset(s).\nFailed assets will be selected.\n\nClick "Show Details" for more.'.format(len(error)))
            qmsgBox.setWindowTitle('Error Switching Variant')
            qmsgBox.setDetailedText(detailedText)
            qmsgBox.setIcon(QtWidgets.QMessageBox.Critical)
            qmsgBox.addButton('    OK    ', QtWidgets.QMessageBox.AcceptRole)
            answer = qmsgBox.exec_()
            # select failed asset
            mc.select(to_sel, r=True)

        # close the dialog
        self.close()

    def switch_ref_variant(self, selected_variant):
        curr_file_index = self.file_combobox.currentIndex()
        new_ref_path = self.file_combobox.itemData(curr_file_index, QtCore.Qt.UserRole)
        for item in self.name_listwidget.selectedItems():
            # get new path for current ref
            rn, topGrp = item.data(QtCore.Qt.UserRole)
            ref_path = mc.referenceQuery(rn, f=True, withoutCopyNumber=True)
            fn = os.path.basename(ref_path)
            asset = context_info.ContextPathInfo(path=ref_path)

            # replace reference
            # add origianl variant tag
            if not mc.objExists('{}.{}'.format(topGrp, ORIGINAL_VARIANT_ATTR)):
                asset_tag.add_str_attr(obj=topGrp, attr=ORIGINAL_VARIANT_ATTR, value=asset.name)
            # replace  the ref
            mc.file(new_ref_path, loadReference=rn, prompt=False)
            self.switch_shader(selected_variant, topGrp)

    def switch_shader(self, entity, topGrp):
        asset = entity.copy()
        asset.context.update(res='md', look='main')
        asset.context.update(step='rig') # use rig mtr for anim rig 

        # get namespace
        assetNamespace = ':'.join(topGrp.split(':')[:-1])
        shadeNamespace = asset.mtr_namespace

        # get material path - default use rig material
        mtrFile = asset.library(context_info.Lib.rigMtr)
        mtr = '%s/%s' % (asset.path.hero().abs_path(), mtrFile)
        # if rig material not available 
        # try find model tmp shade if rig shade does not exists 
        if not os.path.exists(mtr): 
            mtrFile = asset.library(context_info.Lib.modelTmp)
            mtr = '%s/%s' % (asset.path.hero().abs_path(), mtrFile)
        mtrRel = '%s/%s' % (asset.path.hero(), mtrFile)

        # get connection info
        asset = context_info.ContextPathInfo(path=mtrRel)
        shadeFileExt = os.path.basename(mtrRel)
        shadeFileName, ext = os.path.splitext(shadeFileExt)
        connectionData = []
        if mc.objExists('%s.shdConnect' %topGrp):
            connectionStr = mc.getAttr('%s.shdConnect' %topGrp)
            connectionData = eval(connectionStr)
        shadeFileName = shadeFileName.split('.')[0]  # get rid of .hero
        shadeKey = shadeFileName.split('_mtr_')[-1]
        connectionInfo = []
        if shadeKey in connectionData:
            connectionInfo = connectionData[shadeKey]

        # apply shader
        if os.path.exists(mtr):
            shade_utils.apply_ref_shade(mtrRel, shadeNamespace, assetNamespace, connectionInfo)
        else:
            logger.error('Failed to apply shader: {}'.format(mtr))

    def switch_setdress_variant(self, selected_variant):
        curr_file_index = self.file_combobox.currentIndex()
        new_gpu_path = self.file_combobox.itemData(curr_file_index, QtCore.Qt.UserRole)
        for item in self.name_listwidget.selectedItems():
            # get new path for current ref
            asm, shortname = item.data(QtCore.Qt.UserRole)
            loc = asm_lib.MayaRepresentation(asm)
            # switch gpu path
            loc.set_variant_value(new_gpu_path)
            asm_lib.UiCmd().switch_gpu()

def show():
    from rftool.utils.ui import maya_win
    maya_win.deleteUI(uiName)
    myApp = AssetVariant(parent=maya_win.getMayaWindow())
    if myApp.asset:
        myApp.show()

if __name__ == '__main__':
    show()
