# -*- coding: utf-8 -*-
import os
import sys

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_utils import icon

from PySide2 import QtCore, QtGui, QtWidgets
COLUMNS_INDEX = {
    'Pose Name': 0,
    'frame': 1,
}


class PoseEditorUI(QtWidgets.QWidget):
    def __init__(self, parent=None) :
        super(PoseEditorUI, self).__init__(parent)

        self.main_layout = QtWidgets.QVBoxLayout()
        self.logo_layout = QtWidgets.QHBoxLayout()
        self.Logo = QtWidgets.QLabel()
        self.logo_layout.addWidget(self.Logo)

        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.logo_layout.addItem(spacerItem)
        self.main_layout.addLayout(self.logo_layout)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.list_wid_pose_inter_layout = QtWidgets.QVBoxLayout()
        self.pose_inter_label = QtWidgets.QLabel()
        self.pose_inter_label.setFrameShape(QtWidgets.QFrame.WinPanel)
        self.pose_inter_label.setFrameShadow(QtWidgets.QFrame.Plain)
        self.pose_inter_label.setAlignment(QtCore.Qt.AlignCenter)
        self.list_wid_pose_inter_layout.addWidget(self.pose_inter_label)
        self.list_wid_pose_inter = QtWidgets.QListWidget()
        self.list_wid_pose_inter_layout.addWidget(self.list_wid_pose_inter)
        self.horizontalLayout.addLayout(self.list_wid_pose_inter_layout)
        self.push_button_layout = QtWidgets.QHBoxLayout()
        self.push_button_add =QtWidgets.QPushButton('Add')
        self.push_button_edit =QtWidgets.QPushButton('Edit')
        self.push_button_layout.addWidget(self.push_button_add)
        self.push_button_layout.addWidget(self.push_button_edit)
        self.pose_name_layout = QtWidgets.QVBoxLayout()
        self.pose_name_label = QtWidgets.QLabel('Pose Name')
        self.pose_name_label.setFrameShape(QtWidgets.QFrame.WinPanel)
        self.pose_name_label.setFrameShadow(QtWidgets.QFrame.Plain)
        self.pose_name_label.setAlignment(QtCore.Qt.AlignCenter)
        self.table_widget = QtWidgets.QTableWidget()
        self.table_widget.setColumnCount(2)
        self.table_widget.setRowCount(0)
        pose_name_table_item = QtWidgets.QTableWidgetItem()
        pose_name_table_item.setSizeHint(QtCore.QSize(50,30))
        frame_table_item = QtWidgets.QTableWidgetItem()
        self.table_widget.setHorizontalHeaderItem(0, pose_name_table_item)
        self.table_widget.setHorizontalHeaderItem(1, frame_table_item)
        self.pose_name_layout.addWidget(self.pose_name_label)
        self.pose_name_layout.addWidget(self.table_widget)
        self.pose_name_layout.addLayout(self.push_button_layout)
        self.horizontalLayout.addLayout(self.pose_name_layout)
        header = self.table_widget.horizontalHeader()
        header.setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(1, QtWidgets.QHeaderView.ResizeToContents)
        self.horizontalLayout.setStretch(1, 5)
        self.list_wid_pose_inter_layout.setStretch(1,5)


        self.main_layout.addLayout(self.horizontalLayout)
        self.logo_layout.addStretch(1)
        self.all_layout = QtWidgets.QHBoxLayout()
        self.all_layout.addLayout(self.main_layout)
        self.setLayout(self.all_layout)
        self.retranslateUi()

    def add_item_list_inter(self, node_name):
        item = QtWidgets.QListWidgetItem(self.list_wid_pose_inter)
        name = node_name.split('_poseInterpolator')[0]
        item.setText(name)
        data = [node_name]
        item.setData(QtCore.Qt.UserRole, data)

    def add_table_item_pose_name(self, pose_name, frame):
        pose_name_item = QtWidgets.QTableWidgetItem()
        pose_name_item.setText(pose_name)
        frame_item = QtWidgets.QTableWidgetItem()
        frame_item.setFlags(QtCore.Qt.ItemIsEnabled)
        frame_item.setText(str(frame))
        overall_row = self.table_widget.rowCount()
        self.table_widget.insertRow(overall_row)
        self.table_widget.setItem(overall_row, COLUMNS_INDEX['Pose Name'], pose_name_item)
        self.table_widget.setItem(overall_row, COLUMNS_INDEX['frame'], frame_item)
        return [pose_name_item, frame_item]

    def retranslateUi(self):
        self.Logo.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))
        self.pose_inter_label.setText(QtWidgets.QApplication.translate("Form", "Pose Interpolators", None, -1))
        self.table_widget.horizontalHeaderItem(0).setText( "Pose Name")
        self.table_widget.horizontalHeaderItem(1).setText( "Frame")
