_title = 'Riff Pose Editor'
_version = 'v.0.0.1'
_des = 'beta'
uiName = 'RFPoseEditor'

#Import python modules
import sys
import os
import getpass
import json
import logging

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# import config
import rf_config as config
from rf_utils import log_utils
# sg

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import ui
reload(ui)
import modules
reload(modules)

class RFPoseEditor(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #SetUp Window
        super(RFPoseEditor, self).__init__(parent)
        #ui read
        self.ui = ui.PoseEditorUI(parent)
        self.setObjectName(uiName)
        self.modules = modules
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.setCentralWidget(self.ui)
        self.adjustSize()
        self.setFixedSize(600,500)
        self.display_pose_inter()
        print '1'
        self.init_state()
        # self.set_logo()

    def init_state(self):
        self.ui.list_wid_pose_inter.itemSelectionChanged.connect(self.display_pose_name)
        self.ui.push_button_add.clicked.connect(self.add_pose)
        self.ui.table_widget.itemSelectionChanged.connect(self.set_current_frame_pose)
        self.ui.push_button_edit.clicked.connect(self.edit_pose)
    def display_pose_name(self):
        
        self.ui.table_widget.setRowCount(0)
        node_name = self.ui.list_wid_pose_inter.currentItem().text()
        self.modules.set_time_range(node_name)
        frame_list = self.modules.get_keyframe(node_name)
        created_attr = self.modules.get_shape_pose()
        list_attr = self.modules.list_pose_shape(node_name)
        for index, attr in enumerate(list_attr):
            #frame = self.modules.set_time_range(attr)
            pose_item, frame_item = self.ui.add_table_item_pose_name(attr, int(frame_list[index]))
            if attr in created_attr:
                pose_item.setForeground(QtGui.QColor('green'))
                frame_item.setForeground( QtGui.QColor('green'))
            else:
                pose_item.setForeground( QtGui.QColor('orange'))
                frame_item.setForeground( QtGui.QColor('orange'))


    def display_pose_inter(self):
        self.pose_inter_nodes = self.modules.get_interpolator_node()
        for node in self.pose_inter_nodes:
            self.ui.add_item_list_inter(node)

    def add_pose(self):
        pose_inter_node = self.ui.list_wid_pose_inter.currentItem().text()
        pose_name = self.ui.table_widget.currentItem().text()
        self.modules.add_pose(pose_inter_node, pose_name)
        self.modules.edit_pose(pose_name)
        cur_row = self.ui.table_widget.currentRow()
        self.display_pose_name()
        self.ui.table_widget.setCurrentCell(cur_row,0)

    def edit_pose(self):
        pose_name = self.ui.table_widget.currentItem().text()
        self.modules.edit_pose(pose_name)

    def set_current_frame_pose(self):
        cur_row = self.ui.table_widget.currentRow()
        frame = self.ui.table_widget.item(cur_row, 1).text()
        self.modules.go_to_pose_intp(frame)

def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = RFPoseEditor(maya_win.getMayaWindow())
        myApp.show()
        return myApp
if __name__ == '__main__':
    show()
