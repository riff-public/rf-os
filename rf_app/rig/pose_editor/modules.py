#Import python modules
import sys
import os
import getpass
import maya.cmds as mc
import pymel.core as pm
from decimal import Decimal, ROUND_HALF_UP
import maya.mel as mm

def get_interpolator_node():
    return mc.ls( type = 'poseInterpolator' )

def get_keyframe(obj):
    return pm.keyframe( obj , query = True )

def set_time_range(obj):
    frame = get_keyframe( obj )
    timeslide = pm.playbackOptions( e = True , minTime = frame[0] , maxTime = frame[-1])

def get_shape_pose():
    bsh = get_blend_shape_node()
    return mc.listAttr( '%s.w' %bsh , m = True )

def list_pose_shape(pose_inter_node):
    poseShapeNm = [ ]
    pose_inter_node = pose_inter_node
    print pose_inter_node
    attrVal = get_attr_and_key_value( pose_inter_node )
    
    if attrVal :
        for attrKey in attrVal.keys() :
            for each in attrVal[attrKey] :
                rot = { 'rotateX' : 0 , 'rotateY' : 0 , 'rotateZ' : 0 }
                rot[attrKey] = int(Decimal(each[1]).quantize(0, ROUND_HALF_UP))
                rot[attrKey] = '%s' %rot[attrKey]
                
                if '-' in rot[attrKey] :
                    val = rot[attrKey].split('-')[-1]
                    rot[attrKey] = 'n%s' %val
                
                if not rot[attrKey] == '0' :
                    poseNm = '%s_%s_%s_%s' %( pose_inter_node , rot['rotateX'] , rot['rotateY'] , rot['rotateZ'])
                    poseShapeNm.append(poseNm)
    
    return poseShapeNm

def get_attr_and_key_value(obj):
    attrLists = { }
    print obj
    animCuvs = mc.listConnections( obj , type = 'animCurve' )
    exception = [ 'rotateX' , 'rotateY' , 'rotateZ' ]
    
    for anim in animCuvs :
        attr = anim.split('_')[-1]
        
        if attr in exception :
            range = pm.keyframe( obj , at = attr , query = True )
            value = pm.keyframe( obj , at = attr , t =(range[0],range[-1]) , q = True , vc = True , tc = True )
            attrLists[ attr ] = value
    
    return attrLists

def get_blend_shape_node():
    bsh_node = mc.ls(type ='blendShape')
    if mc.objExists(bsh_node[0]):
        return bsh_node[0]

def set_time_range(obj):
    mc.select(obj)
    frame = get_keyframe( obj )
    timeslide = pm.playbackOptions( e = True , minTime = frame[0] , maxTime = frame[-1])

def add_pose( poseIntpNd, pose_name):
    bsh_node = get_blend_shape_node()
    poseIntpNd = mc.listConnections( poseIntpNd , type = 'poseInterpolator' )[0]
    print poseIntpNd , pose_name , bsh_node
    cmd = 'poseInterpolatorAddShapePose( "%s" , "%s" , "swing" , {"%s"} , 1 )' %( poseIntpNd , pose_name, bsh_node )
    mm.eval(cmd)

def edit_pose(pose_name):
    bsh_node = get_blend_shape_node()
    indexBsh = get_index_shape_pose( bsh_node , pose_name )
    cmd = 'sculptTarget -e -target %s blendShape1' %indexBsh
    mm.eval(cmd)


def get_index_shape_pose(bsh_node, pose_name):
    shapes = get_shape_pose( )
    return shapes.index( pose_name )

def go_to_pose_intp(frame):
    pm.currentTime( int(frame) )
        