# v.0.0.1 polytag switcher
_title = 'Template App'
_version = 'v.0.0.1'
_des = 'Wip'
uiName = 'TemplateUI'

#Import python modules
import sys
import os
import logging
import getpass
import traceback
from collections import OrderedDict
import pymel.core as pmc
import os
import json
import pickle

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
import rf_config as config

# framework modules
from rf_utils import log_utils
from rf_utils.ui import load
reload(load)
from rf_utils.ui import stylesheet

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui
from rf_utils.widget import project_widget
from rf_utils.sg import sg_utils
from rf_utils import file_utils as fu
from rf_utils.context import context_info
from rf_utils.pipeline import file_info 
reload(file_info)

# maya & module function 
import pymel.core as pmc
import maya.cmds as mc
import maya.mel as mel


from rf_app.mocap.function.mocapData import ioData as iod
reload(iod)

from rf_app.mocap.function.mocapData import poseData as posd
reload(posd)

from rf_app.mocap.function.mocapData import ctrlData as ctrld
reload(ctrld)

from rf_app.mocap.function.mocapData import mocapData
reload(mocapData)

from rf_app.mocap.function.mocapData import hikList as hikl
reload(hikl)

from rf_app.mocap.function import fkiksnap
reload(fkiksnap)

import rf_app.mocap.function.hikDef as hikd
reload(hikd)



class MocapLibrary(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(MocapLibrary, self).__init__(parent)

        # ui read
        uiFile = '%s/ui_bck.ui' % moduleDir
        self.ui = load.setup_ui_maya(uiFile, parent)
        # set size 
        self.w = 500
        self.h = 600 

        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.resize(self.w, self.h)
        self.setWindowTitle('%s %s %s' % (_title, _version, _des))

        self.asset_list = ['']
        self.action_folder_list = []
        self.projectWidget = project_widget.SGProjectComboBox(sg=sg_utils.sg, layout=QtWidgets.QHBoxLayout())
        self.projectWidget.setContentsMargins(0, 0, 3, 0)
        #spacerItem = QtWidgets.QSpacerItem(10, 10, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        #self.projectLayout.addItem(spacerItem)

        self.init_signals()
        self.projectWidget.projectComboBox.setCurrentText(self.mocap_char_project)
        self.ui.asset_name.setCurrentText(self.mocap_char_asset)

        # print self.asset_data_path
        # print self.share_anim_path
        # print self.shot_path
        # print fu.listFolder(self.shot_path)
        # print self.asset_anim_path
        # print self.action_path_list

    def init_signals(self): 
        """ connect signal widgets """ 
        self.ui.data_layout.addWidget(self.projectWidget, 0, 0, 1, 1)
        self.projectWidget.projectComboBox.activated.connect(self.get_project)
        self.ui.asset_name.currentTextChanged.connect(self.get_asset)
        self.ui.anim_type.activated.connect(self.get_type)
        self.ui.search_name.returnPressed.connect(self.get_type)
        self.get_character()
        #self.get_project()
        #self.get_asset()
        self.get_type()
        self.get_shot()

        self.ui.shot_name.currentIndexChanged.connect(self.shot_name_changed)
        self.ui.listWidget.currentItemChanged.connect(self.action_selected)
        self.ui.bake_button.clicked.connect(self.bake_character)


    def get_project(self):
        # change self.project from project from comboBox
        self.project = self.projectWidget.projectComboBox.currentText()
        # after change project it will change asset in combo box
        asset_list = fu.listFolder(r"P:/%s/asset/publ/char/"%(self.project))
        self.ui.asset_name.clear()
        self.ui.asset_name.addItems(asset_list)
        self.get_asset()

    def get_asset(self):
        # change self.asset from asset commboBox
        self.asset = self.ui.asset_name.currentText()
        self.get_asset_path()
        self.get_type()

    def get_asset_path(self):
        self.share_anim_path = r"P:/%s/share/human/anim"%(self.project)
        self.shot_path = r"P:/%s/scene/publ"%(self.project)
        self.asset_anim_path = r"P:/%s/asset/publ/char/%s/mocap/anim"%(self.project,self.asset)

    def get_type(self):
        # get type to filter file from asset or scene
        self.action_path_list = []
        self.types = self.ui.anim_type.currentText()

        # each type will give different action path list to find action
        # asset type will show only loop animation action of that character
        if self.types == 'asset':
            self.ui.shot.hide()
            self.ui.shot_name.hide()
            self.action_path_list.append(self.asset_anim_path.replace('\\','/'))
            #print self.action_path_list
            self.action_from_assetNShot()
        
        # all shot type will show all animation that character has in every shot in this project
        elif self.types == 'all shot':
            self.ui.shot.show()
            self.ui.shot_name.show()
            
            # get all sq folder in this project
            sq_path_list = []
            self.shot_name_list = []
            self.ui.shot_name.clear()
            self.ui.shot_name.setDisabled(1)
            self.ui.shot_name.addItem('all')
            
            i = 1 
            for sq_name in fu.listFolder(self.shot_path):
                #print sq_name
                sq_path_list = ( os.path.join(self.shot_path,sq_name))
                #print sq_path_list
                if sq_path_list:
                # get all shot from sq folder
                    #for sq_folder in sq_path_list:
                    #print 'sqFolder:%s'%sq_folder
                    # get action from specific asset in shot folder
                    for shot_name in fu.listFolder(sq_path_list):
                        #print 'shotName:%s'%shot_name
                        shot_path_list = os.path.join(sq_path_list,shot_name,self.asset)
                        #print 'shot_path_list:%s'%shot_path_list
                        if os.path.isdir(shot_path_list):
                            self.ui.shot_name.addItem(shot_name)
                            self.ui.shot_name.setItemData(i, shot_path_list.replace('\\','/'), QtCore.Qt.UserRole)
                            self.action_path_list.append(shot_path_list.replace('\\','/'))
                            i+=1
                            #print '%s  :  %s  :  %s'%(sq_folder , shot_name,self.action_path_list)
                #print self.action_path_list
            self.ui.shot_name.setItemData(0, self.action_path_list, QtCore.Qt.UserRole)
            self.ui.shot_name.setEnabled(1)
            self.shot_name_changed()

        # shot will show only animation that character has in current shot
        elif self.types == 'shot':
            self.ui.shot.hide()
            self.ui.shot_name.hide()
            shot_path_list = os.path.join(self.cur_shot_publ_path,self.asset)
            self.action_path_list.append(shot_path_list.replace('\\','/'))
            #print self.action_path_list
            self.action_from_assetNShot()
        #self.get_action_list()
        
    
    def action_from_assetNShot(self):
        #shot = self.ui.shot_name.currentText()
        currentPath = self.action_path_list
        self.search = self.ui.search_name.displayText().lower()
        actions = []
        #print currentPath
        if currentPath:
            for path in currentPath:
                #print path
                actions.append(path) 
        self.ui.listWidget.clear()
        
        if actions:
            for path in actions:
               if fu.listFolder(path):
                    for action in fu.listFolder(path): 
                        if self.search:
                            if self.search in action.lower():
                                item = QtWidgets.QListWidgetItem(self.ui.listWidget)
                                item.setText(action)
                                item.setData(QtCore.Qt.UserRole, '%s/%s' % (path, action))
                        else:
                            item = QtWidgets.QListWidgetItem(self.ui.listWidget)
                            item.setText(action)
                            item.setData(QtCore.Qt.UserRole, '%s/%s' % (path, action))  

    def shot_name_changed(self): 
        """ list action and fill to action widget """ 
        index = self.ui.shot_name.currentIndex()
        currentPath = self.ui.shot_name.itemData(index, QtCore.Qt.UserRole)
        self.search = self.ui.search_name.displayText().lower()
        actions = []
        if index == 0: 
            if currentPath != None:
                for path in currentPath: 
                    actions.append(path)
                self.ui.listWidget.clear()
                
                if actions:
                    for path in actions:
                        if fu.listFolder(path):
                            for action in fu.listFolder(path): 
                                if self.search:
                                    if self.search in action.lower():
                                        item = QtWidgets.QListWidgetItem(self.ui.listWidget)
                                        item.setText(action)
                                        item.setData(QtCore.Qt.UserRole, '%s/%s' % (path, action))
                                else:
                                    item = QtWidgets.QListWidgetItem(self.ui.listWidget)
                                    item.setText(action)
                                    item.setData(QtCore.Qt.UserRole, '%s/%s' % (path, action))  
        else: 
            actions = fu.listFolder(currentPath)

            self.ui.listWidget.clear()
            for path in currentPath:
                if actions:
                    for action in actions: 
                        if self.search:
                            if self.search in action.lower():
                                item = QtWidgets.QListWidgetItem(self.ui.listWidget)
                                item.setText(action)
                                item.setData(QtCore.Qt.UserRole, '%s/%s' % (currentPath, action))
                        else:
                            item = QtWidgets.QListWidgetItem(self.ui.listWidget)
                            item.setText(action)
                            item.setData(QtCore.Qt.UserRole, '%s/%s' % (currentPath, action))                       


    def action_selected(self): 
        """ what happen when click action """ 
        item = self.ui.listWidget.currentItem()
        self.fbx_file_name =  item.text()
        if item:
            data = item.data(QtCore.Qt.UserRole)
            self.action_folder_path = data


    def get_action_list(self):
        # query action folder from all arugement
        self.action_folder_name_list = []
        self.action_folder_path_list = []
        for path in self.action_path_list:
            action_folder_list = fu.listFolder(path)
            if action_folder_list:
                for action_folder in action_folder_list:
                    self.action_folder_name_list.append(action_folder)
                    self.action_folder_path_list.append(os.path.join(path,action_folder).replace('\\','/'))
    
    def bake_character(self):
        # get ma file from select action widget
        #print self.action_folder_path
        mocap_file_path = os.path.join(self.action_folder_path,self.fbx_file_name+'.mb').replace('\\','/')

        self.create_hik_from_path(mocap_file_path)


    def get_shot(self):
        scene_name = context_info.ContextPathInfo()
        if scene_name.valid:
            self.shot = scene_name.name
            shot_list = scene_name.path.name().split('/')[1:6]
            shot_list[2] = 'publ'
            self.cur_shot_publ_path = 'P:/%s'%('/'.join(shot_list))
            self.ui.anim_type.setCurrentIndex(1)
            self.get_type()
        else:
            self.shot =''
            self.sq = ''
            self.ui.anim_type.clear()
            self.ui.anim_type.addItems(['asset','all shot'])
            self.ui.anim_type.setCurrentIndex(1)
            self.get_type()

    def get_character(self):
        character = pmc.selected()[0]
        self.sel = character
        ref_file = character.referenceFile().path
        f = file_info.File(ref_file)
        path = f.redirect()
        #print path
        self.mocap_char_project,self.mocap_char_asset = self.get_project_asset(path)

        self.ns = character.namespace()
        
        if self.ns:
            self.char_name = self.ns
            self.char_name = self.ns.replace(':','')+'_mocap'
        else:
            self.char_name = self.ns +'_mocap'
        
        self.asset_data_path = r"P:/%s/asset/publ/char/%s/mocap/data"%(self.mocap_char_project,self.mocap_char_asset)
        self.bindPose_data_path = os.path.join(self.asset_data_path,'bindPose').replace('\\','/')
        self.tpose_data_path = os.path.join(self.asset_data_path,'tpose').replace('\\','/')
        self.hik_data_path = os.path.join(self.asset_data_path,'char_list').replace('\\','/')
        self.fkiksnap_data_path = os.path.join(self.asset_data_path,'fkikDict').replace('\\','/')

        proj_idx = [self.projectWidget.projectComboBox.itemText(i) for i in range(self.projectWidget.projectComboBox.count())].index(self.mocap_char_project)
        self.projectWidget.projectComboBox.setCurrentIndex(proj_idx)
        self.get_project()
        
        asset_idx = [self.ui.asset_name.itemText(i) for i in range(self.ui.asset_name.count())].index(self.mocap_char_asset)
        self.ui.asset_name.setCurrentIndex(asset_idx)
        self.get_asset()
        
        self.load_char_list()
        self.load_fkikDict()
        self.get_all_ctrl()

        
    def get_project_asset(self,path):
        project = path.split('/')[1]
        asset = path.split('/')[5]

        return project,asset


    # create hik function
    def load_char_list(self):
        if os.path.exists(self.hik_data_path):
            with open(self.hik_data_path, 'r') as f:
                self.hik_char_data = json.load(f)
                #print 'import complete'

    def load_fkikDict(self):
        if os.path.exists(self.fkiksnap_data_path):
            with open(self.fkiksnap_data_path, 'r') as f:
                self.fkIk_dict = json.load(f)
                #print 'import complete'
        print self.fkIk_dict

    def tPose_set(self):
        '''set Character Rig to Tpose Position and switch IK to FK Control.'''
        posd.read_pose(self.tpose_data_path, self.ns, '', '')

    def bindPose_set(self):
        posd.read_pose(self.bindPose_data_path, self.ns, '', '')

    def create_hik_character(self):
        pmc.select(self.sel)
        #if os.path.isdir(self.tpose_data_path):
        self.bindPose_set()
        self.fkIk_dict = fkiksnap.add_dup_ik_ctrl(self.ns, self.fkIk_dict)
        self.tPose_set()
        pmc.select(self.sel)
        hikd.create_char(self.ns, self.char_name, self.hik_char_data)
        hikd.create_hik_custom_rig(self.ns, self.char_name,self.hik_char_data)
        

    def delete_hik(self):
        hikd.delete_HIK_unknown_node(self.char_name)

    def create_ctrl_attr_list(self):
        self.ctrl_attr_list = []
        
        for part in self.ctrl_list:
            ctrl = '%s%s'%(self.ns,part)
            
            if pmc.objExists(ctrl):
                if pmc.PyNode(ctrl).t.isConnected():
                    self.ctrl_attr_list.append(ctrl+ '.translate')
                if pmc.PyNode(ctrl).r.isConnected():
                    self.ctrl_attr_list.append(ctrl+'.rotate')

    def get_time(self):
        self.start = mc.playbackOptions(min=1,q=1)
        self.end = mc.playbackOptions(max=1,q=1)
        self.time = "%s:%s"%(self.start,self.end)

    def set_time(self,start,end):
        mc.playbackOptions(min = start,max=end)

    def get_time_from_ref(self, ref_ns = ''):
        mc.select(mc.ls('%s:*'%ref_ns))
        kf = mc.keyframe(q=1,tc=1)
        return [min(kf),max(kf)]

    def bake_char(self):
        mc.select(cl=1)
        #self.turn_off_deform()
        #mel.eval('selectLayer("BaseAnimation")')
        #mc.animLayer('BaseAnimation',e=1,sel=1)
        #mel.eval("hikBakeCharacter 0; hikSetCurrentSourceFromCharacter(hikGetCurrentCharacter()); hikUpdateSourceList; hikUpdateContextualUI;")
        #mc.eval('hikBakeToControlRigPre("%s" );'%self.char_name)
        mc.bakeResults(self.ctrl_list , 
        at = ['translate','rotate'],
        simulation =  1,
        t =  (self.animKey[0],self.animKey[1]) ,
        sampleBy =  1 ,
        oversamplingRate =  1 ,
        disableImplicitControl =  1 ,
        preserveOutsideKeys =  1 ,
        sparseAnimCurveBake =  0 ,
        removeBakedAttributeFromLayer =  0 ,
        removeBakedAnimFromLayer =  0,
        bakeOnOverrideLayer =  1 ,
        destinationLayer = self.tmp_mocapLayer,
        minimizeRotation =  1 ,
        controlPoints =  0 ,
        shape =  1 )
        mel.eval( 'hikBakeCharacterPost( "%s" );'%(self.char_name))
        #mc.eval('hikBakeToControlRigPost( "%s" );'%self.char_name)
        
    def turn_off_deform(self):
        for node in mc.ls(type=['skinCluster','blendShape']):
            mc.setAttr(node+'.envelope',0)

    def turn_on_deform(self):
        for node in mc.ls(type=['skinCluster','blendShape']):
            mc.setAttr(node+'.envelope',1)

    def create_hik_from_path(self,source_file):
        # reference mocap animation
        if not mc.pluginInfo('mayaHIK',loaded=1,q=1):
            mc.loadPlugin('mayaHIK')
        
        mel.eval("HIKCharacterControlsTool")
        if mc.objExists('%s_main'%(self.char_name)):
            mc.animLayer('%s_main'%(self.char_name),e=1,m=1)      
        

        self.get_time()
        self.currentTime = mc.currentTime(q = 1)
        if '\\' in source_file:
            file_name = source_file.split('\\')[-1].split('.')[0]
        elif '/' in source_file:
            file_name = source_file.split('/')[-1].split('.')[0]

        self.ref_ns = str(file_name)
        
        for letter in [' ','-','+','*']:
            if letter in self.ref_ns:
                self.ref_ns = self.ref_ns.replace(letter,'_')
        


        mc.file(source_file, ns=self.ref_ns, r=1)    
        source_char = '%s:%s'%(self.ref_ns,'Character1')
        self.animKey = self.get_time_from_ref(self.ref_ns)
        self.set_time(self.animKey[0], self.animKey[1])
        
        self.get_all_ctrl()
        mc.select(self.ctrl_list)
        self.deselect_animLayer()
        self.tmp_mocapLayer = mc.animLayer('%s%s'%(self.char_name, self.ref_ns),aso =1 ,o=1)
        mc.animLayer(self.tmp_mocapLayer,e=1,sel=1)
        self.create_hik_character()

        # set current human ik source
        hikd.set_hik_source(self.char_name, source_char)
        self.turn_off_deform()
        self.bake_char()
        self.delete_hik()
        
        self.deselect_animLayer()
        mc.animLayer(self.tmp_mocapLayer,e=1,sel=1)
        fkiksnap.fk_to_Ik_all(self.ns, self.fkIk_dict ,[self.animKey[0],self.animKey[1]],self.tmp_mocapLayer)
        self.turn_on_deform()
        #mc.animLayer(self.mocap_layer,e=1,m=0,l=0)
        self.copy_frame_to_main()
      
        mc.file(source_file, rr=1)
        mc.animLayer(self.mocap_layer,e=1,m=0)
        self.set_time(self.start,self.end)
    
    def get_all_ctrl(self):
        self.ctrl_list = []
        
        for part in self.hik_char_data:
            if part[3]:
                self.ctrl_list.append(part[3])

        for key in self.fkIk_dict.keys():
            part_dict = self.fkIk_dict[key]
            for ctrl in part_dict['fk_ctrl']:
                if ctrl not in self.ctrl_list:
                    self.ctrl_list.append(ctrl)

            for ctrl  in part_dict['ik_ctrl']:
                if ctrl not in self.ctrl_list:
                    self.ctrl_list.append(ctrl)
        
        for num in range(len(self.ctrl_list)):
            self.ctrl_list[num] = self.ns + self.ctrl_list[num]


    def copy_frame_to_main(self):
        ''' create Mocap and Animation AnimLayer by using controllerList and namespace '''

        self.mocap_layer_name = '%s_main'%(self.char_name)
        
        if not mc.objExists(self.mocap_layer_name):
            self.mocap_layer = mc.animLayer(self.mocap_layer_name)
        else:
            self.mocap_layer = self.mocap_layer_name
            
        mc.select(self.ctrl_list)
        #self.mute_animLayer()
        mc.animLayer(self.mocap_layer, aso=1 ,e=1, o=1 , m=1)   
        mc.animLayer(self.tmp_mocapLayer ,m=1, o=1,e=1)   
        #print 'tmpMocapLayer : %s'%(self.tmp_mocapLayer)
        #print 'mainMocap : %s'%(self.mocap_layer)
        #print self.ctrl_list
        mc.copyKey(self.ctrl_list, t = (self.animKey[0], self.animKey[1]), al=self.tmp_mocapLayer , iub=0 , an ='objects' , o ='keys') 

        mc.pasteKey(self.ctrl_list,  to=self.currentTime, al =self.mocap_layer , option = 'replace',an = 'objects')


    def deselect_animLayer(self):
        for layer in mc.ls(type = 'animLayer'):
            mc.animLayer(layer,sel=0,e=1)
    def mute_animLayer(self):
        for layer in mc.ls(type = 'animLayer'):
            mc.animLayer(layer,m=1,e=1)                

def show(mode='default'):
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = MocapLibrary(maya_win.getMayaWindow())
        myApp.show()
        return myApp


if __name__ == '__main__':
    show()
