# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui.ui'
#
# Created: Fri Apr 17 11:12:22 2020
#      by: PyQt4 UI code generator 4.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.setWindowModality(QtCore.Qt.NonModal)
        Dialog.resize(483, 607)
        self.verticalLayout_2 = QtGui.QVBoxLayout(Dialog)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.data_layout = QtGui.QGridLayout()
        self.data_layout.setObjectName(_fromUtf8("data_layout"))
        self.asset = QtGui.QLabel(Dialog)
        self.asset.setObjectName(_fromUtf8("asset"))
        self.data_layout.addWidget(self.asset, 1, 0, 1, 1)
        self.asset_name = QtGui.QComboBox(Dialog)
        self.asset_name.setObjectName(_fromUtf8("asset_name"))
        self.data_layout.addWidget(self.asset_name, 1, 1, 1, 1)
        self.anim = QtGui.QLabel(Dialog)
        self.anim.setObjectName(_fromUtf8("anim"))
        self.data_layout.addWidget(self.anim, 0, 3, 1, 1)
        self.anim_type = QtGui.QComboBox(Dialog)
        self.anim_type.setObjectName(_fromUtf8("anim_type"))
        self.anim_type.addItem(_fromUtf8(""))
        self.anim_type.addItem(_fromUtf8(""))
        self.anim_type.addItem(_fromUtf8(""))
        self.data_layout.addWidget(self.anime_type, 0, 4, 1, 1)
        self.search_name = QtGui.QLineEdit(Dialog)
        self.search_name.setObjectName(_fromUtf8("search_name"))
        self.data_layout.addWidget(self.search_name, 1, 4, 1, 1)
        self.project = QtGui.QLabel(Dialog)
        self.project.setObjectName(_fromUtf8("project"))
        self.data_layout.addWidget(self.project, 0, 0, 1, 1)
        self.search = QtGui.QLabel(Dialog)
        self.search.setObjectName(_fromUtf8("search"))
        self.data_layout.addWidget(self.search, 1, 3, 1, 1)
        self.project_name = QtGui.QComboBox(Dialog)
        self.project_name.setObjectName(_fromUtf8("project_name"))
        self.data_layout.addWidget(self.project_name, 0, 1, 1, 1)
        spacerItem = QtGui.QSpacerItem(15, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.data_layout.addItem(spacerItem, 0, 2, 1, 1)
        spacerItem1 = QtGui.QSpacerItem(15, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.data_layout.addItem(spacerItem1, 1, 2, 1, 1)
        self.data_layout.setColumnStretch(1, 3)
        self.data_layout.setColumnStretch(4, 3)
        self.verticalLayout.addLayout(self.data_layout)
        self.action_scroll = QtGui.QScrollArea(Dialog)
        self.action_scroll.setFrameShape(QtGui.QFrame.Box)
        self.action_scroll.setFrameShadow(QtGui.QFrame.Sunken)
        self.action_scroll.setLineWidth(1)
        self.action_scroll.setMidLineWidth(0)
        self.action_scroll.setWidgetResizable(True)
        self.action_scroll.setObjectName(_fromUtf8("action_scroll"))
        self.scrollAreaWidgetContents_3 = QtGui.QWidget()
        self.scrollAreaWidgetContents_3.setGeometry(QtCore.QRect(0, 0, 459, 463))
        self.scrollAreaWidgetContents_3.setObjectName(_fromUtf8("scrollAreaWidgetContents_3"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.scrollAreaWidgetContents_3)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.listWidget = QtGui.QListWidget(self.scrollAreaWidgetContents_3)
        self.listWidget.setObjectName(_fromUtf8("listWidget"))
        self.horizontalLayout.addWidget(self.listWidget)
        self.action_scroll.setWidget(self.scrollAreaWidgetContents_3)
        self.verticalLayout.addWidget(self.action_scroll)
        self.bake_button = QtGui.QPushButton(Dialog)
        self.bake_button.setMinimumSize(QtCore.QSize(0, 60))
        self.bake_button.setObjectName(_fromUtf8("bake_button"))
        self.verticalLayout.addWidget(self.bake_button)
        self.verticalLayout_2.addLayout(self.verticalLayout)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "Dialog", None))
        self.asset.setText(_translate("Dialog", "asset", None))
        self.anim.setText(_translate("Dialog", "anim type", None))
        self.anime_type.setItemText(0, _translate("Dialog", "shot", None))
        self.anime_type.setItemText(1, _translate("Dialog", "asset action", None))
        self.anime_type.setItemText(2, _translate("Dialog", "share action", None))
        self.project.setText(_translate("Dialog", "project", None))
        self.search.setText(_translate("Dialog", "search", None))
        self.bake_button.setText(_translate("Dialog", "bake", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Dialog = QtGui.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

