# v.0.0.1 polytag switcher
_title = 'Convert App'
_version = 'v.0.0.1'
_des = 'Wip'
uiName = 'ConvertUI'

#Import python modules
import sys
import os
import logging
import getpass
import traceback
from collections import OrderedDict
import pymel.core as pmc
import os
import json
import pickle

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
import rf_config as config

# framework modules
from rf_utils import log_utils
from rf_utils.ui import load
reload(load)
from rf_utils.ui import stylesheet

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui
from rf_utils.widget import project_widget
from rf_utils.sg import sg_utils
from rf_utils import file_utils as fu
from rf_utils.context import context_info
from rf_utils.pipeline import file_info 
reload(file_info)

# maya & module function 
import pymel.core as pmc
import maya.cmds as mc
import maya.mel as mel


class ConvertFile(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(ConvertFile, self).__init__(parent)

        # ui read
        uiFile = '%s/convert_file.ui' % moduleDir
        self.ui = load.setup_ui_maya(uiFile, parent)
        # set size 
        self.w = 500
        self.h = 600 

        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.resize(self.w, self.h)
        self.setWindowTitle('%s %s %s' % (_title, _version, _des))

        self.asset_list = ['']
        self.action_folder_list = []
        # self.projectWidget = project_widget.SGProjectComboBox(sg=sg_utils.sg, layout=QtWidgets.QHBoxLayout())
        # self.projectWidget.setContentsMargins(0, 0, 3, 0)
        # #spacerItem = QtWidgets.QSpacerItem(10, 10, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        # #self.projectLayout.addItem(spacerItem)

        # self.projectWidget.projectComboBox.setCurrentText(self.mocap_char_project)
        # self.ui.asset_name.setCurrentText(self.mocap_char_asset)
   

def show(mode='default'):
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = ConvertFile(maya_win.getMayaWindow())
        myApp.show()
        return myApp


if __name__ == '__main__':
    show()
