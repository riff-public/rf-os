# v.0.0.1 polytag switcher
_title = 'Template App'
_version = 'v.0.0.1'
_des = 'Wip'
uiName = 'TemplateUI'

#Import python modules
import sys
import os
import logging
import getpass
import traceback
from collections import OrderedDict
import pymel.core as pmc
import os
import json
import pickle
import subprocess

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
print moduleDir
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
import rf_config as config

# framework modules
from rf_utils import log_utils
from rf_utils.ui import load
reload(load)
from rf_utils.ui import stylesheet

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui
from rf_utils.widget import project_widget
from rf_utils.sg import sg_utils
from rf_utils import file_utils as fu
from rf_utils.context import context_info
from rf_utils.pipeline import file_info 
from rf_utils.widget import display_widget
reload(file_info)

# maya & module function 
import pymel.core as pmc
import maya.cmds as mc
import maya.mel as mel


from rf_app.mocap.function.mocapData import ioData as iod
reload(iod)

from rf_app.mocap.function.mocapData import poseData as posd
reload(posd)

from rf_app.mocap.function.mocapData import ctrlData as ctrld
reload(ctrld)

from rf_app.mocap.function.mocapData import mocapData
reload(mocapData)

from rf_app.mocap.function.mocapData import hikList as hikl
reload(hikl)

import rf_app.mocap.function.hikDef as hikd
reload(hikd)

# import publishAction as pa
# reload(pa)

class MocapLibrary(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(MocapLibrary, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        self.ui = load.setup_ui_maya(uiFile, parent)
        # set size 
        self.w = 500
        self.h = 600 

        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.resize(self.w, self.h)
        self.setWindowTitle('%s %s %s' % (_title, _version, _des))

        self.asset_list = ['']
        self.action_folder_list = []
        self.projectWidget = project_widget.SGProjectComboBox(sg=sg_utils.sg, layout=QtWidgets.QHBoxLayout())
        self.projectWidget.setContentsMargins(0, 0, 1, 0)
        self.ui.data_layout.addWidget(self.projectWidget, 0, 0, )
        pacerItem = QtWidgets.QSpacerItem(10, 10, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.dropWidget = display_widget.DropUrlList()
        self.ui.action_layout.addWidget(self.dropWidget)


        # #self.projectLayout.addItem(spacerItem)
        self.init_signals()

    def init_signals(self): 
        """ connect signal widgets """ 
        
        self.projectWidget.projectComboBox.activated.connect(self.get_project)
        self.ui.asset_name.currentTextChanged.connect(self.get_asset)
        self.ui.anim_type.currentTextChanged.connect(self.get_type)
        self.get_project()
        #self.get_asset()
        self.get_type()
        self.ui.shot_name.currentIndexChanged.connect(self.shot_name_changed)
        self.dropWidget.multipleDropped.connect(self.get_fbx_list)
        self.dropWidget.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
        self.ui.publish_button.clicked.connect(self.batch_action_folder)
        #self.ui.listWidget.currentItemChanged.connect(self.action_selected)
        #self.ui.bake_button.clicked.connect(self.bake_cahracter)

    def get_fbx_list(self,path_list):
        for num,path in enumerate(path_list):
            action_name = path.split('/')[-1].replace('.fbx','')
            action_item = QtWidgets.QListWidgetItem(self.dropWidget)
            action_item.setText(action_name)
            action_item.setData(QtCore.Qt.UserRole, path)
            
    def get_project(self):
        # change self.project from project from comboBox
        self.project = self.projectWidget.projectComboBox.currentText()
        # after change project it will change asset in combo box
        asset_list = fu.listFolder(r"P:/%s/asset/publ/char/"%(self.project))
        self.ui.asset_name.clear()
        self.ui.asset_name.addItems(asset_list)
        self.get_asset()
        
        self.shot_path = r"P:/%s/scene/publ"%(self.project)
        self.ui.shot_name.clear()
        self.ui.shot_name.setDisabled(1)
        
        i = 0
        for sq_name in fu.listFolder(self.shot_path):
            sq_path_list = ( os.path.join(self.shot_path,sq_name))
            if sq_path_list:
            # get all shot from sq folder
                # get action from specific asset in shot folder
                for shot_name in fu.listFolder(sq_path_list):
                    #print 'shotName:%s'%shot_name
                    shot_path_list = os.path.join(sq_path_list,shot_name)
                    #print 'shot_path_list:%s'%shot_path_list
                    if os.path.isdir(shot_path_list):
                        #print 'shot path : %s'%shot_path_list
                        self.ui.shot_name.addItem(shot_name)
                        self.ui.shot_name.setItemData(i, shot_path_list.replace('\\','/'), QtCore.Qt.UserRole)
                        i+=1
        self.get_path()
         

    def get_asset(self):
        # change self.asset from asset commboBox
        self.asset = self.ui.asset_name.currentText()
        self.get_asset_path()
        self.get_type()
        

    def get_asset_path(self):
        self.share_anim_path = r"P:/%s/share/human/anim"%(self.project)
        self.asset_anim_path = r"P:/%s/asset/publ/char/%s/mocap/anim"%(self.project,self.asset)
        #self.get_path()

    def get_type(self):
        # get type to filter file from asset or scene
        self.action_path_list = []
        self.types = self.ui.anim_type.currentText()

        # each type will give different action path list to find action
        # asset type will show only loop animation action of that character
        if self.types == 'asset':
            self.ui.shot.hide()
            self.ui.shot_name.hide()
            #print self.action_path_list
            #self.action_from_assetNShot()
            self.ui.shot_name.setDisabled(1)
            self.get_path()
        # shot will show only animation that character has in current shot
        
        elif self.types == 'shot':
            self.ui.shot.show()
            self.ui.shot_name.show()
            self.ui.shot_name.setEnabled(1)
            # get all sq folder in this project
            self.get_path()

        

    def get_path(self):
        self.project = self.projectWidget.projectComboBox.currentText()
        self.asset = self.ui.asset_name.currentText()
        self.anim_type = self.ui.anim_type.currentText()
        self.shot = self.ui.shot_name 
        

        self.share_anim_path = r"P:/Library/Mocap/share/human/anim"
        self.asset_anim_path = r"P:/%s/asset/publ/char/%s/mocap/anim"%(self.project,self.asset)
        #self.shot_path = r"P:/%s/scene/publ"%(self.project)


        if self.project != 'Library':
            if self.anim_type == 'asset':
                self.action_path = self.asset_anim_path

            elif self.anim_type == 'shot':
                
                index = self.ui.shot_name.currentIndex()
                shot_path = self.ui.shot_name.itemData(index, QtCore.Qt.UserRole)

                action_raw_path = os.path.join(shot_path,self.asset)
                self.action_path = action_raw_path.replace('\\','/')

        else:
            self.action_path = self.share_anim_path

        print 'action path : %s'%self.action_path



    def shot_name_changed(self): 
        """ list action and fill to action widget """ 
        self.get_path()

    def batch_action_folder(self):
        items = self.dropWidget.selectedItems()
        self.fbx_path_list = []
        if items:
            for item in items:
                data = item.data(QtCore.Qt.UserRole)
                self.fbx_path_list.append(data)
    


        python_path = r'C:\Program Files\Autodesk\Maya2018\bin\mayapy.exe'
        batchPath = os.path.join(moduleDir,'publishAction.py')
        for fbx_path in self.fbx_path_list:
            proc_obj  = subprocess.Popen([python_path, batchPath, fbx_path, self.action_path], 
                stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        out, err = proc_obj.communicate()
        print('out -> %s' % out.decode())
        print('err -> %s' % err.decode())

        print self.fbx_path_list


    # def action_selected(self): 
    #     """ what happen when click action """ 
    #     item = self.ui.listWidget.currentItem()
    #     self.fbx_file_name =  item.text()
    #     if item:
    #         data = item.data(QtCore.Qt.UserRole)
    #         self.action_folder_path = data


    # def get_action_list(self):
    #     # query action folder from all arugement
    #     self.action_folder_name_list = []
    #     self.action_folder_path_list = []
    #     for path in self.action_path_list:
    #         action_folder_list = fu.listFolder(path)
    #         if action_folder_list:
    #             for action_folder in action_folder_list:
    #                 self.action_folder_name_list.append(action_folder)
    #                 self.action_folder_path_list.append(os.path.join(path,action_folder).replace('\\','/'))
    
    # def bake_cahracter(self):
    #     # get ma file from select action widget
    #     mocap_file_path = os.path.join(self.action_folder_path,self.fbx_file_name+'.mb')
    #     self.create_hik_from_path(mocap_file_path)

       
    # def filter_action_list(self):
    #     #search ='', get string from search_nam lineBox
    #     #action_list= [] # get list from existing action list
    #     # file name in action_list with search string
    #     # new action_list = []
    #     # for file in action_list:
    #         #if search in file:
    #             # new_action_list.append(search)

    #     #self.action_list = new_action_list
    #     pass


    # def get_shot(self):
    #     scene_name = context_info.ContextPathInfo()
    #     if scene_name.valid:
    #         self.shot = scene_name.name
    #         shot_list = scene_name.path.name().split('/')[1:6]
    #         shot_list[2] = 'publ'
    #         self.cur_shot_publ_path = 'P:/%s'%('/'.join(shot_list))
    #         self.ui.anim_type.setCurrentIndex(1)
    #         self.get_type()
    #     else:
    #         self.shot =''
    #         self.sq = ''
    #         self.ui.anim_type.clear()
    #         self.ui.anim_type.addItems(['asset','all shot'])
    #         self.ui.anim_type.setCurrentIndex(1)
    #         self.get_type()


    # def get_project_asset(self,path):
    #     project = path.split('/')[1]
    #     asset = path.split('/')[5]

    #     return project,asset



def show(mode='default'):
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = MocapLibrary(maya_win.getMayaWindow())
        myApp.show()
        return myApp


if __name__ == '__main__':
    show()
