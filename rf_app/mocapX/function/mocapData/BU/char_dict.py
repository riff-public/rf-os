from collections import OrderedDict

char_dict = {
"Hips": [1, "Root_Jnt","Root_Ctrl"],
"LeftUpLeg": [2, "UpLeg_L_Jnt","UpLegFk_L_Ctrl"],
"LeftLeg": [3, "LowLeg_L_Jnt","LowLegFk_L_Ctrl"],
"LeftFoot": [4, "Ankle_L_Jnt","AnkleFk_L_Ctrl"],
"RightUpLeg": [5, "UpLeg_R_Jnt","UpLegFk_R_Ctrl"],
"RightLeg": [6, "LowLeg_R_Jnt","LowLegFk_R_Ctrl"],
"RightFoot": [7, "Ankle_R_Jnt","AnkleFk_R_Ctrl"],
"Spine": [8, "Spine1Pos_Jnt","Spine1_Ctrl"],
"LeftArm": [9, "UpArm_L_Jnt","UpArmFk_L_Ctrl"],
"LeftForeArm": [10, "Forearm_L_Jnt","ForearmFk_L_Ctrl"],
"LeftHand": [11, "Wrist_L_Jnt","WristFk_L_Ctrl"],
"RightArm": [12, "UpArm_R_Jnt","UpArmFk_R_Ctrl"],
"RightForeArm": [13, "Forearm_R_Jnt","ForearmFk_R_Ctrl"],
"RightHand": [14, "Wrist_R_Jnt","WristFk_R_Ctrl"],
"Head": [15, "Head_Jnt","Head_Ctrl"],

"LeftShoulder": [18, "Clav_L_Jnt","Clav_L_Ctrl"],
"RightShoulder": [19, "Clav_R_Jnt","Clav_R_Ctrl"],
"Neck": [20, "Neck_Jnt","Neck_Ctrl"],
"Spine1": [23, "Spine2Pos_Jnt","Spine2_Ctrl"],
"Spine2": [24, "Spine3Pos_Jnt","Spine3_Ctrl"],
"Spine3": [25, "Spine4Pos_Jnt","Spine4_Ctrl"],

"LeftHandThumb1": [50, "Thumb1_L_Jnt", 'Thumb1_L_Ctrl'],
"LeftHandThumb2": [51, "Thumb2_L_Jnt", 'Thumb2_L_Ctrl'],
"LeftHandThumb3": [52, "Thumb3_L_Jnt ",'Thumb3_L_Ctrl'],

"LeftHandIndex1": [54, "Index1_L_Jnt", 'Index1_L_Ctrl'],
"LeftHandIndex2": [55, "Index2_L_Jnt", 'Index2_L_Ctrl'],
"LeftHandIndex3": [56, "Index3_L_Jnt", 'Index3_L_Ctrl'],
"LeftHandIndex4": [57, "Index4_L_Jnt", 'Index4_L_Ctrl'],

"LeftHandMiddle1": [58, "Middle1_L_Jnt", 'Middle1_L_Ctrl'],
"LeftHandMiddle2": [59, "Middle2_L_Jnt", 'Middle2_L_Ctrl'],
"LeftHandMiddle3": [60, "Middle3_L_Jnt", 'Middle3_L_Ctrl'],
"LeftHandMiddle4": [61, "Middle4_L_Jnt", 'Middle4_L_Ctrl'],

"LeftHandRing1": [62, "Ring1_L_Jnt", 'Ring1_L_Ctrl'],
"LeftHandRing2": [63, "Ring2_L_Jnt", 'Ring2_L_Ctrl'],
"LeftHandRing3": [64, "Ring3_L_Jnt", 'Ring3_L_Ctrl'],
"LeftHandRing4": [65, "Ring4_L_Jnt", 'Ring4_L_Ctrl'],

"LeftHandPinky1": [66, "Pinky1_L_Jnt", 'Pinky1_L_Ctrl'],
"LeftHandPinky2": [67, "Pinky2_L_Jnt ", 'Pinky2_L_Ctrl'],
"LeftHandPinky3": [68, "Pinky3_L_Jnt", 'Pinky3_L_Ctrl'],
"LeftHandPinky4": [69, "Pinky4_L_Jnt", 'Pinky4_L_Ctrl'], 

"RightHandThumb1": [74, "Thumb1_R_Jnt", 'Thumb1_R_Ctrl'],
"RightHandThumb2": [75, "Thumb2_R_Jnt", 'Thumb2_R_Ctrl'],
"RightHandThumb3": [76, "Thumb3_R_Jnt", 'Thumb3_R_Ctrl'],

"RightHandIndex1": [78, "Index1_R_Jnt", 'Index1_R_Ctrl'],
"RightHandIndex2": [79, "Index2_R_Jnt", 'Index2_R_Ctrl'],
"RightHandIndex3": [80, "Index3_R_Jnt", 'Index3_R_Ctrl'],
"RightHandIndex4": [81, "Index4_R_Jnt", 'Index4_R_Ctrl'],

"RightHandMiddle1": [82, "Middle1_R_Jnt", 'Middle1_R_Ctrl'],
"RightHandMiddle2": [83, "Middle2_R_Jnt", 'Middle2_R_Ctrl'],
"RightHandMiddle3": [84, "Middle3_R_Jnt", 'Middle3_R_Ctrl'],
"RightHandMiddle4": [85, "Middle4_R_Jnt", 'Middle4_R_Ctrl'],

"RightHandRing1": [86, "Ring1_R_Jnt", 'Ring1_R_Ctrl'],
"RightHandRing2": [87, "Ring2_R_Jnt", 'Ring2_R_Ctrl'],
"RightHandRing3": [88, "Ring3_R_Jnt", 'Ring3_R_Ctrl'],
"RightHandRing4": [89, "Ring4_R_Jnt", 'Ring4_R_Ctrl'],

"RightHandPinky1": [90, "Pinky1_R_Jnt", 'Pinky1_R_Ctrl'],
"RightHandPinky2": [91, "Pinky2_R_Jnt", 'Pinky2_R_Ctrl'],
"RightHandPinky3": [92, "Pinky3_R_Jnt", 'Pinky3_R_Ctrl'],
"RightHandPinky4": [93, "Pinky4_R_Jnt", 'Pinky4_R_Ctrl'],}