import os
import re
from collections import defaultdict, OrderedDict
import time

# logger
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc
import maya.mel as mel

ANIM_CURVE_NAME = 'ANIM_CURVE'
CONSTRAINTED_ATTRS = ('tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz')
TEMP_NAMESPACE = 'animIOTMP_001'
ANIM_OBJ_ATTR = 'animObj'

from rf_app.anim.animIO import core
reload(core)

class MocapAnimAsset(core.AnimAsset):
	def __init__(self, root):
		super(MocapAnimAsset,self).__init__(root)

	def import_anim(self, data, ns = None, replaceCompletely=False,time_offset=0):
		if replaceCompletely:
			option = 'replaceCompletely'
		else:
			option = 'replace'
		
		namespace = ''
		if ns !=None:
			namespace = '%s:'%ns
		else:
			if data.name:
				namespace = '%s:' %data.name

		start = 1
		if data.start:
			start = data.start
			
		
		# import anim curve
		crvs = mc.file(data.path, i=True, returnNewNodes=True, type='mayaAscii')

		# --- collect anim connections and anim nodes
		animNodes = []
		animDict = {}

		t_range = mc.keyframe(crvs,q=1,tc=1)
		start = min(t_range)
		end = max(t_range)

		for crv in crvs:
			# find anim controller
			animObjValue = mc.getAttr('%s.%s' %(crv, ANIM_OBJ_ATTR))
			animObjName = core.addDagPathNamespace(path=animObjValue, namespace=namespace)
			animAttr = None

			animAttrs = mc.ls(animObjName)
			if not animAttrs or len(animAttrs) != 1:
				logger.error('Cannot find: %s' %(animObjName))
				continue
			animAttr = animAttrs[0]

			animNode = animAttr.split('.')[0]
			animNodes.append(animNode)
			
			animDict[animAttr] = crv 

		# clear channels - remove unwanted keys, constraints and pairBlends
		#self.clearChannels(animNodes=animNodes)
		print animNodes
		# --- Copy keyframes
		for animAttr, crv in animDict.iteritems():
			# make sure attribute exists, in case it got cleared by clearChannels
			animAttrInputs = []
			try:
				# get keys
				animAttrInputs = mc.listConnections(animAttr, type='animCurve')
			except:
				continue
			
			keys = []
			if animAttrInputs:
				keys = [c for c in animAttrInputs if not mc.referenceQuery(c, inr=True) and not mc.lockNode(c, q=True)[0]]

			# set new keyframe
			if not keys:
				key_result = mc.setKeyframe(animAttr,t=time_offset)
				if not key_result:
					logger.error('Cannot set key: %s' %animAttr)
					continue
				keys = mc.findKeyframe(animAttr, curve=True)

			# copy + paste the key
			paste_result = 0
			try:
				mc.copyKey(crv, hierarchy=False, controlPoints=False, shape=False, option='curve',t = (start,end))
				if start >0:
					paste_time = time_offset-start
				else:
					paste_time = time_offset
				paste_result = mc.pasteKey(keys[0], option=option, copies=1, connect=False, timeOffset= paste_time, floatOffset=0, valueOffset=0)
			except Exception, e:
				pass 
			if not paste_result:
				logger.error('Cannot copy key: %s' %animAttr)
				continue

		# --- set poses
		metadata = core.readMetaData(data.path)
		if 'poses' in metadata:
			poses = metadata['poses']
			for objName, values in poses.iteritems():
				objNameNs = core.addDagPathNamespace(path=objName, namespace=namespace)

				objNameLs = mc.ls(objNameNs)
				if not objNameLs or len(objNameLs) != 1:
					logger.error('Cannot find: %s' %(objNameNs))
					continue
				poseObj = objNameLs[0]
					
				for attr, value in values.iteritems():
					if poseObj not in self.root:
						try:
							mc.setAttr('%s.%s' %(poseObj, attr), value)
						except:
							logger.error('Error setting pose: %s %s %s' %(poseObj, attr, value))

		# delete imported anim curves
		mc.delete(crvs)
		return data.path

	def export_anim(self, name, path, start, end, metadata={}):
		objWithInputs, poses, pivots = core.getAllInputs(root=self.root)
		animCrvs = []
		# bake constraint and pairBlend
		with core.BakeConstraintsInHierarchy(objWithInputs, start, end) as bakeCons:
			# add ANIM_OBJ_ATTR attribute to anim curves
			for obj, attrInputs in objWithInputs.iteritems():
				for objAttr, srcAttr in zip(attrInputs[0::2], attrInputs[1::2]):
					# if the object is baked, re-list the connections
					if objAttr in bakeCons.baked:
						srcAttrs = mc.listConnections(objAttr, s=True, d=False, p=True)
						if srcAttrs:
							srcAttr = srcAttrs[0]
						else:
							continue

					srcNode = srcAttr.split('.')[0]
					animCrv = None
					# a key frame is connected
					if mc.objectType(srcNode, isa='animCurve'):
						animCrv = srcNode
					# pairBlend is connected
					elif mc.objectType(srcNode, isa='pairBlend'):
						attrName = '.'.join(objAttr.split('.')[1:])
						pbAttrs = mc.listAttr(srcNode, st='i%s*' %attrName, sn=True)
						if pbAttrs:
							for attr in pbAttrs:
								pbInputs = mc.listConnections('%s.%s' %(srcNode, attr), s=True, d=False, type='animCurve')
								if pbInputs:
									animCrv = pbInputs[0]
									break
				
					if animCrv:
						# duplcate the anim curve
						dupCrv = mc.duplicate(animCrv, n='%s1' %ANIM_CURVE_NAME)[0]

						# set ANIM_OBJ_ATTR attr
						try:
							mc.addAttr(dupCrv, ln=ANIM_OBJ_ATTR, dt='string')
						except:
							pass

						animAttr = '.'.join(objAttr.split('.')[1:])
						objName = core.removeDagPathNamespace(obj)
						animName = '%s.%s' %(objName, animAttr)
						mc.setAttr('%s.%s' %(dupCrv, ANIM_OBJ_ATTR), animName, type='string')

						# insert key frame on start and end
						mc.setKeyframe(dupCrv, i=True, t=(start, end))
						animCrvs.append(dupCrv)

		if not animCrvs:
			logger.error('Cannot find animation to export: %s' %name)
			return

		# export anim curves
		mc.select(animCrvs)
		mc.file(path, es=True, type="mayaAscii", f=True)
		mc.delete(animCrvs)

		# attach ref_path and pose metadata
		refPath = ''
		if mc.referenceQuery(self.root, inr=True):
			refPath = mc.referenceQuery(self.root, f=True, wcn=True)
		metadata['ref_path'] = str(refPath)
		metadata['poses'] = dict(poses)
		core.attachMetaData(path, metadata)

		if not name:
			name = os.path.basename(path)

		data = core.AnimData(name=name, path=path, start=start, end=end, metadata=metadata)
		return data


		