# HIK DEFINITION NEWRIG V 2.0
import maya.cmds as mc
import maya.mel as mel
import pymel.core as pmc
import os
import maya.app.hik.retargeter as rte
import sys

#from mocapData import char_list as cl
#reload(cl)

from mocapData import mocapData
reload(mocapData)


def get_ns():
    ''' get NameSpace for another usage'''
    # sel = mc.ls(sl=True)
    # if ':' in sel[0]:
    #     ns = sel[0].split(':')[0]
    #     return ns
    # else:
    #     return None
    ns = pmc.PyNode(mc.ls(sl=True)[0]).namespace()
    return ns

# def tPose_set(from_asset = 0):
#     '''
#     set Character Rig to Tpose Position and switch IK to FK Control.
#     '''

#     ns = pmc.PyNode(mc.ls(sl=True)[0]).namespace()
    
#     if from_asset:
#         fld_path = mocapData.get_asset_path(mc.ls(sl=True)[0])
#     else:
#         fld_path = mocapData.get_mocap_folder()
    
#     tPose_file_name = 'tPose'
#     tPose_path = os.path.join(fld_path, tPose_file_name)
    
#     if os.path.isdir( fld_path ):
#         mocapData.read_tpose(tPose_path,ns)
#     else:
#         pass
    

def tPose_set(from_asset = 0):
    '''
    set Character Rig to Tpose Position and switch IK to FK Control.
    '''
    ns = pmc.PyNode(mc.ls(sl=True)[0]).namespace()
    
    if from_asset:
        proj,asset = mocapData.get_project_from_asset(mc.ls(sl=True)[0])
    else:
        proj,asset = mocapData.get_project_from_scene()

    tPose_file_name = 'tPose'
    #fld_path = mocapData.get_mocap_folder(proj,asset)
    mocapData.read_tpose(proj,asset,ns)
    #tPose_path = os.path.join(fld_path, tPose_file_name)

    #print 'tPose_path : %s'%tPose_path
    #if os.path.isdir( fld_path ):
        #mocapData.read_tpose(tPose_path,ns)
    

# create HIK Definition for Character
def create_char(ns='',char_name='Character1',char_list = []):
    # get rig hik data from path
    if mel.eval('hikHasDefinition( "%s")'%(char_name)):
        return

    mel.eval('hikCreateCharacter( "%s" );'%char_name)
    mel.eval('hikUpdateCharacterList();')
    mel.eval('hikSelectDefinitionTab();')
    
    for part_list in char_list:
        #part_data = char_dict[part]
        partNumList = part_list[1]
        charJntList = part_list[2]

        if ns:
            if ns[-1] == ':':
                charJnt = '{}{}'.format(ns,charJntList)
            else: 
                charJnt = '{}:{}'.format(ns,charJntList)

        else:
            charJnt = charJntList
        if pmc.objExists(charJnt):
            mel.eval('setCharacterObject("%s", "%s" , %s ,0);'%(charJnt, char_name, partNumList))
    
    #mel.eval("hikCreateCustomRig(hikGetCurrentCharacter());")
    mel.eval('hikSetCurrentCharacter "%s" ;'% char_name)
    mel.eval('hikSetCurrentSourceFromCharacter("%s");'%char_name)
    mel.eval("hikUpdateSourceList();")

    mel.eval("hikDefinitionUpdateCharacterLists;")
    mel.eval("hikDefinitionUpdateBones")
    mel.eval("HIKCharacterControlsTool;")

# connect Controller to CustomRig Map
def create_hik_custom_rig(ns='', char_name = '',char_list = []):
    ''' This Part is creating customRig map node by using MAYA HIK MEL MODULE'''
   
    # check if this character has custom rig map so do not do anything.
    if mel.eval('hikHasCustomRig( "%s")'%(char_name)):
        return
    # create rig custom map
    try:
        mel.eval("hikCreateCustomRig(hikGetCurrentCharacter());")
    except:
        mel.eval("hikCreateCustomRig(hikGetCurrentCharacter());")
    
    for part_list in char_list:
        part = part_list[0]
        partNum = part_list[1]
        partCtlList = part_list[3]
        
        if ns:
            if ns[-1] == ':':
                partCtl = '{}{}'.format(ns,partCtlList)
            else:
                partCtl = '{}:{}'.format(ns,partCtlList)
        else:
            partCtl = partCtlList
        
       
        char_name = mel.eval("hikGetCurrentCharacter() ;")
        
        if mc.objExists(partCtl):
            mc.select(partCtl)    
            # get character name
            retargeter = mel.eval('RetargeterGetName("%s");'%(char_name))

            # check character control
        
            # do rig custom map
            if part == 'Hips':
                retargetCom = 'RetargeterAddMapping("%s", "%s", "R", "%s",%s) ;' %(retargeter, part, partCtl, partNum)
                mel.eval(retargetCom)
        
                retargetCom = 'RetargeterAddMapping("%s", "%s", "T", "%s",%s) ;' %(retargeter, part, partCtl, partNum)
                mel.eval(retargetCom)
            else :
                retargetCom = 'RetargeterAddMapping("%s", "%s", "R", "%s",%s) ;' %(retargeter, part, partCtl, partNum)
                mel.eval(retargetCom)

            mel.eval('hikUpdateCustomRigAssignedMappings "%s";'%(char_name))
            mel.eval('hikCustomRigToolWidget -e -sl %s;'%(partNum))
            mel.eval("hikUpdateCustomRigUI;")


def delete_HIK_unknown_node(char_name='Character1'):
    '''' This script will delete Character Node and delete etc Node that made during retargeting '''
    # delCom = "deleteCharacter("+'"'+Character+'"'+");"
    # mel.eval(delCom)
    mel.eval('hikDeleteCharacter("%s")'%(char_name))
    mc.delete(mc.ls(type='composeMatrix'))
    mc.delete(mc.ls(type='eulerToQuat'))
    mc.delete(mc.ls(type='quatInvert'))
    mc.delete(mc.ls(type='CustomRigDefaultMappingNode'))


def set_hik_source(char_name = 'Character1', source_char = ''):
    # set source to character
    mel.eval('hikSetCurrentCharacter "%s" ;'% char_name)
    if mel.eval('hikHasDefinition( "%s")'%(char_name)):
      # mel.eval('hikSetCurrentSource( "%s" ) ;hikUpdateStatusLineUI() ;hikUpdateContextualUI();'%source_char)
       mel.eval('hikSetCharacterInput( "%s", "%s" )'%(char_name,source_char))


# import pymel.core as pmc

# def set_hik_source(char_name = 'Character1', source_char = ''):
#     # set source to character
#     mel.eval('hikSetCurrentCharacter "%s" ;'% char_name)
#     if mel.eval('hikHasDefinition( "%s")'%(char_name)):
#       # mel.eval('hikSetCurrentSource( "%s" ) ;hikUpdateStatusLineUI() ;hikUpdateContextualUI();'%source_char)
#        mel.eval('hikSetCharacterInput( "%s", "%s" )'%(char_name,source_char))


def bake_hik_range(source , start,end, char_name,ctrl_list,path):
    set_hik_source(char_name, source)
    
    mc.bakeResults(ctrl_list, 
    at = ['translate','rotate'],
    simulation =  1,
    t =  (start,end) ,
    sampleBy =  1 ,
    oversamplingRate =  1 ,
    disableImplicitControl =  1 ,
    preserveOutsideKeys =  1 ,
    sparseAnimCurveBake =  0 ,
    removeBakedAttributeFromLayer =  0 ,
    removeBakedAnimFromLayer =  0,
    bakeOnOverrideLayer =  0 ,
    minimizeRotation =  1 ,
    controlPoints =  0 ,
    shape =  1 )
    mel.eval( 'hikBakeCharacterPost( "%s" );'%('jax'))
    
    anim_crv = exportAnimCurve(ctrl_list, pmc.PyNode(source).namespace()[:-1],path)
    mc.delete(anim_crv)
    
#for src in mc.ls(type = 'HIKCharacterNode'):
    #bake_hik_range(src,0,15,'jax',ctrl_list,r'C:\Users\pornpavis.t\Desktop\testExpAnim')


def exportAnimCurve(ctrlLs, dirName):
    get_ctrl_dict = []
    animCurve_dict = {}
    mc.select(cl=True)
    for obj in ctrlLs:
        all_animCrv = mc.keyframe(obj, q=True, n=1)
        mc.select(all_animCrv, add=True)
        get_attr_dict = []
        ctrl_dict = {}
        for y,OBJ in enumerate(all_animCrv):
            attr = OBJ.split("_")[-1]
            
            anim_name = {}
            anim_type = {}
            anim_value = {}
            attr_dict = {}
            
            anim_name["name"] = all_animCrv[y]
            anim_type["type"] = mc.nodeType(OBJ)
            anim_value["value"] = mc.keyframe(OBJ, q=1, eval=1) # get from current frame
            
            attr_dict["%s"%attr] = [anim_name, anim_type, anim_value]
            get_attr_dict.append(attr_dict)
        
        ctrl_dict["%s"%obj] = get_attr_dict
        get_ctrl_dict.append(ctrl_dict)
        
    animCurve_dict["animCurve"] = get_ctrl_dict
    anim_crv = mc.ls(sl=True)
    #----------------------------------------------------------------------------- dict
        
    filePath = mc.file(q=True, sn=True)
    fileName = os.path.basename(filePath)
    
    path = r"C:\Users\pornpavis.t\Desktop\testExpAnim"
    if not os.path.isdir(path + r"\{}".format(dirName)):
        os.mkdir(path + r"\{}".format(dirName))
        
    with open(path + r"\{}\animCurve.json".format(dirName), "w") as P:
        json.dump(animCurve_dict, P, indent=3)
    
    mc.file(path + r"\{}\animCurve.ma".format(dirName), exportSelected=True, preserveReferences=True, type="mayaAscii", force=True)
    mc.select(cl=True)
    return anim_crv

# Anim Layer 
# def createControlList(ctlList=[],ns=''):
#     '''createController List for another usage  '''
#     if ns!='':          
#         newCtlList = [ns+':'+i for i in ctlList if i != '']
#     else:
#         newCtlList = ctlList
#     print newCtlList
#     return newCtlList

# def moveAnimLayer(ctlList=[],ns=''):
#     ''' create Mocap and Animation AnimLayer by using controllerList and namespace '''
#     newCtlList = createControlList(ctlList,ns)
#     anim = mc.animLayer('%s:Anim'%ns)
#     mc.animLayer(anim,newCtlList,aso=1,e=1)
#     mc.select(newCtlList)
#     mel.eval("string $lSelection[]=`ls -selection`;string $layers[]={" +"\"" +"BaseAnimation" +"\"" +"}; layerEditorExtractObjectsAnimLayer($lSelection, $layers );")
#     mocapLayer = mc.rename('BaseAnimation_extract','%s:Mocap'%ns)
    


# run script
#def runAll()
#NS=get_ns()
#create_hik_custom_rig(NS)
#create_hik_custom_rig(NS)
#delete_HIK_unknown_node(Character)
#moveAnimLayer(HIKCharList.partCtlList,NS)