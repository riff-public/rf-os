import maya.cmds as mc
import math
import pymel.core as pmc
import maya.mel as mel
import os
from rf_app.mocap.function import animData
reload(animData)

from rf_app.mocap.function import fkiksnap
reload(fkiksnap)

import rf_app.mocap.function.hikDef as hikd
reload(hikd)

from rf_app.mocap.function.mocapData import ioData
reload(ioData)
# def sourceHikNode():
# 	#get_hik_node = []
# 	#get_hik_jnt = []
# 	#get_shot = []
	
# 	hik_node = mc.ls(typ="HIKCharacterNode")
# 	sel_rf = mc.ls(rf=1)
# 	dicts= {}
# 	if sel_rf:
# 		#dicts = {}
# 		for rfn in sel_rf:
# 			rf_node = mc.referenceQuery(rfn, ns=True).split(":")[1]
			
# 			start_end = []
# 			for obj in hik_node:
# 				if rf_node in obj:

# 					hik_jnt = rfn.replace("RN", ":Reference")
					
# 					#get_hik_node.append(obj)
# 					#get_hik_jnt.append(hik_jnt)
# 					#get_shot.append(rf_node)
					
# 					startframe = mc.findKeyframe(hik_jnt, w='first')
# 					endframe = mc.findKeyframe(hik_jnt, w='last')
# 					int_endframe = math.ceil(endframe)
# 					start_end.extend([startframe, int_endframe])
					
# 					dicts["%s"%obj] = start_end
									
# 	return dicts

def sourceHikNode():
	dicts={}
	hik_list = mc.ls(typ="HIKCharacterNode")
	ref_hik_list = []
	
	for hik_node in hik_list:
		if mc.referenceQuery(hik_node,isNodeReferenced=True):
			ref_hik_list.append(hik_node)
	
	for obj in ref_hik_list:
		start_end = []
		hik_jnt = obj.split(':')[0]+":Reference"
		#print hik_jnt
		#get_hik_node.append(obj)
		#get_hik_jnt.append(hik_jnt)
		#get_shot.append(rf_node)
		
		startframe = mc.findKeyframe(hik_jnt, w='first')
		endframe = mc.findKeyframe(hik_jnt, w='last')
		int_endframe = math.ceil(endframe)
		start_end.extend([startframe, int_endframe])
			
		dicts["%s"%obj] = start_end
					
	return dicts

def set_hik_source(char_name = 'Character1', source_char = ''):
    # set source to character
    mel.eval('hikSetCurrentCharacter "%s" ;'% char_name)
    if mel.eval('hikHasDefinition( "%s")'%(char_name)):
      # mel.eval('hikSetCurrentSource( "%s" ) ;hikUpdateStatusLineUI() ;hikUpdateContextualUI();'%source_char)
       mel.eval('hikSetCharacterInput( "%s", "%s" )'%(char_name,source_char))


def bake_hik_range(source ,char_name,all_mover, start,end,ctrl_list,path,file_name,fkikDict={}):
	set_hik_source(char_name, source)
	ns = pmc.PyNode(all_mover).namespace()[:-1]
	mc.bakeResults(ctrl_list, 
	at = ['translate','rotate'],
	simulation =  1,
	t =  (start,end) ,
	sampleBy =  1 ,
	oversamplingRate =  1 ,
	disableImplicitControl =  1 ,
	preserveOutsideKeys =  1 ,
	sparseAnimCurveBake =  0 ,
	removeBakedAttributeFromLayer =  0 ,
	removeBakedAnimFromLayer =  0,
	bakeOnOverrideLayer =  0 ,
	minimizeRotation =  1 ,
	controlPoints =  0 ,
	shape =  1 )
	mel.eval( 'hikBakeCharacterPost( "%s" );'%(char_name))
	
	if fkikDict:
		#print 'do fkikSnap'
		#print start
		#print end
		#print ns+':'
		fkiksnap.fk_to_Ik_all(ns+':', fkikDict ,[start+1,end])

	x = animData.MocapAnimAsset(all_mover)
	ioData.create_folder(path)
	

	x.export_anim(ns, os.path.join(path,file_name), start+1,end)
	anim_crv = mc.keyframe(ctrl_list, q=True, n=1)
	# do fk/ik snap
	mc.delete(anim_crv)
	

def import_anim(all_mover,path,time_offset):
	char_anim = animData.MocapAnimAsset(all_mover)
	ns = pmc.PyNode(all_mover).namespace()[:-1]
	import_anim_data = animData.core.AnimData.init_from_file(path)
	char_anim.import_anim(import_anim_data,ns,0,time_offset)

def test():
	print test

# src = 's0020_0030_track014_t01:Character1'
# char_name = 'jax'
# all_mover = 'jax_md:AllMover_Ctrl'
# start = 100
# end = 200
# #ctrl_list = ctrl_list
# path = r'C:\Users\pornpavis.t\Desktop\test\testAnim.ma'

#bake_hik_range(src,char_name,all_mover,start,end,ctrl_list,path)


# all_mover2 = 'jaxTwo_001:AllMover_Ctrl'
# time_offset = mc.currentTime(q=1)
# import_anim(all_mover2 ,path,time_offset)



	

	