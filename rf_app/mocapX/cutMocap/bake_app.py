# v.0.0.1 polytag switcher
_title = 'Bake App'
_version = 'v.0.0.1'
_des = 'Wip'
uiName = 'mocapXUI'

#Import python modules
import sys
import os
import logging
import getpass
import traceback
from collections import OrderedDict
import pymel.core as pmc
import os
import json
import pickle
import math
moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
import rf_config as config
# framework modules
from rf_utils import log_utils
reload(log_utils)
from rf_utils.ui import load
reload(load)
from rf_utils.ui import stylesheet
from rf_utils import admin
reload(admin)
import tempfile

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui
from rf_utils.widget import project_widget
reload(project_widget)
from rf_utils.sg import sg_utils
reload(sg_utils)
from rf_utils import file_utils as fu
reload(fu)
from rf_utils.context import context_info
reload(context_info)
from rf_utils.sg import sg_process
reload(sg_process)
# maya & module function 
import pymel.core as pmc
import maya.cmds as mc
import maya.mel as mel

from rf_maya.rftool.rig.MocapX import customMocapX as cmx
reload(cmx)

from rf_maya.rftool.shade import shade_utils
reload(shade_utils)


def read_pickle_data(file_path):
	with open(file_path, 'r') as attr_file:
		return pickle.load(attr_file)

def write_pickle_data(data, file_path):
	with open(file_path, 'w') as attr_file:
		pickle.dump(data,attr_file)


class Bake(QtWidgets.QMainWindow):
	def __init__(self, parent=None):
		#Setup Window
		super(Bake, self).__init__(parent)
		self.poseLibName = 'PoseLib'
		self.collectionName = 'AttributeCollection'
		self.poseList = cmx.poseList
		self.eyePoseRotList = cmx.eyePoseRotList
		self.headPoseList= cmx.headPoseList

		# ui read
		uiFile = '%s/bake_app.ui' % moduleDir
		self.ui = load.setup_ui_maya(uiFile, parent)
		# set size 
		self.w = 500
		self.h = 600 

		self.setCentralWidget(self.ui)
		self.setObjectName(uiName)
		self.resize(self.w, self.h)
		self.setWindowTitle('%s %s %s' % (_title, _version, _des))
		#self.ui.exListWidget.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
		self.mocapXData = 'mocapX_data'
		self.all_mocap = []
		self.asset_mocap_dict = {}
		self.shot_mocap = []
		self.show_mocap = []
		self.search_char = ''
		self.search_name = ''

		self.imProjComboBox = project_widget.SGProjectComboBox(sg=sg_utils.sg, layout=QtWidgets.QHBoxLayout())
		self.imProjComboBox.allLayout.setStretch(1, 1)
		self.imProjComboBox.label.setText('Project : ')
		#self.mocapX_list
		#self.bake_anim_list = []
		self.projectWidget = project_widget.SGProjectComboBox(sg=sg_utils.sg, layout=QtWidgets.QHBoxLayout())
		self.projectWidget.setContentsMargins(0, 0, 3, 0)
		self.projectWidget.label.setText('Project : ')
		# #spacerItem = QtWidgets.QSpacerItem(10, 10, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
		# #self.projectLayout.addItem(spacerItem)
		# self.projectWidget.projectComboBox.setCurrentText(self.mocap_char_project)
		# self.ui.asset_name.setCurrentText(self.mocap_char_asset)
		self.init_import_function()
		self.init_signal()


	def createMocapXSys(self,*args):
		self.all_ctrl_list = self.list_add_ns(self.ctrlList)
		cmx.createMocapXSystem(self.poseLibName,self.collectionName,self.poseList,'',self.ctrlList)
		cmx.multipleUpdatePose(self.ctrlList,self.poseList)
		cmx.deleteKey(self.ctrlList)

	def connectPose(self,*args):
		cmx.connectAll('','MocapX',self.ctrlList,self.headCtrlList,self.eyeCtrlList)

	def createMocapXDevice(self,*args):
		cmx.createMocapXDevice()

	def create_mocapX(self,*args):
		self.createMocapXSys()
		self.createMocapXDevice()
		self.connectPose()
		

#===========================================================================================================#
	#import function
	def init_import_function(self):
		# self.proj_list_cbb()
		self.ui.horizontalLayout_9.addWidget(self.imProjComboBox,0)
		self.imProjComboBox.projectComboBox.activated.connect(self.import_get_project)
		self.import_get_project()
		#self.add_button()
		# self.date_list_cbb()
		# self.obj_listWidget()
		self.ui.imAnimType.activated.connect(self.update_show_list)
		self.ui.imAssetComboBox.activated.connect(self.update_show_list)
		self.ui.search_name.returnPressed.connect(self.update_show_list)
		self.init_mocapX_data()
		self.ui.imListWidget.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
		self.ui.mcpxInScene.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
		self.ui.importButton.clicked.connect(self.import_mocapX_path)
		self.ui.removeButton.clicked.connect(self.remove_mocapX_path)

		if not mc.pluginInfo('mocapx_plugin',loaded=1,q=1):
			mc.loadPlugin('mocapx_plugin')

	# def proj_list_cbb(self):
	# 	self.ui.imProjComboBox.clear()
	# 	self.proj_ls = os.listdir(r"P:\Library\MocapX")
	# 	self.ui.imProjComboBox.addItems(self.proj_ls)

	# def change_dateList(self, *args):
	# 	self.ui.imDateComboBox.clear()
	# 	get_proj_list =  self.ui.imProjComboBox.currentText()
	# 	get_date = os.listdir(r"P:\Library\MocapX\%s"%get_proj_list)
	# 	self.ui.imDateComboBox.addItems(get_date)

	# def date_list_cbb(self):
	# 	self.change_dateList()
	# 	self.ui.imProjComboBox.currentTextChanged.connect(self.change_dateList)

	# def change_animList(self, *args):
	# 	self.ui.imListWidget.clear()
	# 	get_proj_list =  self.ui.imProjComboBox.currentText()
	# 	get_date_list = self.ui.imDateComboBox.currentText()
	# 	self.date_path = r"P:\Library\MocapX\%s\%s"%(get_proj_list, get_date_list)
	# 	self.maya_path = os.path.join(self.date_path)
	# 	try:
	# 		get_anim = os.listdir(self.maya_path )

	# 		self.all_anim_data = []
	# 		for path in get_anim:
	# 			#print path
	# 			#print path.split('.')[-1]
	# 			if 'mcpx' in path.split('.')[-1]:
	# 				item = QtWidgets.QListWidgetItem(self.ui.imListWidget)
	# 				item.setText(path)
	# 				item.setData(QtCore.Qt.UserRole, os.path.join(self.maya_path ,path).replace('\\','/'))
					
	# 				self.all_anim_data.append(os.path.join(self.maya_path ,path).replace('\\','/'))
	# 	except:
	# 		self.all_anim_data = []

	def import_get_project(self):
		# change self.project f om project from comboBox
		self.im_project = self.imProjComboBox.projectComboBox.currentText()
		context = context_info.Context()
		context.update(project= self.im_project, entityType='asset', entityGrp ='char')
		asset_char = context_info.ContextPathInfo(context = context)
		# after change project it will change asset in combo box
		try: 
			filter_asset_list = fu.listFolder("{}".format(asset_char.path.scheme(key='publishAssetTypePath').abs_path()))
			
			if filter_asset_list:
				# filter asset that have mocap data
				check_context = context_info.Context()

				check_context.use_sg(sg_utils.sg, self.im_project, 'asset')
				check_asset = context_info.ContextPathInfo(context = check_context)

				asset_list = ['all']
				self.asset_mocap_dict['all'] = []
				for asset in filter_asset_list:
					if asset == 'mocap':
						continue
					else:
						check_asset.context.update(step='mocapX', process='', res='', entity=asset,entityGrp ='char')
						check_mocap = check_asset.path.scheme(key='publishProcessPath').abs_path()
						mocap_data_path = os.path.join("{}".format(check_mocap))
						mocap_anim_path = os.path.join("{}".format(check_mocap),'raw')
						
						if os.path.isdir(mocap_data_path):
							asset_list.append(asset)
							self.asset_mocap_dict[asset] = []
						
						if os.path.isdir(mocap_anim_path):
							asset_mocap_list = fu.listFile(mocap_anim_path)
							
							if asset_mocap_list:
								for asset_mocap_file in asset_mocap_list:
									self.asset_mocap_dict[asset].append([asset_mocap_file,os.path.join(mocap_anim_path,asset_mocap_file)])
									self.asset_mocap_dict['all'].append([asset_mocap_file,os.path.join(mocap_anim_path,asset_mocap_file)])
								#for asset in asset_mocap_list:
									#self.asset_mocap.append()
							else:
								pass

				self.ui.imAssetComboBox.clear()
				self.ui.imAssetComboBox.addItems(asset_list)
			else:
				asset_list = []
			
		except Exception as e:
			asset_list = []

		self.ui.imAssetComboBox.clear()
		self.ui.imAssetComboBox.addItems(asset_list)
		self.change_animList()
		self.update_show_list()
		if not asset_list:
			mc.confirmDialog(m = '"{}" cannot do motion capture right now.'.format(self.im_project))
			
	def change_animList(self, *args):
		get_proj =  self.imProjComboBox.projectComboBox.currentText()
		shot_list = [sg_shot['code'] for sg_shot in sg_process.get_all_shots(get_proj)]
		shot_context = context_info.Context()
		shot_context.update(project=get_proj , entityType='scene')
		scene = context_info.ContextPathInfo(context=shot_context)
		self.shot_mocap = []
		#print shot_list
		for shot in shot_list:
			scene.context.update(entity = shot)
			scene.update_scene_context()
			shot_publ_path = scene.path.scheme(key='publishShotPath').abs_path()
			path = os.path.join(shot_publ_path,'mocapX','raw')
			#print path
			if os.path.isdir(path):
				mocap_file_list = fu.listFile(path)
				if mocap_file_list:
					for mocap_file in mocap_file_list:
						self.shot_mocap.append([mocap_file,os.path.join(path,mocap_file)])
				else:
					continue
			else:
				continue

		# for shot in shot_list:
		# 	os.path.isdir(os.path.join)
		self.all_mocap = self.asset_mocap_dict['all'][:]+self.shot_mocap[:]

	def update_show_list(self):
		char_filter = self.ui.imAssetComboBox.currentText()
		search_filter = self.ui.search_name.text()
		animType = self.ui.imAnimType.currentText()
		
		if char_filter == 'all':
			char_filter =''
		self.show_mocap= []
		
		if animType == 'all':
			tmp_mocap_list = self.all_mocap[:]
		elif animType =='asset':
			tmp_mocap_list = self.asset_mocap_dict['all'][:]
		elif animType =='shot':
			tmp_mocap_list = self.shot_mocap[:]

		if char_filter and search_filter:
			print 'case1'
			for files in tmp_mocap_list:
				if char_filter in files[0] and search_filter in files[0]:
					self.show_mocap.append(files)

		elif char_filter and not search_filter:
			print 'case2'
			for files in tmp_mocap_list:
				if char_filter in files[0]:
					self.show_mocap.append(files)
					
		elif not char_filter and search_filter:
			print 'case3'
			for files in tmp_mocap_list:
				if search_filter in files[0]:
					self.show_mocap.append(files)
					
		elif not char_filter and not search_filter:
			print 'case4'
			self.show_mocap = tmp_mocap_list[:]
		
		self.show_import_widget()


	def show_import_widget(self):
		self.ui.imListWidget.clear()
		for action in self.show_mocap:
			item = QtWidgets.QListWidgetItem(self.ui.imListWidget)
			item.setText(action[0])
			item.setData(QtCore.Qt.UserRole, action[1])

	def list_add_ns(self,obj_list):
		ns_list = []
		for obj in obj_list:
			ns_list.append('{}{}'.format(self.ns,obj))
		return ns_list


	def obj_listWidget(self):
		self.change_animList()
		self.ui.imDateComboBox.activated.connect(self.change_animList)       


	def import_mocapX_path(self,*args):
		item_path_list = self.ui.imListWidget.selectedItems()
		if self.mocap_fileName_list == ['']:
			self.mocap_fileName_list = []
		
		if self.mocap_filePath_list == ['']:
			self.mocap_filePath_list = []
		
		for item in item_path_list:
			source_file = item.data(QtCore.Qt.UserRole)
			if '\\' in source_file:
				file_name = source_file.split('\\')[-1].split('.')[0]
			elif '/' in source_file:
				file_name = source_file.split('/')[-1].split('.')[0]
			
			self.mocap_fileName_list.append(file_name)
			self.mocap_filePath_list.append(source_file)

		mc.setAttr(self.mocapXData+'.file_name' , ','.join(self.mocap_fileName_list),type = 'string')
		mc.setAttr(self.mocapXData+'.file_path' , ','.join(self.mocap_filePath_list),type = 'string')
		
		self.update_mocapXInscene()

	def init_mocapX_data(self):
		if not mc.objExists(self.mocapXData):
			mc.createNode('transform', n = 'mocapX_data')
			mc.addAttr(self.mocapXData, ln ='file_name', dt = 'string')
			mc.addAttr(self.mocapXData, ln = 'file_path', dt = 'string')

		self.mocap_fileName_list = mc.getAttr(self.mocapXData+'.file_name')
		self.mocap_filePath_list = mc.getAttr(self.mocapXData+'.file_path')
		if self.mocap_filePath_list:
			self.mocap_fileName_list = mc.getAttr(self.mocapXData+'.file_name').split(',')
			self.mocap_filePath_list = mc.getAttr(self.mocapXData+'.file_path').split(',')
		else:
			self.mocap_fileName_list = []
			self.mocap_filePath_list = []
		self.update_mocapXInscene()

	def remove_mocapX_path(self, *args):
		item_path_list = self.ui.mcpxInScene.selectedItems()
		remove_list = []
		for item in item_path_list:
			idx = self.ui.mcpxInScene.row(item)
			remove_list.append(idx)
			self.ui.mcpxInScene.takeItem(idx)

		remove_list.sort()
		for i in remove_list[::-1]:
			self.mocap_fileName_list.pop(i)
			self.mocap_filePath_list.pop(i)

		mc.setAttr(self.mocapXData+'.file_name' , ','.join(self.mocap_fileName_list),type = 'string')
		mc.setAttr(self.mocapXData+'.file_path' , ','.join(self.mocap_filePath_list),type = 'string')
	
	def update_mocapXInscene(self):
		self.ui.mcpxInScene.clear()
		for idx, path in enumerate(self.mocap_filePath_list):
			item = QtWidgets.QListWidgetItem(self.ui.mcpxInScene)
			item.setText(self.mocap_fileName_list[idx])
			item.setData(QtCore.Qt.UserRole, path)

			

# #==============================================================================================================#
	
	# export function   

	def init_signal(self):
		print self.projectWidget , type(self.projectWidget)
		self.ui.horizontalLayout_11.addWidget(self.projectWidget)
		self.ui.exAddButton.clicked.connect(self.add_widgets)
		self.ui.exDelButton.clicked.connect(self.del_widgets)
		self.projectWidget.projectComboBox.activated.connect(self.get_project)
		self.ui.fromSceneButton.clicked.connect(self.auto_create_task)
		self.ui.exAssetComboBox.activated.connect(self.get_asset_mocap_path)
		self.ui.exListWidget.itemClicked.connect(self.swap_mcpx_source)
		self.ui.bakeButton.clicked.connect(self.do_all)
		self.ui.bakeSelButton.clicked.connect(self.do_sel)
		self.ui.mocapXButton.clicked.connect(self.create_char)
		self.ui.mulSel_checkBox.stateChanged .connect(self.selection_mode)
		self.ui.loadSceneButton.clicked.connect(self.load_from_scene)
		self.ui.customCheck.stateChanged.connect(self.custom_check)
		self.get_project()
		try:
			self.get_asset_mocap_path()
		except:
			pass

	def loadDict(self,path):
		if os.path.exists(path):
			with open(path, 'r') as f:
				data = json.load(f)
				return data

	def add_widgets(self, animLayer = None, meta_data = None):
		item = QtWidgets.QListWidgetItem(self.ui.exListWidget)
		if animLayer == None or meta_data == None:
			track_obj = track_item(self)
			self.ui.exListWidget.addItem(item)
			self.ui.exListWidget.setItemWidget(item,track_obj)
			item.setData(QtCore.Qt.UserRole,track_obj)
			item.setSizeHint(track_obj.sizeHint())
		else:
			track_obj = track_item.load_metaData(self,animLayer,meta_data)
			self.ui.exListWidget.addItem(item)
			self.ui.exListWidget.setItemWidget(item,track_obj)
			item.setData(QtCore.Qt.UserRole,track_obj)
			item.setSizeHint(track_obj.sizeHint())
		return track_obj

	def del_widgets(self):
		widgetItems = self.ui.exListWidget.selectedItems()
		for item in widgetItems:
			mc.delete(item.data(QtCore.Qt.UserRole).anim_layer)
			idx = self.ui.exListWidget.row(item)
			self.ui.exListWidget.takeItem(idx)

	def selection_mode(self):
		if self.ui.mulSel_checkBox.isChecked():
			self.ui.exListWidget.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
			self.ui.exListWidget.itemClicked.disconnect()

		elif not self.ui.mulSel_checkBox.isChecked():
			self.ui.exListWidget.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
			self.ui.exListWidget.itemClicked.connect(self.swap_mcpx_source)
	
	def get_project(self):
		# change self.project f om project from comboBox
		self.project = self.projectWidget.projectComboBox.currentText()
		context = context_info.Context()
		context.update(project= self.project, entityType='asset', entityGrp ='char')
		asset_char = context_info.ContextPathInfo(context = context)
		# after change project it will change asset in combo box
		try: 
			filter_asset_list = fu.listFolder("{}".format(asset_char.path.scheme(key='publishAssetTypePath').abs_path()))
			#print filter_asset_list
			
			if filter_asset_list:
				# filter asset that have mocap data
				check_context = context_info.Context()

				check_context.use_sg(sg_utils.sg, self.project, 'asset')
				check_asset = context_info.ContextPathInfo(context = check_context)	

				asset_list = []
				
				for asset in filter_asset_list:
					check_asset.context.update(step='mocapX', process='', res='', entity=asset,entityGrp ='char')
					check_mocap = check_asset.path.scheme(key='publishProcessPath').abs_path()
					if os.path.isdir(check_mocap):
						asset_list.append(asset)
				self.ui.exAssetComboBox.clear()
				self.ui.exAssetComboBox.addItems(asset_list)

			else:
				asset_list = []
		
		except Exception as e:
			asset_list = []

		self.ui.exAssetComboBox.clear()
		self.ui.exAssetComboBox.addItems(asset_list)

		#self.get_asset_mocap_path()
		if not asset_list:
			mc.confirmDialog(m = '"{}" cannot do motion capture right now.'.format(self.project))
		

	def get_asset_mocap_path(self):
		self.project = self.projectWidget.projectComboBox.currentText()
		self.asset = self.ui.exAssetComboBox.currentText()
		self.context = context_info.Context()
		self.context.use_sg(sg_utils.sg, self.project, 'asset', entityName = self.asset)
		self.asset_context = context_info.ContextPathInfo(context = self.context)
		
		self.heroPath = self.asset_context.path.hero().abs_path()
		self.asset_context.context.update(step='mocapX', process='', res='', app='')
		self.asset_mocap_path = self.asset_context.path.scheme(key='publishStepPath').abs_path()
		self.asset_anim_path = "{}/anim".format(self.asset_mocap_path)
		self.char_folder_path = self.asset_context.path.scheme(key='publishAssetTypePath').abs_path()
		self.ctrl_path = os.path.join(self.asset_mocap_path,'ctrl_list').replace('\\','/')
		self.key_path = os.path.join(self.asset_mocap_path,'mocapX_key.ma').replace('\\','/')		
		self.head_path = os.path.join(self.asset_mocap_path,'head_ctrl').replace('\\','/')
		self.eye_path = os.path.join(self.asset_mocap_path,'eye_ctrl').replace('\\','/')

		self.asset_context.context.update(step='rig',process = 'main',res='md',app='maya',look='main')
		self.md_file =self.asset_context.output_name(outputKey = 'rig',hero=True)
		self.shd_file = self.asset_context.output_name(outputKey = 'materialRig',hero=True)
		#self.asset_context.context.update(step = 'mtr',process = 'main', res = 'md',app='maya')
		self.md_path = '{}/{}'.format(self.heroPath,self.md_file)
		self.shd_path = '{}/{}'.format(self.heroPath,self.shd_file)

		self.all_mover_name = self.asset_context.projectInfo.asset.get('allMoverCtrl')
		


		self.ns = self.asset+':'
		self.rig_path = self.md_path
		self.ctrlList = self.list_add_ns(read_pickle_data(self.ctrl_path))
		self.headCtrlList = self.list_add_ns(read_pickle_data(self.head_path))
		self.eyeCtrlList = self.list_add_ns(read_pickle_data(self.eye_path))

	# def get_all_ctrl(self):
	# 	self.ctrl_list = []
		
	def create_char(self):
		#self.get_asset_mocap_path()
		# check ctrl reference
		fps = mc.currentUnit(t=1,q=1)

		mc.currentUnit(t='film')

		if not mc.objExists(self.ns[:-1]+'RN'):
			# if ctrl rig not in scene yet
			# ref one
			mc.file(self.rig_path ,r=1 ,ns = self.asset)


			if os.path.isfile(self.shd_path):
				#mc.file(self.shd_file, ns = self.asset+'_shd')
				shade_utils.apply_ref_shade(self.shd_path,self.asset+'_shd',self.asset)


			# import key to  rig
			cmx.import_fclKey(self.ns,  self.ctrlList, self.key_path,0 ,'Head_ctrl',1)
			self.create_mocapX()


		# keep dict connection from ctrl
		ctrl_list = self.ctrlList
		self.mcpx_connect_list = []
		self.mcpx_pose_connect = []
		for ctrl in ctrl_list:
			for attr in mc.listAttr(ctrl,k=1):
				ctrl_attr = ctrl+'.'+attr
				#print ctrl_attr
				if mc.connectionInfo(ctrl_attr,sfd=1):
					#print ctrl_attr
					print 'Append MCPX Connections'
					self.mcpx_connect_list.append([mc.listConnections(ctrl_attr,s=1,d=0,p=1,scn=1)[0],ctrl_attr])

		for mcpxP in mc.ls(type = 'MCPXPose'):
			#print mcpxP
			connect_list =  mc.listConnections(mcpxP,s=0,d=1,c=1,p=1)
			inputList = connect_list[::2]
			outputList = connect_list[1::2]
			
			for num in range(len(connect_list)/2):
				self.mcpx_pose_connect.append([inputList[num],outputList[num]])

		mc.currentUnit(t=fps)

	def export_mocap_anim(self,source_path,ctrl_list,start,end,path,file_name):
		#source_path = self.sourceComboBox.currentData(QtCore.Qt.UserRole)
		cmx.set_source('MocapX','ClipReader')

		cmx.set_clipReader_file('ClipReader',source_path)

		if not os.path.isdir(path):
			admin.makedirs(path)
		mc.playbackOptions(min=start,max=end,ast=start,aet=end,e=1)
		export_path = os.path.join(path,file_name)
		mc.refresh(suspend=1)

		#x = cmx.export_fclKey(self.ns,ctrl_list,export_path)
		mc.select(ctrl_list)

		x = cmx.animData.MocapXAnimAsset(ctrl_list)
		handle,tmp_path = tempfile.mkstemp(suffix='.ma')
		os.close(handle)
		# temp_dir = os.path.dirname(tmp_path)
		# temp_file = os.path.basename(tmp_path)
		
		
		try:
			cmx.bake_key()
			x.export_anim(self.ns,tmp_path,start,end)
			
			admin.copyfile(tmp_path, export_path)
			os.remove(temp_output_path)		

		except:
			mc.refresh(suspend=0)

		anim_crv = mc.keyframe(ctrl_list, q=True, n=1)
		mc.delete(anim_crv)
		mc.refresh(suspend=0)
		print self.mcpx_connect_list
		for connect_pair in self.mcpx_connect_list:
			print '{} : {}'.format(connect_pair[0],connect_pair[1])
			mc.connectAttr(connect_pair[0],connect_pair[1])

		for connect_pair in self.mcpx_pose_connect:
			if not mc.isConnected(connect_pair[0],connect_pair[1]):
				mc.connectAttr(connect_pair[0],connect_pair[1])

		#cmx.import_fclKey(self.ns,  self.list_add_ns(self.ctrlList), self.key_path)


	def do_all(self):
		self.do_task(all=1)

	def do_sel(self):
		self.do_task(all=0)

	def do_task(self,all = 1):
		# get fcl ctrl from mocapX data

		# get item list to do task
		# filter task as all or seleceted
		if all:
			times = range(self.ui.exListWidget.count())
			item_list = []
			for idx in times:
				item_list.append(self.ui.exListWidget.item(idx))
		else:
			item_list = self.ui.exListWidget.selectedItems()

		# get item data to do task
		for item in item_list:
			task = item.data(QtCore.Qt.UserRole)
			task_data = self.setAnimData(task)

			# relocate path file
			if task_data['animType'] == 'shot':
				file_name = task_data['source'].split(':')[0]+'.ma'
				export_path = os.path.join(task_data['path'],'mocapX',self.asset).replace('\\','/')

			elif task_data['animType'] == 'asset':
				file_name = task_data['source'].split(':')[0]+'.ma'
				export_path = os.path.join(self.asset_anim_path).replace('\\','/')

			source_path = task_data['source_path']
			print export_path
			print file_name
			print source_path
			print task_data['startEnd']
			self.export_mocap_anim(task_data['source_path'],self.ctrlList 
				,float(task_data['startEnd'][0]) ,float(task_data['startEnd'][1]),
				export_path,file_name)

			#anim_crv = mc.keyframe(self.ctrl_list, q=True, n=1)
			#mc.delete(anim_crv)			
		# load clipreader for baking each task from task_data

		# bake fcl ctrl key and export


		mc.confirmDialog(m='bake mocap animation complete')


	def auto_create_task(self):
		mcpx_key = self.mocap_fileName_list
		#mcpx_key = hik_node.keys()
		#mcpx_key.sort()
		
		task_context = context_info.Context()
		for name in mcpx_key :
			#frame = hik_node[name]
			item = self.add_widgets()
			item.sourceComboBox.setCurrentText(name)
			#item.startLine.setText(str(frame[0]))
			#item.endLine.setText(str(frame[1]))
			
			#hik_ns = pmc.PyNode(name).namespace()[:-1]
			splitList = name
			
			if len(splitList) >= 4:
				shotName = '_'.join(name.split('_')[:4])
				task_context.use_sg(sg_utils.sg, self.project,'scene',entityName=shotName)
				scene_context = context_info.ContextPathInfo(context=task_context)
				sq = scene_context.episode

			else:
				sq =''
				shotName = ''

			if item.sqComboBox.findText(sq) != -1:
				sqIdx  = item.sqComboBox.findText(sq)
				
				item.sqComboBox.setCurrentIndex (sqIdx)
				item.add_shot()
				
				shotIdx = item.shotComboBox.findText(shotName)
				
				item.shotComboBox.setCurrentText (shotName)
			else:
				item.animComboBox.setCurrentText('asset')
				item.sqShot_active()

			item.task_metaData()

	def swap_mcpx_source(self):
		#print self.ui.exListWidget.count()
		if self.ui.exListWidget.count():
			cur_task = self.ui.exListWidget.currentItem()
			task = cur_task.data(QtCore.Qt.UserRole)
			task.set_source()
			#task_data = self.setAnimData(task)
			#source = task_data['source']
			#hikd.set_hik_source(self.char_name,source)


	def setAnimData(self,track_obj):
		source_data = track_obj.sourceComboBox.currentText()
		source_path = track_obj.sourceComboBox.currentData()
		animType_data = track_obj.animComboBox.currentText()
		sequence_data = track_obj.sqComboBox.currentText()
		shot_data = track_obj.shotComboBox.currentText()
		startEnd_data = [track_obj.startLine.text(), track_obj.endLine.text()]
		shot_path = track_obj.shotComboBox.itemData(track_obj.shotComboBox.currentIndex())
		obj_data = {"source":source_data, "source_path" : source_path,"animType":animType_data, "sequence":sequence_data, "shot":shot_data, "startEnd":startEnd_data, "path":shot_path}
		return obj_data


	def load_from_scene(self):
		self.ui.exListWidget.clear()
		animLayer = mc.ls(type='animLayer',ro=0)
		animLayer.remove('BaseAnimation')
		for layer in animLayer:
			metaData = pickle.loads(mc.getAttr(layer+'.taskData'))
			track_obj = self.add_widgets(layer,metaData)

	def custom_check(self):
		if not self.ui.customCheck.isChecked():
			self.ui.exAddButton.setDisabled(1)
			self.ui.exDelButton.setDisabled(1)
			self.ui.loadSceneButton.setDisabled(1)
		else:
			self.ui.exAddButton.setEnabled(1)
			self.ui.exDelButton.setEnabled(1)
			self.ui.loadSceneButton.setEnabled(1)	


class ComboBoxNoWheel(QtWidgets.QComboBox):
    def wheelEvent (self, event):
        event.ignore()

class track_item(QtWidgets.QFrame):
	def __init__(self,ui,animLayer=None):
		self.ui = ui
		super(track_item, self).__init__()
		self.setFrameShape(QtWidgets.QFrame.Box)
		self.setFrameShadow(QtWidgets.QFrame.Sunken)
		self.setLineWidth(2)
		self.setColor("lightGreen")

		widgetLayout = QtWidgets.QVBoxLayout()
		spacerItem = QtWidgets.QSpacerItem(15, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

		sourceLayout =  QtWidgets.QHBoxLayout()
		sourceLabel =  QtWidgets.QLabel("Source : ")
		self.sourceComboBox = ComboBoxNoWheel()
		sourceLayout.addWidget(sourceLabel)
		sourceLayout.addWidget(self.sourceComboBox)
		sourceLayout.setStretch(1,1)
		sourceLayout.setStretch(2,1)

		animLayout =  QtWidgets.QHBoxLayout()
		animLabel =  QtWidgets.QLabel("AnimType : ")
		self.animComboBox = ComboBoxNoWheel()
		animLayout.addWidget(animLabel)
		animLayout.addWidget(self.animComboBox)
		animLayout.setStretch(1,1)
		animLayout.setStretch(2,1)

		sqLayout =  QtWidgets.QHBoxLayout()
		self.sqLabel =  QtWidgets.QLabel("Episode : ")
		self.sqComboBox = ComboBoxNoWheel()
		sqLayout.addWidget(self.sqLabel)
		sqLayout.addWidget(self.sqComboBox)

		#shotLayout =  QtWidgets.QHBoxLayout()
		self.shotLabel =  QtWidgets.QLabel("Shot : ")
		self.shotComboBox = ComboBoxNoWheel()
		sqLayout.addWidget(self.shotLabel)
		sqLayout.addWidget(self.shotComboBox)
		
		sqLayout.setStretch(1,1)
		sqLayout.setStretch(2,0)
		sqLayout.setStretch(3,1)

		timeLayout = QtWidgets.QHBoxLayout()
		startLabel = QtWidgets.QLabel("Start : ")
		self.startLine = QtWidgets.QLineEdit()
		endLabel = QtWidgets.QLabel("End : ")
		self.endLine = QtWidgets.QLineEdit()
		timeLayout.addWidget(startLabel)
		timeLayout.addWidget(self.startLine)
		timeLayout.addWidget(endLabel)
		timeLayout.addWidget(self.endLine)

		# lockLayout = QtWidgets.QHBoxLayout()
		# self.autoFrame = QtWidgets.QCheckBox("Auto Fill")
		# lockLayout.addWidget(self.autoFrame)
		# self.lockWidget = QtWidgets.QCheckBox("Lock settting")
		# lockLayout.addWidget(self.lockWidget)
		# self.autoFrame.setChecked(1)

		widgetLayout.addLayout(sourceLayout)
		widgetLayout.addLayout(animLayout)
		widgetLayout.addLayout(sqLayout)
		#widgetLayout.addLayout(shotLayout)
		widgetLayout.addLayout(timeLayout)
		#widgetLayout.addLayout(lockLayout)
		#widgetLayout.addWidget(line)
		widgetLayout.addStretch()
		self.ui.projectWidget.projectComboBox.activated.connect(self.change_project)
		
		if animLayer == None:
			self.create_anim_layer()
		else:
			
			self.anim_layer = animLayer

		self.track_item_init()
		self.setLayout(widgetLayout)

	def track_item_init(self):
		#self.hikSource()
		self.mcpx_source()
		self.sqShot_active()
		self.animComboBox.activated.connect(self.sqShot_active)
		self.add_sequence()
		self.animComboBox.activated.connect(self.add_sequence)
		self.sqComboBox.activated.connect(self.add_shot)
		self.startLine.textChanged.connect(self.task_metaData)
		self.endLine.textChanged.connect(self.task_metaData)
		#self.lockWidget.toggled.connect(self.disableQFrame)
		self.createStratEnd()
		self.sourceComboBox.activated.connect(self.set_source)

	def mcpx_source(self):
		self.mcpx_name = self.ui.mocap_fileName_list
		self.mcpx_path = self.ui.mocap_filePath_list
		for idx,path in enumerate(self.mcpx_path):
			self.sourceComboBox.addItem(self.mcpx_name[idx])
			self.sourceComboBox.setItemData(idx,path,QtCore.Qt.UserRole)

		animCbb = ["shot", "asset"]
		self.animComboBox.addItems(animCbb)
	
	def sqShot_active(self):
		mode = self.animComboBox.currentText()
		if mode =='asset':
			self.hide_disable_widget(self.sqComboBox)
			self.hide_disable_widget(self.shotComboBox)
			self.hide_disable_widget(self.sqLabel)
			self.hide_disable_widget(self.shotLabel)
			self.sqComboBox.clear()
			self.shotComboBox.clear()

		elif mode =='shot':
			self.show_enable_widget(self.sqComboBox)
			self.show_enable_widget(self.shotComboBox)
			self.show_enable_widget(self.sqLabel)
			self.show_enable_widget(self.shotLabel)

		self.task_metaData()

	def hide_disable_widget(self,qtWidget):
		qtWidget.setDisabled(1)

	def show_enable_widget(self,qtWidget):
		qtWidget.setEnabled(1)

	def add_sequence(self):
		if self.animComboBox.currentText() == "asset":
			self.sqComboBox.clear()
		elif self.animComboBox.currentText() == "shot":
			self.sqComboBox.clear()
			get_proj = self.ui.projectWidget.projectComboBox.currentText()
			shot_context = context_info.Context()
			shot_context.update(project=get_proj , entityType='scene')
			scene = context_info.ContextPathInfo(context=shot_context)
			self.sq_list = fu.listFolder(scene.path.publish().abs_path())
			#print self.sq_list
			self.sqComboBox.addItems(self.sq_list)

		self.add_shot()
		self.task_metaData()

	def add_shot(self):
		if self.animComboBox.currentText() == "asset":
			self.shotComboBox.clear()
		elif self.animComboBox.currentText() == "shot":
			self.shotComboBox.clear()
			get_proj = self.ui.projectWidget.projectComboBox.currentText()
			get_sq = self.sqComboBox.currentText()
			
			sq_context = context_info.Context()
			sq_context.update(project=get_proj , entityType='scene',entityGrp = get_sq)
			get_sq = context_info.ContextPathInfo(context=sq_context)
			get_shot = fu.listFolder(get_sq.path.scheme(key='publishEpisodePath').abs_path())
			self.shotComboBox.addItems(get_shot)

			for idx,shot in enumerate(get_shot):
				get_sq.context.update(entity = shot)
				path = get_sq.path.scheme(key = 'publishShotPath').abs_path()
				#print path
				self.shotComboBox.setItemData(idx,path,QtCore.Qt.UserRole)

		self.task_metaData()

	def createStratEnd(self):
		proj = self.sourceComboBox.currentText()
		startF = mc.playbackOptions(q=1 , min=1)
		endF = mc.playbackOptions(q=1 , max =1)
		self.startLine.setText(str(startF))
		self.endLine.setText(str(endF))
		self.task_metaData()

	def change_project(self):
		self.add_sequence()

	def disableQFrame(self):
		is_check = self.lockWidget.isChecked()
		
		if is_check == True:
			self.sourceComboBox.setDisabled(1)
			self.animComboBox.setDisabled(1)
			self.sqComboBox.setDisabled(1)
			self.shotComboBox.setDisabled(1)
			self.startLine.setDisabled(1)
			self.endLine.setDisabled(1)
			
		elif is_check == False:
			mode = self.animComboBox.currentText()
			if mode =='asset':
				self.sourceComboBox.setEnabled(1)
				self.animComboBox.setEnabled(1)
				self.sqComboBox.setDisabled(1)
				self.sqComboBox.setDisabled(1)
				self.startLine.setEnabled(1)
				self.endLine.setEnabled(1)
			elif mode =='shot':
				self.sourceComboBox.setEnabled(1)
				self.animComboBox.setEnabled(1)
				self.sqComboBox.setEnabled(1)
				self.shotComboBox.setEnabled(1)
				self.startLine.setEnabled(1)
				self.endLine.setEnabled(1)

	def setColor(self, color):
		pal = self.palette()
		pal.setColor(QtGui.QPalette.Window, QtGui.QColor(color))
		self.setPalette(pal)

	def set_source(self):
		#source = sourceComboBox.currentText()
		source_path = self.sourceComboBox.currentData(QtCore.Qt.UserRole)
		print source_path
		#hikd.set_hik_source(self.ui.char_name, source)
		cmx.set_source('MocapX','ClipReader')

		cmx.set_clipReader_file('ClipReader',source_path)



	def create_anim_layer(self):
		self.anim_layer = mc.animLayer(aso =1 ,o=1,m=1)
		mc.select(d=1)

	def task_metaData(self):
		data = pickle.dumps(self.ui.setAnimData(self))
		if not mc.objExists(self.anim_layer+'.taskData'):
			mc.addAttr(self.anim_layer,ln='taskData', dt='string')
		mc.setAttr(self.anim_layer+'.taskData',data,type ='string')
		
	def str_sel_comboBox(self,comboBox,str_sel):
		idx = comboBox.findText(str_sel)
		print idx
		
		if idx != -1:
			comboBox.setCurrentIndex(idx)
			#comboBox.completer()
			return True
		else:
			return False

	@classmethod
	def load_metaData(cls,ui,anim_layer,meta_data):
		task_obj = cls(ui,anim_layer)
		print meta_data
		task_obj.str_sel_comboBox(task_obj.sourceComboBox,meta_data['source'])
		task_obj.str_sel_comboBox(task_obj.animComboBox,meta_data['animType'])
		task_obj.sqShot_active()
		if meta_data['animType'] == 'shot': 
			task_obj.add_sequence()
			task_obj.str_sel_comboBox(task_obj.sqComboBox,meta_data['sequence'])
			task_obj.add_shot()
			task_obj.str_sel_comboBox(task_obj.shotComboBox,meta_data['shot'])

		task_obj.startLine.setText(meta_data['startEnd'][0])
		task_obj.endLine.setText(meta_data['startEnd'][1])

		return task_obj



def show(mode='default'):
	if config.isMaya:
		from rftool.utils.ui import maya_win
		logger.info('Run in Maya\n')
		maya_win.deleteUI(uiName)
		myApp = Bake(maya_win.getMayaWindow())
		myApp.show()
		return myApp

if __name__ == '__main__':
	show()

def add_attr(obj,attr_name = '' ,**kwargs):
	obj = pmc.PyNode(obj)
	if not pmc.objExists('{}.{}'.format(obj.name,attr_name)):
		obj.addAttr(attr_name,**kwargs)

def add_lock_attr(obj,attr_name = '',**kwargs):
	obj = pmc.PyNode(obj)
	if not pmc.objExists('{}.{}'.format(obj.name,attr_name)):
		add_attr(obj,attr_name,k=1,dv=0)
		obj.attr(attr_name).lock()