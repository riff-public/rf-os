# v.0.0.1 polytag switcher
_title = 'Bake App'
_version = 'v.0.0.1'
_des = 'Wip'
uiName = 'BakeUI'

#Import python modules
import sys
import os
import logging
import getpass
import traceback
from collections import OrderedDict
import pymel.core as pmc
import os
import json
import pickle

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
import rf_config as config
# framework modules
from rf_utils import log_utils
from rf_utils.ui import load
reload(load)
from rf_utils.ui import stylesheet

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui
from rf_utils.widget import project_widget
from rf_utils.sg import sg_utils
from rf_utils import file_utils as fu
from rf_utils.context import context_info
from rf_utils.pipeline import file_info 
reload(file_info)

# maya & module function 
import pymel.core as pmc
import maya.cmds as mc
import maya.mel as mel

import sourceHik as hik
reload(hik)

class Bake(QtWidgets.QMainWindow):
	def __init__(self, parent=None):
		#Setup Window
		super(Bake, self).__init__(parent)

		# ui read
		uiFile = '%s/bake_app.ui' % moduleDir
		self.ui = load.setup_ui_maya(uiFile, parent)
		# set size 
		self.w = 500
		self.h = 600 

		self.setCentralWidget(self.ui)
		self.setObjectName(uiName)
		self.resize(self.w, self.h)
		self.setWindowTitle('%s %s %s' % (_title, _version, _des))
		self.ui.exListWidget.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
	   
		self.asset_list = ['']
		self.action_folder_list = []
		self.projectWidget = project_widget.SGProjectComboBox(sg=sg_utils.sg, layout=QtWidgets.QHBoxLayout())
		self.projectWidget.setContentsMargins(0, 0, 3, 0)
		self.projectWidget.label.setText('Project : ')
		# #spacerItem = QtWidgets.QSpacerItem(10, 10, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
		# #self.projectLayout.addItem(spacerItem)
		# self.projectWidget.projectComboBox.setCurrentText(self.mocap_char_project)
		# self.ui.asset_name.setCurrentText(self.mocap_char_asset)
		self.init_signal()
		self.init_import_function()

#===========================================================================================================#
	#import function
	def init_import_function(self):
		self.proj_list_cbb()
		#self.add_button()
		self.date_list_cbb()
		self.obj_listWidget()
		
		self.ui.imListWidget.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
		self.ui.importButton.clicked.connect(self.import_hik)
		
		
	def proj_list_cbb(self):
		self.ui.imProjComboBox.clear()
		self.proj_ls = os.listdir(r"P:\Library\Mocap")
		self.ui.imProjComboBox.addItems(self.proj_ls)

	def change_dateList(self, *args):
		self.ui.imDateComboBox.clear()
		get_proj_list =  self.ui.imProjComboBox.currentText()
		get_date = os.listdir(r"P:\Library\Mocap\%s"%get_proj_list)
		self.ui.imDateComboBox.addItems(get_date)

	def date_list_cbb(self):
		self.change_dateList()
		self.ui.imProjComboBox.currentTextChanged.connect(self.change_dateList)

	def change_animList(self, *args):
		self.ui.imListWidget.clear()
		get_proj_list =  self.ui.imProjComboBox.currentText()
		get_date_list = self.ui.imDateComboBox.currentText()
		self.date_path = r"P:\Library\Mocap\%s\%s"%(get_proj_list, get_date_list)
		get_anim = os.listdir(self.date_path)

		self.all_anim_data = []
		for path in get_anim:
			if '.ma' in path or '.mb' in path:
				item = QtWidgets.QListWidgetItem(self.ui.imListWidget)
				item.setText(path)
				item.setData(QtCore.Qt.UserRole, os.path.join(self.date_path,path).replace('\\','/'))
				
				self.all_anim_data.append(os.path.join(self.date_path,path).replace('\\','/'))

	def obj_listWidget(self):
		self.change_animList()
		self.ui.imDateComboBox.currentTextChanged.connect(self.change_animList)            

	def import_hik(self,*args):
		item_path_list = self.ui.imListWidget.selectedItems()   
		for item in item_path_list:
			source_file = item.data(QtCore.Qt.UserRole)
			if '\\' in source_file:
				file_name = source_file.split('\\')[-1].split('.')[0]
			elif '/' in source_file:
				file_name = source_file.split('/')[-1].split('.')[0]

			self.ref_ns = str(file_name)
			
			for letter in [' ','-','+','*']:
				if letter in self.ref_ns:
					self.ref_ns = self.ref_ns.replace(letter,'_')

			mc.file(source_file, ns=self.ref_ns, r=1) 

#==============================================================================================================#
	
	# export function   

	def init_signal(self):
		self.ui.horizontalLayout_11.addWidget(self.projectWidget, 0, 1)
		
		self.ui.exAddButton.clicked.connect(self.add_widgets)
		self.ui.exDelButton.clicked.connect(self.del_widgets)

		self.projectWidget.projectComboBox.activated.connect(self.get_project)
		self.get_project()

	def add_widgets(self):
		item = QtWidgets.QListWidgetItem(self.ui.exListWidget)
		track_obj = track_item()
		self.ui.exListWidget.addItem(item)
		self.ui.exListWidget.setItemWidget(item,track_obj.widget)
		item.setSizeHint(track_obj.widget.sizeHint())

	def del_widgets(self):
		#self.ui.exListWidget.takeItem()
		#print self.ui.exListWidget.currentRow()
		widgetItems = self.ui.exListWidget.selectedItems()
		for item in widgetItems:
			idx = self.ui.exListWidget.row(item)
			self.ui.exListWidget.takeItem(idx)

	def get_project(self):
		# change self.project from project from comboBox
		self.project = self.projectWidget.projectComboBox.currentText()
		# after change project it will change asset in combo box
		asset_list = fu.listFolder(r"P:/%s/asset/publ/char/"%(self.project))
		self.ui.exAssetComboBox.clear()
		self.ui.exAssetComboBox.addItems(asset_list)

	def bake_character(self):
		pass

	def auto_create_task(self):
		hik_node = hik.sourceHikNode()
		for name, frame in hik_node.iteritems():
			item = QtWidgets.QListWidgetItem(self.ui.exListWidget)
			box = track_item()
			box.sourceComboBox.setCurrentText(name)
			box.startLine.setText(str(frame[0]))
			box.endLine.setText(str(frame[1]))
			self.ui.exListWidget.addItem(item)
			self.ui.exListWidget.setItemWidget(item,box.widget)
			item.setSizeHint(box.widget.sizeHint())

class track_item():
	def __init__(self):
		#super(track_item,self).__init__()
		self.widget = QtWidgets.QFrame()
		self.widget.setFrameShape(QtWidgets.QFrame.Box)
		self.widget.setFrameShadow(QtWidgets.QFrame.Sunken)
		self.widget.setLineWidth(2)
		#self.widget.setColor(QColor.darkGray)
		widgetLayout = QtWidgets.QVBoxLayout()
		#line = QtWidgets.QFrame()
		#line.setFrameShape(QtWidgets.QFrame.HLine)
		#line.setFrameShadow(QtWidgets.QFrame.Sunken)
		spacerItem = QtWidgets.QSpacerItem(15, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

		sourceLayout =  QtWidgets.QHBoxLayout()
		sourceLabel =  QtWidgets.QLabel("Source : ")
		self.sourceComboBox = QtWidgets.QComboBox()
		sourceLayout.addWidget(sourceLabel)
		sourceLayout.addWidget(self.sourceComboBox)
		#sourceLayout.addItem(spacerItem)
		sourceLayout.setStretch(1,1)
		sourceLayout.setStretch(2,1)

		animLayout =  QtWidgets.QHBoxLayout()
		animLabel =  QtWidgets.QLabel("AnimType : ")
		self.animComboBox = QtWidgets.QComboBox()
		animLayout.addWidget(animLabel)
		animLayout.addWidget(self.animComboBox)
		# animLayout.addItem(spacerItem)
		animLayout.setStretch(1,1)
		animLayout.setStretch(2,1)

		shotLayout =  QtWidgets.QHBoxLayout()
		shotLabel =  QtWidgets.QLabel("Shot : ")
		self.shotComboBox = QtWidgets.QComboBox()
		shotLayout.addWidget(shotLabel)
		shotLayout.addWidget(self.shotComboBox)
		# shotLayout.addItem(spacerItem)
		shotLayout.setStretch(1,1)
		shotLayout.setStretch(2,1)

		timeLayout = QtWidgets.QHBoxLayout()
		startLabel = QtWidgets.QLabel("Start : ")
		self.startLine = QtWidgets.QLineEdit()
		endLabel = QtWidgets.QLabel("End : ")
		self.endLine = QtWidgets.QLineEdit()
		timeLayout.addWidget(startLabel)
		timeLayout.addWidget(self.startLine)
		timeLayout.addWidget(endLabel)
		timeLayout.addWidget(self.endLine)

		widgetLayout.addLayout(sourceLayout)
		widgetLayout.addLayout(animLayout)
		widgetLayout.addLayout(shotLayout)
		widgetLayout.addLayout(timeLayout)
		#widgetLayout.addWidget(line)
		widgetLayout.addStretch()

		self.widget.setLayout(widgetLayout)
		#self.inputSource()
		self.hikSource()
	
	def hikSource(self):
		hik_node = hik.sourceHikNode()
		animCbb = ["asset", "shot"]
		
		get_hik = []
		for name in hik_node.iterkeys():
			get_hik.append(name)
			
		self.sourceComboBox.addItems(get_hik)
		
		self.animComboBox.addItems(animCbb)
	
	def test(self):
		print 'test success'

def show(mode='default'):
	if config.isMaya:
		from rftool.utils.ui import maya_win
		logger.info('Run in Maya\n')
		maya_win.deleteUI(uiName)
		myApp = Bake(maya_win.getMayaWindow())
		myApp.show()
		return myApp


if __name__ == '__main__':
	show()
