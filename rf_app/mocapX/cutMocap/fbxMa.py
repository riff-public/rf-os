import sys
import maya.standalone
maya.standalone.initialize()

file_path = sys.argv[1]
destination = sys.argv[2]

print 'action_path_list: %s'%file_path

print 'destination: %s'%destination

import maya.cmds as mc
import maya.mel as mel
import pymel.core as pmc
import maya.OpenMaya as om

import rf_app.mocap.function.fbxDef as fbxDef
reload(fbxDef)

import rf_app.mocap.function.mocapData.ioData as ioData
reload(ioData)

if not mc.pluginInfo('mayaHIK',loaded=1,q=1):
	mc.loadPlugin('mayaHIK')

if not mc.pluginInfo('fbxmaya',loaded=1,q=1):
	mc.loadPlugin('fbxmaya')


def batch_ma_file(path_file, destination):
	#fbxDef.create_action_folder(destination ,action_path)
	fbxDef.publish_fbx_def(path_file, location = destination)

batch_ma_file(file_path,destination)