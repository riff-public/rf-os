import os
import logging
import getpass
import traceback
from collections import OrderedDict
import pymel.core as pmc
import os
import json
import pickle
import math
import sys
import time
moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]
# v.0.0.1 polytag switcher
_title = ''
_version = 'v.0.0.1'
_des = 'Wip'
uiName = 'record'

# import config
import rf_config as config
# framework modules
from rf_utils import log_utils
from rf_utils.ui import load
reload(load)
from rf_utils.ui import stylesheet

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui
from rf_utils.widget import project_widget
from rf_utils.sg import sg_utils
from rf_utils import file_utils as fu
from rf_utils.context import context_info
from rf_utils.pipeline import file_info 
reload(file_info)

# maya & module function 
import pymel.core as pmc
import maya.cmds as mc
import maya.mel as mel

from rf_maya.mocapx import commands
from rf_maya.mocapx.lib import nodes
from rf_maya.rftool.rig.MocapX import customMocapX as cmpx
reload(cmpx)

class RecordUI(QtWidgets.QMainWindow):
	def __init__(self, parent=None):
		#Setup Window
		super(RecordUI, self).__init__(parent)

		# ui read
		uiFile = '%s/record.ui' % moduleDir
		self.ui = load.setup_ui_maya(uiFile, parent)
		# set size 
		self.w = 500
		self.h = 350
		
		# time variable
		self.start = 0
		self.end = 0
		self.rec_time = 0
		self.record = 0
		
		# character data
		self.ns = ''
		self.mocapX_node = ''
		self.realtime_device = ''
		self.default_clip = ''
		
		self.setCentralWidget(self.ui)
		self.setObjectName(uiName)
		self.resize(self.w, self.h)
		self.setWindowTitle('%s %s %s' % (_title, _version, _des))
		self.init_function()
		self.ui.record_button.setStyleSheet("background-color: rgb(65,150,65)")
		self.ui.character_lineEdit.setStyleSheet("background-color: rgb(200,65,65)")
   	
	# data function
	def start_rec(self):
		self.start = time.time()
		self.record = 1
		print 'start : recording'
		cmpx.set_source(self.mocapX_node,self.realtime_device)
		nodes.set_realtime_device_conn_type(self.realtime_device,1,1)
		commands.establish_connection(self.realtime_device)
		commands.start_session(self.realtime_device)
		
	def end_rec(self):
		commands.break_connection(self.realtime_device)
		self.end = time.time()
		self.record=0
		self.rec_time = math.ceil(self.end-self.start)
		nodes.clipreader_save_clip(self.realtime_device,self.output_path,self.rec_time,self.output_path)
		print 'stop : recording'
		print self.rec_time
		

	def init_function(self):
		self.proj_list_cbb()
		self.date_list_cbb()
		self.add_sequence()
		self.add_shot()
		self.ui.animComboBox.activated.connect(self.sqShot_active)
		self.ui.sqComboBox.activated.connect(self.add_shot)
		self.ui.record_button.clicked.connect(self.recordMocapX)
		self.ui.get_button.clicked.connect(self.get_character)


	def proj_list_cbb(self):
		self.ui.proj_list.clear()
		self.proj_ls = os.listdir(r"P:\Library\Mocap")
		self.ui.proj_list.addItems(self.proj_ls)

	def date_list_cbb(self):
		self.change_dateList()
		self.ui.proj_list.currentTextChanged.connect(self.change_dateList)

	def change_dateList(self, *args):
		self.ui.date_list.clear()
		get_proj_list =  self.ui.proj_list.currentText()
		get_date = os.listdir(r"P:\Library\Mocap\%s"%get_proj_list)
		self.ui.date_list.addItems(get_date)


	def sqShot_active(self):
		mode = self.ui.animComboBox.currentText()
		if mode =='asset':
			self.ui.sqComboBox.clear()
			self.ui.shotComboBox.clear()
			self.hide_disable_widget(self.ui.sqComboBox)
			self.hide_disable_widget(self.ui.shotComboBox)
			self.hide_disable_widget(self.ui.sqLabel)
			self.hide_disable_widget(self.ui.shotLabel)

		elif mode =='shot':
			self.show_enable_widget(self.ui.sqComboBox)
			self.show_enable_widget(self.ui.shotComboBox)
			self.show_enable_widget(self.ui.sqLabel)
			self.show_enable_widget(self.ui.shotLabel)
			self.add_sequence()
			self.add_shot()

	def hide_disable_widget(self,qtWidget):
		qtWidget.setDisabled(1)

	def show_enable_widget(self,qtWidget):
		qtWidget.setEnabled(1)

	def add_sequence(self):
		if self.ui.animComboBox.currentText() == "asset":
			self.ui.sqComboBox.clear()
		elif self.ui.animComboBox.currentText() == "shot":
			self.ui.sqComboBox.clear()
			get_proj = self.ui.proj_list.currentText()
			get_sq = fu.listFolder(r"P:/{}/scene/publ/".format(get_proj))
			self.ui.sqComboBox.addItems(get_sq)
		self.add_shot()

	def add_shot(self):
		if self.ui.animComboBox.currentText() == "asset":
			self.ui.shotComboBox.clear()
		elif self.ui.animComboBox.currentText() == "shot":
			self.ui.shotComboBox.clear()
			get_proj = self.ui.proj_list.currentText()
			get_sq = self.ui.sqComboBox.currentText()
			get_shot = fu.listFolder(r"P:/{}/scene/publ/{}/".format(get_proj, get_sq))
			self.ui.shotComboBox.addItems(get_shot)

			get_path = []
			for idx,shot in enumerate(get_shot):
				path = r"P:/{}/scene/publ/{}/{}".format(get_proj, get_sq, shot)
				self.ui.shotComboBox.setItemData(idx,path,QtCore.Qt.UserRole)

	def recordMocapX(self):
		self.create_path()
		if not self.record:
			do_it = 1
			if os.path.isfile(self.output_path):
				confirm = mc.confirmDialog(m='This file is already exists\nDo you want to override it?',b= ['yes','no'])
				if confirm =='yes':
					pass
				elif confirm == 'no':
					do_it =0
			if do_it:
				self.ui.record_button.setStyleSheet("background-color: rgb(200,65,65)")
				self.ui.record_button.setText('Recording...')
				self.start_rec()

		else:
			self.ui.record_button.setStyleSheet("background-color: rgb(65,150,65)")
			self.ui.record_button.setText('Record')
			print self.output_path
			self.end_rec()

	def get_character(self):
		sel = pmc.selected()
		if sel:
			self.ns = sel[0].namespace()
			self.ui.character_lineEdit.setText(self.ns) 
			self.ui.character_lineEdit.setStyleSheet("background-color: rgb(65,150,65)")
			self.mocapX_node = self.ns +'MocapX'
			self.realtime_device = self.ns + 'RealtimeDevice' 
			self.default_clip = self.ns + 'Default'
			self.clip_reader = self.ns+'ClipReader'


	def create_path(self):
		project = self.ui.proj_list.currentText()
		date = self.ui.date_list.currentText()
		shot = self.ui.shotComboBox.currentText()
		des = self.ui.lineEdit.text()
		
		if shot != '' and des != '':
			shot = shot+'_'

		file_name = '{}{}.mcpx'.format(shot,des)
		self.folder_path = os.path.join('P:\\Library\\Mocap',project,date,'mcpx')

		if not os.path.isdir(self.folder_path):
			os.mkdir(self.folder_path)

		self.output_path = os.path.join(self.folder_path,file_name)
		print self.output_path

def show(mode='default'):
	if config.isMaya:
		from rftool.utils.ui import maya_win
		logger.info('Run in Maya\n')
		maya_win.deleteUI(uiName)
		myApp = RecordUI(maya_win.getMayaWindow())
		myApp.show()
		return myApp


if __name__ == '__main__':
	show()
