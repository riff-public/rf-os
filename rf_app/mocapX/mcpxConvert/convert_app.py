# v.0.0.1 polytag switcher
_title = 'MocapX App'
_version = 'v.0.0.1'
_des = 'Wip'
uiName = 'MocapX Copy'

#Import python modules
import sys
import os
import logging
import getpass
import traceback
from collections import OrderedDict
import pymel.core as pmc
import os
import json
import pickle
import subprocess
import re

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
import rf_config as config
reload(config)
# framework modules
from rf_utils import log_utils
reload(log_utils)
from rf_utils.ui import load
reload(load)
from rf_utils.ui import stylesheet
reload(stylesheet)

import shutil

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui
from rf_utils.widget import project_widget
reload(project_widget)
from rf_utils.sg import sg_utils
reload(sg_utils)
from rf_utils import file_utils as fu
reload(fu)
from rf_utils.context import context_info
reload(context_info)
from rf_utils.pipeline import file_info 
reload(file_info)
from rf_utils.pipeline import create_asset
reload(create_asset)
from rf_utils import admin
reload(admin)
from rf_utils.sg import sg_process
reload(sg_process)
# maya & module function 
import pymel.core as pmc
import maya.cmds as mc
import maya.mel as mel

def create_folder(path):
    if '\\' in path:
        path = path.replace('\\','/')
    print 'path: %s'%path

    for level in range(len(path.split('/'))):
        print level
        path_check = '/'.join(path.split('/')[:level+1]).replace('\\','/')
        print path_check
        if not os.path.isdir(path_check):
            print 'print create folder'
            os.mkdir(path_check)

    return path_check

class AddWindow(QtWidgets.QDialog):

    def __init__(self, parent=None, convertUi = ''):
        super(AddWindow, self).__init__(parent)

        self.input_dir = QtWidgets.QLineEdit()
        self.okButton = QtWidgets.QPushButton("OK")
        self.convertUi = convertUi
        addLayout = QtWidgets.QHBoxLayout()
        addLayout.addWidget(self.input_dir)
        addLayout.addWidget(self.okButton)

        self.resize(250, 50)
        self.setLayout(addLayout)
        
        self.okButton.clicked.connect(self.closeAddWindow)
        
    def closeAddWindow(self):
        dir_name = self.input_dir.text()
        if dir_name:
            os.mkdir(r"P:\Library\MocapX\%s"%dir_name)
            self.convertUi.proj_list_cbb()
            self.close()

class ConvertFile(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(ConvertFile, self).__init__(parent)

        # ui read
        uiFile = '%s/convert_file.ui' % moduleDir
        self.ui = load.setup_ui_maya(uiFile, parent)
        # set size 
        self.w = 500
        self.h = 600 

        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.resize(self.w, self.h)
        self.setWindowTitle('%s %s %s' % (_title, _version, _des))

        self.asset_list = ['']
        self.action_folder_list = []

        self.addWindow = AddWindow(None ,  convertUi = self)
        self.init_function()

    def init_function(self):
        self.proj_list_cbb()
        #self.add_button()
        self.date_list_cbb()
        self.obj_listWidget()
        #self.bakeSel_button()
        #self.bakeAll_button()
        
        self.ui.anim_listBox.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
        #self.ui.anim_listBox.currentItemChanged.connect(self.print_value)
        self.ui.bakeAll_butt.clicked.connect(self.bakeAll_button)
        self.ui.bakeSel_butt.clicked.connect(self.bakeSel_button)
        

    def proj_list_cbb(self):
        self.ui.proj_list.clear()
        self.proj_ls = os.listdir(r"P:\Library\MocapX")
        self.ui.proj_list.addItems(self.proj_ls)
    #-------------------------------------------------

    def add_button(self):
        self.ui.addButton.clicked.connect(self.showAddWindow)
        
    def showAddWindow(self, *args):
        self.addWindow.show()
    #-------------------------------------------------

    def date_list_cbb(self):
        self.change_dateList()
        self.ui.proj_list.currentTextChanged.connect(self.change_dateList)

    def change_dateList(self, *args):
        self.ui.date_list.clear()
        get_proj_list =  self.ui.proj_list.currentText()
        self.project = get_proj_list
        get_date = os.listdir(r"P:\Library\MocapX\%s"%get_proj_list)
        self.ui.date_list.addItems(get_date)
        self.sg_shot = sg_process.get_all_shots(get_proj_list)
    #--------------------------------------------------------------

    def obj_listWidget(self):
        self.change_animList()
        self.ui.date_list.currentTextChanged.connect(self.change_animList)

    def change_animList(self, *args):
        self.ui.anim_listBox.clear()
        get_proj_list =  self.ui.proj_list.currentText()
        get_date_list = self.ui.date_list.currentText()
        self.date_path = r"P:\Library\MocapX\%s\%s"%(get_proj_list, get_date_list)
        
        try:
            get_anim = fu.listFile(self.date_path)
            #print self.date_path
            #print get_anim
            self.all_anim_data = []
            for path in get_anim:
                if 'mcpx' in path.split('.')[-1]:
                    item = QtWidgets.QListWidgetItem(self.ui.anim_listBox)
                    item.setText(path)
                    item.setData(QtCore.Qt.UserRole, os.path.join(self.date_path,path).replace('\\','/'))
                    
                    self.all_anim_data.append(os.path.join(self.date_path,path).replace('\\','/'))

        except:
            self.all_anim_data = []



    #----------------------------------------------------------------------------------

    def bakeSel_button(self):
        item_list = self.ui.anim_listBox.selectedItems()
        path_list = []
        for item in item_list:
            path_list.append(item.data(QtCore.Qt.UserRole))
        self.bake_mcpx(path_list)
        mc.confirmDialog(m='bake process complete')
        
    
    def bakeAll_button(self):
        #self.all_anim_data
        self.bake_mcpx(self.all_anim_data)
        mc.confirmDialog(m='bake process complete')

    def bake_mcpx(self,path_list):
        sc_context = context_info.Context()
        sc_context.update(project = self.project, entityType = 'scene')             
        scene_context = context_info.ContextPathInfo(context=sc_context)

        as_context = create_asset.get_context(self.project,'extra','char','all')
        asset_context = context_info.ContextPathInfo(context=as_context)
        asset_context.context.update(step ='mocapX')

        non_found = []
        for mcpx_path in path_list:
            splitList = mcpx_path.split('/')
            #print splitList
            mcpx_file = splitList[-1]
            file_split = mcpx_file.split('_')
            raw_shot_list = []

            if len(file_split) >= 4:
                proj = file_split[0]
                ep = file_split[1]
                sq = file_split[2]
                asset = file_split[-2]
                ver = file_split[-1]
                shot_list = file_split[3:-2]
           
                for shot in shot_list:
                    for shots in self.sg_shot:
                        if re.search("{}_{}_{}_{}".format(proj,ep,sq,shot),shots['code']):

                            shot_name = shots["code"]
                            scene_context.context.update(entity = shot_name)
                            scene_context.update_scene_context()
                            shot_path =  scene_context.path.scheme(key='publishShotPath').abs_path()
                            raw_shot_path = os.path.join(shot_path,'mocapX','raw')
                            raw_file_name = '{}_{}_{}'.format(shot_name,asset,ver)
                            print '{} : {}'.format(raw_shot_path,raw_file_name)
                            raw_shot_list.append([raw_shot_path,raw_file_name])
                            if splitList[-1] in non_found:
                                non_found.remove(splitList[-1])
                            break

                        elif re.search("{}_{}_{}[a-z]_{}".format(proj,ep,sq,shot),shots['code']):
                            shot_name = shots["code"]
                            scene_context.context.update(entity = shot_name)
                            scene_context.update_scene_context()
                            shot_path =  scene_context.path.scheme(key='publishShotPath').abs_path()
                            raw_shot_path = os.path.join(shot_path,'mocapX','raw')
                            raw_file_name = '{}_{}_{}'.format(shot_name,asset,ver)
                            print '{} : {}'.format(raw_shot_path, raw_file_name)
                            raw_shot_list.append([raw_shot_path, raw_file_name])

                            if splitList[-1] in non_found:
                                non_found.remove(splitList[-1])
                            break

                        else:
                            if splitList[-1] not in non_found:
                                non_found.append(splitList[-1])
                            continue

            else:
                asset_name = sg_process.get_one_asset(self.project,file_split[0])
                if asset_name:
                    asset_name = asset_name['code']
                else:
                    asset_name = 'extra'
            
                #task_context.use_sg(sg_utils.sg, self.project, 'asset', entityName = asset_name)
                asset_context.context.update(entity =asset_name)
                asset_publ_path = asset_context.path.scheme(key='publishStepPath').abs_path()
                raw_shot_path = os.path.join(asset_publ_path,'raw')
                raw_shot_list.append([raw_shot_path, None])
            
            for raw_shot_path in raw_shot_list:
                if not os.path.isdir(raw_shot_path[0]):
                    #print 'create_folder at {}'.format(raw_shot_path[0])
                    admin.makedirs(raw_shot_path[0])
                else:
                    print raw_shot_path

                if raw_shot_path[1]!=None:
                    des_path = os.path.join(raw_shot_path[0],raw_shot_path[1])
                else:
                    des_path = os.path.join(raw_shot_path[0],mcpx_file)
                #print des_path

                #copy file to that path

                print '{}:\n{}'.format(mcpx_path,des_path)
                #shutil.copy2(mcpx_path,des_path)
                admin.copyfile(mcpx_path,des_path)


    def check_extra_folder(self,*args):
        extra_asset = create_asset.get_context(self.project,'extra','char','all')
        asset_context = context_info.ContextPathInfo(context = extra_asset)
        asset_context.context.update(step ='mocapX')

        self.extra_asset_work = asset_context.path.scheme(key='workAssetNamePath').abs_path()
        self.extra_asset_path = asset_context.path.scheme(key='publishStepPath').abs_path()
        self.extra_asset_data_path = os.path.join(self.extra_asset_path,'data','raw').replace('\\','/')

        if os.path.isdir(self.extra_asset_work):
            return True
        else:
            create_asset.create(str(self.project), 'extra', 'char', assetSubtype='main', mainAsset='', tags='', template='default', setEntity=[], link=True) 
            admin.makedirs(self.extra_asset_data_path)
                 

    def print_value(self):
        #idx = self.ui.anim_listBox.currentIndex()
        #print 'idx : %s'%(idx)
        #value = self.ui.anim_listBox.itemData(idx,QtCore.Qt.UserRole)
        item = self.ui.anim_listBox.currentItem()
        data = item.data(QtCore.Qt.UserRole)
   

def show(mode='default'):
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = ConvertFile(maya_win.getMayaWindow())
        myApp.show()
        return myApp


if __name__ == '__main__':
    show()
