# _title = 'Riff Scene Collector Scm'
# _version = 'v.0.2.0'
# _des = 'wip'
# uiName = 'RFSceneCollectorUI'

#Import python modules
import sys
import os
import getpass
from collections import OrderedDict
import json

import argparse

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# config file
config_file = '%s/local_config.json' % moduleDir
with open(config_file) as configData:
    localConfig = json.load(configData)

SCRIPT = localConfig['SCRIPT']
ROOT = os.environ.get(SCRIPT)
core = localConfig['CORE']

# import config
sys.path.append('%s/%s' % (ROOT, core))
import rf_config as config


from rf_utils import file_utils
# framework modules

from rf_app.sceneCollector_scm import utils
reload(utils)
# from rf_app.sceneCollector_scm import get_asset
# reload(get_asset)
from rf_utils import log_utils

# user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
# logFile = log_utils.name(uiName, user)
# logger = log_utils.init_logger(logFile)
# from rf_utils import strip_abc_namespace_collector

def setup_parser(title):
    # create the parser
    parser = argparse.ArgumentParser(description=title, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-p', dest='path', type=str, help='The path to the source scene to be pack', required=True)
    parser.add_argument('-d', dest='destination', type=str, help='The destination path to the keep scene to be pack', required=True)
    parser.add_argument('-m', dest='mode', type=str, help='mode of packing asset/scene', required=True)
    parser.add_argument('-k', dest='keepPath', type=str, help='keep path is mode of packing file to keep old structure', required=True)
    parser.add_argument('-s', dest='splitShot', type=str, help='split shot is mode of split structure by shot', required=True)
    parser.add_argument('-st', dest='step', type=str, help='The step of source scene to be pack', required=False)
    #parser.add_argument('-p', dest='process', nargs='+', help='Processes to be cached', default=[])
    return parser

def pack_asset_batch(allItems, dst, mode, filePath, keepPath, splitShot):
    """ copy to dst dir """
    from rf_utils import strip_abc_namespace_collector
    tempDict = dict()

    moduleDir = 'O:/Pipeline/core/rf_app/sceneCollector_scm'

    assetKey = 'asset'
    sceneKey = 'scene'
    searchKey = [assetKey, sceneKey]
    copyRootPath = '%s/copy_root.py' % moduleDir

    if keepPath == 'True':
        listItem = []
        for item in allItems:
            shotName = filePath.split('/')[5]
            if splitShot == 'True':
                shotName = filePath.split('/')[5]
                dstPath = item.replace(item.split('/')[0], dst+'/%s' % shotName)
            else:
                dstPath = item.replace(item.split('/')[0], dst)
            print dstPath
            file_utils.copy(item, dstPath)
            print 'Copy %s -> %s' % (item, dstPath)
            try :
                if '.abc' in dstPath:
                    abcPath = [r'%s'%str(dstPath)]
                    strip_abc_namespace_collector.main(abcPath)
            except Exception as e:
                print e

            if not item in listItem:
                listItem.append(item)

        # create where.json      

        if os.path.exists(dst):
            keyDict = tempDict
            jsonPath = '%s/where.json'%dst
            # append where.json
            if os.path.exists(jsonPath):
                with open(jsonPath,"r") as file:
                    dictData = json.load(file)
                keyDict.update(dictData)
            if shotName in keyDict:
                WlistItem = keyDict[shotName]
                for Witem in WlistItem:
                    if not Witem in listItem:
                        listItem.append(Witem)

            keyDict[shotName] = listItem

            with open('%s/%s' % (dst, 'where.json'),"w") as file:
                json.dump(keyDict, file)
            print 'Create where.json --> %s' % dst

    else:
        if mode == 'scene':
            shotName = filePath.split('/')[5]
            for item in allItems:
                nrmPath = os.path.normpath(item)
                assetName = nrmPath.split(os.sep)
                if len(assetName) < 6:
                    continue
                assetName = assetName[5]
                key = sceneKey
                if splitShot == 'True':
                    newDst = '%s/%s/%s/%s' % (dst, shotName, key, shotName)
                else:
                    newDst = '%s/%s/%s' % (dst, key, shotName)
                # newDst = '%s/%s' % (dst, key)
                base = os.path.basename(item)
                dstPath = '%s/%s' % (newDst, base)
                tempDict[base] = os.path.splitdrive(item)[-1]

                # copy
                try :
                    file_utils.copy(item, dstPath)
                    print 'Copy %s -> %s' % (item, dstPath)
                    if '.abc' in dstPath:
                        abcPath = [r'%s'%str(dstPath)]
                        strip_abc_namespace_collector.main(abcPath)
                    # logger.info('Copy %s -> %s' % (item, dstPath))

                except Exception as e:
                    print e

                # create where.json        
                if os.path.exists(newDst):
                    keyDict = tempDict
                    jsonPath = '%s/where.json'%newDst
                    # append where.json
                    if os.path.exists(jsonPath):
                        with open(jsonPath,"r") as file:
                            dictData = json.load(file)
                        keyDict.update(dictData)
                    with open('%s/%s' % (newDst, 'where.json'),"w") as file:
                        json.dump(keyDict, file)
                    #print 'Create where.json --> %s' % newDst

            copyRootDst = '%s/%s' % (dst, 'copy_root.py')
            file_utils.copy(copyRootPath, copyRootDst)
            print 'Copy copy_root.py %s --> %s' % (copyRootPath,copyRootDst)

        else:
            if os.path.exists(dst):
                for item in allItems:
                    dstPath = '%s%s' % (dst, os.path.splitdrive(item)[-1])
                    
                    nrmPath = os.path.normpath(item)
                    assetName = nrmPath.split(os.sep)
                    if len(assetName) < 6:
                        continue
                    assetName = assetName[5]
                    for key in searchKey:
                        if key in item:
                            newDst = '%s/%s/%s' % (dst, key, assetName)
                            # newDst = '%s/%s' % (dst, key)
                            base = os.path.basename(item)
                            dstPath = '%s/%s' % (newDst, base)
                            tempDict[base] = os.path.splitdrive(item)[-1]

                        else:
                            key = assetKey
                            newDst = '%s/%s/%s' % (dst, key, assetName)
                            # newDst = '%s/%s' % (dst, key)
                            base = os.path.basename(item)
                            dstPath = '%s/%s' % (newDst, base)
                            tempDict[base] = os.path.splitdrive(item)[-1]
                                   
                    # copy
                    try :
                        file_utils.copy(item, dstPath)
                        print 'Copy %s -> %s' % (item, dstPath)
                        # logger.info('Copy %s -> %s' % (item, dstPath))

                    except Exception as e:
                        print e
             
                    if os.path.exists(newDst):
                        keyDict = tempDict
                        jsonPath = '%s/where.json'%newDst
                        # append where.json
                        if os.path.exists(jsonPath):
                            with open(jsonPath,"r") as file:
                                dictData = json.load(file)
                            keyDict.update(dictData)
                        with open('%s/%s' % (newDst, 'where.json'),"w") as file:
                            json.dump(keyDict, file)
                        print 'Create where.json --> %s' % newDst

                copyRootDst = '%s/%s' % (dst, 'copy_root.py')
                file_utils.copy(copyRootPath, copyRootDst)
                print 'Copy copy_root.py %s --> %s' % (copyRootPath,copyRootDst)
                    #file_utils.jsonDumper('%s/%s' % (newDst, 'where.json'), tempDict[key])

# def main(assetName=None, assetType=None , department=None, destination=None): 
def main(filePath=None, destination=None, mode=None, keepPath=None, splitShot=None, step=None):

    data = []
    allitems = []

    # for name in assetName:
    #     assetPath = r'P:/SevenChickMovie/asset/work/%s/%s/%s/main/maya' % (assetType, name, department)
    #     if os.path.exists(assetPath):
    #         files = os.listdir(assetPath)
    #         matchExt = [a for a in files if os.path.splitext(a)[-1] == extFilter] if extFilter else files
    #         matchIndex = [sorted(matchExt)[int( fileIndex)]] if fileIndex and len(matchExt) else matchExt
    #         filePath = '%s/%s' % (assetPath, matchIndex[0])
    #         filterFiles.append(filePath)

    print filePath
    if os.path.exists(filePath) or step == 'light' or step == 'lookdev':
        data = utils.get_asset(filePath, batch=True, mode='all', version='2020', step=step)
        allitems += data.get('files')
        pack_asset_batch(allitems, destination, mode, filePath, keepPath, splitShot)
# main(assetName=['canis','arthur'], assetType='char', department='lookdev', destination='E:/outSource/test_batch')
#main(filePath='P:/SevenChickMovie/asset/work/char/arthur/lookdev/main/maya/arthur_ldv_main.v018.Preaw.ma', destination='E:/outSource/test_batch', mode='asset')
if __name__ == "__main__":
    # print sys.argv
    parser = setup_parser('batch collector')
    params = parser.parse_args()    
    main(filePath=params.path, destination=params.destination, mode=params.mode, keepPath=params.keepPath, splitShot=params.splitShot, step=params.step)


