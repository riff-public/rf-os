import maya.cmds as mc
import os
import sys

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

import rf_config as config
PIPELINEGLOBAL = config.Env.pipelineGlobal

from rf_utils.deadline import batch_collector
reload(batch_collector)

class collector(object):
    
    def __init__(self):
        if mc.window('collector', q=True, ex=True):
            mc.deleteUI('collector', window=True)
        
        mc.window('collector' , t='collector')
        mc.columnLayout (adj=True,cal='left')
        mc.rowLayout(numberOfColumns=5 , adj=True)
        #mc.optionMenu( ['fileCache'],label='file cache : ', cc=self.refresh_display)
        #mc.menuItem( 'setDress',label='setDress', p = 'fileCache')
        #mc.menuItem( 'finalCam',label='finalCam', p = 'fileCache')
        mc.text( label='Path : ' )
        mc.textField('path', w=500, h=30)
        #mc.checkBox( 'filter',label='Filter -1' )
        mc.button('add', l='Add', w=100, h=30, c=self.add)
        mc.button('clear', l='Clear', w=100, h=30, c=self.clear)
        mc.setParent('..')
        
        mc.frameLayout ("pathList", label ="Path List",w=200 )
        mc.textScrollList('pathListDisplay',ams=True, h=200) # change append path
        
        mc.setParent('..')
        mc.checkBox('keepPathCheckBox', label='Keep Path', v=True)
        mc.checkBox('splitShotCheckBox', label='Pack Split Shot', v=False)

        mc.setParent('..')
        mc.rowLayout(numberOfColumns=3)
        mc.text( label='Mode : ' )
        mc.radioCollection('mode')
        mc.radioButton('scene' ,label='Scene' ,select=True)
        mc.radioButton('asset' ,label='Asset' )

        mc.setParent('..')
        mc.rowLayout(numberOfColumns=3)
        mc.text( label='Destination Path : ' )
        mc.textField('dstPath', w=500, h=30)
        mc.button('collector', l='Pack', w=150, h=30, al='Left', c=self.pack)
        #mc.button('refresh', l='refresh', w=150, h=30, al='Left')
        
        mc.showWindow('collector')
        
        mc.window('collector' ,e=True)
        #self.refresh_display()

    def add(self, *args):
        text = mc.textField('path', q=True, text=True).replace('\\','/')
        if not '.ma' in text:
            listFile = os.listdir(text)
            for file in listFile:
                if not '.ma' in file:
                    listFile.remove(file)
            text = '%s/%s' % (text, listFile[-1])
        if os.path.exists(text):
            mc.textScrollList('pathListDisplay', e=True, append=text)
        else:
            print "Path doesn't exists."

    def clear(self, *args):
        mc.textScrollList('pathListDisplay', e=True, removeAll=True)

    def pack(self, *args):
        script = '%s/Pipeline/core/rf_app/sceneCollector_scm/sceneCollecter_batch.py' % PIPELINEGLOBAL
        allItems = mc.textScrollList('pathListDisplay',q=True, selectItem=True)
        if not allItems:
            allItems = mc.textScrollList('pathListDisplay',q=True, allItems=True)
        dst = mc.textField('dstPath', q=True, text=True).replace('\\','/')
        mode = mc.radioCollection('mode',q=True, select=True)
        keepPath = mc.checkBox('keepPathCheckBox',q=True, v=True)
        splitShot = mc.checkBox('splitShotCheckBox',q=True, v=True)
        if dst:
            if os.path.exists(dst):
                for item in allItems:
                    cmds = [script, '-p', item, '-d', dst, '-m', mode, '-k', str(keepPath), '-s', str(splitShot)]
                    jobname = "Pack %s" % item.split('/')[-1]
                    executable = 'C:/Program Files/Autodesk/Maya2018/bin/mayapy.exe'
                    batch_collector.process_send_to_farm(cmds, jobname=jobname, executable= executable)
            else:
                print 'Destination does not not exists'
   
collector()