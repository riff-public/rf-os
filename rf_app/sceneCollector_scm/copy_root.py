import os
import shutil
import json
import sys

def find(name, path):
	pathList = []
	for root, dirs, files in os.walk(path):
		if name in files:
			pathList.append(os.path.join(root, name))
	return pathList

def copy(src, dst) :
	if os.path.exists(src) :
		dstDir = dst.replace(dst.split('/')[-1], '')
		if not os.path.exists(dstDir) :
			os.makedirs(dstDir)

		shutil.copy2(src, dst)

	if os.path.exists(dst):
		return dst

def copy_file(jsonFilePath ,root):
	if os.path.exists(root):
		if root[-1] != '/' and root[-1] != '\\':
			root = root + '/'
		for jsonFile in jsonFilePath:
			dirPath = os.path.dirname(jsonFile)
			#dictData = yaml.load(open(jsonFile))
			with open(jsonFile,"r") as file:
				dictData = json.load(file)
			for name, path in dictData.iteritems():
				srcPath = os.path.join(dirPath, name)
				dstPath = root+ path
				copy(srcPath,dstPath)
				print 'Copy %s --> %s' % (srcPath, dstPath)
	else:
		return input_path
		

def input_path ():
	root = raw_input("Please enter Root: ")
	if os.path.exists(root):
		copy_file(jsonFilePath ,root)
	else:

		return input_path()

jsonFileName = 'where.json'
mainPath = os.path.dirname(sys.modules[__name__].__file__)
jsonFilePath = find(jsonFileName,mainPath)
if jsonFilePath:
	input_path()
else:
	print 'where.json does not exists'
