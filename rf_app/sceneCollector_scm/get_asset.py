# System modules
import sys
import os
import re
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
moduleDir = os.path.dirname(sys.modules[__name__].__file__)

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

import rf_config as config
from rf_utils import file_utils
from rf_maya.rftool.utils import yeti_lib

from rf_utils.pipeline import shot_data
from rf_utils.context import context_info

def main() :
    # Maya modules
    import pymel.core as pm
    import maya.cmds as mc
    import maya.mel as mm

    mayaFile = sys.argv[1]
    mode = sys.argv[2]
    # maya.standalone.initialize( name='python' )
    step = sys.argv[3]
    if step:
        if step == 'lookdev':
            build_ldv_all(mayaFile)
        elif step == 'light':
            build(mayaFile)
    try:
        mc.file(mayaFile, force=True , open=True, prompt=False, loadReferenceDepth = mode)
    except Exception as e:
        logger.error(e)
    # mc.file(mayaFile,  i = True, options = 'v=0', pr = True, loadReferenceDepth = 'all')
    files = mc.file(query=1, list=1, withoutCopyNumber=1)
    fileIgnore = []
    for file in files:
        if 'gpu' in file:
            fileIgnore.append(file)
    for fileRemove in fileIgnore:
        files.remove(fileRemove)
        
    files = get_additional_files(files)
    files += get_abc_from_gpu()
    ws_data = get_ws_data(mayaFile)
    if ws_data:
        if os.path.exists(ws_data):
            files.append(ws_data)


    gpuAlembicList = get_abc_gpu_grp(mayaFile)
    files += gpuAlembicList

    exportGrp = get_export_grp(mayaFile)
    files += exportGrp
    # assemblyFiles = getAssemblyFiles()
    assemblyFiles = []
    # textureFiles = mayaTools.getTextureFile()
    tmpFile = 'mayaPyDictTmp.txt'
    tmpdir = os.environ.get('TEMP')
    tmpPath = '%s/%s' % (tmpdir, tmpFile)

    data = {'files': files, 'assemblyFiles': assemblyFiles}

    f = open(tmpPath, 'w')
    f.write(str(data))
    f.close()

# build if scene is light
def build(mayaScene):
    from rf_app.bot import build_cmd
    reload(build_cmd)

    try: 
        buildResult = build_cmd.build_outsource(mayaScene,muteFilter=['ref_data','fx_cache'])
    except Exception as e:
        buildResult = '***  Not Build ****'
        logger.error('Build failed')

def build_ldv_all(mayaScene):
    # 1. new scene with lookdev step and outsource process
    # 2. build lookdev
    # 3. create layer
    # 4. build each layer
    # 5. save scene
    import maya.cmds as mc
    # new scene 
    scene = context_info.ContextPathInfo(path=mayaScene)
    scene.context.update(step='lookdev', process='outsource')
    newScene = build_scene_name(scene)

    mc.file(new=True, f=True)

    looks = list_look(scene)
    for look in looks:
        create_layer(look)
        build_ldv(scene, look)

    result = save_file(newScene)
    logger.info('** Build new scene %s' % newScene)

def build_scene_name(scene): 
    path = scene.path.workspace().abs_path()
    workfile = scene.work_name()
    elem = '%s_autoBuild' % workfile.split('.')[0]
    filename = '%s.%s' % (elem, '.'.join(workfile.split('.')[1:]))
    return '%s/%s' % (path, filename)

def list_look(entity): 
    from rf_utils import register_entity
    res = 'md'
    regInfo = register_entity.Register(entity)
    looks = regInfo.get_looks(step='texture', res=res)
    if not looks:
        looks = ['main']
    return looks

def create_layer(look):
    from rf_maya.lib import material_layer 
    reload(material_layer)
    material_layer.create_layer(look, mode='lookdev')

def list_ldv_input(entity, look):
    from rf_utils.widget import auto_build_widget
    reload(auto_build_widget)
    entity.context.update(look=look, process=look)
    data_look = auto_build_widget.list_input(entity)
    return data_look

# build each look
def build_ldv(entity, look):
    import maya.cmds as mc
    import os
    from rf_utils.widget import auto_build_widget
    # must select layer first
    mc.editRenderLayerGlobals( currentRenderLayer='{}_lookdev'.format(look) )
    data_look = list_ldv_input(entity, look)
    for path,data in data_look.items():
        if os.path.exists(path):
            auto_build_widget.build(data)

def save_file(filepath):
    import maya.cmds as mc
    if not os.path.exists(os.path.dirname(filepath)): 
        os.makedirs(os.path.dirname(filepath))
    name, ext = os.path.splitext(filepath)
    filetype = 'mayaAscii' if ext == '.ma' else 'mayaBinary' if ext == '.mb' else ''
    mc.file(rename=filepath)
    result = mc.file(save=True, f=True, type=filetype)
    return result


def get_abc_from_gpu():
    from rf_utils.pipeline.misc import etc
    abc = etc.get_gpu_asset()
    return abc

def get_additional_files(inputFiles):
    # red shifts
    import maya.cmds as mc
    import maya.app.general.fileTexturePathResolver as ftpr

    pluginNodeConfig = {'RedshiftNormalMap': 'tex0', 'RedshiftSprite': 'tex0'}
    additionalFiles = []
    for nodeType, attr in pluginNodeConfig.iteritems():
        pluginNodes = mc.ls(type=nodeType)

        if pluginNodes:
            for pluginNode in pluginNodes:
                filePath = mc.getAttr('%s.%s' % (pluginNode, attr))
                if filePath:
                    pattern = ftpr.getFilePatternString( filePath , False , 2 )
                    allFiles = ftpr.findAllFilesForPattern( pattern , None )

                    for file in allFiles:
                        additionalFiles.append(file) if not file in additionalFiles else None
    # get yetiGroom texture
    texGroom = yeti_lib.find_all_textures()
    if texGroom:
        for key , value in texGroom.iteritems():
            patternGroom = ftpr.getFilePatternString( key , False , 2 )
            allFilesGroom = ftpr.findAllFilesForPattern( patternGroom , None )

            for fileGroom in allFilesGroom:
                additionalFiles.append(fileGroom) if not fileGroom in additionalFiles else None
    # get texture .ftn
    allFileNodes = mc.ls(et="file")
    if allFileNodes:
        for eachFile in allFileNodes:
            currentFile = mc.getAttr("%s.fileTextureName" % eachFile)
            if currentFile:
                pattern = ftpr.getFilePatternString( currentFile , False , 2 )
                allFiles = ftpr.findAllFilesForPattern( pattern , None )
                for file in allFiles:
                    if not file in inputFiles:
                        additionalFiles.append(file) if not file in additionalFiles else None

    # get gpu 
    gpuFileNodes = mc.ls(type='gpuCache')
    if gpuFileNodes: 
        for node in gpuFileNodes: 
            gpuNode = mc.listRelatives(node, p=True, fullPath=True)
            locator = mc.listRelatives(gpuNode, p=True, fullPath=True)[0]
            if mc.objExists('%s.hidden' % locator):
                if not mc.getAttr('%s.hidden' % locator):
                    file = mc.getAttr('%s.cacheFileName' % node)
                    if not file in inputFiles:
                        additionalFiles.append(file) if not file in additionalFiles else None

    # get yetiCache 
    yetiNodes = mc.ls(type='pgYetiMaya')
    if yetiNodes: 
        for yetiNode in yetiNodes: 
            currentFile = mc.getAttr('%s.cacheFileName' % yetiNode)
            if currentFile:
                baseName = os.path.basename(currentFile)
                patternName = baseName.split('.')[0]
                dirPath = os.path.dirname(currentFile)
                allFiles = os.listdir(dirPath)
                for file in allFiles:
                    if patternName in file:
                        file = dirPath + '/' + file
                        if not file in inputFiles:
                            additionalFiles.append(file) if not file in additionalFiles else None

    #get camera .ma
    for file in inputFiles:
        newFile = file.replace('.abc','ma')
        if os.path.exists(newFile):
            if not newFile in inputFiles:
                additionalFiles.append(newFile) if not newFile in additionalFiles else None


    return inputFiles + additionalFiles


def get_asset(mayaFile, mode='all'):
    try:
        mc.file(mayaFile, force=True , open=True, prompt=False, loadReferenceDepth=mode)
    except Exception as e:
        logger.error(e)

    files = mc.file(query=1, list=1, withoutCopyNumber=1)
    tmpFile = 'get_asset_tmp.yml'
    tmpdir = mm.eval("getenv TMPDIR")
    tmpPath = '%s/%s' % (tmpdir, tmpFile)

    # data = {'files': files, 'assemblyFiles': assemblyFiles}
    data = {'files': files}
    file_utils.ymlDumper(tmpPath, data)

    return data

def getAssemblyFiles():
    return []
    sels = mc.ls(type = 'assemblyReference')
    copyFiles = []

    if sels :
        for each in sels :
            ad = mc.getAttr('%s.definition' % each)

            if ad :
                fileStatus = False

                if os.path.exists(ad) :
                    fileStatus = True

                copyFiles.append((ad, fileStatus))

            datas = listRepIndex(each, 'data')

            if datas :
                for each in datas :
                    fileStatus = False
                    if os.path.exists(each) :
                        fileStatus = True

                    copyFiles.append((each, fileStatus))

    return copyFiles


def listRepIndex(assemblyNode, listType = 'name') :
    lists = mc.assembly(assemblyNode, q = True, listRepresentations = True)

    if listType == 'name' :
        return lists

    labels = []
    datas = []

    if lists :
        for i in range(len(lists)) :
            label = mc.getAttr('%s.representations[%s].repLabel' % (assemblyNode, i))
            data = mc.getAttr('%s.representations[%s].repData' % (assemblyNode, i))
            labels.append(label)
            datas.append(data)

    if listType == 'label' :
        return labels

    if listType == 'data' :
        return datas

def get_ws_data(path):
    entity = context_info.ContextPathInfo(path = path)
    if entity.entity_type == 'scene':
        ws_data = shot_data.get_shot_data_path(entity=entity)
    else:
        ws_data = None
    return ws_data

def get_abc_gpu_grp(path):
    from rftool.utils import abc_utils
    import maya.cmds as mc
    entity = context_info.ContextPathInfo(path = path)
    startTime = entity.projectInfo.scene.start_frame
    endTime = startTime + 1

    result = []

    setGrp = entity.projectInfo.asset.get('set') or 'Set_Grp'
    if mc.objExists(setGrp):
        dstDir = entity.path.hero().abs_path()
        filename = entity.output_name(outputKey='setShot', hero=True)
        ext = filename.split('.')[-1]
        filename = filename.replace(ext, 'abc')
        dstPath = '%s/set_alembic/%s' % (dstDir, filename)

        abc_utils.export_abc(setGrp, dstPath, startTime, endTime)

        result.append(dstPath)


    shotdressGrp = entity.projectInfo.asset.get('shotdressGrp') or 'ShotDress_Grp'
    if mc.objExists(shotdressGrp):
        dstDir = entity.path.hero().abs_path()
        filename = entity.output_name(outputKey='shotDress', hero=True)
        ext = filename.split('.')[-1]
        filename = filename.replace(ext, 'abc')
        dstPath = '%s/set_alembic/%s' % (dstDir, filename)

        abc_utils.export_abc(shotdressGrp, dstPath, startTime, endTime)

        result.append(dstPath)

    return result

def get_export_grp(path):
    entity = context_info.ContextPathInfo(path = path)
    result = []
    dstDir = entity.path.hero().abs_path()
    export_path = '%s/exportGrp' % (dstDir)
    if os.path.exists(export_path):
        files = os.listdir(export_path)
        for f in files:
            dstPath = '%s/%s' % (export_path, f)
            result.append(dstPath)
    return result

if __name__ == '__main__':
    main()