import os
import sys
import subprocess
import _subprocess
import logging
import re
# logger = logging.getLogger(__name__)
# logger.addHandler(logging.NullHandler())
moduleDir = os.path.dirname(sys.modules[__name__].__file__)

from rf_utils import log_utils
logFile = log_utils.name('testsubprocess', user='TA')
logger = log_utils.init_logger(logFile)
from rf_utils import file_utils

from rf_utils.pipeline import shot_data
from rf_utils.context import context_info

class LocalConfig:
    getAsset = '%s/get_asset.py' % moduleDir
    placeholder = '%s/scene.ma' % moduleDir

# def get_asset(version, filePath):
#     run_maya_batch(version, LocalConfig.getAsset, filePath)
#     tmpFile = 'get_asset_tmp.yml'
#     tmpdir = os.environ.get('TMPDIR')
#     tmpPath = '%s/%s' % (tmpdir, tmpFile)
#     data = file_utils.ymlLoader(tmpPath)
#     return data


def run_maya_batch(version, pyCommand, mayaFile) :
    MAYA_PYTHON = 'C:\\Program Files\\Autodesk\\Maya%s\\bin\\mayapy.exe' % version
    # mayaCmdFlPath = 'O:/studioTools/maya/python/tool/myStandAlone/mayapyCmd.py'
    # filePath = 'O:/studioTools/maya/python/tool/myStandAlone/maya.ma'
    # startupinfo = subprocess.STARTUPINFO()
    # startupinfo.dwFlags |= _subprocess.STARTF_USESHOWWINDOW
    p = subprocess.Popen([MAYA_PYTHON, pyCommand, mayaFile])
    # p = subprocess.call([MAYA_PYTHON, pyCommand, mayaFile], stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    stdout, stderr = p.communicate()
    # returnCode = p.returncode

    logger.info(stdout)
    logger.info(stderr)

    return p


def list_file(rootPath, nameFilter=str(), pathFilter=str(), extFilter=str(), fileIndex=str(), pathInvFilter=str()):
    pathMatch = True
    pathMatchRmKey = True
    nameMatch = True
    extMatch = True
    filterFiles = []

    for root, dirs, files in os.walk(rootPath):
        if files:
            normRoot = root.replace('\\', '/')
            if pathFilter:
                pathMatch = True if pathFilter in normRoot else False
            if pathInvFilter:
                pathMatchRmKey = True if pathInvFilter not in normRoot else False

            if all([pathMatch, pathMatchRmKey]):
                matchExt = [a for a in files if os.path.splitext(a)[-1] in extFilter] if extFilter else files
                matchName = [a for a in matchExt if nameFilter in a] if nameFilter else matchExt
                matchFiles = ['%s/%s' % (normRoot, a) for a in matchName]
                matchIndex = [sorted(matchFiles)[int(fileIndex)]] if fileIndex and len(matchFiles) else matchFiles
                filterFiles+=matchIndex

    return filterFiles


def get_asset(mayaFile, batch=True, mode='all', version='2017', step=None) :
    if batch :
        print 'this is batch mode'
        # pyCommand = 'D:/TA/Dropbox/script_server/core/rf_app/sceneCollector/get_asset.py'
        pyCommand = LocalConfig.getAsset
        runMayaBatch(pyCommand, mayaFile, version, mode, step)

        tmpFile = 'mayaPyDictTmp.txt'
        tmpdir = os.environ.get('TEMP')
        tmpPath = '%s/%s' % (tmpdir, tmpFile)

        if os.path.exists(tmpPath) :
            f = open(tmpPath, 'r')
            data = f.read()
            f.close()

            result = eval(data)
            os.remove(tmpPath)
            return result
        else:
            print 'file does not exists'
            return dict()

    else :
        try:
            import maya.cmds as mc
            mc.file(mayaFile, force=True , open=True, prompt=False, loadReferenceDepth=mode)
        except Exception as e:
            logger.error(e)

        files = mc.file(query=1, list=1, withoutCopyNumber=1)
        fileIgnore = []
        for file in files:
            if 'gpu' in file:
                fileIgnore.append(file)
        for fileRemove in fileIgnore:
            files.remove(fileRemove)
        
        files = get_additional_files(files)
        files += get_abc_from_gpu()
        ws_data = get_ws_data(mayaFile)
        if ws_data:
            if os.path.exists(ws_data):
                files.append(ws_data)

        gpuAlembicList = get_abc_gpu_grp(mayaFile)
        files += gpuAlembicList

        exportGrp = get_export_grp(mayaFile)
        files += exportGrp

        tmpFile = 'get_asset_tmp.yml'
        tmpdir = os.environ.get('TEMP')
        tmpPath = '%s/%s' % (tmpdir, tmpFile)

        # data = {'files': files, 'assemblyFiles': assemblyFiles}
        data = {'files': files}
        file_utils.ymlDumper(tmpPath, data)

        return data

def get_abc_from_gpu():
    from rf_utils.pipeline.misc import etc
    abc = etc.get_gpu_asset()
    return abc

def get_additional_files(inputFiles):
    # red shifts
    import maya.cmds as mc
    import maya.app.general.fileTexturePathResolver as ftpr
    from rf_maya.rftool.utils import yeti_lib

    pluginNodeConfig = {'RedshiftNormalMap': 'tex0', 'RedshiftSprite': 'tex0'}
    additionalFiles = []
    for nodeType, attr in pluginNodeConfig.iteritems():
        pluginNodes = mc.ls(type=nodeType)

        if pluginNodes:
            for pluginNode in pluginNodes:
                filePath = mc.getAttr('%s.%s' % (pluginNode, attr))
                if filePath:
                    pattern = ftpr.getFilePatternString( filePath , False , 2 )
                    allFiles = ftpr.findAllFilesForPattern( pattern , None )

                    for file in allFiles:
                        additionalFiles.append(file) if not file in additionalFiles else None
    # get yetiGroom texture
    texGroom = yeti_lib.find_all_textures()
    if texGroom:
        for key , value in texGroom.iteritems():
            patternGroom = ftpr.getFilePatternString( value['path'] , False , 2 )
            allFilesGroom = ftpr.findAllFilesForPattern( patternGroom , None )

            for fileGroom in allFilesGroom:
                additionalFiles.append(fileGroom) if not fileGroom in additionalFiles else None
    # get texture .ftn
    allFileNodes = mc.ls(et="file")
    if allFileNodes:
        for eachFile in allFileNodes:
            currentFile = mc.getAttr("%s.fileTextureName" % eachFile)
            if currentFile:
                pattern = ftpr.getFilePatternString( currentFile , False , 2 )
                allFiles = ftpr.findAllFilesForPattern( pattern , None )
                for file in allFiles:
                    if not file in inputFiles:
                        additionalFiles.append(file) if not file in additionalFiles else None

    # get gpu 
    gpuFileNodes = mc.ls(type='gpuCache')
    if gpuFileNodes: 
        for node in gpuFileNodes: 
            gpuNode = mc.listRelatives(node, p=True, fullPath=True)
            locator = mc.listRelatives(gpuNode, p=True, fullPath=True)[0]
            if mc.objExists('%s.hidden' % locator):
                if not mc.getAttr('%s.hidden' % locator):
                    file = mc.getAttr('%s.cacheFileName' % node)
                    if not file in inputFiles:
                        additionalFiles.append(file) if not file in additionalFiles else None

    # get yetiCache 
    yetiNodes = mc.ls(type='pgYetiMaya')
    if yetiNodes: 
        for yetiNode in yetiNodes: 
            currentFile = mc.getAttr('%s.cacheFileName' % yetiNode)
            if currentFile:
                baseName = os.path.basename(currentFile)
                patternName = baseName.split('.')[0]
                dirPath = os.path.dirname(currentFile)
                if os.path.exists(dirPath):
                    allFiles = os.listdir(dirPath)
                    for file in allFiles:
                        if patternName in file:
                            file = dirPath + '/' + file
                            if not file in inputFiles:
                                additionalFiles.append(file) if not file in additionalFiles else None

    #get camera .ma
    for file in inputFiles:
        newFile = file.replace('.abc','ma')
        if os.path.exists(newFile):
            if not newFile in inputFiles:
                additionalFiles.append(newFile) if not newFile in additionalFiles else None

    return inputFiles + additionalFiles

def runMayaBatch(pyCommand, mayaFile, version = '', mode='all', step=None) :
    if not version :
        version = mc.about(v = True)

    if '2012' in version :
        MAYA_PYTHON = 'C:\\Program Files\\Autodesk\\Maya2012\\bin\\mayapy.exe'

    if '2015' in version :
        MAYA_PYTHON = 'C:\\Program Files\\Autodesk\\Maya2015\\bin\\mayapy.exe'

    if '2016' in version :
        MAYA_PYTHON = 'C:\\Program Files\\Autodesk\\Maya2016\\bin\\mayapy.exe'

    if '2017' in version :
        MAYA_PYTHON = 'C:\\Program Files\\Autodesk\\Maya2017\\bin\\mayapy.exe'

    if '2018' in version :
        MAYA_PYTHON = 'C:\\Program Files\\Autodesk\\Maya2018\\bin\\mayapy.exe'

    if '2019' in version :
        MAYA_PYTHON = 'C:\\Program Files\\Autodesk\\Maya2019\\bin\\mayapy.exe'

    if '2020' in version :
        MAYA_PYTHON = 'C:\\Program Files\\Autodesk\\Maya2020\\bin\\mayapy.exe'

    # mayaCmdFlPath = 'O:/studioTools/maya/python/tool/myStandAlone/mayapyCmd.py'
    # filePath = 'O:/studioTools/maya/python/tool/myStandAlone/maya.ma'
    subprocess.call([MAYA_PYTHON, pyCommand, mayaFile, mode, step])


def get_drive_regx(drives):
    strDrive = "("
    for drive in drives:
        strDrive = strDrive + drive + ":|"
    strDrive = strDrive[:-1] + ")"
    return strDrive


def check_duplicate_name(data_path, path):
    if path in data_path:
        return True
    return False


def get_data_path(input_file):
    drive = ["P", "O", "Y", "N", "C", "D"]
    dirve_search = get_drive_regx(drive)
    regx_find_path = "\\b{dirve_search}.*\.[\w:]+".format(dirve_search=dirve_search)
    data_path = []

    with open(input_file, "r") as file_:
        for line in file_:
            if line.startswith("applyMetadata"):
                continue
            result = re.search(regx_find_path, line)
            if result is not None:
                path = result.group()
                isDuplicate = check_duplicate_name(data_path, path)
                if isDuplicate:
                    continue
                data_path.append(path)
    return data_path

def get_ws_data(path):
    entity = context_info.ContextPathInfo(path = path)
    if entity.entity_type == 'scene':
        ws_data = shot_data.get_shot_data_path(entity=entity)
    else:
        ws_data = None
    return ws_data

def get_abc_gpu_grp(path):
    from rftool.utils import abc_utils
    import maya.cmds as mc
    entity = context_info.ContextPathInfo(path = path)
    startTime = entity.projectInfo.scene.start_frame
    endTime = startTime + 1

    result = []

    setGrp = entity.projectInfo.asset.get('set') or 'Set_Grp'
    if mc.objExists(setGrp):
        dstDir = entity.path.hero().abs_path()
        filename = entity.output_name(outputKey='setShot', hero=True)
        ext = filename.split('.')[-1]
        filename = filename.replace(ext, 'abc')
        dstPath = '%s/set_alembic/%s' % (dstDir, filename)

        abc_utils.export_abc(setGrp, dstPath, startTime, endTime)

        result.append(dstPath)


    shotdressGrp = entity.projectInfo.asset.get('shotdressGrp') or 'ShotDress_Grp'
    if mc.objExists(shotdressGrp):
        dstDir = entity.path.hero().abs_path()
        filename = entity.output_name(outputKey='shotDress', hero=True)
        ext = filename.split('.')[-1]
        filename = filename.replace(ext, 'abc')
        dstPath = '%s/set_alembic/%s' % (dstDir, filename)

        abc_utils.export_abc(shotdressGrp, dstPath, startTime, endTime)

        result.append(dstPath)

    return result


def get_export_grp(path):
    entity = context_info.ContextPathInfo(path = path)
    result = []
    dstDir = entity.path.hero().abs_path()
    export_path = '%s/exportGrp' % (dstDir)
    if os.path.exists(export_path):
        files = os.listdir(export_path)
        for f in files:
            dstPath = '%s/%s' % (export_path, f)
            result.append(dstPath)
    return result