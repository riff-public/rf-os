from collections import defaultdict
from rf_utils.context import context_info
import json
from rf_utils.pipeline.misc import etc

scene = context_info.ContextPathInfo()
all_asset = mc.file(query=1, list=1, withoutCopyNumber=1)
abc = etc.get_gpu_asset()
all_asset += abc
send_path = 'P:/SevenChickMovie/ftp/send/Chaya/2019_03_09'
dest_path = os.path.join(send_path, scene.name).replace('\\', '/')


if not os.path.exists(dest_path):
    os.makedirs(dest_path)
where = defaultdict(str) 
error_file = []

for asset in all_asset:
    asset_name = os.path.basename(asset)
    dest_asset_path = os.path.join(dest_path, asset_name).replace('\\', '/')
    status = mc.sysFile( asset, copy=dest_asset_path )
    if status:
        where[asset_name] = os.path.splitdrive(asset)[1]
    else:
        error_file.append(asset)
        
where_file = os.path.join(dest_path, 'where.json')

with open(where_file, "w") as file_asset:
    json.dump(where, file_asset)