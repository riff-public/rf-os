# v.0.0.1 polytag switcher
# v.0.2.0 add recursion maya files
# add drag drop files
# add related files list

_title = 'Riff Scene Collector Scm'
_version = 'v.0.2.0'
_des = 'wip'
uiName = 'RFSceneCollectorUI'

#Import python modules
import sys
import os
import getpass
from collections import OrderedDict
import json

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# config file
config_file = '%s/local_config.json' % moduleDir
with open(config_file) as configData:
    localConfig = json.load(configData)

SCRIPT = localConfig['SCRIPT']
ROOT = os.environ.get(SCRIPT)
core = localConfig['CORE']

# import config
sys.path.append('%s/%s' % (ROOT, core))
import rf_config as config

# framework modules
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet
from rf_utils import icon
from rf_utils import file_utils
from rf_utils.widget import drop_widget
import utils
reload(utils)
import get_asset
reload(get_asset)
reload(icon)

from rf_maya.rftool.utils import yeti_lib

from rf_utils import strip_abc_namespace_collector

import maya.cmds as mc

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

class Config: 
    assetKey = 'asset'
    sceneKey = 'scene'
    searchKey = [assetKey, sceneKey]
    copyRootPath = '%s/copy_root.py' % moduleDir


class RFSceneCollector(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        #Setup Window
        super(RFSceneCollector, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        if config.isMaya:
            self.ui = load.setup_ui_maya(uiFile, parent)
        else:
            self.ui = load.setup_ui(uiFile, self)

        self.ui.show()
        self.ui.setWindowTitle('%s %s-%s' % (_title, _version, _des))

        self.iconMap = {'.ma': icon.maya}
        self.defaultIcon = icon.etc

        self.init_ui()
        self.init_signals()
        self.init_functions()

    def init_ui(self):
        # self.ui.scene_listWidget.setVisible(False)
        self.ui.scene_listWidget = drop_widget.ListWidget()
        self.ui.gridLayout_2.addWidget(self.ui.scene_listWidget, 2, 1, 1, 1)
        self.ui.asset_listWidget = drop_widget.ListWidget()
        self.ui.gridLayout_2.addWidget(self.ui.asset_listWidget, 2, 2, 1, 1)

    def init_signals(self):
        # input file
        self.ui.input_lineEdit.returnPressed.connect(self.add_file)
        self.ui.clear_pushButton.clicked.connect(self.clear_input)

        # list asset
        self.ui.listAsset_pushButton.clicked.connect(self.list_asset)
        self.ui.scene_listWidget.multipleDropped.connect(self.add_files)
        self.ui.asset_listWidget.multipleDropped.connect(self.add_copy_list)

        # dst link
        self.ui.dst_lineEdit.returnPressed.connect(self.check_dst_path)
        self.ui.clearAsset_pushButton.clicked.connect(self.clear_asset)

        # copy
        self.ui.copy_pushButton.clicked.connect(self.pack_asset)

        # save data
        self.ui.save_pushButton.clicked.connect(self.save_report)

        # file lister
        self.ui.list_pushButton.clicked.connect(self.list_file)
        self.ui.clearFile_pushButton.clicked.connect(self.clear_file)
        self.ui.root_lineEdit.returnPressed.connect(self.check_root)
        self.ui.add_pushButton.clicked.connect(self.file_to_list)

    def init_functions(self):
        # clear ui
        self.ui.asset_listWidget.clear()
        self.ui.scene_listWidget.clear()

        # # set debug
        # self.ui.root_lineEdit.setText('P:/pucca/EP04/scene/anim')
        # self.ui.ext_lineEdit.setText('.ma')
        # self.ui.pathFilter_lineEdit.setText('anim/s0400/version')


    def add_file(self):
        """ add file to list """
        inputFile = str(self.ui.input_lineEdit.text()).replace('\\', '/')
        self.add_to_list(inputFile)
        self.summary(self.ui.sum2_label, self.ui.scene_listWidget)

    def add_files(self, files):
        for file in files:
            self.add_to_list(file, check=True)

    def add_to_list(self, path, check=True):
        """ add to scene listWidget """
        if os.path.exists(path):
            self.add_input_list(self.ui.scene_listWidget, path, path, check=check)
            self.ui.input_lineEdit.clear()
        else:
            logger.warning('%s not exists' % path)
            if check:
                QtWidgets.QMessageBox.warning(self, 'File not exists', '%s not exists' % path)


    def add_copy_list(self, dataFiles):
        dataFile = dataFiles[0] if dataFiles else None
        if dataFile:
            fileList = file_utils.ymlLoader(dataFile)
            files = [a['file'] for a in fileList]
            for file in files:
                self.add_input_list(self.ui.asset_listWidget, file, file, check=False)

            self.summary(self.ui.sum3_label, self.ui.asset_listWidget)

    def clear_input(self):
        result = QtWidgets.QMessageBox.question(self, 'Clear list', 'Clear all input list?', QtWidgets.QMessageBox.Yes, QtWidgets.QMessageBox.No)
        if result == QtWidgets.QMessageBox.Yes:
            self.ui.scene_listWidget.clear()
            self.summary(self.ui.sum2_label, self.ui.scene_listWidget)

    def clear_asset(self):
        result = QtWidgets.QMessageBox.question(self, 'Clear list', 'Clear all asset list?', QtWidgets.QMessageBox.Yes, QtWidgets.QMessageBox.No)
        if result == QtWidgets.QMessageBox.Yes:
            self.ui.asset_listWidget.clear()
            self.summary(self.ui.sum3_label, self.ui.asset_listWidget)

    def clear_file(self):
        result = QtWidgets.QMessageBox.question(self, 'Clear list', 'Clear all file list?', QtWidgets.QMessageBox.Yes, QtWidgets.QMessageBox.No)
        if result == QtWidgets.QMessageBox.Yes:
            self.ui.itemFilter_listWidget.clear()
            self.summary(self.ui.sum1_label, self.ui.itemFilter_listWidget)


    def add_input_list(self, widget, text, data, check=False, dialog=True):
        """ add to list widget """
        add = True
        dataFormat = {'file': data, 'status': False}
        if check:
            inputList = [a.data(QtCore.Qt.UserRole)['file'] for a in self.all_items(self.ui.scene_listWidget)]
            add = True if not data in inputList else False

        if add:
            iconPath = self.iconMap.get(os.path.splitext(text)[-1], self.defaultIcon)
            iconWidget = QtGui.QIcon()
            iconWidget.addPixmap(QtGui.QPixmap(iconPath),QtGui.QIcon.Normal,QtGui.QIcon.Off)

            item = QtWidgets.QListWidgetItem(widget)
            item.setText(text)
            item.setData(QtCore.Qt.UserRole, dataFormat)
            item.setIcon(iconWidget)

        else:
            logger.warning('%s already in the list' % text)
            if check and dialog:
                QtWidgets.QMessageBox.warning(self, 'Existing', '%s already in the list' % text)


    def all_items(self, widget):
        allItem = [widget.item(a) for a in range(widget.count())]
        return allItem


    def list_asset(self):
        """ list all asset """
        batch = self.ui.batch_checkBox.isChecked()
        items = self.all_items(self.ui.scene_listWidget)
        modeUi = self.ui.mode_checkBox.isChecked() # asset only
        researchCheckBox = self.ui.researchMa_checkBox.isChecked()
        self.assetDict = OrderedDict()
        self.dictShot = OrderedDict()
        assetList = []

        for item in items:
            itemData = item.data(QtCore.Qt.UserRole)
            path = itemData.get('file')
            status = itemData.get('status')

            # do only status that's not complete
            if not status:
                if config.isMaya:
                    mode = 'none' if modeUi else 'all'
                    data = utils.get_asset(path, batch=batch, mode=mode, version='2017')

                else:
                    mode = 'none' if modeUi else 'all'
                    batch = True
                    data = utils.get_asset(path, batch=batch, mode=mode, version='2017')

                self.assetDict[path] = data.get('files')
                assetList+=data.get('files')
                status = True
                self.set_status(item, status)

                maFiles = [a for a in data.get('files') if os.path.splitext(a)[-1] == '.ma']
                self.add_back_input_list(maFiles) if researchCheckBox else None

                # packScene
                shotName = path.split('/')[5]
                self.dictShot[shotName] = data.get('files')
                # packScene End #
                
                dst = str(self.ui.dst_lineEdit.text())
                if dst:
                    key = Config.assetKey
                    yetiNodes = mc.ls(type='pgYetiMaya')
                    dstPath = '%s/%s/%s' % (dst, key, shotName)
                    if not os.path.exists(dstPath):
                        os.makedirs(dstPath)
                    if yetiNodes: 
                        for yetiNode in yetiNodes: 
                            yeti_lib.cache(yetiNode, '%s/%s.fur'%(dstPath, yetiNode.split(':')[-1]), 1, 1)

        self.display_list(list(set(assetList)))

        if researchCheckBox:
            items = self.all_items(self.ui.scene_listWidget)
            runagain = any([not a.data(QtCore.Qt.UserRole)['status'] for a in items])
            self.list_asset() if runagain else None

        self.summary(self.ui.sum3_label, self.ui.asset_listWidget)

    def add_back_input_list(self, maFiles):
        for maFile in maFiles:
            self.add_input_list(self.ui.scene_listWidget, maFile, maFile, check=True, dialog=False)

        self.add_asset_keyword(maFiles)

    def add_asset_keyword(self, maFiles):
        add = self.ui.addAssetKeyword_checkBox.isChecked()
        keywords = self.list_input(str(self.ui.assetKeyword_lineEdit.text()))
        additionalFiles = []

        if add and keywords:
            for maFile in maFiles:
                dirname = os.path.dirname(maFile)
                files = file_utils.list_file(dirname)
                files = [a for a in files if os.path.splitext(a)[-1] in ['.ma']]
                addFiles = []

                for eachFile in files:
                    if any(key in eachFile for key in keywords):
                        print 'add file', eachFile
                        addFiles.append(eachFile) if not eachFile in addFiles else None

                for addFile in addFiles:
                    addFile = '%s/%s' % (dirname, addFile)
                    if not addFile in additionalFiles:
                        additionalFiles.append(addFile)

            for addFile in additionalFiles:
                self.add_input_list(self.ui.scene_listWidget, addFile, addFile, check=True, dialog=False)

    def list_input(self, rawString):
        # list
        elems = []
        if rawString:
            if ',' in rawString:
                elems = [a.strip() for a in rawString.split(',')]
            else:
                elems = [rawString.strip()]
        return elems

    def display_list(self, assetList):
        """ display asset list """
        allAssets = [a.data(QtCore.Qt.UserRole)['file'] for a in self.all_items(self.ui.asset_listWidget)]
        displayAsset = [a for a in assetList if not a in allAssets]

        for asset in displayAsset:
            self.add_input_list(self.ui.asset_listWidget, asset, asset)

    def check_dst_path(self):
        dstPath = str(self.ui.dst_lineEdit.text()).replace('\\', '/')
        self.ui.dst_lineEdit.setText(dstPath)


    def pack_asset(self):
        #  - Pack Scene Mode will pack asset split by shot
        #  - Pack Asset Mode will pack everything from original path

        # packScene
        if self.ui.packScene_checkBox.isChecked():
            tempDict = dict()
            allItems = self.all_items(self.ui.asset_listWidget)
            dst = str(self.ui.dst_lineEdit.text())
            for shotName, assetList in self.dictShot.iteritems():
                for assetPath in assetList:
                    nrmPath = os.path.normpath(assetPath)
                    assetName = nrmPath.split(os.sep)
                    if len(assetName) < 6:
                        continue
                    assetName = assetName[5]
                    key = Config.sceneKey
                    newDst = '%s/%s/%s' % (dst, key, shotName)
                    # newDst = '%s/%s' % (dst, key)
                    base = os.path.basename(assetPath)
                    dstPath = '%s/%s' % (newDst, base)
                    tempDict[base] = os.path.splitdrive(assetPath)[-1]

                    # copy
                    try :
                        file_utils.copy(assetPath, dstPath)
                        logger.info('Copy %s -> %s' % (assetPath, dstPath))

                        # strip abc namespace 
                        try :
                            if '.abc' in dstPath:
                                abcPath = [r'%s'%str(dstPath)]
                                strip_abc_namespace_collector.main(abcPath)
                        except Exception as e:
                            print e

                        status = True if os.path.exists(dstPath) else False

                        for item in allItems:
                            path = item.data(QtCore.Qt.UserRole)['file']
                            if assetPath == path:
                                self.set_status(item, status)

                    except Exception as e:
                        print e

                # create where.json        
                if os.path.exists(newDst):
                    keyDict = tempDict
                    jsonPath = '%s/where.json'%newDst
                    # append where.json
                    if os.path.exists(jsonPath):
                        with open(jsonPath,"r") as file:
                            dictData = json.load(file)
                        keyDict.update(dictData)
                    with open('%s/%s' % (newDst, 'where.json'),"w") as file:
                        json.dump(keyDict, file)
                    #print 'Create where.json --> %s' % newDst

            if self.ui.direct_checkBox.isChecked():
                copyRootDst = '%s/%s' % (dst, 'copy_root.py')
                file_utils.copy(Config.copyRootPath, copyRootDst)
                print 'Copy copy_root.py %s --> %s' % (Config.copyRootPath,copyRootDst)

        else:
            """ copy to dst dir """
            allItems = self.all_items(self.ui.asset_listWidget)
            dst = str(self.ui.dst_lineEdit.text())
            tempDict = dict()

            if os.path.exists(dst):
                for item in allItems:
                    path = item.data(QtCore.Qt.UserRole)['file']
                    itemStatus = item.data(QtCore.Qt.UserRole)['status']
                    dstPath = '%s%s' % (dst, os.path.splitdrive(path)[-1])
                    
                    if self.ui.direct_checkBox.isChecked():
                        nrmPath = os.path.normpath(path)
                        assetName = nrmPath.split(os.sep)
                        if len(assetName) < 6:
                            continue
                        assetName = assetName[5]
                        for key in Config.searchKey:
                            if key in path:
                                newDst = '%s/%s/%s' % (dst, key, assetName)
                                # newDst = '%s/%s' % (dst, key)
                                base = os.path.basename(path)
                                dstPath = '%s/%s' % (newDst, base)
                                tempDict[base] = os.path.splitdrive(path)[-1]

                            else:
                                key = Config.assetKey
                                newDst = '%s/%s/%s' % (dst, key, assetName)
                                # newDst = '%s/%s' % (dst, key)
                                base = os.path.basename(path)
                                dstPath = '%s/%s' % (newDst, base)
                                tempDict[base] = os.path.splitdrive(path)[-1]
                                   
                    # copy
                    try :
                        file_utils.copy(path, dstPath)
                        logger.info('Copy %s -> %s' % (path, dstPath))

                        # strip abc namespace 
                        try :
                            if '.abc' in dstPath:
                                abcPath = [r'%s'%str(dstPath)]
                                strip_abc_namespace_collector.main(abcPath)
                        except Exception as e:
                            print e

                        status = True if os.path.exists(dstPath) else False
                        self.set_status(item, status)

                    except Exception as e:
                        print e

                    # create where.json 
                    if self.ui.direct_checkBox.isChecked():       
                        if os.path.exists(newDst):
                            keyDict = tempDict
                            jsonPath = '%s/where.json'%newDst
                            # append where.json
                            if os.path.exists(jsonPath):
                                with open(jsonPath,"r") as file:
                                    dictData = json.load(file)
                                keyDict.update(dictData)
                            with open('%s/%s' % (newDst, 'where.json'),"w") as file:
                                json.dump(keyDict, file)
                            print 'Create where.json --> %s' % newDst

                if self.ui.direct_checkBox.isChecked():
                    copyRootDst = '%s/%s' % (dst, 'copy_root.py')
                    file_utils.copy(Config.copyRootPath, copyRootDst)
                    print 'Copy copy_root.py %s --> %s' % (Config.copyRootPath,copyRootDst)
                        #file_utils.jsonDumper('%s/%s' % (newDst, 'where.json'), tempDict[key])


    def set_status(self, item, status):
        iconPath = icon.ok if status else icon.no
        iconWidget = QtGui.QIcon()
        iconWidget.addPixmap(QtGui.QPixmap(iconPath),QtGui.QIcon.Normal,QtGui.QIcon.Off)
        item.setIcon(iconWidget)

        # add status back
        data = item.data(QtCore.Qt.UserRole)
        data['status'] = status
        item.setData(QtCore.Qt.UserRole, data)

        QtWidgets.QApplication.processEvents()

    def save_report(self):
        # asset list dict
        reportPath = '%s/%s' % (str(self.ui.dst_lineEdit.text()), 'scene_list.yml')
        reportPath2 = '%s/%s' % (str(self.ui.dst_lineEdit.text()), 'asset_list.yml')
        paths = [a.data(QtCore.Qt.UserRole) for a in self.all_items(self.ui.asset_listWidget)]

        if os.path.exists(os.path.dirname(reportPath)):
            result1 = file_utils.ymlDumper(reportPath, self.assetDict)

        if os.path.exists(os.path.dirname(reportPath2)):
            result2 = file_utils.ymlDumper(reportPath2, paths)

        return all([result1, result2])


    # label summary
    def summary(self, label, widget):
        items = self.all_items(widget)
        message = '%s files' % len(items)
        label.setText(message)


    # ==== file lister section
    def check_root(self):
        root = str(self.ui.root_lineEdit.text())
        self.ui.root_lineEdit.setText(root.replace('\\', '/'))

    def list_file(self):
        """ list file from criteria below """
        root = str(self.ui.root_lineEdit.text())
        pathFilter = str(self.ui.pathFilter_lineEdit.text())
        pathInvFilter = str(self.ui.pathFilterInv_lineEdit.text())
        nameFilter = str(self.ui.filenameFilter_lineEdit.text())
        extFilter = str(self.ui.ext_lineEdit.text())
        fileIndex = str(self.ui.fileIndex_lineEdit.text())

        files = utils.list_file(rootPath=root, nameFilter=nameFilter, pathFilter=pathFilter, extFilter=extFilter, fileIndex=fileIndex, pathInvFilter=pathInvFilter)
        currentPaths = [str(a.text()) for a in self.all_items(self.ui.itemFilter_listWidget)]

        addFiles = [a for a in files if not a in currentPaths]

        for path in addFiles:
            self.add_file_list(path)

        self.summary(self.ui.sum1_label, self.ui.itemFilter_listWidget)


    def add_file_list(self, path):
        item = QtWidgets.QListWidgetItem(self.ui.itemFilter_listWidget)
        item.setText(path)
        iconPath = self.iconMap.get(os.path.splitext(path)[-1], self.defaultIcon)
        iconWidget = QtGui.QIcon()
        iconWidget.addPixmap(QtGui.QPixmap(iconPath),QtGui.QIcon.Normal,QtGui.QIcon.Off)
        item.setIcon(iconWidget)


    def file_to_list(self):
        """ add file list to scene list """
        addItems = self.ui.itemFilter_listWidget.selectedItems()
        if not addItems:
            addItems = self.all_items(self.ui.itemFilter_listWidget)
        paths = [str(a.text()) for a in addItems]

        for path in paths:
            self.add_to_list(path, check=False)

        self.summary(self.ui.sum2_label, self.ui.scene_listWidget)



def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = RFSceneCollector(maya_win.getMayaWindow())
        return myApp

    else:
        logger.info('Run in RFSceneCollector\n')
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        myApp = RFSceneCollector()
        # stylesheet.set_default(app)
        sys.exit(app.exec_())

if __name__ == '__main__':
    show()
