# check local user if it set up properly 
import sys
import os 
import config_env 
from rf.utils.sg import sg_utils
env = config_env.read()

def local_user(): 
	return os.environ.get(env.get('USER'))

def sg_user(): 
	return sg_utils.sg.find('HumanUser', [], ['name', 'sg_localuser'])

def find_sg_user(user): 
	userEntities = sg_user()

	for userEntity in userEntities: 
		if user == userEntity.get('sg_localuser'): 
			return userEntity

def run(): 
	localUser = local_user()
	sgUser = find_sg_user(localUser)

	return localUser, sgUser
