# v.0.0.1 polytag switcher
_title = 'Riff Builder'
_version = 'v.0.0.1'
_des = 'Wip'
uiName = 'RFBuilderUi'

#Import python modules
import sys
import os
import getpass
from datetime import datetime
import logging
from functools import partial
from collections import OrderedDict

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
import rf_config as config

# framework modules
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui
from view import ui
reload(ui)
from view import widget 
reload(widget)
from model import build_cmd
reload(build_cmd)
from model import data_format
reload(data_format)
from model import display_config as display
reload(display)
from rf_utils import icon

from rf_utils.context import context_info 
from rf_utils.sg import sg_process 
from rf_utils import register_shot
reload(register_shot)
from rf_utils import register_entity
ui.sg = sg_process.sg
sg = sg_process.sg


class BuilderCore(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(BuilderCore, self).__init__(parent)

        # ui read
        self.ui = ui.BuilderUi(parent)
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)

        # title
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.resize(1100, 700)

        self.init_functions()
        self.init_signals()

    def init_functions(self): 
        """ run a list of functions """ 
        self.modules = data_format.get_module_data()
        self.setup_ui() 

    def init_signals(self): 
        """ setup signal logic on ui """ 
        self.ui.setEntityButton.clicked.connect(self.list_data)
        self.ui.manualInputCheckBox.stateChanged.connect(self.override_scene_context)

        # filter data 
        self.ui.filterDataCheckBox.stateChanged.connect(self.list_data)

        # context menu
        self.ui.contentWidget.treeWidget.customContextMenuRequested.connect(self.show_context_menu)

    def show_context_menu(self, pos): 
        """ manage menu """ 
        currentItem = self.ui.contentWidget.treeWidget.currentItem()
        dataType = self.ui.contentWidget.item_type(currentItem)
        
        # menu 
        menu = QtWidgets.QMenu(self)

        if dataType in [ui.ItemLevel.dataType, ui.ItemLevel.groupType]: 
            self.create_sub_menu(menu, currentItem)

        menu.popup(self.ui.contentWidget.treeWidget.mapToGlobal(pos))

    def create_sub_menu(self, menu, item): 
        """ create datatype menu """ 
        itemLabel = self.ui.contentWidget.get_item_label(item)

        buildMenu = menu.addAction(ui.ContextMenu.dataType.get('build'))
        buildMenu.triggered.connect(partial(self.action_call, item, display.ButtonCommand.build))
        menu.addSeparator()
        refreshMenu = menu.addAction(ui.ContextMenu.dataType.get('refresh'))
        refreshMenu.triggered.connect(partial(self.action_call, item, display.ButtonCommand.refresh))
        menu.addSeparator()
        removeMenu = menu.addAction(ui.ContextMenu.dataType.get('remove'))
        removeMenu.triggered.connect(partial(self.action_call, item, display.ButtonCommand.remove))
        menu.addSeparator()

    def set_context(self): 
        """ read context from scene or from nav """ 
        if self.ui.manualInputCheckBox.isChecked(): 
            # read from nav 
            data = self.ui.entityWidget.shotWidget.current_item()
            scene = build_cmd.setup_context(sg, data)
        else: 
            # read from scene 
            scene = context_info.ContextPathInfo()
        return scene 

    def override_scene_context(self, value): 
        state = True if value else False 
        self.ui.projectWidget.projectComboBox.setEnabled(state)
        self.ui.entityWidget.set_enabled(state)
        self.ui.setEntityButton.setText(ui.Button.autoLabel)
        if state: 
            self.ui.projectWidget.fetch_project()
            self.ui.setEntityButton.setText(ui.Button.manualLabel)

    def setup_ui(self): 
        """ setup ui content """ 
        # disabled manuaul navigation
        self.ui.projectWidget.projectComboBox.setEnabled(False)
        self.ui.entityWidget.set_enabled(False) 
        self.ui.filterDataCheckBox.setChecked(True)

    def list_data(self): 
        """ list item from register """ 
        start = datetime.now()
        # instancetiated entity and register objects 
        self.scene = self.set_context()
        self.reg = register_shot.Register(self.scene)

        # get sorting assets name 
        filterData = self.ui.filterDataCheckBox.isChecked()
        dataDisplayOrder = ['camera', 'abc_cache'] # this will be replaced by config 

        assets = build_cmd.get_asset_list(self.scene, self.reg, filterData, preferredOrder=dataDisplayOrder)
        orderedData = build_cmd.regroup_data(self.reg, assets)
        print 'finished fetching in %s' % (datetime.now() - start)

        startDrawing = datetime.now()
        # self.ui.contentWidget.treeWidget.setUpdatesEnabled(False)
        self.set_display(orderedData)
        # self.ui.contentWidget.treeWidget.setUpdatesEnabled(True)
        print 'finished draw in %s' % (datetime.now() - startDrawing)
        print 'done in ', datetime.now() - start 

    def filter_data_chekced(self, value): 
        state = True if value else False 
        if state: 
            build_cmd.filter_cache(entity, reg, assets)

    def set_display(self, orderedData): 
        """ set tree widget """ 
        # root 
        self.ui.contentWidget.clear()
        self.rootItem = self.ui.contentWidget.set_root()
        
        # root all 
        self.rootAllItem = self.ui.contentWidget.set_all(self.rootItem, display.GroupLabel.root)
        self.ui.contentWidget.expand_tree(self.rootAllItem)

        # all buttons 
        self.set_build_all(self.rootAllItem, display.ButtonCommand.build)
        self.set_build_all(self.rootAllItem, display.ButtonCommand.refresh)
        self.set_build_all(self.rootAllItem, display.ButtonCommand.remove)

        # data 
        self.ui.contentWidget.set_data(itemType=ui.ItemLevel.top, item=self.rootAllItem)


        for group, data in orderedData.iteritems(): 
            # print group
            groupRootItem = self.ui.contentWidget.set_group(self.rootAllItem, group)

            # expand 
            self.ui.contentWidget.expand_tree(groupRootItem)
            
            # data 
            self.ui.contentWidget.set_data(itemType=ui.ItemLevel.groupType, item=groupRootItem)

            for dataType, assets in data.iteritems(): 
                # print '\t', dataType
                # hidden data types 
                showType = True if not dataType in display.DataType.hiddenDataTypes else False


                if showType: 
                    typeRootItem = self.ui.contentWidget.set_type(groupRootItem, dataType)

                    # data 
                    self.ui.contentWidget.set_data(itemType=ui.ItemLevel.dataType, item=typeRootItem, dataType=dataType)

                    self.ui.contentWidget.expand_tree(typeRootItem)
                    for asset in assets: 
                        # print '\t\t', asset
                        elementRootItem = self.ui.contentWidget.set_element(typeRootItem, asset)
                        
                        # add widgets 
                        self.set_data_display(elementRootItem, dataType)
                        self.ui.contentWidget.set_data(itemType=ui.ItemLevel.element, asset=asset, item=elementRootItem, dataType=dataType)
                        
                        # set button modules 
                        self.set_element_button(elementRootItem, dataType, display.ButtonCommand.build)
                        self.set_element_button(elementRootItem, dataType, display.ButtonCommand.refresh)
                        self.set_element_button(elementRootItem, dataType, display.ButtonCommand.remove)
                        self.set_status(elementRootItem, dataType, asset)

                # self.sum_status(typeRootItem)
            # self.sum_status(groupRootItem)


        self.set_column_width()

    def set_column_width(self): 
        self.ui.contentWidget.adjust_element_column(200)
        self.ui.contentWidget.adjust_build_column(100)
        self.ui.contentWidget.adjust_refresh_column(100)
        self.ui.contentWidget.adjust_remove_column(100)
        self.ui.contentWidget.adjust_dataType_column(100)
        self.ui.contentWidget.adjust_status_column(70)


    def action_call(self, parentItem, actionType): 
        """ called command from button and context menu """ 
        elementItems = self.ui.contentWidget.list_type_items(parentItem, ui.ItemLevel.element)

        for item in elementItems: 
            data = self.ui.contentWidget.get_data(item)
            asset = data['asset']
            dataType = data['dataType'] 
            itemType = data['itemType']
            
            func = self.modules.get(dataType)
            
            if actionType == display.ButtonCommand.build: 
                statusDict = func.build(asset, self.reg, self.scene.copy())

            if actionType == display.ButtonCommand.refresh: 
                statusDict = func.refresh(asset, self.reg, self.scene.copy())

            if actionType == display.ButtonCommand.remove: 
                statusDict = func.remove(asset, self.reg, self.scene.copy())

            if statusDict: 
                self.set_status_item(item, statusDict)


    def set_status(self, rootItem, dataType, asset): 
        """ check status for item """ 
        # default label 
        text = display.Status.failed
        iconPath = display.Icon.failed
        if dataType in self.modules.keys(): 
            func = self.modules.get(dataType)
            if hasattr(func, 'status'): 
                statusDict = func.status(asset, self.reg, self.scene)
                text = statusDict.get('status')
                iconPath = statusDict.get('icon')

        self.ui.contentWidget.set_status(rootItem, text, iconPath)

    def sum_status(self, rootItem): 
        # list element items below 
        items = self.ui.contentWidget.list_type_items(rootItem, ui.ItemLevel.element)
        # get statuses 
        statuses = [self.ui.contentWidget.get_status(a) for a in items]
        statusSum = display.status_sum(statuses)
        statusDict = display.status_dict(statusSum, '')
        print rootItem.text(0), statuses, statusSum, statusDict
        self.set_status_item(rootItem, statusDict)


    def set_status_item(self, rootItem, statusDict): 
        """ set status for item """ 
        text = statusDict.get('status')
        iconPath = statusDict.get('icon')
        self.ui.contentWidget.set_status(rootItem, text, iconPath)
        QtWidgets.QApplication.processEvents()

    def set_data_display(self, rootItem, dataType): 
        iconPath = ''
        if dataType in self.modules.keys(): 
            func = self.modules.get(dataType)
            if hasattr(func, 'DataIcon'): 
                iconPath = func.DataIcon.data
        self.ui.contentWidget.add_data_type(rootItem, dataType, iconPath)

    
    def set_element_button(self, rootItem, dataType, actionType): 
        """ build button """ 
        buttonLabel = ui.Button.noModule
        iconPath = ''
        enableButton = False 

        if dataType in self.modules.keys(): 
            buttonLabel = ui.Button.noAttr
            func = self.modules.get(dataType)

            if actionType == display.ButtonCommand.build: 
                buildButton = self.ui.contentWidget.add_build_button(rootItem)
                if hasattr(func, 'DataIcon'): 
                    iconPath = func.DataIcon.build
                if hasattr(func, 'build'): 
                    enableButton = True
                    buttonLabel = func.Button.build

            if actionType == display.ButtonCommand.refresh: 
                buildButton = self.ui.contentWidget.add_refresh_button(rootItem)
                if hasattr(func, 'DataIcon'): 
                    iconPath = func.DataIcon.refresh
                if hasattr(func, 'refresh'): 
                    enableButton = True
                    buttonLabel = func.Button.refresh

            if actionType == display.ButtonCommand.remove: 
                buildButton = self.ui.contentWidget.add_remove_button(rootItem)
                if hasattr(func, 'DataIcon'): 
                    iconPath = func.DataIcon.remove
                if hasattr(func, 'remove'): 
                    enableButton = True
                    buttonLabel = func.Button.remove

        if iconPath: 
            buildButton.add_icon(iconPath)
        
        buildButton.button.setText(buttonLabel)
        buildButton.button.clicked.connect(partial(self.action_call, rootItem, actionType))
        buildButton.button.setEnabled(enableButton)


    def set_build_all(self, rootItem, actionType): 
        """ build all button """ 
        buttonLabel = ''
        iconPath = ''
        color = []
        buildButton = None

        if actionType == display.ButtonCommand.build: 
            buildButton = self.ui.contentWidget.add_build_button(rootItem)
            buttonLabel = display.ButtonCommand.buildAll
            iconPath = display.ButtonIcon.build
            color = display.ButtonColor.buildAll

        if actionType == display.ButtonCommand.refresh: 
            buildButton = self.ui.contentWidget.add_refresh_button(rootItem)
            buttonLabel = display.ButtonCommand.refreshAll
            iconPath = display.ButtonIcon.refresh
            color = display.ButtonColor.refreshAll

        if actionType == display.ButtonCommand.remove: 
            buildButton = self.ui.contentWidget.add_remove_button(rootItem)
            buttonLabel = display.ButtonCommand.removeAll
            iconPath = display.ButtonIcon.remove
            color = display.ButtonColor.removeAll

        if buildButton: 
            buildButton.button.clicked.connect(partial(self.action_call, self.rootAllItem, actionType))
        
        if buttonLabel: 
            buildButton.button.setText(buttonLabel)
        if iconPath: 
            buildButton.add_icon(iconPath)
        if color: 
            buildButton.button.setStyleSheet("background-color:rgb(%s, %s, %s)" % (color[0], color[1], color[2]))


def show(): 
    start = datetime.now()
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = BuilderCore(maya_win.getMayaWindow())
        myApp.show()
        print 'Run in %s' % (datetime.now() - start)
        return myApp
