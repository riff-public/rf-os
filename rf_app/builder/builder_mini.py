# This is a mini builder for hounidi 
# not full functinality. Build only set and character / props 

uiName = 'HoudiniBuilder'
from PySide2 import QtWidgets 
from PySide2 import QtGui 
from PySide2 import QtCore 
from rf_utils.context import context_info 
from rf_utils import project_info
from rf_utils import register_shot
from rf_utils.pipeline import shot_data
from rf_utils import file_utils
from rf_hou import asm_hou
reload(asm_hou) 
from rf_utils.sg import sg_process


class App(QtWidgets.QMainWindow):
    """docstring for Ui"""
    def __init__(self, parent=None):
        super(App, self).__init__(parent=parent)
        self.ui = Ui(parent)
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)

        # title
        self.setWindowTitle('Builder')
        self.resize(300, 400)  
        self.init_signals()
        self.fetch() 

    def init_signals(self): 
        self.ui.button.clicked.connect(self.build)

    def fetch(self): 
        self.model = Model()
        self.init_ui()

    def build(self): 
        items = self.ui.asset_list.selectedItems()
        bb = self.ui.bounding_cb.isChecked()
        loadmode = 4 if bb else 1

        for item in items: 
            asset = str(item.text())
            cache_type = self.model.reg.get.check_type(asset)
            print(asset, cache_type)
            step = None
            abc_path = self.model.reg.get.cache(asset, step=step, hero=True)
            print(abc_path)
            # continue
            if cache_type == 'abc_cache': 
                print('abc_cache')
                ws_data_path = shot_data.get_shot_data_path(entity=self.model.scene)
                ws_data = file_utils.ymlLoader(ws_data_path)
                offset_value = ws_data['cameraDistanceFromOrigin']
                loc, abc = asm_hou.create_asset_node(asset, abc_path, offset_value)

            if cache_type == 'set': 
                asm_hou.build(self.model.scene, loadmode)

            if cache_type == 'camera': 
                asm_hou.craete_camera_node(asset, abc_path)
                duration = self.model.get_duration()
                start_frame = self.model.project.render.start_frame()
                end_frame = start_frame + duration - 1 
                print(start_frame, end_frame)
                asm_hou.set_frame_range(start_frame, end_frame)

    def init_ui(self): 
        # asset list 
        self.ui.asset_list.clear()

        for asset in self.model.asset_list: 
            print asset 
            cache_type = self.model.reg.get.check_type(asset)
            if cache_type in ['abc_cache', 'set', 'camera']: 
                self.ui.asset_list.addItem(asset)


class Ui(QtWidgets.QWidget):
    """docstring for Ui"""
    def __init__(self, parent=None):
        super(Ui, self).__init__(parent=parent)
        self.layout = QtWidgets.QVBoxLayout()
        self.asset_list = QtWidgets.QListWidget()
        self.asset_list.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.bounding_cb = QtWidgets.QCheckBox('Bounding Box')
        self.button = QtWidgets.QPushButton('Build')
        self.layout.addWidget(self.asset_list)
        self.layout.addWidget(self.bounding_cb)
        self.layout.addWidget(self.button)
        self.setLayout(self.layout)


class Model(object):
    """docstring for Model"""
    def __init__(self):
        super(Model, self).__init__()
        self.read_scene()
        
    def read_scene(self): 
        self.scene = context_info.ContextPathInfo()
        self.project = project_info.ProjectInfo(self.scene.project)
        self.reg = register_shot.Register(self.scene)
        self.asset_list = self.reg.get.asset_list()

    def get_duration(self):
        filters = [['project.Project.name', 'is', self.scene.project], ['code', 'is', self.scene.name]]
        fields = ['sg_working_duration', 'sg_include_prepost_roll']
        shot_entity = sg_process.sg.find_one('Shot', filters, fields)
        return shot_entity.get('sg_working_duration')

        
        
        
def show(): 
    deleteUi(uiName)
    app = App(getHoudiniMainWindow())
    app.show()


def getHoudiniMainWindow():
    from PySide2 import QtGui
    app = QtWidgets.QApplication.instance()
    if app:
        parent = app.topLevelAt(QtGui.QCursor().pos())
        if parent:
            return parent
        else:
            # root = app.desktop()
            ws = [w for w in app.topLevelWidgets() if w.windowTitle().count('Houdini') and w.windowType().name == 'Window']
            if ws:
                return ws[0] 


def deleteUi(uiName):
    mayaWindow = getHoudiniMainWindow()
    children = mayaWindow.findChildren(QtWidgets.QMainWindow)

    for child in children:
        if child.objectName() == uiName:
            child.deleteLater()