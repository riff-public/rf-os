import sys
import os
from collections import OrderedDict
import logging
logger = logging.getLogger(__name__)

from rf_utils.context import context_info 

def setup_context(sg, shotDict): 
	""" setup context from given entity dictionary """ 
	# Ex data 
	# {u'code': u'pr_ep01_PastIntro_Shot002', u'sg_shot_type': u'Shot', u'project': {u'type': u'Project', u'id': 134, u'name': u'projectName'}, u'sg_sequence_code': u'PastIntro', u'sg_episode': {u'type': u'Scene', u'id': 607, u'name': u'ep01'}, u'type': u'Shot', u'id': 8634, u'sg_shortcode': u'Shot002'}
	contextType = {'Asset': 'asset', 'Shot': 'scene'}
	entityType = contextType.get(shotDict['type'])
	context = context_info.Context()
	context.use_sg(sg, project=shotDict['project']['name'], entityType=entityType, entityId=shotDict['id'])
	scene = context_info.ContextPathInfo(context=context)
	return scene 


def filter_cache(entity, reg, assets): 
    from rf_utils.pipeline import cache_list
    # fitler cache list 
    cache = cache_list.CacheList(entity)
    heroList = cache.list_keys()

    filterAssets = []

    # if heroList: 
    for asset in assets: 
        fileType = reg.get.assetData[asset]['type']
        namespace = reg.get.assetData[asset]['namespace']
        exportNs = reg.get.assetData[asset].get('exportNamespace')
        checkName = exportNs if exportNs else namespace 

        if fileType in ['abc_cache', 'abc_tech_cache', 'yeti_cache']: 
            if checkName in heroList: 
                filterAssets.append(asset)

        else: 
            filterAssets.append(asset)
    # else: 
    #     filterAssets = assets

    return filterAssets

def get_asset_list(scene, reg, filterData=True, preferredOrder=[]): 
    """ get ordered asset list """ 

    types = reg.get.list_types(preferredOrder=preferredOrder)
    assets = []
    for typ in types: 
        assetList = reg.get.asset_list(type=typ)
        assets += assetList if assetList else None 

    if filterData: 
        assets = filter_cache(scene, reg, assets)
    
    return assets 


def regroup_data(reg, assets, nameIndex=1): 
    """ regroup data by name and type """ 
    # assets is a list from callback reg.get.asset_list()
    # arthur
    # abc_cache
    #     arthur_001
    # abc_tech_cache
    #     arthur_001_Tech
    # yeti_cache
    #     arthur_001_EyeBrow
    #     arthur_001_Face
    #     arthur_001_Foot
    #     arthur_001_Hair
    #     arthur_001_HairHightLight
    #     arthur_001_Hand
    #     arthur_001_Neck

    newData = OrderedDict()
    # nameIndex = 1 # use first element of the name -> arthur_001 -> arthur
    for asset in assets: 
        nameGrouping = ('_').join(asset.split('_')[0: nameIndex])
        
        fileType = reg.get.assetData[asset]['type']
        step = reg.get.assetData[asset]['step']
        cache = reg.get.cache(asset, step=step, hero=True)
        data = [reg, asset]

        # reorganized data 
        if nameGrouping in newData.keys(): 
            if fileType in newData[nameGrouping].keys(): 
                elementData = newData[nameGrouping][fileType]
                elementData.append(asset)
            else: 
                newData[nameGrouping][fileType] = [asset]
        else: 
            temp = OrderedDict()
            temp[fileType] = [asset]
            newData[nameGrouping] = temp


    return newData
