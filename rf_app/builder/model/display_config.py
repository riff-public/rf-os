from rf_utils import icon 

class Status: 
    latest = 'Latest'
    obsolete = 'Old version'
    ready = 'Ready'
    notReady = 'No file'
    failed = 'Failed'
    mix = 'Check'

class Icon: 
	latest = icon.sgAprv
	obsolete = icon.needFix
	ready = icon.sgRdy
	notReady = icon.no
	failed = icon.no
	mix = icon.needFix
	na = icon.sgNa

class StatusInfo: 
	statusMap = {
						Status.latest: Icon.latest, 
						Status.obsolete: Icon.obsolete, 
						Status.ready: Icon.ready, 
						Status.notReady: Icon.notReady, 
						Status.failed: Icon.failed, 
						Status.mix: Icon.mix
				}

class DataType: 
	hiddenDataTypes = ['abc_tech_cache']

class GroupLabel: 
	root = 'All'

class ButtonCommand: 
	build = 'Build'
	remove = 'Remove'
	refresh = 'Refresh'
	buildAll = 'Build all'
	removeAll = 'Remove all'
	refreshAll = 'Refresh all'

class ButtonColor: 
	buildAll = [120, 180, 120]
	removeAll = [180, 120, 120]
	refreshAll = [120, 160, 180]

class ButtonIcon: 
    data = icon.Tool.abc16
    build = icon.Tool.build16
    refresh = icon.Tool.refresh16
    remove = icon.Tool.remove16


def status_sum(statuses): 
	""" sum status """ 
	if statuses: 
		if len(list(set(statuses))) == 1: 
			return statuses[0]
		else: 
			return Status.mix


def status_dict(status='', path=''): 
	""" status dict format """ 
	iconPath = StatusInfo.statusMap.get(status, Icon.na)
	return {'status': status, 'path': path, 'icon': iconPath}


def status_format(status='', path='', iconPath=''): 
	""" status dict format """ 
	return {'status': status, 'path': path, 'icon': iconPath}
