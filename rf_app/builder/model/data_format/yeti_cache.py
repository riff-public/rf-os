import os
import sys 
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils import register_entity
reload(register_entity)
from rf_utils import register_shot
from rf_maya.lib import shot_lib
reload(shot_lib)
from rftool.utils import yeti_lib
reload(yeti_lib)
from rf_utils.context import context_info
from rf_utils import icon 
reload(icon)
from rftool.utils import pipeline_utils
from rf_utils import file_utils
from rf_app.builder.model import display_config as display
reload(display)

class Button: 
    build = 'Build yeti'
    refresh = 'Reload'
    remove = 'Remove'


class DataIcon: 
    data = icon.Tool.yeti16
    build = icon.Tool.build16
    refresh = icon.Tool.refresh16
    remove = icon.Tool.remove16


def build(asset, register, entity): 
    """ build function for abc_cache 
    asset = namespace key 
    register = register object 
    entity = scene entity object 
    """ 
    node = asset
    step = ''
    cacheType = register.get.check_type(asset)
    cachePath = register.get.cache(asset, step=step, hero=True)
    cachePath = cachePath if cachePath else register.get.maya(asset, step=step, hero=True)
    data = register.get.asset(asset)
    namespace = data.get(register_shot.Config.namespace)
    node = data.get('node')
    assetDescription = register.get.custom_key(asset, dataType='description')
    assetReg = register_entity.RegisterInfo(assetDescription)

    # import cache 
    yetiNode = yeti_lib.import_cache(node, cachePath, namespace=namespace, override=False)
    # apply shade 
    step = 'ldv' 
    if entity.step in ['techanim', 'anim']: 
        step = 'rig'
    ref_shade(entity.projectInfo, namespace, step)
    
    # setup display yeti  
    displayFile = assetReg.get.groom_yeti_display().get('heroFile')
    if displayFile: 
        yetiDisplayData = file_utils.ymlLoader(displayFile) if os.path.exists(displayFile) else dict()
        yeti_lib.set_display(yetiDisplayData, node, namespace)

    pipeline_utils.scene_group()
    logger.debug('Built yeti node "%s" successfully' % yetiNode)

    return status(asset, register, entity)


def remove(asset, register, entity): 
    """ remove camera """ 
    data = register.get.asset(asset)
    namespace = data.get(register_shot.Config.namespace)
    node = data.get('node')
    yetiNode = yeti_lib.remove_cache(node, namespace)
    remove_namespace(namespace)

    logger.debug('Remove yeti node "%s" successfully' % yetiNode)

    return status(asset, register, entity)


def refresh(asset, register, entity): 
    """ reload camera """ 
    import maya.cmds as mc 
    data = register.get.asset(asset)
    namespace = data.get(register_shot.Config.namespace)
    node = data.get('node')
    yetiNode = '%s:%s' % (namespace, node)

    if mc.objExists(yetiNode): 
        remove(asset, register, entity)
        return build(asset, register, entity)
    return status(asset, register, entity)


def status(asset, register, entity): 
    import maya.cmds as mc 
    step = ''
    cachePath = register.get.cache(asset, step=step, hero=True)
    mayaPath = register.get.maya(asset, step=step, hero=True)
    data = register.get.asset(asset)
    sceneRefs = shot_lib.get_all_asset_references()

    if cachePath in [mc.getAttr('%s.cacheFileName' % a) for a in mc.ls(type='pgYetiMaya')]:
        return display.status_dict(display.Status.latest, cachePath)
    else: 
        if cache_exists(asset, cachePath): # check folder cache name match key only for faster check
            return display.status_dict(display.Status.ready, cachePath)
        else: 
            return display.status_dict(display.Status.notReady, cachePath)


def remove_namespace(namespace):
    import maya.cmds as mc 
    if not mc.namespaceInfo(namespace, ls=True):
        mc.namespace(rm=namespace)

def ref_shade(project, geoNamespace, step='ldv'):
    from rf_maya.rftool.shade import shade_utils
    from rf_utils.pipeline import asset_tag
    import maya.cmds as mc 

    tagGrp = '%s:%s' % (geoNamespace, project.asset.geo_grp())
    tag = asset_tag.AssetTag(tagGrp)

    if mc.objExists(tagGrp) and mc.referenceQuery(tagGrp, inr=True):
        mtrDict = shade_utils.list_mtr_files(tagGrp, step)
        refPath = mc.getAttr('%s.refPath' % tagGrp)
        name = context_info.ContextPathInfo(path=refPath).name

        look = tag.get_look() or 'main'
        shadeFile = mtrDict.get(look)

        if shadeFile:
            shadeNamespace = '%s_%s%sMtr' % (name, step, look.capitalize())
            try: 
                shade_utils.apply_ref_shade(shadeFile, shadeNamespace, geoNamespace)
            except Exception as e: 
                print e
        else:
            pass



def sequence_exists(path): 
    """ check if sequence in this format exists """ 
    # scm_act1_q0090b_s0320_Jacobus_001_EyeBrow.hero.%04d.fur
    # This method is more reliable but consume time 
    yetiPadding = '%04d'
    checkPattern = os.path.basename(path).split(yetiPadding)[0]
    basename, ext = os.path.splitext(path)
    dirname = os.path.dirname(path)
    files = file_utils.list_file(dirname)
    files = [a for a in files if ext in a]
    valids = []

    for file in files: 
        cachePath = '%s/%s' % (dirname, file)
        if checkPattern in file and ext in file: 
            valids.append(cachePath)

    if len(valids) == len(files): 
        return True
    if valids: 
        return True 
    return False 

def cache_exists(asset, path): 
    """ This method check only cache folder exists """ 
    # this is much faster but less reliable 
    dirname = os.path.dirname(path)
    basePath, cacheDir = os.path.split(dirname)
    if asset == cacheDir: 
        return True 
    return False 

