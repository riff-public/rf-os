import os
import sys 
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils import register_entity
reload(register_entity)
from rf_utils import register_shot
from rf_maya.lib import shot_lib
reload(shot_lib)
from rftool.utils import yeti_lib
reload(yeti_lib)
from rf_utils.context import context_info
from rf_utils import icon 
reload(icon)
from rftool.utils import pipeline_utils
from rf_utils import file_utils
from rf_app.builder.model import display_config as display
reload(display)
from rf_app.asm import asm_lib 
reload(asm_lib)

mapDataType = 'set'

class Button: 
    build = 'Build set'
    refresh = 'Reload'
    remove = 'Remove'


class DataIcon: 
    data = icon.Tool.set16
    build = icon.Tool.build16
    refresh = icon.Tool.refresh16
    remove = icon.Tool.remove16


def build(asset, register, entity): 
    """ build function for abc_cache 
    asset = namespace key 
    register = register object 
    entity = scene entity object 
    """ 
    node = asset
    step = ''
    data = register.get.asset(asset)

    setOverride = register.get.custom_data(asset, step=step, dataType=register_shot.Config.setOverride, hero=True)
    setShot = register.get.custom_data(asset, step=step, dataType=register_shot.Config.setShot, hero=True)
    setDescription = register.get.custom_key(asset, step=step, dataType=register_shot.Config.setDescription)
    mayaFile = register.get.maya(asset, step=step, hero=True)
    # shot_lib.import_reference(mayaFile, asset)
    shot_lib.build_set(asset, setDescription, setOverride)
    pipeline_utils.scene_group()


    return status(asset, register, entity)


def remove(asset, register, entity): 
    """ remove camera """ 
    asm_lib.mc.select(asset)
    cmd = asm_lib.UiCmd()
    cmd.remove_set()

    return status(asset, register, entity)


def refresh(asset, register, entity): 
    """ reload camera """ 
    return build(asset, register, entity)


def status(asset, register, entity): 
    """ status of set """
    step = ''
    root = asset 
    setOverride = register.get.custom_data(asset, step=step, dataType=register_shot.Config.setOverride, hero=True)
    setDescription = register.get.custom_key(asset, step=step, dataType=register_shot.Config.setDescription)

    loc = asm_lib.MayaRepresentation(root)
    # check if set is in the scene 
    if loc.is_asm(): 
        asmHasNewFile = asm_lib.is_file_new(root, setDescription)
        overrideHaseNewFile = asm_lib.is_override_new(root, setOverride)
        setHasNewFile = any([asmHasNewFile, overrideHaseNewFile])

        # if no new file 
        if not setHasNewFile: 
            return display.status_dict(display.Status.latest, setDescription)
        # if new file 
        else: 
            return display.status_dict(display.Status.obsolete, setDescription)

    else: 
        if os.path.exists(setDescription): 
            return display.status_dict(display.Status.ready, setDescription)
        else: 
            return display.status_dict(display.Status.notReady, setDescription)

