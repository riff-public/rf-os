# import all modules in maya_lib 
import os
import sys 
import glob
from rf_app.export.asset_lib import *
from collections import OrderedDict


def get_module(): 
    modules = glob.glob(os.path.dirname(__file__)+"/*.py")
    return [ os.path.basename(f)[:-3] for f in modules if os.path.isfile(f) and not f.endswith('__init__.py')]

def get_module_data(): 
    mods = OrderedDict()
    allModules = get_module()
    
    for name in allModules: 
        func = __import__(name, globals(), locals(), [], -1)
        reload(func)
        if hasattr(func, 'mapDataType'): 
            name = func.mapDataType # incase py name and data type not match
        mods[name] = func

    return mods

def get_func(name): 
    func = __import__(name, globals(), locals(), [], -1)
    return func


# __all__ = [ os.path.basename(f)[:-3] for f in modules if os.path.isfile(f) and not f.endswith('__init__.py')]

# sceneModules = OrderedDict()

# for name in __all__: 
#     func = __import__(name, globals(), locals(), [], -1)
#     reload(func)
#     assetModules[name] = func
