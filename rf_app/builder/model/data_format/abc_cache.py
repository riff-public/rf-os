import os
import sys 
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_utils import register_entity
reload(register_entity)
from rf_utils import register_shot
from rf_maya.lib import shot_lib
reload(shot_lib)
from rf_utils.context import context_info
from rf_utils import icon 
reload(icon)
from rftool.utils import pipeline_utils
from rf_app.builder.model import display_config as display
reload(display)

class Button: 
    build = 'Build cache'
    refresh = 'Reload'
    remove = 'Remove'


class DataIcon: 
    data = icon.Tool.abc16
    build = icon.Tool.build16
    refresh = icon.Tool.refresh16
    remove = icon.Tool.remove16


def build(asset, register, entity): 
    """ build function for abc_cache 
    asset = namespace key 
    register = register object 
    entity = scene entity object 
    """ 
    step = ''
    cachePath = register.get.cache(asset, step=step, hero=True)
    cachePath = cachePath if cachePath else register.get.maya(asset, step=step, hero=True)
    
    # asset register 
    data = register.get.asset(asset)
    namespace = data.get(register_shot.Config.namespace)
    
    assetDescription = register.get.custom_key(asset, dataType='description')
    assetReg = register_entity.RegisterInfo(assetDescription)

    if not assetDescription: 
        logger.error('No asset description found on "%s". Recache and make sure to resolve qc "add_asset_tag".' % asset)
        return 
    # asset entity 
    assetPath = assetReg.get.hero_path()
    assetEntity = context_info.ContextPathInfo(path=assetPath) 
    logger.debug('Asset path: %s' % assetPath)

    shot_lib.reference_abc(namespace, cachePath, rebuild=False)
    
    step = 'ldv' 
    if entity.step in ['techanim', 'anim']: 
        step = 'rig'
    ref_shade(entity.projectInfo, namespace, step)
    
    # hidden geo 
    hiddenGeos = pipeline_utils.read_hiddenGeo_data(assetEntity)
    shot_lib.hidden_geo(asset, hiddenGeos)

    pipeline_utils.scene_group()

    return status(asset, register, entity)


def remove(asset, register, entity): 
    """ remove camera """ 
    data = register.get.asset(asset)
    namespace = data.get(register_shot.Config.namespace)
    shot_lib.remove_abc(namespace)
    return status(asset, register, entity)


def refresh(asset, register, entity): 
    """ reload camera """ 
    data = register.get.asset(asset)
    namespace = data.get(register_shot.Config.namespace)
    shot_lib.reload_reference(namespace)
    return status(asset, register, entity)


def status(asset, register, entity): 
    step = ''
    cachePath = register.get.cache(asset, step=step, hero=True)
    mayaPath = register.get.maya(asset, step=step, hero=True)
    data = register.get.asset(asset)
    sceneRefs = shot_lib.get_all_asset_references()

    if cachePath in sceneRefs or mayaPath in sceneRefs:
        # return {'status': display.Status.latest, 'path': cachePath, 'icon': display.Icon.latest}
        return display.status_dict(display.Status.latest, cachePath)
    else: 
        assetInPath = [a for a in sceneRefs if asset in a]
        if assetInPath: 
            # return {'status': display.Status.obsolete, 'path': assetInPath[0], 'icon': display.Icon.obsolete}
            return display.status_dict(display.Status.obsolete, assetInPath[0])
        else: 
            # check if file exists 
            if os.path.exists(cachePath): 
                # return {'status': display.Status.ready, 'path': assetInPath, 'icon': display.Icon.ready}
                return display.status_dict(display.Status.ready, assetInPath)
            else: 
                # return {'status': display.Status.notReady, 'path': assetInPath, 'icon': display.Icon.notReady}
                return display.status_dict(display.Status.notReady, assetInPath)



def ref_shade(project, geoNamespace, step='ldv'):
    from rf_maya.rftool.shade import shade_utils
    from rf_utils.pipeline import asset_tag
    import maya.cmds as mc 

    tagGrp = '%s:%s' % (geoNamespace, project.asset.geo_grp())
    tag = asset_tag.AssetTag(tagGrp)

    if mc.objExists(tagGrp) and mc.referenceQuery(tagGrp, inr=True):
        mtrDict = shade_utils.list_mtr_files(tagGrp, step)
        refPath = mc.getAttr('%s.refPath' % tagGrp)
        name = context_info.ContextPathInfo(path=refPath).name

        look = tag.get_look() or 'main'
        shadeFile = mtrDict.get(look)

        if shadeFile:
            shadeNamespace = '%s_%s%sMtr' % (name, step, look.capitalize())
            try: 
                shade_utils.apply_ref_shade(shadeFile, shadeNamespace, geoNamespace)
            except Exception as e: 
                print e
        else:
            pass
