import os
import sys 
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_utils import register_entity
reload(register_entity)
from rf_utils import register_shot
from rf_maya.lib import shot_lib
reload(shot_lib)
from rf_utils.context import context_info
from rf_utils import icon 
reload(icon)
from rftool.utils import pipeline_utils
from rf_app.builder.model import display_config as display
reload(display)
from rf_app.asm import asm_lib
reload(asm_lib)

class Button: 
    build = 'Build shotdress'
    refresh = 'Reload'
    remove = 'Remove'


class DataIcon: 
    data = icon.Tool.shotdress16
    build = icon.Tool.build16
    refresh = icon.Tool.refresh16
    remove = icon.Tool.remove16


def build(asset, register, entity): 
    """ build function for abc_cache 
    asset = namespace key 
    register = register object 
    entity = scene entity object 
    """ 
    step = ''
    # asset register 
    shotdress = register.get.custom_data(asset, step=step, dataType=register_shot.Config.shotDress, hero=True)
    asm_lib.paste('ShotDress_Grp', shotdress, False)
    asm_lib.build('ShotDress_Grp')
    asm_lib.remove_hidden_child('ShotDress_Grp')
    pipeline_utils.scene_group()

    return status(asset, register, entity)


def remove(asset, register, entity): 
    """ remove camera """ 
    asm_lib.Asm('ShotDress_Grp').remove_root()
    return status(asset, register, entity)


def refresh(asset, register, entity): 
    """ reload camera """ 
    return build(asset, register, entity)


def status(asset, register, entity): 
    import maya.cmds as mc 
    step = ''

    cachePath = register.get.custom_data(asset, step=step, dataType=register_shot.Config.shotDress, hero=True)
    scene = context_info.ContextPathInfo(path=cachePath, schemaKey='cachePath')

    if mc.objExists(scene.projectInfo.asset.shotdress_grp()): 
        loc = asm_lib.MayaRepresentation(scene.projectInfo.asset.shotdress_grp())
        if cachePath == loc.asm_data: 
            return display.status_dict(display.Status.latest, cachePath)
    
    if cachePath: 
        if os.path.exists(cachePath): 
            return display.status_dict(display.Status.ready, cachePath)
        else: 
            return display.status_dict(display.Status.notReady, cachePath)
    return display.status_dict(display.Status.notReady, cachePath)
