import os
import sys 
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_utils import register_entity
reload(register_entity)
from rf_utils import register_shot
from rf_maya.lib import shot_lib
reload(shot_lib)
from rf_utils.context import context_info
from rf_utils import icon 
reload(icon)
from rftool.utils import pipeline_utils
from rf_app.builder.model import display_config as display
reload(display)
from rftool.utils import instance_utils
reload(instance_utils)


class Button: 
    build = 'Build cache'
    refresh = 'Reload'
    remove = 'Remove'


class DataIcon: 
    data = icon.Tool.abc16
    build = icon.Tool.build16
    refresh = icon.Tool.refresh16
    remove = icon.Tool.remove16


def build(asset, register, entity): 
    """ build function for abc_cache 
    asset = namespace key 
    register = register object 
    entity = scene entity object 
    """ 
    step = ''
    cachePath = register.get.cache(asset, step=step, hero=True)
    cachePath = cachePath if cachePath else register.get.maya(asset, step=step, hero=True)

    # asset register 
    shot_lib.import_reference(cachePath, asset)
    pipeline_utils.scene_group()

    return status(asset, register, entity)


def remove(asset, register, entity): 
    """ remove camera """ 
    shot_lib.remove_import_group(asset)
    return status(asset, register, entity)


def refresh(asset, register, entity): 
    """ reload camera """ 
    return build(asset, register, entity)


def status(asset, register, entity): 
    step = ''
    cachePath = register.get.cache(asset, step=step, hero=True)
    cachePath = cachePath if cachePath else register.get.maya(asset, step=step, hero=True)
    scene = context_info.ContextPathInfo(path=cachePath, schemaKey='cachePath')
    
    if mc.objExists(scene.projectInfo.asset.export_grp()): 
        return display.status_dict(display.Status.latest, cachePath)

    else: 
        if cachePath: 
            if os.path.exists(cachePath): 
                return display.status_dict(display.Status.latest, cachePath)
            else: 
                return display.status_dict(display.Status.latest, cachePath)
        return display.status_dict(display.Status.latest, cachePath)
