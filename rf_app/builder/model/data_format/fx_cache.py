import os
import sys 
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_utils import register_entity
reload(register_entity)
from rf_utils import register_shot
from rf_maya.lib import shot_lib
reload(shot_lib)
from rf_utils.context import context_info
from rf_utils import icon 
reload(icon)
from rftool.utils import pipeline_utils
from rf_app.builder.model import display_config as display
reload(display)

class Button: 
    build = 'Build fx'
    refresh = 'Reload'
    remove = 'Remove'


class DataIcon: 
    data = icon.Tool.fx16
    build = icon.Tool.build16
    refresh = icon.Tool.refresh16
    remove = icon.Tool.remove16


def build(asset, register, entity): 
    """ build function for abc_cache 
    asset = namespace key 
    register = register object 
    entity = scene entity object 
    """ 
    step = ''
    cachePath = register.get.cache(asset, step=step, hero=True)
    cachePath = cachePath if cachePath else register.get.maya(asset, step=step, hero=True)
    
    # asset register 
    data = register.get.asset(asset)
    namespace = data.get(register_shot.Config.namespace)

    cachePath = register.get.maya(asset, step=step, hero=True)
    shot_lib.reference_abc(namespace, cachePath, rebuild=False)
    pipeline_utils.scene_group()

    return status(asset, register, entity)


def remove(asset, register, entity): 
    """ remove camera """ 
    data = register.get.asset(asset)
    namespace = data.get(register_shot.Config.namespace)
    shot_lib.remove_abc(namespace)
    return status(asset, register, entity)


def refresh(asset, register, entity): 
    """ reload camera """ 
    data = register.get.asset(asset)
    namespace = data.get(register_shot.Config.namespace)
    shot_lib.reload_reference(namespace)
    return status(asset, register, entity)


def status(asset, register, entity): 
    step = ''
    cachePath = register.get.cache(asset, step=step, hero=True)
    mayaPath = register.get.maya(asset, step=step, hero=True)
    data = register.get.asset(asset)
    sceneRefs = shot_lib.get_all_asset_references()

    cachePaths = [cachePath, mayaPath]

    if cachePath in sceneRefs or mayaPath in sceneRefs:
        return display.status_dict(display.Status.latest, cachePath)
    else: 
        assetInPath = [a for a in sceneRefs if asset in a]
        if assetInPath: 
            return display.status_dict(display.Status.obsolete, assetInPath[0])
        else: 
            # check if file exists 
            if cachePath: 
                if os.path.exists(cachePath): 
                    return display.status_dict(display.Status.ready, assetInPath)
                else: 
                    return display.status_dict(display.Status.notReady, assetInPath)
            if mayaPath: 
                if os.path.exists(mayaPath): 
                    return display.status_dict(display.Status.ready, mayaPath)
                else: 
                    return display.status_dict(display.Status.notReady, mayaPath)
            return display.status_dict(display.Status.notReady, mayaPath)
