import os
import sys 
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_utils import register_entity
reload(register_entity)
from rf_utils import register_shot
from rf_maya.lib import shot_lib
reload(shot_lib)
from rf_utils.context import context_info
from rf_utils import icon 
reload(icon)
from rftool.utils import pipeline_utils
from rf_app.builder.model import display_config as display
reload(display)
from rftool.utils import instance_utils
reload(instance_utils)


class Button: 
    build = 'Build instdress'
    refresh = 'Reload'
    remove = 'Remove'


class DataIcon: 
    data = icon.Tool.instdress16
    build = icon.Tool.build16
    refresh = icon.Tool.refresh16
    remove = icon.Tool.remove16


def build(asset, register, entity): 
    """ build function for abc_cache 
    asset = namespace key 
    register = register object 
    entity = scene entity object 
    """ 
    step = ''
    
    # asset register 
    instdressFile = register.get.custom_data(asset, step=step, dataType=register_shot.Config.maya, hero=True)
    instance_utils.build_instance_shotDress(instdressFile, entity)
    pipeline_utils.scene_group()

    return status(asset, register, entity)


def remove(asset, register, entity): 
    """ remove camera """ 
    instance_utils.remove_instance_shotDress()
    return status(asset, register, entity)


def refresh(asset, register, entity): 
    """ reload camera """ 
    return build(asset, register, entity)


def status(asset, register, entity): 
    import maya.cmds as mc 
    step = ''

    mayaPath = register.get.maya(asset, step=step, hero=True)
    scene = context_info.ContextPathInfo(path=mayaPath, schemaKey='mayaPath')
        
    if mc.objExists(scene.projectInfo.asset.instance_grp()): 
        refs = instance_utils.get_instance_dependency(scene)
        
        if mayaPath in refs: 
            return display.status_dict(display.Status.latest, mayaPath)
    if mayaPath: 
        if os.path.exists(mayaPath): 
            return display.status_dict(display.Status.ready, mayaPath)
        else: 
            return display.status_dict(display.Status.notReady, mayaPath)
    return display.status_dict(display.Status.notReady, mayaPath)
