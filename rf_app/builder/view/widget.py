from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


class LabelIcon(QtWidgets.QWidget):
    status = QtCore.Signal(str)
    def __init__(self, text, parent=None) :
        super(LabelIcon, self).__init__(parent)
        self.text = text
        self.ui()
        
    def ui(self): 
        self.layout = QtWidgets.QHBoxLayout()
        self.label = QtWidgets.QLabel(self.text)
        self.layout.addWidget(self.label)
        self.setLayout(self.layout)
        self.layout.setContentsMargins(1, 1, 1, 1)


class VersionComboBox(QtWidgets.QWidget):
    status = QtCore.Signal(str)
    def __init__(self, parent=None) :
        super(VersionComboBox, self).__init__(parent)
        self.ui()
        
    def ui(self): 
        self.layout = QtWidgets.QHBoxLayout()
        self.comboBox = QtWidgets.QComboBox()
        self.layout.addWidget(self.comboBox)
        self.setLayout(self.layout)
        self.layout.setContentsMargins(1, 1, 1, 1)



class MaterialComboBox(QtWidgets.QWidget):
    status = QtCore.Signal(str)
    def __init__(self, parent=None) :
        super(MaterialComboBox, self).__init__(parent)
        self.ui()
        
    def ui(self): 
        self.layout = QtWidgets.QHBoxLayout()
        self.comboBox = QtWidgets.QComboBox()
        self.layout.addWidget(self.comboBox)
        self.setLayout(self.layout)
        self.layout.setContentsMargins(1, 1, 1, 1)


class WidgetButton(QtWidgets.QWidget):
    status = QtCore.Signal(str)
    def __init__(self, parent=None) :
        super(WidgetButton, self).__init__(parent)
        self.ui()
        
    def ui(self): 
        self.layout = QtWidgets.QHBoxLayout()
        self.button = QtWidgets.QPushButton()
        self.layout.addWidget(self.button)
        self.setLayout(self.layout)
        self.layout.setContentsMargins(1, 1, 1, 1)


class ContentTree(QtWidgets.QWidget):
    status = QtCore.Signal(str)
    def __init__(self, parent=None) :
        super(ContentTree, self).__init__(parent)
        self.ui()
        
    def ui(self): 
        self.layout = QtWidgets.QHBoxLayout()
        self.treeWidget = QtWidgets.QTreeWidget()
        self.layout.addWidget(self.treeWidget)
        self.setLayout(self.layout)
        self.layout.setContentsMargins(1, 1, 1, 1)
