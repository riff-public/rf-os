import sys 
import os 
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils import icon
from rf_utils.widget import project_widget
reload(project_widget)
from rf_utils.widget import entity_browse_widget 
reload(entity_browse_widget)
sg = None

class Column: 
    element = 'element'
    dataType = 'dataType'
    version = 'version'
    material = 'material'
    status = 'Status'
    build = 'Build'
    refresh = 'Refresh'
    remove = 'remove'
    empty = ''

class Button: 
    build = 'Build'
    refresh = 'Reload'
    remove = 'Remove'
    manualLabel = 'List Items'
    autoLabel = 'List Scene Items'
    noModule = 'No Module'
    noAttr = 'No Func'

class ItemLevel: 
    top = 'top'
    groupType = 'groupType'
    dataType = 'dataType'
    element = 'element'

class ContextMenu: 
    groupType = {'build': 'Build all', 'refresh': 'Refresh all', 'remove': 'Remove all'}
    dataType = {'build': 'Build all', 'refresh': 'Refresh all', 'remove': 'Remove all'}

    
class MainDisplay: 
    columnList = [Column.element, Column.dataType, Column.version, Column.material, Column.status, Column.build, Column.remove, Column.refresh, Column.empty]



class BuilderUi(QtWidgets.QWidget):
    status = QtCore.Signal(str)
    def __init__(self, parent=None) :
        super(BuilderUi, self).__init__(parent)

        # main layout 
        self.mainLayout = QtWidgets.QVBoxLayout()

        # add logo at top layout 
        self.titleLayout = self.add_logo()
        self.mainLayout.addLayout(self.titleLayout)

        # content layout 
        self.bodyLayout = QtWidgets.QHBoxLayout()
        self.mainLayout.addLayout(self.bodyLayout)

        # add navigation layout 
        self.navLayout = self.add_navigation_widget()
        self.bodyLayout.addLayout(self.navLayout)

        # add tree layout 
        self.contentLayout = self.add_content_widget()
        self.bodyLayout.addLayout(self.contentLayout)

        # set body layout ratio
        self.bodyLayout.setStretch(0, 1)
        self.bodyLayout.setStretch(1, 5)

        # set layout 
        self.setLayout(self.mainLayout)

        # set logic 
        self.set_navigation_logic()


    def add_logo(self):
        # set logo widget
        logoLayout = QtWidgets.QHBoxLayout()
        spacerItemH = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        logoLayout.addItem(spacerItemH)

        self.logo = QtWidgets.QLabel()
        self.logo.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        logoLayout.addWidget(self.logo)
        self.logo.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))
        return logoLayout

    def add_navigation_widget(self): 
        """ set navigation layout """ 
        navLayout = QtWidgets.QVBoxLayout()
        
        # widgets 
        self.manualInputCheckBox = QtWidgets.QCheckBox('Manual input')
        navLayout.addWidget(self.manualInputCheckBox)

        self.projectWidget = project_widget.SGProjectComboBox(sg, thread=True, fetchNow=False)
        self.projectWidget.allLayout.setSpacing(4)
        navLayout.addWidget(self.projectWidget)

        self.entityWidget = entity_browse_widget.SGSceneNavigation2(sg=sg, shotFilter=True)
        navLayout.addWidget(self.entityWidget)

        self.setEntityButton = QtWidgets.QPushButton(Button.autoLabel)
        navLayout.addWidget(self.setEntityButton)
        
        # line 
        line = self.line()
        navLayout.addWidget(line)

        # spacer 
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        navLayout.addItem(spacerItem)
        
        navLayout.setStretch(0, 0)
        navLayout.setStretch(1, 0)
        navLayout.setStretch(2, 0)
        navLayout.setStretch(3, 0)
        navLayout.setStretch(4, 0)
        navLayout.setStretch(5, 1)

        return navLayout

    def add_content_widget(self): 
        """ tree view for display items """ 
        contentLayout = QtWidgets.QVBoxLayout()
        # widgets 
        # filter data 
        filterLayout = QtWidgets.QHBoxLayout()

        # grouping rule 
        self.groupingCheckBox = QtWidgets.QCheckBox('Grouping')
        self.groupingComboBox = QtWidgets.QComboBox()

        filterLayout.addWidget(self.groupingCheckBox)
        filterLayout.addWidget(self.groupingComboBox)

        # spacer layout 
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        filterLayout.addItem(spacerItem)

        # checkBox 
        self.filterDataCheckBox = QtWidgets.QCheckBox('Filter Data')
        filterLayout.addWidget(self.filterDataCheckBox)

        self.contentWidget = ContentTree()
        contentLayout.addLayout(filterLayout)
        contentLayout.addWidget(self.contentWidget)
        return contentLayout

    def line(self, direction='h', parent=None):
        """ line """
        line = QtWidgets.QFrame(parent)
        line.setFrameShape(QtWidgets.QFrame.HLine) if direction == 'h' else line.setFrameShape(QtWidgets.QFrame.VLine)
        line.setFrameShadow(QtWidgets.QFrame.Sunken)
        return line

    def set_navigation_logic(self): 
        """ link project to entity """ 
        self.projectWidget.projectChanged.connect(self.project_changed)
        # help connect for first time running 
        # currentProject = self.projectWidget.current_item()
        # self.entityWidget.list_episodes(currentProject)

    def project_changed(self, projectEntity): 
        """ call when project choice has changed """ 
        self.entityWidget.list_episodes(projectEntity)


class LabelIcon(QtWidgets.QWidget):
    status = QtCore.Signal(str)
    def __init__(self, text, iconPath=None, parent=None) :
        super(LabelIcon, self).__init__(parent)
        self.text = text
        self.iconPath = iconPath
        self.ui()
        
    def ui(self): 
        self.layout = QtWidgets.QHBoxLayout()
        self.label = QtWidgets.QLabel(self.text)
        self.label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.layout.addWidget(self.label)

        if self.iconPath: 
            self.logo = QtWidgets.QLabel()
            # self.logo.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
            self.logo.setPixmap(QtGui.QPixmap(self.iconPath).scaled(16, 16, QtCore.Qt.KeepAspectRatio))
            self.layout.addWidget(self.logo)

        self.setLayout(self.layout)
        self.layout.setContentsMargins(1, 1, 1, 1)

    def get_text(self): 
        return self.label.text()


class VersionComboBox(QtWidgets.QWidget):
    status = QtCore.Signal(str)
    def __init__(self, parent=None) :
        super(VersionComboBox, self).__init__(parent)
        self.ui()
        
    def ui(self): 
        self.layout = QtWidgets.QHBoxLayout()
        self.comboBox = QtWidgets.QComboBox()
        self.layout.addWidget(self.comboBox)
        self.setLayout(self.layout)
        self.layout.setContentsMargins(1, 1, 1, 1)



class MaterialComboBox(QtWidgets.QWidget):
    status = QtCore.Signal(str)
    def __init__(self, parent=None) :
        super(MaterialComboBox, self).__init__(parent)
        self.ui()
        
    def ui(self): 
        self.layout = QtWidgets.QHBoxLayout()
        self.comboBox = QtWidgets.QComboBox()
        self.layout.addWidget(self.comboBox)
        self.setLayout(self.layout)
        self.layout.setContentsMargins(1, 1, 1, 1)


class WidgetButton(QtWidgets.QWidget):
    status = QtCore.Signal(str)
    def __init__(self, name='', iconPath='', parent=None) :
        super(WidgetButton, self).__init__(parent)
        self.name = name
        self.iconPath = iconPath
        self.ui()
        
    def ui(self): 
        self.layout = QtWidgets.QHBoxLayout()
        self.button = QtWidgets.QPushButton(self.name)
        self.layout.addWidget(self.button)
        if self.iconPath: 
            self.add_icon(self.iconPath)
        self.setLayout(self.layout)
        self.layout.setContentsMargins(1, 1, 1, 1)

    def add_icon(self, iconPath): 
        iconWidget = QtGui.QIcon()
        iconWidget.addPixmap(QtGui.QPixmap(iconPath), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.button.setIcon(iconWidget)

    def set_visible(self, state): 
        self.button.setVisible(state)


class ContentTree(QtWidgets.QWidget):
    status = QtCore.Signal(str)
    def __init__(self, parent=None) :
        super(ContentTree, self).__init__(parent)
        self.ui()
        self.setup()
        
    def ui(self): 
        self.layout = QtWidgets.QHBoxLayout()
        self.treeWidget = QtWidgets.QTreeWidget()
        self.treeWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.treeWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.layout.addWidget(self.treeWidget)
        self.setLayout(self.layout)
        self.layout.setContentsMargins(1, 1, 1, 1)

    def setup(self): 
        """ setup tree column header """ 
        self.headerColumns = MainDisplay.columnList
        self.header = QtWidgets.QTreeWidgetItem(self.headerColumns)
        self.treeWidget.setHeaderItem(self.header)

    def clear(self): 
        """ clear contents """ 
        self.treeWidget.clear()

    def set_root(self): 
        root = self.treeWidget.invisibleRootItem()
        return root

    def list_childs(self, root): 
        allItems = []
        if not root: 
            root = self.treeWidget.invisibleRootItem()
        
        count = root.childCount()
        if not count == 0: 
            items = [root.child(a) for a in range(count)]
            allItems += items
            for item in items: 
                allItems += self.list_childs(item) 
        else: 
            return allItems
        return allItems

    def selected_items(self): 
        selectedItems = self.treeWidget.selectedItems()
        return selectedItems

    def current_item(self): 
        currentItem = self.treeWidget.currentItem()
        data = currentItem.data(0, QtCore.Qt.UserRole)
        return data

    def get_item_label(self, item): 
        return str(item.text(0))

    def item_type(self, item): 
        data = item.data(0, QtCore.Qt.UserRole)
        return data.get('itemType')

    def list_type_items(self, root, dataType): 
        items = [root]
        items += self.list_childs(root)
        targetItems = []
        
        for item in items: 
            data = item.data(0, QtCore.Qt.UserRole)
            itemDataType = data.get('itemType')
            
            if itemDataType: 
                if dataType == itemDataType: 
                    targetItems.append(item)

        return targetItems

    def set_data(self, itemType=None, asset=None, item=None, dataType=None): 
        data = {'itemType': itemType, 'asset': asset, 'item': item, 'dataType': dataType}
        item.setData(0, QtCore.Qt.UserRole, data)

    def get_data(self, item): 
        return item.data(0, QtCore.Qt.UserRole)

    def set_all(self, parentRoot, rootName, data=None): 
        """ root all item """ 
        item = QtWidgets.QTreeWidgetItem(parentRoot, [rootName])
        return item

    def set_group(self, parentRoot, groupName, data=None): 
        """ root item """ 
        item = QtWidgets.QTreeWidgetItem(parentRoot, [groupName])
        return item

    def set_type(self, parentRoot, typeName, data=None): 
        """ add data type """ 
        item = QtWidgets.QTreeWidgetItem(parentRoot, [typeName])
        return item 

    def set_element(self, parentRoot, name, data=None): 
        """ add element name """ 
        item = QtWidgets.QTreeWidgetItem(parentRoot, [name])
        return item 

    def add_data_type(self, rootItem, dataType, iconPath=''): 
        """ add data type """ 
        columnIndex = self.headerColumns.index(Column.dataType)
        labelWidget = LabelIcon(dataType, iconPath, parent=self.treeWidget)
        self.treeWidget.setItemWidget(rootItem, columnIndex, labelWidget)
        return labelWidget

    def add_version_comboBox(self, rootItem): 
        """ add data type """ 
        columnIndex = self.headerColumns.index(Column.version)
        versionWidget = VersionComboBox(self.treeWidget)
        self.treeWidget.setItemWidget(rootItem, columnIndex, versionWidget)
        return versionWidget

    def add_material_comboBox(self, rootItem): 
        """ add data type """ 
        columnIndex = self.headerColumns.index(Column.material)
        materialWidget = MaterialComboBox(self.treeWidget)
        self.treeWidget.setItemWidget(rootItem, columnIndex, materialWidget)
        return materialWidget 

    def add_build_button(self, rootItem, iconPath=''): 
        """ add build button """ 
        columnIndex = self.headerColumns.index(Column.build)
        buttonWidget = WidgetButton(Button.build, iconPath, self.treeWidget)
        self.treeWidget.setItemWidget(rootItem, columnIndex, buttonWidget)
        return buttonWidget 

    def add_refresh_button(self, rootItem, iconPath=''): 
        """ add build button """ 
        columnIndex = self.headerColumns.index(Column.refresh)
        buttonWidget = WidgetButton(Button.refresh, iconPath, self.treeWidget)
        self.treeWidget.setItemWidget(rootItem, columnIndex, buttonWidget)
        return buttonWidget 

    def add_remove_button(self, rootItem, iconPath=''): 
        """ add build button """ 
        columnIndex = self.headerColumns.index(Column.remove)
        buttonWidget = WidgetButton(Button.remove, iconPath, self.treeWidget)
        self.treeWidget.setItemWidget(rootItem, columnIndex, buttonWidget)
        return buttonWidget 

    def expand_tree(self, item): 
        self.treeWidget.expandItem(item)

    def set_status(self, rootItem, text, iconPath): 
        """ set status widget """ 
        columnIndex = self.headerColumns.index(Column.status)
        statusWidget = LabelIcon(text, iconPath, parent=self.treeWidget)
        self.treeWidget.setItemWidget(rootItem, columnIndex, statusWidget)
        return statusWidget 

    def get_status(self, rootItem): 
        columnIndex = self.headerColumns.index(Column.status)
        statusWidget = self.treeWidget.itemWidget(rootItem, columnIndex)
        return statusWidget.get_text()

    def adjust_element_column(self, width): 
        self.treeWidget.setColumnWidth(self.headerColumns.index(Column.element), width)

    def adjust_build_column(self, width): 
        self.treeWidget.setColumnWidth(self.headerColumns.index(Column.build), width)

    def adjust_refresh_column(self, width): 
        self.treeWidget.setColumnWidth(self.headerColumns.index(Column.refresh), width)

    def adjust_remove_column(self, width): 
        self.treeWidget.setColumnWidth(self.headerColumns.index(Column.remove), width)

    def adjust_dataType_column(self, width): 
        self.treeWidget.setColumnWidth(self.headerColumns.index(Column.dataType), width)

    def adjust_status_column(self, width): 
        self.treeWidget.setColumnWidth(self.headerColumns.index(Column.status), width)

