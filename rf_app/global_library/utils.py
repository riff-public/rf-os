from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import os 
import sys
import logging
import shutil
import subprocess
import re
from PIL_Maya import Image
from rf_utils.widget import dialog

class IconPath:
    comp_icon = '%s/core/rf_app/global_library/icons/dialog/complete.png' % os.environ.get('RFSCRIPT')
    add_icon = '%s/core/rf_app/global_library/icons/dialog/add.png' % os.environ.get('RFSCRIPT')
    error_icon = '%s/core/rf_app/global_library/icons/dialog/error.png' % os.environ.get('RFSCRIPT')
    remove_icon = '%s/core/rf_app/global_library/icons/dialog/remove.png' % os.environ.get('RFSCRIPT')
    warning_icon = '%s/core/rf_app/global_library/icons/dialog/warning.png' % os.environ.get('RFSCRIPT')
    edit_icon = '%s/core/rf_app/global_library/icons/dialog/edit.png' % os.environ.get('RFSCRIPT')
    movPreview = '%s/core/rf_app/global_library/icons/mov_preview_icon.png' % os.environ.get('RFSCRIPT')

    folder_icon = '%s/core/icons/dir_icon.png' % os.environ.get('RFSCRIPT')
    file_icon = '%s/core/icons/etc_icon.png' % os.environ.get('RFSCRIPT')
    nopreview_icon = '%s/core/icons/nopreview_icon.png'% os.environ.get('RFSCRIPT')

def forward_slash_to_backslash(text):
    return text.replace('/', '\\')

def back_slash_to_forwardslash(text):
    return text.replace('\\', '/')

def copy_image(images,assetName,destination):
    if not os.path.exists(os.path.dirname(destination+'/')):
        os.makedirs(os.path.dirname(destination+'/'))
    count = 1
    image_list = []

    for img in images:
        #name ,ext = os.path.splitext(img)
        imageName = "%s_main_v.%s%s" %(assetName,str("%04d")%count,'.png')
        dst = os.path.join(destination,imageName)
        dst = dst.replace('\\','/')
        shutil.copy2(img,dst)
        image_list.append(dst)
        count += 1
    return image_list

def copy_mov(mov,assetName,destination):
    from datetime import datetime
    #from rf_utils.pipeline import convert_lib
    date = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")

    movName ,ext = os.path.splitext(mov)
    destination = '%s/%s_combne_%s%s' % (destination, assetName, date, ext)
    #convert_lib.resize_mov(back_slash_to_forwardslash(mov),back_slash_to_forwardslash(destination),scale=[1280,720])
    shutil.copy2(back_slash_to_forwardslash(mov),back_slash_to_forwardslash(destination))

    return destination

def resize_image(images,width,height):
    for img_n in images:
        img = Image.open(img_n).convert('RGBA')
        x,y = img.size
        
        if x < width or y < height:
            min_size = min(width-x,height-y)
            img = img.resize((x+min_size, y+min_size),Image.ANTIALIAS)
            img.save(img_n)
        
        x,y = img.size
        pos_x = x/2
        pos_y = y/2
        pos_w = width/2
        pos_h = height/2
        new_img = Image.new('RGBA', (width, height),(0,0,0))
        new_img.paste(img,(pos_w-pos_x,pos_h-pos_y))
        
        resize = new_img.resize((width, height),Image.ANTIALIAS)
        resize.save(img_n)

def set_size_image(image):
    img = Image.open(image).convert('RGBA')
    x,y = img.size
    if x == 1 and y == 1:
        img = img.resize((1080, 720),Image.ANTIALIAS)
        img.save(image,'png')
    return image

def combine_image_mov(images,imageName,destination):
    from rf_utils.pipeline import convert_lib
    reload(convert_lib)
    from datetime import datetime
    date = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    
    if len(images) == 1:
        return images[0]

    dst = '%s/%s_combine_%s.mov' % (destination, imageName, date)
    dst = back_slash_to_forwardslash(dst)
    convert_lib.img_varysize_to_mov(images,dst,1280,720)
    #convert_lib.img_to_mov2(images, dst)
    return dst

def remove_file(file):
    for file_n in file:
        os.remove(file_n)

def check_specific_char(string):
    pattern = re.compile(r'[^a-zA-Z0-9._ ]')
    string = pattern.search(string)
    return not bool(string)