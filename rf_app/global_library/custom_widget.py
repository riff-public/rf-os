import os
import sys
import utils

from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCore
from datetime import datetime

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
moduleDir = os.path.dirname(sys.modules[__name__].__file__)

class Config:
    movPreview = '%s/icons/mov_preview_icon.png' % moduleDir
    etcPreview = '%s/icons/etc_preview_icon.png' % moduleDir
    imgExt = ['.jpg', '.png', '.tif', '.gif']
    movExt = ['.mov','.mp4']

class DropFile(QtWidgets.QListWidget):
    """ subclass QLineEdit for dragdrop events """
    dropped = QtCore.Signal(list)
    multipleDropped = QtCore.Signal(list)
    deleted = QtCore.Signal(bool)
    def __init__(self, parent=None):
        super(DropFile, self).__init__(parent)
        self.setAcceptDrops(True)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls:
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            links = []
            for url in event.mimeData().urls():
                links.append(str(url.toLocalFile()))
            for url in links:
                self.dropped.emit(url)
            self.multipleDropped.emit(links)
        else:
            event.ignore()

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Delete:
            self._del_item()

    def _del_item(self):
        for item in self.selectedItems():
            self.takeItem(self.row(item))

        self.deleted.emit(True)

class Display(QtWidgets.QWidget):
    dropped = QtCore.Signal(str)
    deleted = QtCore.Signal(bool)
    def __init__(self) :
        super(Display, self).__init__()

        self.allLayout = QtWidgets.QVBoxLayout()
        self.display = DropFile()
        self.display.setViewMode(QtWidgets.QListView.ListMode)
        self.display.setFlow(QtWidgets.QListView.TopToBottom)
        self.display.setMovement(QtWidgets.QListView.Snap)

        self.allLayout.addWidget(self.display)
        self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.allLayout)
        self.display.dropped.connect(self.call_back)

    def set_style(self, path_bg):
        self.display.setStyleSheet("border-image:url("+path_bg+");")
        self.display.deleted.connect(self.deleted.emit)

    def call_back(self, path):
        self.dropped.emit(path)

    def add_item(self, path): 
        item = QtWidgets.QListWidgetItem(self.display)
        iconPath = path

        if os.path.isdir(path):
            item.setIcon(QtGui.QIcon(utils.IconPath.folder_icon))
        else:
            item.setIcon(QtGui.QIcon(utils.IconPath.file_icon))

        item.setText(path)
        item.setData(QtCore.Qt.UserRole, path)
        self.display.setIconSize(QtCore.QSize(65, 65))

    def get_all_items(self): 
        return [self.display.item(a).data(QtCore.Qt.UserRole) for a in range(self.display.count())]

class CustomListWidget(QtWidgets.QListWidget):
    deleted = QtCore.Signal(bool)
    def __init__(self, parent=None):
        super(CustomListWidget, self).__init__(parent)

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Delete:
            self._del_item()

    def _del_item(self):
        for item in self.selectedItems():
            self.takeItem(self.row(item))

        self.deleted.emit(True)

class DisplayReel(QtWidgets.QWidget) :
    clicked = QtCore.Signal(str)
    deleted = QtCore.Signal(bool)
    def __init__(self) :
        super(DisplayReel, self).__init__()
        
        self.allLayout = QtWidgets.QVBoxLayout()
        self.displayListWidget = CustomListWidget()
        self.allLayout.addWidget(self.displayListWidget)

        self.allLayout.setSpacing(0)
        self.allLayout.setContentsMargins(2, 2, 2, 2)
        self.setLayout(self.allLayout)
        self.customExt = dict()

        self.init_signals()

    def init_signals(self): 
        self.displayListWidget.itemSelectionChanged.connect(self.emit_signal)
        self.displayListWidget.deleted.connect(self.deleted.emit)

    def emit_signal(self): 
        currentItem = self.displayListWidget.currentItem()
        if currentItem: 
            path = currentItem.data(QtCore.Qt.UserRole)
            self.clicked.emit(path)

    def add_item(self, path):
        iconPath = self.get_preview_image(path)
        name, ext = os.path.splitext(path)

        if ext in Config.imgExt or ext in Config.movExt:
            item = QtWidgets.QListWidgetItem(self.displayListWidget)
            iconWidget = QtGui.QIcon()
            iconWidget.addPixmap(QtGui.QPixmap(iconPath),QtGui.QIcon.Normal,QtGui.QIcon.Off)
            item.setIcon(iconWidget)
            item.setData(QtCore.Qt.UserRole, path)

            self.displayListWidget.setIconSize(QtCore.QSize(65, 65))

    def get_all_items(self): 
        return [self.displayListWidget.item(a).data(QtCore.Qt.UserRole) for a in range(self.displayListWidget.count())]

    def clear(self):
        self.displayListWidget.clear()

    def get_preview_image(self, path):
        name, ext = os.path.splitext(path)

        if ext in Config.imgExt:
            return path
        elif ext in Config.movExt:
            return Config.movPreview
        elif ext in self.customExt:
            return self.customExt[ext]
        else:
            return Config.etcPreview

class DropUrlButton(QtWidgets.QPushButton):
    """ subclass QLineEdit for dragdrop events """
    dropped = QtCore.Signal(list)
    multipleDropped = QtCore.Signal(list)
    def __init__(self, parent=None):
        super(DropUrlButton, self).__init__(parent)
        self.setAcceptDrops(True)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls:
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            links = []
            for url in event.mimeData().urls():
                links.append(str(url.toLocalFile()))
            #self.dropped.emit(links[-1])
            self.multipleDropped.emit(links)
        else:
            event.ignore()

class DimScreen(QtWidgets.QSplashScreen):
    snapped = QtCore.Signal(str)
    """ snap screen shot with rubber band """
    """ darken the screen by making splashScreen """
    # app = QtWidgets.QApplication.instance() -> for maya
    # app = QtWidgets.QApplication(sys.argv) -> for standalone

    def __init__(self, isMaya):
        """"""
        if isMaya:
            app = QtWidgets.QApplication.instance()
        else:
            app = QtWidgets.QApplication.instance()
            if not app:
                app = QtWidgets.QApplication(sys.argv)

        screenGeo = app.desktop().screenGeometry()
        width = screenGeo.width()
        height = screenGeo.height()
        fillPix = QtGui.QPixmap(width, height)
        fillPix.fill(QtGui.QColor(1,1,1))

        super(DimScreen, self).__init__(fillPix)
        self.havePressed = False
        self.origin = QtCore.QPoint(0,0)
        self.end = QtCore.QPoint(0,0)
        self.rubberBand = None


        self.setWindowState(QtCore.Qt.WindowFullScreen)
        #self.setBackgroundRole(QtWidgets.QPalette.Dark)
        self.setWindowOpacity(0.4)


    def mousePressEvent(self, event):
        self.havePressed = True
        self.origin = event.pos()

        if not self.rubberBand:
            self.rubberBand = QtWidgets.QRubberBand(QtWidgets.QRubberBand.Rectangle, self)
        self.rubberBand.setGeometry(QtCore.QRect(self.origin, QtCore.QSize()))
        self.rubberBand.show()

    def mouseMoveEvent(self, event):
        self.rubberBand.setGeometry(QtCore.QRect(self.origin, event.pos()).normalized())

    def mouseReleaseEvent(self, event):
        self.rubberBand.hide()
        if self.havePressed == True:
            self.end = event.pos()
            self.hide()

            output = self.capture()
        QtWidgets.QApplication.restoreOverrideCursor()

    def capture(self):
        outputFile = self.get_output()
        if not os.path.exists(os.path.dirname(outputFile)):
            os.makedirs(os.path.dirname(outputFile))

        QtGui.QPixmap.grabWindow(QtWidgets.QApplication.desktop().winId(), self.origin.x(), self.origin.y(), self.end.x()-self.origin.x(), self.end.y()-self.origin.y()).save(outputFile, 'png')
        self.snapped.emit(outputFile)
        # logger.info(outputFile)
        return outputFile

    def get_output(self):
        filename = datetime.now().strftime("%y-%m-%d_%H-%M-%S")
        outputPath = '%s/tmpCapture/capture_%s.png' % (os.environ.get('TEMP'), filename)
        return outputPath

class DisplayDropImage(QtWidgets.QWidget) :
    dropped = QtCore.Signal(str)
    captured = QtCore.Signal(str)
    multipleDropped = QtCore.Signal(list)
    precaptured = QtCore.Signal(bool)
    def __init__(self,captureCommand=None,parent=None) :
        super(DisplayDropImage, self).__init__(parent=parent)
        self.captureCommand = captureCommand

        self.allLayout = QtWidgets.QHBoxLayout()
        self.display = DropUrlButton()
        self.allLayout.addWidget(self.display)

        self.allLayout.setSpacing(0)
        self.allLayout.setContentsMargins(2, 2, 2, 2)
        self.setLayout(self.allLayout)
        self.currentPath = ''

        self.preCapScreen = DimScreen(isMaya=True)
        self.preCapScreen.snapped.connect(self.emit_path)

        self.display.multipleDropped.connect(self.call_back)
        self.customExt = dict()

        self.init_signals()

    def init_signals(self):
        self.display.clicked.connect(self.capture)

    def set_label(self, text):
        self.display.setText(text)

    def call_back(self, path):
        self.multipleDropped.emit(path)

    def set_display(self, path, width=None, height=None):
        self.currentPath = path
        preview = self.get_preview_image(path)
        width = self.display.frameGeometry().width() if not width else width
        height = self.display.frameGeometry().height() if not height else height
        self.display.setIcon(QtGui.QPixmap(preview))
        self.display.setIconSize(QtCore.QSize(width,height))
        #return self.display.setIconSize(QtGui.QPixmap(preview).scaled(width, height, QtCore.Qt.KeepAspectRatio))

    def set_style(self, style):
        self.setStyleSheet(style)

    def clear(self):
        self.display.clear()

    def get_preview_image(self, path):
        name, ext = os.path.splitext(path)

        if ext in Config.imgExt:
            return path
        elif ext in Config.movExt:
            return Config.movPreview
        elif ext in self.customExt:
            return self.customExt[ext]
        else:
            return Config.etcPreview

    def capture(self):
        self.precaptured.emit(True)
        if not self.captureCommand:
            self.preCapScreen.show()
            QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.CrossCursor)

        else:
            result = self.captureCommand(self.w, self.h)
            self.captured.emit(result)

    def emit_path(self, path):
        self.captured.emit(path)

    def set_icon(self, iconPath):
        if os.path.exists(iconPath):
            iconWidget = QtGui.QIcon()
            iconWidget.addPixmap(QtGui.QPixmap(iconPath),QtGui.QIcon.Normal,QtGui.QIcon.Off)
            self.display.setIcon(iconWidget)
            self.display.setIconSize(QtCore.QSize(30, 30))