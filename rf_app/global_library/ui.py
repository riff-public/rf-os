from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import os
import sys
import shutil
import re
import subprocess
import logging
import yaml

class Ui(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(Ui, self).__init__(parent)
        self.publishTab = 'Publish'
        self.libraryTab = 'Library'
        self.add_tab_layout()

    def add_tab_layout(self):
        self.allLayout = QtWidgets.QVBoxLayout()

        self.tabWidget = QtWidgets.QTabWidget()
        self.publishTabWidget = QtWidgets.QWidget()
        self.publishLayout = QtWidgets.QVBoxLayout(self.publishTabWidget)

        self.libraryTabWidget = QtWidgets.QWidget()
        self.libraryLayout = QtWidgets.QVBoxLayout(self.libraryTabWidget)

        self.tabWidget.addTab(self.libraryTabWidget, self.libraryTab)
        self.tabWidget.addTab(self.publishTabWidget, self.publishTab)

        self.allLayout.addWidget(self.tabWidget)
        self.setLayout(self.allLayout)
