# v.0.0.1 polytag switcher
_title = 'Global Library'
_version = 'v.0.1.0'
_des = 'wip'
uiName = 'GlobalLibrary'

#Import python modules
import sys
import os 
import json 
import getpass

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# config file 
config_file = '%s/local_config.json' % moduleDir
with open(config_file) as configData: 
	localConfig = json.load(configData)

SCRIPT = localConfig['SCRIPT']
ROOT = os.environ.get(SCRIPT)
core = localConfig['CORE']

# import config
sys.path.append('%s/%s' % (ROOT, core))
import rf_config as config

# framework modules 
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from rf_utils import thread_pool
from rf_utils import file_utils
from rf_utils.widget import dialog
from rf_utils import admin

from functools import partial

import publish_widget
reload(publish_widget)
import library_widget
reload(library_widget)
import utils
import hashtag_widget
import widgetUi
import ui

from shotgun_api3 import shotgun
from shotgun_api3 import Shotgun
server = config.Shotgun.server
script = config.Shotgun.script

id = config.Shotgun.id
sg = None
try: 
	sg = Shotgun(server, script, id)
	connection = True
except shotgun.SSLHandshakeError as e: 
	connection = False

class StandAlone(QtWidgets.QMainWindow):

	def __init__(self, parent=None):
		#Setup Window
		super(StandAlone, self).__init__(parent)

		self._init_sg_project()
		self.ui = ui.Ui()
		self.add_publish_widget()
		self.add_library_widget()
		self.setup_publish_widget()
		self.setup_library_widget()
		self._init_signal()

		self.setCentralWidget(self.ui)
		self.setObjectName(uiName)
		#self.setFixedSize(900,700)
		self.setMinimumSize(923,739)
		self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
		self.setWindowIcon(QtGui.QIcon("%s/icons/GlobalLibrary.png"%moduleDir))
		self.setFocusPolicy(QtCore.Qt.StrongFocus)

	def resizeEvent(self,event):
		w = self.frameGeometry().width()
		h = self.frameGeometry().height()

		self.libraryWidget.explorerWidget.setFixedSize(w-396,h-380)
		self.libraryWidget.explorer_widget.setGeometry(QtCore.QRect(5, 5, w-396, h-380))

		self.libraryWidget.thumbnailWidget.setFixedSize(self.libraryWidget.thumbnail_frame.frameGeometry().width(),h-208)
		self.libraryWidget.thumbnail_listWidget.setGeometry(QtCore.QRect(-5, -5, self.libraryWidget.thumbnail_frame.frameGeometry().width(), h-200))
		self.libraryWidget.thumbnail_moreWidget.setGeometry(QtCore.QRect(234,h-238, 70, 40))

		self.libraryWidget.hashtagLibrary_widget.setGeometry(QtCore.QRect(230, 50, w-611, 95))
		self.libraryWidget.hashtagWidget.hashtag.setFixedSize(w-621,92)
		self.libraryWidget.asset_editeWidget.setGeometry(QtCore.QRect(w-411, 0, 200, 115))
		self.libraryWidget.hashtag_editWidget.setGeometry(QtCore.QRect(w-439, 115, 200, 115))

		self.publishWidget.hashtagWidget.hashtag.setFixedSize(180,h-468)
		self.publishWidget.frame.setFixedSize(w-269,h-328)
		self.publishWidget.stack_widget.setFixedSize(w-269,h-328)
		self.publishWidget.dropImage_widget.display.setIconSize(QtCore.QSize(w-269,h-328))
		self.publishWidget.reelWidget.displayListWidget.setFixedSize(w-269,80)

		self.publishWidget.page1_widget.setGeometry(QtCore.QRect(w-419, 0, 60, 60))
		self.publishWidget.page2_widget.setGeometry(QtCore.QRect(w-369, 0, 60, 60))

	def add_publish_widget(self):
		self.publishWidget = publish_widget.PublishWidget()
		self.ui.publishLayout.addWidget(self.publishWidget)

	def add_library_widget(self):
		self.libraryWidget = library_widget.LibraryWidget()
		self.ui.libraryLayout.addWidget(self.libraryWidget)

	def update_findToAssets(self,assetEntity):
		self.asset_dict[assetEntity['code']] = {}
		self.asset_dict[assetEntity['code']]['sg_library_type'] = assetEntity['sg_library_type']
		self.asset_dict[assetEntity['code']]['sg_library_subtype'] = assetEntity['sg_library_subtype']
		self.asset_dict[assetEntity['code']]['sg_asset_path'] = assetEntity['sg_asset_path']
		self.asset_dict[assetEntity['code']]['tags'] = assetEntity['tags']

	def update_searchToAsset(self,assetEntity):
		if assetEntity['sg_library_type'] not in self.data:
			self.data[assetEntity['sg_library_type']] = {}
		if assetEntity['sg_library_subtype'] not in self.data[assetEntity['sg_library_type']]:
			self.data[assetEntity['sg_library_type']][assetEntity['sg_library_subtype']] = []

		self.data[assetEntity['sg_library_type']][assetEntity['sg_library_subtype']].append(assetEntity['code'])

	def update_searchToDefault(self):
		default = ['Model','Texture','Megascans','Setdress','Vehicle','Fauna','Town','Furniture','Food']
		default_subtype = 'Prop'
		for name in default:
			if name not in self.data:
				self.data[name] = {}
			if default_subtype not in self.data[name]:
				self.data[name][default_subtype] = []

	def _init_sg_project(self):
		self.projectEntity = self.find_project()
		self.asset_entity = self.find_asset()
		self.asset_dict = {}
		self.data = {}
		self.asset_search = []
		self.library_list = []
		self.tabWidget_stutus = True
		self.threadpool = QtCore.QThreadPool()
		self.threadpool.setMaxThreadCount(1)

		for asset in self.asset_entity:
			self.update_findToAssets(asset)
			self.update_searchToAsset(asset)
			self.asset_search.append(asset['code'])
			self.library_list.append(asset['code'])

		#self.update_searchToDefault()
		hashtag_list = [i['name'] for i in sg.find('Tag',[],['name'])]
		asset_None = [self.asset_search.append(i) for i in hashtag_list if i not in self.asset_search]

	def _init_signal(self):
		self.ui.tabWidget.currentChanged.connect(self.fetch)

		self.publishWidget.library_combo.currentIndexChanged.connect(self.set_publish_subtype)
		self.publishWidget.library_addButton.clicked.connect(self.add_publish_librarytype)
		self.publishWidget.library_editButton.clicked.connect(self.set_action_shortcut)
		self.publishWidget.subtype_addButton.clicked.connect(self.add_publish_subtype)
		self.publishWidget.subtype_editButton.clicked.connect(self.set_action_shortcut)
		self.publishWidget.publish_button.clicked.connect(self.submit)

		self.publishWidget.dropImage_widget.precaptured.connect(self.visible_close_UI)
		self.publishWidget.dropImage_widget.captured.connect(self.visible_open_UI)

		self.libraryWidget.library_combo.currentIndexChanged.connect(self.set_library_subtype)
		self.libraryWidget.library_combo.currentIndexChanged.connect(self.search_library_create)
		self.libraryWidget.subtype_combo.currentIndexChanged.connect(self.search_library_create)

		self.libraryWidget.thumbnailWidget.itemClicked.connect(self.worker_runToCreateInfo)
		self.libraryWidget.thumbnailWidget.itemClicked.connect(self.create_library_hashtag)
		self.libraryWidget.thumbnailWidget.itemClicked.connect(self.selcet_library_info)

		self.libraryWidget.search_edit.textChanged.connect(self.search_library_create)
		self.libraryWidget.thumbnail_combo.currentIndexChanged.connect(self.worker_create)

		self.libraryWidget.asset_editbutton.clicked.connect(self.set_library_action_hashtag)
		self.libraryWidget.hashtag_applyButton.clicked.connect(self.apply_info)

		#self.publishWidget.library_edit.selectionChanged.connect(self.check_shortcut)
		self.shortcut_Return_type = QtWidgets.QShortcut(QtGui.QKeySequence(QtCore.Qt.Key_Return),self.publishWidget.library_edit)
		self.shortcut_Enter_type = QtWidgets.QShortcut(QtGui.QKeySequence(QtCore.Qt.Key_Enter),self.publishWidget.library_edit)
		self.shortcut_Return_type.activated.connect(self.add_libraty_type)
		self.shortcut_Enter_type.activated.connect(self.add_libraty_type)
		self.shortcut_Return_type.setEnabled(False)
		self.shortcut_Enter_type.setEnabled(False)

		self.shortcut_Return_subtype = QtWidgets.QShortcut(QtGui.QKeySequence(QtCore.Qt.Key_Return),self.publishWidget.subtype_edit)
		self.shortcut_Enter_subtype = QtWidgets.QShortcut(QtGui.QKeySequence(QtCore.Qt.Key_Enter),self.publishWidget.subtype_edit)
		self.shortcut_Return_subtype.activated.connect(self.add_libraty_type)
		self.shortcut_Enter_subtype.activated.connect(self.add_libraty_type)
		self.shortcut_Return_subtype.setEnabled(False)
		self.shortcut_Enter_subtype.setEnabled(False)

	def focusOutEvent(self,event):
		self.set_action_shortcut()

	def set_action_shortcut(self):
		if self.publishWidget.library_edit.hasFocus() == True:
			self.shortcut_Return_type.setEnabled(True)
			self.shortcut_Enter_type.setEnabled(True)
			self.shortcut_Return_subtype.setEnabled(False)
			self.shortcut_Enter_subtype.setEnabled(False)
		elif self.publishWidget.subtype_edit.hasFocus() == True:
			self.shortcut_Return_subtype.setEnabled(True)
			self.shortcut_Enter_subtype.setEnabled(True)
			self.shortcut_Return_type.setEnabled(False)
			self.shortcut_Enter_type.setEnabled(False)

	def add_libraty_type(self):
		if self.publishWidget.library_edit.hasFocus() == True:
			self.add_publish_librarytype()
			self.publishWidget.set_action_library()
			self.publishWidget.set_action_subtype()
			self.set_action_shortcut()

		elif self.publishWidget.subtype_edit.hasFocus() == True:
			self.add_publish_subtype()
			self.publishWidget.set_action_subtype()
			self.set_action_shortcut()

	def visible_close_UI(self):
		self.setVisible(False)

	def visible_open_UI(self):
		self.setVisible(True)

	def setup_publish_widget(self):
		self.set_publish_librarytype()
		self.set_publish_subtype()
		self.publishWidget.set_hashtagWidget_model()
		self.publishWidget.set_assetWidget_model(self.asset_search)

	def setup_library_widget(self):
		self.create_library_thumbnail()
		self.create_library_imageThumbnail()
		self.set_library_librarytype()
		self.set_library_subtype()
		self.libraryWidget.set_model(self.asset_search)
		self.libraryWidget.add_fsModel()
		self.libraryWidget.explorerWidget.setEnabled(False)

	def set_publish_librarytype(self):
		self.publishWidget.library_combo.clear()
		self.publishWidget.library_edit.clear()
		for dt in sorted(self.data):
			self.publishWidget.library_combo.addItem(dt)

	def set_publish_subtype(self):
		self.publishWidget.subtype_combo.clear()
		self.publishWidget.subtype_edit.clear()

		type_selected = self.publishWidget.library_combo.currentText()
		if type_selected:
			for dt in sorted(self.data[type_selected]):
				self.publishWidget.subtype_combo.addItem(dt)

	def add_publish_librarytype(self):
		text_library = self.publishWidget.library_edit.text()
		self.data[text_library] = {}
		self.publishWidget.library_combo.addItem(text_library)
		self.set_publish_librarytype()
		self.publishWidget.library_combo.setCurrentIndex(self.publishWidget.library_combo.findText(text_library))

	def add_publish_subtype(self):
		text_library = self.publishWidget.library_combo.currentText()
		text_subtype = self.publishWidget.subtype_edit.text()
		self.data[text_library][text_subtype] = []
		self.publishWidget.subtype_combo.addItem(text_subtype)
		self.set_publish_subtype()
		self.publishWidget.subtype_combo.setCurrentIndex(self.publishWidget.subtype_combo.findText(text_subtype))

	def set_library_librarytype(self):
		default = ''
		self.libraryWidget.library_combo.clear()
		self.libraryWidget.library_combo.addItem(default)

		for dt in sorted(self.data):
			if dt:
				self.libraryWidget.library_combo.addItem(dt)

		self.libraryWidget.library_combo.setCurrentIndex(self.libraryWidget.library_combo.findText(default))

	def set_library_subtype(self):
		default = ''
		self.libraryWidget.subtype_combo.clear()
		self.libraryWidget.subtype_combo.addItem(default)

		type_selected = self.libraryWidget.library_combo.currentText()
		if type_selected:
			for dt in sorted(self.data[type_selected]):
				if dt:
					self.libraryWidget.subtype_combo.addItem(dt)

		self.libraryWidget.subtype_combo.setCurrentIndex(self.libraryWidget.subtype_combo.findText(default))

	def set_library_librarytype_info(self):
		assetName = self.libraryWidget.thumbnailWidget.selectedItems()[0].text()
		self.libraryWidget.type_typeCombo.clear()

		for dt in sorted(self.data):
			if dt:
				self.libraryWidget.type_typeCombo.addItem(dt)

		self.libraryWidget.type_typeCombo.setCurrentIndex(self.libraryWidget.type_typeCombo.findText(self.asset_dict[assetName]['sg_library_type']))
		self.libraryWidget.type_typeCombo.currentIndexChanged.connect(self.set_library_subtype_info)
		self.libraryWidget.type_typeLabel.setText("type: "+self.asset_dict[assetName]['sg_library_type'])

	def set_library_subtype_info(self):
		assetName = self.libraryWidget.thumbnailWidget.selectedItems()[0].text()
		type_selected = self.libraryWidget.type_typeCombo.currentText()
		self.libraryWidget.subtype_typeCombo.clear()

		for dt in sorted(self.data[type_selected]):
			if dt:
				self.libraryWidget.subtype_typeCombo.addItem(dt)
				
		self.libraryWidget.subtype_typeCombo.setCurrentIndex(self.libraryWidget.subtype_typeCombo.findText(self.asset_dict[assetName]['sg_library_subtype']))
		self.libraryWidget.subtype_typeLabel.setText("subtype: "+self.asset_dict[assetName]['sg_library_subtype'])
		if self.libraryWidget.subtype_typeCombo.findText(self.asset_dict[assetName]['sg_library_subtype']) == -1:
			self.libraryWidget.subtype_typeCombo.setCurrentIndex(0)


	def create_library_thumbnail(self):
		filter_num = self.libraryWidget.thumbnail_combo.currentText()
		number = None if filter_num == 'all' else int(filter_num)
		for i, name in enumerate(sorted(self.library_list)):
			if self.asset_dict[name]['sg_asset_path']:
				if os.path.isdir(self.asset_dict[name]['sg_asset_path']):
					self.libraryWidget.add_item(name)
			if number == (i+1):
				break

	def create_library_imageThumbnail(self):
		movExt = ['.mov','.mp4']
		for index in range(self.libraryWidget.thumbnailWidget.count()):
			assetName = self.libraryWidget.thumbnailWidget.item(index).text()

			dirIcon = os.path.join(self.asset_dict[assetName]['sg_asset_path'],'_icon')
			imageName = '%s_main_v.0001'%(assetName)
			if os.path.isdir(dirIcon):
				image_icon = [i for i in os.listdir(dirIcon) if imageName in i]
				mov_icon = [i for i in os.listdir(dirIcon) if os.path.splitext(i)[1] in movExt]
			else:
				image_icon = ''
				mov_icon = ''
			
			if image_icon:
				image_icon = os.path.join(self.asset_dict[assetName]['sg_asset_path'],'_icon',image_icon[0])
			elif mov_icon:
				image_icon = utils.IconPath.movPreview
			else:
				image_icon = utils.IconPath.nopreview_icon

			self.libraryWidget.thumbnailWidget.item(index).setIcon(QtGui.QIcon(image_icon)) 

	def create_library_info(self,obj):
		self.libraryWidget.explorerWidget.selectionModel().clearSelection()
		self.libraryWidget.thumbnail.stop_playMedia()
		self.libraryWidget.clear_layout(self.libraryWidget.view_import)
		widgetUi.spaceritemUI(self.libraryWidget.view_import,'import_name',520,2)

		assetName = obj.text()
		image_icon = os.path.join(self.asset_dict[assetName]['sg_asset_path'],'_icon',assetName+'_main_v.0001.png')
		if not os.path.isfile(image_icon):
			image_icon = utils.IconPath.nopreview_icon

		self.libraryWidget.asset_textLabel.setText('Name: '+assetName)
		self.libraryWidget.asset_pathLabel.setText('Path: '+self.asset_dict[assetName]['sg_asset_path'])

		dirIcon = os.path.join(self.asset_dict[assetName]['sg_asset_path'],'_icon')
		
		if os.path.isdir(dirIcon):
			mov_list = [mov for mov in os.listdir(dirIcon) if mov.endswith('.mov') or mov.endswith('.mp4')]
			imgfiles = [i for i in os.listdir(dirIcon) if assetName+'_main_v' in i]

			if mov_list:
				mov_size = self.libraryWidget.is_sizeMov(mov_list,dirIcon)
				image_icon = os.path.join(dirIcon, mov_size)
				self.libraryWidget.thumbnail.loadMedia(str(image_icon))
				self.libraryWidget.thumbnail.mediaThread.wait()

			elif imgfiles:
				image_icon = self.libraryWidget.pathToMediaThumbnail(dirIcon,imgfiles)
				self.libraryWidget.thumbnail.loadMedia(str(image_icon))
				self.libraryWidget.thumbnail.mediaThread.wait()

			else:
				self.libraryWidget.thumbnail.loadMedia(str(image_icon))

		else:
			self.libraryWidget.thumbnail.loadMedia(str(image_icon))
		
		self.set_library_librarytype_info()
		self.set_library_subtype_info()
		self.libraryWidget.thumbnail.setToolTip(os.path.split(image_icon)[-1])

	def selcet_library_info(self, obj):
		if self.libraryWidget.explorerWidget.isEnabled() == False:
			self.libraryWidget.explorerWidget.setEnabled(True)
		assetName = obj.text()
		self.set_library_action_type()
		self.libraryWidget.set_fsModel(self.asset_dict[assetName]['sg_asset_path'])
		self.libraryWidget.thumbnail.setStyleSheet("QToolTip { color: black; background-color: #E5FCC2; border: 0px; }")

		self.libraryWidget.thumbnail.setVisible(True)
		self.libraryWidget.asset_imageWidget.setVisible(False)
		self.libraryWidget.asset_editbutton.setVisible(True)

	def create_library_hashtag(self):
		self.libraryWidget.hashtagWidget.hashtag.clear()
		self.libraryWidget.hashtag_applyButton.setVisible(False)
		item = self.libraryWidget.thumbnailWidget.selectedItems()[0]
		assetName = item.text()
		for i in self.asset_dict[assetName]['tags']:
			self.libraryWidget.hashtagWidget.add_item(i['name'])

		if self.libraryWidget.hashtagWidget.hashtag.itemDoubleClicked.connect(self.libraryWidget.hashtagWidget.add_rename):
			self.libraryWidget.hashtagWidget.hashtag.itemDoubleClicked.disconnect()

	def create_library_hashtag_remove(self):
		self.libraryWidget.hashtagWidget.hashtag.clear()
		self.libraryWidget.hashtag_applyButton.setVisible(True)
		assetName = self.libraryWidget.thumbnailWidget.currentItem().text()
		for i in self.asset_dict[assetName]['tags']:
			self.libraryWidget.hashtagWidget.add_item(i['name'])
			self.libraryWidget.hashtagWidget.add_remove()

		self.libraryWidget.hashtagWidget.hashtag.itemDoubleClicked.connect(self.libraryWidget.hashtagWidget.add_rename)

	def set_library_action_hashtag(self):
		if self.libraryWidget.thumbnailWidget.selectedItems():
			if self.libraryWidget.hashtag_applyButton.isVisible():
				self.create_library_hashtag()
				self.set_library_action_type()

			elif not self.libraryWidget.hashtag_applyButton.isVisible():
				self.create_library_hashtag_remove()
				self.libraryWidget.hashtagWidget.add_create()
				self.set_library_action_type()

	def set_library_action_type(self):
		if self.libraryWidget.hashtag_applyButton.isVisible():
			self.libraryWidget.type_typeCombo.setStyleSheet("color:rgb(255, 255, 255); background-color: rgb(70, 70, 70);")
			self.libraryWidget.type_typeCombo.setVisible(True)
			self.libraryWidget.type_typeLabel.setVisible(False)

			self.libraryWidget.subtype_typeCombo.setStyleSheet("color:rgb(255, 255, 255); background-color: rgb(70, 70, 70);")
			self.libraryWidget.subtype_typeCombo.setVisible(True)
			self.libraryWidget.subtype_typeLabel.setVisible(False)

		elif not self.libraryWidget.hashtag_applyButton.isVisible():
			self.libraryWidget.type_typeCombo.setVisible(False)
			self.libraryWidget.type_typeLabel.setStyleSheet("text-align:left;")
			self.libraryWidget.type_typeLabel.setVisible(True)

			self.libraryWidget.subtype_typeCombo.setVisible(False)
			self.libraryWidget.subtype_typeLabel.setStyleSheet("text-align:left;")
			self.libraryWidget.subtype_typeLabel.setVisible(True)

	def search_library_create(self):
		search_text = self.libraryWidget.search_edit.text()
		search_list = self.search_library_typeName()
		self.library_list = []
		hashtag_list = []
		if search_list:
			self.library_list = [i for i in search_list if search_text in i]
			hashtag_list = [i for i in search_list for x in self.asset_dict[i]['tags'] if search_text in x['name']]
			library_None = [self.library_list.append(i) for i in hashtag_list if i not in self.library_list]

		self.worker_create()

	def search_library_typeName(self):
		search_library = self.libraryWidget.library_combo.currentText()
		search_subtype = self.libraryWidget.subtype_combo.currentText()
		search_list = []
		if search_library and not search_subtype:
			search_list = [asset for asset in self.asset_dict if self.asset_dict[asset]['sg_library_type'] == search_library and not search_subtype]
		
		elif search_library and search_subtype:
			search_list = [asset for asset in self.asset_dict if self.asset_dict[asset]['sg_library_type'] == search_library and self.asset_dict[asset]['sg_library_subtype'] == search_subtype]

		else:
			search_list = [asset for asset in self.asset_dict]

		return search_list

	def publish_hashtag(self,assetName,tag_list):
		add_tags = []
		for tag in tag_list:
			if not tag == 'del_hashtag':
				findTag = hashtag_widget.find_one_tag(tag)
				if not findTag == None:
					add_tags.append(findTag)
				else:
					createTag = ''
					createTag = hashtag_widget.create_hashtag(tag)
					add_tags.append(createTag)

		hashtag_widget.update_hashtag(self.projectEntity,assetName,add_tags)

	def publish_image(self):
		from rf_utils.pipeline import watermark
		from rf_app.publish.asset import sg_hook

		source_image = self.publishWidget.reelWidget.get_all_items()
		source = self.publishWidget.publish_lineEdit.text()
		assetName = source.split('\\')[-1]
		destination_icon = os.path.join(source,'_icon')

		movList = [img for img in source_image if '.mov' in img or '.mp4' in img]

		if not movList:
			imageToMov = utils.copy_image(source_image,assetName,destination_icon)
			utils.resize_image(imageToMov,1280,720)
			media = utils.combine_image_mov(imageToMov,assetName,destination_icon)
		else:
			movPath = utils.copy_mov(source_image[0],assetName,destination_icon)
			resize_mov = sg_hook.proxy_mov(movPath)
			media = utils.back_slash_to_forwardslash(resize_mov)

		entity = self.setup_context()
		watermarked_media, watermark_status = watermark.add_watermark_to_media(entity,media,'default',0.2)

		return watermarked_media,watermark_status

	def remove_watermark_media(self):
		watermarked_media,watermark_status = self.publish_image()
		assetName = self.publishWidget.assetname_edit.text()
		self.create_sg_version(assetName, watermarked_media)
		if watermark_status:
			os.remove(watermarked_media)

	def setup_context(self):
		from rf_utils.context import context_info
		context = context_info.Context()
		context.use_sg(sg=sg, project='GlobalLibrary', entityType='asset', entityName='Prop')
		scene = context_info.ContextPathInfo(context=context)
		return scene

	def set_percent(self,source):
		import re
		pattern = re.compile(r"[\u0E00-\u0E7Fa-zA-Z. ,&$%@!/'+~#()-]")
		amount_list = []
		file_error = []
		min_n = 0
		max_n = 2
		for src in source:
			if not os.path.isdir(src):
				amount_list.append(src)
			elif os.path.isdir(src):
				for root, dirs, files in os.walk(src, topdown=False):
					for name in files:
						file_src = os.path.join(root,name)
						amount_list.append(file_src)
						thai_text = pattern.findall(file_src)
						thai_text =''.join(thai_text)
						if not file_src == thai_text:
							file_error.append(file_src)
		
		max_n += len(amount_list)
		return min_n,max_n,file_error

	def set_value_progressBar(self,index):
		self.amount_min += 1
		number = round(100*float(self.amount_min)/float(self.amount_max))
		self.publishWidget.progressBar.setValue((round(100*float(self.amount_min)/float(self.amount_max))))

		if number == 100:
			self.worker_runToComplete()

	def apply_info(self):
		assetName = self.libraryWidget.thumbnailWidget.selectedItems()[0].text()
		hashtag_new = self.libraryWidget.hashtagWidget.get_item()
		typeName = self.asset_dict[assetName]['sg_library_type']
		subtypeName = self.asset_dict[assetName]['sg_library_subtype']
		typeName_new = self.libraryWidget.type_typeCombo.currentText()
		subtypeName_new = self.libraryWidget.subtype_typeCombo.currentText()

		if 'del_hashtag' in hashtag_new:
			hashtag_new.remove('del_hashtag')
		hastag_old = [i['name'] for  i in self.asset_dict[assetName]['tags']]

		if not hashtag_new == hastag_old:
			self.publish_hashtag(assetName,hashtag_new)
			assetEntity = self.find_one_asset(assetName)
			self.update_findToAssets(assetEntity)
			print hashtag_new,'NAME : '+assetName

		if not typeName == self.libraryWidget.type_typeCombo.currentText() or not subtypeName == self.libraryWidget.subtype_typeCombo.currentText():
			asset_sg = self.find_one_asset(assetName)
			data = {'project': self.projectEntity,'sg_library_type': typeName_new, 'sg_library_subtype': subtypeName_new}
			sg.update('Asset', asset_sg['id'], data)

			self.asset_dict[assetName]['sg_library_type'] = typeName_new
			self.asset_dict[assetName]['sg_library_subtype'] = subtypeName_new
			self.libraryWidget.type_typeLabel.setText("type: "+typeName_new)
			self.libraryWidget.subtype_typeLabel.setText("type: "+subtypeName_new)
			print typeName_new,subtypeName_new,'NAME : '+assetName

		self.set_library_action_hashtag()

	def submit(self):
		self.tabWidget_stutus = False
		self.publish_status = True

		assetName = self.publishWidget.assetname_edit.text()
		path_dir = self.publishWidget.publish_lineEdit.text()
		libraryTypeName = self.publishWidget.library_combo.currentText()
		subTypeName = self.publishWidget.subtype_combo.currentText()
		hashtag = self.publishWidget.hashtagWidget.get_item()
		image_listItem = self.publishWidget.reelWidget.get_all_items()

		self.source = self.publishWidget.dropFile_widget.get_all_items()
		self.destination = utils.back_slash_to_forwardslash(self.publishWidget.publish_lineEdit.text())

		asset_sg = self.find_one_asset(assetName)
		if asset_sg:
			self.publishWidget.assetname_edit.setStyleSheet("background-color: rgb(255, 90, 90);")
			self.publish_status = False
		if self.publishWidget.library_edit.isVisible() == True:
			self.publishWidget.library_edit.setStyleSheet("background-color: rgb(254, 170, 20);")
			self.publish_status = False
		if self.publishWidget.subtype_edit.isVisible() == True:
			self.publishWidget.subtype_edit.setStyleSheet("background-color: rgb(254, 170, 20);")
			self.publish_status = False

		if assetName and libraryTypeName and subTypeName and image_listItem and self.source and self.destination and not asset_sg and self.publish_status:
			self.amount_min,self.amount_max,self.error_path = self.set_percent(self.source)
			if not self.error_path:
				self.publishWidget.progressBar.setVisible(True)
				try:
					self.create_sg_asset(libraryTypeName,subTypeName,assetName)
					self.set_value_progressBar(1)

					if hashtag:
						self.publish_hashtag(assetName,hashtag)

					self.create_sg_link(assetName,path_dir)
					self.set_value_progressBar(1)

					assetEntity = self.find_one_asset(assetName)
					if (assetEntity['sg_library_type'] and 
									assetEntity['sg_library_subtype'] and 
									assetEntity['sg_asset_path'] and 
									assetEntity['sg_asset_dir']):

						if not os.path.exists(os.path.dirname(path_dir+'/')):
							os.makedirs(os.path.dirname(path_dir+'/'))
						os.startfile(path_dir)
						self.worker_copy()

					else:
						self.publishWidget.progressBar.setVisible(False)
						dialog.MessageBox.error('Publish','Publish Error')

				except Exception, e:
					self.amount_min = 1
					self.set_value_progressBar(1)
					logger.error('Failed create shotgun')
					logger.error(e)
			else:
				textName = ''
				for i,path in enumerate(self.error_path):
					textName = '%s%s. %s\n'%(textName,i+1,path)
				dialog.MessageBox.error('Error Path',textName)
		else:
			dialog.MessageBox.warning('Warning','Check Input')


	def fetch(self):
		if not self.tabWidget_stutus:
			self.worker_fetch()

	def worker_createInfo(self):
		threadpool = QtCore.QThreadPool()
		worker = thread_pool.Worker(self.worker_runToCreateInfo)
		worker.signals.finished.connect(self.worker_delay)
		threadpool.start(worker)

	def worker_runToCreateInfo(self):
		threadpool = QtCore.QThreadPool()
		worker = thread_pool.Worker(self.finishToCreateInfo)
		worker.signals.finished.connect(self.worker_delay)
		threadpool.start(worker)

	def finishToCreateInfo(self):
		item = self.libraryWidget.thumbnailWidget.selectedItems()[0]
		self.create_library_info(item)
			
	def worker_fetch(self):
		threadpool = QtCore.QThreadPool()
		worker = thread_pool.Worker(self.clearTofetch)
		worker.signals.finished.connect(self.worker_runTofetch)
		threadpool.start(worker)

	def worker_runTofetch(self):
		threadpool = QtCore.QThreadPool()
		worker = thread_pool.Worker(self.worker_delay)
		worker.signals.finished.connect(self.finishTofetch)
		threadpool.start(worker)

	def clearTofetch(self):
		self.libraryWidget.thumbnailWidget.clear()

	def finishTofetch(self):
		self._init_sg_project()
		self.setup_library_widget()

	def worker_create(self):
		threadpool_create = QtCore.QThreadPool()
		worker = thread_pool.Worker(self.worker_runToCreate)
		worker.signals.finished.connect(self.worker_runToThumbnail)
		threadpool_create.start(worker)

	def worker_runToThumbnail(self):
		threadpool_thumbnail = QtCore.QThreadPool()
		worker = thread_pool.Worker(self.worker_delay)
		worker.signals.finished.connect(self.create_library_imageThumbnail)
		threadpool_thumbnail.start(worker)

	def worker_runToCreate(self):
		if self.library_list:
			self.libraryWidget.thumbnailWidget.clear()
			self.create_library_thumbnail()
		else:
			self.libraryWidget.thumbnailWidget.clear()

	def worker_delay(self):
		pass

	def worker_copy(self):
		self.worker_c = thread_pool.Worker(self.worker_runToCopy)
		self.worker_c.signals.finished.connect(partial(self.set_value_progressBar,1))
		self.threadpool.start(self.worker_c)

	def worker_runToCopy(self):
		for src in self.source:
			if not os.path.isdir(src):
				path_dir,name = os.path.split(src)
				dst = os.path.join(self.destination,name)

				self.result = file_utils.copy(utils.back_slash_to_forwardslash(src),utils.back_slash_to_forwardslash(dst))
				self.worker_c.signals.result.emit(1)
				print src,dst

			elif os.path.isdir(src):
				for root, dirs, files in os.walk(src, topdown=False):
					for name in files:
						file_src = os.path.join(root,name)
						root_dst = os.path.join(self.destination,root.split('/')[-1])

						if not os.path.exists(os.path.dirname(root_dst+'/')):
							os.makedirs(os.path.dirname(root_dst+'/'))

						file_dst = os.path.join(root_dst, name)
						self.result = file_utils.copy(utils.back_slash_to_forwardslash(file_src),utils.back_slash_to_forwardslash(file_dst))
						self.worker_c.signals.finished.emit()
						print file_src,file_dst

		return self.destination

	def worker_runToComplete(self):
		try:
			self.remove_watermark_media()
			self.set_value_progressBar(1)
		except Exception as e:
			print e

		logger.info('publish complete')
		self.publishWidget.progressBar.setVisible(False)
		dialog.MessageBox.success('Publish','Publish Complete')

		self.publishWidget.reelWidget.displayListWidget.clear()
		self.publishWidget.assetname_edit.clear()
		self.publishWidget.hashtagWidget.hashtag.clear()
		self.publishWidget.dropFile_widget.display.clear()
		self.publishWidget.hashtag_lineEdit.clear()

		self.publishWidget.set_display_dropFile()
		self.publishWidget.set_display_dropImage()

	def find_project(self):
		filters = [['name', 'is', 'Global Library']]
		fields = ['id', 'name', 'sg_resolution']
		return sg.find_one('Project', filters, fields)

	def find_asset_type(self,sg_type,sg_subtype):
		filters = [['project', 'is', self.projectEntity]]
		fields = [sg_type,sg_subtype,'code','sg_asset_path']
		return sg.find('Asset', filters, fields)

	def find_asset(self):
		filters = [['project', 'is', self.projectEntity]]
		fields = ['code', 'id','sg_library_type','sg_library_subtype','sg_asset_path','tags']
		return sg.find('Asset', filters, fields)

	def find_one_asset(self,name_asset):
		filters = [['project', 'is', self.projectEntity],['code','is',name_asset]]
		fields = ['code', 'id','sg_library_type','sg_library_subtype','sg_asset_path','tags','sg_asset_dir']
		return sg.find_one('Asset', filters, fields)

	def create_sg_asset(self, libraryType, subType, assetName):
		data = {'project': self.projectEntity,'sg_library_type': libraryType, 'sg_library_subtype': subType,'code': assetName}
		sg.create('Asset', data)

	def create_sg_link(self, name_asset, path_dir):
		filters = [['code', 'is', name_asset], ['project', 'is', self.projectEntity]]
		fields = [ 'id','type']
		id_asset = sg.find_one('Asset',filters,fields)
		path_as = {'sg_asset_path': path_dir }
		data = {'sg_asset_dir': {'link_type': "local", 'local_path': path_dir, 'name': 'Open'}}
		return sg.update('Asset', id_asset['id'], data),sg.update('Asset', id_asset['id'], path_as)

	def create_sg_version(self, name_asset, path_mov):
		filters = [['code', 'is', name_asset], ['project', 'is', self.projectEntity]]
		fields = [ 'id','type']
		assets = sg.find_one('Asset', filters, fields)
		versionName = 'hanuman.v001'
		path = path_mov
		data = {'entity': assets, 'code': versionName, 'sg_path_to_movie': path, 'project': self.projectEntity}
		versionEntity = sg.create('Version', data)
		mediaResult = sg.upload('Version', versionEntity['id'], path, 'sg_uploaded_movie')

def show():
	if config.isMaya:
		from rftool.utils.ui import maya_win
		logger.info('Run in Maya\n')
		uiName = 'StandAlone'
		maya_win.deleteUI(uiName)
		myApp = StandAlone(maya_win.getMayaWindow())
		myApp.show()
		return myApp

	else:
		logger.info('Run in standalone\n')
		app = QtWidgets.QApplication.instance()
		if not app: 
			app = QtWidgets.QApplication(sys.argv)
		myApp = StandAlone()
		myApp.show()
		stylesheet.set_default(app)
		sys.exit(app.exec_())

if __name__ == '__main__': 
	show()
