from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import os
import sys
import shutil
import re
import subprocess
import logging
import yaml

from functools import partial
from PIL_Maya import Image #resize imgae

import utils
import widgetUi
import custom_widget
import hashtag_widget
from rf_utils import icon
moduleDir = os.path.dirname(sys.modules[__name__].__file__)

class Config:
    imgPreview = '%s/icons/dropImage.png' % moduleDir
    filePreview = utils.back_slash_to_forwardslash('%s/icons/dropfile.png' % moduleDir)

class PublishWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(PublishWidget, self).__init__(parent)

        self.setup_layout()
        self.logo_widget()
        self.setup_ui()

        self._inite_signal()

        self.shortcut_Return.setEnabled(False)
        self.shortcut_Enter.setEnabled(False)

    def setup_layout(self):
        self.allLayout = QtWidgets.QVBoxLayout()

        self.tophead_layout = QtWidgets.QVBoxLayout()
        self.head_layout = QtWidgets.QHBoxLayout()
        self.tophead_layout.addLayout(self.head_layout)

        self.mid_layout = QtWidgets.QHBoxLayout()
        self.menu_layout = QtWidgets.QVBoxLayout()
        self.tab_layout = QtWidgets.QVBoxLayout()

        self.mid_layout.addLayout(self.menu_layout)
        self.mid_layout.addLayout(self.tab_layout)

        self.allLayout.addLayout(self.tophead_layout)
        self.allLayout.addLayout(self.mid_layout)
        self.mid_layout.setStretch(1,2)

        self.setLayout(self.allLayout)

    def setup_ui(self):
        self.stackPage_widget()
        self.add_dropped_layout()
        self.assetname_widget()
        self.add_library_tpye()
        self.add_sub_tpye()
        self.add_hashtag_layout()
        self.add_progressBar_layout()

    def logo_widget(self):
        # set logo widgetdx
        self.logo = QtWidgets.QLabel()
        self.head_layout.addWidget(self.logo)
        self.logo.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))
        widgetUi.hline_widgetUI(self.tophead_layout,'line')

    def assetname_widget(self):
        self.assetname_text = widgetUi.labelUI(self.menu_layout,"assetName_text","Name")
        self.assetname_edit = widgetUi.lineEditUI(self.menu_layout,"assetname_edit")
        self.assetname_edit.setStyleSheet("background-color: rgb(50, 50, 50);")

    def add_library_tpye(self):
        #text Library Type
        widgetUi.labelUI(self.menu_layout,"library_text","Library Type")
        self.librarytype_Layout = widgetUi.boxLayoutUI(self.menu_layout,"librarytype_Layout",pattern='QH')
        self.library_combo = widgetUi.comboBoxUI(self.librarytype_Layout,"comboBox_library",)
        self.library_edit = widgetUi.lineEditUI(self.librarytype_Layout,"editbrary_lineEdit")
        self.library_edit.setVisible(False)

        self.library_addButton = widgetUi.buttonUI(self.librarytype_Layout,"library_addButton","")
        self.library_addButton.setEnabled(False)
        self.library_addButton.setVisible(False)
        self.library_addButton.setFixedSize(20,20)
        self.library_addButton.setIcon(QtGui.QPixmap(utils.IconPath.comp_icon))
        self.library_addButton.setIconSize(QtCore.QSize(20, 20))

        self.library_delButton = widgetUi.buttonUI(self.librarytype_Layout,"library_delButton","")
        self.library_delButton.setVisible(False)
        self.library_delButton.setFixedSize(20,20)
        self.library_delButton.setIcon(QtGui.QPixmap(utils.IconPath.error_icon))
        self.library_delButton.setIconSize(QtCore.QSize(20, 20))

        self.library_editButton = widgetUi.buttonUI(self.librarytype_Layout,"library_editButton","")
        self.library_editButton.setFixedSize(20,20)
        self.library_editButton.setIcon(QtGui.QPixmap(utils.IconPath.add_icon))
        self.library_editButton.setIconSize(QtCore.QSize(20, 20))

    def add_sub_tpye(self):
        #text Sub Type
        widgetUi.labelUI(self.menu_layout,"asset_text","Sub Type")
        self.subtype_Layout = widgetUi.boxLayoutUI(self.menu_layout,"subtype_Layout",pattern='QH')
        self.subtype_combo = widgetUi.comboBoxUI(self.subtype_Layout,"comboBox_asset",)
        self.subtype_edit = widgetUi.lineEditUI(self.subtype_Layout,"editasset_lineEdit")
        self.subtype_edit.setVisible(False)

        self.subtype_addButton = widgetUi.buttonUI(self.subtype_Layout,"subtype_addButton","")
        self.subtype_addButton.setEnabled(False)
        self.subtype_addButton.setVisible(False)
        self.subtype_addButton.setFixedSize(20,20)
        self.subtype_addButton.setIcon(QtGui.QPixmap(utils.IconPath.comp_icon))
        self.subtype_addButton.setIconSize(QtCore.QSize(20, 20))

        self.subtype_delButton = widgetUi.buttonUI(self.subtype_Layout,"subtype_delButton","")
        self.subtype_delButton.setVisible(False)
        self.subtype_delButton.setFixedSize(20,20)
        self.subtype_delButton.setIcon(QtGui.QPixmap(utils.IconPath.error_icon))
        self.subtype_delButton.setIconSize(QtCore.QSize(20, 20))

        self.subtype_editButton = widgetUi.buttonUI(self.subtype_Layout,"subtype_editButton","")
        self.subtype_editButton.setFixedSize(20,20)
        self.subtype_editButton.setIcon(QtGui.QPixmap(utils.IconPath.add_icon))
        self.subtype_editButton.setIconSize(QtCore.QSize(20, 20))

    def add_hashtag_layout(self):
        #text Hashtag
        widgetUi.labelUI(self.menu_layout,"hashtag_text","Hashtag (enter)")
        self.hashtag_lineEdit = widgetUi.lineEditUI(self.menu_layout,"hashtag_lineEdit")

        self.hashtagWidget = hashtag_widget.hashtagWidget()
        self.hashtagWidget.hashtag.setFlow(QtWidgets.QListView.LeftToRight)
        self.hashtagWidget.hashtag.setStyleSheet("background-color: rgb(68, 68, 68);")
        self.hashtagWidget.hashtag.setWrapping(True)
        self.hashtagWidget.hashtag.setSpacing(4)
        self.hashtagWidget.hashtag.setFixedSize(180,290)
        self.hashtagWidget.hashtag.setViewMode(QtWidgets.QListView.ListMode)
        self.hashtagWidget.hashtag.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.hashtagWidget.hashtag.setFont(QtGui.QFont("Segoe UI",10))
        self.hashtagWidget.hashtag.setSelectionMode(QtWidgets.QAbstractItemView.NoSelection)
        self.menu_layout.addWidget(self.hashtagWidget.hashtag)

    def add_progressBar_layout(self):
        self.progressBar = widgetUi.progressBarUi(self.menu_layout,'progressBar')
        self.progressBar.setVisible(False)
        self.progressBar.setFixedSize(180,15)

    def stackPage_widget(self):
        self.frame = QtWidgets.QFrame()
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFixedSize(680,450)
        self.tab_layout.addWidget(self.frame)

        self.stack_widget = QtWidgets.QStackedWidget(self.frame)
        self.stack_widget.setFixedSize(680,450)
        self.firstPageWidget = QtWidgets.QWidget()
        self.twoPageWidget = QtWidgets.QWidget()

        self.page1_layout = QtWidgets.QVBoxLayout(self.firstPageWidget)
        self.page2_layout = QtWidgets.QVBoxLayout(self.twoPageWidget)

        self.pagereel_layout = QtWidgets.QVBoxLayout()
        self.pagereel_layout.setContentsMargins(0, 0, 0, 0)
        self.tab_layout.addLayout(self.pagereel_layout)

        self.stack_widget.addWidget(self.firstPageWidget)
        self.stack_widget.addWidget(self.twoPageWidget)

        self.page1_widget = QtWidgets.QWidget(self.frame)
        self.page1_widget.setGeometry(QtCore.QRect(530, 0, 60, 60))
        self.page1_frame = QtWidgets.QVBoxLayout(self.page1_widget)
        self.page1_button = QtWidgets.QPushButton()
        self.page1_button.setText("File")
        self.page1_button.setFixedSize(45,25)
        self.page1_frame.addWidget(self.page1_button)

        self.page2_widget = QtWidgets.QWidget(self.frame)
        self.page2_widget.setGeometry(QtCore.QRect(580, 0, 60, 60))
        self.page2_frame = QtWidgets.QVBoxLayout(self.page2_widget)
        self.page2_button = QtWidgets.QPushButton()
        self.page2_button.setText("Image")
        self.page2_button.setFixedSize(45,25)
        self.page2_frame.addWidget(self.page2_button)

        self.OpenStackPage1_Window()

    def add_dropped_layout(self):
        self.dropFile_widget = custom_widget.Display()
        self.dropFile_widget.set_style(Config.filePreview)
        self.dropImage_widget = custom_widget.DisplayDropImage()
        self.dropImage_widget.display.setStyleSheet("background-color: rgb(50, 50, 50);")
        self.dropImage_widget.set_display(Config.imgPreview)

        self.reelWidget = custom_widget.DisplayReel()
        self.reelWidget.displayListWidget.setStyleSheet("background-color: rgb(50, 50, 50);")
        self.reelWidget.displayListWidget.setFlow(QtWidgets.QListView.LeftToRight)
        self.reelWidget.displayListWidget.setFixedSize(680,80)

        self.page1_layout.addWidget(self.dropFile_widget)
        self.page2_layout.addWidget(self.dropImage_widget)
        self.pagereel_layout.addWidget(self.reelWidget)

        self.publish_layout = widgetUi.boxLayoutUI(self.tab_layout,"publish_layout","QH")
        self.publish_lineEdit = widgetUi.lineEditUI(self.publish_layout,'tab_layout','tab_layout')
        self.publish_lineEdit.setReadOnly(True)
        self.publish_button = widgetUi.buttonUI(self.publish_layout,"publish_button","Publish")
        self.publish_button.setFixedSize(200,30)

    def dropped_file(self,path):
        self.dropFile_widget.add_item(path)
        self.dropFile_widget.display.setStyleSheet("background-color: rgb(50, 50, 50);")

    def captured_image(self,path):
        resize_image = utils.set_size_image(path)
        print resize_image
        self.dropImage_widget.set_display(resize_image)
        self.reelWidget.add_item(resize_image)

    def dropped_image(self, path):
        from rf_utils.widget import dialog
        for img in path:
            if '.mov' not in img and '.mp4' not in img and self.dropImage_widget.display.isEnabled() == True:
                self.reelWidget.add_item(img)
                self.dropImage_widget.set_display(path[0])

            elif self.reelWidget.displayListWidget.count() == 0:
                self.reelWidget.add_item(img)
                self.dropImage_widget.display.setEnabled(False)
                self.dropImage_widget.set_display(path[0])

            else:
                if self.dropImage_widget.display.isEnabled() == True:
                    dialog.MessageBox.warning(' ','not drop video')
                elif self.dropImage_widget.display.isEnabled() == False:
                    dialog.MessageBox.warning(' ','not drop image')

    def set_display_Widget(self, path):
        self.dropImage_widget.set_display(path)
        if self.stack_widget.currentIndex() == 0:
            self.OpenStackPage2_Window()

    def OpenStackPage1_Window(self):
        self.stack_widget.setCurrentIndex(0)
        self.page1_button.setStyleSheet("background-color: rgb(50, 50, 50);")
        self.page2_button.setStyleSheet("")
        self.page1_button.setEnabled(False)
        self.page2_button.setEnabled(True)

    def OpenStackPage2_Window(self):
        self.stack_widget.setCurrentIndex(1)
        self.page1_button.setStyleSheet("")
        self.page2_button.setStyleSheet("background-color: rgb(50, 50, 50);")
        self.page1_button.setEnabled(True)
        self.page2_button.setEnabled(False)

    def set_action_library(self):
        if self.library_editButton.isVisible():
            self.library_editButton.setVisible(False)
            self.library_combo.setVisible(False)
            self.library_edit.setVisible(True)
            self.library_edit.setFocus()
            self.library_addButton.setVisible(True)
            self.library_delButton.setVisible(True)
        elif not self.library_editButton.isVisible():
            self.library_editButton.setVisible(True)
            self.library_combo.setVisible(True)
            self.library_edit.setVisible(False)
            self.library_addButton.setVisible(False)
            self.library_delButton.setVisible(False)

    def set_action_subtype(self):
        if self.subtype_editButton.isVisible():
            self.subtype_editButton.setVisible(False)
            self.subtype_combo.setVisible(False)
            self.subtype_edit.setVisible(True)
            self.subtype_edit.setFocus()
            self.subtype_addButton.setVisible(True)
            self.subtype_delButton.setVisible(True)
        elif not self.subtype_editButton.isVisible():
            self.subtype_editButton.setVisible(True)
            self.subtype_combo.setVisible(True)
            self.subtype_edit.setVisible(False)
            self.subtype_addButton.setVisible(False)
            self.subtype_delButton.setVisible(False)

    def _inite_signal(self):
        self.library_editButton.clicked.connect(self.set_action_library)
        self.library_addButton.clicked.connect(self.set_action_library)
        self.library_delButton.clicked.connect(self.set_action_library)
        self.subtype_editButton.clicked.connect(self.set_action_subtype)
        self.subtype_addButton.clicked.connect(self.set_action_subtype)
        self.subtype_delButton.clicked.connect(self.set_action_subtype)

        self.dropFile_widget.dropped.connect(self.dropped_file)
        self.dropFile_widget.deleted.connect(self.set_display_dropFile)
        self.dropImage_widget.multipleDropped.connect(self.dropped_image)
        self.dropImage_widget.captured.connect(self.captured_image)
        self.reelWidget.clicked.connect(self.set_display_Widget)
        self.reelWidget.deleted.connect(self.set_display_dropImage)

        self.page1_button.clicked.connect(self.OpenStackPage1_Window)
        self.page2_button.clicked.connect(self.OpenStackPage2_Window)
        #option
        self.assetname_edit.textChanged.connect(self.get_path_output)

        self.assetname_edit.textChanged.connect(partial(self.check_ui_publish,self.assetname_edit))
        self.library_edit.textChanged.connect(partial(self.check_ui_publish,self.library_edit))
        self.subtype_edit.textChanged.connect(partial(self.check_ui_publish,self.subtype_edit))
        self.hashtag_lineEdit.textChanged.connect(partial(self.check_ui_publish,self.hashtag_lineEdit))

        #hashtag
        self.shortcut_Return = QtWidgets.QShortcut(QtGui.QKeySequence(QtCore.Qt.Key_Return),self.hashtag_lineEdit)
        self.shortcut_Enter = QtWidgets.QShortcut(QtGui.QKeySequence(QtCore.Qt.Key_Enter),self.hashtag_lineEdit)
        self.shortcut_Return.activated.connect(self.add_hashtag)
        self.shortcut_Enter.activated.connect(self.add_hashtag)

    def set_hashtagWidget_model(self):
        tags = [i['name'] for i in hashtag_widget.find_tag()]
        self.model = QtGui.QStandardItemModel()
        self.hashtag_lineEdit.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.completer = QtWidgets.QCompleter(tags)
        self.completer.setCompletionMode(QtWidgets.QCompleter.PopupCompletion)
        self.hashtag_lineEdit.setCompleter(self.completer)

    def set_assetWidget_model(self, asset_list):
        self.model = QtGui.QStandardItemModel()
        self.assetname_edit.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.completer = QtWidgets.QCompleter(asset_list)
        self.completer.setCompletionMode(QtWidgets.QCompleter.PopupCompletion)
        self.assetname_edit.setCompleter(self.completer)

    def add_hashtag(self):
        text_tag = self.hashtag_lineEdit.text()
        name_tag = hashtag_widget.find_one_tag(text_tag)
        text = name_tag['name'] if name_tag != None else text_tag

        self.hashtagWidget.add_item(text)
        self.hashtagWidget.add_remove()
        self.hashtag_lineEdit.clear()

    def get_path_output(self):
        path = os.path.join('P:\\GlobalLibrary',self.assetname_edit.text())
        self.publish_lineEdit.setText(path)

    def set_display_dropImage(self):
        if self.dropImage_widget.display.isEnabled() == False:
            self.dropImage_widget.display.setEnabled(True)
        if self.reelWidget.displayListWidget.count() == 0:
            self.dropImage_widget.set_display(Config.imgPreview)

    def set_display_dropFile(self):
        if self.dropFile_widget.display.count() == 0:
            self.dropFile_widget.set_style(Config.filePreview)

    def check_ui_publish(self,widget_edit,text):
        if not text:
            if widget_edit == self.library_edit:
                self.library_addButton.setEnabled(False)
            elif widget_edit == self.subtype_edit:
                self.subtype_addButton.setEnabled(False)
            elif widget_edit == self.hashtag_lineEdit:
                self.shortcut_Return.setEnabled(False)
                self.shortcut_Enter.setEnabled(False)
        else:
            if not utils.check_specific_char(text) or text[0] == " "or text == "":
                widget_edit.setStyleSheet("background-color: rgb(255, 90, 90);")
                if widget_edit == self.library_edit:
                    self.library_addButton.setEnabled(False)
                elif widget_edit == self.subtype_edit:
                    self.subtype_addButton.setEnabled(False)
                elif widget_edit == self.hashtag_lineEdit:
                    self.shortcut_Return.setEnabled(False)
                    self.shortcut_Enter.setEnabled(False)
            else:
                widget_edit.setStyleSheet("")
                if widget_edit == self.library_edit:
                    self.library_addButton.setEnabled(True)
                elif widget_edit == self.subtype_edit:
                    self.subtype_addButton.setEnabled(True)
                elif widget_edit == self.hashtag_lineEdit:
                    self.shortcut_Return.setEnabled(True)
                    self.shortcut_Enter.setEnabled(True)