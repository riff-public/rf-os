from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from functools import partial
import utils
import os

from rf_utils.sg import sg_process
sg = sg_process.sg

class IconPath:
    remove_icon = '%s/core/rf_app/global_library/icons/dialog/remove.png' % os.environ.get('RFSCRIPT')

class hashtagWidget(QtWidgets.QWidget):
    def __init__(self) :
        super(hashtagWidget, self).__init__()

        self.allLayout = QtWidgets.QVBoxLayout()
        self.hashtag = QtWidgets.QListWidget()
        self.allLayout.addWidget(self.hashtag)

        self.allLayout.setSpacing(0)
        self.allLayout.setContentsMargins(2, 2, 2, 2)
        self.setLayout(self.allLayout)

        sg_hashtag = find_tag()
        self.hashtag_list = [i['name'] for i in sg_hashtag]
        self.buttonEnabled = False
        #self._init_signal()

    def _init_signal(self):
        self.hashtag.itemDoubleClicked.connect(self.add_rename)

    def add_item(self, text):
        item = QtWidgets.QListWidgetItem(self.hashtag)
        item.setForeground(QtCore.Qt.black)
        item.setBackground(QtGui.QColor("#E5FCC2"))
        item.setText("#"+text)
        item.setData(QtCore.Qt.UserRole, text)

    def add_remove(self):
        if self.hashtag.selectedItems():
            item = self.hashtag.selectedItems()[0]
        else:
            item = self.hashtag.item(self.hashtag.count()-1)

        label_text = QtWidgets.QLabel()
        label_text.setFont(QtGui.QFont("Segoe UI",10))
        label_text.setText(item.text())
        self.hashtag.setItemWidget(item,label_text)
        sizeLabel = label_text.sizeHint().width()
        self.hashtag.removeItemWidget(item)

        frame = QtWidgets.QFrame()
        frame.setFixedSize(sizeLabel+15,10)
        frame.setAttribute(QtCore.Qt.WA_TranslucentBackground)
        widget = QtWidgets.QWidget(frame)
        widget.setGeometry(QtCore.QRect(sizeLabel+1.5, 0, 10, 10))

        button = QtWidgets.QPushButton(widget)
        button.setFixedSize(10,10)
        button.setIcon(QtGui.QPixmap(IconPath.remove_icon))
        self.hashtag.setItemWidget(item,frame)

        button.clicked.connect(partial(self.remove,item))

    def remove(self,item):
        self.hashtag.removeItemWidget(item)
        self.hashtag.takeItem(self.hashtag.row(item))

    def add_rename(self,item):
        line_text = QtWidgets.QLineEdit()

        line_text.setStyleSheet("background-color: #E5FCC2;color: black")
        model = QtGui.QStandardItemModel()
        line_text.setFocusPolicy(QtCore.Qt.StrongFocus)
        completer = QtWidgets.QCompleter(self.hashtag_list)
        completer.setCompletionMode(QtWidgets.QCompleter.PopupCompletion)
        line_text.setCompleter(completer)

        self.hashtag.setItemWidget(item,line_text)
        hashtag_shortcut_Return = QtWidgets.QShortcut(QtGui.QKeySequence(QtCore.Qt.Key_Return),line_text,activated=partial(self.rename,item,line_text))
        hashtag_shortcut_Return = QtWidgets.QShortcut(QtGui.QKeySequence(QtCore.Qt.Key_Enter),line_text,activated=partial(self.rename,item,line_text))
        self.hashtag.itemDoubleClicked.disconnect(self.add_rename)

    def rename(self,item,line_text):
        item_last = item.text()

        if line_text.text():
            item.setText("#"+line_text.text())
            item.setData(QtCore.Qt.UserRole, line_text.text())
        else:
            item.setText(item_last)

        self.hashtag.removeItemWidget(item)
        self.add_remove()
        self.hashtag.itemDoubleClicked.connect(self.add_rename)

        if self.buttonEnabled:
            self.buttonEnabled.setEnabled(True)

    def add_create(self):
        add_button = QtWidgets.QPushButton()
        add_button.setFixedSize(15,15)
        add_button.setIcon(QtGui.QPixmap(utils.IconPath.add_icon))
        new_item = QtWidgets.QListWidgetItem()
        new_item.setText("")
        new_item.setData(QtCore.Qt.UserRole, 'del_hashtag')
        self.hashtag.addItem(new_item)
        self.hashtag.setItemWidget(new_item,add_button)
        add_button.clicked.connect(partial(self.create,new_item))
        return add_button

    def create(self,item):
        self.remove(item)
        line_text = QtWidgets.QLineEdit()
        line_text.setStyleSheet("background-color: #E5FCC2;color: black")
        new_item = QtWidgets.QListWidgetItem()
        new_item.setText("New Tag")
        new_item.setForeground(QtCore.Qt.black)
        new_item.setBackground(QtGui.QColor("#E5FCC2"))

        model = QtGui.QStandardItemModel()
        line_text.setFocusPolicy(QtCore.Qt.StrongFocus)
        completer = QtWidgets.QCompleter(self.hashtag_list)
        completer.setCompletionMode(QtWidgets.QCompleter.PopupCompletion)
        line_text.setCompleter(completer)

        self.hashtag.addItem(new_item)
        self.hashtag.setItemWidget(new_item,line_text)

        hashtag_shortcut_Return = QtWidgets.QShortcut(QtGui.QKeySequence(QtCore.Qt.Key_Return),line_text,activated=partial(self.rename,new_item,line_text))
        hashtag_shortcut_Return = QtWidgets.QShortcut(QtGui.QKeySequence(QtCore.Qt.Key_Enter),line_text,activated=partial(self.rename,new_item,line_text))
        line_text.setFocus()
        self.hashtag.itemDoubleClicked.disconnect(self.add_rename)
        self.hashtag.setCurrentItem(new_item)
        self.buttonEnabled = self.add_create()
        self.buttonEnabled.setEnabled(False)

    def get_item(self):
        return [self.hashtag.item(a).data(QtCore.Qt.UserRole) for a in range(self.hashtag.count())]

def create_hashtag(tagName):
    data = {'name':tagName}
    return sg.create('Tag', data)

def update_hashtag(projectEntity,assetName,tagEntity):
    filters = [['code','is',assetName],['project', 'is', projectEntity]]
    assetEntity = sg.find_one('Asset',filters, ['name'])
    data = {'tags': tagEntity}
    return sg.update('Asset', assetEntity['id'], data)

def find_tag():
    return sg.find('Tag',[],['name'])

def find_one_tag(tagName):
    return sg.find_one('Tag',[['name', 'is', tagName]],['name'])

def find_asset_tags(assetName):
    return sg.find_one('Asset', [['code','is',assetName]], ['tags'])

    