from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

def comboBoxUI(layout,nameUI,*text):
    nameUI = QtWidgets.QComboBox()
    for text_n in text:
        nameUI.addItem(text_n)
    layout.addWidget(nameUI)
    return nameUI

def labelUI(layout,nameUI,text):
    nameUI = QtWidgets.QLabel(text)
    layout.addWidget(nameUI)
    nameUI.setStyleSheet("color: rgb(120, 120, 120)")
    return nameUI

def buttonUI(layout,nameUI,text):
    nameUI = QtWidgets.QPushButton(text)
    layout.addWidget(nameUI)
    return nameUI

def boxLayoutUI(layout,nameUI,pattern='QV'):
    if pattern == 'QV':
        nameUI = QtWidgets.QVBoxLayout()
    elif pattern == 'QH':
        nameUI = QtWidgets.QHBoxLayout()
    layout.addLayout(nameUI)
    return nameUI

def lineEditUI(layout,nameUI,text=""):
    nameUI = QtWidgets.QLineEdit()
    layout.addWidget(nameUI)
    return nameUI

def hline_widgetUI(layout,nameUI):
    nameUI = QtWidgets.QFrame()
    nameUI.setFrameShape(QtWidgets.QFrame.HLine)
    nameUI.setFrameShadow(QtWidgets.QFrame.Sunken)
    layout.addWidget(nameUI)
    return nameUI

def spaceritemUI(layout,nameUI,width,height):
    nameUI = QtWidgets.QSpacerItem(width,height)
    layout.addItem(nameUI)
    return nameUI

def progressBarUi(layout,nameUI):
    nameUI = QtWidgets.QProgressBar()
    nameUI.setMinimum(0)
    nameUI.setMaximum(100)
    layout.addWidget(nameUI)
    return nameUI
