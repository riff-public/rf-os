from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import os
import sys
import shutil
import re
import subprocess
import logging
import yaml

from rf_utils import admin
from rf_utils.widget import dialog
import utils
import widgetUi
import hashtag_widget
import hook
from rf_utils.widget import media_widget
import rf_config as config
from rf_utils import icon

class Config:
    imgExt = ['.jpg', '.png', '.tif','.tiff ','.gif','.exr']
    fileExt = ['.ma','.mb','.obj','.OBJ','.fbx','.abc']
    showExt = ['.jpg', '.png', '.tiff','.tif', '.gif']
    movExt = ['.mov','.mp4']


class LibraryWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(LibraryWidget, self).__init__(parent)

        self.setup_layout()
        self.setup_ui_tophead()
        self.setup_ui_mid()
        self._init_signal()

    def setup_layout(self):
        self.allLayout = QtWidgets.QVBoxLayout()

        self.tophead_layout = QtWidgets.QVBoxLayout()
        self.head_layout = QtWidgets.QHBoxLayout()
        self.tophead_layout.addLayout(self.head_layout)

        self.mid_layout = QtWidgets.QHBoxLayout()
        self.left_layout = QtWidgets.QVBoxLayout()
        self.right_layout = QtWidgets.QVBoxLayout()
        self.mid_layout.addLayout(self.left_layout)
        self.mid_layout.addLayout(self.right_layout)

        self.view_top_layout = QtWidgets.QVBoxLayout()
        self.view_down_layout = QtWidgets.QVBoxLayout()
        self.view_top_down = QtWidgets.QHBoxLayout()
        self.view_import = QtWidgets.QHBoxLayout()
        self.right_layout.addLayout(self.view_top_layout)
        self.right_layout.addLayout(self.view_down_layout)
        self.right_layout.addLayout(self.view_import)

        self.right_layout.setStretch(0,1)
        self.right_layout.setStretch(1,2)
        self.right_layout.setStretch(2,0)

        self.mid_layout.setStretch(0,1)
        self.mid_layout.setStretch(1,2)

        self.allLayout.addLayout(self.tophead_layout)
        self.allLayout.addLayout(self.mid_layout)

        self.setLayout(self.allLayout)

    def setup_ui_tophead(self):
        self.add_logo_widget()
        self.add_search_layout()
        widgetUi.hline_widgetUI(self.tophead_layout,'line')

    def setup_ui_mid(self):
        self.add_asset_layout()
        self.add_thumbnail_layuot()
        self.add_hashtag_layout()
        self.add_explorer_layout()

    def _init_signal(self):
        self.explorerWidget.clicked.connect(self.set_imageToThumbnail)
        self.explorerWidget.customContextMenuRequested.connect(self.clearPosSelect)
        self.explorerWidget.customContextMenuRequested.connect(self.show_menu)

    def add_logo_widget(self):
        self.logo = QtWidgets.QLabel()
        self.head_layout.addWidget(self.logo)
        self.logo.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))

    def add_search_layout(self):
        self.head_frame = QtWidgets.QFrame()
        #self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.head_frame.setFixedSize(780,65)

        self.search_widget = QtWidgets.QWidget(self.head_frame)
        self.search_widget.setGeometry(QtCore.QRect(500, 0, 280, 40))
        self.search_frame = QtWidgets.QHBoxLayout(self.search_widget)
        self.search_text = widgetUi.labelUI(self.head_layout,"search_text","search (name,tag)")
        self.search_text.setStyleSheet("color: rgb(200, 200, 200);")
        self.search_edit = widgetUi.lineEditUI(self.head_layout,"assetname_comboBox")
        self.search_edit.setFixedSize(170,20)

        self.library_widget = QtWidgets.QWidget(self.head_frame)
        self.library_widget.setGeometry(QtCore.QRect(483, 30, 140, 40))
        self.library_frame = QtWidgets.QHBoxLayout(self.library_widget)
        self.library_label = widgetUi.labelUI(self.head_layout,"comboBox_library","type")
        self.library_label.setStyleSheet("color: rgb(200, 200, 200);")
        self.library_combo = widgetUi.comboBoxUI(self.head_layout,"comboBox_library")
        self.library_combo.setFixedSize(100,20)

        self.subtype_widget = QtWidgets.QWidget(self.head_frame)
        self.subtype_widget.setGeometry(QtCore.QRect(618, 30, 160, 40))
        self.subtype_frame = QtWidgets.QHBoxLayout(self.subtype_widget)
        self.subtype_label = widgetUi.labelUI(self.head_layout,"comboBox_library","subtype")
        self.subtype_label.setStyleSheet("color: rgb(200, 200, 200);")
        self.subtype_combo = widgetUi.comboBoxUI(self.head_layout,"comboBox_subtype")
        self.subtype_combo.setFixedSize(100,20)

        self.head_layout.addWidget(self.head_frame)
        self.search_frame.addWidget(self.search_text)
        self.search_frame.addWidget(self.search_edit)
        self.library_frame.addWidget(self.library_label)
        self.library_frame.addWidget(self.library_combo)
        self.subtype_frame.addWidget(self.subtype_label)
        self.subtype_frame.addWidget(self.subtype_combo)

    def add_thumbnail_layuot(self):
        self.thumbnail_frame = QtWidgets.QFrame()
        self.thumbnail_frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        #self.thumbnail_frame.setFixedSize(300,530)
        self.thumbnail_frame.setFixedWidth(300)

        self.thumbnail_listWidget = QtWidgets.QWidget(self.thumbnail_frame)
        self.thumbnail_listWidget.setGeometry(QtCore.QRect(-5,-5, 300, 500))
        self.thumbnail_listFrame = QtWidgets.QHBoxLayout(self.thumbnail_listWidget)
        
        self.thumbnailWidget = QtWidgets.QListWidget()
        self.thumbnailWidget.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.thumbnailWidget.setFixedSize(300,500)
        self.thumbnailWidget.setViewMode(QtWidgets.QListView.ListMode)
        self.thumbnailWidget.setIconSize(QtCore.QSize(100, 80))
        self.thumbnailWidget.setStyleSheet("background-color: rgb(40, 40, 40);")
        self.thumbnailWidget.setStyleSheet("QToolTip { color: black; background-color: #E5FCC2; border: 0px; }");
        self.thumbnailWidget.setAlternatingRowColors(False)
        self.thumbnailWidget.setSpacing(1)

        filters = ['10','50','100','all']
        self.thumbnail_moreWidget = QtWidgets.QWidget(self.thumbnail_frame)
        self.thumbnail_moreWidget.setGeometry(QtCore.QRect(234,494, 70, 40))
        self.thumbnail_moreframe = QtWidgets.QHBoxLayout(self.thumbnail_moreWidget)
        self.thumbnail_combo = widgetUi.comboBoxUI(self.thumbnail_moreframe,'thumbnail_moreframe')
        self.thumbnail_combo.setStyleSheet("background-color: rgb(90, 90, 90);") if config.isMaya else self.thumbnail_combo.setStyleSheet("")
        for i,fil in enumerate(filters):
            self.thumbnail_combo.addItem(fil)
            self.thumbnail_combo.setItemData(i, QtCore.Qt.AlignCenter,QtCore.Qt.TextAlignmentRole)

        self.thumbnail_combo.setCurrentIndex(self.thumbnail_combo.findText('10'))
        self.thumbnail_combo.setEditable(True)
        self.thumbnail_combo.lineEdit().setReadOnly(True)
        self.thumbnail_combo.lineEdit().setAlignment(QtCore.Qt.AlignRight)

        self.left_layout.addWidget(self.thumbnail_frame)
        self.thumbnail_listFrame.addWidget(self.thumbnailWidget)

    def add_asset_layout(self):
        self.asset_frame = QtWidgets.QFrame()
        #self.asset_frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        #self.asset_frame.setAttribute(QtCore.Qt.WA_TranslucentBackground)
        self.asset_frame.setFixedHeight(140)
        self.asset_frame.setStyleSheet("background-color: rgb(40, 40, 40);")
        self.asset_frame.setSizePolicy(QtWidgets.QSizePolicy.Expanding,QtWidgets.QSizePolicy.Expanding)

        self.thumbnail_imageWidget = QtWidgets.QWidget(self.asset_frame)
        self.thumbnail_imageWidget.setGeometry(QtCore.QRect(-15, -14, 250, 209))
        self.layout_media = QtWidgets.QVBoxLayout(self.thumbnail_imageWidget)
        self.thumbnail_imageWidget.setStyleSheet("background-color: rgb(40, 40, 40);")

        self.thumbnail = media_widget.PreviewMedia()
        self.thumbnail.setFixedSize(250,204)
        self.thumbnail._loadMedia(str(utils.IconPath.nopreview_icon))
        self.thumbnail.informationText.setVisible(False)
        self.thumbnail.information2Text.setVisible(False)
        self.thumbnail.pause_text = ''
        self.thumbnail.controller_play.setVisible(False)
        self.thumbnail.setStyleSheet("background-color: rgb(40, 40, 40);")
        self.layout_media.addWidget(self.thumbnail)

        self.asset_imageWidget = QtWidgets.QWidget(self.asset_frame)
        self.asset_imageWidget.setGeometry(QtCore.QRect(3, 3, 250, 200))
        self.asset_imagebutton = QtWidgets.QPushButton(self.asset_imageWidget)
        self.asset_imagebutton.setFixedSize(233,132)
        self.asset_imagebutton.setIcon(QtGui.QPixmap(utils.IconPath.nopreview_icon))
        self.asset_imagebutton.setIconSize(QtCore.QSize(200, 130))
        self.asset_imagebutton.setStyleSheet("background-color: rgb(40, 40, 40);")
        self.asset_imageWidget.setVisible(False)

        self.asset_textWidget = QtWidgets.QWidget(self.asset_frame)
        self.asset_textWidget.setGeometry(QtCore.QRect(240, 0, 250, 115))
        self.asset_textLabel = QtWidgets.QLabel(self.asset_textWidget)
        self.asset_textLabel.setText("")
        self.asset_textLabel.setFixedSize(330,20)

        self.asset_editeWidget = QtWidgets.QWidget(self.asset_frame)
        self.asset_editeWidget.setGeometry(QtCore.QRect(528, 0, 200, 115))
        self.asset_editbutton = QtWidgets.QPushButton(self.asset_editeWidget)
        self.asset_editbutton.setFixedSize(20,20)
        self.asset_editbutton.setStyleSheet("padding: 1px;min-width: 1em;border-color: beige")
        self.asset_editbutton.setIcon(QtGui.QPixmap(utils.IconPath.edit_icon))
        self.asset_editbutton.setVisible(False)
        
        self.asset_pathWidget = QtWidgets.QWidget(self.asset_frame)
        self.asset_pathWidget.setGeometry(QtCore.QRect(240, 20, 330, 115))
        self.asset_pathLabel = QtWidgets.QLabel(self.asset_pathWidget)
        self.asset_pathLabel.setFixedSize(330,20)
        self.asset_pathLabel.setText(" ")
        self.asset_pathLabel.setFont(QtGui.QFont("Segoe UI",8))

        self.typy_comboWidget = QtWidgets.QWidget(self.asset_frame)
        self.typy_comboWidget.setGeometry(QtCore.QRect(240, 40, 110, 20))
        self.type_typeCombo = QtWidgets.QComboBox(self.typy_comboWidget)
        self.type_typeCombo.setFixedSize(110,20)
        self.type_typeCombo.setVisible(False)
        self.type_typeLabel = QtWidgets.QLabel(self.typy_comboWidget)
        self.type_typeLabel.setFixedSize(110,20)
        self.type_typeLabel.setVisible(True)

        self.subtypy_comboWidget = QtWidgets.QWidget(self.asset_frame)
        self.subtypy_comboWidget.setGeometry(QtCore.QRect(355, 40, 110, 20))
        self.subtype_typeCombo = QtWidgets.QComboBox(self.subtypy_comboWidget)
        self.subtype_typeCombo.setFixedSize(110,20)
        self.subtype_typeCombo.setVisible(False)
        self.subtype_typeLabel = QtWidgets.QLabel(self.subtypy_comboWidget)
        self.subtype_typeLabel.setFixedSize(110,20)
        self.subtype_typeLabel.setVisible(True)

        self.view_top_layout.addWidget(self.asset_frame)
        self.view_top_layout.addLayout(self.view_top_down)
        widgetUi.hline_widgetUI(self.view_top_down,'line')

    def add_hashtag_layout(self):
        self.hashtagLibrary_widget = QtWidgets.QWidget(self.asset_frame)
        self.hashtagLibrary_widget.setGeometry(QtCore.QRect(230, 50, 328, 95))
        self.hashtagLibrary_frame = QtWidgets.QVBoxLayout(self.hashtagLibrary_widget)
        
        self.hashtagWidget = hashtag_widget.hashtagWidget()
        self.hashtagWidget.hashtag.setFixedSize(318,95)
        self.hashtagWidget.hashtag.setSpacing(4)
        self.hashtagWidget.hashtag.setWrapping(True)
        self.hashtagWidget.hashtag.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.hashtagWidget.hashtag.setViewMode(QtWidgets.QListView.ListMode)
        self.hashtagWidget.hashtag.setFlow(QtWidgets.QListView.LeftToRight)
        self.hashtagWidget.hashtag.setFont(QtGui.QFont("Segoe UI",10))
        self.hashtagWidget.hashtag.setStyleSheet("background-color: rgb(50, 50, 50);")
        self.hashtagLibrary_frame.addWidget(self.hashtagWidget)

        self.hashtag_editWidget = QtWidgets.QWidget(self.asset_frame)
        self.hashtag_editWidget.setGeometry(QtCore.QRect(500, 115, 200, 115))
        self.hashtag_applyButton = QtWidgets.QPushButton(self.hashtag_editWidget)
        self.hashtag_applyButton.setFixedSize(50,20)
        self.hashtag_applyButton.setText('APPLY')
        self.hashtag_applyButton.setStyleSheet("background-color: rgb(121, 189, 154);color: black")
        self.hashtag_applyButton.setVisible(False)

    def add_explorer_layout(self):
        self.explorer_frame = QtWidgets.QFrame()
        self.explorer_frame.setSizePolicy(QtWidgets.QSizePolicy.Expanding,QtWidgets.QSizePolicy.Expanding)
        #self.explorer_frame.setFixedSize(540,390)
        self.explorer_frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.explorer_frame.setStyleSheet("background-color: rgb(40, 40, 40);")

        self.explorer_widget = QtWidgets.QWidget(self.explorer_frame)
        self.explorer_widget.setGeometry(QtCore.QRect(5, 5, 540, 402))

        self.explorerWidget = ExplorerWidget(self.explorer_widget)
        self.explorerWidget.setFixedSize(540,402)
        self.explorerWidget.setStyleSheet("background-color: rgb(40, 40, 40);")
        self.explorerWidget.setSelectionMode(QtWidgets.QTreeView.SingleSelection)
        self.explorerWidget.setEditTriggers(QtWidgets.QTreeView.NoEditTriggers)
        self.explorerWidget.setDragDropMode(QtWidgets.QAbstractItemView.NoDragDrop)
        self.explorerWidget.setDragEnabled(False)
        self.explorerWidget.setAcceptDrops(False)
        self.explorerWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        #self.explorerWidget.setDragDropMode(QtWidgets.QAbstractItemView.InternalMove)
        #self.explorerWidget.setEditTriggers(QtWidgets.QTreeView.DoubleClicked)
        widgetUi.spaceritemUI(self.view_import,'import',520,2)
        self.view_down_layout.addWidget(self.explorer_frame)

    def add_fsModel(self):
        self.fsModel = QtWidgets.QFileSystemModel()
        self.fsModel.setReadOnly(False)
        self.explorerWidget.setModel(self.fsModel)
        self.explorerWidget.setColumnWidth(0,280)
        self.explorerWidget.setColumnWidth(1,50)
        self.explorerWidget.setColumnWidth(2,68)
        self.explorerWidget.setColumnWidth(3,50)

    def set_fsModel(self,path):
        self.fsModel_path = path
        self.fsModel.setRootPath(path)
        self.explorerWidget.setRootIndex(self.fsModel.index(path))
        self.foldersToDisShow(path)
        self.explorerWidget.setSortingEnabled(True)
        self.explorerWidget.sortByColumn(0, QtCore.Qt.AscendingOrder)

    def foldersToDisShow(self, path):
        foldersToShow = []
        for currentDir, dirnames, filenames in os.walk(path):
            if '_icon' not in currentDir :
                foldersToShow.append(currentDir)
            else:
                index = self.fsModel.index(currentDir)
                self.explorerWidget.setRowHidden(index.row(), index.parent(), True)

    def set_model(self, asset_list):
        self.model = QtGui.QStandardItemModel()
        self.search_edit.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.completer = QtWidgets.QCompleter(asset_list)
        self.completer.setCompletionMode(QtWidgets.QCompleter.PopupCompletion)
        self.search_edit.setCompleter(self.completer)

    def add_item(self,assetName):
        item = QtWidgets.QListWidgetItem(self.thumbnailWidget)
        item.setText(assetName)
        item.setData(QtCore.Qt.UserRole, assetName)
        item.setSizeHint(QtCore.QSize(60, 50))
        item.setToolTip(assetName)

    def set_action_explorerWidget(self):
        if self.edit_check.checkState() == QtCore.Qt.Unchecked:
            self.explorer_frame.setStyleSheet("background-color: rgb(40, 40, 40);")
            self.explorerWidget.setEditTriggers(QtWidgets.QTreeView.NoEditTriggers)
            self.explorerWidget.setDragDropMode(QtWidgets.QAbstractItemView.NoDragDrop)
            self.explorerWidget.setDragEnabled(False)
            self.explorerWidget.setAcceptDrops(False)

        elif self.edit_check.checkState() == QtCore.Qt.Checked:
            self.explorer_frame.setStyleSheet("background-color: rgb(190, 70, 98);")
            self.explorerWidget.setAcceptDrops(True)
            self.explorerWidget.setDragDropMode(QtWidgets.QAbstractItemView.InternalMove)
            self.explorerWidget.setEditTriggers(QtWidgets.QTreeView.DoubleClicked)

    def set_imageToThumbnail(self):
        self.file_select = self.explorerWidget.selectionModel().selectedIndexes()[0].data()
        name, ext = os.path.splitext(self.file_select)

        if ext in Config.showExt:
            image_file = self.fsModel.filePath(self.explorerWidget.selectionModel().selectedIndexes()[0])
            self.asset_imagebutton.setIcon(QtGui.QPixmap(image_file))

        #elif ext in Config.movExt:
        #    self.asset_imagebutton.setVisible(False)
        #    self.thumbnail.setVisible(True)
        #    mov_file = self.fsModel.filePath(self.explorerWidget.selectionModel().selectedIndexes()[0])
        #    self.thumbnail.loadMedia(str(utils.forward_slash_to_backslash(mov_file)))
        #    self.thumbnail.mediaThread.wait()

        else:
            image_icon = utils.IconPath.nopreview_icon
            self.asset_imagebutton.setIcon(QtGui.QPixmap(image_icon))

        self.asset_imageWidget.setVisible(True)
        self.thumbnail.setVisible(False)

    def pathToMediaThumbnail(self,rootPath,imgList):
        #'P:/GlobalLibrary/TESTPROP/_icon/TESTPROP_main_v.[0001-0003].png'
        name,ext = os.path.splitext(imgList[0])
        amount_min = '0001'
        if len(imgList) > 1:
            amount_max = str('%04d'%len(imgList))
            amount = '[%s-%s]'%(amount_min,amount_max)
        else:
            amount = amount_min
        number = name.split('.')[-1]
        imageName = name.split('.')[0]
        image_icon = rootPath+'\\'+imageName+'.'+amount+ext
        return image_icon

    def is_sizeMov(self, movList, dirIcon):
        FIXSIZE = 15000000
        size, min_size, min_file = 0, FIXSIZE, os.path.join(dirIcon,movList[0])
        for mov in movList: 
            size = os.stat(os.path.join(dirIcon, mov)).st_size
            if size < min_size: 
                min_size = size 
                min_file = os.path.join(dirIcon, mov)

        if os.path.getsize(min_file) <= FIXSIZE:
            return min_file
        else:
            return utils.IconPath.nopreview_icon

    def show_menu(self,pos):
        self.pos = pos
        self.pos.setY(self.pos.y()+20)
        menu = QtWidgets.QMenu(self)
        explorerMenu = menu.addAction('Open in Explorer')
        explorerMenu.triggered.connect(self.open_folder)
        menu.addSeparator()

        if self.explorerWidget.selectionModel().selectedIndexes():
            file_select = self.explorerWidget.selectionModel().selectedIndexes()[0].data()
            name, ext = os.path.splitext(file_select)

            if ext in Config.fileExt and config.isMaya:
                import_menu = menu.addAction('Import')
                referecne_menu = menu.addAction('Reference')

                import_menu.triggered.connect(self.importToMaya)
                referecne_menu.triggered.connect(self.referenceToMaya)

            elif ext in Config.imgExt:
                view_menu = menu.addAction('View')
                view_menu.triggered.connect(self.view_image)

        menu.popup(self.explorerWidget.mapToGlobal(self.pos))

    def clearPosSelect(self,pos):
        if not self.explorerWidget.indexAt(pos).data():
            self.explorerWidget.selectionModel().clearSelection()

    def importToMaya(self):
        path = self.fsModel.filePath(self.explorerWidget.selectionModel().selectedIndexes()[0])
        hook.import_file(path)

    def referenceToMaya(self):
        file_select = self.explorerWidget.selectionModel().selectedIndexes()[0].data()
        name, ext = os.path.splitext(self.file_select)
        path = self.fsModel.filePath(self.explorerWidget.selectionModel().selectedIndexes()[0])
        hook.reference_file(name,path)

    def view_image(self):
        path = self.fsModel.filePath(self.explorerWidget.selectionModel().selectedIndexes()[0])
        os.startfile(path)

    def open_folder(self):
        if self.explorerWidget.selectionModel().selectedIndexes():
            path = self.fsModel.filePath(self.explorerWidget.selectionModel().selectedIndexes()[0])
        else:
            path = self.fsModel_path

        if os.path.isdir(path):
            os.startfile(path)

        elif os.path.isfile(path):
            name,file = os.path.split(path)
            os.startfile(name)

    def clear_layout(self, layout):
        while layout.count():
            item = layout.takeAt(0)
            widget = item.widget()
            if widget is not None:
                widget.deleteLater()

class ExplorerWidget(QtWidgets.QTreeView):
    dropped = QtCore.Signal(list)
    def __init__(self ,parent=None):
        super(ExplorerWidget, self).__init__(parent)
        self.setDragEnabled(True)
        self.setAcceptDrops(True)
        self.setDropIndicatorShown(True)


    def dropEvent(self, event):
        super(ExplorerWidget, self).dropEvent(event)
        root = self.model().fileInfo(self.rootIndex()).absoluteFilePath()

        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            ix = self.indexAt(event.pos())
            pathDir = self.model().filePath(ix)
            urlLocals = [url for url in event.mimeData().urls() if url.isLocalFile()]
            for urlLocal in urlLocals:
                links = []
                path = urlLocal.toLocalFile()
                info = QtCore.QFileInfo(path)

                source = info.absoluteFilePath()
                destination = QtCore.QDir(pathDir).filePath(info.fileName())

                if not QtCore.QFile(destination).exists():
                    if not os.path.isfile(destination) and not os.path.isdir(destination):
                        if './' in destination:
                            file = destination.split('/')[-1]
                            destination = os.path.join(root,file)

                if os.path.isdir(source):
                    if not os.path.isdir(destination):
                        self.result = admin.win_copy_directory(source,destination)
                        dialog.MessageBox.success('finish','copy directory by admin')

                elif os.path.isfile(source):
                    if not os.path.isfile(destination):
                        self.result = admin.win_copy_file(source,destination)
                        dialog.MessageBox.success('finish','copy file by admin')

                print 'source',source
                print 'destination',destination
        else:
            event.ignore()

    def dragEnterEvent(self, event):
        root = self.model().fileInfo(self.rootIndex()).absoluteFilePath()
        if event.mimeData().hasUrls:
            event.accept()
            ix = self.indexAt(event.pos())
            pathDir = self.model().filePath(ix)
            if root in pathDir:
                self.setDragDropMode(QtWidgets.QAbstractItemView.InternalMove)
            else:
                self.setDragDropMode(QtWidgets.QAbstractItemView.DragDrop)
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()