import os
import sys

try: 
	import maya.cmds as mc 
except ImportError: 
	pass 

def reference_file(namespace, path):
	return mc.file(path, r=True, ns=namespace)

def import_file(path):
	return mc.file(path,  i=True, options = 'v=0', pr = True, loadReferenceDepth = 'all')