_title = 'Riff MattePaint'
_version = 'v.0.0.1'
_des = 'init'
uiName = 'RFMattePaint'

#Import python modules
import sys
import os
import getpass
import json
import logging

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# import config
import rf_config as config
from rf_utils import log_utils
# sg

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui
from rf_utils.ui import stylesheet
import ui
import re 
from rf_utils import register_shot
from functools import partial 
from rf_utils.sg import sg_process
from rf_utils import file_utils
from rf_utils.context import context_info
from rf_utils.widget import dialog

class RFMattePaint(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #SetUp Window
        super(RFMattePaint, self).__init__(parent)
        self.ui = ui.MattePaintExportUI(parent)
        self.setObjectName(uiName)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.setCentralWidget(self.ui)
        self.resize(900, 300)
        self.init_ui()
        self.__init_signal()
        self.source_folder = ''
        self.source_render_folder = ''

    def init_ui(self):
    	self.add_matte_paint()

    def __init_signal(self):
        self.ui.publish_pushButton.clicked.connect(self.publish) 
        self.ui.matte_paint_comboBox.currentIndexChanged.connect(self.fetch_asset_context)
        self.ui.file_tex_comboBox.currentIndexChanged.connect(self.set_display_source)
        self.ui.department_comboBox.currentIndexChanged.connect(self.fetch_asset_context)

    def list_matte_paint(self):
        type_filters = [['sg_asset_type', 'is', 'mattepaint']]
        list_matte_asset = sg_process.get_assets('SevenChickMovie', type_filters)
        return list_matte_asset

    def add_matte_paint(self):
        list_matte = self.list_matte_paint()
        for asset in list_matte:
            self.ui.matte_paint_comboBox.addItem(asset['code'],asset)


    def get_texture_path (self, entity): #entity example {'code': 'matteMonas_test','id': 6750,'sg_asset_type': 'mattepaint','sg_episodes': None, 'sg_subtype': 'main','type': 'Asset'}]
        from rf_utils.sg import sg_utils
        sg = sg_utils.sg
        
        context = context_info.Context()
        context.use_sg(sg, project='SevenChickMovie', entityType=entity['type'], entityName=entity['code'], entityId=entity['id'])
        asset = context_info.ContextPathInfo(context=context)
        asset.context.update(look = 'main')
        if self.ui.department_comboBox.currentText() == 'Art':
            return asset.path.preview_texture().abs_path()
        elif self.ui.department_comboBox.currentText() == 'Render':
            return asset.path.publish_texture().abs_path()
        

    def fetch_asset_context(self):
        cur_matte_paint = self.ui.matte_paint_comboBox.itemData( self.ui.matte_paint_comboBox.currentIndex() ) ## entity from shotgun##
        cur_depart = self.ui.department_comboBox.currentText()
        self.source_folder = self.get_texture_path(cur_matte_paint)
        print self.source_folder,'rest'
        self.fetch_texure_file(self.source_folder)

        # 
    def fetch_texure_file(self, path_tex):
        print path_tex
        if os.path.exists(path_tex):
            all_file = os.listdir(path_tex)
            texture_list = []
            self.ui.file_tex_comboBox.clear()
            for file_tex in all_file:
                if file_tex.endswith('.jpg') or file_tex.endswith('.png'):
                    texture_list.append(os.path.join(path_tex, file_tex))
                    self.ui.file_tex_comboBox.addItem(file_tex)
            return texture_list

    def set_display_source(self):
        file_tex_name = self.ui.file_tex_comboBox.currentText()
        cur_depart = self.ui.department_comboBox.currentText()
        # if cur_depart == 'Render':
        path = os.path.join(self.source_folder,file_tex_name).replace('\\', '/')
        print path,'picture'
        self.ui.source_display.set_display(path)


    def publish(self):
        send_file = self.ui.output_display.currentPath
        publ_hero_folder, output_version_folder = self.fetch_asset_dst() ########## got hero file
        print publ_hero_folder, output_version_folder 
        ######## next find output_version_folder
        ####hard_code
        print output_version_folder,'ov'
        ####################

        file_name = self.ui.file_tex_comboBox.currentText()
        #########
        output_version_folder = self.check_exists(output_version_folder)
        publ_hero_folder = self.check_exists(publ_hero_folder)
        file_utils.copy(send_file, os.path.join(publ_hero_folder, file_name))
        file_utils.copy(send_file, os.path.join(output_version_folder, file_name))

        dialog.MessageBox.success('Complete', 'Publish %s complete' % file_name)

    def publish_render(self):
        print self.ui.source_display.currentPath

    def fetch_asset_dst(self):
        cur_matte_paint = self.ui.matte_paint_comboBox.itemData( self.ui.matte_paint_comboBox.currentIndex() ) ## entity from shotgun##
        cur_depart = self.ui.department_comboBox.currentText()
        self.publ_hero_folder, self.output_version_folder = self.get_texture_path_dst(cur_matte_paint)

        return self.publ_hero_folder, self.output_version_folder

    def get_texture_path_dst (self, entity, version =''): #entity example {'code': 'matteMonas_test','id': 6750,'sg_asset_type': 'mattepaint','sg_episodes': None, 'sg_subtype': 'main','type': 'Asset'}]
        from rf_utils.context import context_info
        from rf_utils.sg import sg_utils
        sg = sg_utils.sg
        
        context = context_info.Context()
        context.use_sg(sg, project='SevenChickMovie', entityType=entity['type'], entityName=entity['code'], entityId=entity['id'])
        asset = context_info.ContextPathInfo(context=context)
        asset.context.update(look = 'main')
        file_name = self.ui.file_tex_comboBox.currentText()
        if version:
            asset.context.update(publishVersion= version)
        if self.ui.department_comboBox.currentText() == 'Art':
            asset.context.update(process = 'main', step ='design')
            list_outout = [asset.path.publish_texture().abs_path()]
            asset.context.update(process = file_name)
            version_path = self.increment_version(asset.path.publish_output().abs_path().split('none')[0])
            list_outout.append(version_path)
            return list_outout #outputImg
            # return asset.path.publish_texture().abs_path(), asset.path.output_hero().abs_path() #outputImg
        elif self.ui.department_comboBox.currentText() == 'Render':
            asset.context.update(process = 'main', step ='light')
            list_outout = [asset.path.preview_texture().abs_path()]
            asset.context.update(process = file_name)
            version_path = self.increment_version(asset.path.publish_output().abs_path().split('none')[0])
            print version_path,'faaaaaaaa'
            list_outout.append(version_path)

            return list_outout #outputImg

    def increment_version(self, file_path, file_name='v001'):
        file_path = self.check_exists(file_path)
        all_file = os.listdir(file_path)
        print file_path, all_file
        asset = context_info.ContextPathInfo(path =file_path)
        cur_depart = self.ui.department_comboBox.currentText()
        if cur_depart == 'Render':
            step = 'light'
        elif cur_depart == 'Art':
            step = 'design'
        if not  file_name in all_file:
            asset.context.update(step =step, res='md', look='main',publishVersion ='v001')
            return asset.path.publish_output().abs_path()

        else:
            import re
            increment_count=[]
            for file in all_file:
                increment = re.findall ( 'v' + '[0-9]{3}', file ) ;
                if increment:
                    increment_count.append(increment[0])
            version_lastest = int(max(increment_count).split('v')[1])
            new_version = 'v%03d'%(version_lastest+1)
            asset.context.update(step =step, res='md', look='main',publishVersion =new_version)
            return self.check_exists(asset.path.publish_output().abs_path())

    def check_exists(self, path):
        exists = os.path.exists(path)
        if exists:
            return path
        else:
            os.makedirs(path)
            return path



def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = RFMattePaint(maya_win.getMayaWindow())
        myApp.show()
        return myApp
        
    else: 
        logger.info('Run in standalone\n')
        app = QtWidgets.QApplication.instance()
        if not app: 
            app = QtWidgets.QApplication(sys.argv)
        myApp = RFMattePaint()
        myApp.show()
        stylesheet.set_default(app)
        sys.exit(app.exec_())

if __name__ == '__main__':
    show()