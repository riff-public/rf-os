import os
import sys

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from rf_utils.widget import display_widget

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_utils import icon


class MattePaintExportUI(QtWidgets.QWidget):
	def __init__(self, parent=None):
		super(MattePaintExportUI, self).__init__(parent)
		
		self.verticalLayout = QtWidgets.QVBoxLayout()
		self.matte_paint_label = QtWidgets.QLabel('MattePaint Name')
		self.matte_paint_comboBox = QtWidgets.QComboBox()
		
		self.department_label = QtWidgets.QLabel('Department')
		self.department_comboBox = QtWidgets.QComboBox()
		self.department_comboBox.addItem('Render')
		self.department_comboBox.addItem('Art')        

		self.file_tex_label = QtWidgets.QLabel('file Texture')
		self.file_tex_comboBox = QtWidgets.QComboBox()


		self.verticalLayout.addWidget(self.matte_paint_label)
		self.verticalLayout.addWidget(self.matte_paint_comboBox)
		self.verticalLayout.addWidget(self.department_label)
		self.verticalLayout.addWidget(self.department_comboBox)
		self.verticalLayout.addWidget(self.file_tex_label)
		self.verticalLayout.addWidget(self.file_tex_comboBox)

		self.horizontalLayout = QtWidgets.QHBoxLayout()
		self.source_display = self.preview_widget()
		self.horizontalLayout.addWidget(self.source_display)

		self.left_layout = QtWidgets.QVBoxLayout()

		self.left_layout.addLayout(self.verticalLayout)
		# self.left_layout.addLayout(self.horizontalLayout)
		self.mid_layout = QtWidgets.QVBoxLayout()
		self.mid_layout.addLayout(self.horizontalLayout)

		self.publish_pushButton = QtWidgets.QPushButton('Publish')
		self.right_layout = QtWidgets.QVBoxLayout()
		self.verticalSpacer = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)

		self.output_display = self.preview_widget()
		self.right_layout.addWidget(self.output_display)
		self.right_layout.addWidget(self.publish_pushButton)



		self.centralwidget = QtWidgets.QHBoxLayout()
		self.centralwidget.addLayout(self.left_layout)
		self.centralwidget.addLayout(self.mid_layout)
		self.centralwidget.addLayout(self.right_layout)
		self.setLayout(self.centralwidget)

	def preview_widget(self):
		# display
		self.displayWidget = display_widget.Display()
		self.displayWidget.set_label('JPG Here')
		return self.displayWidget