#!/usr/bin/env python
# -- coding: utf-8 --

# v.beta - test version
# v.1.0.0 - initial release
# v.1.0.1 - add sorting, add All tab
# v.1.0.2 - order task list by step, change image per row to 4
# v.1.0.3 - add modified, created date in asset tooltip, change texts, fix bugs
# v.1.1.0 - support video file
# v.1.2.0 - support displaying of parent designs
# v.1.3.0 - support view/edit/create SG brief
#         - optimize tabs
# v.1.3.1 - fix text errors
#         - fix image extension bug
#         - create brief: add task selection
# v.1.3.2 - add splitter in brief tab Note TextEdit
# v.1.4.0 - Add asset properties on right-click
#         - Fix bug: images not display when selecting asset task and related asset
#           task sharing the same taskname
#         - Add asset name in image&video bottom text for related asset display
# v.1.4.1 - Bug fix: QTreeWidget.setResizeMode not compatible with PySide2 in Maya 
# v.1.4.2 - Add delete asset 
# v.1.4.3 - Add open directory menu on right click of an asset
# v.1.4.4 - Make AddBriefWin in brief tab stored step and task seleciton from last time
#         - Fix entity not update in AddBriefWin
#         - Make AddBriefWin disabled when no task available
# v.1.5.0 - Add brief attachment image save
# v.1.5.1 - Add app icon to brief save dialog

_title = 'RF Asset Monitor'
_version = '1.5.1'
_des = ''
uiName = 'AssetMonitorUi'

#Import python modules
import sys
import os
import logging
import getpass
import traceback
import json
import subprocess
from collections import OrderedDict, defaultdict
from functools import partial
from glob import glob

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '{}/core'.format(os.environ.get('RFSCRIPT'))
sys.path.append(core)

# import config
import rf_config as config

from rf_utils.context import context_info
from rf_utils import log_utils
from rf_utils import user_info
from rf_utils.ui import load
from rf_utils.ui import stylesheet
from rf_utils import thread_pool
from rf_utils import admin

user = '{}-{}'.format(config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

# maya & module function
from . import ui
from . import tabs
from rf_utils.sg import sg_process
from rf_utils.context import context_info
ui.sg = sg_process.sg

if config.isNuke:
    import nuke
    import nukescripts
elif config.isMaya:
    import maya.cmds as mc

modulePath = sys.modules[__name__].__file__.replace('\\', '/')
moduleDir  = os.path.dirname(modulePath)
appModuleDir = os.path.dirname(moduleDir)

class TabConfig:
    """ name of tabs module """
    activeTab = 'published'
    allowedTabs = ['published', 'brief']
    tabNameMap = {}

class AssetMonitor(QtWidgets.QMainWindow):
    tabChanged = QtCore.Signal(bool)
    def __init__(self, parent=None,):
        #Setup Window
        super(AssetMonitor, self).__init__(parent)
        self.entity = None
        self.property_win = None
        user = user_info.User()
        if user.is_producer() or user.is_admin() or user.is_supervisor():
            self.is_admin = True
        else:
            self.is_admin = False

        # ui read
        uiFile = '{}/ui.py'.format(moduleDir)
        self.ui = ui.AssetMonitorUi()
        self.w = 1190
        self.h = 685

        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.resize(self.w, self.h)
        self.setWindowTitle('{} {} {}'.format(_title, _version, _des))
        self.setWindowIcon(QtGui.QIcon('{}/icons/app_icon.png'.format(moduleDir)))

        self.setup_ui()
        self.init_functions()
        self.init_signals()
        self.set_default()

    def closeEvent(self, event):
        '''
        Closes all tab widgets before actually close itself triggering each tab closeEvents
        '''
        for i in xrange(self.ui.tab_widget.count()):
            widget = self.ui.tab_widget.widget(i)
            widget.close()

    def setup_ui(self):
        # set tabs
        self.ui.tab_widget = self.add_tab_widget()
        self.ui.sub_layout.setStretch(0, 1)
        self.ui.sub_layout.setStretch(1, 6)

        # toolTips
        self.ui.current_button.setToolTip('Jump to current scene.')
        self.ui.refresh_button.setToolTip('Refresh current tab.')

    def init_signals(self):
        self.ui.asset_widget.entitySelected.connect(self.asset_selected)
        self.ui.asset_widget.customContextMenuRequested.connect(self.asset_right_clicked)
        self.ui.current_button.clicked.connect(self.jump_to_current_scene)
        self.ui.refresh_button.clicked.connect(self.refresh_tab)
        self.ui.tab_widget.currentChanged.connect(self.tab_changed)
        self.ui.sort_alp_radioButton.clicked.connect(self.sort_changed)
        self.ui.sort_mod_radioButton.clicked.connect(self.sort_changed)
        self.ui.sort_latestver_radioButton.clicked.connect(self.sort_changed)

    def asset_right_clicked(self, pos):
        asset_tree_widget = self.ui.asset_widget.entity_widget.treeWidget
        item = asset_tree_widget.itemAt(asset_tree_widget.mapFrom(self.ui.asset_widget, pos))
        # right clicked on an item
        if not item:
            return
        # get asset entity from item data
        entity = item.data(QtCore.Qt.UserRole, 0)
        rightClickMenu = QtWidgets.QMenu(self)

        # properties menu
        properties_action = QtWidgets.QAction('Properties...', self)
        properties_action.setIcon(QtGui.QIcon('{}/icons/gear_icon.png'.format(moduleDir)))
        properties_action.triggered.connect(partial(self.thread_asset_property_called, entity))
        rightClickMenu.addAction(properties_action)

        rightClickMenu.addSeparator()

        # ----- open work directory menu
        open_work_dir_menu = QtWidgets.QMenu('Work directory',  self)
        open_work_dir_menu.setIcon(QtGui.QIcon('{}/icons/folder_icon.png'.format(moduleDir)))
        rightClickMenu.addMenu(open_work_dir_menu)
        # design work dir
        design_work_action = QtWidgets.QAction('Design', self)
        design_work_action.setIcon(QtGui.QIcon('{}/icons/folder_icon.png'.format(moduleDir)))
        design_work_action.triggered.connect(partial(self.open_design_work_dir, entity))
        open_work_dir_menu.addAction(design_work_action)
        # asset work dir
        asset_work_action = QtWidgets.QAction('Asset', self)
        asset_work_action.setIcon(QtGui.QIcon('{}/icons/folder_icon.png'.format(moduleDir)))
        asset_work_action.triggered.connect(partial(self.open_asset_work_dir, entity))
        open_work_dir_menu.addAction(asset_work_action)

        # ----- open publ directory menu
        open_publ_dir_menu = QtWidgets.QMenu('Publish directory',  self)
        open_publ_dir_menu.setIcon(QtGui.QIcon('{}/icons/star_icon.png'.format(moduleDir)))
        rightClickMenu.addMenu(open_publ_dir_menu)
        # design publ dir
        design_publ_action = QtWidgets.QAction('Design', self)
        design_publ_action.setIcon(QtGui.QIcon('{}/icons/star_icon.png'.format(moduleDir)))
        design_publ_action.triggered.connect(partial(self.open_design_publ_dir, entity))
        open_publ_dir_menu.addAction(design_publ_action)
        # asset publ dir
        asset_publ_action = QtWidgets.QAction('Asset', self)
        asset_publ_action.setIcon(QtGui.QIcon('{}/icons/star_icon.png'.format(moduleDir)))
        asset_publ_action.triggered.connect(partial(self.open_asset_publ_dir, entity))
        open_publ_dir_menu.addAction(asset_publ_action)

        if self.is_admin:
            rightClickMenu.addSeparator()
            # delete menu
            delete_action = QtWidgets.QAction('Delete Asset', self)
            delete_action.setIcon(QtGui.QIcon('{}/icons/delete_icon.png'.format(moduleDir)))
            delete_action.triggered.connect(partial(self.asset_delete_called, entity))
            rightClickMenu.addAction(delete_action)

        # show menu
        rightClickMenu.exec_(self.ui.asset_widget.mapToGlobal(pos))

    def open_design_work_dir(self, sgEntity):
        context = context_info.Context()
        context.use_sg(sg_process.sg, sgEntity.get('project')['name'], 'asset', entityName=sgEntity.get('code'))
        entity = context_info.ContextPathInfo(context=context)
        entity.context.update(entityType='design')
        dirname = entity.path.name().abs_path()
        if os.path.exists(dirname):
            subprocess.Popen(r'explorer {}'.format(dirname.replace('/', '\\')))

    def open_asset_work_dir(self, sgEntity):
        context = context_info.Context()
        context.use_sg(sg_process.sg, sgEntity.get('project')['name'], 'asset', entityName=sgEntity.get('code'))
        entity = context_info.ContextPathInfo(context=context)
        dirname = entity.path.name().abs_path()
        if os.path.exists(dirname):
            subprocess.Popen(r'explorer {}'.format(dirname.replace('/', '\\')))

    def open_design_publ_dir(self, sgEntity):
        context = context_info.Context()
        context.use_sg(sg_process.sg, sgEntity.get('project')['name'], 'asset', entityName=sgEntity.get('code'))
        entity = context_info.ContextPathInfo(context=context)
        entity.context.update(entityType='design')
        dirname = entity.path.name(workspace=context_info.ContextKey.publish).rel_publ().abs_path()
        if os.path.exists(dirname):
            subprocess.Popen(r'explorer {}'.format(dirname.replace('/', '\\')))

    def open_asset_publ_dir(self, sgEntity):
        context = context_info.Context()
        context.use_sg(sg_process.sg, sgEntity.get('project')['name'], 'asset', entityName=sgEntity.get('code'))
        entity = context_info.ContextPathInfo(context=context)
        dirname = entity.path.name(workspace=context_info.ContextKey.publish).rel_publ().abs_path()
        if os.path.exists(dirname):
            subprocess.Popen(r'explorer {}'.format(dirname.replace('/', '\\')))

    def asset_delete_called(self, sgEntity):
        sg = sg_process.sg
        project = sgEntity.get('project')['name']
        entityType = 'asset'
        entityName = sgEntity.get('code')
        assetType = sgEntity.get('sg_asset_type')
        if not project or not entityName:
            return

        # pop up to confirm user
        qmsgBox = QtWidgets.QMessageBox(self)
        qmsgBox.setWindowTitle('Warning')
        qmsgBox.setText('Delete Asset: {} - {}\nAre you sure?'.format(assetType, entityName))
        qmsgBox.setIcon(QtWidgets.QMessageBox.Warning)
        # add button
        qmsgBox.addButton('  Yes  ', QtWidgets.QMessageBox.AcceptRole)
        qmsgBox.addButton('  No  ', QtWidgets.QMessageBox.RejectRole)

        answer = qmsgBox.exec_()
        if answer == 0: 
            QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)

            context = context_info.Context()
            context.use_sg(sg, project, entityType, entityName=entityName)

            # ----- delete sg asset
            sg.delete('Asset', int(context.id))
            
            # ----- move structure folders
            # asset path 
            delete_dir = '.delete'
            entity = context_info.ContextPathInfo(context=context)
            # asset work: 'P:/Hanuman/asset/work/prop'
            asset_work_dir = entity.path.name().abs_path()
            bk_asset_work_dir = '{}/{}/{}'.format(entity.path.type().abs_path(), delete_dir, entityName)
            self.backup_directory(src=asset_work_dir, des=bk_asset_work_dir)

            # asset publ: 'P:/Hanuman/asset/publ/prop'
            asset_publ_dir = entity.path.name(workspace=context_info.ContextKey.publish).rel_publ().abs_path()
            bk_asset_publ_dir = '{}/{}/{}'.format(entity.path.type(workspace=context_info.ContextKey.publish).rel_publ().abs_path(), delete_dir, entityName)
            self.backup_directory(src=asset_publ_dir, des=bk_asset_publ_dir)

            # asset publ R: 'R:/Hanuman/asset/publ/prop'
            asset_version_dir = entity.path.name(workspace=context_info.ContextKey.publish).rel_version().abs_path()
            bk_asset_version_dir = '{}/{}/{}'.format(entity.path.type(workspace=context_info.ContextKey.publish).rel_version().abs_path(), delete_dir, entityName)
            self.backup_directory(src=asset_version_dir, des=bk_asset_version_dir)

            # design work: 'P:/Hanuman/design/work/prop'
            entity.context.update(entityType='design')
            design_work_dir = entity.path.name().abs_path()
            bk_design_work_dir = '{}/{}/{}'.format(os.path.split(design_work_dir)[0], delete_dir, entityName)
            self.backup_directory(src=design_work_dir, des=bk_design_work_dir)

            # design publ: 'P:/Hanuman/design/publ/prop'
            design_publ_dir = entity.path.name(workspace=context_info.ContextKey.publish).rel_publ().abs_path()
            bk_design_publ_dir = '{}/{}/{}'.format(os.path.split(design_publ_dir)[0], delete_dir, entityName)
            self.backup_directory(src=design_publ_dir, des=bk_design_publ_dir)

            # design publ R: 'R:/Hanuman/design/publ/prop'
            design_version_dir = entity.path.name(workspace=context_info.ContextKey.publish).rel_version().abs_path()
            bk_design_version_dir = '{}/{}/{}'.format(os.path.split(design_version_dir)[0], delete_dir, entityName)
            self.backup_directory(src=design_version_dir, des=bk_design_version_dir)

            self.ui.asset_widget.refresh_current_project()
            QtWidgets.QApplication.restoreOverrideCursor()

    def backup_directory(self, src, des):
        if not os.path.exists(src):
            return
        delete_dir = os.path.dirname(des)
        if not os.path.exists(delete_dir):
            admin.makedirs(delete_dir)

        result = admin.rename(src, des, check_result=True)
        print 'Backed up: {} ---> {}'.format(src, des)

    def thread_asset_property_called(self, entity):
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        worker = thread_pool.Worker(partial(self.get_asset_properties, entity))
        worker.signals.result.connect(self.show_asset_property_win)
        self.threadpool.start(worker)

    def get_asset_properties(self, entity):
        project = self.ui.asset_widget.project_widget.current_item()
        assets = self.ui.asset_widget.entities
        looks = sg_process.list_looks(entity=entity)
        users = user_info.SGUser().userEntities
        tasks = find_tasks(project=project['name'], entity=entity)
        return (entity, assets, looks, tasks, users)

    def show_asset_property_win(self, results, *args, **kwargs):
        entity, assets, looks, tasks, users = results
        self.property_win = ui.AssetPropertyUi(entity=entity, 
                                assets=assets, 
                                looks=looks, 
                                tasks=tasks,
                                users=users,
                                allow_edit=self.is_admin,
                                parent=self)
        self.property_win.tagEdited.connect(self.tag_edited)
        self.property_win.usedInEdited.connect(self.usedin_edited)
        self.property_win.parentsEdited.connect(self.parents_edited)
        self.property_win.relatedEdited.connect(self.related_edited)
        self.property_win.looksEdited.connect(self.looks_edited)
        self.property_win.dateEdited.connect(self.date_edited)
        self.property_win.assigneeEdited.connect(self.assignee_edited)
        self.property_win.show()

        QtWidgets.QApplication.restoreOverrideCursor()

    def tag_edited(self, tags):
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        existing_tags = sg_process.sg.find('Tag', [], ['name'])
        existing_tag_names = [str(t['name']) for t in existing_tags]
        valid_tags = []
        for tagName, tag in tags:
            if str(tagName) not in existing_tag_names:
                tag = sg_process.sg.create('Tag', {'name': tagName})
            valid_tags.append(tag)
        data = {'tags': valid_tags}
        result = sg_process.sg.update('Asset', self.entity['id'], data)

        # update current entity
        self.entity.update(result)
        self.property_win.entity = self.entity
        # update property win
        self.property_win.update_tags()
        self.property_win.unlock_displays()
        # update item data
        self.ui.asset_widget.update_one_entity(new_entity=self.entity)
        # reselect item
        self.asset_selected(entity=self.entity)
        QtWidgets.QApplication.restoreOverrideCursor()

    def looks_edited(self, looks):
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        new_looks = dict(looks)
        existing_looks = dict([(l.get('sg_name'), l) for l in sg_process.list_looks(self.entity)])

        # adding new look
        if len(existing_looks) < len(looks):
            for lookName, look in looks:
                if lookName not in existing_looks:
                    sg_process.create_look(entity=self.entity, lookName=lookName)
        else:  # removing look
            for lookName, look in existing_looks.iteritems():
                if lookName not in new_looks:
                    sg_process.remove_look(entity=self.entity, lookName=lookName)

        # update property win
        self.property_win.looks = sg_process.list_looks(self.entity)
        self.property_win.update_looks()
        self.property_win.unlock_displays()

        # reselect item
        self.asset_selected(entity=self.entity)
        QtWidgets.QApplication.restoreOverrideCursor()

    def usedin_edited(self, used_ins):
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        data = {'sg_scenes': [], 'sequences': [], 'shots': []}
        for name, entity in used_ins:
            entity_type = entity.get('type')
            # episodes
            if entity_type == 'Scene':
                data['sg_scenes'].append(entity)
            # sequences
            elif entity_type == 'Sequence':
                data['sequences'].append(entity)
            else:
                # shots
                sg_shot_type = entity.get('sg_shot_type')
                if sg_shot_type == 'Sequence':
                    data['sequences'].append(entity.get('sg_sequence'))
                else:
                    data['shots'].append(entity)
        result = sg_process.sg.update('Asset', self.entity['id'], data)

        # update current entity
        self.entity.update(result)
        self.property_win.entity = self.entity
        # update property win
        self.property_win.update_usedin()
        self.property_win.unlock_displays()
        # update item data
        self.ui.asset_widget.update_one_entity(new_entity=self.entity)
        # reselect item
        self.asset_selected(entity=self.entity)
        QtWidgets.QApplication.restoreOverrideCursor()

    def parents_edited(self, parents):
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        data = {'parents': [p[1] for p in parents]}
        result = sg_process.sg.update('Asset', self.entity['id'], data)

        # update current entity
        self.entity.update(result)
        self.property_win.entity = self.entity
        # update property win
        self.property_win.update_parents()
        self.property_win.unlock_displays()
        # update item data
        self.ui.asset_widget.update_one_entity(new_entity=self.entity)
        # reselect item
        self.asset_selected(entity=self.entity)
        QtWidgets.QApplication.restoreOverrideCursor()

    def related_edited(self, related):
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        data = {'sg_link_designs': [r[1] for r in related]}
        result = sg_process.sg.update('Asset', self.entity['id'], data)

        # update current entity
        self.entity.update(result)
        self.property_win.entity = self.entity
        # update property win
        self.property_win.update_related()
        self.property_win.unlock_displays()
        # update item data
        self.ui.asset_widget.update_one_entity(new_entity=self.entity)
        # reselect item
        self.asset_selected(entity=self.entity)
        QtWidgets.QApplication.restoreOverrideCursor()

    def date_edited(self, edits):
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        task, field, date, expand_items = edits
        data = {field: date}
        result = sg_process.sg.update('Task', task['id'], data)

        # update property win
        update_task_id = result.get('id')
        self.property_win.update_tasks(task_id=update_task_id, value=result)
        self.property_win.update_tasks_table(expand_items=expand_items)
        self.property_win.unlock_displays()

        # reselect item
        self.asset_selected(entity=self.entity)
        QtWidgets.QApplication.restoreOverrideCursor()

    def assignee_edited(self, edits):
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        task, assignees, expand_items = edits
        data = {'task_assignees': [a[1] for a in assignees]}
        result = sg_process.sg.update('Task', task['id'], data)

        # update property win
        update_task_id = result.get('id')
        self.property_win.update_tasks(task_id=update_task_id, value=result)
        self.property_win.update_tasks_table(expand_items=expand_items)
        self.property_win.unlock_displays()

        # reselect item
        self.asset_selected(entity=self.entity)
        QtWidgets.QApplication.restoreOverrideCursor()

    def set_default(self):
        # hide current scene button in standalone mode
        if not config.isMaya and not config.isNuke:
            self.ui.current_button.hide()
        else:
            # try to jump to current scene
            self.jump_to_current_scene()

    def sort_changed(self):
        if self.ui.sort_alp_radioButton.isChecked():
            func = lambda i: i.get('code', None)
            reverse = False
        elif self.ui.sort_mod_radioButton.isChecked():
            func = lambda i: i.get('updated_at', None)
            reverse = True
        elif self.ui.sort_latestver_radioButton.isChecked():
            func = lambda i: i.get('created_at', None)
            reverse = True

        self.ui.asset_widget.sort_items(sort_func=func, sort_reverse=reverse)

    def init_functions(self):
        self.ui.asset_widget.refresh()

    @classmethod
    def init_from_context(cls, context, parent=None):
        app = cls(parent=parent)
        logger.debug('Class initialized from context')
        app.jump_to_asset(project=project, filter_type=filter_type, asset=asset) 
        return app

    def jump_to_current_scene(self):
        if config.isMaya:
            context = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))
            self.jump_to_context(context=context)
        elif config.isNuke:
            context = context_info.ContextPathInfo(path=nuke.root().name())
            self.jump_to_context(context=context)

    def jump_to_context(self, context):
        # asset contexts
        project = context.project
        entity_type = context.entity_type
        filter_type = context.type
        asset = context.name

        if entity_type == 'asset' and project and filter_type and asset:
            logger.debug('Jump to assset: {} {} {}'.format(project, filter_type, asset))
            # setup app
            self.ui.asset_widget.blockSignals(True)
            # set project
            self.ui.asset_widget.project_widget.set_current_item(project)
            # set entity type
            index = self.ui.asset_widget.entitytype_comboBox.findText('Asset', QtCore.Qt.MatchFixedString)
            self.ui.asset_widget.entitytype_comboBox.setCurrentIndex(index)
            # set type
            index = self.ui.asset_widget.filtertype_comboBox.findText(filter_type, QtCore.Qt.MatchFixedString)
            self.ui.asset_widget.filtertype_comboBox.setCurrentIndex(index)
            self.ui.asset_widget.blockSignals(False)
            # set tag to all tags
            self.ui.asset_widget.tag_widget.set_current_index(0)
            # set asset
            self.ui.asset_widget.entity_widget.blockSignals(True)
            self.ui.asset_widget.entity_widget.set_current(displayText=asset, signal=False)
            self.ui.asset_widget.entity_widget.blockSignals(False)
            # explicit call to asset selected
            entity = self.ui.asset_widget.entity_widget.current_item()
            if entity:
                self.asset_selected(entity=entity)

    def add_tab_widget(self):
        """ looping for each tabs modules and compare to allowedTab setting,
            add to tab widgets """
        
        modules = tabs.get_module()
        activeIndex = 0
        self.tabModules = dict()
        self.threadpool = QtCore.QThreadPool()
        self.threadpool.setMaxThreadCount(1)
        tabNames = []

        for tabName in TabConfig.allowedTabs:
            for index, name in enumerate(modules):
                title = TabConfig.tabNameMap.get(name) or name.capitalize()
                if name == tabName:
                    func = tabs.get_func(name)
                    reload(func)
                    widget = func.Ui(thread=self.threadpool, parent=self)
                    # connect tab changed signal so vid viewer update icon size when tab changed
                    self.tabChanged.connect(widget.trigger_update_size)

                    self.ui.tab_widget.addTab(widget, title)
                    if hasattr(widget, 'refresh'):
                        widget.refresh
                    if name == TabConfig.activeTab:
                        activeIndex = len(self.tabModules.keys())

                    self.tabModules[name] = widget
        # active tab
        self.ui.tab_widget.setCurrentIndex(activeIndex)
        return self.ui.tab_widget

    def asset_selected(self, entity):
        logger.debug('Asset selected: {}'.format(entity))

        """ asset selected, context changed """
        self.entity = entity
        # set unsync for all tabs
        self.unsync_tabs()
        # refresh current tab 
        self.refresh_tab()

    def refresh_tab(self):
        """ refresh current tab """
        widget = self.ui.tab_widget.currentWidget()
        widget.refresh(self.entity)
        widget.context_sync = True
        logger.debug('Refresh')

    def tab_changed(self): 
        """ tab only refresh if context has changed """
        widget = self.ui.tab_widget.currentWidget()
        if not widget.context_sync: 
            self.refresh_tab()
        else: 
            logger.debug('Not refresh')

        self.tabChanged.emit(True)

    def unsync_tabs(self): 
        """ unsync all tabs so it will refresh when tab changed """ 
        tab_count = self.ui.tab_widget.count()
        for i in range(tab_count): 
            widget = self.ui.tab_widget.widget(i)
            widget.context_sync = False 

def find_tasks(project, entity):
    filters = [['project.Project.name', 'is', project], ['entity', 'is', entity]]
    fields = ['content', 'id', 'step.Step.code', 'start_date', 'due_date', 'task_assignees']
    tasks = sg_process.sg.find('Task', filters, fields)
    results = OrderedDict()
    for task in tasks:
        step = task['step.Step.code']
        if step not in results:
            results[step] = []
        results[step].append(task)
    return results

def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        parent = maya_win.getMayaWindow()
        myApp = AssetMonitor(parent=parent)
        myApp.show()
    elif config.isNuke:
        from rf_nuke import nuke_win 
        # reload(nuke_win)
        logger.info('Run in Nuke\n')
        parent = nuke_win._nuke_main_window()
        nuke_win.deleteUI(uiName)
        myApp = AssetMonitor(parent=parent)
        myApp.show()
    else:
        logger.info('Run in standalone\n')
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        myApp = AssetMonitor(parent=None)
        myApp.show()
        stylesheet.set_default(app)
        sys.exit(app.exec_())
    return myApp


if __name__ == '__main__':
    show()
