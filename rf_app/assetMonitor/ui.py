#!/usr/bin/env python
# -- coding: utf-8 --

import os
import sys
import traceback
from datetime import datetime
from collections import OrderedDict, defaultdict
from functools import partial

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCompat

module_path = sys.modules[__name__].__file__
module_dir = os.path.dirname(module_path)
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils.widget import project_widget
from rf_utils.widget import entity_browser
from rf_utils.widget import hashtag_widget
from rf_utils import icon
sg = None


class Setting:
    refreshIcon = '{}/icons/refresh.png'.format(module_dir)
    currentIcon = '{}/icons/current_icon.png'.format(module_dir)

class AssetMonitorUi(QtWidgets.QWidget):
    status = QtCore.Signal(str)

    def __init__(self, parent=None):
        super(AssetMonitorUi, self).__init__(parent)
        self.layout = QtWidgets.QVBoxLayout()
        self.sub_layout = QtWidgets.QHBoxLayout()
        self.layout.addLayout(self.sub_layout)
        self.setLayout(self.layout)

        self.setup_widget()
        self.init_signals()
        self.init_functions()

    def setup_widget(self):
        # logo
        self.logo = QtWidgets.QLabel()
        self.logo.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))
        self.layout.insertWidget(0, self.logo)

        # button layout
        self.button_layout = QtWidgets.QHBoxLayout()
        self.layout.insertLayout(1, self.button_layout)
        # spacer
        button_spacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.button_layout.addItem(button_spacer)
        # current button
        self.current_button = QtWidgets.QPushButton()
        self.current_button.setMaximumSize(32, 32)
        self.current_button.setIcon(QtGui.QIcon(Setting.currentIcon))
        self.button_layout.addWidget(self.current_button)
        # refresh button
        self.refresh_button = QtWidgets.QPushButton()
        self.refresh_button.setMaximumSize(32, 32)
        self.refresh_button.setIcon(QtGui.QIcon(Setting.refreshIcon))
        self.button_layout.addWidget(self.refresh_button)
        
        # splitter
        self.mainSplitter = QtWidgets.QSplitter()
        self.mainSplitter.setOrientation(QtCore.Qt.Horizontal)

        self.navSplitWidget = QtWidgets.QWidget(self.mainSplitter)
        self.viewSplitWidget = QtWidgets.QWidget(self.mainSplitter)
        self.mainSplitter.setSizes([100, 300])
        self.mainSplitter.setStretchFactor(0, 0)
        self.mainSplitter.setStretchFactor(1, 1)

        self.sub_layout.addWidget(self.mainSplitter)

        # navigation bar
        self.nav_layout = QtWidgets.QVBoxLayout(self.navSplitWidget)
        self.nav_layout.setSpacing(7)
        
        # asset widget
        self.asset_widget = entity_browser.EntityTreeBrowser()
        self.asset_widget.search_lineEdit.setPlaceholderText('Search by name, tag or chapter...')
        self.asset_widget.filtertype_comboBox.setMinimumWidth(80)
        self.asset_widget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        # hide entity type, only asset
        self.asset_widget.entitytype_label.hide()
        self.asset_widget.entitytype_comboBox.hide()

        # set margins
        self.asset_widget.layout.setContentsMargins(0, 0, 0, 0)
        self.nav_layout.addWidget(self.asset_widget)

        # options layout
        self.options_layout = QtWidgets.QVBoxLayout()
        self.options_layout.setSpacing(3)
        self.options_layout.setContentsMargins(0, 0, 0, 0)
        self.nav_layout.addLayout(self.options_layout)

        # sort layout
        self.sort_groupBox = QtWidgets.QGroupBox()
        self.options_layout.addWidget(self.sort_groupBox)

        # sort radio buttons
        self.sort_layout = QtWidgets.QHBoxLayout(self.sort_groupBox)
        self.sort_layout.setSpacing(25)
        self.sort_layout.setContentsMargins(0, 0, 0, 0)

        spacerItem1 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.sort_layout.addItem(spacerItem1)

        self.sort_alp_radioButton = QtWidgets.QRadioButton('A-Z')
        self.sort_alp_radioButton.setChecked(True)
        self.sort_layout.addWidget(self.sort_alp_radioButton)

        self.sort_mod_radioButton = QtWidgets.QRadioButton('Modified')
        self.sort_layout.addWidget(self.sort_mod_radioButton)

        self.sort_latestver_radioButton = QtWidgets.QRadioButton('Created')
        self.sort_layout.addWidget(self.sort_latestver_radioButton)

        spacerItem2 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.sort_layout.addItem(spacerItem2)

        # view
        self.view_layout = QtWidgets.QVBoxLayout(self.viewSplitWidget)
        # tab widget
        self.tab_widget = QtWidgets.QTabWidget()
        self.view_layout.addWidget(self.tab_widget)

        self.nav_layout.setStretch(0, 1)
        self.nav_layout.setStretch(1, 0)
        self.layout.setStretch(0, 0)
        self.layout.setStretch(1, 0)
        self.layout.setStretch(2, 1)

    def init_signals(self):
        pass

    def init_functions(self):
        pass

class DateLineEdit(QtWidgets.QWidget):
    dateChanged = QtCore.Signal(QtCore.QDate)
    def __init__(self, parent=None):
        super(DateLineEdit, self).__init__(parent)
        self.allLayout = QtWidgets.QHBoxLayout()

        # lineEdit
        self.lineEdit = QtWidgets.QLineEdit()
        self.lineEdit.setAlignment(QtCore.Qt.AlignRight)
        self.lineEdit.setReadOnly(True)
        self.allLayout.addWidget(self.lineEdit)

        # button
        self.button = QtWidgets.QPushButton()
        self.button.setIcon(QtGui.QIcon('{}/icons/calendar_icon.png'.format(module_dir)))
        self.button.setFixedSize(QtCore.QSize(16, 16))
        self.allLayout.addWidget(self.button)
        self.button.clicked.connect(self.show_calendar)

        # calendar
        self.calendar = QtWidgets.QCalendarWidget(self)
        self.calendar._orig_date = None
        self.calendar.setWindowFlags(QtCore.Qt.Popup)
        self.calendar.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.calendar.clicked.connect(self.date_changed)

        self.setLayout(self.allLayout)
        self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.allLayout.setSpacing(0)
        self.allLayout.setStretch(0, 1)
        self.allLayout.setStretch(1, 0)

        self.setStyleSheet("background-color: rgba(0, 0, 0, 0); border: 0px;")

    def setDate(self, date):
        if date:
            date = datetime.strftime(date, '%d/%m/%y')
            # date = '{}/{}/{}'.format(str(date.year).zfill(2), str(date.month).zfill(2), str(date.day).zfill(2))
        else:
            date = ''
        self.lineEdit.setText(date)

    def clear(self):
        self.lineEdit.clear()

    def show_calendar(self):
        text = self.lineEdit.text()
        if text:
            date = datetime.strptime(text, '%d/%m/%y').date()
            qdate = QtCore.QDate(date.year, date.month, date.day)
        else:
            qdate = QtCore.QDate.currentDate()

        self.calendar.setSelectedDate(qdate)    
        self.calendar._orig_date = qdate                       
        btn_global_point = self.button.mapToGlobal(self.button.rect().topLeft())  
        win_global_point = self.mapToGlobal(self.rect().topLeft()) 

        self.calendar.move(btn_global_point.x(), btn_global_point.y())
        self.calendar.show()

    def set_edit_enabled(self, enable):
        if enable:
            self.button.show()
        else:
            self.button.hide()

    def date_changed(self, date=None):
        self.calendar.hide()
        if date != self.calendar._orig_date:
            self.dateChanged.emit(date)

class AssetPropertyUi(QtWidgets.QMainWindow):
    tagEdited = QtCore.Signal(list)
    usedInEdited = QtCore.Signal(list)
    parentsEdited = QtCore.Signal(list)
    relatedEdited = QtCore.Signal(list)
    looksEdited = QtCore.Signal(list)
    dateEdited = QtCore.Signal(list)
    assigneeEdited = QtCore.Signal(list)

    def __init__(self, entity, assets, looks, tasks, users, allow_edit, parent=None):
        super(AssetPropertyUi, self).__init__(parent)
        self.bg_col = (43, 43, 43)
        self.fg_col = (87, 87, 87)
        self.entity = entity
        self.assets = assets
        self.looks = looks
        self.tasks = tasks
        self.users = users
        self.allow_edit = allow_edit

        self.setupUi()
        self.init_signal()
        self.set_default()

    def setupUi(self):
        # main win
        self.main_widget = QtWidgets.QWidget()
        self.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.setCentralWidget(self.main_widget)
        self.resize(520, 645)
        self.setWindowModality(QtCore.Qt.ApplicationModal)
        self.setWindowTitle('{} Properties'.format(self.entity.get('code')))
        self.setWindowIcon(QtGui.QIcon('{}/icons/app_icon.png'.format(module_dir)))

        # main layout
        self.main_layout = QtWidgets.QVBoxLayout(self.main_widget)

        # form
        self.form_layout = QtWidgets.QFormLayout()
        self.main_layout.addLayout(self.form_layout)

        project = self.entity.get('project')
        # Tags
        self.hashtagWidget = hashtag_widget.SGTagWidget(inLine=True)
        self.hashtagWidget.use_hash = True
        self.hashtagWidget.allow_creation = True
        self.hashtagWidget.allow_rename = True
        self.hashtagWidget.line_edit.setStyleSheet('background-color: rgb{}'.format(self.bg_col))
        self.hashtagWidget.label.hide()
        self.hashtagWidget.display.setMaximumHeight(23)
        self.form_layout.addRow('Tags', self.hashtagWidget)

        # Used in
        self.epWidget = hashtag_widget.SGEpTagWidget(project=project, inLine=False)
        self.epWidget.line_edit.setStyleSheet('background-color: rgb{}'.format(self.bg_col))
        self.epWidget.display.setStyleSheet('background-color: rgb{}'.format(self.bg_col))
        self.epWidget.use_hash = False
        self.epWidget.allow_creation = False
        self.epWidget.allow_rename = False
        self.epWidget.label.hide()
        self.epWidget.display.setMaximumHeight(104)
        self.form_layout.addRow('Used in', self.epWidget)

        # Set
        set_filter = [['sg_asset_type', 'is', 'Set']]
         # filter out sets form assets
        all_sets = [a for a in self.assets if a.get('sg_asset_type')=='Set']
        self.parentWidget = hashtag_widget.SGAssetTagWidget(inLine=True, project=project, filters=set_filter, assets=all_sets)
        self.parentWidget.line_edit.setStyleSheet('background-color: rgb{}'.format(self.bg_col))
        self.parentWidget.use_hash = False
        self.parentWidget.allow_creation = False
        self.parentWidget.allow_rename = False
        self.parentWidget.label.hide()
        self.parentWidget.display.setMaximumHeight(23)
        self.form_layout.addRow('Set', self.parentWidget)

        # Related
        self.relatedWidget = hashtag_widget.SGAssetTagWidget(inLine=True, project=project, assets=self.assets)
        self.relatedWidget.line_edit.setStyleSheet('background-color: rgb{}'.format(self.bg_col))
        self.relatedWidget.use_hash = False
        self.relatedWidget.allow_creation = False
        self.relatedWidget.allow_rename = False
        self.relatedWidget.label.hide()
        self.relatedWidget.display.setMaximumHeight(23)
        self.form_layout.addRow('Related', self.relatedWidget)

        # looks widget
        self.looksWidget = hashtag_widget.TagWidget(inLine=True)
        self.looksWidget.line_edit.setStyleSheet('background-color: rgb{}'.format(self.bg_col))
        self.looksWidget.use_hash = False
        self.looksWidget.allow_creation = True
        self.looksWidget.allow_rename = True
        self.looksWidget.label.hide()
        self.looksWidget.display.setMaximumHeight(23)
        self.form_layout.addRow('Looks', self.looksWidget)

        # start/due date
        self.task_treeWidget = QtWidgets.QTreeWidget()
        policy = self.task_treeWidget.sizePolicy()
        policy.setVerticalStretch(True)
        self.task_treeWidget.setSizePolicy(policy)
        self.task_treeWidget.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        header_item = self.task_treeWidget.headerItem()
        header_item.setText(0, "Name")
        header_item.setText(1, "Assignees")
        header_item.setText(2, "Start")
        header_item.setText(3, "Due")
        header_item.setTextAlignment(0, QtCore.Qt.AlignCenter)
        header_item.setTextAlignment(1, QtCore.Qt.AlignCenter)
        header_item.setTextAlignment(2, QtCore.Qt.AlignCenter)
        header_item.setTextAlignment(3, QtCore.Qt.AlignCenter)
        header = self.task_treeWidget.header()
        header.resizeSection(0, 135)
        header.resizeSection(1, 145)
        header.resizeSection(2, 60)
        header.resizeSection(3, 60)
        # header.setResizeMode(0, QtWidgets.QHeaderView.Interactive)
        # header.setResizeMode(1, QtWidgets.QHeaderView.Interactive)
        # header.setResizeMode(2, QtWidgets.QHeaderView.Stretch)
        # header.setResizeMode(3, QtWidgets.QHeaderView.Stretch)
        QtCompat.setSectionResizeMode(header, 0, QtWidgets.QHeaderView.Interactive)
        QtCompat.setSectionResizeMode(header, 1, QtWidgets.QHeaderView.Interactive)
        QtCompat.setSectionResizeMode(header, 2, QtWidgets.QHeaderView.Stretch)
        QtCompat.setSectionResizeMode(header, 3, QtWidgets.QHeaderView.Stretch)
        self.task_treeWidget.header().setStretchLastSection(True)
        self.form_layout.addRow('Tasks', self.task_treeWidget)

        # buttons
        self.buttonBox = QtWidgets.QDialogButtonBox(QtCore.Qt.Horizontal)
        self.close_button = QtWidgets.QPushButton('    Close    ')
        self.buttonBox.addButton(self.close_button, QtWidgets.QDialogButtonBox.AcceptRole)
        self.main_layout.addWidget(self.buttonBox)

    def init_signal(self):
        self.buttonBox.accepted.connect(self.close)

        # tag edit triggers
        self.hashtagWidget.edited.connect(self.tag_edited)
        self.epWidget.edited.connect(self.usedin_edited)
        self.parentWidget.edited.connect(self.parents_edited)
        self.relatedWidget.edited.connect(self.related_edited)
        self.looksWidget.edited.connect(self.looks_edited)

    def clear_displays(self):
        self.hashtagWidget.clear()
        self.epWidget.clear()
        self.parentWidget.clear()
        self.relatedWidget.clear()
        self.looksWidget.clear()
        self.task_treeWidget.clear()

    def lock_displays(self):
        self.hashtagWidget.setEnabled(False)
        self.epWidget.setEnabled(False)
        self.parentWidget.setEnabled(False)
        self.relatedWidget.setEnabled(False)
        self.looksWidget.setEnabled(False)
        self.task_treeWidget.setEnabled(False)

    def unlock_displays(self):
        self.hashtagWidget.setEnabled(True)
        self.epWidget.setEnabled(True)
        self.parentWidget.setEnabled(True)
        self.relatedWidget.setEnabled(True)
        self.looksWidget.setEnabled(True)
        self.task_treeWidget.setEnabled(True)

    def set_default(self):
        if self.allow_edit:
            self.hashtagWidget.line_edit.setReadOnly(False)
            self.epWidget.line_edit.setReadOnly(False)
            self.parentWidget.line_edit.setReadOnly(False)
            self.relatedWidget.line_edit.setReadOnly(False)
            self.looksWidget.line_edit.setReadOnly(False)
        else:
            self.hashtagWidget.line_edit.setReadOnly(True)
            self.epWidget.line_edit.setReadOnly(True)
            self.parentWidget.line_edit.setReadOnly(True)
            self.relatedWidget.line_edit.setReadOnly(True)
            self.looksWidget.line_edit.setReadOnly(True)

        self.update_tags()
        self.update_usedin()
        self.update_parents()
        self.update_related()
        self.update_looks()
        self.update_tasks_table()

    def update_tags(self):
        self.hashtagWidget.blockSignals(True)
        self.hashtagWidget.clear()
        # tags
        tags = self.entity.get('tags')
        if tags:
            tag_names = [t.get('name') for t in tags]
            self.hashtagWidget.add_items(zip(tag_names, tags), check=False)
            self.hashtagWidget._original_values = tag_names
        self.hashtagWidget.blockSignals(False)

    def update_usedin(self):
        self.epWidget.blockSignals(True)
        self.epWidget.clear()
        # Used in
        episodes = self.entity.get('sg_scenes', [])
        sequences = self.entity.get('sequences', [])
        shots = self.entity.get('shots', [])
        used_in = episodes + sequences + shots
        if used_in:
            names = [i.get('name') for i in used_in]
            self.epWidget.add_items(zip(names, used_in), check=False)
            self.epWidget._original_values = names
        self.epWidget.blockSignals(False)

    def update_parents(self):
        self.parentWidget.blockSignals(True)
        self.parentWidget.clear()
        # Set
        parents = self.entity.get('parents', [])
        if parents:
            parent_names = [p.get('name') for p in parents]
            self.parentWidget.add_items(zip(parent_names, parents), check=False)
            self.parentWidget._original_values = parent_names
        self.parentWidget.blockSignals(False)

    def update_related(self):
        self.relatedWidget.blockSignals(True)
        self.relatedWidget.clear()
        # related
        related = self.entity.get('sg_link_designs', [])
        if related:
            related_names = [r.get('name') for r in related]
            self.relatedWidget.add_items(zip(related_names, related), check=False)
            self.relatedWidget._original_values = related_names
        self.relatedWidget.blockSignals(False)

    def update_looks(self):
        self.looksWidget.blockSignals(True)
        self.looksWidget.clear()
        # looks
        if self.looks:
            look_names = [l.get('sg_name') for l in self.looks]
            self.looksWidget.add_items(zip(look_names, self.looks), check=False)
            self.looksWidget._original_values = look_names
        self.looksWidget.blockSignals(False)

    def update_tasks_table(self, expand_items=[]):
        self.task_treeWidget.blockSignals(True)
        self.task_treeWidget.clear()
        if self.tasks:
            for step, tasks in self.tasks.iteritems():
                parentItem = QtWidgets.QTreeWidgetItem(self.task_treeWidget)
                parentItem.setText(0, step.title())
                for task in tasks:
                    item = QtWidgets.QTreeWidgetItem(parentItem)
                    item.setText(0, task['content'])
                    item.setData(QtCore.Qt.UserRole, 0, task)

                    # assignees
                    assigneeWidget = hashtag_widget.SGUserWidget(users=self.users, inLine=True, parent=self)
                    assigneeWidget.line_edit.setStyleSheet('background-color: rgb{};'.format(self.fg_col))
                    assigneeWidget.allLayout.setSpacing(0)
                    assigneeWidget.use_hash = False
                    assigneeWidget.allow_creation = False
                    assigneeWidget.allow_rename = False
                    assigneeWidget.line_edit.setReadOnly(True)
                    assigneeWidget.label.hide()
                    assigneeWidget.display.setMaximumHeight(23)
                    self.task_treeWidget.setItemWidget(item, 1, assigneeWidget)

                    assignees = task.get('task_assignees')
                    if assignees:
                        assignee_names = [a.get('name') for a in assignees] 
                        assigneeWidget.add_items(zip(assignee_names, assignees), check=False)

                    # start date
                    start_date = task.get('start_date')
                    start_dateEdit = DateLineEdit(parent=self)
                    start_dateEdit.set_edit_enabled(self.allow_edit)
                    if start_date:
                        start_date = datetime.strptime(start_date, '%Y-%m-%d').date()
                        start_dateEdit.setDate(start_date)
                    start_dateEdit.dateChanged.connect(partial(self.date_changed, 'start_date', task, start_dateEdit))
                    self.task_treeWidget.setItemWidget(item, 2, start_dateEdit)

                    # due date
                    due_date = task.get('due_date')
                    due_dateEdit = DateLineEdit(parent=self)
                    due_dateEdit.set_edit_enabled(self.allow_edit)
                    if due_date:
                        due_date = datetime.strptime(due_date, '%Y-%m-%d').date()
                        due_dateEdit.setDate(due_date)
                    
                    due_dateEdit.dateChanged.connect(partial(self.date_changed, 'due_date', task, due_dateEdit))
                    self.task_treeWidget.setItemWidget(item, 3, due_dateEdit) 
                    if self.allow_edit:
                        # assignee editable
                        assigneeWidget.line_edit.setReadOnly(False)
                        assigneeWidget.edited.connect(partial(self.assignee_edited, task, assigneeWidget))

                        # delete action
                        # start date
                        start_del_action = QtWidgets.QAction(self.task_treeWidget)
                        start_del_action.setShortcutContext(QtCore.Qt.WidgetWithChildrenShortcut)
                        start_del_action.setShortcut(QtGui.QKeySequence(QtCore.Qt.Key_Delete))
                        start_dateEdit.lineEdit.addAction(start_del_action)
                        start_del_action.triggered.connect(partial(self.date_changed, 'start_date', task, start_dateEdit, None))

                        # due_date
                        due_del_action = QtWidgets.QAction(self.task_treeWidget)
                        due_del_action.setShortcutContext(QtCore.Qt.WidgetWithChildrenShortcut)
                        due_del_action.setShortcut(QtGui.QKeySequence(QtCore.Qt.Key_Delete))
                        due_dateEdit.lineEdit.addAction(due_del_action)
                        due_del_action.triggered.connect(partial(self.date_changed, 'due_date', task, due_dateEdit, None))

            # reexpand items
            if expand_items:
                rootItem = self.task_treeWidget.invisibleRootItem()
                for i in range(rootItem.childCount()):
                    item = rootItem.child(i)
                    if item.text(0) in expand_items:
                        self.task_treeWidget.setItemExpanded(item, True)

        self.task_treeWidget.blockSignals(False)

    def update_tasks(self, task_id, value):
        updated_tasks = OrderedDict()
        for step, tasks in self.tasks.iteritems():
            new_tasks = []
            for t in tasks:
                if t.get('id') == task_id:
                    t.update(value)
                new_tasks.append(t)
            updated_tasks[step] = new_tasks

        self.tasks = updated_tasks

    def assignee_edited(self, task, widget, *args, **kwargs):
        item_data = widget.get_all_item_data()
        expand_items = self.get_task_expanded()
        self.lock_displays()
        self.assigneeEdited.emit([task, item_data, expand_items])

    def tag_edited(self, *args, **kwargs):
        item_data = self.hashtagWidget.get_all_item_data()
        self.lock_displays()
        self.tagEdited.emit(item_data)

    def usedin_edited(self, *args, **kwargs):
        item_data = self.epWidget.get_all_item_data()
        self.lock_displays()
        self.usedInEdited.emit(item_data)

    def parents_edited(self, *args, **kwargs):
        item_data = self.parentWidget.get_all_item_data()
        self.lock_displays()
        self.parentsEdited.emit(item_data)

    def related_edited(self, *args, **kwargs):
        item_data = self.relatedWidget.get_all_item_data()
        self.lock_displays()
        self.relatedEdited.emit(item_data)

    def looks_edited(self, *args, **kwargs):
        item_data = self.looksWidget.get_all_item_data()
        self.lock_displays()
        self.looksEdited.emit(item_data)

    def date_changed(self, field, task, dateEdit, date=None, *args, **kwargs):
        # print '...', field, task, dateEdit, date
        # user press delete
        if not date:
            dateEdit.clear()
        else:
            date = date.toPython()
            if field == 'start_date':
                due_date = task.get('due_date')
                if due_date and datetime.strptime(due_date, '%Y-%m-%d').date() < date:
                    self.error_dialog(title='Error', message='Start date cannot be later than Due date!')
                    return
            else:  # 'due_date'
                start_date = task.get('start_date')
                if start_date and datetime.strptime(start_date, '%Y-%m-%d').date() > date:
                    self.error_dialog(title='Error', message='Due date cannot be earlier than Start date!')
                    return
            # dateEdit.setDate(date)

        
        expand_items = self.get_task_expanded()
        self.lock_displays()
        self.dateEdited.emit([task, field, date, expand_items])

    def get_task_expanded(self):
        # get expanded step item
        expand_items = []
        rootItem = self.task_treeWidget.invisibleRootItem()
        for i in range(rootItem.childCount()):
            item = rootItem.child(i)
            if self.task_treeWidget.isItemExpanded(item):
                expand_items.append(item.text(0))
        return expand_items

    def error_dialog(self, title, message):
        # pop up user to choose application to play media
        qmsgBox = QtWidgets.QMessageBox(self)
        # set tile, icon
        qmsgBox.setWindowTitle(title)
        qmsgBox.setText(message)
        qmsgBox.setIcon(QtWidgets.QMessageBox.Critical)

        # add button
        qmsgBox.addButton('  OK  ', QtWidgets.QMessageBox.AcceptRole)
        qmsgBox.exec_()