import os
import sys
import glob
from rf_app.info_panel.tabs import *
from collections import OrderedDict
from rf_utils import file_utils
moduleDir = os.path.dirname(sys.modules[__name__].__file__)


sortedOrder = ['notes', 'activities']
moduleDataCache = OrderedDict()

def get_module():
    modules = glob.glob(os.path.dirname(__file__)+"/*.py")
    moduleNames = [ os.path.basename(f)[:-3] for f in modules if os.path.isfile(f) and not f.endswith('__init__.py')]
    return sort_order(moduleNames)

def get_func(name): 
    func = __import__(name, globals(), locals(), [], -1)
    return func


def sort_order(modules): 
	sortedOrder = []
	for module in modules: 
		if module in sortedOrder: 
			sortedOrder.append(module)

	for module in modules: 
		if not module in sortedOrder: 
			sortedOrder.append(module)

	return sortedOrder
