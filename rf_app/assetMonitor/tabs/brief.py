#!/usr/bin/env python
# -- coding: utf-8 --

import os
import sys
import re
import tempfile
import subprocess
from collections import OrderedDict, defaultdict
from functools import partial
from datetime import datetime
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from Qt import QtCore
from Qt import QtGui
from Qt import QtWidgets

import rf_config as config
from rf_utils import thread_pool
from rf_utils import user_info
from rf_utils import file_utils
from rf_utils.sg import sg_note
from rf_utils.widget import image_widget
from rf_utils.widget import collapse_widget
from rf_utils.widget import textEdit
from rf_utils.widget import status_widget
from rf_utils.widget import filmstrip_widget
from rf_utils import project_info
from . import master

modulePath = sys.modules[__name__].__file__.replace('\\', '/')
moduleDir  = os.path.dirname(modulePath)
appModuleDir = os.path.dirname(moduleDir)

# ----- TODO -----
#// - Add brief UI
#// - Add brief function with existance check
#// - Remove brief
#// - drag n drop
#// - add "new" logo
#// - sort new brief to top
#// - Set Brief status
#// - Support adding breif image by pasting

class BriefTreeWidget(QtWidgets.QTreeWidget):
    def __init__(self, parent=None):
        super(BriefTreeWidget, self).__init__(parent)
        self.setStyleSheet('QTreeWidget::item{padding:2px 0}')

class AddBriefWin(QtWidgets.QMainWindow):
    confirm = QtCore.Signal(tuple)
    def __init__(self, entity, parent=None):
        super(AddBriefWin, self).__init__(parent)
        self.entity = entity
        self.existing_briefs = []
        self.supported_formats = ['.'+str(f) for f in QtGui.QImageReader.supportedImageFormats()]
        self.bg_col = (43, 43, 43)
        self.edit_col = (100, 50, 50)

        self.setupUi()
        self.init_signal()
        self.set_default()

    def setupUi(self):
        # main win
        self.main_widget = QtWidgets.QWidget()
        self.setCentralWidget(self.main_widget)
        self.resize(420, 320)
        self.setWindowModality(QtCore.Qt.ApplicationModal)
        self.setWindowTitle('Create Brief')
        self.setWindowIcon(QtGui.QIcon('{}/icons/app_icon.png'.format(appModuleDir)))

        # main layout
        self.main_layout = QtWidgets.QVBoxLayout(self.main_widget)

        # form
        self.form_layout = QtWidgets.QFormLayout()
        self.main_layout.addLayout(self.form_layout)

        self.link_layout = QtWidgets.QHBoxLayout()

        # step
        self.step_label = QtWidgets.QLabel('Step: ')
        self.link_layout.addWidget(self.step_label)

        self.step_combobox = QtWidgets.QComboBox()
        self.step_combobox.setFixedWidth(100)
        self.link_layout.addWidget(self.step_combobox)

        # task
        self.task_label = QtWidgets.QLabel('Task: ')
        self.link_layout.addWidget(self.task_label)

        self.task_combobox = QtWidgets.QComboBox()
        self.task_combobox.setFixedWidth(100)
        self.link_layout.addWidget(self.task_combobox)

        spacerItem1 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.link_layout.addWidget(self.task_combobox)
        self.form_layout.addRow(self.link_layout)

        # subject
        self.subject_lineEdit = QtWidgets.QLineEdit()
        self.subject_lineEdit.setFixedWidth(250)
        self.subject_lineEdit.setPlaceholderText('What is it about?')
        self.form_layout.addRow('Subject:', self.subject_lineEdit)
        
        # content
        self.content_textEdit = textEdit.TextEditWithPlaceHolder()
        self.content_textEdit.setMinimumHeight(100)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.content_textEdit.setSizePolicy(sizePolicy)
        self.content_textEdit.setPlaceholderText('Detailed notes for artists...')
        self.content_textEdit.setWordWrapMode(QtGui.QTextOption.WrapAtWordBoundaryOrAnywhere)
        self.main_layout.addWidget(self.content_textEdit)

        # attachments
        self.attachment_groupBox = QtWidgets.QGroupBox('Attach Image')
        self.main_layout.addWidget(self.attachment_groupBox)

        self.attachment_layout = QtWidgets.QHBoxLayout(self.attachment_groupBox)
        self.attachment_layout.setSpacing(3)
        self.attachment_layout.setContentsMargins(9, 4, 9, 9)
        self.reelWidget = filmstrip_widget.DisplayReel()
        self.reelWidget.allow_paste = True
        self.reelWidget.setFixedHeight(32)
        self.reelWidget.displayListWidget.setFlow(QtWidgets.QListView.LeftToRight)
        self.reelWidget.displayListWidget.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.reelWidget.setMinimumWidth(100)
        self.attachment_layout.addWidget(self.reelWidget)

        # add image button
        self.attach_image_button = QtWidgets.QPushButton('')
        self.attach_image_button.setFixedSize(QtCore.QSize(22, 32))
        self.attach_image_button.setIcon(QtGui.QIcon('{}/icons/paperclip_icon.png'.format(appModuleDir)))
        self.attachment_layout.addWidget(self.attach_image_button)

        # buttons
        self.buttonBox = QtWidgets.QDialogButtonBox(QtCore.Qt.Horizontal)
        self.create_button = QtWidgets.QPushButton('  Add  ')
        self.cancel_button = QtWidgets.QPushButton('  Cancel  ')
        self.buttonBox.addButton(self.create_button, QtWidgets.QDialogButtonBox.AcceptRole)
        self.buttonBox.addButton(self.cancel_button, QtWidgets.QDialogButtonBox.RejectRole)
        self.main_layout.addWidget(self.buttonBox)

        self.main_layout.setStretch(0, 0)
        self.main_layout.setStretch(1, 1)
        
    def init_signal(self):
        # button signals
        self.buttonBox.accepted.connect(self.create_brief)
        self.buttonBox.rejected.connect(self.close)
        self.reelWidget.fileDropped.connect(self.add_images)
        self.step_combobox.currentIndexChanged.connect(self.step_changed)
        self.attach_image_button.clicked.connect(self.browse_add_attachment)

    def hideEvent(self, event):
        self.subject_lineEdit.clear()
        self.content_textEdit.clear()
        self.reelWidget.clear()

    def step_changed(self, *args, **kwargs):
        step = self.step_combobox.currentText()
        self.task_combobox.clear()
        self.set_enable_create(True)
        if step == 'Asset':
            self.task_combobox.addItem('-') 
        else:
            project = self.entity.get('project')['name']
            entityName = self.entity.get('code')
            tasks = sg_note.fetch_tasks(project, 'asset', entityName, step=step)
            task_names = [t.get('content') for t in tasks]
            if task_names:
                self.task_combobox.addItems(task_names) 
            else:
                self.task_combobox.addItem('-')
                self.set_enable_create(False)

    def set_enable_create(self, enabled):
        self.task_combobox.setEnabled(enabled)
        self.subject_lineEdit.setEnabled(enabled)
        self.content_textEdit.setEnabled(enabled)
        self.reelWidget.setEnabled(enabled)
        self.attach_image_button.setEnabled(enabled)
        self.create_button.setEnabled(enabled)

    def browse_add_attachment(self):
        dialog = QtWidgets.QFileDialog()
        dialog.setWindowTitle('Add attachments to Brief')
        dialog.setNameFilters(["PNG (*.png *.PNG)", "JPEG (*.jpg *.JPG)", "TIFF (*.tiff *.TIFF)"])
        dialog.setFileMode(QtWidgets.QFileDialog.ExistingFiles)
        # dialog.setViewMode(QtWidgets.QFileDialog.Detail)
        dialog.setAcceptMode(QtWidgets.QFileDialog.AcceptOpen) 
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            chosen_paths = dialog.selectedFiles()
            self.add_images(paths=chosen_paths)

    def add_images(self, paths):
        paths = [l for l in paths if len(os.path.splitext(l)) > 1 and os.path.splitext(l)[-1].lower() in self.supported_formats]
        if paths:
            self.reelWidget.add_items(paths=paths)

    def set_default(self):
        self.setup_steps()
        self.step_changed()

    def setup_steps(self):
        # store current selections
        curr_step = self.step_combobox.currentText()
        curr_task = self.task_combobox.currentText()

        self.step_combobox.blockSignals(True)
        self.step_combobox.clear()
        projectName = self.entity.get('project')['name']
        steps = project_info.ProjectInfo(projectName).steps()['asset'].keys()
        self.step_combobox.addItems(['Asset'] + steps)

        # try to select
        self.set_step_task(step=curr_step, task=curr_task)
        self.step_combobox.blockSignals(False)

    def set_entity(self, entity):
        self.entity = entity
        self.setup_steps()

    def set_step_task(self, step, task):
        for i in range(self.step_combobox.count()):
            if self.step_combobox.itemText(i) == step:
                self.step_combobox.setCurrentIndex(i)
                break
        for i in range(self.task_combobox.count()):
            if self.task_combobox.itemText(i) == task:
                self.task_combobox.setCurrentIndex(i)
                break

    def create_brief(self):
        subject = self.subject_lineEdit.text()
        selected_step = self.step_combobox.currentText()

        # check if any required field is empty
        error = False
        if not subject:
            self.subject_lineEdit.setStyleSheet('background-color: rgb{}'.format(self.edit_col))
            error = True
        content = self.content_textEdit.toPlainText()
        if not content:
            self.content_textEdit.setStyleSheet('background-color: rgb{}'.format(self.edit_col))
            error = True
        if error:
            return

        # check if existing brief within the same level uses the same subject
        if subject in [b.get('subject') for b in self.existing_briefs[selected_step]]:
            qmsgBox = QtWidgets.QMessageBox(self)
            qmsgBox.setWindowTitle('Error')
            qmsgBox.setText('Brief with the same subject already exists: ' + subject)
            qmsgBox.setIcon(QtWidgets.QMessageBox.Critical)
            qmsgBox.addButton('  OK  ', QtWidgets.QMessageBox.AcceptRole)
            qmsgBox.exec_()
            return

        # step needs to be None in case we want to add brief to Asset level
        task = None if selected_step == 'Asset' else self.task_combobox.currentText()
        # attchments
        attachments = self.reelWidget.get_all_items()

        self.confirm.emit((task, subject, content, attachments))
        self.hide()

class Ui(QtWidgets.QWidget):
    """docstring for Ui"""
    NEW_ITEM_DURATION = 3  # anything newer than 3 days will have "new" icon

    def __init__(self, thread=None, parent=None):
        super(Ui, self).__init__(parent)
        self.parent = parent
        self.threadpool = QtCore.QThreadPool() if not thread else thread

        # internal vars
        self.player_app = {'Keyframe Pro': self.play_keyframepro, 'RV': self.play_rv}
        self.chosen_app = None
        self.context_sync = False
        self.entity = None
        self.user = user_info.User()
        self.is_admin = True if self.user.is_producer() or self.user.is_admin() or self.user.is_supervisor() else False
        self.cache = {}
        self.add_brief_win = None

        # fonts
        self.bg_col = (43, 43, 43)
        self.edit_col = (100, 50, 50)
        self.grey_col = (128, 128, 128)
        self.white_col = (255, 255, 255)
        self.active_font = QtGui.QFont()
        self.active_font.setItalic(False)
        self.active_font.setPointSize(8)
        self.display_font = QtGui.QFont()
        self.display_font.setItalic(True)
        self.display_font.setPointSize(8)

        # palettes
        self.grey_font_palette = QtGui.QPalette()
        self.grey_font_palette.setBrush(QtGui.QPalette.Text, QtGui.QBrush(QtGui.QColor(self.grey_col[0], self.grey_col[1], self.grey_col[2])))
        self.white_font_palette = QtGui.QPalette()
        self.white_font_palette.setBrush(QtGui.QPalette.Text, QtGui.QBrush(QtGui.QColor(self.white_col[0], self.white_col[1], self.white_col[2])))

        self.setup_ui()
        self.init_signal()
        self.set_default()

    def closeEvent(self, event):
        self.clear_cache()

    def clear_cache(self):
        # remove temp files
        for image in sorted({x['filepath'] for v in self.cache.itervalues() for x in v}):
            try:
                os.remove(image)
            except:
                pass
        self.cache = {}

    def setup_ui(self):
        # main layout
        self.layout = QtWidgets.QVBoxLayout()
        self.frame = QtWidgets.QFrame(self)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.main_layout = QtWidgets.QVBoxLayout(self.frame)
        self.layout.addWidget(self.frame)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

        # splitter
        self.mainSplitter = QtWidgets.QSplitter()
        self.mainSplitter.setOrientation(QtCore.Qt.Horizontal)

        self.processSplitWidget = QtWidgets.QWidget(self.mainSplitter)
        self.processSplitWidget.setMinimumWidth(120)
        self.viewSplitWidget = QtWidgets.QWidget(self.mainSplitter)
        self.mainSplitter.setSizes([135, 420])

        self.main_layout.addWidget(self.mainSplitter)

        # process list layout
        self.briefInfo_layout = QtWidgets.QVBoxLayout(self.processSplitWidget)
        self.briefInfo_layout.setSpacing(6)
        self.briefInfo_layout.setContentsMargins(3, 0, 3, 0)

        # status layout
        self.status_layout = QtWidgets.QFormLayout()
        self.status_layout.setLabelAlignment(QtCore.Qt.AlignLeft)
        self.status_layout.setFormAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)
        self.status_layout.setSpacing(3)
        self.briefInfo_layout.addLayout(self.status_layout)
        self.creator_value_label = QtWidgets.QLabel()
        self.status_layout.addRow('Creator: ', self.creator_value_label)
        self.date_value_label = QtWidgets.QLabel()
        self.status_layout.addRow('Updated: ', self.date_value_label)
        self.status_value_label = status_widget.StatusLabel(icon_data=status_widget.NoteIcon)
        self.status_layout.addRow('Status: ', self.status_value_label)

        # list widget
        self.brief_treeWidget = BriefTreeWidget()
        self.brief_treeWidget.setColumnCount(2)
        # self.brief_treeWidget.setColumnWidth(0, 210)
        # self.brief_treeWidget.setColumnWidth(1, 50)
        self.brief_treeWidget.setSortingEnabled(False)
        self.brief_treeWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.brief_treeWidget.setHeaderHidden(True)
        # self.brief_treeWidget.header().setResizeMode(QtWidgets.QHeaderView.ResizeToContents)
        self.brief_treeWidget.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.briefInfo_layout.addWidget(self.brief_treeWidget)

        # display options
        self.display_option_groupBox = collapse_widget.CollapsibleBox(title='Display options')
        self.briefInfo_layout.addWidget(self.display_option_groupBox)
        self.display_option_layout = QtWidgets.QVBoxLayout(self.display_option_groupBox)

        # dept filter
        self.status_filter_layout = QtWidgets.QFormLayout()
        self.status_combobox = QtWidgets.QComboBox()
        self.status_combobox.setMinimumHeight(20)
        self.status_filter_layout.addRow('Status filter: ', self.status_combobox)
        self.display_option_layout.addLayout(self.status_filter_layout)
        self.display_option_groupBox.setContentLayout(self.display_option_layout)

        # view layout 
        self.view_layout = QtWidgets.QVBoxLayout(self.viewSplitWidget)
        self.view_layout.setSpacing(9)
        self.view_layout.setContentsMargins(0, 0, 0, 0)

        self.viewSplitter = QtWidgets.QSplitter()
        self.viewSplitter.setOrientation(QtCore.Qt.Vertical)
        self.viewSplitWidget = QtWidgets.QWidget(self.viewSplitter)
        self.bottomSplitWidget = QtWidgets.QWidget(self.viewSplitter)
        self.viewSplitter.setSizes([500, 75])
        self.view_layout.addWidget(self.viewSplitter)

        # media layout
        self.media_layout = QtWidgets.QHBoxLayout(self.viewSplitWidget)
        self.media_layout.setContentsMargins(0, 0, 0, 0)
        self.media_layout.setSpacing(9)

        # still image viewer
        self.image_viewer = image_widget.ImageSlideViewer()
        self.image_viewer.reelWidget.allow_delete = False
        self.image_viewer.reelWidget.setStyleSheet('background-color: rgb{};'.format(self.bg_col))
        self.image_viewer.allow_image_drop = True
        self.image_viewer.drop_add_image = False
        self.image_viewer.paste_add_image = False
        self.image_viewer.viewer.setBackgroundBrush(QtGui.QColor(self.bg_col[0], self.bg_col[1], self.bg_col[2]))
        self.media_layout.addWidget(self.image_viewer)

        # media button layout
        self.media_button_layout = QtWidgets.QVBoxLayout()
        self.media_button_layout.setSpacing(5)
        # view button
        self.view_media_button = QtWidgets.QPushButton('')
        self.view_media_button.setFixedSize(QtCore.QSize(40, 22))
        self.view_media_button.setIcon(QtGui.QIcon('{}/icons/eye_icon.png'.format(appModuleDir)))
        self.media_button_layout.addWidget(self.view_media_button)
        # save button
        self.save_media_button = QtWidgets.QPushButton('')
        self.save_media_button.setFixedSize(QtCore.QSize(40, 22))
        self.save_media_button.setIcon(QtGui.QIcon('{}/icons/disk_icon.png'.format(appModuleDir)))
        self.media_button_layout.addWidget(self.save_media_button)
        # add brief button
        self.add_brief_button = QtWidgets.QPushButton('')
        self.add_brief_button.setFixedSize(QtCore.QSize(40, 22))
        self.add_brief_button.setIcon(QtGui.QIcon('{}/icons/add_icon.png'.format(appModuleDir)))
        spacerItem1 = QtWidgets.QSpacerItem(25, 25, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.media_button_layout.addWidget(self.add_brief_button)
        # remove brief button
        self.remove_brief_button = QtWidgets.QPushButton('')
        self.remove_brief_button.setFixedSize(QtCore.QSize(40, 22))
        self.remove_brief_button.setIcon(QtGui.QIcon('{}/icons/delete_icon.png'.format(appModuleDir)))
        spacerItem1 = QtWidgets.QSpacerItem(25, 25, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.media_button_layout.addWidget(self.remove_brief_button)

        self.media_button_layout.addItem(spacerItem1)

        self.image_button_layout = QtWidgets.QVBoxLayout()
        self.image_button_layout.setSpacing(3)
        self.image_button_layout.setContentsMargins(0, 2, 0, 0)
        self.media_button_layout.addLayout(self.image_button_layout)
        # add image button
        self.add_image_button = QtWidgets.QPushButton('')
        self.add_image_button.setFixedSize(QtCore.QSize(40, 20))
        self.add_image_button.setIcon(QtGui.QIcon('{}/icons/add_image_icon.png'.format(appModuleDir)))
        self.image_button_layout.addWidget(self.add_image_button)
        # remove image button
        self.remove_image_button = QtWidgets.QPushButton('')
        self.remove_image_button.setFixedSize(QtCore.QSize(40, 20))
        self.remove_image_button.setIcon(QtGui.QIcon('{}/icons/remove_image_icon.png'.format(appModuleDir)))
        self.image_button_layout.addWidget(self.remove_image_button)
        self.media_layout.addLayout(self.media_button_layout)

        # content layout
        self.content_layout = QtWidgets.QVBoxLayout(self.bottomSplitWidget)
        self.content_layout.setContentsMargins(0, 0, 0, 0)
        self.content_layout.setSpacing(0)

        # content button layout
        self.content_button_layout = QtWidgets.QHBoxLayout()
        self.content_button_layout.setContentsMargins(0, 0, 0, 0)
        self.content_label = QtWidgets.QLabel('Note: ')
        spacerItem2 = QtWidgets.QSpacerItem(25, 25, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.edit_content_button = QtWidgets.QPushButton('')
        self.edit_content_button.setCheckable(True)
        self.edit_content_button.setFixedSize(QtCore.QSize(40, 20))
        self.edit_content_button.setIcon(QtGui.QIcon('{}/icons/pencil_icon.png'.format(appModuleDir)))
        self.content_button_layout.addWidget(self.content_label)
        self.content_button_layout.addItem(spacerItem2)
        self.content_button_layout.addWidget(self.edit_content_button)
        self.content_layout.addLayout(self.content_button_layout)

        # content box
        self.content_textEdit = textEdit.TextEditWithPlaceHolder()
        self.content_textEdit.setStyleSheet('background-color: rgb{};'.format(self.bg_col))
        self.content_textEdit.setPlaceholderText('< No note found >')
        self.content_textEdit.setWordWrapMode(QtGui.QTextOption.WrapAtWordBoundaryOrAnywhere)
        self.content_textEdit.setTextInteractionFlags(QtCore.Qt.TextSelectableByMouse)
        self.content_textEdit.setFont(self.display_font)
        self.content_textEdit.setPalette(self.grey_font_palette)
        self.content_layout.addWidget(self.content_textEdit)

        # setup sizes
        self.media_layout.setStretch(0, 15)
        self.media_layout.setStretch(1, 3)
        self.media_layout.setStretch(2, 1)
        self.content_layout.setStretch(0, 1)
        self.content_layout.setStretch(1, 3)

        self.setup_display_options()

        # tool tips
        self.view_media_button.setToolTip('Open images in media viewer app')
        self.save_media_button.setToolTip('Save attachment image to disk')
        self.add_brief_button.setToolTip('Add new Brief')
        self.remove_brief_button.setToolTip('Remove selected Brief')
        self.add_image_button.setToolTip('Add attachment to this Brief')
        self.remove_image_button.setToolTip('Remove selected attachments from this Brief')
        self.edit_content_button.setToolTip('Toggle edit current note')
        self.status_combobox.setToolTip('Only display Brief with this status')

    def refresh(self, entity):
        self.entity = entity
        self.brief_treeWidget.clear()
        self.reset_info()
        self.reset_view()
        self.clear_cache()
        if not self.entity:
            return

        self.thread_fetch_data(self.entity)

    def set_default(self):
        self.brief_treeWidget.clear()
        self.reset_info()
        self.reset_view()

        # only allow some feature for sup, producer, admin
        if self.is_admin:
            self.edit_content_button.setVisible(True)
            self.add_brief_button.setVisible(True)
            self.add_image_button.setVisible(True)
            self.remove_image_button.setVisible(True)
            if self.user.is_admin():  # only allow admin to remove a brief
                self.remove_brief_button.setVisible(True)
            else:
                self.remove_brief_button.setVisible(False)
        else:
            self.edit_content_button.setVisible(False)
            self.add_brief_button.setVisible(False)
            self.remove_brief_button.setVisible(False)
            self.add_image_button.setVisible(False)
            self.remove_image_button.setVisible(False)

    def setup_display_options(self):
        for i, status in enumerate(status_widget.NoteIcon.availableMaps['Brief']):
            status_data = status_widget.NoteIcon.statusMap[status]
            self.status_combobox.addItem(status_data['display'])
            icon = QtGui.QIcon('{}/{}'.format(status_widget.NoteIcon.path, status_data['icon']))
            self.status_combobox.setItemIcon(i, icon)
            self.status_combobox.setItemData(i, status, QtCore.Qt.UserRole)
        self.status_combobox.addItem('All')
        self.status_combobox.setItemData(self.status_combobox.count()-1, 'All', QtCore.Qt.UserRole)

    def init_signal(self):
        self.brief_treeWidget.customContextMenuRequested.connect(self.brief_right_clicked)
        self.brief_treeWidget.itemSelectionChanged.connect(self.brief_selected)
        self.status_combobox.currentIndexChanged.connect(self.status_filter_changed)
        self.image_viewer.reelWidget.deleted.connect(self.image_deleted)
        self.image_viewer.imageDropped.connect(self.add_attachments)
        self.image_viewer.imagePasted.connect(self.image_pasted)
        self.edit_content_button.toggled.connect(self.edit_content_toggled)
        self.content_textEdit.editorLostFocus.connect(lambda: self.disable_content_edit(force=False))
        self.view_media_button.clicked.connect(self.view_media)
        self.save_media_button.clicked.connect(self.save_media)
        self.add_brief_button.clicked.connect(self.add_brief_clicked)
        self.remove_brief_button.clicked.connect(self.remove_brief_clicked)
        self.add_image_button.clicked.connect(self.browse_add_attachment)
        self.remove_image_button.clicked.connect(self.remove_attachment)
        self.image_viewer.reelWidget.itemDoubleClicked.connect(self.doubleclick_view_media)
    
    def image_pasted(self, pixmap):
        if not self.is_admin:
            return
        item, brief = self.get_current_brief()
        if not item:
            return
        qmsgBox = QtWidgets.QMessageBox(self)
        text = 'Paste image to Brief: ' + brief.get('subject') + '?'
        qmsgBox.setText(text)
        qmsgBox.setWindowTitle('Confirm')
        qmsgBox.setIcon(QtWidgets.QMessageBox.Question)
        qmsgBox.addButton('  Yes  ', QtWidgets.QMessageBox.AcceptRole)
        qmsgBox.addButton('  No  ', QtWidgets.QMessageBox.RejectRole)
        answer = qmsgBox.exec_()
        if answer == 0: 
            fh, temp = tempfile.mkstemp(suffix='.png')
            os.close(fh)
            pixmap.save(temp, "PNG")
            self.thread_upload_data([temp])

    def remove_brief_clicked(self):
        if not self.entity:
            return
        item, brief = self.get_current_brief()
        if not brief:
            self.error_no_selection('Remove Brief', 'Please select a Brief!')
            return
        bid = brief.get('id')
        if not bid:
            return
        # confirm user
        qmsgBox = QtWidgets.QMessageBox(self)
        qmsgBox.setText('Delete Brief: ' + brief.get('subject') + '?')
        qmsgBox.setWindowTitle('Confirm')
        qmsgBox.setIcon(QtWidgets.QMessageBox.Question)
        qmsgBox.addButton('  Yes  ', QtWidgets.QMessageBox.AcceptRole)
        qmsgBox.addButton('  No  ', QtWidgets.QMessageBox.RejectRole)
        answer = qmsgBox.exec_()
        if answer == 0: 
            self.thread_remove_brief(bid)

    def thread_remove_brief(self, bid):
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        self.enable_ui(False)
        worker = thread_pool.Worker(partial(sg_note.sg.delete, 'Note', bid))
        worker.signals.result.connect(self.brief_expand_update)
        self.threadpool.start(worker)

    def brief_expand_update(self, result, *args, **kwargs):
        self.enable_ui(True)
        QtWidgets.QApplication.restoreOverrideCursor()
        if result:
            item, brief = self.get_current_brief()
            expand_items = []
            rootItem = self.brief_treeWidget.invisibleRootItem()
            for i in range(rootItem.childCount()):
                item = rootItem.child(i)
                if self.brief_treeWidget.isItemExpanded(item):
                    expand_items.append(item.text(0))
            self.brief_treeWidget.clear()
            self.reset_info()
            self.reset_view()
            self.clear_cache()
            self.enable_ui(False)
            QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
            worker = thread_pool.Worker(self.fetch_data, self.entity)
            worker.signals.result.connect(partial(self.list_expand_briefs, expand_items))
            self.threadpool.start(worker)

    def list_expand_briefs(self, expand_items, new_data, *args, **kwargs):
        self.list_briefs(new_data)
        if expand_items:
            rootItem = self.brief_treeWidget.invisibleRootItem()
            for i in range(rootItem.childCount()):
                item = rootItem.child(i)
                if item.text(0) in expand_items:
                    self.brief_treeWidget.setItemExpanded(item, True)
        self.enable_ui(True)
        
    def add_brief_clicked(self):
        if not self.entity:
            return
        
        step = 'Asset'
        item, brief = self.get_current_brief()
        # if brief:
        #     sg_brief = brief.get('sg_brief')
        #     if sg_brief['type'] == 'Task':
        #         step = sg_brief['name']

        # get existing briefs
        if not self.add_brief_win:
            self.add_brief_win = AddBriefWin(entity=self.entity, parent=self)
            self.add_brief_win.confirm.connect(self.thread_add_brief)
            # self.add_brief_win.set_step(step=step)  # set step
        else:
            self.add_brief_win.set_entity(entity=self.entity)

        self.add_brief_win.existing_briefs = self.get_all_briefs()
        self.add_brief_win.show()

    def thread_add_brief(self, creation_data):
        task, subject, content, attachments = creation_data
        project = self.entity.get('project')['name']
        asset_name = self.entity.get('code')

        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        self.enable_ui(False)
        worker = thread_pool.Worker(partial(self.add_brief, 
                                                project, 
                                                asset_name, 
                                                task, 
                                                self.user.sg_user()),
                                                subject,
                                                content,
                                                attachments)
        worker.signals.result.connect(self.brief_expand_update)
        self.threadpool.start(worker)

    def add_brief(self, project, asset_name, task, user, subject, content, attachments):
        # link_entity = sg_note.find_link_entity(project, 'Asset', asset_name, task)
        # result = sg_note.create_brief(project, link_entity, subject, content, user, attachments=attachments)
        tasks = [task] if task else list()
        entity = sg_note.find_entity(project, 'Asset', asset_name)
        result = sg_note.create_brief(project, entity, subject, content, user, tasks, attachments=attachments)
        return result

    def brief_right_clicked(self, pos):
        item = self.brief_treeWidget.itemAt(pos)
        # right clicked on an item
        if not item:
            return
        # get brief from item data
        brief = item.data(QtCore.Qt.UserRole, 0)
        if not brief:
            return
        # get brief ID
        bid = brief.get('id')
        if not bid:
            return

        # create menu
        rightClickMenu = QtWidgets.QMenu(self)
        actions = []
        # admin menu
        if self.is_admin:
            status_menu = QtWidgets.QMenu('Set status...',  self)
            rightClickMenu.addMenu(status_menu)
            available_status = status_widget.NoteIcon.availableMaps['Brief']
            current_status = brief.get('sg_status_list')
            # add status actions
            check_index = 0
            for a, status in enumerate(available_status):
                action = QtWidgets.QAction(status_widget.NoteIcon.statusMap[status]['display'], self)
                icon = QtGui.QIcon('{0}/{1}'.format(status_widget.NoteIcon.path, 
                                                    status_widget.NoteIcon.statusMap[status]['icon']))
                action.setIcon(icon)
                action.triggered.connect(partial(self.thread_update_status, bid, status, current_status))
                status_menu.addAction(action)
                actions.append(action)
        if actions:
            rightClickMenu.exec_(self.brief_treeWidget.mapToGlobal(pos))

    def thread_update_status(self, bid, status, current_status):
        if status == current_status:
            return
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        self.enable_ui(False)
        worker = thread_pool.Worker(partial(sg_note.update_note, bid, status, self.user.sg_user()))
        worker.signals.result.connect(self.update_status_finished)
        self.threadpool.start(worker)

    def update_status_finished(self, result):
        self.enable_ui(True)
        QtWidgets.QApplication.restoreOverrideCursor()
        if result:
            self.thread_update_data()

    def doubleclick_view_media(self, item_data):
        if isinstance(item_data, dict) and 'filepath' in item_data and os.path.exists(item_data['filepath']):
            self.launch_viewer_app(paths=[item_data.get('filepath')])

    def view_media(self):
        currentItems = self.image_viewer.selected_items()
        if not currentItems:
            self.error_no_selection('View Media', 'Please select at least 1 image!')
            return
        media_paths = []
        for item in currentItems:
            data = item.data(QtCore.Qt.UserRole)
            if isinstance(data, dict) and 'filepath' in data and os.path.exists(data['filepath']):
                media_paths.append(data['filepath'])
        if media_paths:
            self.launch_viewer_app(paths=media_paths)

    def save_media(self):
        currentItems = self.image_viewer.selected_items()
        if not currentItems:
            self.error_no_selection('Save Media', 'Please select an image!')
            return
        item = currentItems[-1]
        data = item.data(QtCore.Qt.UserRole)
        if isinstance(data, dict) and 'filepath' in data and 'name' in data and os.path.exists(data['filepath']):
            dialog = QtWidgets.QFileDialog()
            dialog.setWindowIcon(QtGui.QIcon('{}/icons/app_icon.png'.format(appModuleDir)))
            dialog.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
            dialog.setWindowTitle('Save attachment')
            dialog.setNameFilters(["JPEG (*.jpg *.JPG)", "PNG (*.png *.PNG)", "TIFF (*.tiff *.TIFF)"])
            dialog.setDefaultSuffix('jpg')
            dialog.setLabelText (QtWidgets.QFileDialog.Accept, "Save")
            dialog.setFileMode(QtWidgets.QFileDialog.AnyFile)
            dialog.setAcceptMode(QtWidgets.QFileDialog.AcceptSave) 
            # dialog.setDirectory(os.path.expanduser("~"))
            dialog.setOption(QtWidgets.QFileDialog.DontUseNativeDialog)
            dialog.selectFile(data['name'])

            if dialog.exec_() == QtWidgets.QDialog.Accepted:
                chosen_path = dialog.selectedFiles()[0]
                file_utils.copy(data['filepath'], chosen_path)

    def error_no_selection(self, title, message):
        # pop up user to choose application to play media
        qmsgBox = QtWidgets.QMessageBox(self)
        # set tile, icon
        qmsgBox.setWindowTitle(title)
        qmsgBox.setText(message)
        qmsgBox.setIcon(QtWidgets.QMessageBox.Critical)

        # add button
        qmsgBox.addButton('  OK  ', QtWidgets.QMessageBox.AcceptRole)
        qmsgBox.exec_()

    def launch_viewer_app(self, paths):
        mods = QtWidgets.QApplication.keyboardModifiers()
        if not self.chosen_app or mods & QtCore.Qt.ControlModifier:
            # pop up user to choose application to play media
            qmsgBox = QtWidgets.QMessageBox(self)
            # set tile, icon
            qmsgBox.setWindowTitle('View Media')
            qmsgBox.setIcon(QtWidgets.QMessageBox.Question)

            # set text
            text = 'Open media with?'
            qmsgBox.setText(text)

            # set detailed text
            detailedText = 'Playlist: \n- '
            detailedText += '\n- '.join([os.path.basename(path) for path in paths])
            qmsgBox.setDetailedText(detailedText)

            # add buttons
            for app_name in self.player_app:
                qmsgBox.addButton(app_name, QtWidgets.QMessageBox.AcceptRole)
            cancel_button = qmsgBox.addButton('Cancel', QtWidgets.QMessageBox.RejectRole)
            qmsgBox.exec_()
            clicked_button = qmsgBox.clickedButton()
            if clicked_button != cancel_button:
                app_name = clicked_button.text()
                self.chosen_app = app_name
                func = self.player_app[app_name]
                func(paths)
        else:
            func = self.player_app[self.chosen_app]
            func(paths)
    def play_keyframepro(self, paths):
        app_path = config.Software.pythonwPath
        if not os.path.exists(app_path):
            print 'Cannot find application: {}'.foramt(app_path)
            return
        script_path = '{}/rf_utils/keyframepro_utils.py'.format(config.Env.core)
        input_paths = ['-i'] + paths
        commands = [app_path, script_path] + input_paths
        kfp = subprocess.Popen(commands, 
                    stdout=subprocess.PIPE)

    def play_rv(self, paths):
        app_path = config.Software.rv
        if not os.path.exists(app_path):
            print 'Cannot find application: {}'.foramt(app_path)
            return
        commands = [app_path] + paths
        subprocess.Popen(commands)

    def browse_add_attachment(self):
        if not self.entity:
            return

        item, brief = self.get_current_brief()
        if not brief:
            self.error_no_selection('Add Attachments', 'Please select a Brief!')
            return

        dialog = QtWidgets.QFileDialog()
        dialog.setWindowTitle('Add attachments to Brief')
        dialog.setNameFilters(["PNG (*.png *.PNG)", "JPEG (*.jpg *.JPG)", "TIFF (*.tiff *.TIFF)"])
        dialog.setFileMode(QtWidgets.QFileDialog.ExistingFiles)
        dialog.setViewMode(QtWidgets.QFileDialog.Detail)
        dialog.setAcceptMode(QtWidgets.QFileDialog.AcceptOpen) 
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            chosen_paths = dialog.selectedFiles()
            self.add_attachments(links=chosen_paths)
            
    def remove_attachment(self):
        sels = self.image_viewer.selected_items()
        if not sels:
            self.error_no_selection('Remove Attachments', 'Please select at least 1 image!')
            return
        del_data = [item.data(QtCore.Qt.UserRole) for item in sels if item.data(QtCore.Qt.UserRole)]
        self.image_deleted(del_data)

    def add_attachments(self, links):
        if not self.is_admin:
            return
        item, brief = self.get_current_brief()
        if not item:
            return
        im_names = [os.path.basename(l) for l in links]
        qmsgBox = QtWidgets.QMessageBox(self)
        text = 'Add image to Brief: ' + brief.get('subject') + '?\n - ' + '\n - '.join(im_names)
        qmsgBox.setText(text)
        qmsgBox.setWindowTitle('Confirm')
        qmsgBox.setIcon(QtWidgets.QMessageBox.Question)
        qmsgBox.addButton('  Yes  ', QtWidgets.QMessageBox.AcceptRole)
        qmsgBox.addButton('  No  ', QtWidgets.QMessageBox.RejectRole)
        answer = qmsgBox.exec_()
        if answer == 0: 
            self.thread_upload_data(links)

    def thread_upload_data(self, links):
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        self.enable_ui(False)
        item, brief = self.get_current_brief()
        worker = thread_pool.Worker(partial(sg_note.upload_attachments, brief.get('id'), links))
        worker.signals.result.connect(self.upload_finished)
        self.threadpool.start(worker)

    def upload_finished(self, result):
        self.enable_ui(True)
        QtWidgets.QApplication.restoreOverrideCursor()
        if result:
            self.thread_update_data()

    def edit_content_toggled(self):
        item, brief = self.get_current_brief()
        if not item:
            self.error_no_selection('Edit Note', 'Please select a Brief!')
            self.disable_content_edit()
            return 

        current_note = brief.get('content') if brief.get('content') else ''
        # start editing
        if self.edit_content_button.isChecked():
            self.enable_content_edit()
        else: # finish editing
            # .decode(encoding='UTF-8', errors='strict')
            current_text = self.content_textEdit.toPlainText()
            if current_text != current_note:
                # popup question to user
                qmsgBox = QtWidgets.QMessageBox(self)
                qmsgBox.setText('Edit Brief note: ' + brief.get('subject') + '?')
                qmsgBox.setWindowTitle('Confirm')
                qmsgBox.setIcon(QtWidgets.QMessageBox.Question)
                qmsgBox.addButton('  Yes  ', QtWidgets.QMessageBox.AcceptRole)
                qmsgBox.addButton('  No  ', QtWidgets.QMessageBox.RejectRole)
                answer = qmsgBox.exec_()
                if answer == 0: 
                    result = sg_note.update_brief_content(briefId=brief.get('id'), 
                                                        content=current_text, 
                                                        userEntity=self.user.sg_user())
                    if result:
                        # update ui
                        self.thread_update_data()
                    else:
                        self.content_textEdit.setPlainText(current_note)
                else:
                    # set text back to previous text
                    self.content_textEdit.setPlainText(current_note)
            # disable edit UI
            self.disable_content_edit()

    def disable_content_edit(self, force=True):
        # set toggle off
        self.edit_content_button.blockSignals(True) if force else None
        self.edit_content_button.setChecked(False)
        self.edit_content_button.blockSignals(False) if force else None

        # set non editable
        self.content_textEdit.setTextInteractionFlags(QtCore.Qt.TextSelectableByMouse)
        # set look
        self.content_textEdit.setFont(self.display_font)
        self.content_textEdit.setPalette(self.grey_font_palette)
        self.content_textEdit.setStyleSheet('background-color: rgb{};'.format(self.bg_col))

    def enable_content_edit(self):
        # set editable
        self.content_textEdit.setTextInteractionFlags(QtCore.Qt.TextEditorInteraction)
        # set look
        self.content_textEdit.setPalette(self.white_font_palette)
        self.content_textEdit.setFont(self.active_font)
        self.content_textEdit.setFocus()
        self.content_textEdit.moveCursor(QtGui.QTextCursor.End)
        self.content_textEdit.setStyleSheet('background-color: rgb{};'.format(self.edit_col))

    def image_deleted(self, delete_data):
        if not self.is_admin:
            return

        att_ids = [d.get('id') for d in delete_data]
        if not att_ids:
            return

        item, brief = self.get_current_brief()
        # popup question to user
        qmsgBox = QtWidgets.QMessageBox(self)
        qmsgBox.setText('Delete ' + str(len(att_ids)) + ' image(s) from Brief: ' + brief.get('subject') + '?')
        qmsgBox.setWindowTitle('Confirm')
        qmsgBox.setIcon(QtWidgets.QMessageBox.Warning)
        qmsgBox.addButton('  Yes  ', QtWidgets.QMessageBox.AcceptRole)
        qmsgBox.addButton('  No  ', QtWidgets.QMessageBox.RejectRole)
        answer = qmsgBox.exec_()
        if answer == 0: 
            self.thread_delete_attachments(att_ids)

    def thread_delete_attachments(self, att_ids):
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        self.enable_ui(False)
        worker = thread_pool.Worker(partial(self.delete_attachments, att_ids))
        worker.signals.result.connect(self.delete_attachment_finished)
        self.threadpool.start(worker)

    def delete_attachments(self, att_ids):
        results = []
        for att_id in att_ids:
            result = sg_note.sg.delete('Attachment', att_id)
            results.append(result)
        return results

    def delete_attachment_finished(self, results, *args, **kwargs):
        self.enable_ui(True)
        QtWidgets.QApplication.restoreOverrideCursor()
        self.thread_update_data()

    def trigger_update_size(self):
        self.resizeEvent(QtGui.QResizeEvent(self.size(), QtCore.QSize()))

    def get_current_brief(self):
        sels = self.brief_treeWidget.selectedItems()
        if not sels:
            return None, None
        current_item_data = sels[0].data(QtCore.Qt.UserRole, 0)
        return sels[0], current_item_data

    def get_all_briefs(self):
        rootItem = self.brief_treeWidget.invisibleRootItem()
        briefs = defaultdict(list)
        for i in range(rootItem.childCount()):
            item = rootItem.child(i)
            data = item.data(QtCore.Qt.UserRole, 0)
            if data:
                briefs['Asset'].append(data)
            else:
                children = [item.child(i) for i in range(item.childCount())]
                children_data = [c.data(QtCore.Qt.UserRole, 0) for c in children if c.data(QtCore.Qt.UserRole, 0)]
                briefs[item.text(0)].extend(children_data)
        return briefs

    def thread_fetch_data(self, entity): 
        if not entity:
            return
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        worker = thread_pool.Worker(self.fetch_data, entity)
        worker.signals.result.connect(self.list_briefs)
        self.threadpool.start(worker)

    def fetch_data(self, entity): 
        # fetch from SG
        brief_data = sg_note.fetch_brief(entity.get('project').get('name'), entity)
        # decode content
        root_data = []
        step_data = []
        for key in sorted(brief_data):  # sorted by key
            briefs = brief_data[key]
            sorted_briefs = sorted(briefs, key=lambda i:i.get('updated_at') \
                                            if i.get('updated_at') else i.get('created_at'), \
                                            reverse=True)
            new_briefs = []
            for brief in sorted_briefs:
                brief = self.decode_brief(brief)  # decode content with Thai language
                new_briefs.append(brief)
            if key == 'entity':
                root_data.append((key, new_briefs))
            else:
                step_data.append((key, new_briefs))
        new_data = root_data + step_data
        return new_data

    def thread_update_data(self):
        self.enable_ui(False)
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        item, brief = self.get_current_brief()
        worker = thread_pool.Worker(partial(sg_note.get_note, brief.get('id')))
        worker.signals.result.connect(self.update_current_brief)
        self.threadpool.start(worker)

    def update_current_brief(self, new_brief):
        new_brief = self.decode_brief(new_brief)
        current_item, current_brief = self.get_current_brief()
        self.setup_brief_item(current_item, new_brief)
        # needs to trigger resizeEvent or text will be on top of icon
        self.brief_treeWidget.resizeEvent(QtGui.QResizeEvent(self.brief_treeWidget.size(), QtCore.QSize()))
        self.filter_brief()
        self.enable_ui(True)
        QtWidgets.QApplication.restoreOverrideCursor()
        self.brief_selected(force=True)
        
    def decode_brief(self, brief):
        # decode Thai
        note_text = brief.get('content')
        if note_text:
            note_text = note_text.decode(encoding='UTF-8', errors='strict')
            brief['content'] = note_text

        subject = brief.get('subject')
        if subject:
            subject = subject.decode(encoding='UTF-8', errors='strict')
            brief['subject'] = subject
        return brief

    def brief_selected(self, force=False):
        items = self.brief_treeWidget.selectedItems()
        self.reset_info()
        self.reset_view()
        if not items:
            return

        # get item data
        item = items[0]
        item_data = item.data(QtCore.Qt.UserRole, 0)
        if not item_data:
            return

        # update info 
        self.update_info(data=item_data)

        # get attachements
        attachments = item_data.get('attachments', [])
        bid = item_data.get('id')

        # update view if any attachment
        if attachments:
            # pull from cache if index of item exists in cache
            if bid in self.cache and not force:
                self.update_view(images=self.cache[bid])
            else:  # download
                QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
                self.enable_ui(False)
                worker = thread_pool.Worker(self.download_attatchments, attachments, bid)
                worker.signals.result.connect(self.download_finished)
                self.threadpool.start(worker)

    def list_briefs(self, brief_data):
        self.brief_treeWidget.clear()
        self.reset_info()
        self.reset_view()

        # ----- update brief to UI
        for btype, briefs in brief_data:
            # create department parent object if needed
            parent = self.brief_treeWidget
            if btype != 'entity':
                parent = QtWidgets.QTreeWidgetItem(self.brief_treeWidget)
                parent.setText(0, btype.title())

            # create brief item
            for brief in briefs:
                item = QtWidgets.QTreeWidgetItem(parent)
                item.setIcon(0, QtGui.QIcon('{}/icons/brief_icon.png'.format(appModuleDir)))
                self.setup_brief_item(item, brief)

        self.filter_brief()
        QtWidgets.QApplication.restoreOverrideCursor()

    def setup_brief_item(self, item, brief):
        subject = brief.get('subject')
        item.setData(QtCore.Qt.UserRole, 0, brief)
        # date
        date = brief.get('updated_at') if brief.get('updated_at') else brief.get('created_at')
        time_since_updated = datetime.now() - date.replace(tzinfo=None)
        if time_since_updated.days <= self.NEW_ITEM_DURATION:
            itemWidget = QtWidgets.QWidget(self)
            widgetLayout = QtWidgets.QHBoxLayout(itemWidget)
            widgetLayout.setContentsMargins(0, 0, 0, 0)
            widgetLayout.setSpacing(5)

            textLabel = QtWidgets.QLabel(subject)
            textLabel.setFixedWidth(textLabel.sizeHint().width())
            textLabel.setStyleSheet("background-color: rgba(0, 0, 0, 0); border: 0px;")
            widgetLayout.addWidget(textLabel)
            
            movie_label = QtWidgets.QLabel()
            movie_label.setStyleSheet("background-color: rgba(0, 0, 0, 0); border: 0px;")
            movie_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
            widgetLayout.addWidget(movie_label)

            movie = QtGui.QMovie('{}/icons/new_icon.gif'.format(appModuleDir))
            movie.setScaledSize(QtCore.QSize(22, 22))
            movie_label.setMovie(movie)
            movie.start()
            self.brief_treeWidget.setItemWidget(item, 0, itemWidget)
        else:
            item.setText(0, subject)

    def status_filter_changed(self):
        sels = self.brief_treeWidget.selectedItems()
        self.filter_brief()
        if sels:
            if sels[0].isHidden():
                sels[0].setSelected(False)

    def filter_brief(self):
        # filter
        index = self.status_combobox.currentIndex()
        status_to_show = self.status_combobox.itemData(index, QtCore.Qt.UserRole)
        # get all items
        rootItem = self.brief_treeWidget.invisibleRootItem()
        items = []
        for i in range(rootItem.childCount()):
            item = rootItem.child(i)
            if item.childCount():  # it's a group
                children = [item.child(i) for i in range(item.childCount())]
                items += children
            # it's entity level brief
            else:  
                items.append(item)

        for item in items:
            data = item.data(QtCore.Qt.UserRole, 0)

            status = data.get('sg_status_list')
            hidden = False
            if status_to_show != 'All' and status != status_to_show:
                hidden = True
            item.setHidden(hidden)
        sels = self.brief_treeWidget.selectedItems()
        if sels:
            data = sels[0].data(QtCore.Qt.UserRole, 0)
            self.update_info(data)

    def download_attatchments(self, attachments, bid):
        images = []
        for att in attachments:
            fn, ext = os.path.splitext(att.get('name'))
            fh, temp = tempfile.mkstemp(suffix=ext)
            os.close(fh)
            sg_note.download_attachment(att, temp)
            att['filepath'] = temp
            images.append(att)
        return (images, bid)

    def download_finished(self, results):
        images, bid = results
        self.update_view(images=images)
        self.cache[bid] = images
        self.enable_ui(True)
        QtWidgets.QApplication.restoreOverrideCursor()

    def update_view(self, images):
        if images:
            self.image_viewer.add_images(paths=images) 

    def update_info(self, data):
        # creator
        user = data.get('sg_modified_by') if data.get('sg_modified_by') else data.get('user')
        if user:
            user = user.get('name')
            self.creator_value_label.setFont(self.active_font)
            self.creator_value_label.setStyleSheet('color: white')
            self.creator_value_label.setText(user)
        # date
        date = data.get('updated_at') if data.get('updated_at') else data.get('created_at')
        if date:
            self.date_value_label.setFont(self.active_font)
            self.date_value_label.setStyleSheet('color: white')
            self.date_value_label.setText(str(datetime.strftime(date, '%y/%m/%d-%H:%M')))
        # status
        status = data.get('sg_status_list')
        if status:
            self.status_value_label.text_label.setFont(self.active_font)
            self.status_value_label.text_label.setStyleSheet('color: white')
            self.status_value_label.set_status(status)
        # note
        note_text = data.get('content')
        if note_text:
            self.content_textEdit.setPlainText(note_text)

    def reset_view(self):
        self.image_viewer.clear()

    def reset_info(self):
        self.content_textEdit.clear()

        self.creator_value_label.setFont(self.display_font)
        self.creator_value_label.setStyleSheet('color: grey')
        self.creator_value_label.setText('N/A')

        self.date_value_label.setFont(self.display_font)
        self.date_value_label.setStyleSheet('color: grey')
        self.date_value_label.setText('N/A')

        self.status_value_label.text_label.setFont(self.display_font)
        self.status_value_label.text_label.setStyleSheet('color: grey')
        self.status_value_label.set_status('N/A')

    def enable_ui(self, enable):
        self.brief_treeWidget.setEnabled(enable)
        self.edit_content_button.setEnabled(enable)
        self.view_media_button.setEnabled(enable)
        self.save_media_button.setEnabled(enable)
        self.add_brief_button.setEnabled(enable)
        self.remove_brief_button.setEnabled(enable)
        self.add_image_button.setEnabled(enable)
        self.remove_image_button.setEnabled(enable)
        self.status_combobox.setEnabled(enable)