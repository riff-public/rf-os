#!/usr/bin/env python
# -- coding: utf-8 --

import os
import sys
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

#!/usr/bin/env python
# -- coding: utf-8 --

from Qt import QtCore
from Qt import QtGui
from Qt import QtWidgets

from rf_utils import thread_pool
from . import master
# reload(master)

isNuke = True
try:
    import nuke
    import nukescripts
    from rf_nuke.utils import shared_comp
    reload(shared_comp)
except ImportError:
    isNuke = False

modulePath = sys.modules[__name__].__file__.replace('\\', '/')
moduleDir  = os.path.dirname(modulePath)
appModuleDir = os.path.dirname(moduleDir)

class Ui(master.Ui):
    """docstring for Ui"""
    def __init__(self, thread=None, parent=None):
        super(Ui, self).__init__(thread=thread, parent=parent)
        self.step = 'keyvis'
        



