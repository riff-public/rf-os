#!/usr/bin/env python
# -- coding: utf-8 --
import os
import sys
import subprocess
import time
import re
from glob import glob
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import collections
from datetime import datetime
from functools import partial

from Qt import QtCore
from Qt import QtGui
from Qt import QtWidgets

import rf_config as config 
from rf_utils.context import context_info
from rf_utils import thread_pool
from rf_utils import user_info
from rf_utils.sg import sg_process
from rf_utils import file_utils
from rf_utils.widget import image_widget
from rf_utils.widget import media_widget
from rf_utils.widget import textEdit
from rf_utils.widget.status_widget import Icon
from rf_utils import project_info
from rf_shotgun_event_daemon.src.plugins import status_update
# reload(status_update)
from rf_utils.pipeline.notification import noti_module_cliq
# reload(noti_module_cliq)

modulePath = sys.modules[__name__].__file__
moduleDir  = os.path.dirname(modulePath)
appModuleDir = os.path.dirname(moduleDir)

class Color:
    projectLabel = 'color: rgb(220, 220, 220);'
    entityLabel = 'color: rgb(180, 220, 240);'
    grpLabel = 'color: rgb(160, 160, 160);'
    submitButton = 'background-color: rgb(80, 160, 240);'
    snapButton = 'background-color: rgb(200, 200, 200);'


class Default:
    userIcon = '%s/icons/user.png' % os.path.split(moduleDir)[0]
    refreshIcon = '%s/icons/refresh.png' % os.path.split(moduleDir)[0]
    captureIcon = '%s/icons/snap.png' % os.path.split(moduleDir)[0]

class TaskListWidget(QtWidgets.QListWidget):
    mouseReleased = QtCore.Signal(bool)

    def __init__(self, parent=None):
        super(TaskListWidget, self).__init__(parent)
        self._mouse_holding = False

    def mouseReleaseEvent(self, event):
        super(TaskListWidget, self).mouseReleaseEvent(event)
        self.mouse_holding = False
        buttons = event.button()
        if buttons == QtCore.Qt.LeftButton:
            self.mouseReleased.emit(True)

    def mousePressEvent(self, event):
        super(TaskListWidget, self).mousePressEvent(event)
        self.mouse_holding = True

    @property
    def mouse_holding(self):
        return self._mouse_holding

    @mouse_holding.setter
    def mouse_holding(self, value):
        self._mouse_holding = value

class TaskItem(QtWidgets.QWidget):
    """docstring for EntityItem"""
    def __init__(self, text='', text2='', icon='', movie='', parent=None):
        super(TaskItem, self).__init__(parent=parent)
        self.icon_size = 16
        self.movie_size = 22

        self.enable_font = QtGui.QFont()
        self.enable_font.setItalic(False)
        self.disable_font = QtGui.QFont()
        # self.disable_font.setItalic(True)
        self.text2_font = QtGui.QFont()
        self.text2_font.setItalic(True)
        self.enable_styleSheet = 'color: rgb(255, 255, 255)'
        self.disable_styleSheet = 'color: rgb(175, 175, 175)'
        self.text2_styleSheet = 'color: rgb(175, 175, 175)'

        self.setup_ui()
        if text:
            self.set_text(text=text)
        if text2:
            self.set_text2(text=text2)
        if icon:
            self.set_icon(path=icon)
        if movie:
            self.set_movie(path=movie)

    def setup_ui(self):
        self.layout = QtWidgets.QHBoxLayout()
        self.layout.setSpacing(3)
        self.layout.setContentsMargins(3, 3, 3, 3)
        self.setLayout(self.layout)

        # widgets
        self.icon_label = QtWidgets.QLabel()
        self.icon_label.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.text_label = QtWidgets.QLabel()
        self.text_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.text2_label = QtWidgets.QLabel()
        self.text2_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.text2_label.setFont(self.text2_font)
        self.text2_label.setStyleSheet(self.text2_styleSheet)
        self.movie_label = QtWidgets.QLabel()
        self.movie_label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        spacerItem = QtWidgets.QSpacerItem(self.icon_size, self.icon_size, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.layout.addWidget(self.icon_label)
        self.layout.addWidget(self.text_label)
        self.layout.addWidget(self.text2_label)
        self.layout.addWidget(self.movie_label)
        self.layout.addItem(spacerItem)

        self.layout.setStretch(0, 0)
        self.layout.setStretch(1, 0)
        self.layout.setStretch(2, 0)
        self.layout.setStretch(3, 0)
        self.layout.setStretch(4, 1)
        self.setStyleSheet("background-color: rgba(0, 0, 0, 0); border: 0px;")

    def set_icon(self, path):
        pixmap = QtGui.QPixmap(path).scaled(self.icon_size, self.icon_size, QtCore.Qt.KeepAspectRatio)
        self.icon_label.setPixmap(pixmap)

    def set_text(self, text):
        self.text_label.setText(text)

    def set_text2(self, text):
        self.text2_label.setText('- ' + text)

    def text(self):
        return self.text_label.text()

    def text2(self):
        curr_text = self.text2_label.text()
        return curr_text.replace('- ', '')

    def raw_text(self):
        text2 = ' ' + self.text2_label.text() if self.text2_label.text() else ''
        return self.text_label.text() + text2

    def set_text_enable(self, enable):
        if enable:
            self.text_label.setFont(self.enable_font)
            self.text_label.setStyleSheet(self.enable_styleSheet)
        else:
            self.text_label.setFont(self.disable_font)
            self.text_label.setStyleSheet(self.disable_styleSheet)

    def set_movie(self, path):
        movie = QtGui.QMovie(path)
        movie.setScaledSize(QtCore.QSize(self.movie_size, self.movie_size))
        self.movie_label.setMovie(movie)
        movie.start()

class Ui(QtWidgets.QWidget):
    """docstring for Ui"""
    # contants
    IMAGE_PER_ROW = 4
    GRIDSCALE = (4096, 4096)
    NEW_ITEM_DURATION = 3  # anything newer than 3 days will have "new" icon
    BORDER_WIDTH = 32

    DEFAULT_VID_SIZE = (1920, 1080)
    DEFAULT_VID_FPS = 24
    DEFAULT_VID_QUALITY = 1.0
    VID_SIZE_MULT = 1.0
    AUTOPLAY_VID = True
    SHOW_PLAY_BUTTON = True

    def __init__(self, thread=None, parent=None):
        super(Ui, self).__init__(parent)
        self.parent = parent
        self.threadpool = QtCore.QThreadPool() if not thread else thread

        # internal vars
        self.context_sync = False
        self.entity = None
        self.context = None
        self._include_related = False
        self._previous_tasks = []  # store all task items selected when switching between asset (refresh() called)
        self._previous_task_items_selected = []  # store previous selection that has been displayed in viewer
        self.player_app = {'Keyframe Pro': self.play_keyframepro, 'RV': self.play_rv}
        self.chosen_app = None
        
        # icon paths
        self.nopreivew_path = '{}/icons/nopreview_icon.png'.format(appModuleDir)
        self.new_icon = '{}/icons/new_icon.gif'.format(appModuleDir)
        icon_size = int((self.GRIDSCALE[0] + self.GRIDSCALE[1]) *0.5 * 0.1)  # start icon at 10% the averaged grid size
        self.item_icon_size = (icon_size, icon_size)

        # fonts
        self.taskname_font = QtGui.QFont('Calibri', 64)
        self.taskname_color = QtGui.QColor('yellow')
        self.bg_col = (43, 43, 43)
        self.edit_col = (100, 50, 50)
        self.active_font = QtGui.QFont()
        self.active_font.setItalic(False)
        self.active_font.setPointSize(8)
        self.display_font = QtGui.QFont()
        self.display_font.setItalic(True)
        self.display_font.setPointSize(8)
        self.approved_border_color = QtGui.QColor('green')
        self.inprogress_border_color = QtGui.QColor('red')

        # palettes
        self.grey_font_palette = QtGui.QPalette()
        self.grey_font_palette.setBrush(QtGui.QPalette.Text, QtGui.QBrush(QtGui.QColor(128, 128, 128)))
        self.white_font_palette = QtGui.QPalette()
        self.white_font_palette.setBrush(QtGui.QPalette.Text, QtGui.QBrush(QtGui.QColor(255, 255, 255)))

        self.setup_ui()
        self.init_signal()
        self.set_default()

    def setup_ui(self):
        # main layout
        self.layout = QtWidgets.QVBoxLayout()
        self.frame = QtWidgets.QFrame(self)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.main_layout = QtWidgets.QVBoxLayout(self.frame)
        self.layout.addWidget(self.frame)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

        # splitter
        self.mainSplitter = QtWidgets.QSplitter()
        self.mainSplitter.setOrientation(QtCore.Qt.Horizontal)

        self.processSplitWidget = QtWidgets.QWidget(self.mainSplitter)
        self.processSplitWidget.setMinimumWidth(120)
        self.viewSplitWidget = QtWidgets.QWidget(self.mainSplitter)
        self.mainSplitter.setSizes([135, 420])

        self.main_layout.addWidget(self.mainSplitter)

        # process list layout
        self.tasklist_layout = QtWidgets.QVBoxLayout(self.processSplitWidget)
        self.tasklist_layout.setSpacing(6)
        self.tasklist_layout.setContentsMargins(3, 0, 3, 0)

        # status layout
        self.status_layout = QtWidgets.QFormLayout()
        self.status_layout.setLabelAlignment(QtCore.Qt.AlignLeft)
        self.status_layout.setFormAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)
        self.status_layout.setSpacing(3)
        self.tasklist_layout.addLayout(self.status_layout)
        
        self.creator_value_label = QtWidgets.QLabel()
        self.date_value_label = QtWidgets.QLabel()
        self.startdue_value_label = QtWidgets.QLabel()

        self.status_layout.addRow('Creator: ', self.creator_value_label)
        self.status_layout.addRow('Updated: ', self.date_value_label)
        self.status_layout.addRow('Start/Due: ', self.startdue_value_label)
        # list widget
        self.task_listwidget = TaskListWidget()
        self.task_listwidget.setSortingEnabled(False)
        self.task_listwidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.tasklist_layout.addWidget(self.task_listwidget)

        # view layout 
        self.view_layout = QtWidgets.QVBoxLayout(self.viewSplitWidget)
        self.view_layout.setSpacing(9)
        self.view_layout.setContentsMargins(0, 0, 0, 0)

        # media layout
        self.media_layout = QtWidgets.QHBoxLayout()
        self.media_layout.setSpacing(9)
        self.view_layout.addLayout(self.media_layout)

        self.media_view_layout = QtWidgets.QHBoxLayout()
        self.media_view_layout.setContentsMargins(0, 0, 0, 0)
        self.media_view_layout.setSpacing(0)
        self.media_layout.addLayout(self.media_view_layout)

        # media splitter
        self.viewSplitter = QtWidgets.QSplitter()
        self.viewSplitter.setOrientation(QtCore.Qt.Horizontal)
        self.stillSplitWidget = QtWidgets.QWidget(self.viewSplitter)
        self.vidSplitWidget = QtWidgets.QWidget(self.viewSplitter)
        self.viewSplitter.setSizes([330, 100])
        self.media_layout.addWidget(self.viewSplitter)

        # still image viewer
        self.still_layout = QtWidgets.QVBoxLayout(self.stillSplitWidget)
        self.still_layout.setContentsMargins(0, 0, 0, 0)
        self.image_viewer = image_widget.ImageGridViewer(gridScale=self.GRIDSCALE, imagePerRow=self.IMAGE_PER_ROW)
        self.image_viewer.borderWidth = self.BORDER_WIDTH  # border width
        self.image_viewer.setBackgroundBrush(QtGui.QColor(self.bg_col[0], self.bg_col[1], self.bg_col[2]))
        self.still_layout.addWidget(self.image_viewer)

        # video viewer
        self.vid_layout = QtWidgets.QVBoxLayout(self.vidSplitWidget)
        self.vid_layout.setContentsMargins(0, 0, 0, 0)

        self.vid_viewer = media_widget.MediaGridViewer(quality=self.DEFAULT_VID_QUALITY)
        self.vid_viewer.setStyleSheet('background-color: rgb{};'.format(self.bg_col))
        self.vid_viewer.auto_play = self.AUTOPLAY_VID
        self.vid_viewer.show_play_button = self.SHOW_PLAY_BUTTON
        self.vid_viewer.setMinimumWidth(128)
        self.vid_layout.addWidget(self.vid_viewer)

        # media button layout
        self.media_button_layout = QtWidgets.QVBoxLayout()
        self.media_button_layout.setSpacing(5)
        # view button
        self.view_media_button = QtWidgets.QPushButton('')
        self.view_media_button.setFixedSize(QtCore.QSize(40, 22))
        self.view_media_button.setIcon(QtGui.QIcon('{}/icons/eye_icon.png'.format(appModuleDir)))
        # dir button
        self.open_image_dir_button = QtWidgets.QPushButton('')
        self.open_image_dir_button.setFixedSize(QtCore.QSize(40, 22))
        self.open_image_dir_button.setIcon(QtGui.QIcon('{}/icons/folder_icon.png'.format(appModuleDir)))
        spacerItem1 = QtWidgets.QSpacerItem(25, 25, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)

        self.media_button_layout.addWidget(self.view_media_button)
        self.media_button_layout.addWidget(self.open_image_dir_button)
        self.media_button_layout.addItem(spacerItem1)
        self.media_layout.addLayout(self.media_button_layout)

        # description layout
        self.description_layout = QtWidgets.QVBoxLayout()
        self.description_layout.setContentsMargins(0, 0, 0, 0)
        self.description_layout.setSpacing(0)
        self.view_layout.addLayout(self.description_layout)

        # description button layout
        self.description_button_layout = QtWidgets.QHBoxLayout()
        self.description_button_layout.setContentsMargins(0, 0, 0, 0)
        self.description_label = QtWidgets.QLabel('Description: ')
        spacerItem2 = QtWidgets.QSpacerItem(25, 25, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.edit_description_button = QtWidgets.QPushButton('')
        self.edit_description_button.setCheckable(True)
        self.edit_description_button.setFixedSize(QtCore.QSize(40, 18))
        self.edit_description_button.setIcon(QtGui.QIcon('{}/icons/pencil_icon.png'.format(appModuleDir)))
        self.description_button_layout.addWidget(self.description_label)
        self.description_button_layout.addItem(spacerItem2)
        self.description_button_layout.addWidget(self.edit_description_button)
        self.description_layout.addLayout(self.description_button_layout)

        # description box
        self.description_textEdit = textEdit.TextEditWithPlaceHolder()
        self.description_textEdit.setStyleSheet('background-color: rgb{};'.format(self.bg_col))
        self.description_textEdit.setPlaceholderText('< No description >')
        self.description_textEdit.setWordWrapMode(QtGui.QTextOption.WrapAtWordBoundaryOrAnywhere)
        self.description_textEdit.setTextInteractionFlags(QtCore.Qt.NoTextInteraction)
        self.description_textEdit.setFont(self.display_font)
        self.description_textEdit.setPalette(self.grey_font_palette)
        self.description_layout.addWidget(self.description_textEdit)

        # setup sizes
        self.media_layout.setStretch(0, 15)
        self.media_layout.setStretch(1, 3)
        self.media_layout.setStretch(2, 1)
        self.view_layout.setStretch(0, 10)
        self.view_layout.setStretch(1, 1)
        self.description_layout.setStretch(0, 1)
        self.description_layout.setStretch(1, 3)

        # toolTips
        self.view_media_button.setToolTip('Open images in media viewer app')
        self.task_listwidget.setToolTip('Show all tasks for this asset')
        self.open_image_dir_button.setToolTip('Open file explorer at image directories')
        self.edit_description_button.setToolTip('Edit description (Supervisor/Producer only)')

    def refresh(self, entity):
        self.store_previous_tasks()
        self.entity = entity

        self.reset_info()
        self.reset_view()
        self.task_listwidget.clear()

        if not self.entity:
            return

        # setup path context 
        context = context_info.Context()
        context.update(project=entity.get('project').get('name'),
                    entityType='asset',
                    entity=entity.get('code'))
        self.context = context_info.ContextPathInfo(context=context)

        if self.context:
            # update media widget settings everytime entity changes
            self.vid_viewer.fps = self.context.projectInfo.render.fps()
            w, h = self.context.projectInfo.render.final_outputH()
            self.vid_viewer.item_size = (w * self.VID_SIZE_MULT, h * self.VID_SIZE_MULT)  # multply the load size for performance
        else:
            self.vid_viewer.fps = self.DEFAULT_VID_FPS
            self.vid_viewer.item_size = self.DEFAULT_VID_SIZE
        
        self.thread_fetch_data(self.entity)

    def set_default(self):
        # only allow description edit button for sup, producer, admin
        user = user_info.User()
        if user.is_producer() or user.is_admin() or user.is_supervisor():
            self.edit_description_button.setVisible(True)
        else:
            self.edit_description_button.setVisible(False)

        # clear UI
        self.task_listwidget.clear()
        self.reset_info()
        self.reset_view()

    def init_signal(self):
        # task list widget
        self.task_listwidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.task_listwidget.customContextMenuRequested.connect(self.task_right_clicked)
        self.task_listwidget.itemSelectionChanged.connect(self.task_selected)
        self.task_listwidget.mouseReleased.connect(lambda: self.update_view(force=False))

        # buttons and clicks
        self.edit_description_button.toggled.connect(self.edit_description_toggled)
        self.description_textEdit.editorLostFocus.connect(lambda: self.disable_description_edit(force=False))
        self.view_media_button.clicked.connect(self.view_media)
        self.image_viewer.itemDoubleClicked.connect(self.view_media)
        self.vid_viewer.itemDoubleClicked.connect(self.view_media)
        self.open_image_dir_button.clicked.connect(self.open_media_dir)

        # resize video item when view splitter moved
        self.viewSplitter.splitterMoved.connect(lambda: self.vid_viewer.stretch_items(mode='horizontal'))

    def resizeEvent(self, event):
        super(Ui, self).resizeEvent(event)
        self.vid_viewer.stretch_items(mode='horizontal')

    def trigger_update_size(self):
        self.resizeEvent(QtGui.QResizeEvent(self.size(), QtCore.QSize()))

    def thread_fetch_data(self, entity): 
        if not self.step or not entity:
            return
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        worker = thread_pool.Worker(self.fetch_data, entity)
        worker.signals.result.connect(self.list_tasks)
        self.threadpool.start(worker)

    def thread_update_data(self, entity, taskname, index): 
        if not self.step or not entity:
            return
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        worker = thread_pool.Worker(self.fetch_data, entity)
        worker.signals.result.connect(partial(self.update_task, entity, taskname, index))
        self.threadpool.start(worker)

    def get_related(self, entity):
        filters = [['id','is', entity['id']]]
        fields = ['sg_link_designs', 'id']
        result = sg_process.sg.find_one('Asset', filters, fields)
        return result.get('sg_link_designs', [])

    def fetch_data(self, entity): 
        # fetch from SG
        fields = [
            'code', 'description', 'sg_status_list', 'sg_path_to_movie', 'updated_at',
            'entity', 'user', 'sg_task', 'created_at', 'sg_task.Task.sg_status_list', 
            'sg_task.Task.start_date', 'sg_task.Task.due_date']

        # get related
        extra_entity = []
        if self._include_related:
            extra_entity = self.get_related(entity)
        if not extra_entity:
            filters = [['entity', 'is', entity], ['sg_task.Task.step.Step.code', 'is', self.step]]
        else:
            filters = [['sg_task.Task.step.Step.code', 'is', self.step]]
            advancedFilter1 = {
                                "filter_operator": "any",
                                "filters": [['entity', 'is', e] for e in extra_entity]
                            }
            filters.append(advancedFilter1)

        versions = sg_process.sg.find('Version', filters, fields)

        # store in dict
        tasks = collections.OrderedDict()  # {'task':[v1, v2, v3]}
        parent_tasks = collections.OrderedDict()
        for version in versions:
            taskname = version['sg_task'].get('name', None)
            version_entity = version['entity']
            if version_entity['id'] != entity['id']:
                entityname = version_entity.get('name')
                taskname = '{} - {}'.format(taskname, entityname)
                if taskname and taskname not in parent_tasks:
                    parent_tasks[taskname] = []
                parent_tasks[taskname].append(version)
            else:
                if taskname and taskname not in tasks:
                    tasks[taskname] = []
                tasks[taskname].append(version)

        tasks = collections.OrderedDict([(k, tasks[k]) for k in sorted(tasks.keys())])
        if parent_tasks:
            parent_tasks = collections.OrderedDict([(k, parent_tasks[k]) for k in sorted(parent_tasks.keys())])
            tasks.update(parent_tasks)

        return tasks
    
    def decode_descriptions(self, versions):
        task_versions = []
        for version in versions:
            if 'description' in version and version['description']:
                version['description'] = version['description'].decode(encoding='UTF-8', errors='strict')
            task_versions.append(version)
        return task_versions

    def update_task(self, entity, taskname, index, tasks, *args, **kwargs):
        versions = tasks[taskname]
        task_versions = self.decode_descriptions(versions)
        selected_version = task_versions[index]
        data = {'current': index, 'versions':task_versions, 'entity':entity}
        for i in range(self.task_listwidget.count()):
            item = self.task_listwidget.item(i)
            itemWidget = self.task_listwidget.itemWidget(item)
            # if itemWidget.text() == taskname:
            if item.data(QtCore.Qt.UserRole)['entity']['name'] == entity['name'] and itemWidget.text() == taskname:
                self.update_task_item(item, taskname, entity, selected_version, data)
                if itemWidget.raw_text() in self._previous_tasks:
                    item.setSelected(True)
                break

        self.task_selected()
        self.update_view(force=True)
        QtWidgets.QApplication.restoreOverrideCursor()

    def list_tasks(self, tasks):
        self.task_listwidget.clear()
        self.reset_info()
        self.reset_view()
        # add tasks to list
        for rawname, versions in tasks.iteritems():  # sort by task name
            relatedname = ''
            if ' - ' in rawname:
                splits = rawname.split(' - ')
                taskname = splits[0]
                relatedname = splits[1]
            else:
                taskname = rawname

            item = QtWidgets.QListWidgetItem()
            task_versions = self.decode_descriptions(versions)
            selected_version = task_versions[-1]
            entity = selected_version['entity']
            data = {'current': len(task_versions)-1, 'versions':task_versions, 'entity': entity}

            self.task_listwidget.addItem(item)
            self.update_task_item(item, taskname, entity, selected_version, data)
            if rawname in self._previous_tasks:
                item.setSelected(True)

        self.task_selected()
        self.update_view(force=True)
        QtWidgets.QApplication.restoreOverrideCursor()

    def update_task_item(self, item, taskname, entity, version, data):
        isLatest = data['current'] == len(data['versions'])-1
        status_key = 'sg_task.Task.sg_status_list' if isLatest else 'sg_status_list'
        status_icon_path = '{0}/{1}'.format(Icon.path, Icon.statusMap[version[status_key]]['icon'])

        relatedname = entity['name'] if entity['name'] != self.entity['code'] else ''
        itemWidget = TaskItem(text=taskname, text2=relatedname, icon=status_icon_path)

        # enable text only if it's the latest version
        text_enable = True if isLatest else False
        itemWidget.set_text_enable(text_enable)

        # attach data into item
        item.setData(QtCore.Qt.UserRole, data)
        item.setSizeHint(itemWidget.sizeHint())
        self.task_listwidget.setItemWidget(item, itemWidget)
        
        # set Tooltip
        user = version.get('user')
        if user:
            user = user.get('name')
        else:
            user = 'N/A'
        date = version.get('updated_at') if version.get('updated_at') else version.get('created_at')
        if date:
            dateStr = str(datetime.strftime(date, '%y/%m/%d-%H:%M'))
            # new video
            time_since_updated = datetime.now() - date.replace(tzinfo=None)
            if time_since_updated.days <= self.NEW_ITEM_DURATION:
                itemWidget.set_movie(self.new_icon)
        else:
            dateStr = 'N/A'
        itemToolTip = 'Creator: {}\nUpdated: {}'.format(user, dateStr)
        item.setToolTip(itemToolTip)

    def store_previous_tasks(self):
        items = self.task_listwidget.selectedItems()
        itemWidgets = [self.task_listwidget.itemWidget(i) for i in items]
        self._previous_tasks = [i.raw_text() for i in itemWidgets]

    def task_selected(self):
        items = self.task_listwidget.selectedItems()
        self.reset_info()
        if not items:
            return

        if len(items) == 1:
            index, versions = self.get_item_versions(item=items[0])
            selected_version = versions[index]
            self.update_info(data=selected_version)
        else:
            self.set_multiple_info()

    def task_right_clicked(self, pos):
        item = self.task_listwidget.itemAt(pos)
        # right clicked on an item
        if item:
            # create menu
            rightClickMenu = QtWidgets.QMenu(self)

            index, versions = self.get_item_versions(item=item)
            version_actions = []
            for i, version in enumerate(versions):
                action = QtWidgets.QAction(version['code'], self)
                action.setCheckable(True)
                if i == index:  # set checked for current version
                    action.setChecked(True)
                action.triggered.connect(partial(self.update_task_version, item, i))
                version_actions.append(action)
            # add actions
            for action in version_actions:
                rightClickMenu.addAction(action)

            # status widget for producer and only with selected asset only
            status_actions = []
            user = user_info.User()
            has_right = True if user.is_producer() or user.is_admin() or user.is_supervisor() else False
            itemData = item.data(QtCore.Qt.UserRole)
            if has_right and itemData['entity']['id'] == self.entity['id']:
                # add separator
                rightClickMenu.addSeparator()
                # add submenu
                status_menu = QtWidgets.QMenu('Set status...',  self)
                rightClickMenu.addMenu(status_menu)
                selected_version = versions[index]
                isLatest = True if index == len(versions)-1 else False
                task_status = [s for s in Icon.statusMap.keys() if s in Icon.availableMaps['Task']]
                version_status = [s for s in Icon.statusMap.keys() if s in Icon.availableMaps['Version']]
                available_status = task_status if isLatest else version_status
                # add status actions
                for status in available_status:
                    action = QtWidgets.QAction(Icon.statusMap[status]['display'], self)
                    icon = QtGui.QIcon('{0}/{1}'.format(Icon.path, Icon.statusMap[status]['icon']))
                    action.setIcon(icon)
                    
                    action.triggered.connect(partial(self.update_status, selected_version, status, isLatest, index))
                    status_actions.append(action)
                # add actions
                for status_action in status_actions:
                    status_menu.addAction(status_action)
            
            if version_actions or status_actions:
                rightClickMenu.exec_(self.task_listwidget.mapToGlobal(pos))

    def update_status(self, version, newStatus, isLatest, version_index):
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        versionId = version.get('id')
        # set version status, if newStatus is not 'apr', 'rev' or 'fix', use 'wip'
        if newStatus in Icon.availableMaps['Version']:
            versionStatus = newStatus
        else:
            versionStatus = 'wip'
        sg_process.set_version_status(versionId, versionStatus)

        # set Task status if the version is latest version
        task = version.get('sg_task')
        entity = version.get('entity')
        taskname = task.get('name')
        if isLatest:
            
            taskId = task.get('id')
            sg_process.set_task_status(taskId, newStatus)

            # get task dependency 
            taskEntity = sg_process.get_task_from_id(taskId)
            description = version['description'] if version['description'] else ''
            user = user_info.User()
            userEntity =  user.sg_user()
            taskResult = []
            if taskEntity['step']['name'] == 'Design':
                if newStatus == 'rev':
                    taskResult = [taskEntity]
                elif newStatus == 'apr' or newStatus == 'fix' : # newStatus == 'apr'
                    triggerStep = 'Model'
                    triggerTaskName = 'Model'
                    taskResult = [sg_process.get_one_task_by_step(taskEntity['entity'], triggerStep, triggerTaskName)]
                    taskResult.append(taskEntity)
            else: 
                project_entity = taskEntity['project']
                proj = project_info.ProjectInfo(project=project_entity['name'])
                tasks_trigger_table = proj.task.dependency('asset')
                if newStatus == 'rev':
                    taskResult = [taskEntity]
                elif newStatus == 'apr' or newStatus == 'fix' : # newStatus == 'apr'
                    taskResult = status_update.update_dependency_event_api(tasks_trigger_table, taskEntity)
                    taskResult.append(taskEntity)
                    taskname = taskEntity.get('content')
                    if taskname == 'rigUv' or taskname == 'rig':
                        anim = {'step':{'name':'anim'}, 'task_assignees':[{'name':'-'}], 'project':taskEntity['project'], 'sg_status_list':'def' }
                        taskResult.append(anim)

            # get emails of those whoe's involved
            emails = noti_module_cliq.get_notification_data(taskResult, taskEntity, description, userEntity)

            imgurl = taskEntity['image']
            if not imgurl:
                imgurl = 'https://previewengine-accl.zoho.com/image/WD/54mxc9836fa8a51d545eab56f79f735e2029c?width=2046&height=1536'
            urlImage = [imgurl]

            # send notification
            noti_module_cliq.send_notification(emails, taskEntity, urlImage=urlImage)

        QtWidgets.QApplication.restoreOverrideCursor()
        # refresh UI
        self.thread_update_data(entity, taskname, version_index)

    def update_task_version(self, item, index):
        itemData = item.data(QtCore.Qt.UserRole)
        itemData['current'] = index  # update the current index
        item.setData(QtCore.Qt.UserRole, itemData)

        index, versions = self.get_item_versions(item=item)
        selected_version = versions[index]
        taskname = selected_version['sg_task'].get('name')
        entity = selected_version['entity']

        # update item
        self.update_task_item(item, taskname, entity, selected_version, itemData)

        # update info, description
        self.task_selected()
        # update view
        self.update_view(force=True)

    def update_view(self, force=False):
        items = self.task_listwidget.selectedItems()
        selected_task_names = [self.task_listwidget.itemWidget(i).raw_text() for i in items]

        if not force and selected_task_names == self._previous_task_items_selected:
            return
        else:
            self._previous_task_items_selected = selected_task_names

        self.reset_view()
        if not items:
            return

        # ----- show still images
        version_data = collections.OrderedDict()
        for item in items:
            index, versions = self.get_item_versions(item=item)
            version = versions[index]
            assetname = version['entity'].get('name')
            taskname = version['sg_task'].get('name')
            if taskname and assetname:
                version_data['{} - {}'.format(assetname, taskname)] = version

        show_images = []
        show_vids = []
        if version_data:
            this_asset = self.entity['code']
            # extract all paths
            all_paths = []
            all_datas = []
            for name, data in version_data.iteritems():
                assetname, taskname = name.split(' - ')
                path = data['sg_path_to_movie'] if data['sg_path_to_movie'] else ''
                if '#' in path:
                    images = [im.replace('\\', '/') for im in glob(path.replace('#', '*'))]
                    if images:
                        all_paths.extend(images)
                        for im in xrange(len(images)):
                            version = data.copy()
                            all_datas.append({'taskname': taskname, 'assetname': assetname, 'version': version})
                else: # single image/vid
                    version = data.copy()
                    if not os.path.exists(path):
                        path = self.nopreivew_path
                        version = None
                    all_paths.append(path)
                    all_datas.append({'taskname': taskname, 'assetname': assetname, 'version': version})
            overlayBorder = self.image_viewer.borderWidth
            # datenow = datetime.now()

            for path, data in zip(all_paths, all_datas):
                version = data['version']
                
                if version:
                    assetname = data['assetname']
                    taskname = data['taskname']
                    basename = os.path.basename(path)
                    fn, ext = os.path.splitext(basename)

                    # actual file path attached to the item
                    version['filepath'] = path
                    # toolTip data
                    version['__toolTip'] = fn

                    # text under image/vid
                    bottom_text = taskname if assetname==this_asset else '{} - {}'.format(assetname, taskname)
                    # found still images
                    if ext in image_widget.IMAGE_EXT:
                        # frame border data
                        if version['sg_task.Task.sg_status_list'] == 'apr':
                            color = self.approved_border_color
                        else:
                            color = self.inprogress_border_color
                        version['__border_color'] = color

                        # set texts data
                        version['__text'] = []
                        version['__pixmap'] = []
                        version['__text'].append((bottom_text, 'bottomCenter', overlayBorder, self.taskname_font, self.taskname_color))

                        # data to be sent to viewer
                        show_images.append((path, version))

                    # found videos
                    elif ext in media_widget.VIDEO_EXT:
                        show_vids.append((path, bottom_text, version))
                
        # show all images
        self.image_viewer.showImages(images=show_images)
        # show all videos
        # show_vids += [("P:/Hanuman/scene/publ/act1/hnm_act1_q0050f_s1750/techanim/main/v009/outputMedia/hnm_act1_q0050f_s1750_techanim_main.v009.mov", 'turnaround', None),
        #             ("R:/Hanuman/scene/publ/act1/hnm_act1_q0010a_s0060/comp/main/v004/outputMedia/hnm_act1_q0010a_s0060_comp_main.v004_preview.mov", 'render', None)]
        self.vid_viewer.add_items(media_info=show_vids)
        self.vid_viewer.stretch_items(mode='horizontal')

    def reset_view(self):
        self.image_viewer.clear()
        self.vid_viewer.clear()

    def set_multiple_info(self):
        multiple_value_str = '< multiple values >'
        self.creator_value_label.setStyleSheet('color: grey')
        self.creator_value_label.setFont(self.display_font)
        self.creator_value_label.setText(multiple_value_str)

        self.date_value_label.setStyleSheet('color: grey')
        self.date_value_label.setFont(self.display_font)
        self.date_value_label.setText(multiple_value_str)
        
        self.startdue_value_label.setStyleSheet('color: grey')
        self.startdue_value_label.setFont(self.display_font)
        self.startdue_value_label.setText(multiple_value_str)

        self.description_textEdit.setStyleSheet('color: grey')
        self.description_textEdit.setPlainText(multiple_value_str)

    def update_info(self, data):
        # update creator
        user = data.get('user')
        if user:
            user = user.get('name')
        else:
            user = 'N/A'
        self.creator_value_label.setFont(self.active_font)
        self.creator_value_label.setStyleSheet('color: white')
        self.creator_value_label.setText(user)

        # update date
        date = data.get('updated_at') if data.get('updated_at') else data.get('created_at')
        if date:
            date = str(datetime.strftime(date, '%y/%m/%d-%H:%M'))
        else:
            date = 'N/A'
        self.date_value_label.setFont(self.active_font)
        self.date_value_label.setStyleSheet('color: white')
        self.date_value_label.setText(date)

        # start/due date
        has_date = False
        start_date = 'N/A'

        if data.get('sg_task.Task.start_date'):
            start_date = datetime.strftime(datetime.strptime(data.get('sg_task.Task.start_date'), '%Y-%m-%d'), '%y/%m/%d')
            has_date = True
        due_date = 'N/A'
        if data.get('sg_task.Task.due_date'):
            due_date = datetime.strftime(datetime.strptime(data.get('sg_task.Task.due_date'), '%Y-%m-%d'), '%y/%m/%d')
            has_date = True
        if has_date:
            self.startdue_value_label.setFont(self.active_font)
            self.startdue_value_label.setStyleSheet('color: white')
            self.startdue_value_label.setText('{} - {}'.format(start_date, due_date))

        # update description
        if 'description' in data and data['description']:
            description = data['description']
            self.description_textEdit.setStyleSheet('color: white')
            self.description_textEdit.setPlainText(description)

    def reset_info(self):
        self.description_textEdit.clear()

        self.creator_value_label.setFont(self.display_font)
        self.creator_value_label.setStyleSheet('color: grey')
        self.creator_value_label.setText('N/A')

        self.date_value_label.setFont(self.display_font)
        self.date_value_label.setStyleSheet('color: grey')
        self.date_value_label.setText('N/A')

        self.startdue_value_label.setFont(self.display_font)
        self.startdue_value_label.setStyleSheet('color: grey')
        self.startdue_value_label.setText('N/A')

    def disable_description_edit(self, force=True):
        # set toggle off
        self.edit_description_button.blockSignals(True) if force else None
        self.edit_description_button.setChecked(False)
        self.edit_description_button.blockSignals(False) if force else None

        # set non editable
        self.description_textEdit.setTextInteractionFlags(QtCore.Qt.NoTextInteraction)
        # set look
        self.description_textEdit.setFont(self.display_font)
        self.description_textEdit.setPalette(self.grey_font_palette)
        self.description_textEdit.setStyleSheet('background-color: rgb{};'.format(self.bg_col))

    def enable_description_edit(self):
        # set editable
        self.description_textEdit.setTextInteractionFlags(QtCore.Qt.TextEditorInteraction)
        # set look
        self.description_textEdit.setPalette(self.white_font_palette)
        self.description_textEdit.setFont(self.active_font)
        self.description_textEdit.setFocus()
        self.description_textEdit.moveCursor(QtGui.QTextCursor.End)
        self.description_textEdit.setStyleSheet('background-color: rgb{};'.format(self.edit_col))

    def get_item_versions(self, item):
        itemData = item.data(QtCore.Qt.UserRole)
        index = itemData['current']
        versions = itemData['versions']
        return index, versions 

    def edit_description_toggled(self):
        items = self.task_listwidget.selectedItems()
        if len(items) != 1:
            self.error_no_selection('Edit Description', 'Please select a Task!')
            self.disable_description_edit()
            return 
        item = items[0]

        index, versions = self.get_item_versions(item=item)
        selected_version = versions[index]
        # start editing
        if self.edit_description_button.isChecked():
            self.enable_description_edit()
        else: # finish editing
            current_text = self.description_textEdit.toPlainText()

            if current_text != selected_version['description']:
                # popup question to user
                qmsgBox = QtWidgets.QMessageBox(self)
                qmsgBox.setText('Edit description?')
                qmsgBox.setWindowTitle('Confirm')
                qmsgBox.setIcon(QtWidgets.QMessageBox.Question)
                qmsgBox.addButton('  Yes  ', QtWidgets.QMessageBox.AcceptRole)
                qmsgBox.addButton('  No  ', QtWidgets.QMessageBox.RejectRole)
                answer = qmsgBox.exec_()
                if answer == 0: 
                    try:
                        # update shotgun data
                        update_description(taskid=selected_version.get('id'), description=current_text)
                        
                        # also update local data stored inside listwidget item
                        selected_version['description'] = current_text
                        itemData = item.data(QtCore.Qt.UserRole)
                        itemData['versions'][index] = selected_version
                        item.setData(QtCore.Qt.UserRole, itemData)
                    except Exception as e:
                        logger.error(e)
                        self.description_textEdit.setPlainText(selected_version['description'])
                else:
                    # set text back to previous text
                    self.description_textEdit.setPlainText(selected_version['description'])
            # disable edit UI
            self.disable_description_edit()

    def launch_viewer_app(self, paths):
        mods = QtWidgets.QApplication.keyboardModifiers()
        if not self.chosen_app or mods & QtCore.Qt.ControlModifier:
            # pop up user to choose application to play media
            qmsgBox = QtWidgets.QMessageBox(self)
            # set tile, icon
            qmsgBox.setWindowTitle('View Media')
            qmsgBox.setIcon(QtWidgets.QMessageBox.Question)

            # set text
            text = 'Open media with?'
            qmsgBox.setText(text)

            # set detailed text
            detailedText = 'Playlist: \n- '
            detailedText += '\n- '.join([os.path.basename(path) for path in paths])
            qmsgBox.setDetailedText(detailedText)

            # add buttons
            for app_name in self.player_app:
                qmsgBox.addButton(app_name, QtWidgets.QMessageBox.AcceptRole)
            cancel_button = qmsgBox.addButton('Cancel', QtWidgets.QMessageBox.RejectRole)
            qmsgBox.exec_()
            clicked_button = qmsgBox.clickedButton()
            if clicked_button != cancel_button:
                app_name = clicked_button.text()
                self.chosen_app = app_name
                func = self.player_app[app_name]
                func(paths)
        else:
            func = self.player_app[self.chosen_app]
            func(paths)
    
    def play_keyframepro(self, paths):
        app_path = config.Software.pythonwPath
        if not os.path.exists(app_path):
            print('Cannot find application: {}'.foramt(app_path))
            return
        script_path = '{}/rf_utils/keyframepro_utils.py'.format(config.Env.core)
        input_paths = ['-i']

        # check for image sequence pattern of keyframe Pro  name.####.ext
        prefixes = set()
        for path in paths:
            basename = os.path.basename(path)
            fn, ext = os.path.splitext(basename)
            m = re.match(r'(.+)\.([0-9]+)$', fn)
            if m:
                if m.group(1) not in prefixes:
                    prefixes.add(m.group(1))
                    input_paths.append(path)
            else:
                input_paths.append(path)
        commands = [app_path, script_path] + input_paths
        kfp = subprocess.Popen(commands, 
                    stdout=subprocess.PIPE)
        # outputs = kfp.communicate()
        # for stdout_line in iter(kfp.stdout.readline, ""):
        #     if stdout_line.startswith('PORT='):
        #         kfp_port = int(stdout_line.split('=')[1])
        #         break

    def play_rv(self, paths):
        app_path = config.Software.rv
        if not os.path.exists(app_path):
            print('Cannot find application: {}'.foramt(app_path))
            return
        commands = [app_path] + paths
        subprocess.Popen(commands)

    def error_no_selection(self, title, message):
        # pop up user to choose application to play media
        qmsgBox = QtWidgets.QMessageBox(self)
        # set tile, icon
        qmsgBox.setWindowTitle(title)
        qmsgBox.setText(message)
        qmsgBox.setIcon(QtWidgets.QMessageBox.Critical)

        # add button
        qmsgBox.addButton('  OK  ', QtWidgets.QMessageBox.AcceptRole)
        qmsgBox.exec_()

    def view_media(self):
        currentStillItems = self.image_viewer.scene().selectedItems()
        currentVidItems = self.vid_viewer.selectedItems()
        currentItems = currentStillItems + currentVidItems
        if not currentItems:
            self.error_no_selection('View Media', 'Please select at least 1 image!')
            return
        media_paths = []
        for item in currentItems:
            data = item.data(QtCore.Qt.UserRole)
            if isinstance(data, dict) and 'filepath' in data and os.path.exists(data['filepath']):
                media_paths.append(data['filepath'])
        if media_paths:
            self.launch_viewer_app(paths=media_paths)

    def open_media_dir(self):
        currentStillItems = self.image_viewer.scene().selectedItems()
        currentVidItems = self.vid_viewer.selectedItems()
        currentItems = currentStillItems + currentVidItems
        if not currentItems:
            self.error_no_selection('Open Directory', 'Please select at least 1 image!')
            return
        open_dirs = set()
        for item in currentItems:
            data = item.data(QtCore.Qt.UserRole)
            if isinstance(data, dict) and 'filepath' in data:
                dirname = os.path.dirname(data['filepath'])
                if os.path.exists(dirname) and dirname not in open_dirs:
                    open_dirs.add(dirname)
                    subprocess.Popen(r'explorer {}'.format(dirname.replace('/', '\\')))

def update_description(taskid, description): 
    """ this function update description from task id 
    get task id from shot[self.taskname]['id']
    """ 
    data = {'description': description}
    return sg_process.sg.update('Version', taskid, data)
