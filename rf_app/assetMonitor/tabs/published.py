#!/usr/bin/env python
# -- coding: utf-8 --

import os
import sys
from collections import OrderedDict
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from Qt import QtCore
from Qt import QtGui
from Qt import QtWidgets

from rf_utils import thread_pool
from rf_utils.sg import sg_process
from . import master
# reload(master)

isNuke = True
try:
    import nuke
    import nukescripts
    from rf_nuke.utils import shared_comp
    reload(shared_comp)
except ImportError:
    isNuke = False

modulePath = sys.modules[__name__].__file__.replace('\\', '/')
moduleDir  = os.path.dirname(modulePath)
appModuleDir = os.path.dirname(moduleDir)

class Ui(master.Ui):
    """docstring for Ui"""
    def __init__(self, thread=None, parent=None):
        super(Ui, self).__init__(thread=thread, parent=parent)
        self.step = ['design', 'keyvis', 'model', 'texture', 'rig', 'groom', 'lookdev']
        self._include_related = ['design']
    
    def version_to_tasks(self, versions, entity):
        # store in dict
        tasks = OrderedDict()  # {'task':[v1, v2, v3]}
        step_codes = []
        for version in versions:
            taskname = version['sg_task'].get('name', None)
            
            version_entity = version['entity']
            if version_entity['id'] != entity['id']:
                entityname = version_entity['name']
                taskname = '{} - {}'.format(taskname, entityname)
            if taskname and taskname not in tasks:
                tasks[taskname] = []
                step_codes.append((version['sg_task.Task.step.Step.code'].lower(), taskname))
            tasks[taskname].append(version)
        return tasks, step_codes

    def fetch_data(self, entity): 
        # fetch from SG
        fields = [
            'code', 'description', 'sg_status_list', 'sg_path_to_movie', 'sg_task.Task.step.Step.code',
            'entity', 'user', 'sg_task', 'created_at', 'sg_task.Task.sg_status_list',
            'sg_task.Task.start_date', 'sg_task.Task.due_date']

        filters = [['entity', 'is', entity]]
        advancedFilter = {
                            "filter_operator": "any",
                            "filters": [['sg_task.Task.step.Step.code', 'is', s] for s in self.step]
                        }
        filters.append(advancedFilter)
        versions = sg_process.sg.find('Version', filters, fields)

        tasks, step_codes = self.version_to_tasks(versions, entity)
        # sort task name by department
        sort_index = {k:v for v, k in enumerate(self.step)}
        step_codes.sort(key=lambda i: sort_index.get(i[0]))
        result_tasks = OrderedDict()
        for sc, taskname in step_codes:
            result_tasks[taskname] = tasks[taskname]

        # -------------------------------------
        # get related
        extra_entity = []
        if self._include_related:
            extra_entity = self.get_related(entity)
        if extra_entity:
            advancedFilter = {
                            "filter_operator": "any",
                            "filters": [['sg_task.Task.step.Step.code', 'is', s] for s in self._include_related]
                            }
            extra_filters = [advancedFilter]
            advancedFilter = {
                                "filter_operator": "any",
                                "filters": [['entity', 'is', e] for e in extra_entity]
                            }
            extra_filters.append(advancedFilter)
            related_versions = sg_process.sg.find('Version', extra_filters, fields)
            related_tasks, related_step_codes = self.version_to_tasks(related_versions, entity)
            related_tasks = OrderedDict([(k, related_tasks[k]) for k in sorted(related_tasks.keys())])
            for taskname, related_versions in related_tasks.iteritems():
                entityname = related_versions[-1]['entity'].get('name')
                taskname = '{} - {}'.format(taskname, entityname)
                result_tasks[taskname] = related_versions

        return result_tasks



