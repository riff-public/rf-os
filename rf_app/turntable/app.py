_title = 'Riff TurnTable'
_version = 'v.0.0.1'
_des = 'beta'
uiName = 'RFTurnTable'

#Import python modules
import sys
import os
import getpass
import json
import logging

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# import config
import rf_config as config
from rf_utils import log_utils
# sg

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import ui

import maya.cmds as mc

class RFTurnTable(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #SetUp Window
        super(RFTurnTable, self).__init__(parent)
        #ui read
        self.ui = ui.TurntableUI(parent)
        self.setObjectName(uiName)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.setCentralWidget(self.ui)
        self.init_state()

        # self.setFixedSize(50,195)
        # self.set_logo()

    def init_state(self):
        self.ui.viewport_set_pushButton.clicked.connect(self.set_viewport)
        self.ui.horizontalSlider.valueChanged.connect(self.trigger_distance)
        self.ui.distance_lineEdit.textChanged.connect(self.distance)
        self.ui.horizontalSlider_2.valueChanged.connect(self.trigger_crane)
        self.ui.crane_lineEdit.textChanged.connect(self.crane)
        self.ui.tilt_horizontalSlider.valueChanged.connect(self.trigger_tilt)
        self.ui.tilt_lineEdit.textChanged.connect(self.tilt)
        self.ui.set_turn_pushButton.clicked.connect(self.set_turn_table)
        self.ui.setup_tt_pushButton.clicked.connect(self.Import_turn_table)

    def set_viewport(self):
        from pymel.core import *
        for elem  in  getPanel(typ="modelPanel") :
            mel.eval( 'setRendererInModelPanel  "ogsRenderer" '+ elem.name() )

    def distance(self):
        mc.setAttr("TT_cam.translateZ", int(self.ui.horizontalSlider.value()))

    def trigger_distance(self):
        self.ui.distance_lineEdit.setText(str(self.ui.horizontalSlider.value()))

    def crane(self):
        mc.setAttr("TT_cam.rotateX", int(self.ui.horizontalSlider_2.value()-12))

    def trigger_crane(self):
        self.ui.crane_lineEdit.setText(str(self.ui.horizontalSlider_2.value()-12))

    def tilt(self):
        mc.setAttr("TT_cam.translateY", int(self.ui.tilt_horizontalSlider.value()))

    def trigger_tilt(self):
        self.ui.tilt_lineEdit.setText(str(self.ui.tilt_horizontalSlider.value()))

    def set_turn_table(self):
        duration = int(self.ui.duration_lineEdit.text())
        mc.currentTime( 0, edit=True )
        mc.setAttr('TT_Grp.rotateY', 0 )
        mc.setKeyframe( 'TT_Grp', attribute='rotateY')
        
        mc.currentTime( duration, edit=True )
        mc.setAttr('TT_Grp.rotateY', 360)
        mc.setKeyframe( 'TT_Grp', attribute='rotateY')

    def Import_turn_table(self):
        mc.file('P:/SevenChickMovie/asset/publ/_global/tt/turntable_setup.ma', i =True, type='mayaAscii')


def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = RFTurnTable(maya_win.getMayaWindow())
        myApp.show()
        return myApp
if __name__ == '__main__':
    show()