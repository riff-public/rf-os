# utils for tool 
import os 
import sys 
import json 

# from shiboken2 import wrapInstance

try: 
    import maya.cmds as mc 
    import maya.OpenMayaUI as mui 
except ImportError: 
    pass 

def save_increment(): 
    currentScene = mc.file(q=True, sn=True) or mc.file(q=True, location=True)
    # filename = increment_file(currentScene)
    filename = pipeline_increment(currentScene)
    name, ext = os.path.splitext(currentScene)

    format = 'mayaAscii' if ext == '.ma' else 'mayaBinary' if ext == '.mb' else ''
    
    if format: 
        return save_file(filename, format)

def save_file_farm(nameUser='.submitFarm'): 
    currentScene = mc.file(q=True, sn=True) or mc.file(q=True, location=True)
    filename = currentScene
    name, ext = os.path.splitext(currentScene)
    name, user = os.path.splitext(name)
    user = nameUser
    filename = name+user+ext

    format = 'mayaAscii' if ext == '.ma' else 'mayaBinary' if ext == '.mb' else ''
    
    if format: 
        return save_file(filename, format)
        

def increment_file(path): 
    dirname = os.path.dirname(path)
    newVersion = calculate_version(list_file(dirname))
    currentVersion = search_for_version(path)
    basename = os.path.basename(path)
    newName = basename.replace(currentVersion, newVersion)
    incrementFileName = '%s/%s' % (dirname, newName)
    return incrementFileName

def pipeline_increment(path): 
    from rf_utils.context import context_info

    # new version 
    dirname = os.path.dirname(path)
    newVersion = calculate_version(list_file(dirname))
    # user 
    user = os.environ.get('RFUSER')

    # hard code detect project 
    # Nu edit 17/1/20, added MI, Quantum, Ream, ThreeEyes
    entity = context_info.ContextPathInfo()
    projects = entity.projectInfo.list_all(False)

    if entity.project in projects: 
        entity.context.update(version=newVersion, user=user)
        workspacePath = entity.path.workspace().abs_path()
        workfile = entity.work_name(user=True)
        incrementFile = '%s/%s' % (dirname, workfile)
        return incrementFile

    else: 
        return increment_file(path)


def pipeline_increment2(entity, path): 
    from rf_utils.context import context_info

    # new version 
    dirname = os.path.dirname(path)
    newVersion = calculate_version(list_file(dirname))
    # user 
    user = os.environ.get('RFUSER')

    # hard code detect project 
    projects = ['SevenChickMovie', 'Elex2', 'projectName']

    if entity.project in projects: 
        entity.context.update(version=newVersion, user=user)
        workspacePath = entity.path.workspace().abs_path()
        workfile = entity.work_name(user=True)
        incrementFile = '%s/%s' % (dirname, workfile)
        return incrementFile

    else: 
        return increment_file(path)


def list_file(path): 
    return [d for d in os.listdir(path) if os.path.isfile(os.path.join(path, d))]   

def list_folder(path): 
    return [d for d in os.listdir(path) if os.path.isdir(os.path.join(path,d))]  

def open_file(path): 
    return mc.file(path, o=True, f=True)

def save_file(path, format): 
    mc.file(rename=path)
    return mc.file(save=True, type=format)

def is_scene_modify(): 
    return mc.file(q=True, modified=True)


def search_for_version(filename, prefix='v', padding=3): 
    versionNumber = [a[0: padding] for a in filename.split(prefix) if a[0: padding].isdigit()]
    return '%s%s' % (prefix, versionNumber[0]) if versionNumber else ''

def increment_version(filename, prefix='v', padding=3): 
    version = search_for_version(filename, prefix=prefix, padding=padding)
    if version: 
        num = version.split(prefix)[-1]
        if num.isdigit(): 
            nextVersion = int(num) + 1
            newVersion = get_version(prefix, padding, nextVersion)
            return newVersion

    else: 
        return get_version(prefix, padding, 1)

def get_version(prefix, padding, version): 
    cmd = '"%0' + str(padding) + 'd" % version' 
    newVersion = '%s%s' % (prefix, eval(cmd))
    return newVersion


def calculate_version(files, prefix='v', padding=3): 
    allVersions = sorted([search_for_version(a).split(prefix)[-1] for a in files if search_for_version(a)])
    intVersions = [int(a) for a in allVersions if a.isdigit()]
    if intVersions: 
        nextVersion = max(intVersions) + 1
        return get_version(prefix=prefix, padding=padding, version=nextVersion)
    else: 
        return get_version(prefix=prefix, padding=padding, version=1)


# def publish_data_scene_eva():
#     from rf_utils.context import scene_description
#     reload(scene_description)

#     file_path = mc.file(loc=True, q=True)

#     scene_eva = scene_description.SceneDescription(maya_path =file_path )
#     if scene_eva._scene.project == 'Eva':
#         dict_data = scene_eva.data
#         data_scene_step = scene_eva.collect_data(step=True)

#         if not scene_eva._scene.name in dict_data:
#             dict_data[scene_eva._scene.name] = data_scene_step
#         else:
#             for key, value in data_scene_step.iteritems():
#                 dict_data[scene_eva._scene.name][key] = value
#         scene_eva.write_data(dictData= dict_data)
