import os
import sys
from collections import OrderedDict
import shutil
from datetime import datetime
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_utils.context import context_info
from rf_utils import custom_exception
from rf_utils import file_utils


def file_time(filePath):
    mtime = 0.00
    sequenceInfo = file_utils.get_sequence_info(filePath)
    if sequenceInfo: 
        filePath = sequenceInfo['sequences'][0]

    if os.path.exists(filePath):
        mtime = os.path.getmtime(filePath)

    return mtime

def is_file_new(startFileTime, endFileTime):
    logger.debug('endFileTime, startFileTime')
    logger.debug('%s %s' % (endFileTime, startFileTime))
    if endFileTime > startFileTime:
        return True

    else:
        return False

def export_output(publishInfo, outputList, func, *args):
    exportGrp = publishInfo.entity.projectInfo.asset.geo_grp()
    # assume first args is outputPath
    masterOutput = args[0]
    # create dir
    os.makedirs(os.path.dirname(masterOutput)) if not os.path.exists(os.path.dirname(masterOutput)) else None

    startTime = file_time(masterOutput)
    masterOutput = func(*args)

    if masterOutput == context_info.Status.skip:
        return context_info.Status.skip

    masterResult = is_file_new(startTime, file_time(masterOutput))
    results = [masterResult]

    if masterResult:
        for outputFile in outputList:
            startTime = file_time(outputFile)

            if not os.path.exists(os.path.dirname(outputFile)):
                os.makedirs(os.path.dirname(outputFile))

            if not masterOutput == outputFile:
                file_utils.copy2(masterOutput, outputFile)
                # shutil.copy2(masterOutput, outputFile)
                logger.info('Copy output success %s' % outputFile)

                endTime = file_time(outputFile)
                result = is_file_new(startTime, endTime)
                results.append(result)

        return all(results)
    else:
        message = 'No output from "%s" with args (%s). Possible error in mayapy mode. Please check log for more details.' % (func.__name__, args)
        raise custom_exception.PipelineError(message)


def publish_output_list(publishInfo, hero=True, outputKey=None):
    outputList = []
    outputDict = OrderedDict()
    outputPath = publishInfo.publish_file(outputKey=outputKey)

    publishFile = context_info.RootPath(outputPath).abs_path()
    outputList.append(publishFile)
    outputDict['publishedFile'] = publishFile

    # Result: u'$RFPUBL/ProjectName/asset/publish/character/main/sunnyBase/model/main/v001/output/sunnyBase_mdl_main_md.v001.ma' #
    # publishInfo.entity.context.update(app='gpu')
    publishHeroPath = publishInfo.publish_file(outputKey=outputKey, hero=True)
    publishHeroFile = context_info.RootPath(publishHeroPath).abs_path()
    outputList.append(publishHeroFile)
    outputDict['publishedHeroFile'] = publishHeroFile

    # Result: u'$RFPUBL/ProjectName/asset/publish/character/main/sunnyBase/model/main/hero/sunnyBase_mdl_main_md.hero.ma' #
    # publishInfo.entity.context.update(app='gpu')
    if hero:
        hero = publishInfo.hero_file(outputKey=outputKey)
        heroFile = context_info.RootPath(hero).abs_path()
        outputList.append(heroFile)
        outputDict['heroFile'] = heroFile

    return outputList, outputDict


def publish_workspace(publishInfo, hero=True):
    workspaceList = []
    workspaceDict = OrderedDict()
    workspaceInfo = publishInfo.publish_workspace()
    publishWorkspace = context_info.RootPath(workspaceInfo).abs_path()
    workspaceList.append(publishWorkspace)
    workspaceDict['publishedFile'] = publishWorkspace

    if hero:
        workspaceHeroInfo = publishInfo.publish_hero_workspace()
        publishWorkspaceHero = context_info.RootPath(workspaceHeroInfo).abs_path()
        workspaceList.append(publishWorkspaceHero)
        workspaceDict['publishedHeroFile'] = publishWorkspaceHero

    return workspaceList, workspaceDict

def parent_group(entity, res, check=True):
    import maya.cmds as mc
    geoGrp = '%s' % (entity.projectInfo.asset.geo_grp()) if entity else 'Geo_Grp'
    mdGeoGrp = '%s' % (entity.projectInfo.asset.md_geo_grp()) if entity else 'MdGeo_Grp'
    prGeoGrp = '%s' % (entity.projectInfo.asset.pr_geo_grp()) if entity else 'PrGeo_Grp'
    list_child = mc.listRelatives(geoGrp, c=True)
    if check:
        if res == 'pr' and not mdGeoGrp in list_child:
            if mc.objExists(mdGeoGrp):
                mc.parent(mdGeoGrp , geoGrp)
        elif res == 'md' and not prGeoGrp in list_child:
            if mc.objExists(prGeoGrp):
                mc.parent(prGeoGrp , geoGrp)
    else:
        if res == 'pr' and mdGeoGrp in list_child:
            if mc.objExists(mdGeoGrp):
                mc.parent(mdGeoGrp , w=True)
        elif res == 'md' and prGeoGrp in list_child:
            if mc.objExists(prGeoGrp):
                mc.parent(prGeoGrp , w=True)

def get_geo_grp(entity, res): 
    geoGrp = '%s' % (entity.projectInfo.asset.geo_grp()) if entity else 'Geo_Grp'
    mdGeoGrp = '%s' % (entity.projectInfo.asset.md_geo_grp()) if entity else 'MdGeo_Grp'
    prGeoGrp = '%s' % (entity.projectInfo.asset.pr_geo_grp()) if entity else 'PrGeo_Grp'
    loGeoGrp = '%s' % (entity.projectInfo.asset.lo_geo_grp()) if entity else 'LoGeo_Grp'
    pvGeoGrp = '%s' % (entity.projectInfo.asset.pv_geo_grp()) if entity else 'PvGeo_Grp'

    if res == 'pr': 
        return prGeoGrp
    if res == 'md': 
        return mdGeoGrp
    if res == 'lo': 
        return loGeoGrp
    if res == 'pv': 
        return pvGeoGrp
    else: 
        return geoGrp

def maya_utils(): 
    from rftool.utils import maya_utils
    return maya_utils

