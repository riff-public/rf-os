import os
import sys

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

core = '%s/core' % (os.environ.get('RFSCRIPT'))
if not core in sys.path: 
    sys.path.append(core)
import rf_config as config
import maya.standalone
maya.standalone.initialize( name='python' )
import maya.cmds as mc
from rf_maya import lib


def main() :
    # init maya standalone
    # set arguments

    src = sys.argv[1]
    dst = sys.argv[2]
    processes = sys.argv[3]
    output = sys.argv[4]
    export = sys.argv[5]
    print 'processes\n\n', processes

    if not os.path.exists(os.path.dirname(dst)):
        os.makedirs(os.path.dirname(dst))

    # open scene
    print 'open scene'
    mc.file(src, o=True, f=True, prompt=False)
    print 'open suceess'
    # run process
    for process in eval(processes):
        lib.run(process)

    name, ext = os.path.splitext(dst)
    type = 'mayaAscii' if ext == '.ma' else 'mayaBinary' if ext == '.mb' else 'mayaAscii'

    if output == 'save':
        mc.file(rename=dst)
        mc.file(save=True, f=True, type=type)
    if output == 'export':
        mc.select(export)
        mc.file(dst, force=True, options='v=0', typ=type, pr=True, es=True)

if __name__ == '__main__':
    main()