import sys, os
import subprocess
import maya.cmds as mc

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
version = mc.about(v = True)

if version in ['2015', '2016', '2017']:
	MAYA_PYTHON = 'C:/Program Files/Autodesk/Maya%s/bin/mayabatch.exe' % version
	MAYA_PYTHON = 'C:\\Program Files\\Autodesk\\Maya%s\\bin\\mayapy.exe' % version

MAYACMD = '%s/maya_batch_process.py' % moduleDir

def run(src, dst, processList, output='save', export=''):
	print 'Running subprocess'
	# subprocess.call([MAYA_PYTHON, MAYACMD, src, dst, processList, output])
	# return
	p = subprocess.Popen([MAYA_PYTHON, MAYACMD, src, dst, processList, output, export], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
	stdout, stderr = p.communicate()
	print 'Finish subprocess\n\n'

	logger.info(stdout)
	# logger.error(stderr)
	if os.path.exists(dst):
		return dst