import sys, os
import subprocess
import maya.cmds as mc

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_maya import lib
from rf_maya.lib import maya_file
reload(maya_file)

def run(src, dst, processes, output, export) :
    # run in current session
    if not os.path.exists(os.path.dirname(dst)):
        os.makedirs(os.path.dirname(dst))
    print 'output',output
    print 'processes', processes
    print 'export', export
    # run process
    for process in processes:
        lib.run(process)

    name, ext = os.path.splitext(dst)
    fileType = 'mayaAscii' if ext == '.ma' else 'mayaBinary' if ext == '.mb' else 'mayaAscii'

    if output == 'save':
        mc.file(rename=dst)
        result = mc.file(save=True, f=True, type=fileType)
        mc.file(src, o=True, f=True, prompt=False)
    if output == 'export':
        exportGrp = export
        if not type(export) == type(list()):
            exportGrp = [export]
        if type(export) == type(list()):
            exportGrp = export
        result = maya_file.export_selection(dst, exportGrp, fileType)

    if output == 'exportNoShade':
        exportGrp = export
        if not type(export) == type(list()):
            exportGrp = [export]
        if type(export) == type(list()):
            exportGrp = export
        result = maya_file.export_selection(dst, exportGrp, fileType, exportShade=False)

    if output == 'exportMiarmy':
        exportGrp = export
        print exportGrp, '1'
        if not type(export) == type(list()):
            miarmy_agent = mc.listRelatives(export, type='McdAgentGroup')
            if miarmy_agent:
                exportGrp = miarmy_agent
        if type(export) == type(list()):
            miarmy_agent = mc.listRelatives(export, type='McdAgentGroup')
            if miarmy_agent:
                exportGrp = miarmy_agent
                print exportGrp
        print exportGrp, '2'
        result = maya_file.export_selection(dst, exportGrp, fileType)
        # result = mc.file(dst, force=True, options='v=0', typ=type, pr=True, es=True)
    return result