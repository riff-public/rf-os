import sys
import os 
import argparse
import subprocess

import pymel.core as pm 
import maya.cmds as mc 
from rf_utils import file_utils
from rf_maya.rftool.utils import maya_utils

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


def setup_parser(title):
    # create the parser
    parser = argparse.ArgumentParser(description=title, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-s', dest='src', type=str, help='Source path', required=True)
    parser.add_argument('-d', dest='dst', type=str, help='Output path', required=True)
    parser.add_argument('-o', dest='option', type=str, help='export options "save", "export"')
    parser.add_argument('-e', dest='exportGrp', type=str, help='export group name')
    return parser



def main(src, dst, option='save', exportGrp=''): 
    # open scene 
    print 'option', option, exportGrp
    try: 
        mc.file(src, o=True, f=True, prompt=False)
    except Exception as e: 
        logger.error('Some error during file open. this fall into exception. Details below')
        logger.error(e)

    name, ext = os.path.splitext(dst)
    fileType = 'mayaAscii' if ext == '.ma' else 'mayaBinary' if ext == '.mb' else 'mayaAscii'
    result = ''

    if option == 'save':
        result = pm.saveAs(dst, f=True)
        # mc.file(rename=dst)
        # result = mc.file(save=True, f=True, type=fileType)

    if option == 'export': 
        if mc.objExists(exportGrp): 
            mc.select(exportGrp)
            result = mc.file(dst, force=True, options='v=0', typ=fileType, pr=True, es=True)

def run_cmd(src, dst, option='save', exportGrp='', env=None): 
    mayapy = maya_utils.pyinterpreter()
    py = sys.modules[__name__].__file__
    fileCheck = file_utils.ExportCheck(dst)

    with fileCheck: 
        si = subprocess.STARTUPINFO()
        si.dwFlags |= subprocess.STARTF_USESHOWWINDOW
        p = subprocess.Popen(['%s' % mayapy, '%s' % py, '-s', '%s' % src, '-d', '%s' % dst, '-o', '%s' % option, '-e', '%s' % exportGrp], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, startupinfo=si, env=env)
        stdout, stderr = p.communicate()
        if stderr: 
            logger.debug(stdout)
            logger.error(stderr)

    return dst if fileCheck.result else fileCheck.result


if __name__ == "__main__":
    # print sys.argv
    parser = setup_parser('save params')
    params = parser.parse_args()    
    main(src=params.src, dst=params.dst, option=params.option, exportGrp=params.exportGrp)
    
"""
example 
# default cache 
mayapy.exe cache_cmd -s "P:/projectName/scene/work/ep01/pr_ep01_q0010_s0020/anim/main/maya/pr_ep01_q0010_s0020_anim_main.v005.TA.ma" 
# department cache 
mayapy.exe cache_cmd -s "P:/projectName/scene/work/ep01/pr_ep01_q0010_s0020/anim/main/maya/pr_ep01_q0010_s0020_anim_main.v005.TA.ma" -step "anim" 
# specific namespace cache 
mayapy.exe cache_cmd -s "P:/projectName/scene/work/ep01/pr_ep01_q0010_s0020/anim/main/maya/pr_ep01_q0010_s0020_anim_main.v005.TA.ma" -n "arthur_001" "canis_001" -m "cache_single" 
"""

"""
from rf_utils.deadline import cache_all
cmds = ["O:/Pipeline/core/rf_app/bot/cache_cmd.py", '-s', "P:/projectName/scene/work/ep01/pr_ep01_q0010_s0020/anim/main/maya/pr_ep01_q0010_s0020_anim_main.v005.TA.ma"]
cache_all.process_send_to_farm(cmds)
"""

# mayapy.exe cache_cmd -s "P:/projectName/scene/work/ep01/pr_ep01_q0040_s0010/anim/main/maya/pr_ep01_q0040_s0010_anim_main.v001.TA.ma" 