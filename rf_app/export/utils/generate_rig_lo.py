import sys
import os 
import argparse
import subprocess

import pymel.core as pm 
import maya.cmds as mc 

import rftool.rig
from rf_utils import file_utils
from rf_maya.rftool.utils import maya_utils
from rf_utils.pipeline import asset_tag
from rf_utils.context import context_info
# reload(context_info)
from utaTools.utapy import cleanUp
from rf_maya.rftool.asset import pullUV
from rf_maya.ncscript.util import cleanUpTools as clean
from utaTools.pkrig import rigTools
from rf_maya.nuTools.util import bshUtil
from utaTools.utapy import utaCore

reload(utaCore)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


def setup_parser(title):
    # create the parser
    parser = argparse.ArgumentParser(description=title, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-s', dest='src', type=str, help='Source path', required=True)
    parser.add_argument('-d', dest='dst', type=str, help='Output path', required=True)
    parser.add_argument('-g', dest='geoGrp', type=str, help='The Geo_Grp')
    return parser

    
def main(src, dst, geoGrp):
    ''' Open a scene, add tag, cleanup and save as .mb '''

    # open scene 
    try: 
        mc.file(src, o=True, f=True, prompt=False)
    except Exception as e: 
        logger.error('Some error during file open. this fall into exception. Details below')
        logger.error(e)

    entity = context_info.ContextPathInfo(path=src)

    # add tag
    check = mc.ls(geoGrp)[0]
    if check:
        project = entity.project
        assetType = entity.type
        assetName = entity.name
        assetId = entity.id
        step = entity.step
        process = entity.process
        publishVersion = entity.publishVersion
        publishPath = entity.path.publish_version()
        asset_tag.add_tag(check, project=project, assetType=assetType, assetName=assetName, assetId=assetId, step=step, process=process, publishVersion=publishVersion, publishPath=publishPath)
    
    # cleanup
    cleanUp.OnOffVisibilityObj()
    cleanUp.cleanShade_batch()  # needs to use cleanShade_batch() cuz hyperShade cmd cannot run in batch
    cleanUp.remove_all_unused_influences('MdGeo_Grp')
    clean.clear_all_lock_scale()
    cleanUp.clearDeleteGrp()
    cleanUp.clearLayer()
    cleanUp.clearUnusedAnim()
    cleanUp.clearUnknowNode()
    cleanUp.clearUnusedNode()
    cleanUp.clearVraySettings()
    cleanUp.clearCameraDefault()
    rigTools.scaleGimbalControl()
    cleanUp.clearInheritsTransform()
    bshUtil.turnOffBshEdits()
    utaCore.setDefaultControl(char = assetName)
    objSets = mc.ls('set_*')
    for each in objSets:
        mc.delete(each)
    cleanUp.checkParameterUV()
    maya_utils.remove_plugins()

    # pull UV
    pullUV.toRig(entity=entity)
    
    # save as the file to R:
    result = pm.saveAs(dst, f=True)


def run_cmd(src, dst, geoGrp, env=None): 
    mayapy = maya_utils.pyinterpreter()
    py = sys.modules[__name__].__file__
    fileCheck = file_utils.ExportCheck(dst)

    with fileCheck: 
        si = subprocess.STARTUPINFO()
        si.dwFlags |= subprocess.STARTF_USESHOWWINDOW
        mc.waitCursor(st=True)
        p = subprocess.Popen(['%s' % mayapy, '%s' % py, '-s', '%s' % src, '-d', '%s' % dst, '-g', '%s' % geoGrp], 
            stdout=subprocess.PIPE, 
            stderr=subprocess.STDOUT, 
            startupinfo=si,
            env=env)
        stdout, stderr = p.communicate()
        if stderr: 
            logger.debug(stdout)
            logger.error(stderr)
        mc.waitCursor(st=False)

    return dst if fileCheck.result else fileCheck.result

if __name__ == "__main__":
    # print sys.argv
    parser = setup_parser('save params')
    params = parser.parse_args()    
    main(src=params.src, dst=params.dst, geoGrp=params.geoGrp)
    
