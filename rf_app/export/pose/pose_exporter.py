# v.0.0.1 polytag switcher
_title = 'Riff Pose Export'
_version = 'v.0.0.1'
_des = 'beta'
uiName = 'PoseExportUi'

#Import python modules
import sys
import os
import getpass
from datetime import datetime
from functools import partial
from collections import OrderedDict

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import rf_config as config
from rf_utils.context import context_info
from rf_utils import file_utils
from rf_app.anim.animIO import core as animIO
from rf_utils.widget import dialog
reload(dialog)


class PoseExport(QtWidgets.QMainWindow):
    status = QtCore.Signal(str)
    def __init__(self, namespace, refPath, parent=None) :
        super(PoseExport, self).__init__(parent)

        # ui 
        self.ui = self.setup_ui()
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)

        self.refPath = refPath 
        self.namespace = namespace

        # processes 
        self.processes = ['main']

        # title
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.resize(300, 100)

        self.init_functions()
        self.init_signals()

    def setup_ui(self): 
        ui = QtWidgets.QWidget()
        layout = QtWidgets.QVBoxLayout()
        self.assetLabel = QtWidgets.QLabel('Asset')
        poseType = QtWidgets.QLabel('Pose type')

        poseLayout = QtWidgets.QHBoxLayout()
        self.poseComboBox = QtWidgets.QComboBox()
        self.addPushButton = QtWidgets.QPushButton('+')
        self.exportPushButton = QtWidgets.QPushButton('Export Pose')
        self.exportPushButton.setMinimumSize(QtCore.QSize(30, 30))
        
        poseLayout.addWidget(poseType)
        poseLayout.addWidget(self.poseComboBox)
        poseLayout.addWidget(self.addPushButton)

        poseLayout.setStretch(0, 0)
        poseLayout.setStretch(1, 4)
        poseLayout.setStretch(2, 1)

        layout.addWidget(self.assetLabel)
        layout.addLayout(poseLayout)
        layout.addWidget(self.exportPushButton)
        ui.setLayout(layout)
        return ui

    def init_functions(self): 
        self.setup_context()
        self.add_process()

    def init_signals(self): 
        self.exportPushButton.clicked.connect(self.export)
        self.addPushButton.clicked.connect(self.add)

    def setup_context(self): 
        self.asset = context_info.ContextPathInfo(path=self.refPath)
        if self.asset.valid: 
            self.asset.context.update(process='main', step='rig', app='maya')
            self.asset.extract_name(os.path.basename(self.refPath), outputKey='rig')
            self.asset.context.update(step='pose')
            self.assetLabel.setText(self.asset.name)

    def add_process(self): 
        processPath = os.path.split(self.asset.path.process().abs_path())[0]
        # create default 
        for process in self.processes: 
            defaultProcess = '%s/%s' % (processPath, process)
            if not os.path.exists(defaultProcess): 
                os.makedirs(defaultProcess)

        processes = file_utils.list_folder(processPath)
        self.poseComboBox.clear()
        self.poseComboBox.addItems(processes)

    def add(self): 
        """ add process """ 
        processPath = os.path.split(self.asset.path.process().abs_path())[0]
        result = dialog.InputDialog.show('Enter process', 'Pose name: ')
        newProcess = '%s/%s' % (processPath, result.field_value())

        if not os.path.exists(newProcess): 
            os.makedirs(newProcess)

        self.add_process()


    def export(self): 
        # setup context 
        # process selection 
        process = str(self.poseComboBox.currentText())
        self.asset.context.update(process=process)

        # do export 
        self.export_animIO()
        self.copy_workspace()
        dialog.MessageBox.success('Success', 'Export pose complete')


    def export_animIO(self): 
        # output 
        dst = self.asset.path.output_hero().abs_path()
        name = self.asset.publish_name(hero=True)
        dstPath = '%s/%s' % (dst, name)

        if not os.path.exists(dst): 
            os.makedirs(dst)
        # export command 
        animIO.export_pose(project=self.asset.project, namespace=self.namespace, path=dstPath)
        logger.info('Exported pose %s' % dstPath)


    def copy_workspace(self): 
        # export selection to workspace 
        workspace = self.asset.path.workspace().abs_path()
        os.makedirs(workspace) if not os.path.exists(workspace) else None 

        # version 
        count = len(file_utils.list_file(workspace) or [])
        version = 'v%03d' % (count + 1 )
        self.asset.context.update(version=version)
        
        # dst path 
        filename = self.asset.work_name()
        dstPath = '%s/%s' % (workspace, filename)
        
        if config.isMaya: 
            from rftool.utils import maya_utils
            reload(maya_utils)
            maya_utils.export_from_namespace(dstPath, self.namespace)
            logger.info('Exported workspace %s' % dstPath)


def show(namespace='', refPath=''):
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = PoseExport(namespace, refPath, maya_win.getMayaWindow())
        myApp.show()
