import os 
import sys 

def pose_dir(refPath): 
    """ find pose dir from current rig path 
        This is hard coded without config for now """ 
    step = 'pose'
    dirname = os.path.dirname(refPath)
    root, dir = os.path.split(dirname)
    poseDir = '%s/%s' % (root, step)
    return poseDir


def list_pose(poseDir): 
    exts = ['.ma']
    poses = []
    if os.path.exists(poseDir): 
        for root, dirs, files in os.walk(poseDir): 
            for file in files: 
                name, ext = os.path.splitext(file)
                if ext in exts: 
                    posePath = '%s/%s' % (root.replace('\\', '/'), file)
                    poses.append(posePath)
    return poses


def default_pose(entity): 
    asset = entity.copy()
    asset.context.update(step='pose', res='md', process='main', app='maya')
    path = asset.path.scheme(key='publishHeroOutputPath').abs_path()
    filename = asset.publish_name(hero=True)
    heroFile = '%s/%s' % (path, filename)

    return heroFile
