# v.0.0.1 polytag switcher
_title = 'Export Tool'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'ExportUI'

#Import python modules
import sys
import os 
import json 
import getpass

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# config file 
config_file = '%s/local_config.json' % moduleDir
with open(config_file) as configData: 
    localConfig = json.load(configData)

SCRIPT = localConfig['SCRIPT']
ROOT = os.environ.get(SCRIPT)
core = localConfig['CORE']

# import config
sys.path.append('%s/%s' % (ROOT, core))
import rf_config as config

# framework modules 
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet
from rf_qc.maya_lib

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name('export', user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui


class RFExportTool(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        #Setup Window
        super(RFExportTool, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        if config.isMaya: 
            self.ui = load.setup_ui_maya(uiFile, parent)
        else: 
            self.ui = load.setup_ui(uiFile, self)
        
        self.ui.show()
        self.ui.setWindowTitle('Riff Desktop')


def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = RFExportTool(maya_win.getMayaWindow())
        return myApp

    else: 
        logger.info('Run in RFExportTool\n')
        app = QtWidgets.QApplication.instance()
        if not app: 
            app = QtWidgets.QApplication(sys.argv)
        myApp = RFExportTool()
        stylesheet.set_default(app)
        sys.exit(app.exec_())

if __name__ == '__main__': 
    show()
