# template for export mod
import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import custom_exception
from rf_utils import register_entity
reload(publish_info)
reload(export_utils)


def run(publishInfo):
    """ export Alembic cache file from Geo_Grp. """
    app = publishInfo.entity.context.app
    process = publishInfo.entity.context.process
    # publishInfo.entity.context.update(app='gpu')

    # track time
    outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=False, outputKey='cache')
    publishInfo.entity.context.update(app=app) # restore

    exportGrp = publishInfo.entity.projectInfo.asset.geo_grp()
    res = publishInfo.entity.res
    contentGrp = export_utils.get_geo_grp(publishInfo.entity, res)
    outputFile = outputList[0]
    run = True if publishInfo.entity.step in ['model'] else False 
    
    with export_utils.maya_utils().IsolateGeo(exportGrp, contentGrp, run=run): 
        exportResult = export_utils.export_output(publishInfo, outputList, export_cache_still, outputFile, exportGrp)
    
    register_data(publishInfo.entity, outputDict)
    return [exportResult, outputList]


def export_cache_still(outputPath, exportGrp) :
    import maya.cmds as mc
    import maya.mel as mm
    from rf_maya.lib import maya_file
    reload(maya_file)

    pluginName = 'AbcExport.mll'

    if not mc.pluginInfo(pluginName, q=True, l=True):
        mc.loadPlugin(pluginName, qt=True)

    # if dir not exists, create one
    if not os.path.exists(os.path.dirname(outputPath)) :
        os.makedirs(os.path.dirname(outputPath))

    # frame range
    start = mc.currentTime(q=True)
    end = start

    # cmds
    options = []
    options.append('-frameRange %s %s' % (start, end))
    options.append('-uvWrite')
    options.append('-writeUVSets')
    options.append('-writeVisibility')
    options.append('-worldSpace')
    # options.append('-noNormals')
    options.append('-dataFormat ogawa')
    options.append('-attr project -attr assetID -attr assetType -attr assetSubType -attr assetName -attr assetShader -attr adPath -attr refPath')
    options.append('-attr id -attr model -attr uv -attr rig -attr surface -attr data -attr ref -attr lod -attr publishVersion -attr publishPath ')
    options.append('-root %s' % exportGrp)
    outputPath = maya_file.export_alembic_geo(outputPath, options)

    if os.path.exists(outputPath):
        return outputPath
    else:
        raise custom_exception.PipelineError('Failed to export Alembic cache')


def register_data(entity, outputDict):
    """ register gpu hero file """
    description = register_entity.Register(entity)
    output = register_entity.Output()
    output.add_process(entity.process)
    output.add_cache(outputDict['publishedFile'], outputDict['publishedHeroFile'])
    description.set(output=output)
    return description.write()


def export_cache_still_legacy(outputPath, exportGrp) :
    import maya.cmds as mc
    import maya.mel as mm
    pluginName = 'AbcExport.mll'

    if not mc.pluginInfo(pluginName, q=True, l=True):
        mc.loadPlugin(pluginName, qt=True)

    # if dir not exists, create one
    if not os.path.exists(os.path.dirname(outputPath)) :
        os.makedirs(os.path.dirname(outputPath))

    # frame range
    start = mc.currentTime(q=True)
    end = start

    # cmds
    options = []
    options.append('-frameRange %s %s' % (start, end))
    # options.append('-attr project -attr assetID -attr assetType -attr assetSubType -attr assetName -attr assetShader ')
    # options.append('-attr id -attr model -attr uv -attr rig -attr surface -attr data -attr ref -attr lod ')
    options.append('-uvWrite')
    # this will send bad uv to look dev. do not enable this.
    #options.append('-writeFaceSets')
    options.append('-writeVisibility')
    options.append('-worldSpace')
    options.append('-dataFormat ogawa')
    options.append('-root %s' % exportGrp)
    options.append('-file %s' % outputPath)
    optionCmd = (' ').join(options)

    cmd = 'AbcExport -j "%s";' % optionCmd
    result = mm.eval(cmd)

    if os.path.exists(outputPath):
        return outputPath
    else:
        raise custom_exception.PipelineError('Failed to export Alembic cache')