# template for export mod
import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import custom_exception
from rf_utils.pipeline import local_hook
from rf_utils import register_entity
from rftool.render.redshift import rs_utils
reload(publish_info)
reload(export_utils)
reload(register_entity)
reload(rs_utils)



def run(publishInfo):
    """ export GPU cache file from Geo_Grp. """
    assetType = publishInfo.entity.type
    export = False
    if assetType in ['prop']:
        export = True

    if export:
        # track time
        outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=True, outputKey='rsproxy')

        exportGrp = publishInfo.entity.projectInfo.asset.geo_grp()
        outputFile = outputList[0]
        exportResult = export_utils.export_output(publishInfo, outputList, export_rsproxy, outputFile, exportGrp)

        # write to register
        register_data(publishInfo.entity, outputDict)
        return [exportResult, outputList]

    else:
        logger.info('Skip export Gpu')
        return [context_info.Status.skip, []]


def export_rsproxy(outputPath, exportGrp) :
    """ export gpu cache """
    import maya.cmds as mc
    geoGrp = mc.ls('*:%s' % exportGrp)
    targetGrp = geoGrp[0] if geoGrp else None

    if targetGrp:
        dupGrp = mc.duplicate(targetGrp)
        rs_utils.export_rsProxy(outputPath, dupGrp[0], connectivity=0, animation=0, start=0, end=0)
        mc.delete(dupGrp[0])
        return outputPath

    else:
        raise custom_exception.PipelineError('Export Group %s Not found' % targetGrp)
        return context_info.Status.skip


def register_data(entity, outputDict):
    """ register gpu hero file """
    description = register_entity.Register(entity)
    output = register_entity.Output()
    output.add_look(entity.process)
    output.add_rsproxy(outputDict['publishedFile'], outputDict['publishedHeroFile'])
    description.set(output=output)
    return description.write()


