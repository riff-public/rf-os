# template for export mod
import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import register_entity
reload(publish_info)
from rf_app.export.utils import session_call
reload(session_call)




workspace = True

def run(publishInfo):
    """ copy and backup this workspace file """
    # main function to to run for output_widget
    # standard output list
    publishWorkspaces, workspaceDict = export_utils.publish_workspace(publishInfo)
    exportResult = export_utils.export_output(publishInfo, publishWorkspaces, copy_workspace, publishWorkspaces[0])
    register_data(publishInfo.entity, workspaceDict)
    return [exportResult, publishWorkspaces]

def propit_run(publishInfo):
    """ export selected object to workspace """
    # standard output list
    hero = True if publishInfo.entity.step in ['rig'] else False
    publishWorkspaces, workspaceDict = export_utils.publish_workspace(publishInfo)
    outputFile = publishWorkspaces[0]
    processList = []
    topGrp = publishInfo.entity.projectInfo.asset.top_grp()
    geoGrp = publishInfo.entity.projectInfo.asset.geo_grp()
    exportGrp = topGrp if publishInfo.entity.step == 'rig' else geoGrp if publishInfo.entity.step == 'model' else ''
    exportResult = export_utils.export_output(publishInfo, publishWorkspaces, session_export, outputFile, processList, 'export', exportGrp)
    register_data(publishInfo.entity, workspaceDict)
    return [exportResult, publishWorkspaces]


def copy_workspace(outputFile): 
    import maya.cmds as mc
    from rf_maya.lib import maya_file

    currentLocation = mc.file(q=True, loc=True)
    src = maya_file.save_as(currentLocation, type='mayaAscii')

    if not os.path.exists(os.path.dirname(outputFile)):
        os.makedirs(os.path.dirname(outputFile))
    shutil.copy2(src, outputFile)
    return outputFile


def register_data(entity, outputDict): 
    """ register gpu hero file """
    description = register_entity.Register(entity)
    output = register_entity.Output()
    output.add_process(entity.process) if not entity.step in [context_info.Step.texture, context_info.Step.lookdev] else output.add_look(entity.look)
    output.add_workspace(outputDict['publishedFile'], outputDict['publishedHeroFile'])
    description.set(output=output)
    return description.write()

def copy_workspace_legacy(outputFile):
    import maya.cmds as mc
    src = mc.file(save=True)

    if not os.path.exists(os.path.dirname(outputFile)):
        os.makedirs(os.path.dirname(outputFile))
    shutil.copy2(src, outputFile)
    return outputFile

def session_export(dst, processList, output='save', export=''):
    import maya.cmds as mc
    src = mc.file(q=True, sn=True)
    return session_call.run(src, dst, processList, output, export)
