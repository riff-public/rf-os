# template for export mod
import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_utils import publish_info
from rf_utils.context import context_info
from rftool.shade import shade_utils
from rf_utils import custom_exception
from rf_utils import file_utils
from rftool.utils import maya_utils
from rf_utils import register_entity
from rf_maya.rftool.clean.ascii_sanitizer import main as ascl

reload(publish_info)
reload(export_utils)


def run(publishInfo):
    """ export mtr_tmp shader. """
    exportGrp = get_export_grp(publishInfo)
    res = publishInfo.entity.res
    contentGrp = export_utils.get_geo_grp(publishInfo.entity, res)

    # track time
    outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=True, outputKey='materialTmp')

    outputFile = outputList[0]

    run = True if publishInfo.entity.step in ['model'] else False 
    with export_utils.maya_utils().IsolateGeo(exportGrp, contentGrp, run=run): 
        exportResult = export_utils.export_output(publishInfo, outputList, export_material, outputFile, exportGrp, publishInfo)
    register_data(publishInfo.entity, outputDict)
    return [exportResult, outputList]


def export_material(outputPath, exportGrp, publishInfo) :
    """ export gpu cache """
    import maya.cmds as mc
    if not mc.objExists(exportGrp):
        raise custom_exception.PipelineError('No %s found' % exportGrp)

    publishTexture = publishInfo.entity.path.preview_texture().abs_path()
    replaceDict = maya_utils.move_texture_path(publishTexture, exportGrp, resize=128, relink=False)
    result = shade_utils.export_shade_with_data(outputPath, targetGrp=exportGrp)
    file_utils.search_replace_keys(outputPath, replaceDict, backupFile=False)
    # clean if file is not clean 
    ascl.run_clean(outputPath) if not ascl.is_clean(outputPath) else None
    # shade_utils.export_shade_data(dataPath, targetGrp=None)
    if not result:
        raise custom_exception.PipelineError('Failed to export material')
    return result


def get_export_grp(publishInfo):
    import maya.cmds as mc
    exportGrp = publishInfo.entity.projectInfo.asset.geo_grp()

    if not mc.objExists(exportGrp):
        exportGrpNs = mc.ls('*:%s' % exportGrp)[0] if mc.ls('*:%s' % exportGrp) else None
        if exportGrpNs:
            return exportGrpNs
    else:
        return exportGrp


def register_data(entity, outputDict):
    """ register gpu hero file """
    description = register_entity.Register(entity)
    output = register_entity.Output()
    output.add_look(entity.look) if not entity.step == context_info.Step.model else output.add_process(entity.process)
    output.add_mtr_preview(outputDict['publishedFile'], outputDict['heroFile'])
    description.set(output=output)
    return description.write()