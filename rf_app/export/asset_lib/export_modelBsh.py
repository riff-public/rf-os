# template for export mod
import os
import sys
import shutil
import logging
import maya.cmds as mc
import maya.mel as mm 

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_app.export.utils import batch_call
from rf_app.export.utils import session_call
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import register_entity
import rftool.rig
from utaTools.utapy import utaCore
from utaTools.utapy import utaAttrWriteRead
from utaTools.utapy import cleanUp
from utaTools.pkrig import rigTools
from utaTools.utapy import utaTagNameConnectWriteRead as tagNameConWR
from rf_maya.rftool.asset import pullUV
from rf_maya.nuTools.util import bshUtil
from rf_maya.rftool.clean.ascii_sanitizer import main as ascl
from rf_app.export.utils import save_utils
reload(save_utils)
reload(bshUtil)
reload(pullUV)
reload(tagNameConWR)
reload(rigTools)
reload(cleanUp)
reload(utaCore)
reload(publish_info)
reload(session_call)
reload(batch_call)
reload(utaAttrWriteRead)


def run(publishInfo, runProcess=True):
    """ export modelBsh """
    # from rf_utils.context import context_info
    asset = context_info.ContextPathInfo() 
    charName = asset.name

    ## cleanUp File Bsh
    cleanResolutionFile()
    cleanUp.OnOffVisibilityObj()
    cleanUp.cleanShade()
    cleanUp.deleteGrp (DelGrp = ['YetiNode_Grp'])

    processListsFiel = publishInfo.entity.process
    processRigs = []

    processName = processListsFiel.split('Rig')
    processRigs.append(processName[0])
    processObj = processRigs[0].capitalize()
    exportGrp = []
    
    hero = False
    exportGrp.append('BshRigCtrl_Grp') 
    exportGrp.append('BshRigStill_Grp') 
    #exportGrp.append('BshRigGeo_Grp') 
    outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=hero)
    outputFile = outputList[0]
    runProcess = True
    processList = [] if runProcess else []
    exportResult = export_utils.export_output(publishInfo, outputList, session_export, outputFile, processList, 'export', exportGrp)
    register_data(publishInfo.entity, outputDict)


    print "-------------------------------------------------------------------------------------------"
    print "--------------------------------- PUBLISH BLENDSHAPE IS DONE ------------------------------"
    print "-------------------------------------------------------------------------------------------"

    return [exportResult, outputList]


def batch_export(dst, processList, output='save', export=''):
    import maya.cmds as mc
    src = mc.file(q=True, sn=True)
    batch_call.run(src, dst, str(processList), output=output, export=export)
    return dst

def session_export(dst, processList, output='save', export=''):
    import maya.cmds as mc
    src = mc.file(q=True, sn=True)
    result = session_call.run(src, dst, processList, output, export)

    # clean plugin 
    return ascl.run_clean(dst)


def register_data(entity, outputDict):
    """ register gpu hero file """
    description = register_entity.Register(entity)
    output = register_entity.Output()
    output.add_process(entity.process)
    hero = outputDict.get('heroFile')
    if not hero: 
        hero = outputDict.get('publishedHeroFile')
    output.add_maya(outputDict['publishedFile'], hero)
    description.set(output=output)
    return description.write()



def cleanResolutionFile():
    # cleanUp
    cleanUp.clearDeleteGrp()
    cleanUp.clearLayer();
    cleanUp.clearUnusedAnim();
    cleanUp.clearUnknowNode();
    cleanUp.clearUnusedNode();
    cleanUp.clearVraySettings()
    cleanUp.clearCameraDefault()
    rigTools.scaleGimbalControl()
    cleanUp.clearInheritsTransform()
    bshUtil.turnOffBshEdits()
    objSets = mc.ls('set_*')
    for each in objSets:
        mc.delete(each)