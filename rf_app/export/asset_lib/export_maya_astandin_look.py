# template for export mod
import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import custom_exception
from rf_utils.pipeline import local_hook
from rf_utils import register_entity
from rftool.render.redshift import rs_utils
from rf_app.export.utils import batch_call
from rf_app.export.utils import session_call
from rf_maya.lib import material_layer 
from rf_maya.rftool.clean.ascii_sanitizer import main as ascl
reload(material_layer)
reload(publish_info)
reload(export_utils)
reload(register_entity)
reload(rs_utils)

reload(session_call)



def run(publishInfo):
    """ export GPU cache file from Geo_Grp. """
    assetType = publishInfo.entity.type
    looks = material_layer.get_lookdev_look() # try to get lookdev first 
    allOutputs = []
    allResults = []
    export = False

    if assetType in ['prop']:
        export = True

    if export and looks:
        for look in looks: # look manage 
            # track time
            publishInfo.entity.context.update(look=look, process=look)
            outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=True, outputKey='mayarsproxy')
            rsOutputList, rsOutputDict = export_utils.publish_output_list(publishInfo, hero=True, outputKey='rsproxy')
            results = []

            for i, output in enumerate(outputList):
                rsproxyFile = rsOutputList[i]
                if os.path.exists(rsproxyFile):
                    result = export_maya_astandin(publishInfo, output, rsproxyFile)
                    results.append(True) if os.path.exists(output) else results.append(False)
                else:
                    results.append(False)

            # write to register
            register_data(publishInfo.entity, outputDict)
             # result collects 
            allOutputs += outputList
            allResults.append(all(results))

        resultValue = all(allResults)
        return [resultValue, allOutputs]

    else:
        logger.info('Skip export Gpu')
        return [context_info.Status.skip, []]


def export_maya_astandin(publishInfo, outputPath, rsproxyFile) :
    """ export gpu cache """
    import maya.cmds as mc
    transform, shape = arnold_utils.create_aiStandIn(nodeName=publishInfo.entity.name, proxyPath=rsproxyFile, displayMode=0, force=True)
    if not transform or not shape:
        return

    rigGrp = publishInfo.entity.projectInfo.asset.top_grp()
    geoGrp = publishInfo.entity.projectInfo.asset.geo_grp()

    geoGrp = mc.group(transform, n=geoGrp)
    rigGrp = mc.group(geoGrp, n=rigGrp)
    session_export(outputPath, [], output='export', export=rigGrp)
    mc.delete(rigGrp)
    return outputPath



def register_data(entity, outputDict):
    """ register gpu hero file """
    description = register_entity.Register(entity)
    output = register_entity.Output()
    output.add_look(entity.process)
    output.add_maya_rsproxy(outputDict['publishedFile'], outputDict['publishedHeroFile'])
    description.set(output=output)
    return description.write()

def batch_export(dst, processList, output='save', export=''):
    import maya.cmds as mc
    src = mc.file(q=True, sn=True)
    batch_call.run(src, dst, str(processList), output=output, export=export)
    return dst

def session_export(dst, processList, output='save', export=''):
    import maya.cmds as mc
    src = mc.file(q=True, sn=True)
    # clean if not clean 
    result = session_call.run(src, dst, processList, output, export)
    ascl.run_clean(dst) if not ascl.is_clean(dst) else None
    return result
