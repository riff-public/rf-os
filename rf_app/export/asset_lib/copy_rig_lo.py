# template for export mod
import os
import sys
import shutil
import logging
import subprocess

from collections import OrderedDict

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc

from rf_app.export.utils import export_utils
from rf_app.export.utils import batch_call
from rf_app.export.utils import session_call
from rf_app.export.utils import generate_rig_lo
from rf_app.export.utils import save_utils
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import register_entity
from rf_utils import file_utils
from rf_maya.environment import envparser

reload(generate_rig_lo)
reload(publish_info)
reload(session_call)
reload(batch_call)
reload(export_utils)
reload(envparser)
reload(save_utils)

def run(publishInfo):
    """ copy and published this file for next department """
    # standard output list
    hero = True if publishInfo.entity.step in ['model', 'rig'] else False
    entity = publishInfo.entity.copy()
    entity.context.update(process='bodyRig')
    bodyRigFile = '%s/%s' % (entity.path.output_hero().abs_path(), entity.publish_name(hero=True))

    if not os.path.exists(bodyRigFile): 
        logger.info('BodyRig not found %s' % bodyRigFile)
        return [context_info.Status.skip, []]

    # modify res to lo 
    currentRes = publishInfo.entity.res
    publishInfo.entity.context.update(res='lo')
    outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=hero, outputKey='rig')

    outputFile = outputList[0]
    res = publishInfo.entity.res
    geoGrp = publishInfo.entity.projectInfo.asset.geo_grp()

    # get environments for the project and use them in mayapy
    logger.info('Creating environments for mayapy...')
    version = mc.about(v=True)
    project = entity.project
    logger.info('Project: %s' %(project))
    logger.info('Version: %s' %(version))
    department = 'Py'

    # merge environments with current environments
    envStr = entity.projectInfo.get_project_env(dcc='maya', dccVersion=version, department=department)
    envStr['PYMEL_SKIP_MEL_INIT'] = '1'  # need to skip MEL initialization or mayapy might crash
    
    # do the export
    exportResult = export_utils.export_output(publishInfo, outputList, generate, outputFile, bodyRigFile, geoGrp, envStr)
    logger.info('Export returned with result: %s' %exportResult)

    # copy from R: to P: hero folder
    register_data(publishInfo.entity, outputDict)
    publishInfo.entity.context.update(res=currentRes)

    # Save As MB 
    heroFile = outputDict['heroFile']
    logger.debug( 'Run subprocess to save as mb %s' %heroFile)
    save_utils.run_cmd(outputFile, heroFile)

    register_data( publishInfo.entity, outputDict )

    return [exportResult, outputList]

def register_data(entity, outputDict):
    """ register gpu hero file """
    description = register_entity.Register(entity)
    output = register_entity.Output()
    output.add_process(entity.process)
    output.add_maya(outputDict['publishedFile'], outputDict['publishedHeroFile'])
    description.set(output=output)
    return description.write()

def generate(dst, src, geoGrp, env):
    exportResult = generate_rig_lo.run_cmd(src, dst, geoGrp, env)
    return dst