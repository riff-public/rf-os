# template for export mod
import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import custom_exception
from rf_utils.pipeline import local_hook
from rf_utils import register_entity
from rftool.render.redshift import rs_utils
from rf_maya.lib import material_layer 
reload(material_layer)
reload(publish_info)
reload(export_utils)
reload(register_entity)
reload(rs_utils)



def run(publishInfo):
    """ export GPU cache file from Geo_Grp. """
    assetType = publishInfo.entity.type
    looks = material_layer.get_lookdev_look() # try to get lookdev first 
    allOutputs = []
    results = []
    export = True
    startFrame, endFrame = get_time_range(publishInfo.entity.name)
    
    if assetType in ['animated']:
        export = True

    if export and looks:
        for look in looks:
            layer = material_layer.get_current_layer()
            material_layer.show_all_geo(layer)
        for look in looks: 
            # look manage 
            publishInfo.entity.context.update(look=look, process=publishInfo.entity.process, step='anim')
            materialType = material_layer.get_active_material(look)
            # set active 
            material_layer.set_active_layer(look, materialType)
            
            # track time
            outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=False, outputKey='rsproxySq')

            outputFile = outputList[0]
            exportGrp = export_grp(publishInfo.entity)
            exportResult = export_utils.export_output(publishInfo, outputList, export_rsproxy, outputFile, exportGrp, startFrame, endFrame)

            # write to register
            register_data(publishInfo.entity, outputDict)

            # result collects 
            allOutputs += outputList
            results.append(exportResult)

        results = all(results)
        return [results, allOutputs]

    else:
        logger.info('Skip export Gpu')
        return [context_info.Status.skip, []]



def export_rsproxy(outputPath, exportGrp, startFrame, endFrame) :
    """ export gpu cache """
    from rftool.utils import maya_utils

    with maya_utils.FreezeViewport(mode='legacy'): 
        rs_utils.export_rsProxy(outputPath, exportGrp, connectivity=0, animation=1, start=startFrame, end=endFrame)
    
    return outputPath

def register_data(entity, outputDict):
    """ register gpu hero file """
    description = register_entity.Register(entity)
    output = register_entity.Output()
    output.add_look(entity.process)
    output.add_rsproxy(outputDict['publishedFile'], outputDict['publishedHeroFile'])
    description.set(output=output)
    return description.write()

def export_grp(entity): 
    import maya.cmds as mc
    exportGrp = entity.projectInfo.asset.export_grp(entity.res)
    # geoGrp = entity.projectInfo.asset.geo_grp()
    # targetGrps = mc.ls('*:%s' % geoGrp, l=True)
    # targetGrps = [a for a in targetGrps if exportGrp in a]

    return exportGrp if mc.objExists(exportGrp) else None

def get_time_range(assetName): 
    import maya.cmds as mc 
    startTime = mc.shot(assetName, q=True, startTime=True)
    endTime = mc.shot(assetName, q=True, endTime=True)
    return startTime, endTime
