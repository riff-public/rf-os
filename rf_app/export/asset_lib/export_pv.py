# template for export mod
import os
import sys
import shutil
import logging
import maya.cmds as mc

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from utaTools.utapy import utaCore
from rf_app.export.utils import export_utils
from rf_app.export.utils import batch_call
from rf_app.export.utils import session_call
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import register_entity
from utaTools.utapy import cleanUp
from utaTools.pkrig import rigTools
reload(utaCore)
reload(rigTools)
reload(cleanUp)
reload(publish_info)
reload(session_call)
reload(batch_call)

def find_export_grp( expGrp = 'Export_Grp' ):
    listConnect = mc.listConnections(expGrp,p=True,c=True)
    #delete connection from export grp
    if listConnect:
        for i in range(0,len(listConnect)-1,2):
            mc.disconnectAttr(listConnect[i],listConnect[i+1])

    return mc.listRelatives( expGrp , c = True )

def run(publishInfo):
   
    publishInfo.entity.context.update(process='main')
    asset = context_info.ContextPathInfo() 
    charName = asset.name

    ## clean
    cleanUp.OnOffVisibilityObj()
    cleanResolutionFile()
    utaCore.setDefaultControl(char = charName)
    exportGrp = find_export_grp()

    hero = True
    outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=hero)
    outputFile = outputList[0]
    runProcess = True
    processList = [] if runProcess else []
    exportResult = export_utils.export_output(publishInfo, outputList, session_export, outputFile, processList, 'export', exportGrp)
    
    register_data(publishInfo.entity, outputDict)

    utaCore.openFileWorldSpace()
    return [exportResult, outputList]

def register_data(entity, outputDict):
    """ register gpu hero file """
    description = register_entity.Register(entity)
    output = register_entity.Output()
    output.add_process(entity.process)
    output.add_maya(outputDict['publishedFile'], outputDict['publishedHeroFile'])
    description.set(output=output)
    return description.write()

def batch_export(dst, processList, output='save', export=''):
    # import maya.cmds as mc
    src = mc.file(q=True, sn=True)
    batch_call.run(src, dst, str(processList), output=output, export=export)
    return dst

def session_export(dst, processList, output='save', export=''):
    # import maya.cmds as mc
    src = mc.file(q=True, sn=True)
    return session_call.run(src, dst, processList, output, export)


def cleanResolutionFile():

    cleanUp.clearDeleteGrp()
    cleanUp.clearLayer();
    cleanUp.clearUnusedAnim();
    cleanUp.clearUnknowNode();
    cleanUp.clearUnusedNode();
    cleanUp.clearVraySettings()
    cleanUp.clearCameraDefault()
    rigTools.scaleGimbalControl()
    cleanUp.clearInheritsTransform()
    cleanUp.checkParameterUV()

    objSets = mc.ls('set_*')
    for each in objSets:
        mc.delete(each)