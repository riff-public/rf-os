# template for export mod
import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import custom_exception
from rf_utils.pipeline import local_hook
from rf_utils import register_entity
from rf_app.asm import asm_lib
import maya.cmds as mc 
reload(publish_info)
reload(export_utils)
reload(register_entity)
reload(asm_lib)



def run(publishInfo):
    """ export Set from set group. """
    assetType = publishInfo.entity.type
    exportGrp = publishInfo.entity.projectInfo.asset.cam_grp()
    export = False
    outputLists = []
    exportResults = []
    if assetType in ['set']:
        if mc.objExists(exportGrp): 
            export = True

    if export:
        # track time
        outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=True, outputKey='cam')
        outputFile = outputList[0]
        exportResult = export_utils.export_output(publishInfo, outputList, export_cam, outputFile, exportGrp)

        if not exportResult: 
            return [context_info.Status.skip, []]
        # write to register
        register_data(publishInfo.entity, outputDict)

        return [exportResult, outputList]

    else:
        logger.info('Wrong set format')
        return [context_info.Status.skip, []]


def export_cam(outputPath, exportGrp) :
    """ export gpu cache """
    from rftool.utils import maya_utils
    import maya.cmds as mc 
    if mc.objExists(exportGrp): 
        maya_utils.export_selection(outputPath, exportGrp)
    return outputPath if os.path.exists(outputPath) else None


def get_zones(mainGrp): 
    # name for set group 
    import maya.cmds as mc
    # find only group
    zones = [a for a in mc.listRelatives(mainGrp, c=True, type='transform') if not mc.listRelatives(a, s=True)]
    return zones if zones else None

def register_data(entity, outputDict):
    """ register gpu hero file """
    description = register_entity.Register(entity)
    output = register_entity.Output()
    output.add_process(entity.process)
    output.add_cam(outputDict['publishedFile'], outputDict['heroFile'])
    description.set(output=output)
    return description.write()

