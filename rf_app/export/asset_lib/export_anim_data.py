# template for export mod
import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_utils.pipeline import check_maya_plugin
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import custom_exception
from rf_utils import register_entity
reload(publish_info)
reload(export_utils)
reload(check_maya_plugin)


def run(publishInfo):
    """ Export .anim data. only work for 1 asset not for group loop """
    app = publishInfo.entity.context.app

    outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=True, outputKey='animData')
    publishInfo.entity.context.update(app=app) # restore

    exportGrp = publishInfo.entity.projectInfo.asset.allmover_grp()
    
    # assume 1 asset in the scene, pick the 1st one with namespace 
    exportGrp = detect_export_grp(exportGrp)
    res = publishInfo.entity.res
    outputFile = outputList[0]
    exportResult = export_utils.export_output(publishInfo, outputList, export_anim, outputFile, exportGrp)
    register_data(publishInfo.entity, outputDict)
    return [exportResult, outputList]


@check_maya_plugin.anim_export
def export_anim(outputPath, exportGrp) :
    import maya.cmds as mc
    import maya.mel as mm
    mc.select(exportGrp)
    # take time slider time 
    # assume that qc will take care of the range before get into export 
    # to be sure of range, better get data from shotgun 
    startFrame = mc.playbackOptions(q=True, min=True)
    endFrame = mc.playbackOptions(q=True, max=True)
    mm.eval('file -force -options "precision=8;intValue=17;nodeNames=1;verboseUnits=0;whichRange=1;range=%s:%s;options=keys;hierarchy=below;controlPoints=0;shapes=0;helpPictures=0;useChannelBox=0;copyKeyCmd=-animation objects -option keys -hierarchy below -controlPoints 0 -shape 0 " -typ "animExport" -pr -es "%s";' % (startFrame, endFrame, outputPath))

    if os.path.exists(outputPath):
        return outputPath
    else:
        raise custom_exception.PipelineError('Failed to export Alembic cache')


def detect_export_grp(exportGrp): 
    import maya.cmds as mc 
    if not mc.objExists(exportGrp): 
        ls = mc.ls('*:%s' % exportGrp)
        if ls: 
            return ls[0]
    return exportGrp


def register_data(entity, outputDict):
    """ register gpu hero file """
    description = register_entity.Register(entity)
    output = register_entity.Output()
    output.add_process(entity.process)
    output.add_anim_data(outputDict['publishedFile'], outputDict['publishedHeroFile'])
    description.set(output=output)
    return description.write()




