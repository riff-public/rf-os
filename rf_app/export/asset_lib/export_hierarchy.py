# template for export mod
import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_utils import publish_info
from rf_utils import custom_exception
from rf_utils import register_entity
from rf_utils import file_utils
from rf_utils.context import context_info
reload(publish_info)
reload(export_utils)
reload(register_entity)


def run(publishInfo):
    assetType = publishInfo.entity.type
    export = False
    if not assetType in ['set']:
        export = True

    if export:
        outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=False, outputKey='hierarchy')
        exportGrp = publishInfo.entity.projectInfo.asset.geo_grp()
        outputFile = outputList[0]
        exportResult = export_utils.export_output(publishInfo, outputList, export_hierarchy, outputFile, exportGrp)

        # write to register
        register_data(publishInfo.entity, outputDict)
        return [exportResult, outputList]

    else:
        logger.info('Skip export Hierarchy Data')
        return [context_info.Status.skip, []]


def export_hierarchy(outputPath, exportGrp) :
    """ export gpu cache """
    import maya.cmds as mc
    if mc.objExists(exportGrp):
        data = mc.ls(exportGrp, dag=True, l=True)
        file_utils.ymlDumper(outputPath, data)
        return outputPath
    else:
        return context_info.Status.skip
        raise custom_exception.PipelineError('No %s found' % exportGrp)


def register_data(entity, outputDict):
    """ register gpu hero file """
    description = register_entity.Register(entity)
    output = register_entity.Output()
    output.add_process(entity.process)
    value = output.add_files(outputDict['publishedFile'], outputDict['publishedHeroFile'])
    output.add_key('hierarchy', value)
    description.set(output=output)
    return description.write()

