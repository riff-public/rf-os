# template for export mod
import os
import sys
import shutil
import logging
import pymel.core as pm 

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_utils.miarmy import miarmy_utils
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import register_entity
from utaTools.utapy import cleanUp
from utaTools.utapy import utaCore
reload(publish_info)
reload(miarmy_utils)
reload(cleanUp)
reload(utaCore)

def run(publishInfo, runProcess=True):
    """ Export Miarmy Original Agent """
    process = publishInfo.entity.process
    outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=True, outputKey='miarmyOa')
    
    ## CleanUp
    cleanUp.clearDeleteGrp()   

    exportGrp = miarmy_utils.find_agent_loco()
    actions = miarmy_utils.list_actions()
    outputFile = outputList[0]
    processList = []
    exportResult = False

    with IsolateToWorld(*actions): 
        miarmy_utils.disconnect_miarmy_content()
        exportResult = export_utils.export_output(publishInfo, outputList, export_oa, outputFile, exportGrp)

    register_data(publishInfo.entity, outputDict)

    ## Open File Work
    utaCore.openFileWorldSpace()

    return [exportResult, outputList]


def export_oa(dst, exportGrp):
    import maya.cmds as mc
    from rf_maya.lib import maya_file
    
    name, ext = os.path.splitext(dst)
    fileType = 'mayaAscii' if ext == '.ma' else 'mayaBinary' if ext == '.mb' else 'mayaAscii'
    result = maya_file.export_selection(dst, [exportGrp], fileType)
    return result

# def delete_miarmy_content():
#     miarmy_contents = mc.listRelatives('Export_Grp')[0]
#     agent = mc.listRelatives(miarmy_contents)[0]
#     if mc.objectType(agent) == 'McdAgentGroup':
#         mc.parent(agent, world=True)
#         mc.delete(miarmy_contents)
#         mc.parent(agent, 'Export_Grp')



def register_data(entity, outputDict):
    """ register gpu hero file """
    description = register_entity.Register(entity)
    output = register_entity.Output()
    output.add_process(entity.process)
    output.add_rig(outputDict['publishedFile'], outputDict['publishedHeroFile'])
    description.set(output=output)
    return description.write()


class IsolateToWorld(object):
    """ Unparent Agent_locl and Actions """
    def __init__(self, *args):
        super(IsolateToWorld, self).__init__()
        self.objs = args
        self.locDict = dict()

    def __enter__(self): 
        for obj in self.objs: 
            node = pm.PyNode(obj)
            parent = pm.listRelatives(node, p=True)
            if parent: 
                # unparent Agent loco
                pm.parent(node, w=True)
                self.locDict[node] = parent[0]

    def __exit__(self, *args): 
        for node, parent in self.locDict.iteritems(): 
            pm.parent(node, parent)
