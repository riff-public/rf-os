# template for export mod
import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_app.export.utils import batch_call
from rf_app.export.utils import session_call
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import register_entity
from rf_utils import daily
import holiday
reload(publish_info)
reload(session_call)
reload(batch_call)
reload(holiday)

def run(publishInfo):
    """ copy and published this file for next department """
    import maya.cmds as mc 
    from rf_maya.lib import maya_file
    entity = publishInfo.entity.copy()
    entity.context.update(process='holiday')

    # save 
    fileName = mc.file(save=True, f=True)
    hero = entity.publish_name(hero=True)
    dir = entity.path.hero().abs_path()

    dst = '%s/%s' % (dir, hero)
    if not os.path.exists(dir): 
        os.makedirs(dir)
    # standard output list
    geo = holiday.convert_model(publishInfo.entity)
    maya_file.export_selection(dst, [geo], 'mayaAscii')
    daily.submit(entity, [dst])

    # reopen workspace 
    mc.file(fileName, o=True, f=True, prompt=False)

    return [True, [dst]]
#     hero = True if publishInfo.entity.step in ['model', 'rig'] else False
#     publishInfo.entity.context.update(process='holiday')
#     outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=hero, outputKey='cache')
#     outputFile = outputList[0]
#     processList = ['import_ref', 'remove_namespaces']
    
#     res = publishInfo.entity.res
#     exportGrp = 
#     contentGrp = export_utils.get_geo_grp(publishInfo.entity, res)
#     run = True if publishInfo.entity.step in ['model'] else False 
    
#     with export_utils.maya_utils().IsolateGeo(exportGrp, contentGrp, run=run): 
#         exportResult = export_utils.export_output(publishInfo, outputList, session_export, outputFile, processList, 'export', exportGrp)
    
#     register_data(publishInfo.entity, outputDict)
#     return [exportResult, outputList]

# def register_data(entity, outputDict):
#     """ register gpu hero file """
#     description = register_entity.Register(entity)
#     output = register_entity.Output()
#     output.add_process(entity.process)
#     output.add_maya(outputDict['publishedFile'], outputDict['publishedHeroFile'])
#     description.set(output=output)
#     return description.write()

# def batch_export(dst, processList, output='save', export=''):
#     import maya.cmds as mc
#     src = mc.file(q=True, sn=True)
#     batch_call.run(src, dst, str(processList), output=output, export=export)
#     return dst

def session_export(dst, processList, output='export', export=''):
    import maya.cmds as mc
    src = mc.file(q=True, sn=True)
    return session_call.run(src, dst, processList, output, export)