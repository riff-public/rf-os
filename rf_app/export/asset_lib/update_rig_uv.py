# template for export mod
import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import custom_exception
from rf_utils.pipeline import local_hook
from rf_utils import register_entity
reload(publish_info)
reload(export_utils)
reload(register_entity)



def run(publishInfo):
    """ update new uv into rig file """
    assetType = publishInfo.entity.type
    export = False
    if assetType in ['char', 'prop']:
        export = True

    currentStep = publishInfo.entity.step

    if export:
        app = publishInfo.entity.context.app
        process = publishInfo.entity.context.process
        publishInfo.entity.context.update(step='rig')

        # track time
        outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=True, outputKey='rig')
        heroFile = outputDict['heroFile']

        if not os.path.exists(heroFile): 
            # restore
            publishInfo.entity.context.update(step=currentStep)
            return [context_info.Status.skip, []]

        if check_rig_status(publishInfo.entity): 
            result = pull_uv(heroFile)
            update_rig_uv_status(publishInfo.entity, result)

            # restore 
            publishInfo.entity.context.update(step=currentStep)

            if result: 
                return [result, [heroFile]]
            else: 
                return [context_info.Status.skip, []]

        else: 
            # restore 
            publishInfo.entity.context.update(step=currentStep)
            
            logger.warning('Skip because rig status not "apr"')
            return [context_info.Status.skip, []]


def pull_uv(path): 
    from rftool.asset import pullUV
    reload(pullUV)
    result = pullUV.mergeUV_batch(path)
    return result


def update_rig_uv_status(entity, status): 
    from rf_utils.sg import sg_process
    taskName = 'rigUv'
    status = 'apr' if status else 'fix'
    entity.context.use_sg(sg_process.sg, project=entity.project, entityType='asset', entityName=entity.name)

    # find task 
    filters = [['project.Project.name', 'is', entity.project], ['entity', 'is', entity.context.sgEntity], ['content', 'is', taskName]]
    fields = ['content', 'id']
    taskEntity = sg_process.sg.find_one('Task', filters, fields)

    # set status 
    if taskEntity: 
        return sg_process.set_task_status(taskEntity['id'], status)


def check_rig_status(entity): 
    from rf_utils.sg import sg_process
    taskName = 'rig'
    entity.context.use_sg(sg_process.sg, project=entity.project, entityType='asset', entityName=entity.name)

    # find task 
    filters = [['project.Project.name', 'is', entity.project], ['entity', 'is', entity.context.sgEntity], ['content', 'is', taskName]]
    fields = ['content', 'id', 'sg_status_list']
    taskEntity = sg_process.sg.find_one('Task', filters, fields)

    # set status 
    rigStatus = taskEntity.get('sg_status_list')

    return True if rigStatus == 'apr' else False




