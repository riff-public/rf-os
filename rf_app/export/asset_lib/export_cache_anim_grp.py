# template for export mod
import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
import pymel.core as pm 
from rf_app.export.utils import export_utils
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import custom_exception
from rf_utils import register_entity
reload(publish_info)
reload(export_utils)


def run(publishInfo):
    """ export Alembic cache file from Geo_Grp. """
    # This export is only for animated type which has more than 1 asset in the scene 
    if publishInfo.entity.type != 'animated':
        return [context_info.Status.skip, []]
    app = publishInfo.entity.context.app
    geoGrp = publishInfo.entity.projectInfo.asset.geo_grp() or 'Geo_Grp'
    list_asset = pm.ls('*:%s' % geoGrp)
    exportResults = [] 
    allOutputList = []
    assetName = publishInfo.entity.name
    
    for asset_geo_grp in list_asset:
        namespace = asset_geo_grp.namespace()
        # publishInfo.entity.context.update(process=publishInfo.entity.name, entityGrp='char')
        
        ######context asset
        ref_pymel_node = pm.FileReference(namespace = namespace)
        path = ref_pymel_node.path
        asset = context_info.ContextPathInfo(path=path)
        localPublishInfo = publish_info.PreRegister(asset)
        
        ########update entity
        # publishInfo.entity.context.update(entity=asset.name)

        localPublishInfo.entity.context.update(process=publishInfo.entity.name, step='anim', publishVersion=publishInfo.entity.publishVersion, res=publishInfo.entity.res)
        # publishInfo.entity.context.update(app='gpu')

        # track time
        outputList, outputDict = export_utils.publish_output_list(localPublishInfo, hero=True, outputKey='cache')
        
        publishInfo.entity.context.update(app=app) # restore

        exportGrp = asset_geo_grp
        res = publishInfo.entity.res
        outputFile = outputList[0]
        exportResult = export_utils.export_output(publishInfo, outputList, export_cache_still, outputFile, exportGrp, assetName)
        register_data(publishInfo.entity, outputDict)

        exportResults.append(exportResult)
        allOutputList += outputList
        # export_utils.parent_group(publishInfo.entity, res, check=True)
        
    return [all(exportResults), allOutputList]
        
        # assetType = detech_asset_type(exportGrp)

        # if assetType == 'import': 
        # # if publishInfo.entity.step in ['model']:
        # #     res = publishInfo.entity.res
        # #     export_utils.parent_group(publishInfo.entity, res, check=False)
        #     res = publishInfo.entity.res
        #     outputFile = outputList[0]
        #     exportResult = export_utils.export_output(publishInfo, outputList, export_cache_still, outputFile, exportGrp)
        #     register_data(publishInfo.entity, outputDict)
        #     export_utils.parent_group(publishInfo.entity, res, check=True)
        #     return [exportResult, outputList]

        # if assetType == 'reference': 
        



def export_cache_still(outputPath, exportGrp, assetName) :
    import maya.cmds as mc
    import maya.mel as mm
    from rf_maya.lib import maya_file
    reload(maya_file)

    pluginName = 'AbcExport.mll'

    if not mc.pluginInfo(pluginName, q=True, l=True):
        mc.loadPlugin(pluginName, qt=True)

    # if dir not exists, create one
    if not os.path.exists(os.path.dirname(outputPath)) :
        os.makedirs(os.path.dirname(outputPath))

    start = mc.shot(assetName, q=True, startTime=True)
    end = mc.shot(assetName, q=True, endTime=True)

    # frame range
    # end = mc.playbackOptions(max=True,q=True)
    # start = mc.playbackOptions(min=True,q=True)

    # cmds
    options = []
    options.append('-frameRange %s %s' % (start, end))
    options.append('-uvWrite')
    options.append('-writeVisibility')
    options.append('-worldSpace')
    options.append('-dataFormat ogawa')
    options.append('-attr project -attr assetID -attr assetType -attr assetSubType -attr assetName -attr assetShader -attr adPath -attr refPath')
    options.append('-attr id -attr model -attr uv -attr rig -attr surface -attr data -attr ref -attr lod ')
    options.append('-root %s' % exportGrp)
    outputPath = maya_file.export_alembic_geo(outputPath, options)

    if os.path.exists(outputPath):
        return outputPath
    else:
        raise custom_exception.PipelineError('Failed to export Alembic cache')


def detect_export_grp(exportGrp): 
    import maya.cmds as mc 
    if not mc.objExists(exportGrp): 
        ls = mc.ls('*:%s' % exportGrp)
        if ls: 
            return ls[0]
    return exportGrp

def detech_asset_type(exportGrp): 
    import maya.cmds as mc 
    if mc.objExists(exportGrp): 
        return 'import'

    if not mc.objExists(exportGrp): 
        ls = mc.ls('*:%s' % exportGrp)
        if ls: 
            return 'reference'



def register_data(entity, outputDict):
    """ register gpu hero file """
    description = register_entity.Register(entity)
    output = register_entity.Output()
    output.add_process(entity.process)
    output.add_cache(outputDict['publishedFile'], outputDict['publishedHeroFile'])
    description.set(output=output)
    return description.write()


def export_cache_still_legacy(outputPath, exportGrp) :
    import maya.cmds as mc
    import maya.mel as mm
    pluginName = 'AbcExport.mll'

    if not mc.pluginInfo(pluginName, q=True, l=True):
        mc.loadPlugin(pluginName, qt=True)

    # if dir not exists, create one
    if not os.path.exists(os.path.dirname(outputPath)) :
        os.makedirs(os.path.dirname(outputPath))

    # frame range
    start = mc.currentTime(q=True)
    end = start

    # cmds
    options = []
    options.append('-frameRange %s %s' % (start, end))
    # options.append('-attr project -attr assetID -attr assetType -attr assetSubType -attr assetName -attr assetShader ')
    # options.append('-attr id -attr model -attr uv -attr rig -attr surface -attr data -attr ref -attr lod ')
    options.append('-uvWrite')
    # this will send bad uv to look dev. do not enable this.
    #options.append('-writeFaceSets')
    options.append('-writeVisibility')
    options.append('-worldSpace')
    options.append('-dataFormat ogawa')
    options.append('-root %s' % exportGrp)
    options.append('-file %s' % outputPath)
    optionCmd = (' ').join(options)

    cmd = 'AbcExport -j "%s";' % optionCmd
    result = mm.eval(cmd)

    if os.path.exists(outputPath):
        return outputPath
    else:
        raise custom_exception.PipelineError('Failed to export Alembic cache')