# template for export mod
import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_app.export.utils import batch_call
from rf_app.export.utils import session_call
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import register_entity
from rf_utils import daily
import AAApex
reload(publish_info)
reload(session_call)
reload(batch_call)
reload(AAApex)

def run(publishInfo):
    """ copy and published this file for next department """
    import maya.cmds as mc 
    from rf_maya.lib import maya_file
    entity = publishInfo.entity.copy()
    entity.context.update(process='AAApex')

    # save 
    fileName = mc.file(save=True, f=True)
    hero = entity.publish_name(hero=True)
    dir = entity.path.hero().abs_path()

    dst = '%s/%s' % (dir, hero)
    if not os.path.exists(dir): 
        os.makedirs(dir)
    # standard output list
    geo = AAApex.convert_model(publishInfo.entity)
    maya_file.export_selection(dst, [geo], 'mayaAscii')
    daily.submit(entity, [dst])

    # reopen workspace 
    mc.file(fileName, o=True, f=True, prompt=False)

    return [True, [dst]]
