# template for export mod
import os
import sys
import shutil
import logging
import maya.cmds as mc

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_app.export.utils import batch_call
from rf_app.export.utils import session_call
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import register_entity
import rftool.rig
from utaTools.utapy import utaCore
from utaTools.utapy import utaAttrWriteRead
from utaTools.utapy import cleanUp
from utaTools.pkrig import rigTools
from rf_maya.rftool.utils import pipeline_utils
reload(rigTools)
reload(cleanUp)
reload(utaCore)
reload(publish_info)
reload(session_call)
reload(batch_call)
reload(utaAttrWriteRead)


def run(publishInfo, runProcess=True):
    # from rf_utils.context import context_info
    asset = context_info.ContextPathInfo() 
    charName = asset.name
    projectName = asset.project  
    """ copy and published this file for next department """
    # standard output list
    if publishInfo.entity.process in [  'main', 
                                        'combRig'] :
        ## Clean File FclRig, AddRig for Publish Proxy
        utaCore.cleanUpReferenceFileForProxy()

        ## clean
        cleanResolutionFile()
        if not projectName == 'Ream':
            cleanUp.OnOffVisibilityObj()
            cleanUp.lockAllMover()            
        utaAttrWriteRead.attrWrite()
        cleanUp.remove_all_unused_influences('PrGeo_Grp')

        if publishInfo.entity.type == 'char':
            ## Check Pr or Md
            selectProcess(process = 'Pr')
            if not projectName == 'Ream':
                utaCore.setDefaultControl(char = charName)

        elif publishInfo.entity.type == 'prop':
            ## Check Pr or Md
            selectProcess(process = 'Pr')
        else:
            ## Check Pr or Md
            selectProcess(process = '')

        hero = True
        exportGrp = 'Rig_Grp'

    elif publishInfo.entity.process in ['skel', 
                                        'hdRigTmpLoc', 
                                        'nsRigTmpLoc', 
                                        'eyeRigTmpLoc', 
                                        'earRigTmpLoc', 
                                        'mthRigTmpLoc', 
                                        'dtlRigTmpLoc', 
                                        'ebRigTmpLoc', 
                                        'ttRigTmpLoc'] :
        # clean
        cleanResolutionFile()
        hero = False
        exportGrp = 'Export_Grp'

        
    elif publishInfo.entity.process in ['ctrl', 
                                        'bodyRig']:
        # clean
        cleanResolutionFile()
        hero = False
        exportGrp = 'Rig_Grp'

    elif publishInfo.entity.process in ['hdRigCtrl', 
                                        'nsRigCtrl', 
                                        'eyeRigCtrl', 
                                        'earRigCtrl', 
                                        'mthRigCtrl', 
                                        'dtlRigCtrl', 
                                        'ebRigCtrl', 
                                        'ttRigCtrl', 
                                        'fclRig', 
                                        'bsh']:
        # clean
        cleanResolutionFile()

        processListsFiel = publishInfo.entity.process
        processRigs = []

        processName = processListsFiel.split('Rig')
        processRigs.append(processName[0])
        processObj = processRigs[0].capitalize()
        exportGrp = []
        if publishInfo.entity.process == 'hdRigCtrl':
            hero = False
            #List Export
            CtrlGrp = (processObj + 'DfmRigCtrl_Grp')
            SkinGrp = (processObj + 'DfmRigSkin_Grp')
            StillGrp = (processObj + 'DfmRigStill_Grp')
            JntGrp = (processObj + 'DfmRigJnt_Grp')

            if mc.objExists(CtrlGrp):
                exportGrp.append(CtrlGrp)
            else:
                print "Please Check >>" , CtrlGrp
            if mc.objExists(SkinGrp):
                exportGrp.append(SkinGrp)
            else:
                print "Please Check >>" , SkinGrp
            if mc.objExists(StillGrp):
                exportGrp.append(StillGrp)
            else:
                print "Please Check >>" , StillGrp
            if mc.objExists(JntGrp):
                exportGrp.append(JntGrp)
            else:
                print "Please Check >>" , JntGrp
        else:
            hero=False
            #List Export
            CtrlGrp = (processObj + 'RigCtrl_Grp')
            SkinGrp = (processObj + 'RigSkin_Grp')
            StillGrp = (processObj + 'RigStill_Grp')
            JntGrp = (processObj + 'RigJnt_Grp')
            RigGrp = ('Rig_Grp')
            FclRig = ('FacailRig_Grp')
            BshGeoRig = (processObj + 'RigGeo_Grp')

            if mc.objExists(CtrlGrp):
                exportGrp.append(CtrlGrp)
            else:
                print "Please Check >>" , CtrlGrp
            if mc.objExists(SkinGrp):
                exportGrp.append(SkinGrp)
            else:
                print "Please Check >>" , SkinGrp
            if mc.objExists(StillGrp):
                exportGrp.append(StillGrp)
            else:
                print "Please Check >>" , StillGrp
            if mc.objExists(JntGrp):
                exportGrp.append(JntGrp)
            else:
                print "Please Check >>" , JntGrp
            if mc.objExists(RigGrp):
                exportGrp.append(RigGrp)
            else:
                print "Please Check >>" , RigGrp
            if mc.objExists(BshGeoRig):
                exportGrp.append(BshGeoRig)
            else:
                print "Please Check >>" , BshGeoRig
            if mc.objExists(FclRig):
                exportGrp.append(FclRig)
            else:
                print "Please Check >>" , FclRig

    elif publishInfo.entity.process in ['hdRig', 
                                        'nsRig', 
                                        'eyeRig', 
                                        'earRig', 
                                        'mthRig', 
                                        'dtlRig', 
                                        'ebRig', 
                                        'addRig', 
                                        'ttRig', 
                                        'hdGeo', 
                                        'volCompRig']:
        # clean
        cleanResolutionFile()

        processListsFiel = publishInfo.entity.process
        processRigs = []
        processName = processListsFiel.split('Rig')
        processRigs.append(processName[0])
        processObj = processRigs[0].capitalize()
        hero = False
        exportGrp = []
        if publishInfo.entity.process == 'hdGeo':
            #List Export
            fclRigGeo = 'FclRigGeo_Grp'
            if mc.objExists(fclRigGeo):
                exportGrp.append(fclRigGeo)
            else:
                print "Please Check >> ", fclRigGeo

        elif publishInfo.entity.process == 'dtlRig':
            dtlRigSkin = (processObj + 'RigSkin_Grp')
            dtlRigStill = (processObj + 'RigStill_Grp')
            dtlRigHi = (processObj + 'RigCtrlHi_Grp')
            dtlRigMid = (processObj + 'RigCtrlMid_Grp')
            dtlRigLow = (processObj + 'RigCtrlLow_Grp')
            if mc.objExists(dtlRigSkin):
                exportGrp.append(dtlRigSkin)
            else:
                print "Please Check >> ", dtlRigSkin
            if mc.objExists(dtlRigStill):
                exportGrp.append(dtlRigStill)
            else:
                print "Please Check >> ", dtlRigStill
            if mc.objExists(dtlRigHi):
                exportGrp.append(dtlRigHi)
            else:
                print "Please Check >> ", dtlRigHi
            if mc.objExists(dtlRigMid):
                exportGrp.append(dtlRigMid)
            else:
                print "Please Check >> ", dtlRigMid
            if mc.objExists(dtlRigLow):
                exportGrp.append(dtlRigLow)
            else:
                print "Please Check >> ", dtlRigLow

        elif publishInfo.entity.process == 'hdRig':
            #List Export
            CtrlGrp = (processObj + 'DfmRigCtrl_Grp')
            SkinGrp = (processObj + 'DfmRigSkin_Grp')
            StillGrp = (processObj + 'DfmRigStill_Grp')
            JntGrp = (processObj + 'DfmRigJnt_Grp')
            if mc.objExists(CtrlGrp):
                exportGrp.append(CtrlGrp)
            else:
                print "Please Check >> ", CtrlGrp
            if mc.objExists(SkinGrp):
                exportGrp.append(SkinGrp)
            else:
                print "Please Check >> ", SkinGrp
            if mc.objExists(StillGrp):
                exportGrp.append(StillGrp)
            else:
                print "Please Check >> ", StillGrp
            if mc.objExists(JntGrp):
                exportGrp.append(JntGrp)
            else:
                print "Please Check >> ", JntGrp
        else:
            #List Export
            CtrlGrp = (processObj + 'RigCtrl_Grp')
            SkinGrp = (processObj + 'RigSkin_Grp')
            StillGrp = (processObj + 'RigStill_Grp')
            JntGrp = (processObj + 'RigJnt_Grp')
            addRig = (processObj + 'RigGeo_Grp')

            if mc.objExists(CtrlGrp):
                exportGrp.append(CtrlGrp)
            else:
                print "Please Check >> ", CtrlGrp
            if mc.objExists(SkinGrp):
                exportGrp.append(SkinGrp)
            else:
                print "Please Check >> ", SkinGrp
            if mc.objExists(StillGrp):
                exportGrp.append(StillGrp)
            else:
                print "Please Check >> ", StillGrp
            if mc.objExists(JntGrp):
                exportGrp.append(JntGrp)
            else:
                print "Please Check >> ", JntGrp
            if mc.objExists(addRig):
                exportGrp.append(addRig)
            else:
                print "Please Check >> ", addRig

    else:
        ## clean
        cleanResolutionFile()
        
        hero = True
        exportGrp = 'Rig_Grp'

    outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=hero)
    outputFile = outputList[0]
    runProcess = True
    processList = [] if runProcess else []
    # print processList, 'processList'
    exportResult = export_utils.export_output(publishInfo, outputList, session_export, outputFile, processList, 'export', exportGrp)
    # print exportResult, 'exportResult'
    register_data(publishInfo.entity, outputDict)

    if publishInfo.entity.process == 'main': 
        if hero: 
            outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=hero, outputKey='rig')
            heroFile = outputDict['heroFile']
            print 123145, heroFile
            session_export(heroFile, processList, 'export', exportGrp)
            register_data(publishInfo.entity, outputDict)

    print "-------------------------------------------------------------------------------------------"
    print "------------------------------------- PUBLISH PROXY IS DONE -------------------------------"
    print "-------------------------------------------------------------------------------------------"

    ## Open File Path
    utaCore.openFileWorldSpace()

    pipeline_utils.write_autoGeneratedRig_data(value=False, entity=asset, res='pr')

    return [exportResult, outputList]

def batch_export(dst, processList, output='save', export=''):
    import maya.cmds as mc
    src = mc.file(q=True, sn=True)
    batch_call.run(src, dst, str(processList), output=output, export=export)
    return dst

def session_export(dst, processList, output='save', export=''):
    import maya.cmds as mc
    src = mc.file(q=True, sn=True)
    return session_call.run(src, dst, processList, output, export)


def register_data(entity, outputDict):
    """ register gpu hero file """
    description = register_entity.Register(entity)
    output = register_entity.Output()
    output.add_process(entity.process)
    hero = outputDict.get('heroFile')
    if not hero: 
        hero = outputDict.get('publishedHeroFile')
    output.add_rig(outputDict['publishedFile'], hero)
    # output.add_rig(outputDict['publishedFile'], outputDict['publishedHeroFile'])
    # print outputDict['publishedHeroFile']
    description.set(output=output)
    return description.write()

def cleanResolutionFile():

    cleanUp.clearDeleteGrp()
    cleanUp.clearLayer();
    cleanUp.clearUnusedAnim();
    cleanUp.clearUnknowNode();
    cleanUp.clearUnusedNode();
    cleanUp.clearVraySettings()
    cleanUp.clearCameraDefault()
    rigTools.scaleGimbalControl()
    cleanUp.clearInheritsTransform()
    cleanUp.checkParameterUV()

    objSets = mc.ls('set_*')
    for each in objSets:
        mc.delete(each)

def selectProcess(process = '', *args):
    processOp = ['Md', 'Lo', 'Pr']
    for each in processOp:
        if each == process:
            pass
        else:
            processGrp = '%sGeo_Grp' % each
            if mc.objExists (processGrp):
                mc.delete(processGrp)
    print '#', '%sGeo_Grp' % process, ': is Ready'

