# template for export mod
import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import custom_exception
from rf_utils.pipeline import local_hook
from rf_utils import register_entity
from rf_utils import file_utils
reload(publish_info)
reload(export_utils)
reload(register_entity)
from rftool.loc_tracker import rivet
reload(rivet)


def run(publishInfo):
    """ export GPU cache file from Geo_Grp. """
    return [True, []]


def export():
    """ export locator """ 
    import maya.cmds as mc
    sels = mc.ls(sl=True)
    assets = list()
    loc_dict = dict()

    # list all assets 
    for sel in sels: 
        if mc.referenceQuery(sel, inr=True): 
            path = mc.referenceQuery(sel, f=True)
            assets.append(path)

    # list all locators 
    locs = rivet.list_trackers()
    for loc in locs: 
        mesh = rivet.find_object(loc)
        if mc.referenceQuery(mesh, inr=True): 
            asset_path = mc.referenceQuery(mesh, f=True)
            entity = context_info.ContextPathInfo(path=asset_path)
            if not entity.name in loc_dict.keys(): 
                loc_dict[entity.name] = [loc]
            else: 
                loc_dict[entity.name].append(loc)

    print loc_dict

    for path in list(set(assets)): 
        data = dict()
        entity = context_info.ContextPathInfo(path=path)
        entity.context.update(process='main', res='md', step='rig')
        data_file = '{}/{}'.format(entity.path.hero().abs_path(), entity.output_name(outputKey='data', hero=True))
        print(data)
        if entity.name in loc_dict.keys(): 
            locs = loc_dict[entity.name]
            for loc in locs: 
                edges = rivet.find_edges(loc)
                data[loc] = edges
            raw_data = file_utils.ymlLoader(data_file)
            raw_data['trackers'] = data
            file_utils.ymlDumper(data_file, raw_data)
            print('write {} - {}'.format(data_file, data))
        else: 
            print('no locator found on {}'.format(entity.name))


def write_data(file, locator_dict): 
    key = 'trackLocator'
    data = file_utils.ymlLoader(file)
    data[key] = locator_dict 
    file_utils.ymlDumper(file, data)

