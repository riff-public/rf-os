# template for export mod
import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_app.export.utils import batch_call
from rf_app.export.utils import session_call
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import register_entity
from rf_utils import daily
import holiday
from rftool.utils import maya_utils
from rf_maya.lib import material_layer 
from rf_utils import file_utils
reload(maya_utils)
reload(publish_info)
reload(session_call)
reload(batch_call)
reload(holiday)

def run(publishInfo):
    """ copy and published this file for next department """
    import maya.cmds as mc 
    from rf_maya.lib import maya_file
    entity = publishInfo.entity.copy()
    entity.context.update(process='holiday')
    # save 
    fileName = mc.file(save=True, f=True)

    looks = material_layer.get_texture_look()

    for look in looks: 
        materialType = material_layer.get_active_material(look)
        # set active 
        material_layer.set_active_layer(look, materialType)

        hero = entity.output_name(outputKey='holidayTexture', hero=True)
        dir = entity.path.hero().abs_path()

        dst = '%s/%s' % (dir, hero)
        if not os.path.exists(dir): 
            os.makedirs(dir)
        # standard output list
        maya_utils.flat_all_references()
        geo = holiday.convert_model(publishInfo.entity)
        replaceDict = copy_texture(entity, geo)

        maya_file.export_selection(dst, [geo], 'mayaAscii')

        daily.submit(entity, [dst])

    # reopen workspace 
    mc.file(fileName, o=True, f=True, prompt=False)

    return [True, [dst]]


def copy_texture(entity, targetGrp): 
    replaceDict = dict()
    textures = maya_utils.collect_textures(targetGrp=targetGrp)
    dailyPath = daily.get_path(entity)
    textureDstDir = '%s/%s' % (dailyPath, entity.name)
    dirs = []
    
    if not os.path.exists(textureDstDir): 
        os.makedirs(textureDstDir)

    for texture in textures: 
        relPath = '%s/%s' % (entity.name, os.path.basename(texture))
        dst = '%s/%s' % (textureDstDir, os.path.basename(texture))
        file_utils.xcopy_file(texture, dst)
        print 'Copy %s' % dst


def session_export(dst, processList, output='export', export=''):
    import maya.cmds as mc
    src = mc.file(q=True, sn=True)
    return session_call.run(src, dst, processList, output, export)