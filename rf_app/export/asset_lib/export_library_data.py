# template for export mod
import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
import pymel.core as pm 
from rf_app.export.utils import export_utils
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import custom_exception
from rf_utils import register_entity
from rf_utils import file_utils
reload(publish_info)
reload(export_utils)


def run(publishInfo):
    """ export Alembic cache file from Geo_Grp. """
    # This export anim data only for type animated which has more than 1 asset in the scene 
    
    if publishInfo.entity.type != 'animated':
        return [context_info.Status.skip, []]
    app = publishInfo.entity.context.app
    geoGrp = publishInfo.entity.projectInfo.asset.geo_grp() or 'Geo_Grp'
    list_asset = pm.ls('*:%s' % geoGrp)
    exportResults = [] 
    allOutputList = []
    pubInfos = []

    for asset_geo_grp in list_asset:
        namespace = asset_geo_grp.namespace()
        # publishInfo.entity.context.update(process=publishInfo.entity.name, entityGrp='char')
        
        ######context asset
        ref_pymel_node = pm.FileReference(namespace = namespace)
        path = ref_pymel_node.path
        asset = context_info.ContextPathInfo(path=path)
        localPublishInfo = publish_info.PreRegister(asset)
        
        ########update entity
        # publishInfo.entity.context.update(entity=asset.name)

        localPublishInfo.entity.context.update(process=publishInfo.entity.name, step='anim', publishVersion=publishInfo.entity.publishVersion, res=publishInfo.entity.res)
        # publishInfo.entity.context.update(app='gpu')

        # track time
        outputList, outputDict = export_utils.publish_output_list(localPublishInfo, hero=True, outputKey='cache')
        allOutputList += outputList
        pubInfos.append(localPublishInfo)

    outputPath = '%s/%s' % (publishInfo.entity.path.hero().abs_path(), publishInfo.entity.output_name(outputKey='refData', hero=True))
    result = export_library_data(outputPath, pubInfos)
    return [result, [outputPath]]


def export_library_data(outputPath, pubInfos):
    libData = dict()

    for publishInfo in pubInfos: 
        heroPath = publishInfo.entity.path.hero().abs_path()
        cacheFile = publishInfo.entity.output_name(outputKey='cache', hero=True)
        output = '%s/%s' % (heroPath, cacheFile)
        libData.update({publishInfo.entity.name: output})

    if not os.path.exists(os.path.dirname(outputPath)): 
        os.makedirs(os.path.dirname(outputPath))
        
    file_utils.ymlDumper(outputPath, libData)
    return True if os.path.exists(outputPath) else False 
