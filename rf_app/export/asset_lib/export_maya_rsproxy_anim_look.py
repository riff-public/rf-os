# template for export mod
import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import custom_exception
from rf_utils.pipeline import local_hook
from rf_utils import register_entity
from rftool.render.redshift import rs_utils
from rf_app.export.utils import batch_call
from rf_app.export.utils import session_call
from rf_maya.lib import material_layer 
from rf_utils import file_utils
reload(material_layer)
reload(publish_info)
reload(export_utils)
reload(register_entity)
reload(rs_utils)
reload(file_utils)

reload(session_call)



def run(publishInfo):
    """ export GPU cache file from Geo_Grp. """
    assetType = publishInfo.entity.type
    looks = material_layer.get_lookdev_look()
    export = True
    allResult = []
    allOutputs = []
    if assetType in ['animated']:
        export = True

    if export and looks:
        for look in looks: 
            # look manage 
            # publishInfo.entity.context.update(look=look, process=look, step='lookdev')
            publishInfo.entity.context.update(look=look, process=publishInfo.entity.process, step='anim')
            # track time
            outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=True, outputKey='mayarsproxySq')
            rsOutputList, rsOutputDict = export_utils.publish_output_list(publishInfo, hero=False, outputKey='rsproxySq')
            # add hero back to last list 
            rsOutputList.append(rsOutputList[1])
            results = []

            for i, output in enumerate(outputList):
                rsproxyFile = rsOutputList[i]
                sequenceInfo = file_utils.get_sequence_info(rsproxyFile)
                if sequenceInfo:
                    result = export_maya_rsproxy2(publishInfo, output, rsproxyFile)
                    results.append(True) if os.path.exists(output) else results.append(False)
                else:
                    results.append(False)

            allResult += results
            allOutputs += outputList

            # write to register
            register_data(publishInfo.entity, outputDict)
        print allResult
        return [all(allResult), allOutputs]

    else:
        logger.info('Skip export Gpu')
        return [context_info.Status.skip, []]


def export_maya_rsproxy(publishInfo, outputPath, rsproxyFile) :
    """ export gpu cache """
    import maya.cmds as mc
    node, shape, transform = rs_utils.create_proxy(nodeName=publishInfo.entity.name, proxyPath=rsproxyFile, displayMode=0, displayPercentage=100, force=True, useSequence=1)
    rigGrp = publishInfo.entity.projectInfo.asset.top_grp()
    geoGrp = publishInfo.entity.projectInfo.asset.geo_grp()

    geoGrp = mc.group(transform, n=geoGrp)
    rigGrp = mc.group(geoGrp, n=rigGrp)
    session_export(outputPath, [], output='export', export=rigGrp)
    mc.delete(rigGrp)
    return outputPath


def export_maya_rsproxy2(publishInfo, outputPath, rsproxyFile): 
    templateFile = publishInfo.entity.projectInfo.asset.global_asset(key='rsTemplate')
    searchStr = publishInfo.entity.projectInfo.asset.path_dummy()
    outputPath = file_utils.copy(templateFile, outputPath)

    with open(outputPath, 'r') as hf:
        data = hf.read()
            
    data = data.replace(searchStr, rsproxyFile)
    with open(outputPath, 'w') as hf:
        hf.write(data)

    return outputPath



def register_data(entity, outputDict):
    """ register gpu hero file """
    description = register_entity.Register(entity)
    output = register_entity.Output()
    output.add_look(entity.process)
    output.add_maya_rsproxy(outputDict['publishedFile'], outputDict['publishedHeroFile'])
    description.set(output=output)
    return description.write()

def batch_export(dst, processList, output='save', export=''):
    import maya.cmds as mc
    src = mc.file(q=True, sn=True)
    batch_call.run(src, dst, str(processList), output=output, export=export)
    return dst

def session_export(dst, processList, output='save', export=''):
    import maya.cmds as mc
    src = mc.file(q=True, sn=True)
    return session_call.run(src, dst, processList, output, export)
