# template for export mod
import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import custom_exception
from rf_utils.pipeline import local_hook
from rf_utils import register_entity
from rftool.render.arnold import arnold_utils
from rf_maya.lib import material_layer 
reload(material_layer)
reload(publish_info)
reload(export_utils)
reload(register_entity)
reload(arnold_utils)



def run(publishInfo):
    """ export GPU cache file from Geo_Grp. """
    assetType = publishInfo.entity.type
    looks = material_layer.get_lookdev_look() # try to get lookdev first 
    allOutputs = []
    results = []
    export = False
    
    if assetType in ['prop']:
        export = True

    if export and looks:
        for look in looks: 
            # look manage 
            publishInfo.entity.context.update(look=look, process=look)
            materialType = material_layer.get_active_material(look)
            # set active 
            material_layer.set_active_layer(look, materialType)
            
            # track time
            outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=True, outputKey='aistandin')

            geoGrp = publishInfo.entity.projectInfo.asset.geo_grp()
            outputFile = outputList[0]
            exportGrp = export_grp(publishInfo.entity)
            exportResult = export_utils.export_output(publishInfo, outputList, export_astandin, outputFile, geoGrp, exportGrp)

            # write to register
            register_data(publishInfo.entity, outputDict)

            # result collects 
            allOutputs += outputList
            results.append(exportResult)

        results = all(results)
        return [results, allOutputs]

    else:
        logger.info('Skip export Gpu')
        return [context_info.Status.skip, []]



def export_astandin(outputPath, geoGrp, exportGrp) :
    """ export gpu cache """
    import maya.cmds as mc
    geoGrp = mc.ls('*:%s' % geoGrp, l=True)
    geoGrp = [a for a in geoGrp if exportGrp in a]
    targetGrp = geoGrp[0] if geoGrp else None

    if targetGrp:
        dupGrp = mc.duplicate(targetGrp)
        arnold_utils.export_aiStandIn(outputPath, dupGrp[0], animation=0, start=0, end=0)
        mc.delete(dupGrp[0])
        return outputPath

    else:
        raise custom_exception.PipelineError('Export Group %s Not found' % targetGrp)
        return context_info.Status.skip


def register_data(entity, outputDict):
    """ register gpu hero file """
    description = register_entity.Register(entity)
    output = register_entity.Output()
    output.add_look(entity.process)
    output.add_rsproxy(outputDict['publishedFile'], outputDict['publishedHeroFile'])
    description.set(output=output)
    return description.write()

def export_grp(entity): 
    import maya.cmds as mc
    exportGrp = entity.projectInfo.asset.export_grp(entity.res)
    geoGrp = entity.projectInfo.asset.geo_grp()
    targetGrps = mc.ls('*:%s' % geoGrp, l=True)
    targetGrps = [a for a in targetGrps if exportGrp in a]

    return targetGrps[0] if targetGrps else None
