# template for export mod
import os 
import sys 
import shutil
import logging 

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_utils import publish_info 
from rf_utils.context import context_info
from rf_utils import custom_exception
reload(publish_info)
reload(export_utils)


def run(publishInfo): 
    """ main function to to run for output_widget """
    app = publishInfo.entity.context.app
    process = publishInfo.entity.context.process
    publishInfo.entity.context.update(app='gpu')

    # track time 
    outputList = export_utils.publish_output_list(publishInfo, hero=True, outputKey='gpu')
    publishInfo.entity.context.update(app=app) # restore

    exportGrp = publishInfo.entity.projectInfo.asset.geo_grp()
    outputFile = outputList[0]
    exportResult = export_utils.export_output(publishInfo, outputList, export_gpu_still, outputFile, exportGrp)
    return [exportResult, outputList]


def export_gpu_still(outputPath, exportGrp) : 
    """ export gpu cache """
    import maya.cmds as mc 
    gpuCachePlugin = 'gpuCache.mll'
    
    if not mc.pluginInfo(gpuCachePlugin, q=True, l=True): 
        mc.loadPlugin(gpuCachePlugin, qt=True)

    # code below 
    currentFrame = mc.currentTime(q = True)
    startFrame = currentFrame
    endFrame = currentFrame 

    exportDir = os.path.dirname(outputPath)
    filename = os.path.splitext(os.path.basename(outputPath))[0]

    # export objs 
    if mc.objExists(exportGrp) : 
        mc.select(exportGrp) 
        result = mc.gpuCache(exportGrp, 
                                startTime = startFrame, 
                                endTime = endFrame, 
                                optimize = True, 
                                optimizationThreshold = 40000, 
                                writeMaterials = True, 
                                dataFormat = 'ogawa', 
                                directory = exportDir, 
                                fileName = filename, 
                                saveMultipleFiles = False
                                )

        return outputPath 

    else: 
        raise custom_exception.PipelineError('No %s found' % exportGrp)
