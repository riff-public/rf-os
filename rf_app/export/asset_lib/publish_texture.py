# template for export mod
import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_utils import publish_info
from rf_utils.context import context_info
from rftool.shade import shade_utils
from rf_utils import custom_exception
reload(publish_info)
reload(export_utils)


def run(publishInfo):
    """ Publish textures, resize, convert tx and copy to publish areas """
    process = publishInfo.entity.context.process
    texturePath = publishInfo.entity.path.publish_texture()
    previewPath = publishInfo.entity.path.preview_texture()
    # track time
    outputList = export_utils.publish_output_list(publishInfo, hero=True, outputKey='material')

    exportGrp = publishInfo.entity.projectInfo.asset.geo_grp()
    outputFile = outputList[0]
    exportResult = export_utils.export_output(publishInfo, outputList, export_material, outputFile, exportGrp)
    return [exportResult, outputList]


def export_material(outputPath, exportGrp) :
    """ export gpu cache """
    import maya.cmds as mc
    if not mc.objExists(exportGrp):
        raise custom_exception.PipelineError('No %s found' % exportGrp)

    # result = shade_utils.export_shade(outputPath, targetGrp=exportGrp)
    result = shade_utils.export_shade_with_data(outputPath, targetGrp=exportGrp)
    # shade_utils.export_shade_data(dataPath, targetGrp=None)
    if not result:
        raise custom_exception.PipelineError('Failed to export material')
    return result
