# template for export mod
import os
import sys
import shutil
import logging
import maya.cmds as mc
import maya.mel as mm 

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
#from rf_app.export.utils import batch_call
from rf_maya.rftool.rig.export_config import batchUE_call
reload(batchUE_call)
from rf_app.export.utils import session_call
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import register_entity
    
def run(publishInfo, runProcess=True):
    ''' Export rig file of resolution Md as both .ma and .mb '''
    asset = publishInfo.entity
    charName = asset.name
    projectName = asset.project 
    assetType = asset.type 
    res = asset.res
    task = asset.process
    print projectName
    print res
    print task
    exportGrp = 'Export_Grp'
    hero = True
    publishInfo.entity.context.update(look = task)
    outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=hero , outputKey = 'fbxUE')
    outputFile = outputList[0]
    runProcess = True
    processList = [] if runProcess else []
    exportResult = export_utils.export_output(publishInfo, outputList, batch_export, outputFile, processList,'export', exportGrp, projectName ,res , task)
    register_data(publishInfo.entity, outputDict)

    return [exportResult, outputList]


def batch_export(outputFile, processList, output='save', export='', projectName='', res='' , task=''):
    src = mc.file(q=True, sn=True)
    batchUE_call.run(src, outputFile, str(processList), output=output, export=export, project = projectName, res = res , task = task)
    return outputFile


def session_export(dst, processList, output='save', export=''):
    import maya.cmds as mc
    src = mc.file(q=True, sn=True)
    result = session_call.run(src, dst, processList, output, export)
    return ascl.run_clean(dst)


def register_data(entity, outputDict):
    description = register_entity.Register(entity)
    output = register_entity.Output()
    output.add_process(entity.process)
    hero = outputDict.get('heroFile')
    if not hero: 
        hero = outputDict.get('publishedHeroFile')
    output.add_rig(outputDict['publishedFile'], hero)
    description.set(output=output)
    return description.write()


def find_export_grp( expGrp = 'Export_Grp' ):
    listConnect = mc.listConnections(expGrp,p=True,c=True)
    #delete connection from export grp
    if listConnect:
        for i in range(0,len(listConnect)-1,2):
            try: 
                mc.disconnectAttr(listConnect[i], listConnect[i+1])
            except Exception as e:
                print e

    return mc.listRelatives( expGrp , c = True )
