# template for export mod
import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_app.export.utils import batch_call
from rf_app.export.utils import session_call
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import register_entity

reload(publish_info)
reload(session_call)
reload(batch_call)

def run(publishInfo):
    """ copy and published this file for next department """
    # standard output list
    hero = True if publishInfo.entity.step in ['model', 'rig'] else False
    outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=hero)
    outputFile = outputList[0]
    processList = ['import_ref', 'remove_namespaces']
    
    res = publishInfo.entity.res
    exportGrp = publishInfo.entity.projectInfo.asset.geo_grp()
    contentGrp = export_utils.get_geo_grp(publishInfo.entity, res)
    run = True if publishInfo.entity.step in ['model'] else False 
    
    with export_utils.maya_utils().IsolateGeo(exportGrp, contentGrp, run=run): 
        exportResult = export_utils.export_output(publishInfo, outputList, session_export, outputFile, processList, 'export', exportGrp)
    
    register_data(publishInfo.entity, outputDict)
    return [exportResult, outputList]

def register_data(entity, outputDict):
    """ register gpu hero file """
    description = register_entity.Register(entity)
    output = register_entity.Output()
    output.add_process(entity.process)
    output.add_maya(outputDict['publishedFile'], outputDict['publishedHeroFile'])
    description.set(output=output)
    return description.write()

def batch_export(dst, processList, output='save', export=''):
    import maya.cmds as mc
    src = mc.file(q=True, sn=True)
    batch_call.run(src, dst, str(processList), output=output, export=export)
    return dst

def session_export(dst, processList, output='save', export=''):
    import maya.cmds as mc
    src = mc.file(q=True, sn=True)
    return session_call.run(src, dst, processList, output, export)