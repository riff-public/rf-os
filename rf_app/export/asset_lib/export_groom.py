# template for export mod
# 22-09-01 change export type to exportNoShade. No need to create temp layer anymore

import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_app.export.utils import batch_call
from rf_app.export.utils import session_call
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import register_entity
from rftool.utils import yeti_lib
from rf_utils import file_utils
reload(yeti_lib)
reload(register_entity)

reload(publish_info)
reload(session_call)
reload(batch_call)
reload(context_info)

def run(publishInfo):
    """ copy and published this file for next department """
    # standard output list
    hero = False
    outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=hero, outputKey='yeti')
    outputFile = outputList[0]
    processList = []
    exportGrp = 'Yeti_Grp'
    exportResult = export_utils.export_output(publishInfo, outputList, export, outputFile, processList, publishInfo, 'exportNoShade', exportGrp)
    register_data(publishInfo.entity, outputDict)

    return [exportResult, outputList]


def export(dst, processList, publishInfo, output='save', export=''):
    import maya.cmds as mc
    src = mc.file(q=True, sn=True)
    publishTexture = publishInfo.entity.path.publish_groom_texture().abs_path()
    replaceDict = yeti_lib.move_yeti_texture_path(publishTexture)
    # replaceDict = maya_utils.move_texture_path(publishTexture, exportGrp, resize=False, relink=False)
    # currentLayer, tmpLayer = add_temp_layer(export)
    result = session_call.run(src, dst, processList, output, export)
    # restore_layer(currentLayer, tmpLayer)
    return result 
    # print '--------------', replaceDict
    # file_utils.search_replace_keys(dst, replaceDict, backupFile=False)
    # return dst 

def register_data(entity, outputDict):
    """ register gpu hero file """
    description = register_entity.Register(entity)
    output = register_entity.Output()
    output.add_look(entity.process)
    output.add_groom_yeti(outputDict['publishedFile'], outputDict['publishedHeroFile'])
    description.set(output=output)
    return description.write()


def add_temp_layer(exportGrp): 
    import maya.cmds as mc
    tmpLayer = 'tmpLayerOverride'
    current = mc.editRenderLayerGlobals(q=True, currentRenderLayer=True)
    if mc.objExists(tmpLayer): 
        mc.delete(tmpLayer)
    tmpLayer = mc.createRenderLayer(exportGrp, n=tmpLayer, number=1, noRecurse=True)
    mc.editRenderLayerGlobals(currentRenderLayer=tmpLayer)

    # assign lambert1
    mc.select(exportGrp)
    mc.hyperShade(assign='lambert1')

    return current, tmpLayer

def restore_layer(currentLayer, tmpLayer): 
    import maya.cmds as mc
    mc.editRenderLayerGlobals(currentRenderLayer=currentLayer)
    mc.delete(tmpLayer) if mc.objExists(tmpLayer) else None 



