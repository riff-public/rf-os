import os
import sys
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.file_manager.ftp import model
reload(model)

def run(publishInfo):
    """ sync asset """
    sync_result = model.upload_asset(publishInfo.entity)
    return [sync_result, list()]



