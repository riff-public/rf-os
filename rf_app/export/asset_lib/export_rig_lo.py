# template for export mod
import os
import sys
import shutil
import logging
import maya.cmds as mc

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_app.export.utils import batch_call
from rf_app.export.utils import session_call
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import register_entity
import rftool.rig
from utaTools.utapy import utaCore
from utaTools.utapy import utaAttrWriteRead
from utaTools.utapy import cleanUp
from utaTools.pkrig import rigTools
from utaTools.utapy import utaTagNameConnectWriteRead as tagNameConWR
from rf_maya.rftool.asset import pullUV
from rf_maya.nuTools.util import bshUtil
from rf_maya.ncscript.util import cleanUpTools as clean

reload(bshUtil)
reload(pullUV)
reload(tagNameConWR)
reload(rigTools)
reload(cleanUp)
reload(utaCore)
reload(publish_info)
reload(session_call)
reload(batch_call)
reload(utaAttrWriteRead)


def run(publishInfo, runProcess=True):
    # from rf_utils.context import context_info
    asset = context_info.ContextPathInfo() 
    charName = asset.name
    """ copy and published this file for next department """
    # standard output list
    if publishInfo.entity.process in [  'main', 
                                        'combRig'] :
        ## clean
        cleanResolutionFile()
        cleanUp.OnOffVisibilityObj()
        cleanUp.cleanShade()
        cleanUp.lockAllMover()   
        utaCore.delete_unused_skin()
        cleanUp.deleteGrp (DelGrp = ['YetiNode_Grp'])

        if publishInfo.entity.type == 'char':
            print "# Generate >> Check Pr or Md or Lo"
            selectProcess(process = 'Lo')
            print "# Generate >> Check Char Check TagName"
            tagNameConWR.tagNameConnectionWrite()
            print "# Generate >> Add 'TechGeo_Grp' "
            utaCore.createTechGeo_Grp()
            print "# Generate >> SetAttr Control Layer Default"
            utaCore.setDefaultControl(char = charName)

        elif publishInfo.entity.type == 'prop':
            ## Check Pr or Md
            selectProcess(process = 'Lo')
        else:
            ## Check Pr or Md
            selectProcess(process = '')

        # Attr Write, tagNameConnectionWrite
        utaAttrWriteRead.attrWrite()

        ## Check UV From Model
        pullUVFile(publishInfo.entity)

        hero = True
        exportGrp = 'Rig_Grp'


    elif publishInfo.entity.process in ['skel', 
                                        'hdRigTmpLoc', 
                                        'nsRigTmpLoc', 
                                        'eyeRigTmpLoc', 
                                        'earRigTmpLoc', 
                                        'mthRigTmpLoc', 
                                        'dtlRigTmpLoc', 
                                        'ebRigTmpLoc', 
                                        'ttRigTmpLoc'] :
        # clean
        cleanResolutionFile()
        hero = False
        exportGrp = 'Export_Grp'

    elif publishInfo.entity.process in [    'ctrl', 
                                            'bodyRig']:
        # clean
        cleanResolutionFile()
        cleanUp.OnOffVisibilityObj()

        hero = False
        exportGrp = 'Rig_Grp'

    elif publishInfo.entity.process in [    'hdRigCtrl', 
                                            'nsRigCtrl', 
                                            'eyeRigCtrl', 
                                            'earRigCtrl', 
                                            'mthRigCtrl', 
                                            'dtlRigCtrl', 
                                            'ebRigCtrl', 
                                            'ttRigCtrl', 
                                            'fclRig', 
                                            'bsh']:
        # clean
        cleanResolutionFile()

        processListsFiel = publishInfo.entity.process
        processRigs = []

        processName = processListsFiel.split('Rig')
        processRigs.append(processName[0])
        processObj = processRigs[0].capitalize()
        exportGrp = []
        if publishInfo.entity.process == 'hdRigCtrl':
            hero = False
            #List Export
            CtrlGrp = (processObj + 'DfmRigCtrl_Grp')
            SkinGrp = (processObj + 'DfmRigSkin_Grp')
            StillGrp = (processObj + 'DfmRigStill_Grp')
            JntGrp = (processObj + 'DfmRigJnt_Grp')

            if mc.objExists(CtrlGrp):
                exportGrp.append(CtrlGrp)
            else:
                print "Please Check >>" , CtrlGrp
            if mc.objExists(SkinGrp):
                exportGrp.append(SkinGrp)
            else:
                print "Please Check >>" , SkinGrp
            if mc.objExists(StillGrp):
                exportGrp.append(StillGrp)
            else:
                print "Please Check >>" , StillGrp
            if mc.objExists(JntGrp):
                exportGrp.append(JntGrp)
            else:
                print "Please Check >>" , JntGrp
        else:
            hero=False
            #List Export
            CtrlGrp = (processObj + 'RigCtrl_Grp')
            SkinGrp = (processObj + 'RigSkin_Grp')
            StillGrp = (processObj + 'RigStill_Grp')
            JntGrp = (processObj + 'RigJnt_Grp')
            RigGrp = ('Rig_Grp')
            FclRig = ('FacailRig_Grp')
            BshGeoRig = (processObj + 'RigGeo_Grp')

            if mc.objExists(CtrlGrp):
                exportGrp.append(CtrlGrp)
            else:
                print "Please Check >>" , CtrlGrp
            if mc.objExists(SkinGrp):
                exportGrp.append(SkinGrp)
            else:
                print "Please Check >>" , SkinGrp
            if mc.objExists(StillGrp):
                exportGrp.append(StillGrp)
            else:
                print "Please Check >>" , StillGrp
            if mc.objExists(JntGrp):
                exportGrp.append(JntGrp)
            else:
                print "Please Check >>" , JntGrp
            if mc.objExists(RigGrp):
                exportGrp.append(RigGrp)
            else:
                print "Please Check >>" , RigGrp
            if mc.objExists(BshGeoRig):
                exportGrp.append(BshGeoRig)
            else:
                print "Please Check >>" , BshGeoRig
            if mc.objExists(FclRig):
                exportGrp.append(FclRig)
            else:
                print "Please Check >>" , FclRig
            # print processObj
    elif publishInfo.entity.process in ['hdRig', 
                                        'nsRig', 
                                        'eyeRig', 
                                        'earRig', 
                                        'mthRig', 
                                        'dtlRig', 
                                        'ebRig', 
                                        'addRig', 
                                        'ttRig', 
                                        'hdGeo', 
                                        'volCompRig', 
                                        'clothRig']:
        # clean
        cleanResolutionFile()

        processListsFiel = publishInfo.entity.process
        processRigs = []
        processName = processListsFiel.split('Rig')
        processRigs.append(processName[0])
        processObj = processRigs[0].capitalize()
        # print processObj, 'processObj'
        hero = False
        exportGrp = []
        if publishInfo.entity.process == 'hdGeo':
            #List Export
            fclRigGeo = 'FclRigGeo_Grp'
            if mc.objExists(fclRigGeo):
                exportGrp.append(fclRigGeo)
            else:
                print "Please Check >> ", fclRigGeo

        elif publishInfo.entity.process == 'dtlRig' or publishInfo.entity.process == 'addRig' or publishInfo.entity.process == 'clothRig':
            RigCtrl = (processObj +'RigCtrl_Grp')
            RigSkin = (processObj +'RigSkin_Grp')
            # RigStill = (processObj +'RigStill_Grp')
            RigJnt = (processObj +'RigJnt_Grp')
            RigGeo = (processObj +'RigGeo_Grp')
            RigStill = (processObj +'RigStill_Grp')
            RigHi = (processObj +'RigCtrlHi_Grp')
            RigMid = (processObj +'RigCtrlMid_Grp')
            RigLow = (processObj +'RigCtrlLow_Grp')

            # Clear SecmentCompense 
            cleanUp.clearTturnOff()
            if mc.objExists(RigCtrl):
                exportGrp.append(RigCtrl)
            else:
                print "Please Check >> ", RigCtrl
            if mc.objExists(RigSkin):
                exportGrp.append(RigSkin)
            else:
                print "Please Check >> ", RigSkin
            if mc.objExists(RigStill):
                exportGrp.append(RigStill)
            else:
                print "Please Check >> ", RigStill
            if mc.objExists(RigJnt):
                exportGrp.append(RigJnt)
            else:
                print "Please Check >> ", RigJnt
            if mc.objExists(RigGeo):
                exportGrp.append(RigGeo)
            else:
                print "Please Check >> ", RigGeo
            if mc.objExists(RigHi):
                mc.select(RigHi)
                cleanUp.clearInheritsTransformObj()
                exportGrp.append(RigHi)
            else:
                print "Please Check >> ", RigHi
            if mc.objExists(RigMid):
                mc.select(RigMid)
                cleanUp.clearInheritsTransformObj()
                exportGrp.append(RigMid)
            else:
                print "Please Check >> ", RigMid
            if mc.objExists(RigLow):
                mc.select(RigLow)
                cleanUp.clearInheritsTransformObj()
                exportGrp.append(RigLow)
            else:
                print "Please Check >> ", RigLow                                          


        elif publishInfo.entity.process == 'hdRig':
            #List Export
            CtrlGrp = (processObj + 'DfmRigCtrl_Grp')
            SkinGrp = (processObj + 'DfmRigSkin_Grp')
            StillGrp = (processObj + 'DfmRigStill_Grp')
            JntGrp = (processObj + 'DfmRigJnt_Grp')
            if mc.objExists(CtrlGrp):
                exportGrp.append(CtrlGrp)
            else:
                print "Please Check >> ", CtrlGrp
            if mc.objExists(SkinGrp):
                exportGrp.append(SkinGrp)
            else:
                print "Please Check >> ", SkinGrp
            if mc.objExists(StillGrp):
                exportGrp.append(StillGrp)
            else:
                print "Please Check >> ", StillGrp
            if mc.objExists(JntGrp):
                exportGrp.append(JntGrp)
            else:
                print "Please Check >> ", JntGrp

        elif publishInfo.entity.process == 'volCompRig':
            compSp= processObj.split("Vol")
            volSp= processObj.split("comp")
            compCapObj = compSp[-1].capitalize()
            processObj = '%s%s'% (volSp[0],compCapObj)
            #List Export
            CtrlGrp = (processObj + 'RigCtrl_Grp')
            SkinGrp = (processObj + 'RigSkin_Grp')
            StillGrp = (processObj + 'RigStill_Grp')
            JntGrp = (processObj + 'RigJnt_Grp')
            addRig = (processObj + 'RigGeo_Grp')
            if mc.objExists(CtrlGrp):
                exportGrp.append(CtrlGrp)
            else:
                print "Please Check >> ", CtrlGrp
            if mc.objExists(SkinGrp):
                exportGrp.append(SkinGrp)
            else:
                print "Please Check >> ", SkinGrp
            if mc.objExists(StillGrp):
                exportGrp.append(StillGrp)
            else:
                print "Please Check >> ", StillGrp
            if mc.objExists(JntGrp):
                exportGrp.append(JntGrp)
            else:
                print "Please Check >> ", JntGrp


        else:
            #List Export
            CtrlGrp = (processObj + 'RigCtrl_Grp')
            SkinGrp = (processObj + 'RigSkin_Grp')
            StillGrp = (processObj + 'RigStill_Grp')
            JntGrp = (processObj + 'RigJnt_Grp')
            addRig = (processObj + 'RigGeo_Grp')
            # print StillGrp, "StillGrp"
            if mc.objExists(CtrlGrp):
                exportGrp.append(CtrlGrp)
            else:
                print "Please Check >> ", CtrlGrp
            if mc.objExists(SkinGrp):
                exportGrp.append(SkinGrp)
            else:
                print "Please Check >> ", SkinGrp
            if mc.objExists(StillGrp):
                exportGrp.append(StillGrp)
            else:
                print "Please Check >> ", StillGrp
            if mc.objExists(JntGrp):
                exportGrp.append(JntGrp)
            else:
                print "Please Check >> ", JntGrp
            if mc.objExists(addRig):
                exportGrp.append(addRig)
            else:
                print "Please Check >> ", addRig
    else:
        ## clean
        cleanResolutionFile()

        ## Process Check
        processListsFiel = publishInfo.entity.process
        processRigs = []
        processName = processListsFiel.split('Rig')
        processRigs.append(processName[0])
        processObj = processRigs[0].capitalize()
        exportGrp = []

        RigCtrl = (processObj +'RigCtrl_Grp')
        RigSkin = (processObj +'RigSkin_Grp')
        RigStill = (processObj +'RigStill_Grp')
        RigJnt = (processObj +'RigJnt_Grp')
        RigGeo = (processObj +'RigGeo_Grp')
        RigHi = (processObj +'RigCtrlHi_Grp')
        RigMid = (processObj +'RigCtrlMid_Grp')
        RigLow = (processObj +'RigCtrlLow_Grp')

        # Clear SecmentCompense 
        hero = True
        cleanUp.clearTturnOff()
        if mc.objExists(RigCtrl):
            exportGrp.append(RigCtrl)
        else:
            print "Please Check >> ", RigCtrl
        if mc.objExists(RigSkin):
            exportGrp.append(RigSkin)
        else:
            print "Please Check >> ", RigSkin
        if mc.objExists(RigStill):
            exportGrp.append(RigStill)
        else:
            print "Please Check >> ", RigStill
        if mc.objExists(RigJnt):
            exportGrp.append(RigJnt)
        else:
            print "Please Check >> ", RigJnt
        if mc.objExists(RigGeo):
            exportGrp.append(RigGeo)
        else:
            print "Please Check >> ", RigGeo
        if mc.objExists(RigHi):
            mc.select(RigHi)
            cleanUp.clearInheritsTransformObj()
            exportGrp.append(RigHi)
        else:
            print "Please Check >> ", RigHi
        if mc.objExists(RigMid):
            mc.select(RigMid)
            cleanUp.clearInheritsTransformObj()
            exportGrp.append(RigMid)
        else:
            print "Please Check >> ", RigMid
        if mc.objExists(RigLow):
            mc.select(RigLow)
            cleanUp.clearInheritsTransformObj()
            exportGrp.append(RigLow)
        else:
            print "Please Check >> ", RigLow
        if mc.objExists ('Rig_Grp'):
            exportGrp = 'Rig_Grp'
        else:
            print "Please Check >> ", 'Rig_Grp'
        if mc.objExists ('Export_Grp'):
            exportGrp = 'Export_Grp'      
        else:
            print "Please Check >> ", 'Export_Grp'
    # print '3k'
    outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=hero)
    # print outputList, outputDict, 'xx'
    outputFile = outputList[0]
    runProcess = True
    processList = [] if runProcess else []
    exportResult = export_utils.export_output(publishInfo, outputList, session_export, outputFile, processList, 'export', exportGrp)
    register_data(publishInfo.entity, outputDict)

    # hero mb override 
    # only main 
    if publishInfo.entity.process == 'main': 
        if hero: 
            outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=hero, outputKey='rig')
            heroFile = outputDict['heroFile']
            session_export(heroFile, processList, 'export', exportGrp)
            register_data(publishInfo.entity, outputDict)

    print "-------------------------------------------------------------------------------------------"
    print "------------------------------------- PUBLISH MEDIUM IS DONE ------------------------------"
    print "-------------------------------------------------------------------------------------------"
    ## Open File Path
    utaCore.openFileWorldSpace()

    return [exportResult, outputList]


def batch_export(dst, processList, output='save', export=''):
    import maya.cmds as mc
    src = mc.file(q=True, sn=True)
    batch_call.run(src, dst, str(processList), output=output, export=export)
    return dst

def session_export(dst, processList, output='save', export=''):
    import maya.cmds as mc
    src = mc.file(q=True, sn=True)
    return session_call.run(src, dst, processList, output, export)


def register_data(entity, outputDict):
    """ register gpu hero file """
    description = register_entity.Register(entity)
    output = register_entity.Output()
    output.add_process(entity.process)
    hero = outputDict.get('heroFile')
    if not hero: 
        hero = outputDict.get('publishedHeroFile')
    output.add_rig(outputDict['publishedFile'], hero)
    description.set(output=output)
    return description.write()

def cleanResolutionFile():
    # cleanUp
    print "# Generate >> Delete 'Delete_Grp'"
    cleanUp.clearDeleteGrp()
    print "# Generate >> Delete 'Layer'"
    cleanUp.clearLayer();
    print "# Generate >> Delete 'UnuseAnim'"
    cleanUp.clearUnusedAnim();
    print "# Generate >> Delete 'Unknownode'"
    cleanUp.clearUnknowNode();
    print "# Generate >> Delete 'UnusedNode'"
    cleanUp.clearUnusedNode();
    print "# Generate >> Delete 'VraySettings'"
    cleanUp.clearVraySettings()
    print "# Generate >> Delete 'Camera'"
    cleanUp.clearCameraDefault()
    print '# Generate >> Scale Gimbal'
    rigTools.scaleGimbalControl()
    # print "# Generate >> TurnOff 'Segment Scale Compensate'"
    # cleanUp.clearTturnOff()
    print "# Generate >> TurnOff 'Inherits Transform'"
    cleanUp.clearInheritsTransform()
    print "# Generate >> turnOffBshEdits"
    bshUtil.turnOffBshEdits()
    
    print "# Generate >> Delete Sets Unuse"
    objSets = mc.ls('set_*')
    for each in objSets:
        mc.delete(each)



def selectProcess(process = '', *args):
    processOp = ['Md', 'Lo', 'Pr']
    for each in processOp:
        if each == process:
            pass
        else:
            processGrp = '%sGeo_Grp' % each
            if mc.objExists (processGrp):
                mc.delete(processGrp)
    print '#', '%sGeo_Grp' % process, ': is Ready'

def pullUVFile(entity=None, *args):
    pullUVObj = pullUV.toRig(entity=entity)
    if pullUVObj:
        print "# Generate >> Pull UV Is DONE"
