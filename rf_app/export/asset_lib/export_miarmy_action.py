# template for export mod
import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_utils.miarmy import miarmy_utils

from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import register_entity

reload(publish_info)
reload(miarmy_utils)


def run(publishInfo, runProcess=True):
    """ Export Miarmy Action Loop """
    actions = miarmy_utils.list_actions()
    currentProcess = publishInfo.entity.process 
    outputLists = []
    exportResults = []

    if actions: 
        for action in actions: 
            name = miarmy_utils.get_action_name(action)
            publishInfo.entity.context.update(process='action', look=name)
            outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=True, outputKey='miarmyAction')
            
            exportGrp = action
            outputFile = outputList[0]
            processList = []
            exportResult = False
            
            exportResult = export_utils.export_output(publishInfo, outputList, export_action, outputFile, exportGrp)
            register_data(publishInfo.entity, outputDict)
            outputLists += outputList
            exportResults.append(exportResult)

        return [all(exportResults), outputLists]
    else: 
        return [context_info.Status.skip, []]


def export_action(dst, exportGrp):
    from rf_maya.lib import maya_file
    name, ext = os.path.splitext(dst)
    fileType = 'mayaAscii' if ext == '.ma' else 'mayaBinary' if ext == '.mb' else 'mayaAscii'
    result = maya_file.export_selection(dst, [exportGrp], fileType)
    return result


def register_data(entity, outputDict):
    """ register gpu hero file """
    description = register_entity.Register(entity)
    output = register_entity.Output()
    output.add_process(entity.process)
    output.add_rig(outputDict['publishedFile'], outputDict['publishedHeroFile'])
    description.set(output=output)
    return description.write()
