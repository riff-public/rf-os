# template for export mod
import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils.context import context_info
from rf_utils import custom_exception
from rf_maya.lib import material_layer 
from rf_utils import file_utils
reload(material_layer)
reload(file_utils)

import maya.cmds as mc



def run(publishInfo):
    """ export GPU cache file from Geo_Grp. """
    assetType = publishInfo.entity.type
    # looks = material_layer.get_lookdev_look()
    export = True
    allResult = []
    allOutputs = []
    if assetType in ['animated']:
        export = True

    dictData = {}

    costumeList = mc.ls('*:Costume*Geo_Grp')
    hairList =  mc.ls('*:Hair*Geo_Grp')
    # for costume in costumeList:
    #     print mc.listRelatives(costume,allDescendents=True)
        

    # for hair in hairList:
    #     print mc.listRelatives(hair,allDescendents=True)

    if costumeList and hairList:
        for costume in costumeList:
            for hair in hairList:
                costumeGrpName = costume.split(':')[-1]
                costumeName = costumeGrpName.split('Geo_Grp')[0]
                hairGrpName = hair.split(':')[-1]
                hairName = hairGrpName.split('Geo_Grp')[0]
                look = '{}{}'.format(costumeName, hairName)
                # look manage 
                # publishInfo.entity.context.update(look=look, process=look, step='lookdev')
                publishInfo.entity.context.update(look=look, process=publishInfo.entity.process, step='anim')
                # track time

                # renderLayerName = "{}_lookdev".format(look)
                # stuffInLayer = mc.editRenderLayerMembers( renderLayerName, q = True)
                # stuffNewLayer = []
                # if stuffInLayer:
                #     for stuff in stuffInLayer:
                #         children = mc.listRelatives(stuff, allDescendents=True)
                #         stuffNewLayer.append(stuff.split(':')[-1])
                #         if children:
                #             for child in children:
                #                 if not child in stuffInLayer:
                #                     stuffNewLayer.append(child.split(':')[-1])
                stuffInLayer = []

                stuffInLayer += mc.listRelatives(hair,allDescendents=True)
                stuffInLayer += mc.listRelatives(costume,allDescendents=True)

                stuffNewLayer = []

                exceptGeos = []

                for costumeExcept in costumeList:
                    if costumeExcept == costume:
                        continue
                    else:
                        exceptGeos += mc.listRelatives(costumeExcept, allDescendents=True)
                        exceptGeos.append(costumeExcept)

                for hairExcept in hairList:
                    if hairExcept == hair:
                        continue
                    else:
                        exceptGeos += mc.listRelatives(hairExcept, allDescendents=True)
                        exceptGeos.append(hairExcept)

                allGeos = mc.listRelatives('*:Geo_Grp', allDescendents=True)
                for geo in allGeos:
                    if geo not in exceptGeos:
                        stuffInLayer.append(geo)


                if stuffInLayer:
                    for stuff in stuffInLayer:
                        stuffNewLayer.append(stuff.split(':')[-1])


                dictData[look] = stuffNewLayer

        write_geo_data(dictData, publishInfo.entity)
        return [True, dictData]

    else:
        logger.info('Skip export Gpu')
        return [context_info.Status.skip, []]



    # if export and looks:
    #     for look in looks: 
    #         # look manage 
    #         # publishInfo.entity.context.update(look=look, process=look, step='lookdev')
    #         publishInfo.entity.context.update(look=look, process=publishInfo.entity.process, step='anim')
    #         # track time

    #         renderLayerName = "{}_lookdev".format(look)
    #         stuffInLayer = mc.editRenderLayerMembers( renderLayerName, q = True)
    #         stuffNewLayer = []
    #         if stuffInLayer:
    #             for stuff in stuffInLayer:
    #                 children = mc.listRelatives(stuff, allDescendents=True)
    #                 stuffNewLayer.append(stuff.split(':')[-1])
    #                 if children:
    #                     for child in children:
    #                         if not child in stuffInLayer:
    #                             stuffNewLayer.append(child.split(':')[-1])

    #         dictData[look] = stuffNewLayer

    #     write_geo_data(dictData, publishInfo.entity)
    #     return [True, dictData]

    # else:
    #     logger.info('Skip export Gpu')
    #     return [context_info.Status.skip, []]

def write_geo_data(dictData, entity=None):
    if not entity:
        entity = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))

    process = entity.process

    dataFile = entity.output_name(outputKey='data', hero=True)
    dataLookFile = dataFile.split('_')[0] + '_{}_look_geo_'.format(process) + dataFile.split('_')[-1]
    heroPath = entity.path.hero().abs_path()
    dataFilePath = '%s/%s' % (heroPath, dataLookFile)

    # data = dict()
    # for look in looks:
    #     data[look] = objs
    file_utils.ymlDumper(dataFilePath, dictData)
    logger.info('Looks Geo data updated: %s' %dataFilePath)