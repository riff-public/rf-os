# template for export mod
import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_utils import publish_info
from rf_utils.context import context_info
from rftool.shade import shade_utils
from rf_utils import custom_exception
from rftool.utils import maya_utils
from rf_utils import file_utils
from rf_utils import register_entity
from rftool.render.redshift import rs_utils
from rf_maya.lib import material_layer 
from rf_maya.rftool.clean.ascii_sanitizer import main as ascl

reload(publish_info)
reload(export_utils)


def run(publishInfo):
    """ export GPU cache file from Geo_Grp. """
    exportGrp = get_export_grp(publishInfo)

    looks = material_layer.get_texture_look()
    allOutputs = []
    results = []
    
    if looks: 
        for look in looks: 
            # look manage 
            # entity = publishInfo.entity.copy()
            publishInfo.entity.context.update(look=look, process=look)
            # entity.context.update(look=look, process=look)
            materialType = material_layer.get_active_material(look)
            # set active 
            material_layer.set_active_layer(look, materialType)

            # track time
            # hero = False if publishInfo.entity.step == 'texture' else True
            hero = True
            outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=hero, outputKey='material')

            outputFile = outputList[0]
            exportResult = export_utils.export_output(publishInfo, outputList, export_material, outputFile, exportGrp, publishInfo)
            register_data(publishInfo.entity, outputDict)

             # result collects 
            allOutputs += outputList
            results.append(exportResult)
        results = all(results)
    else: 
        results = context_info.Status.skip

    return [results, allOutputs]


def export_material(outputPath, exportGrp, publishInfo) :
    """ export gpu cache """
    import maya.cmds as mc
    if not mc.objExists(exportGrp):
        raise custom_exception.PipelineError('No %s found' % exportGrp)


    publishTexture = publishInfo.entity.path.publish_groom_texture().abs_path()
    replaceDict = maya_utils.move_texture_path(publishTexture, exportGrp, resize=False, relink=False)
    result = shade_utils.export_shade_with_data(outputPath, targetGrp=exportGrp)
    file_utils.search_replace_keys(outputPath, replaceDict, backupFile=False)
    rs_utils.convert_all_texture(publishTexture)
    # shade_utils.export_shade_data(dataPath, targetGrp=None)
    # clean if not clean 
    ascl.run_clean(outputPath) if not ascl.is_clean(outputPath) else None
    
    if not result:
        raise custom_exception.PipelineError('Failed to export material')
    return result


def get_export_grp(publishInfo):
    import maya.cmds as mc
    exportGrp = publishInfo.entity.projectInfo.asset.export_grp()
    if publishInfo.entity.step == context_info.Step.groom:
        exportGrp = publishInfo.entity.projectInfo.asset.groom_grp()

    return exportGrp
    # exportGrp = publishInfo.entity.projectInfo.asset.geo_grp()

    # if not mc.objExists(exportGrp):
    #     exportGrpNs = mc.ls('*:%s' % exportGrp)[0] if mc.ls('*:%s' % exportGrp) else None
    #     if exportGrpNs:
    #         return exportGrpNs
    # else:
    #     return exportGrp


def register_data(entity, outputDict):
    """ register gpu hero file """
    description = register_entity.Register(entity)
    output = register_entity.Output()
    output.add_look(entity.look)
    if entity.step in [context_info.Step.texture]:
        output.add_mtr(outputDict['publishedFile'], outputDict['publishedHeroFile'])
    if entity.step in [context_info.Step.lookdev]:
        output.add_mtr_render(outputDict['publishedFile'], outputDict['publishedHeroFile'])
    description.set(output=output)
    return description.write()
