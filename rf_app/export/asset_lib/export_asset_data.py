# template for export mod
import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_utils import publish_info
from rf_utils.context import context_info
reload(publish_info)
from rf_app.asm import asm_lib
from rf_utils import file_utils


def run(publishInfo):
    """ main function to to run for output_widget """
    outputHero = publishInfo.entity.path.hero().abs_path()
    outputFile = publishInfo.entity.output_name(outputKey='assetData', hero=True)
    outputData = '%s/%s' % (outputHero, outputFile)

    # track time
    exportResult = export_data(outputData, publishInfo.entity)
    return [exportResult, [outputData]]


def export_data(outputData, entity):
    results = []
    assetDict = asm_lib.collect_asset_description(entity)
    sgResult = update_shotgun(entity, assetDict)
    file_utils.ymlDumper(outputData, assetDict)

    results.append(True) if sgResult else results.append(False)
    results.append(True) if os.path.exists(outputData) else results.append(False)

    return all(results)

def update_shotgun(entity, assetDict):
    from rf_utils.sg import sg_process
    assetEntity = sg_process.get_one_asset(entity.project, entity.name)
    entities = [{'type': 'Asset', 'id': int(data['id']), 'name': data['name']} for assetName, data in assetDict.iteritems()]
    result = sg_process.update_subasset_list(assetEntity.get('id'), entities, sg_field='assets')
    return result


