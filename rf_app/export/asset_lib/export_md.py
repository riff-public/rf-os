import maya.cmds as mc

def run(publishInfo, runProcess=True):

    if publishInfo.entity.process in ['main', 'combRig'] :
        selectedMd()
    return True, []

def selectedMd(*args):
    mdLists = 'PrGeo_Grp'
    if mc.objExists(mdLists):
        mc.delete(mdLists)
    else:
        print "Folder 'MdGeo_Grp' is not Ready >> Please Create 'PrGeo_Grp'"



