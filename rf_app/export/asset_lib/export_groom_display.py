# template for export mod
import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_app.export.utils import batch_call
from rf_app.export.utils import session_call
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import register_entity
from rftool.utils import yeti_lib
from rf_utils import file_utils
reload(yeti_lib)
reload(register_entity)

reload(publish_info)
reload(session_call)
reload(batch_call)
reload(context_info)

def run(publishInfo):
    """ export display setting for yetiNode """
    # standard output list
    hero = False
    publishInfo.entity.context.update(look='main', process='main')
    outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=hero, outputKey='displayData')
    outputFile = outputList[0]
    exportResult = export_utils.export_output(publishInfo, outputList, export, outputFile)
    register_data(publishInfo.entity, outputDict)

    return [exportResult, outputList]


def export(dst):
    import maya.cmds as mc
    displayInfo = yeti_lib.get_display_info()
    result = file_utils.ymlDumper(dst, displayInfo)
    return dst 


def register_data(entity, outputDict):
    """ register gpu hero file """
    description = register_entity.Register(entity)
    output = register_entity.Output()
    output.add_look(entity.process)
    output.add_groom_yeti_display(outputDict['publishedFile'], outputDict['publishedHeroFile'])
    description.set(output=output)
    return description.write()

