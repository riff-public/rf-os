# template for export mod
import os 
import sys 
import shutil
import logging 

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_utils import publish_info 
from rf_utils.context import context_info
reload(publish_info)


def run(publishInfo): 
    """ main function to to run for output_widget """
    publishInfo.entity.context.update(app='abc')
    # standard output list 
    outputPath = publishInfo.publish_file()
    outputFile = context_info.RootPath(outputPath).abs_path()

    # Result: u'$RFPUBL/ProjectName/asset/publish/character/main/sunnyBase/model/main/v001/output/sunnyBase_mdl_main_md.v001.ma' # 
    # publishInfo.entity.context.update(app='gpu')
    publishHeroPath = publishInfo.publish_hero_file()
    publishHeroFile = context_info.RootPath(publishHeroPath).abs_path()

    # Result: u'$RFPUBL/ProjectName/asset/publish/character/main/sunnyBase/model/main/hero/sunnyBase_mdl_main_md.hero.ma' # 
    # publishInfo.entity.context.update(app='gpu')
    hero = publishInfo.hero_file()
    heroFile = context_info.RootPath(hero).abs_path()
    # Result: u'$RFPUBL/ProjectName/asset/publish/character/main/sunnyBase/hero/sunnyBase_mdl_main_md.hero.ma' # 

    # track time 
    exportResult = export_output(publishInfo, [outputFile, publishHeroFile, heroFile])
    return [exportResult, [outputFile, publishHeroFile, heroFile]]


def export_output(publishInfo, outputList): 
    exportGrp = publishInfo.entity.projectInfo.asset.geo_grp()
    output1 = str()
    results = []

    for outputFile in outputList: 
        startTime = export_utils.file_time(outputFile)
        
        if not os.path.exists(os.path.dirname(outputFile)): 
            os.makedirs(os.path.dirname(outputFile))

        if not output1: # export 
            output1 = export_gpu_still(exportGrp, outputFile)
            logger.info('Export success %s' % outputFile)
        
        else: # copy result 
            shutil.copy2(output1, outputFile)
            logger.info('Copy output success %s' % outputFile)

        endTime = export_utils.file_time(outputFile)
        result = export_utils.is_file_new(startTime, endTime)
        results.append(result)

    return all(results)


def export_gpu_still(exportGrp, outputPath) : 
    """ export gpu cache """
    import maya.cmds as mc 
    currentFrame = mc.currentTime(q = True)
    startFrame = currentFrame
    endFrame = currentFrame 

    exportDir = os.path.dirname(outputPath)
    filename = os.path.splitext(os.path.basename(outputPath))[0]

    # export objs 
    if mc.objExists(exportGrp) : 
        mc.select(exportGrp) 
        result = mc.gpuCache(exportGrp, 
                                startTime = startFrame, 
                                endTime = endFrame, 
                                optimize = True, 
                                optimizationThreshold = 40000, 
                                writeMaterials = True, 
                                dataFormat = 'ogawa', 
                                directory = exportDir, 
                                fileName = filename, 
                                saveMultipleFiles = False
                                )

        return outputPath 

    else: 
        raise PipelineError('No %s found' % exportGrp)


class PipelineError(Exception):
    def __init__(self, message):
        super(PipelineError, self).__init__(message)
        self.message = message
        

        