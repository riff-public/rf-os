import maya.cmds as mc
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import pymel.core as pm
import os , sys, re
import shutil
import time
import maya.mel as mel
from rf_app.sg_manager import dcc_hook
reload(dcc_hook)
from rf_app.export.utils import export_utils
from rf_utils import register_entity
from rf_utils import custom_exception
from rf_utils.context import context_info

try: 
    from rf_utils.miarmy import miarmy_utils
    reload(miarmy_utils)
    from rftool.utils import maya_utils
    reload(maya_utils)
    import platform
    import brainView as bv
    reload(bv)
    import yy_func as yyfc
    from collections import OrderedDict
    from rftool.rig.miarmyTool.utils import mmOaPrepUtils as mmoap
    reload(mmoap)
    from rftool.rig.miarmyTool.utils import mmAgentUtils as mmoa
    reload(mmoa)
    from rftool.rig.miarmyTool.utils import mmBakeAnimUtils as mmbau
    reload(mmbau)
    from rftool.rig.miarmyTool.utils import miarmySwitchChildren as msct
    reload(msct)
    from rftool.rig.miarmyTool.utils import miarmyNewPlacement as mmnp
    reload(mmnp)
    from rftool.rig.miarmyTool.utils import modifyMiarmyScripts as mmsc
    reload(mmsc)
    from utaTools.utapy import utaCore
    reload(utaCore)
    from McdGeneral import *
    import McdGeneral as mg
    reload(mg)
    from utaTools.utapy import renameMiarmy
    reload(renameMiarmy)
except ImportError: 
    pass 

def run(publishInfo, runProcess=True):
    """ Export Miarmy Action Loop """
    if publishInfo.entity.type == 'animated':
        return [context_info.Status.skip, []]

    currentProcess = publishInfo.entity.process 


    publishInfo.entity.context.update(process='action', look=publishInfo.entity.process)
    outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=True, outputKey='miarmyAction')
    outputFile = outputList[0]
    exportResult = False
    
    exportResult = export_utils.export_output(publishInfo, outputList, export_action, outputFile, publishInfo.entity.copy(), currentProcess)
    # restore 
    publishInfo.entity.context.update(process=currentProcess)

    register_data(publishInfo.entity, outputDict)

    return [exportResult, outputList]


# def export_action(dst, exportGrp):
#     from rf_maya.lib import maya_file
#     name, ext = os.path.splitext(dst)
#     fileType = 'mayaAscii' if ext == '.ma' else 'mayaBinary' if ext == '.mb' else 'mayaAscii'
#     result = maya_file.export_selection(dst, [exportGrp], fileType)
#     return result


def register_data(entity, outputDict):
    """ register gpu hero file """
    description = register_entity.Register(entity)
    output = register_entity.Output()
    output.add_process(entity.process)
    output.add_rig(outputDict['publishedFile'], outputDict['publishedHeroFile'])
    description.set(output=output)
    return description.write()


def export_action(exportPath, entity, actionName):
    # reference prep rig 
    # set context to rig prep 
    currentTime = mc.currentTime(q=True)

    entity.context.update(process=actionName)
    asset = entity.copy()
    asset.context.update(step='rig', process='prep', res='md')
    heroPath = asset.path.hero()
    prepFile = asset.publish_name(hero=True)
    rigPrepFile = '%s/%s' % (heroPath, prepFile)
    rigPrepFileAbs = '%s/%s' % (asset.path.hero().abs_path(), prepFile)

    if os.path.exists(rigPrepFileAbs): 
        nsPrep = dcc_hook.miarmy_oa_namespace(asset)+'001'
        try:
            dcc_hook.create_miarmyOa_reference(entity, rigPrepFile)
        except:
            pass
        referenceGrps = maya_utils.find_top_reference_transform(nsPrep)
        prepRNNode = mc.referenceQuery(referenceGrps[0], referenceNode=True)

        
        # create Miarmy Ready 
        miarmy_utils.miarmy_ready(agentLoco=True)
        
        # agentN = (mc.ls('{}:Rig_Grp_*'.format(nsPrep)))[0].split('Rig_Grp_')[-1]
        agentN = miarmy_utils.agent_loco_name(asset)


        agentObj = asset.projectInfo.asset.name_suffix('miarmyLoco')
        preferKeyOrder = ['SkinJnt','Rig_Grp','Geo_Grp']
        sortedGrp = sort_by_key(preferKeyOrder, referenceGrps)

        # rename default loco to asset_Loco
        renameMiarmy.renameMM(name=agentObj, newCreatureName=agentN)
        locoAgent = mc.ls(type='SetSetupRig')
        mc.parent(sortedGrp, locoAgent[0])

        charGrp = 'Char_Grp'
        members = mc.listRelatives(charGrp, c=True)

        stFrm = mc.playbackOptions(q = True , min = True )
        EdFrm = mc.playbackOptions(q = True , max = True )

        if members: 
            ns = mc.referenceQuery(members[0], ns=True)

            #bake anim layer
            layer_name = 'baked_layer_miarmy'
            # animLayer = mc.animLayer(layer_name, override=True, passthrough=False)
            allMoverCtrl = '{}:AllMover_Ctrl'.format(ns[1:])
            ctrls = []
            allMoverChildren = mc.listRelatives(allMoverCtrl, ad=True,type = 'transform')
            allMoverChildren = allMoverChildren if allMoverChildren else []
            allMoverChildren.insert(0, allMoverCtrl)
            #scan ctrl+attr for bake anim layer
            for each in allMoverChildren:
                if each.endswith('_Ctrl') or each.endswith('_ctrl'):
                    attrCtrl = mc.listAttr(each, k=True, u=True)
                    if attrCtrl:
                        for attr in attrCtrl:
                            ctrls.append('{}.{}'.format(each, attr))

            existing_layers = mc.ls(type='animLayer')
            containerNode = mc.ls(type='container')
            mc.modelEditor('modelPanel4', e=1, allObjects=0)
            # print ctrls
            mc.bakeResults(ctrls, 
                    simulation=True, 
                    t=(stFrm,EdFrm), 
                    sampleBy=1, 
                    oversamplingRate=1, 
                    disableImplicitControl=False, 
                    preserveOutsideKeys=False, 
                    sparseAnimCurveBake=False, 
                    removeBakedAttributeFromLayer=False, 
                    removeBakedAnimFromLayer=False, 
                    bakeOnOverrideLayer=True, 
                    minimizeRotation=True, 
                    controlPoints=False, 
                    shape=False)
            new_layer = [l for l in mc.ls(type='animLayer') if l not in existing_layers][0]
            mc.rename(new_layer, layer_name)
            mc.animLayer(layer_name, e=True, override=True, passthrough=False)

            for lyr in mc.ls(type='animLayer'):
                value = 0
                if lyr == layer_name:
                    value = 1
                mel.eval('animLayerEditorOnSelect %s %s' %(lyr, value))

            #set scaleSize
            if mc.objExists(ns + ':AllMover_Ctrl.scaleSize'):
                mc.cutKey(ns[1:] + ':AllMover_Ctrl.scaleSize' , s=True)
                sizeValue = mc.getAttr(ns[1:] + ':AllMover_Ctrl.scaleSize')
                mc.setAttr(ns[1:] + ':AllMover_Ctrl.scaleSize',1)

            #set scale to 1
            else:
                pm.PyNode(ns[1:] + ':AllMover_Ctrl').sy.unlock()
                scaleValue = mc.getAttr(ns + ':AllMover_Ctrl.sy')
                mc.setAttr(ns[1:] + ':AllMover_Ctrl.sy',1)



            # bake anim joints 
            mmbau.animMiarmyList([ns[1:]])

            #Bake Ctrl
            agentN = miarmy_utils.agent_loco_name(asset)
            adRigGrps = ['FacialCtrl_Grp','AddRigCtrlPar_Grp','AddRigCtrlPars_Grp','BpmRigCtrlPar_Grp','BpmRigStillPar_Grp']
            AllCtrl = []
            allBake = []
            for adRigGrp in adRigGrps:
                # adRigGrpName = deerBuckDefaultOa_agent:FacialCtrl_Grp_deerBuckDefaultOa_Loco
                adRigGrpName = '{}:'.format(nsPrep) + adRigGrp + '_' + agentN

                if mc.objExists(adRigGrpName):
                    
                    grpC = mc.listRelatives('{}:{}'.format(ns[1:],adRigGrp),ad=True,type = 'transform')    
                    Ctrl = []
                    if grpC:
                        for each in grpC:
                            if '_Ctrl' in each:
                                Ctrl.append(each)
                            if '_ctrl' in each:
                                Ctrl.append(each)
                            else: pass
                        for eachCnnt in Ctrl:
                            attrCtrl = mc.listAttr(eachCnnt,k=True,u=True)
                            obj = eachCnnt.split(':')[-1]
                            objLoco = '{}:{}_{}'.format(nsPrep,obj,agentN)
                            AllCtrl.append(objLoco)
                            if attrCtrl:
                                for cnnt in attrCtrl:
                                    mc.connectAttr('{}.{}'.format(eachCnnt,cnnt) , '{}.{}'.format(objLoco,cnnt))
                                    allBake.append('{}.{}'.format(objLoco,cnnt))
            mc.select(AllCtrl)
            #bake Facial+addRig Ctrl
            if AllCtrl:
                mc.bakeResults(AllCtrl,simulation=True,t=( stFrm , EdFrm ))

            # delete ANIM layer
            mc.delete(layer_name)

            # Get hiddenGeo
            geo_grp = mc.ls('{}:Geo_Grp'.format(ns[1:]))
            hidden_list = []
            list_geo = mc.listRelatives(geo_grp,ad=True,type = 'transform')
            attr = 'hiddenGeo'

            for each in list_geo:
                value = mc.getAttr('{}.v'.format(each))
                if not value:
                    hidden_list.append(each)

            #create action
            # unload other refs 
            refPath =  mc.file(q=True,reference=True)
            reloadRef = []
            for eachPath in refPath:
                isload = mc.referenceQuery(eachPath,  isLoaded = True)
                if isload == True:
                    refNode = mc.file(eachPath,q=True,rfn=True)
                    if refNode == prepRNNode:
                        pass
                    else:
                        mc.file(ur = refNode)
                        reloadRef.append(refNode)
                else: pass
      
            acNode = mmsc.McdCreateActionCmd(entity.process)
            
            # add hiddenGeo list to action node
            objAttr = '%s.%s' % (acNode, attr)
            if not mc.objExists(objAttr):
                mc.addAttr(acNode, ln=attr, dt='string')
                mc.setAttr(objAttr, e=True, keyable=True)
            mc.setAttr(objAttr, hidden_list, type='string') if hidden_list else None
            # END add hiddenGeo

            A = McdAddNewHoldersForExport(AllCtrl = AllCtrl)
            McdBakeAttributeActionForExport()

            for eachPath in reloadRef:
                try:
                    mc.file(lr = eachPath)
                except:
                    pass

            # unparent action 
            mc.parent(acNode, w=True)
            # remove prep rig 
            try:
                mc.file(rigPrepFile, rr=True)
            except:
                pass
            # clear miarmy content 
            miarmy_utils.remove_miarmy_content()

            # load back 
            for eachLr in reloadRef:
                try:
                    mc.file(lr=eachLr)
                except:
                    pass

            maya_utils.export_selection(exportPath, acNode)
            mc.delete(acNode) if mc.objExists(acNode) else None
            for each in mc.ls(type='container'):
                if not each in containerNode:
                    mc.delete(each)
            mc.modelEditor('modelPanel4', e=1, allObjects=1)

            #set scaleSize
            if mc.objExists(ns + ':AllMover_Ctrl.scaleSize'):
                mc.setAttr(ns[1:] + ':AllMover_Ctrl.scaleSize',sizeValue)

            #set scale back
            else:
                mc.setAttr(ns[1:] + ':AllMover_Ctrl.sy',scaleValue)
                pm.PyNode(ns[1:] + ':AllMover_Ctrl').sy.lock()

            # set timeline back 
            mc.currentTime(currentTime)

            if os.path.exists(exportPath): 
                return exportPath

    else: 
        logger.error('No rigPrepFileAbs %s' % rigPrepFileAbs)
        return context_info.Status.skip

        # 

        # for eachPath in reloadRef: 
        #     rnNode = mc.file(eachPath, q=True, rfn=True)
        #     mc.file(eachPath, loadReference=rnNode, prompt=False)
        
        # print 'DONE----------------------------------------------------------------------'


def McdAddNewHoldersForExport(AllCtrl = ''):
    selObjs = AllCtrl

    acNode = mc.listRelatives((mc.ls('Action_*')),c=True)[0]
    
    allExistedHolders = []
    counter = 0
    while(1):
        stri = str(counter)
        if cmds.getAttr(acNode + ".attrEnable[" + stri + "]"):
            holder = cmds.getAttr(acNode + ".attrHolders[" + stri + "]")
            allExistedHolders.append(holder)
        else:
            break
        counter += 1    
    counterNew = 0
    for i in range(len(selObjs)):
        stri = str(i + counter)
        cmds.setAttr(acNode + ".attrEnable[" + stri + "]", 1)
        cmds.setAttr(acNode + ".attrHolders[" + stri + "]", selObjs[i], type = "string")
        allExistedHolders.append(selObjs[i])
        counterNew += 1

    stri = str(counter + counterNew)
    result = allExistedHolders
    return result

def McdBakeAttributeActionForExport():
    acNode = mc.listRelatives((mc.ls('Action_*')),c=True)[0]
    counter = 0
    allHolders = []
    while(1):
        stri = str(counter)
        if mc.getAttr(acNode + ".attrEnable[" + stri + "]") :
            a = mc.getAttr(acNode + ".attrHolders[" + stri + "]")
            if mg.MIsBlank(a):
                counter += 1
                continue
            allHolders.append(a)
            counter += 1       
        else:
            break
    if len(allHolders) == 0:
        mc.setAttr(acNode + ".attrNum", 0)
        mc.confirmDialog(t = "Abort", m = "No controller in action.")
        return
    
    sourceList = []
    for i in range(len(allHolders)):
        objTest = mc.ls(allHolders[i])
        if not mg.MIsBlank(objTest):
            if len(objTest) == 1:
                sourceList.append(allHolders[i])
                
    allAttrs = []
    for i in range(len(sourceList)):
        attrs = mc.listAttr(sourceList[i], k = True , u =True)
        if not mg.MIsBlank(attrs):
            for j in range(len(attrs)):
                allAttrs.append(sourceList[i] + "." + attrs[j])
            
    counter = 0
    for i in range(len(allAttrs)):
        counter += 1
        stri = str(i)
        attrNameStr = allAttrs[i]
        attrNameNS = attrNameStr.split(":")[-1]
        mc.setAttr(acNode + ".attrNames[" + stri + "]", attrNameNS, type = "string")
    mc.setAttr(acNode + ".attrNum", counter)
    
    
    startTime = int(mc.playbackOptions(q = True, min = True)+0.01)
    endTime = int(mc.playbackOptions(q = True, max = True)+0.01)
    nbFrame = endTime - startTime + 1
    
    for i in range(nbFrame):
        stri = str(i)
        mc.currentTime(startTime+i)
        for j in range(counter):
            strj = str(j)
            attrVal = mc.getAttr(allAttrs[j])
            mc.setAttr(acNode + ".attrValuesRoot[" + strj + "].attrValues[" + stri + "]", attrVal)
    
    mc.setAttr(acNode + ".attrBaked", 1)
    return acNode

# preferKeyOrder = ['SkinJnt','Rig_Grp','Geo_Grp']
# inputList = ['Rig_Grp_deerBuckDefaultOa_Loco', 'SkinJnt_Grp', 'CharGeo_Grp', 'Test']

def sort_by_key(preferKeyOrder, inputList): 
    newList = []
    for key in preferKeyOrder: 
        for member in inputList: 
            if key in member: 
                newList.append(member)
                break
    
    listed = list(set(inputList) - set(newList))
    newList +=listed

    return newList

# filePath = mc.file(q=True,sn=True)
# asset = filePath.split('/')
# "P:\SevenChickMovie\asset\publ\char\{}\anim\{}\hero\output".format(asset[5],asset[7])