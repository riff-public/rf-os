# template for export mod
import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import custom_exception
from rf_utils.pipeline import local_hook
from rf_utils import register_entity
reload(publish_info)
reload(export_utils)
reload(register_entity)



def run(publishInfo):
    """ export GPU cache file from Geo_Grp. """
    assetType = publishInfo.entity.type
    export = False
    if assetType in ['char', 'prop']:
        export = True

    if export:
        app = publishInfo.entity.context.app
        process = publishInfo.entity.context.process
        publishInfo.entity.context.update(app='gpu')

        # track time
        outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=True, outputKey='gpu')
        publishInfo.entity.context.update(app=app) # restore

        res = publishInfo.entity.res
        exportGrp = publishInfo.entity.projectInfo.asset.geo_grp()
        contentGrp = export_utils.get_geo_grp(publishInfo.entity, res)
        outputFile = outputList[0]
        run = True if publishInfo.entity.step in ['model'] else False 
        
        with export_utils.maya_utils().IsolateGeo(exportGrp, contentGrp, run=run): 
            exportResult = export_utils.export_output(publishInfo, outputList, export_gpu_still, outputFile, exportGrp)

        # write to register
        register_data(publishInfo.entity, outputDict)
        return [exportResult, outputList]

    else:
        logger.info('Skip export Gpu')
        return [context_info.Status.skip, []]


def export_gpu_still(outputPath, exportGrp) :
    """ export gpu cache """
    import maya.cmds as mc

    gpuCachePlugin = 'gpuCache.mll'

    if not mc.pluginInfo(gpuCachePlugin, q=True, l=True):
        mc.loadPlugin(gpuCachePlugin, qt=True)

    # code below
    currentFrame = mc.currentTime(q = True)
    startFrame = currentFrame
    endFrame = currentFrame

    localName = local_hook.local_name(outputPath)

    exportDir = os.path.dirname(localName)
    filename, ext = os.path.splitext(os.path.basename(localName))

    # export objs
    if mc.objExists(exportGrp) :
        mc.select(exportGrp)
        result = mc.gpuCache(exportGrp,
                                startTime = startFrame,
                                endTime = endFrame,
                                optimize = True,
                                optimizationThreshold = 40000,
                                writeMaterials = True,
                                dataFormat = 'ogawa',
                                directory = exportDir,
                                fileName = filename,
                                saveMultipleFiles = False
                                )

        shutil.copy2(localName, outputPath)
        return outputPath if os.path.exists(outputPath) else None

    else:
        return context_info.Status.skip
        raise custom_exception.PipelineError('No %s found' % exportGrp)


def register_data(entity, outputDict):
    """ register gpu hero file """
    description = register_entity.Register(entity)
    output = register_entity.Output()
    output.add_process(entity.process)
    output.add_gpu(outputDict['publishedFile'], outputDict['heroFile'])
    description.set(output=output)
    return description.write()

def export_gpu_still_legacy(outputPath, exportGrp) :
    """ export gpu cache """
    import maya.cmds as mc
    gpuCachePlugin = 'gpuCache.mll'

    if not mc.pluginInfo(gpuCachePlugin, q=True, l=True):
        mc.loadPlugin(gpuCachePlugin, qt=True)

    # code below
    currentFrame = mc.currentTime(q = True)
    startFrame = currentFrame
    endFrame = currentFrame

    exportDir = os.path.dirname(outputPath)
    filename = os.path.splitext(os.path.basename(outputPath))[0]

    # export objs
    if mc.objExists(exportGrp) :
        mc.select(exportGrp)
        result = mc.gpuCache(exportGrp,
                                startTime = startFrame,
                                endTime = endFrame,
                                optimize = True,
                                optimizationThreshold = 40000,
                                writeMaterials = True,
                                dataFormat = 'ogawa',
                                directory = exportDir,
                                fileName = filename,
                                saveMultipleFiles = False
                                )

        return outputPath

    else:
        return context_info.Status.skip
        raise custom_exception.PipelineError('No %s found' % exportGrp)
