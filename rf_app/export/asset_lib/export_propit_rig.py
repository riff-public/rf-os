# template for export mod
import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_app.export.utils import batch_call
from rf_app.export.utils import session_call
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import register_entity
from rf_maya.rftool.clean.ascii_sanitizer import main as ascl
from rf_app.export.utils import save_utils
reload(publish_info)
reload(session_call)
reload(batch_call)
reload(save_utils)

def run(publishInfo, runProcess=True):
    """ copy and published this file for next department """
    # standard output list
    hero = True if publishInfo.entity.step in ['rig'] else False
    outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=hero)
    outputFile = outputList[0]
    processList = ['import_ref', 'remove_namespaces'] if runProcess else []
    exportGrp = publishInfo.entity.projectInfo.asset.top_grp()
    exportResult = export_utils.export_output(publishInfo, outputList, session_export, outputFile, processList, 'export', exportGrp)

    # mb part 
    outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=hero, outputKey='rig')
    heroFile = outputDict['heroFile']

    current = save_utils.mc.file(q=True, sn=True)
    isFileClean = ascl.is_clean(current) if current else False
    
    if isFileClean: 
        print 'save session to mb'
        session_export(heroFile, processList, 'export', exportGrp)
    else: 
        # save as mb 
        print 'run save_utils to mb'
        save_utils.run_cmd(outputFile, heroFile)

    register_data(publishInfo.entity, outputDict)
    return [exportResult, outputList]

def batch_export(dst, processList, output='save', export=''):
    import maya.cmds as mc
    src = mc.file(q=True, sn=True)
    batch_call.run(src, dst, str(processList), output=output, export=export)
    return dst


def session_export(dst, processList, output='save', export=''):
    import maya.cmds as mc
    src = mc.file(q=True, sn=True)
    result = session_call.run(src, dst, processList, output, export)

    # clean plugin 
    return ascl.run_clean(dst)


def register_data(entity, outputDict): 
    """ register gpu hero file """
    description = register_entity.Register(entity)
    output = register_entity.Output()
    output.add_process(entity.process)
    output.add_rig(outputDict['publishedFile'], outputDict['heroFile'])
    description.set(output=output)
    return description.write()
