# template for export mod
import os
import sys
import shutil
import logging
import maya.cmds as mc

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_app.export.utils import batch_call
from rf_app.export.utils import session_call
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import register_entity
import rftool.rig
from utaTools.utapy import utaCore
from utaTools.utapy import utaAttrWriteRead
from utaTools.utapy import cleanUp
from utaTools.pkrig import rigTools
reload(rigTools)
reload(cleanUp)
reload(utaCore)
reload(publish_info)
reload(session_call)
reload(batch_call)
reload(utaAttrWriteRead)


def run(publishInfo, runProcess=True):
    # from rf_utils.context import context_info
    asset = context_info.ContextPathInfo() 
    charName = asset.name
    """ copy and published this file for next department """
    # standard output list
    exportGrp = 'Rig_Grp'
    hero = True
    outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=hero)
    outputFile = outputList[0]
    runProcess = True
    processList = [] if runProcess else []
    # print processList, 'processList'
    exportResult = export_utils.export_output(publishInfo, outputList, session_export, outputFile, processList, 'export', exportGrp)
    # print exportResult, 'exportResult'
    register_data(publishInfo.entity, outputDict)

    if publishInfo.entity.process == 'main': 
        if hero: 
            outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=hero, outputKey='rig')
            heroFile = outputDict['heroFile']
            session_export(heroFile, processList, 'export', exportGrp)
            register_data(publishInfo.entity, outputDict)

    print "-------------------------------------------------------------------------------------------"
    print "------------------------------------- PUBLISH PROXY IS DONE -------------------------------"
    print "-------------------------------------------------------------------------------------------"

    ## Open File Path
    utaCore.openFileWorldSpace()

    return [exportResult, outputList]

def batch_export(dst, processList, output='save', export=''):
    import maya.cmds as mc
    src = mc.file(q=True, sn=True)
    batch_call.run(src, dst, str(processList), output=output, export=export)
    return dst

def session_export(dst, processList, output='save', export=''):
    import maya.cmds as mc
    src = mc.file(q=True, sn=True)
    return session_call.run(src, dst, processList, output, export)


def register_data(entity, outputDict):
    """ register gpu hero file """
    description = register_entity.Register(entity)
    output = register_entity.Output()
    output.add_process(entity.process)
    hero = outputDict.get('heroFile')
    if not hero: 
        hero = outputDict.get('publishedHeroFile')
    output.add_rig(outputDict['publishedFile'], hero)
    # output.add_rig(outputDict['publishedFile'], outputDict['publishedHeroFile'])
    # print outputDict['publishedHeroFile']
    description.set(output=output)
    return description.write()

def cleanResolutionFile():

    cleanUp.clearDeleteGrp()
    cleanUp.clearLayer();
    cleanUp.clearUnusedAnim();
    cleanUp.clearUnknowNode();
    cleanUp.clearUnusedNode();
    cleanUp.clearVraySettings()
    cleanUp.clearCameraDefault()
    rigTools.scaleGimbalControl()
    cleanUp.clearInheritsTransform()
    cleanUp.checkParameterUV()

    objSets = mc.ls('set_*')
    for each in objSets:
        mc.delete(each)

def selectProcess(process = '', *args):
    processOp = ['Md', 'Lo', 'Pr']
    for each in processOp:
        if each == process:
            pass
        else:
            processGrp = '%sGeo_Grp' % each
            if mc.objExists (processGrp):
                mc.delete(processGrp)
    print '#', '%sGeo_Grp' % process, ': is Ready'

