# template for export mod
import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils import custom_exception
from rf_utils import register_entity
reload(publish_info)
reload(export_utils)

import maya.mel as mel


def run(publishInfo):
    """ export fbx cache file from Geo_Grp. """
    app = publishInfo.entity.context.app
    process = publishInfo.entity.context.process
    # publishInfo.entity.context.update(app='gpu')

    # track time
    outputList, outputDict = export_utils.publish_output_list(publishInfo, hero=False, outputKey='fbx')
    publishInfo.entity.context.update(app=app) # restore

    exportGrp = publishInfo.entity.projectInfo.asset.geo_grp()
    res = publishInfo.entity.res
    contentGrp = export_utils.get_geo_grp(publishInfo.entity, res)
    outputFile = outputList[0]
    run = True if publishInfo.entity.step in ['model'] else False 
    
    with export_utils.maya_utils().IsolateGeo(exportGrp, contentGrp, run=run): 
        exportResult = export_utils.export_output(publishInfo, outputList, export_cache_fbx, outputFile, exportGrp)
    
    register_data(publishInfo.entity, outputDict)
    return [exportResult, outputList]


def export_cache_fbx(outputPath, exportGrp) :
    import maya.cmds as mc
    import maya.mel as mm
    from rf_maya.lib import maya_file
    reload(maya_file)

    pluginName = 'fbxmaya.mll'

    if not mc.pluginInfo(pluginName, q=True, l=True):
        mc.loadPlugin(pluginName, qt=True)

    # if dir not exists, create one
    if not os.path.exists(os.path.dirname(outputPath)) :
        os.makedirs(os.path.dirname(outputPath))

    # frame range
    start = mc.currentTime(q=True)
    end = start

    # update for UE4
    mel.eval('FBXExportSmoothingGroups -v 1')
    mel.eval('FBXExportHardEdges -v 0')
    mel.eval('FBXExportTangents -v 0')
    mel.eval('FBXExportSmoothMesh -v 0')
    mel.eval('FBXExportInstances -v 0')
    mel.eval('FBXExportReferencedAssetsContent -v 1')
    mel.eval('FBXExportInputConnections -v 1')
    mel.eval('FBXExportConvertUnitString -v cm')
    mel.eval('FBXExportUpAxis z')
    mel.eval('FBXExportInAscii -v 0')
    mel.eval('FBXExportFileVersion -v "FBX201800"')

    outputPath = maya_file.export_fbx(outputPath, [exportGrp])

    if os.path.exists(outputPath):
        return outputPath
    else:
        raise custom_exception.PipelineError('Failed to export FBX cache')


def register_data(entity, outputDict):
    """ register gpu hero file """
    description = register_entity.Register(entity)
    output = register_entity.Output()
    output.add_process(entity.process)
    output.add_cache(outputDict['publishedFile'], outputDict['publishedHeroFile'])
    description.set(output=output)
    return description.write()