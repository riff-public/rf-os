import os
import sys
import traceback
from collections import OrderedDict
from functools import partial
import subprocess
import rf_config as config

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

# import framework modules
from rf_utils import file_utils
from rf_utils import icon
from rf_utils.context import context_info
from rf_utils import publish_info
from rf_utils.widget import dialog

class Config:
    asset = 'asset'
    scene = 'scene'
    configFile = '%s/config' % module_dir
    configExt = '.yml'

class Console:
    message = 'message'
    traceback = 'traceback'

class Color:
    bgred = 'background-color: rgb(200, 0, 0);'
    red = 'background-color: rgb(200, 0, 0);'

class OutputWidgetUI(QtWidgets.QWidget) :

    def __init__(self, parent = None) :
        super(OutputWidgetUI, self).__init__(parent)
        self.allLayout = QtWidgets.QVBoxLayout()

        # display listWidget
        self.output_listWidget = QtWidgets.QListWidget()
        # lable
        self.label = QtWidgets.QLabel('Output List')
        # export pushButton
        self.exportButton = QtWidgets.QPushButton()
        # res comboBox
        self.resComboBox = QtWidgets.QComboBox()
        self.output_listWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)

        self.topLayout = QtWidgets.QHBoxLayout()
        self.topLayout.addWidget(self.label)
        self.topLayout.addWidget(self.resComboBox)

        # console
        self.consoleLayout = QtWidgets.QVBoxLayout()
        self.consoleLabel = QtWidgets.QLabel('Information')
        self.consoleWidget = QtWidgets.QPlainTextEdit()
        self.consoleLayout.addWidget(self.consoleLabel)
        self.consoleLayout.addWidget(self.consoleWidget)

        # add to layout
        self.allLayout.addLayout(self.topLayout)
        self.allLayout.addWidget(self.output_listWidget)
        self.allLayout.addWidget(self.exportButton)
        self.allLayout.addLayout(self.consoleLayout)
        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(2, 2, 2, 2)
        self.setLayout(self.allLayout)

        self.allLayout.setStretch(0, 4)
        self.allLayout.setStretch(1, 0)
        self.allLayout.setStretch(2, 0)
        self.allLayout.setStretch(3, 1)


        # set display
        self.exportButton.setText('Export')
        self.exportButton.setMinimumSize(QtCore.QSize(0, 30))


class OutputWidget(OutputWidgetUI):
    """docstring for OutputWidget"""
    itemSelectionChanged = QtCore.Signal(dict)
    exportFeedback = QtCore.Signal(dict)
    widgetStatus = QtCore.Signal(dict)
    preRegister = QtCore.Signal(publish_info.PreRegister)
    exportStatus = QtCore.Signal(bool)
    resChanged = QtCore.Signal(str)

    def __init__(self, entityType=Config.asset, parent=None):
        super(OutputWidget, self).__init__(parent)
        self.entityType = entityType
        self.entity = None # this must be setup from tool view
        self.allowExport = True
        self.allowMessage = ''
        self.enable_console(False)

        self.init_signals()
        self.init_functions()

    def update_entity(self, entity):
        self.entity = entity
        if entity: 
            self.init_functions()


    def add_res(self, res):
        self.blankRes = '-- Select --'
        self.resComboBox.clear()
        self.resComboBox.addItem(self.blankRes)
        self.resComboBox.addItems(res)


    def init_signals(self):
        """ init signals """
        self.exportButton.clicked.connect(self.export)
        self.output_listWidget.clicked.connect(self.emit_signal)
        self.output_listWidget.customContextMenuRequested.connect(self.show_menu)
        self.resComboBox.currentIndexChanged.connect(self.res_signal)


    def init_functions(self):
        """ start logic """
        self.list_output() if config.isMaya else None

    def enable_console(self, value=True): 
        self.consoleLabel.setVisible(value)
        self.consoleWidget.setVisible(value)


    def list_module(self):
        """ list module from input app """
        if self.entityType == Config.asset:
            import asset_lib as lib
            reload(lib)
            # optimizing data by loading only name of func 
            # modules = lib.get_module_data()
            modules = lib.get_module()
            return modules


    def list_output(self):
        """ display module list """
        import asset_lib as lib
        reload(lib)
        modules = self.list_module()
        self.output_listWidget.clear()
        filterModules = self.find_filter() or []

        for name in filterModules:
            # func = modules.get(name)
            # load func into memory 
            func = lib.get_func(name)
            reload(func)
            traceback = func.__doc__
            self.add_output_item(name, func, traceback)

        # for name, func in modules.iteritems():
        #     traceback = func.__doc__
        #     self.add_output_item(name, func, traceback)

    def find_filter(self):
        if self.entity:
            name = '%s%s' % (self.entity.project, Config.configExt)
            configFilePath = '%s/%s' % (Config.configFile, name)

            if os.path.exists(configFilePath):
                data = file_utils.ymlLoader(configFilePath)
                defaultModules = data.get(self.entity.app, OrderedDict()).get(self.entityType, OrderedDict()).get('default')
                defaultProcess = data.get(self.entity.app, OrderedDict()).get(self.entityType, OrderedDict()).get(self.entity.res, OrderedDict()).get(self.entity.step, OrderedDict()).get('default')
                modules = data.get(self.entity.app, OrderedDict()).get(self.entityType, OrderedDict()).get(self.entity.res, OrderedDict()).get(self.entity.step, OrderedDict()).get(self.entity.process, defaultProcess)
                return modules or defaultModules


    def pre_register(self):
        res = str(self.resComboBox.currentText())
        self.entity.context.update(permanent=True, res=res)
        logger.info('pre_register1 %s' % self.entity.version)

        self.publishInfo = publish_info.PreRegister(self.entity)
        logger.info('pre_register2 %s' % self.entity.version)

        self.preRegister.emit(self.publishInfo)
        return self.publishInfo


    def export_selection(self, item):
        self.export(items=[item])

    def export(self, items=None):
        """ main export function """
        # check res
        res = str(self.resComboBox.currentText())

        if res == self.blankRes:
            self.resComboBox.setStyleSheet(Color.bgred)
            logger.error('Resolution unselected')
            self.message(Console.traceback, 'Please select resolution')
            return

        if not self.allowExport: 
            logger.error(self.allowMessage)
            dialog.MessageBox.error('Warning', self.allowMessage)
            return 

        items = [self.output_listWidget.item(a) for a in range(self.output_listWidget.count())] if not items else items
        # self.entity.context.update(permanent=True, res=str(self.resComboBox.currentText()))
        # self.publishInfo = publish_info.PreRegister(self.entity)
        allSuccess = True

        for item in items:
            data = item.data(QtCore.Qt.UserRole)
            func = data['func']
            workspace = func.workspace if hasattr(func, 'workspace') else False

            try:
                if item.checkState():
                    # reload first 
                    result, outputFiles = func.run(self.publishInfo)
                    data['message'] = 'Export success' if result == True else 'skip' if result == context_info.Status.skip else ''
                    data['outputs'] = outputFiles
                    self.register_files(workspace, outputFiles)

                else:
                    result = context_info.Status.skip

            except Exception as e:
                # store traceback to var
                error = traceback.format_exc()
                traceback.print_exc()
                logger.error(error)

                # send back to ui
                data['traceback'] = error
                self.exportFeedback.emit(data)
                result = False
                allSuccess = False

            item.setData(QtCore.Qt.UserRole, data)
            self.set_status(item, result)
        self.publishInfo.register()
        self.exportStatus.emit(allSuccess)


    def register_files(self, workspace, outputLists):
        """ add output to publishInfo """
        for path in outputLists:
            if workspace:
                self.publishInfo.add_workspace(path)
            else:
                self.publishInfo.add_output(path)
        self.publishInfo.add_workfile(context_info.ContextPathInfo().path.path().abs_path())


    def add_output_item(self, name, func, traceback):
        """ qt add item section """
        data = OrderedDict()
        data['func'] = func
        data['traceback'] = traceback
        iconWidget = QtGui.QIcon()
        iconWidget.addPixmap(QtGui.QPixmap(icon.gear),QtGui.QIcon.Normal,QtGui.QIcon.Off)

        item = QtWidgets.QListWidgetItem(self.output_listWidget)

        # set checked
        item.setFlags(item.flags() | QtCore.Qt.ItemIsUserCheckable)
        item.setCheckState(QtCore.Qt.Checked)

        item.setData(QtCore.Qt.UserRole, data)
        item.setText(name)
        item.setIcon(iconWidget)

    def set_status(self, item, status):
        iconWidget = QtGui.QIcon()
        iconPath = icon.ok if status == True else icon.skip if status == context_info.Status.skip else icon.no
        iconWidget.addPixmap(QtGui.QPixmap(iconPath),QtGui.QIcon.Normal,QtGui.QIcon.Off)
        item.setIcon(iconWidget)
        QtWidgets.QApplication.processEvents()

    def emit_signal(self):
        item = self.output_listWidget.currentItem()
        data = item.data(QtCore.Qt.UserRole)
        self.itemSelectionChanged.emit(data)
        self.console(data)

    def res_signal(self):
        """ reset color if there are activities on comboBox """
        self.resComboBox.setStyleSheet('')
        # self.entity.context.update(permanent=True, res=str(self.resComboBox.currentText()))

        # start preregistering when res has changed
        res = str(self.resComboBox.currentText())
        if not res == self.blankRes:
            self.publishInfo = self.pre_register()
        self.resChanged.emit(res)
        if config.isMaya: 
            self.list_output()


    def show_menu(self, pos):
        """ show menu """
        menu = QtWidgets.QMenu(self)
        currentItem = self.output_listWidget.currentItem()
        data = currentItem.data(QtCore.Qt.UserRole)
        outputs = data.get('outputs', [])
        runSelected = menu.addAction('Run')
        runSelected.triggered.connect(partial(self.export_selection, currentItem))

        if outputs:
            # menu.triggered.connect(partial(self.menu_action, selectedItems))
            menu.addSeparator()
            outputMenu = QtWidgets.QMenu('Show output', self)
            outputMenu.triggered.connect(self.show_output)

            for output in outputs:
                outputMenu.addAction(output)

            menu.addMenu(outputMenu)


        menu.popup(self.output_listWidget.mapToGlobal(pos))

    def show_output(self, menuItem):
        """ event from show menu """
        path = str(menuItem.text()) or ''
        absPath = context_info.RootPath(path).abs_path()

        if os.path.exists(absPath):
            open_in_explorer(absPath)

        else:
            logger.warning('Path not exists : %s' % absPath)

    def console(self, data):
        """ display console """
        display = []
        func = data.get('func')
        doc = func.run.__doc__ if func else ''
        traceback = data.get('traceback', '')
        message = data.get('message')
        consoleColor = ''

        if doc:
            display.append(doc)

        if traceback:
            display = []
            display.append(traceback)
            consoleColor = Color.red

        if message:
            display = []
            display.append(message)
            consoleColor = ''

        displayMessage = ('\n').join(display)

        self.consoleWidget.setPlainText(displayMessage)
        self.consoleWidget.setStyleSheet(consoleColor)

    def message(self, type, message):
        """ emit status message """
        data = OrderedDict()
        data[type] = message
        self.widgetStatus.emit(data)


def open_in_explorer(path):
    if '####' in path: 
        path = os.path.dirname(path)
    subprocess.Popen(r'explorer /select,"%s"' % path.replace('/', '\\'))

