import sys
import os
from datetime import date, datetime
from settings.base import (
    ICONS,
    NOTE_ICONS
)
moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)
import rf_config as config

from rf_utils.ui import load

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import QtGui, QtWidgets, QtCore


class QCustomQWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(QCustomQWidget, self).__init__(parent)
        self.textQVBoxLayout = QtWidgets.QVBoxLayout()
        self.textUpQLabel = QtWidgets.QLabel()
        self.textDownQLabel = QtWidgets.QLabel()
        self.textDown2QLabel = QtWidgets.QLabel()
        self.textQVBoxLayout.addWidget(self.textUpQLabel)
        self.textQVBoxLayout.addWidget(self.textDownQLabel)
        self.textQVBoxLayout.addWidget(self.textDown2QLabel)
        self.textQVBoxLayout.setSpacing(0)
        self.textQVBoxLayout.setContentsMargins(4, 4, 4, 4)

        self.dueDateBoxLayout = QtWidgets.QVBoxLayout()
        self.dueDateLabel = QtWidgets.QLabel()
        self.startDateLabel = QtWidgets.QLabel()
        self.durationLabel = QtWidgets.QLabel()
        self.dueDateBoxLayout.addWidget(self.startDateLabel)
        self.dueDateBoxLayout.addWidget(self.dueDateLabel)
        self.dueDateBoxLayout.addWidget(self.durationLabel)
        self.dueDateBoxLayout.setSpacing(0)
        self.dueDateBoxLayout.setContentsMargins(4, 4, 4, 4)

        self.allQHBoxLayout = QtWidgets.QHBoxLayout()
        self.iconQLabel = QtWidgets.QLabel()
        self.allQHBoxLayout.addWidget(self.iconQLabel, 0)
        self.allQHBoxLayout.addLayout(self.textQVBoxLayout, 1)
        self.allQHBoxLayout.addLayout(self.dueDateBoxLayout, 1)
        self.allQHBoxLayout.setSpacing(0)
        self.allQHBoxLayout.setContentsMargins(2, 2, 2, 2)
        self.setLayout(self.allQHBoxLayout)

        # setStyleSheet
        self.textUpQLabel.setStyleSheet("""
            padding-left: 7px;
            color: #fff;
            background:transparent;
            font-weight: bold;
            font-size: 12px
        """)
        self.textDownQLabel.setStyleSheet("""
            padding-left: 7px;
            color: #fff;
            font-size: 12px;
            background:transparent;
        """)
        self.textDown2QLabel.setStyleSheet("""
            padding-left: 7px;
            color: #999999;
            font-size: 11px;
            background:transparent;
        """)
        self.startDateLabel.setStyleSheet("""
            color: #ddd;
            font-size: 11px;
            background:transparent;
        """)
        self.dueDateLabel.setStyleSheet("""
            color: #ddd;
            font-size: 11px;
            background:transparent;
        """)
        self.durationLabel.setStyleSheet("""
            color: #ddd;
            font-size: 11px;
            background:transparent;
        """)
        self.iconQLabel.setStyleSheet("""
            background:transparent;
        """)

    def setTextUp(self, text):
        if text is None:
            text = "-"
        self.textUpQLabel.setText(text.decode('utf8'))

    def setTextDown(self, text):
        if text is None:
            text = "-"
        self.textDownQLabel.setText(text.decode('utf8'))

    def setTextDown2(self, text):
        if text is None:
            text = "-"
        self.textDown2QLabel.setText(text.decode('utf8'))

    def setStartDateLabel(self, text):
        if text is None:
            text = "-"
        self.startDateLabel.setText('start: '+str(text))

    def setDueDateLabel(self, text):
        if text is None:
            text = "-"
        self.dueDateLabel.setText('due: '+str(text))

    def setDurationLabel(self, text):
        if text is None:
            text = "-"
            self.durationLabel.setText("-")
        else:
            today = date.today()
            due_date = datetime.strptime(text, '%Y-%m-%d').date()
            duration_day = (due_date - today)
            self.durationLabel.setText(str(duration_day.days) + " day left")
            if duration_day.days < 0:
                self.durationLabel.setStyleSheet("""
                    color: red;
                    background:transparent;
                """)
                self.dueDateLabel.setStyleSheet("""
                    color: red;
                    background:transparent;
                """)

    def setIcon(self, status_code):
        for icon in ICONS:
            if icon['code'] == status_code:
                self.iconQLabel.setPixmap(QtGui.QPixmap(
                    '{core_path}/rf_app/task_manager/images/icons/{picture_name}'.format(
                        core_path=core,
                        picture_name=icon['picture_name'],
                    )
                ))
                break


class CustomListNotesWidget(QtWidgets.QWidget):
    def __init__(self, parent=None, sg_service=None, note=None):
        super(CustomListNotesWidget, self).__init__(parent)
        self.parent = parent
        self.sg = sg_service
        self.current_note = note
        self.textQVBoxLayout = QtWidgets.QVBoxLayout()
        self.textUpQLabel = QtWidgets.QLabel()
        self.textDownQLabel = QtWidgets.QLabel()
        self.textQVBoxLayout.addWidget(self.textUpQLabel)
        self.textQVBoxLayout.addWidget(self.textDownQLabel)
        self.textQVBoxLayout.setSpacing(0)
        self.textQVBoxLayout.setContentsMargins(0, 0, 0, 0)
        self.allQHBoxLayout = QtWidgets.QHBoxLayout()
        self.iconQLabel = QtWidgets.QLabel()
        self.allQHBoxLayout.addWidget(self.iconQLabel, 0)
        self.allQHBoxLayout.addLayout(self.textQVBoxLayout, 1)
        self.allQHBoxLayout.setSpacing(0)
        self.allQHBoxLayout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.allQHBoxLayout)
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.on_context_menu)
        self.set_context_menu()
        # setStyleSheet
        self.textUpQLabel.setStyleSheet("""
            color: #fff;
            font-weight: bold;
            padding: 9px;
            padding-bottom: 0px;
            background:transparent;
            font-size: 14px;
        """)
        self.textDownQLabel.setStyleSheet("""
            color: #fff;
            font-size: 12px;
            padding-top: 0px;
            padding: 9px;
            background:transparent;
            white-space: pre;
        """)
        self.iconQLabel.setStyleSheet("""
            background:transparent;
        """)

        self.textUpQLabel.setWordWrap(True)
        self.textDownQLabel.setWordWrap(True)

    def set_context_menu(self):
        self.context_menu = QtWidgets.QMenu(self)
        self.context_menu.setStyleSheet("""
            background: #444444;
        """)
        for icon in NOTE_ICONS:
            icon_menu = QtGui.QIcon('{core_path}/icons/_sg/note_status/{picture_name}'.format(core_path=core, picture_name=icon['picture_name']))
            action = self.context_menu.addAction(icon_menu, icon['display'])
            action.setData(icon)

    def on_context_menu(self, point):
        action_selected = self.context_menu.exec_(self.mapToGlobal(point)).data()
        if action_selected:
            if self.current_note['sg_status_list'] != action_selected['code']:
                self.parent.loading_start()
                QtCore.QCoreApplication.processEvents()
                updated_note = self.sg.update_note_status(self.current_note, action_selected)
                if updated_note:
                    self.parent.fetch_notes()
                    self.parent.loading_stop()
            else:
                print 'same status'

    def setTextUp(self, text):
        self.textUpQLabel.setText(self.truncate_str(text, 80))

    def setTextDown(self, text):
        self.textDownQLabel.setText(self.truncate_str(text, 200))

    def setIcon(self, status_code):
        self.current_status = status_code
        for icon in NOTE_ICONS:
            if icon['code'] == status_code:
                self.iconQLabel.setPixmap(QtGui.QPixmap(
                    '{core_path}/icons/_sg/note_status/{picture_name}'.format(
                        core_path=core,
                        picture_name=icon['picture_name'],
                    )
                ))
                break

    def truncate_str(self, text, limit_str):
        if len(text) > limit_str:
            text = text[:limit_str] + '...'
        return text


class CustomListStatusWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(CustomListStatusWidget, self).__init__(parent)
        self.textQVBoxLayout = QtWidgets.QVBoxLayout()
        self.status_text = QtWidgets.QLabel()
        self.textQVBoxLayout.addWidget(self.status_text)
        self.textQVBoxLayout.setSpacing(0)
        self.textQVBoxLayout.setContentsMargins(4, 4, 4, 4)
        self.allQHBoxLayout = QtWidgets.QHBoxLayout()
        self.iconQLabel = QtWidgets.QLabel()
        self.allQHBoxLayout.addWidget(self.iconQLabel, 0)
        self.allQHBoxLayout.addLayout(self.textQVBoxLayout, 1)
        self.allQHBoxLayout.setSpacing(0)
        self.allQHBoxLayout.setContentsMargins(2, 2, 2, 2)
        self.setLayout(self.allQHBoxLayout)
        # setStyleSheet
        self.status_text.setStyleSheet("""
            padding-left: 7px;
            color: #fff;
            background:transparent;
            font-size: 12px
        """)

        self.iconQLabel.setStyleSheet("""
            background:transparent;
        """)

    def setText(self, text):
        if text is None:
            text = "-"
        self.status_text.setText(text.decode('utf8'))

    def setIcon(self, status_code):
        for icon in ICONS:
            if icon['code'] == status_code:
                self.iconQLabel.setPixmap(QtGui.QPixmap(
                    '{core_path}/rf_app/task_manager/images/icons/{picture_name}'.format(
                        core_path=core,
                        picture_name=icon['picture_name'],
                    )
                ))
                break


class DialogAddProcess(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(DialogAddProcess, self).__init__(parent)
        uiFile = '%s/add_process.ui' % moduleDir
        self.ui = load.setup_ui(uiFile, self)
        self.parent = parent
        self.init_gui()
        self.init_signals()
        self.ui.show()

    def init_gui(self):
        self.ui.setWindowTitle("Create Process")

    def init_signals(self):
        self.ui.create_process.clicked.connect(self.add_process)

    def get_name_process(self):
        return self.ui.text_process.text()

    def add_process(self):
        try:
            process_name = self.get_name_process()
            process_path = self.parent.get_process_path()
            new_path_process = "{process_path}/{process_name}".format(
                process_path=process_path,
                process_name=process_name
            )
            if not os.path.exists(new_path_process):
                os.makedirs(new_path_process)
                maya_dir = "{new_path_process}/maya".format(new_path_process=new_path_process)
                os.makedirs(maya_dir)
        except Exception as e:
            print 'Error %s', e
        finally:
            self.parent.init_list_process()
            self.ui.close()


class DialogAddProcessTemplate(QtWidgets.QWidget):
    def __init__(self, mainWindow=None, ContextPathInfo=None):
        super(DialogAddProcessTemplate, self).__init__(parent=mainWindow)
        uiFile = '%s/add_process_template.ui' % moduleDir
        self.ui = load.setup_ui(uiFile, self)
        self.mainWindow = mainWindow
        self.asset = ContextPathInfo
        self.init_gui()
        self.init_signals()
        self.ui.show()

    def init_gui(self):
        self.ui.setWindowTitle("Create Process")
        self.list_process_template()

    def init_signals(self):
        self.ui.create_process.clicked.connect(self.add_process)
        self.ui.moveRight_pushButton.clicked.connect(self.move_right)
        self.ui.moveLeft_pushButton.clicked.connect(self.move_left)
        self.ui.input_lineEdit.returnPressed.connect(self.add_input)

    def list_process_template(self):
        self.processes = self.asset.projectInfo.asset.get(self.asset.step).get('process')
        if self.processes:
            self.ui.template_listWidget.addItems(self.processes)

    def add_input(self):
        text = self.ui.input_lineEdit.text()
        self.ui.input_lineEdit.clear()
        self.ui.create_listWidget.addItem(text)

    def move_right(self):
        items = self.ui.template_listWidget.selectedItems()

        for item in items[::-1]:
            self.ui.create_listWidget.addItem(str(item.text()))
            self.ui.template_listWidget.takeItem(self.ui.template_listWidget.row(item))

    def move_left(self):
        items = self.ui.create_listWidget.selectedItems()

        for item in items[::-1]:
            self.ui.create_listWidget.takeItem(self.ui.create_listWidget.row(item))

            if str(item.text()) in self.processes:
                self.ui.template_listWidget.addItem(str(item.text()))


    def get_name_process(self):
        return self.ui.text_process.text()

    def add_process(self):
        if self.ui.create_listWidget.count():
            processes = [str(self.ui.create_listWidget.item(a).text()) for a in range(self.ui.create_listWidget.count())]
            process_path = self.mainWindow.get_process_path()
            step_selected = self.mainWindow.get_selected_step()
            for process_name in processes:
                try:
                    new_path_process = "{process_path}/{process_name}".format(
                        process_path=process_path,
                        process_name=process_name
                    )
                    if not os.path.exists(new_path_process):
                        os.makedirs(new_path_process)
                        maya_dir = "{new_path_process}/maya".format(new_path_process=new_path_process)
                        if step_selected['short_name'] == "comp":
                            nuke_dir = "{new_path_process}/nuke".format(new_path_process=new_path_process)
                            if not os.path.exists(nuke_dir):
                                os.makedirs(nuke_dir)
                        os.makedirs(maya_dir)
                except Exception as e:
                    print 'Error %s', e

            self.mainWindow.init_list_process()
        self.ui.close()
