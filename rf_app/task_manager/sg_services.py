import os
from os.path import expanduser
from settings.base import (
    SHOTGUN_URL,
    SCRIPTS_NAME,
    API_KEY
)
from shotgun_api3 import Shotgun
from datetime import datetime

STEP_IN_ASSET = ['Model', 'Rig', 'Design', 'Groom', 'Set', 'Lookdev', 'Hair', 'Sim', 'Cloth', 'Texture', 'AssetQC', 'Anim']
STEP_IN_SCENE = ['Layout', 'Anim', 'TechAnim', 'SetDress', 'FinalCam', 'FX', 'Light', 'Comp', 'Render', 'Sim']
TASK_PROCESS_SYNC = ['AssetQC', 'Sim']

class ShotgunServices:
    def __init__(self):
        self.sg = self.sg_connect()

    def sg_connect(self):
        try:
            sg = Shotgun(
                SHOTGUN_URL,
                SCRIPTS_NAME,
                API_KEY
            )
            print("connected shotgun api: {}".format(sg))
        except Exception as e:
            print("Cannot connect shotgun api.{}".format(e))

        return sg

    def sg_connection(self):
        return self.sg

    def filter_steps(self, steps, entity_selected):
        if entity_selected == 'Asset':
            filtered_steps = list(filter(lambda x: x['code'] in STEP_IN_ASSET, steps))
        elif entity_selected == 'Shot':
            filtered_steps = list(filter(lambda x: x['code'] in STEP_IN_SCENE, steps))
        return filtered_steps

    def get_task_process_sync(self):
        return TASK_PROCESS_SYNC

    def get_list_users_by_group(self, group_selected):
        group_name = group_selected['code']
        filters = [['groups', 'name_contains', group_name]]
        users = self.sg.find('HumanUser', filters, ['name', 'projects'])
        users.sort(key=lambda item: item['name'].lower())
        return users

    def get_list_users_by_acc(self, account):
        filters = [['sg_ad_account', 'is', account]]
        users = self.sg.find_one('HumanUser', filters, ['name', 'groups', 'projects'])
        return users

    def get_list_project_by_user(self):
        projects = self.sg.find('Project', [['sg_status', 'is', 'Active']], ['name'])
        return projects

    def get_list_groups(self):
        groups = self.sg.find('Group', [], ['code'])
        groups.sort(key=lambda item: item['code'].lower())
        return groups

    def get_list_step(self, entity_selected):
        step_filters = ['entity_type', 'is', entity_selected]
        steps = self.sg.find('Step', [step_filters], ['code', 'short_name'])
        steps = self.filter_steps(steps, entity_selected)
        return steps

    def get_tasks_list(self, data):
        task_assignees = []
        project_filtered = {
            "type": data['project']['type'],
            "id": data['project']['id'],
            "name": data['project']['name']
        }
        step_filtered = {
            "type": data['step']['type'],
            "id": data['step']['id'],
            "code": data['step']['code']
        }

        if data['user']['name'] == "Show all":
            users = self.get_list_users_by_group(data['group'])
            for user in users:
                user_filtered = {
                    "type": user['type'],
                    "id": user['id'],
                    "name": user['name']
                }
                task_assignees.append(user_filtered)
                task_assignees.append(data['group'])
        else:
            user_filtered = {
                "type": data['user']['type'],
                "id": data['user']['id'],
                "name": data['user']['name']
            }
            task_assignees.append(user_filtered)

        tasks_filter = [
            ['project', 'is', project_filtered],
            ['task_assignees', 'in', task_assignees],
            ['step', 'is', step_filtered]
        ]
        fields = [
            'content',
            'sg_status_list',
            'entity',
            'entity.Asset.sg_asset_type',
            'entity.Shot.sg_sequence_code',
            'entity.Asset.sg_mainasset',
            'entity.Shot.sg_episode.Scene.code',
            'entity.Asset.code',
            'due_date', 
            'start_date',
            'tags'
        ]
        tasks = self.sg.find('Task', tasks_filter, fields)
        print("tasks length: %s", len(tasks))
        return tasks

    def get_tasks_list_no_filter_user(self, data):
        project_filtered = {
            "type": data['project']['type'],
            "id": data['project']['id'],
            "name": data['project']['name']
        }
        step_filtered = {
            "type": data['step']['type'],
            "id": data['step']['id'],
            "code": data['step']['code']
        }

        tasks_filter = [
            ['project', 'is', project_filtered],
            ['step', 'is', step_filtered]
        ]
        fields = [
            'content',
            'sg_status_list',
            'entity',
            'entity.Asset.sg_asset_type',
            'entity.Shot.sg_sequence_code',
            'entity.Asset.sg_mainasset',
            'entity.Shot.sg_episode.Scene.code',
            'entity.Asset.code',
            'due_date',
            'start_date',
            'tags'
        ]
        tasks = self.sg.find('Task', tasks_filter, fields)
        print("tasks length: %s", len(tasks))
        return tasks

    def fetch_notes(self, project, task, radio_checked):
        project_filtered = {
            "type": project['type'],
            "id": project['id'],
            "name": project['name']
        }
        note_links_filtered = [
            {
                "type": task['entity']['type'],
                "id": task['entity']['id'],
                "name": task['entity']['name']
            }
        ]
        note_field = ['subject', 'content', 'attachments', 'sg_status_list', 'sg_local_attachment_path']
        if radio_checked == 'task':
            tasks_filtered = [
                {
                    "type": task['type'],
                    "id": task['id'],
                    "content": task['content']
                }
            ]
            notes_filtered = [
                ["project", "is", project_filtered],
                ["note_links", "is", note_links_filtered],
                ["tasks", "is", tasks_filtered]
            ]
            notes = self.sg.find('Note', notes_filtered, note_field)
        elif radio_checked == 'asset':
            notes_filtered = [
                ["project", "is", project_filtered],
                ["note_links", "is", note_links_filtered]
            ]
            notes = self.sg.find('Note', notes_filtered, note_field)
        else:
            notes = []
        return notes

    def get_attachment(self, attachment):
        attachment_filtered = [['id', 'is', attachment['id']]]
        fields = ['image']
        attachments = self.sg.find_one('Attachment', attachment_filtered, fields)
        return attachments

    def update_note_status(self, note_selected, action_selected):
        payload = {
            'sg_status_list': action_selected['code']
        }
        note_updated = self.sg.update('Note', note_selected['id'], payload)
        return note_updated

    def dowload_image(self, attachment_data, download_note_path):
        downloaded_path = "{download_note_path}/{attachment_id}.png".format(
            download_note_path=download_note_path,
            attachment_id=attachment_data['id'])
        downloaded_image = self.sg.download_attachment(attachment_data, file_path=downloaded_path)
        return downloaded_image

    def fetch_status_list(self):
        status = self.sg.find('Status', [], ['name', 'code'])
        return status

    def get_tasks_write_note(self, department_selected, options_data, task):
        project_filtered = {
            "type": options_data['project']['type'],
            "id": options_data['project']['id'],
            "name": options_data['project']['name']
        }
        step_filtered = {
            "type": department_selected['type'],
            "id": department_selected['id'],
            "code": department_selected['code']
        }
        entity_filtered = {
            "type": task['entity']['type'],
            "id": task['entity']['id'],
            "name": task['entity']['name']
        }
        tasks_filtered = [
            ['project', 'is', project_filtered],
            ['step', 'is', step_filtered],
            ['entity', 'is', entity_filtered]
        ]

        fields = ['entity', 'content']
        tasks = self.sg.find('Task', tasks_filtered, fields)

        return tasks

    def get_last_version_task_asset(self, department, task):
        version_filter = [
            ['sg_department', 'is', department['short_name']],
            ['sg_task', 'is', task]
        ]
        order_version = [{'field_name': 'created_at', 'direction': 'desc'}]
        fields = ['code', 'sg_department', 'sg_task', 'user']
        last_version = self.sg.find_one('Version', version_filter, fields, order_version)
        return last_version

    def create_note(self, data):
        note_created = self.sg.create('Note', data)

        return note_created

    def upload_note_attachment(self, data, file_path):
        uploaded = self.sg.upload(data['type'], data['id'], file_path, field_name="attachments")
        return uploaded

    def update_task_status(self, task_target, short_code):
        payload = {
            'sg_status_list': short_code
        }
        task_updated = self.sg.update('Task', task_target['id'], payload)
        return task_updated

    def update_note_local_attachment_path(self, data, note_path):
        payload = {
            'sg_local_attachment_path': note_path
        }
        updated_note = self.sg.update(data['type'], data['id'], payload)
        return updated_note

    def update_in_progress_task(self, task_selected):
        payload = {
            'sg_status_list': 'ip'
        }
        updated_task = self.sg.update('Task', task_selected['id'], payload)
        print "update status task in_progress"
        return updated_task

    def update_task_start(self, task_selected): 
        task_id = task_selected['id']
        task = sg.find_one('Task', [['id', 'is', task_id]], ['start_date', 'content'])
        data = {'start_date': datetime.today().date()}
        return self.sg.update('Task', task_id, data)

    def update_ip_condition(self, task_selected): 
        prev_status = task_selected['sg_status_list']
        start_date = task_selected['start_date']
        if prev_status == 'wtg' or prev_status == 'rdy': 
            data = {'sg_status_list': 'ip'}
            if not start_date: 
                data.update({'start_date': datetime.today().date()})
            self.sg.update('Task', task_selected['id'], data)
