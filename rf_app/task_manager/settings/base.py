SHOTGUN_URL = "https://riffanimation.shotgunstudio.com"
SCRIPTS_NAME = "rf_app_taskmanager"
API_KEY = "14fb0ac3058857135f5ae67ef2685ada43ebef80bf73ba4166b4dad02d8867a3"

ICONS = [
    {
        'display': 'Approved',
        'code': 'apr',
        'picture_name': 'aprv_icon.png'
    },
    {
        'display': 'Fix',
        'code': 'fix',
        'picture_name': 'fix_icon.png'
    },
    {
        'display': 'On Hold',
        'code': 'hld',
        'picture_name': 'hld_icon.png'
    },
    {
        'display': 'In Progress',
        'code': 'ip',
        'picture_name': 'ip_icon.png'
    },
    {
        'display': 'Omit',
        'code': 'omt',
        'picture_name': 'omt_icon.png'
    },
    {
        'display': 'Pending for Publish',
        'code': 'pub',
        'picture_name': 'pendpubl_icon.png'
    },
    {
        'display': 'Ready to Start',
        'code': 'rdy',
        'picture_name': 'rdy_icon.png'
    },
    {
        'display': 'Pending Review',
        'code': 'rev',
        'picture_name': 'review_icon.png'
    },
    {
        'display': 'Waiting to Start',
        'code': 'wtg',
        'picture_name': 'wtg_icon.png'
    }
]

NOTE_ICONS = [
    # {
    #     'display': 'Closed',
    #     'code': 'clsd',
    #     'picture_name': 'clsd_icon.png',
    #     'background_color': ''
    # },
    {
        'display': 'Complete',
        'code': 'cmpt',
        'picture_name': 'cmpt_icon.png',
        'background_color': '#009960'
    },
    {
        'display': 'In Progress',
        'code': 'ip',
        'picture_name': 'ip_icon.png',
        'background_color': '#ccaa66'
    },
    {
        'display': 'Open',
        'code': 'opn',
        'picture_name': 'opn_icon.png',
        'background_color': '#ff6767'
    }
]
