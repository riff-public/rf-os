import os
import sys
import shutil
# import json

core = '%s/core' % os.environ.get('RFSCRIPT')
if core not in sys.path:
    sys.path.append(core)

from rf_utils.context import context_info


class NoteModel:
    def __init__(self, parent=None, sg_service=None):
        # super(NoteModel, self).__init__(parent)
        self.sg = sg_service

    def submit_write_note(
        self,
        subject,
        msg,
        department,
        task,
        options_data,
        write_note_attachment
    ):
        last_version = self.sg.get_last_version_task_asset(department, task)
        project = {
            "type": options_data['project']['type'],
            "id": options_data['project']['id'],
            "name": options_data['project']['name']
        }
        notes_link_version = [{
            'type': last_version['type'],
            'id': last_version['id'],
            'name': last_version['code']
        }]
        data = {
            'project': project,
            'subject': subject,
            'content': msg,
            'tasks': [task],
            'user': options_data['user'],
            'note_links': notes_link_version,
            'addressings_to': [last_version['user']]
        }
        note_created = self.sg.create_note(data)

        note_attachment_data = {
            'id': note_created['id'],
            'type': note_created['type']
        }
        task_short_code = 'fix'
        task_status_updated = self.sg.update_task_status(task, task_short_code)

        picture_file = [".jpg", ".png", ".gif", '.JPG', ".PNG"]
        local_attachment_path = []

        for file_path in write_note_attachment:
            name, ext = os.path.splitext(file_path)
            if ext in picture_file:
                note_attachment_uploaded = self.sg.upload_note_attachment(note_attachment_data, file_path)
            else:
                local_attachment_path.append(file_path)
        if len(local_attachment_path) > 0:
            server_attachment_path = []
            for file in local_attachment_path:
                note_path = self.get_note_path(options_data, task, note_created)
                shutil.copy2(file, note_path)
                file_name_path = "{note_path}/{file_name}".format(
                    note_path=note_path,
                    file_name=os.path.basename(file)
                )
                server_attachment_path.append(file_name_path)
            server_attachment_path_str = ",".join(server_attachment_path)
            note_local_path_updated = self.sg.update_note_local_attachment_path(
                note_attachment_data,
                server_attachment_path_str
            )

    def get_note_path(self, options_data, task, note_created):
        context = context_info.Context()
        entity_type = self.convert_msg_shot_to_scene(task['entity']['type'].lower())
        context.use_sg(
            self.sg.sg,
            project=options_data['project']['name'],
            entityType=entity_type,
            entityId=task['entity']['id']
        )
        asset = context_info.ContextPathInfo(context=context)
        asset.context.update(step=options_data['step']['short_name'])
        note_path = asset.path.scheme(key='notePath').abs_path()

        note_id_path = "{note_path}/{note_id}".format(
            note_path=note_path,
            note_id=note_created['id']
        )
        if not os.path.exists(note_id_path):
            os.makedirs(note_id_path)

        return note_id_path

    def get_note_unc_path(self, options_data, task, note_created):
        context = context_info.Context()
        entity_type = self.convert_msg_shot_to_scene(task['entity']['type'].lower())
        context.use_sg(
            self.sg.sg,
            project=options_data['project']['name'],
            entityType=entity_type,
            entityId=task['entity']['id']
        )
        asset = context_info.ContextPathInfo(context=context)
        asset.context.update(step=options_data['step']['short_name'])
        note_path = asset.path.scheme(key='notePath').unc_path()

        note_id_path = "{note_path}/{note_id}".format(
            note_path=note_path,
            note_id=note_created['id']
        )
        if not os.path.exists(note_id_path):
            os.makedirs(note_id_path)

        return note_id_path

    def convert_msg_shot_to_scene(self, msg):
        if msg == "shot":
            return "scene"
        return msg
