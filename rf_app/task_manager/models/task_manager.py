import os
import json
import sys
import datetime

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

PROJECT_SETTING_PATH = "%s/-TEMP-/task_manager" % os.environ.get('RFSCRIPT')


class TaskManagerModel:
    def __init__(self, parent=None, sg_service=None):
        self.sg = sg_service
        self.order_by_options = ["date modified", "version"]
        self.selected_order_by_options = "date modified"

    def init_default_order_by_options(self, step):
        if step.get("code") == "Rig":
            self.set_selected_order_by_options("version")

    def get_order_by_options(self):
        return self.order_by_options

    def get_selected_order_by_options(self):
        return self.selected_order_by_options

    def set_selected_order_by_options(self, order_option_selected):
        self.selected_order_by_options = order_option_selected

    def list_file_dir(self, folder_path):
        return [f for f in os.listdir(folder_path) if os.path.isfile(os.path.join(folder_path, f))]

    def save_default_project(
        self,
        project_selected,
        user_selected,
        entity_selected,
        step_selected
    ):
        file_path = '{PROJECT_SETTING_PATH}/project_setting.json'.format(PROJECT_SETTING_PATH=PROJECT_SETTING_PATH)
        if not os.path.exists(file_path): 
            os.makedirs(os.path.dirname(file_path))
            with open(file_path, 'w') as f: 
                json.dump(dict(), f, indent=4)
                
        if os.stat(file_path).st_size == 0:
            with open(file_path, 'w+') as outfile:
                data = {
                    user_selected['name']: {
                        'project': project_selected['name'],
                        'entity': entity_selected,
                        'step': step_selected,
                        'updated': str(datetime.datetime.now())
                    }
                }
                json.dump(data, outfile, indent=4)
        else:
            with open(file_path, 'r+') as outfile:
                data_loaded = json.load(outfile)
                if user_selected['name'] in data_loaded:
                    data_loaded[user_selected['name']]['project'] = project_selected['name']
                    data_loaded[user_selected['name']]['entity'] = entity_selected
                    data_loaded[user_selected['name']]['step'] = step_selected
                    data_loaded[user_selected['name']]['updated'] = str(datetime.datetime.now())
                    outfile.seek(0)
                    json.dump(data_loaded, outfile, indent=4)
                    outfile.truncate()
                else:
                    pass

    def set_in_progress_task(self, task_selected):
        self.sg.update_in_progress_task(task_selected)

    def get_override_filename(self, workfile_name, override_name):
        workfile_name_split = workfile_name.split('.')
        workfile_name_split[0] = override_name
        result = ".".join(workfile_name_split)
        return result
        # result = workfile_name.split('.')[0].replace()
