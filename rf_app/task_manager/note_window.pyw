# -*- coding: utf-8 -*-
# v.0.0.1 polytag switcher
#Import python modules
import sys
import os
import getpass
import subprocess
from os.path import expanduser
from urllib import urlopen

from models.note import NoteModel

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)
import rf_config as config

# framework modules 
from rf_utils.context import context_info
from rf_utils import log_utils, project_info
from rf_utils.file_utils import forward_slash_to_backslash
from rf_utils.ui import load
from rf_utils.ui import stylesheet
from rf_utils.string_tools import truncate_str

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import QtGui, QtCore, QtWidgets

_title = 'Note'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'NoteUI'

userLog = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, userLog)
logger = log_utils.init_logger(logFile)


class NoteStandAlone(QtWidgets.QMainWindow):
    def __init__(self, parent=None, note_selected=None, options_data=None, task=None, sg_service=None):
        super(NoteStandAlone, self).__init__(parent)
        uiFile = '%s/note.ui' % moduleDir
        self.ui = load.setup_ui(uiFile, self)
        self.sg = sg_service
        self.init_gui()
        self.ui.show()
        self.loading_state()
        self.note_model = NoteModel(self, self.sg)
        QtCore.QCoreApplication.processEvents()
        self._note_path = forward_slash_to_backslash(
            self.note_model.get_note_unc_path(options_data, task, note_selected)
        )
        self.set_subject(note_selected['subject'])
        self.set_description(note_selected['content'])
        self.set_attachment(note_selected['attachments'])
        self.set_reference_layout(note_selected['sg_local_attachment_path'])
        self.loaded_state()

    def init_gui(self):
        self.ui.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.ui.content_layout.setSpacing(0)
        self.ui.content_layout.setContentsMargins(4, 4, 4, 4)

    def get_selected_reference(self):
        reference_selected = self.ui.reference_list.currentItem().data(QtCore.Qt.UserRole)
        return reference_selected

    def set_subject(self, subject):
        self.ui.subject.setText(subject)
        self.ui.subject.setStyleSheet("""
            background-color: #2e2e2e;
            font-weight: bold;
            font-size: 16px;
            color: #fff;
            padding: 18px;
        """)

    def set_description(self, description):
        self.ui.description.setText(description)
        self.ui.description.setStyleSheet("""
            background-color: #3e3e3e;
            font-size: 14px;
            color: #fff;
            padding: 18px;
        """)

    def set_attachment(self, attachments):
        row = 0
        # col = 0
        attachments_data = []
        for idx, attachment in enumerate(attachments):
            pic_wrapper = QtWidgets.QVBoxLayout()
            attachment = self.sg.get_attachment(attachment)
            pic = QtWidgets.QLabel()
            data = urlopen(attachment['image']).read()
            pixmap = QtGui.QPixmap()
            pixmap.loadFromData(data)
            pic.setPixmap(pixmap)
            pic.setStyleSheet("""
                background-color: #3e3e3e;
                padding: 9px;
                width: 200px;
                height: 200px;
            """)
            pic.show()
            pic_wrapper.addWidget(pic)
            self.ui.picture_layout.addLayout(pic_wrapper, row, idx)
            attachments_data.append(attachment)
            # col = col + 1
            # if col % 4 == 0:
            #     row = row + 1
            #     col = 0

        if len(attachments) > 0:
            horizontal_spacer = QtWidgets.QSpacerItem(500, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
            self.ui.attachment_action_layout.addItem(horizontal_spacer)

            explorer_btn = QtWidgets.QPushButton('Explorer', self)
            explorer_btn.clicked.connect(self.explorer_note_path)
            self.ui.attachment_action_layout.addWidget(explorer_btn)

            download_btn = QtWidgets.QPushButton('Download', self)
            download_btn.clicked.connect(lambda attachments_data=attachments_data, download_btn=download_btn: self.click_download_picture(attachments_data, download_btn))
            self.ui.attachment_action_layout.addWidget(download_btn)

    def set_reference_layout(self, local_attachment_path_str):
        if local_attachment_path_str:
            local_attachments_path = local_attachment_path_str.split(',')
            icon_path = '{core_path}/rf_app/task_manager/images/Maya2017.png'.format(core_path=core)
            for idx, reference in enumerate(local_attachments_path):
                data = {
                    'reference_path': reference
                }
                widget_item = QtWidgets.QListWidgetItem(self.ui.reference_list)
                widget_item.setIcon(QtGui.QIcon(icon_path))
                widget_item.setText(truncate_str(reference, 30))
                widget_item.setToolTip(reference)
                widget_item.setData(QtCore.Qt.UserRole, data)
                self.ui.reference_list.addItem(widget_item)
                self.ui.reference_list.setIconSize(QtCore.QSize(80, 80))

            if len(local_attachments_path) > 0:
                horizontal_spacer = QtWidgets.QSpacerItem(500, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
                self.ui.reference_action_layout.addItem(horizontal_spacer)

                reference_btn = QtWidgets.QPushButton('Import reference', self)
                reference_btn.clicked.connect(self.click_import_reference)
                self.ui.reference_action_layout.addWidget(reference_btn)

    def click_download_picture(self, attachments_data, download_btn):
        download_btn.setText('Now Loading... ')
        QtCore.QCoreApplication.processEvents()
        for attachment in attachments_data:
            self.sg.dowload_image(attachment, self._note_path)
        download_btn.setText('Download')
        self.explorer_note_path()

    def click_import_reference(self):
        reference_selected = self.get_selected_reference()
        print "Import reference", reference_selected['reference_path']

    def explorer_note_path(self):
        subprocess.Popen(r'explorer %s' % self._note_path)

    def loading_state(self):
        self.ui.loading.setText("Loading ...")
        self.ui.description.setVisible(False)
        self.ui.subject.setVisible(False)
        self.ui.loading.setVisible(True)

    def loaded_state(self):
        self.ui.description.setVisible(True)
        self.ui.subject.setVisible(True)
        self.ui.loading.setVisible(False)
