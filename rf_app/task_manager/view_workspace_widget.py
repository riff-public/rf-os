import os
import sys

from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCore

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
moduleDir = os.path.dirname(sys.modules[__name__].__file__)

from rf_utils import file_utils
from rf_utils import publish_info
from rf_utils.context import context_info
from rf_utils.widget import file_widget


class ViewWorkspaceUI(QtWidgets.QWidget):
    """ UI ViewWorkspaceUI """
    currentIndexChanged = QtCore.Signal(str)
    def __init__(self, parent=None):
        super(ViewWorkspaceUI, self).__init__(parent=parent)
        self.allLayout = QtWidgets.QVBoxLayout()

        self.layout = QtWidgets.QVBoxLayout()
        self.listWidget = file_widget.FileListWidget()
        self.layout.addWidget(self.listWidget)

        self.allLayout.addLayout(self.layout)
        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.allLayout)


class ViewWorkspace(ViewWorkspaceUI):
    """ Logic Part for viewing publish """
    def __init__(self, parent=None):
        super(ViewWorkspace, self).__init__(parent=parent)

    def update_input(self, ContextPathInfo):
        """ run here to activate ui """
        self.entity = ContextPathInfo
        self.populate_ui()

    def populate_ui(self):
        self.set_workspace()

    def set_workspace(self):
        files = self.list_workspace()
        self.listWidget.clear()
        self.listWidget.add_files(files)

    def list_workspace(self):
        """ list all workspace from publish _data file """
        publish = publish_info.RegisterAsset(self.entity)
        allData = publish.read_all_data()
        files = []

        for data in allData:
            if self.entity.step in data.keys():
                stepData = data[self.entity.step]

                for process, processData in stepData.iteritems():
                    if publish_info.Data.workspace in processData.keys():
                        workspaceFiles = processData[publish_info.Data.workspace]

                        for workspaceFile in workspaceFiles:
                            if not workspaceFile in files:
                                files.append(workspaceFile)

        return sorted(files)
