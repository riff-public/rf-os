import os
import sys
try:
	import maya.cmds as mc
except ImportError:
	pass

def generate_new_file(path, project=''):
    from rf_maya.environment import setup_config
    version = mc.about(v=True)

    return_path = setup_config.generate_empty_scene(path, version=version, project=project)
    return return_path

def import_file(path):
	return mc.file(path,  i=True, type='mayaAscii', options = 'v=0', pr = True, loadReferenceDepth = 'all')

# def import_mtr(path):
# 	from rftool.shade import shade_utils
# 	reload(shade_utils)
# 	obj = mc.ls(sl=True)[0] if mc.ls(sl=True) else ''
# 	namespace = (':').join(obj.split(':')[:-1]) if ':' in obj else None
# 	return shade_utils.apply_import_shade(path, targetNamespace=namespace)

def reference_file(namespace, path):
    from rf_maya.lib import abc_lib
    reload(abc_lib)
    abc_lib.check_plugin(abc_lib.Plugin.abcImport)
    return mc.file(path, r=True, ns=namespace)

def import_abc(path):
    from rf_maya.lib import abc_lib
    reload(abc_lib)
    return abc_lib.import_abc(path)

def import_mtr(targetGrp, path):
    from rftool.shade import shade_utils
    reload(shade_utils)
    obj = mc.ls(sl=True)[0] if mc.ls(sl=True) else ''
    namespace = (':').join(obj.split(':')[:-1]) if ':' in obj else None
    targetNamespace = namespace if namespace else guess_target_namespace(targetGrp)
    return shade_utils.apply_import_shade(path, targetNamespace=targetNamespace)

def guess_target_namespace(targetGrp):
    from rftool.utils import maya_utils
    namespaces = maya_utils.list_all_namespaces()
    namespace = [a for a in namespaces if mc.objExists('%s:%s' % (a, targetGrp))]
    return namespace[0] if namespace else ''
