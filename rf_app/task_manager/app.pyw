# -*- coding: utf-8 -*-
# v.0.0.1 - polytag switcher
# v.0.0.4 - Fix bug when Save++ to empty asset
#         - Use QSettings to store user preference locally instead of json
#         - Disable Open and Save++ button in standalone mode (for now)

_title = 'Task Manager'
_version = 'v.0.0.4'
_des = ''
uiName = 'TaskManagerUI'

#Import python modules
import sys
import os
import getpass
import subprocess
# import json
import tempfile
import re
from shutil import copy2
from settings.base import (
    NOTE_ICONS
)
from models import task_manager
from models.note import NoteModel

# import config
core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)
import rf_config as config

from sg_services import ShotgunServices
from custome_widget import QCustomQWidget, CustomListNotesWidget, CustomListStatusWidget, DialogAddProcessTemplate
from note_window import NoteStandAlone

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# framework modules
from rf_utils.context import context_info
from rf_utils import log_utils, project_info
from rf_utils import file_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet
from rf_utils import icon

from rf_utils import register_entity

from rf_utils.widget import input_widget, ad_input_widget, file_widget, display_widget, auto_build_widget, publish_space_widget, dialog
reload(file_widget)
from rf_app.info_panel.tabs import tasks as info_panel_tasks
# reload(info_panel_tasks)
from rf_utils.widget import media_widget
# reload(media_widget)

import view_workspace_widget
import dcc_hook
import sg_services

# reload(task_manager)

userLog = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, userLog)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide2', 'PySide'])
from Qt import wrapInstance, QtGui, QtCore, QtWidgets


entity_mapper = {
    'asset': 'Asset',
    'scene': 'Shot'
}

# PROJECT_SETTING_PATH = "%s/-TEMP-/task_manager" % os.environ.get('RFSCRIPT')

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)
noPreviewImg = '%s/images/icons/nopreview_icon.png' % module_dir


class StandAlone(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(StandAlone, self).__init__(parent)
        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        self.runningApp = None
        if config.isMaya:
            self.runningApp = 'maya'
            self.ui = load.setup_ui_maya(uiFile, parent)
        elif config.isNuke:
            self.runningApp = 'nuke'
            # if parent.metaObject().className() == 'Foundry::UI::DockMainWindow': # is Nuke
            self.ui = load.setup_ui_maya(uiFile, parent)
        else:
            self.ui = load.setup_ui(uiFile, self)
            self.ui.submit_btn.setHidden(True)
            self.ui.save_pushButton.setHidden(True)

        # ui tab
        self.tab_work = 'Work'
        self.tab_publish = 'Publish'
        self.tab_inputs = 'Inputs'
        self.tab_notes = 'Notes'
        self.tab_build = 'Auto Build'
        self.tab_status = 'Status'
        self.current_tab = self.tab_work
        # self.runningApp = 'maya' if config.isMaya else ''
        self.settings = QtCore.QSettings('Riff', uiName)

        self.users = []
        self.groups = []
        self.projects = []
        self.steps = []
        self.tasks = []
        self.status_list_icons = []
        self.process_dir = []
        self.app_dir = []
        self.workspace_dir = []
        self.task_selected = {}
        self.notes = []
        self.write_note_attachment = []
        self.radioChecked = ""
        self.asset_context = None
        self.search_text = ""
        self.checked_filter_user = None
        self.is_check_override = False
        self.sg = ShotgunServices()
        self.task_manager_model = task_manager.TaskManagerModel(self, self.sg)
        self.note_model = NoteModel(self, self.sg)
        self.status_list = self.sg.fetch_status_list()
        self.w = 985
        self.h = 560

        # internal vars for storing UI selection
        self._current_project = ''
        self._current_entity = ''
        self._current_step = ''

        self.init_gui()
        self.app_process()
        self.file_widget.hook = dcc_hook.reference_file

    def init_gui(self):
        self.setCentralWidget(self.ui)
        self.resize(self.w, self.h)
        self.setWindowTitle('%s %s %s' % (_title, _version, _des))
        self.setObjectName(uiName)
        app_icon = QtGui.QIcon()
        app_icon.addFile(icon.task_manager, QtCore.QSize(16,16))
        self.setWindowIcon(app_icon)

        self.ui.msg_show_task.setVisible(True)
        self.ui.add_process.setEnabled(False)
        self.ui.tabWidget.setCurrentIndex(0)
        self.ui.save_pushButton.setEnabled(False)
        self.init_widgets()
        self.init_set_media_widget()
        # self.ui.msg_show_task.setScaledContents(True)
        # self.gif_loading = QtGui.QMovie("Loading_Ring.gif", QtCore.QByteArray(), self)
        # self.gif_loading.setCacheMode(QtGui.QMovie.CacheAll)
        # self.gif_loading.setSpeed(100)
        # self.ui.msg_show_task.setMovie(self.gif_loading)
        self.ui.logo_label.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))
        self.show()

    def init_signals(self):
        self.ui.group_comboBox.currentIndexChanged.connect(self.group_combobox_changed)
        self.ui.user_comboBox.currentIndexChanged.connect(self.user_combobox_changed)
        self.ui.project_comboBox.currentIndexChanged.connect(self.project_combobox_changed)
        self.ui.entity_comboBox.currentIndexChanged.connect(self.entity_combobox_changed)
        self.ui.step_comboBox.currentIndexChanged.connect(self.step_combobox_changed)
        self.ui.showTask_pushButton.clicked.connect(self.fetch_tasks)
        self.ui.statusFilter_listWidget.itemClicked.connect(self.process_filter_tasks)
        self.ui.taskFilter_listWidget.itemClicked.connect(self.process_filter_tasks)
        self.ui.tagTaskFilter_listWidget.itemClicked.connect(self.process_filter_tasks)
        self.ui.typeFilter_listWidget.itemClicked.connect(self.process_filter_tasks)
        self.ui.task_listWidget.itemClicked.connect(self.task_changed)
        self.ui.process_comboBox.currentIndexChanged.connect(self.process_combobox_changed)
        self.ui.app_comboBox.currentIndexChanged.connect(self.app_combobox_changed)
        self.ui.task_radioButton.clicked.connect(lambda:self.radio_notes_changed(self.ui.task_radioButton))
        self.ui.asset_radioButton.clicked.connect(lambda:self.radio_notes_changed(self.ui.asset_radioButton))
        self.ui.note_listWidget.itemDoubleClicked.connect(self.note_double_clicked)
        if self.runningApp:
            self.file_widget.listWidget.itemDoubleClicked.connect(self.submitted)
            self.ui.submit_btn.clicked.connect(self.submitted)
        self.ui.save_pushButton.clicked.connect(self.save_as)
        self.ui.tabWidget.currentChanged.connect(self.tab_changed)
        self.ui.searchEdit.returnPressed.connect(self.search_task_change)
        self.ui.sortBy_comboBox.currentIndexChanged.connect(self.init_list_tasks_widget)
        self.ui.checkBox_filterUser.stateChanged.connect(self.onchange_filter_user_checkbox)
        self.ui.add_process.clicked.connect(self.show_add_dialog_process)
        self.ui.write_note_department_box.currentIndexChanged.connect(self.write_note_department_change)
        self.ui.write_note_submit_btn.clicked.connect(self.submit_write_note)
        self.write_note_attachment_box.multipleDropped.connect(self.write_note_attachment_changed)
        self.ui.overide_checkbox.stateChanged.connect(self.onchange_overide_filename)
        self.ui.order_options_combobox.currentIndexChanged.connect(self.order_options_combobox_changed)

    def init_widgets(self):
        """ init custom widgets """
        # view published workspace widget
        # self.workspace_widget = view_workspace_widget.ViewWorkspace()
        self.workspace_widget = publish_space_widget.ViewPublishSpace2()
        self.ui.publish_layout.addWidget(self.workspace_widget)

        # input widget
        self.input_widget = ad_input_widget.InputWidget(dccHook=dcc_hook)
        self.ui.input_layout.addWidget(self.input_widget)

        # work file widget
        self.file_widget = file_widget.FileListWidgetExtended()
        self.ui.workfile_layout.addWidget(self.file_widget)

        self.write_note_attachment_box = display_widget.DropUrlList()
        self.ui.horizontalLayout_8.addWidget(self.write_note_attachment_box)
        self.ui.horizontalLayout_8.setStretch(0, 2)
        self.ui.horizontalLayout_8.setStretch(1, 1)

        # add tabs
        self.build_window = QtWidgets.QWidget()
        self.build_tab_layout = QtWidgets.QVBoxLayout(self.build_window)
        self.ui.tabWidget.addTab(self.build_window, self.tab_build)

        self.build_widget = auto_build_widget.AutoBuildWidget()
        self.build_tab_layout.addWidget(self.build_widget)

        # add status tabs
        self.status_widget = info_panel_tasks.Ui()
        self.ui.tabWidget.addTab(self.status_widget, self.tab_status)

    def init_group_combobox(self):
        self.ui.group_comboBox.blockSignals(True)
        self.ui.group_comboBox.clear()
        for row, group in enumerate(self.groups):
            self.ui.group_comboBox.addItem(group.get('code'))
            self.ui.group_comboBox.setItemData(row, group, QtCore.Qt.UserRole)
        self.ui.group_comboBox.blockSignals(False)
        self.group_combobox_changed()

    def init_users_combobox(self):
        self.ui.user_comboBox.blockSignals(True)
        self.ui.user_comboBox.clear()
        group_selected = self.get_selected_group()
        if len(self.users) == 0:
            self.ui.user_comboBox.addItem("Not Found")
        else:
            group_selected['name'] = group_selected['code']
            self.ui.user_comboBox.addItem("Show all")
            self.ui.user_comboBox.setItemData(0, {'name': "Show all"}, QtCore.Qt.UserRole)
            self.ui.user_comboBox.addItem(group_selected['code'])
            self.ui.user_comboBox.setItemData(1, group_selected, QtCore.Qt.UserRole)
            for row, user in enumerate(self.users):
                self.ui.user_comboBox.addItem(user.get('name'))
                self.ui.user_comboBox.setItemData(row + 2, user, QtCore.Qt.UserRole)
        self.ui.user_comboBox.blockSignals(False)
        self.user_combobox_changed()

    def init_projects_combobox(self):
        self.ui.project_comboBox.blockSignals(True)
        self.ui.project_comboBox.clear()
        for row, project in enumerate(self.projects):
            self.ui.project_comboBox.addItem(project.get('name'))
            self.ui.project_comboBox.setItemData(row, project, QtCore.Qt.UserRole)
        self.ui.project_comboBox.blockSignals(False)
        self.project_combobox_changed()

    def init_entity_combobox(self):
        self.ui.entity_comboBox.blockSignals(True)
        self.ui.entity_comboBox.clear()
        items = [
            {'display': 'asset', 'value': 'Asset'},
            {'display': 'scene', 'value': 'Shot'}
        ]
        for row, item in enumerate(items):
            self.ui.entity_comboBox.addItem(item.get('display'))
            self.ui.entity_comboBox.setItemData(row, item, QtCore.Qt.UserRole)
        self.ui.entity_comboBox.blockSignals(False)
        self.entity_combobox_changed()

    def init_step_combobox(self):
        self.ui.step_comboBox.blockSignals(True)
        self.ui.step_comboBox.clear()
        for row, step in enumerate(self.steps):
            self.ui.step_comboBox.addItem(step.get('code'))
            self.ui.step_comboBox.setItemData(row, step, QtCore.Qt.UserRole)
        self.ui.step_comboBox.blockSignals(False)
        self.step_combobox_changed()

    def init_list_tasks_widget(self):
        self.ui.task_listWidget.clear()
        self.set_tasks_list_widget(self.tasks)

    def init_list_tasks_filtered_widget(self, tasks):
        self.ui.task_listWidget.clear()
        if self.search_text == "":
            self.set_tasks_list_widget(tasks)
        else:
            searched_tasks = filter(lambda task: self.search_text.lower().decode('utf8') in task['entity']['name'].lower().decode('utf8'), tasks)
            self.set_tasks_list_widget(searched_tasks)

    def init_list_status_filter(self):
        status_widget_data = self.get_status_widget()
        self.ui.statusFilter_listWidget.clear()
        show_all_item = QtWidgets.QListWidgetItem(self.ui.statusFilter_listWidget)
        show_all_item.setText("Show All ({})".format(len(self.tasks)))
        show_all_item.setData(QtCore.Qt.UserRole, {'count': len(self.tasks), 'display': 'Show All'})
        self.ui.statusFilter_listWidget.addItem(show_all_item)
        for status in status_widget_data:
            status_mapping = self.mapping_status(status['display'])
            display_text = "{status_display} ({status_count})".format(status_display=status_mapping['name'], status_count=status['count'])
            custom_status_list_widget = CustomListStatusWidget()
            custom_status_list_widget.setText(display_text)
            custom_status_list_widget.setIcon(status_mapping['code'])

            widget_item = QtWidgets.QListWidgetItem(self.ui.statusFilter_listWidget)
            widget_item.setSizeHint(custom_status_list_widget.sizeHint())
            widget_item.setData(QtCore.Qt.UserRole, status)

            self.ui.statusFilter_listWidget.addItem(widget_item)
            self.ui.statusFilter_listWidget.setItemWidget(widget_item, custom_status_list_widget)
        self.ui.statusFilter_listWidget.setCurrentRow(0)

    def init_list_task_filter(self):
        tasks_filter = self.get_task_filter_widget()
        self.ui.taskFilter_listWidget.clear()
        show_all_item = QtWidgets.QListWidgetItem(self.ui.taskFilter_listWidget)
        show_all_item.setText("Show All ({})".format(len(self.tasks)))
        show_all_item.setData(QtCore.Qt.UserRole, {'count': len(self.tasks), 'display': 'Show All'})
        self.ui.taskFilter_listWidget.addItem(show_all_item)
        for task in tasks_filter:
            display_text = "{content} ({task_count})".format(content=task['display'], task_count=task['count'])
            widget_item = QtWidgets.QListWidgetItem(self.ui.taskFilter_listWidget)
            widget_item.setText(display_text.decode('utf8'))
            widget_item.setData(QtCore.Qt.UserRole, task)
            self.ui.taskFilter_listWidget.addItem(widget_item)
        self.ui.taskFilter_listWidget.setCurrentRow(0)

    def init_list_tag_task_filter(self):
        tag_tasks_filter = self.get_tag_task_filter_widget()
        self.ui.tagTaskFilter_listWidget.clear()
        show_all_item = QtWidgets.QListWidgetItem(self.ui.tagTaskFilter_listWidget)
        show_all_item.setText("Show All ({})".format(len(self.tasks)))
        show_all_item.setData(QtCore.Qt.UserRole, {'count': len(self.tasks), 'display': 'Show All'})
        self.ui.tagTaskFilter_listWidget.addItem(show_all_item)
        for task in tag_tasks_filter:
            display_text = "{content} ({task_count})".format(content=task['display'], task_count=task['count'])
            widget_item = QtWidgets.QListWidgetItem(self.ui.tagTaskFilter_listWidget)
            widget_item.setText(display_text.decode('utf8'))
            widget_item.setData(QtCore.Qt.UserRole, task)
            self.ui.tagTaskFilter_listWidget.addItem(widget_item)
        self.ui.tagTaskFilter_listWidget.setCurrentRow(0)

    def init_list_type_filter(self):
        types_filter = self.get_type_filter_widget()
        self.ui.typeFilter_listWidget.clear()
        show_all_item = QtWidgets.QListWidgetItem(self.ui.typeFilter_listWidget)
        show_all_item.setText("Show All ({})".format(len(self.tasks)))
        show_all_item.setData(QtCore.Qt.UserRole, {'count': len(self.tasks), 'display': 'Show All'})
        self.ui.typeFilter_listWidget.addItem(show_all_item)
        for asset_type in types_filter:
            display_text = "{content} ({task_count})".format(content=asset_type['display'], task_count=asset_type['count'])
            widget_item = QtWidgets.QListWidgetItem(self.ui.typeFilter_listWidget)
            widget_item.setText(display_text.decode('utf8'))
            widget_item.setData(QtCore.Qt.UserRole, asset_type)
            self.ui.typeFilter_listWidget.addItem(widget_item)
        self.ui.typeFilter_listWidget.setCurrentRow(0)

    def init_list_process(self):
        self.ui.add_process.setEnabled(True)
        self.ui.process_comboBox.clear()
        process_path = self.get_process_path()

        # this function create process as task name automatically
        syncProcess = self.auto_process(self.task_selected, self.step_selected)

        self.process_dir = sorted(os.listdir(process_path))

        for row, process in enumerate(self.process_dir):
            self.ui.process_comboBox.addItem(process)

        # set auto selection
        items = [self.ui.process_comboBox.itemText(a) for a in range(self.ui.process_comboBox.count())]
        
        # set current 
        index = 0
        if syncProcess: 
            index = items.index(syncProcess) if syncProcess in items else 0
        else: 
            index = items.index('main') if 'main' in items else 0
        self.ui.process_comboBox.setCurrentIndex(index)

    def init_app_combobox(self):
        app_path = self.get_app_path()
        if app_path:
            dirs = file_utils.list_folder(app_path)

            if self.runningApp and self.runningApp not in dirs: 
                os.makedirs('%s/%s' % (app_path, self.runningApp))
                logger.info('Created %s/%s' % (app_path, self.runningApp))

            self.ui.app_comboBox.blockSignals(True)
            self.ui.app_comboBox.clear()
            self.app_dir = [folder for folder in os.listdir(app_path) if os.path.isdir(os.path.join(app_path, folder)) and folder != "output"]
            for row, app in enumerate(self.app_dir):
                self.ui.app_comboBox.addItem(app)

            if self.runningApp in self.app_dir:
                index = self.app_dir.index(self.runningApp)
                self.ui.app_comboBox.setCurrentIndex(index)

            self.ui.app_comboBox.blockSignals(False)
            self.init_work_list()

    def set_default_app(self): 
        app_path = self.get_app_path()
        print '-----------------', app_path

    def init_work_list(self):
        # update step
        # print("TEST INIT WORKLISTTTT")
        step = self.get_selected_step()
        self.asset_context.context.update(step=step['short_name'])
        self.file_widget.listWidget.clear()
        workspace_path = self.get_work_space_path()
        # print("workspace_path:", workspace_path)
        self.workspace_dir = self.task_manager_model.list_file_dir(workspace_path)
        order_option_selected = self.task_manager_model.get_selected_order_by_options()
        if order_option_selected == "date modified":
            self.workspace_dir.sort(key=lambda file: os.path.getmtime(os.path.join(workspace_path, file)))
        elif order_option_selected == "version":
            self.workspace_dir.sort()
        if len(self.workspace_dir) == 0:
            widget_item = QtWidgets.QListWidgetItem(self.file_widget.listWidget)
            widget_item.setText("Not found files.")
            self.file_widget.listWidget.addItem(widget_item)
            self.ui.submit_btn.setText("New File")
        else:
            for row, workspace in enumerate(self.workspace_dir[::-1]):
                # widget_item = QtWidgets.QListWidgetItem(self.file_widget.listWidget)
                # widget_item.setText(workspace.decode('utf8'))
                # self.file_widget.listWidget.addItem(widget_item)
                full_path = '%s/%s' % (workspace_path, workspace)
                self.file_widget.add_file(full_path.decode('utf8'))
                self.ui.submit_btn.setText("Open File")
            self.file_widget.listWidget.setCurrentRow(0)

        # save button enable
        self.ui.save_pushButton.setEnabled(os.path.exists(workspace_path))

    def init_note_list(self):
        self.ui.note_listWidget.clear()
        if len(self.notes) == 0:
            widget_item = QtWidgets.QListWidgetItem(self.ui.note_listWidget)
            widget_item.setText("Not Found")
            self.ui.note_listWidget.addItem(widget_item)
            return
        for row, note in enumerate(self.notes):
            if note.get('sg_status_list') != "clsd":
                note['subject'] = self.decode_text(note['subject'])
                note['content'] = self.decode_text(note['content'])
                note_custom_item_widget = CustomListNotesWidget(self, self.sg, note)
                note_custom_item_widget.setTextUp(note['subject'])
                note_custom_item_widget.setTextDown(note['content'])
                note_custom_item_widget.setIcon(note['sg_status_list'])
                widget_item = QtWidgets.QListWidgetItem(self.ui.note_listWidget)
                widget_item.setSizeHint(note_custom_item_widget.sizeHint())

                for icon in NOTE_ICONS:
                    if icon['code'] == note['sg_status_list']:
                        widget_item.setBackground(QtGui.QColor(icon['background_color']))
                        break

                widget_item.setData(QtCore.Qt.UserRole, note)
                self.ui.note_listWidget.addItem(widget_item)
                self.ui.note_listWidget.setItemWidget(widget_item, note_custom_item_widget)

    def init_write_note_components(self):
        self.init_write_note_department()
        # self.ui.note_listWidget.clear()

    def init_write_note_department(self):
        self.ui.write_note_department_box.blockSignals(True)
        self.ui.write_note_department_box.clear()
        for row, step in enumerate(self.steps):
            self.ui.write_note_department_box.addItem(step.get('code'))
            self.ui.write_note_department_box.setItemData(row, step, QtCore.Qt.UserRole)
        self.ui.write_note_department_box.blockSignals(False)
        self.write_note_department_change()

    def init_write_note_tasks(self, tasks):
        self.ui.write_note_tasks_box.blockSignals(True)
        self.ui.write_note_tasks_box.clear()
        for row, task in enumerate(tasks):
            self.ui.write_note_tasks_box.addItem(task.get('content'))
            self.ui.write_note_tasks_box.setItemData(row, task, QtCore.Qt.UserRole)
        self.ui.write_note_tasks_box.blockSignals(False)

    def init_order_by_options(self):
        order_by_options = self.task_manager_model.get_order_by_options()
        self.ui.order_options_combobox.blockSignals(True)
        self.ui.order_options_combobox.clear()
        for row, option in enumerate(order_by_options):
            self.ui.order_options_combobox.addItem(option)
            self.ui.order_options_combobox.setItemData(row, option, QtCore.Qt.UserRole)
        self.ui.order_options_combobox.blockSignals(False)
        self.task_manager_model.init_default_order_by_options(self.get_selected_step())
        self.set_default_order_option()

    def init_set_media_widget(self):
        self.displayLayout = QtWidgets.QVBoxLayout()
        self.mediaSplitter = QtWidgets.QSplitter()
        self.mediaSplitter.setOrientation(QtCore.Qt.Vertical)


        self.media_display_widget = media_widget.PreviewMedia()

        self.mediaSplitter.addWidget(self.media_display_widget)

        self.taskWidget = QtWidgets.QWidget(self.mediaSplitter)
        self.taskLayout = QtWidgets.QVBoxLayout(self.taskWidget)
        self.taskLayout.addWidget(self.ui.label_8)
        self.taskLayout.addWidget(self.ui.task_listWidget)

        self.ui.displayLayout.addWidget(self.mediaSplitter)

        self.mediaSplitter.setSizes([250, 300])
        self.mediaSplitter.setStretchFactor(0, 0)
        self.mediaSplitter.setStretchFactor(1, 1)
        # self.mediaSplitter.setHandleWidth(0)

    def preview(self, path):
        self.media_display_widget.loadMedia([path])

    def no_preview(self, entity=None):
        entity = self.asset_context if not entity else entity
        # self.media_display_widget.setText('%s\n No Hero' % entity.name)
        self.media_display_widget.mediaplayer.clear()
        self.media_display_widget.clear_view()
        self.media_display_widget.loadMedia(noPreviewImg)

    def show_preview(self):
        if self.asset_context.entity_type == context_info.ContextKey.asset:
            self.set_preview()

        elif self.asset_context.entity_type == context_info.ContextKey.scene: 
            self.set_playblast_preview()

    def set_preview(self):
        regInfo = register_entity.Register(self.asset_context)
        path = regInfo.get.preview(step='texture', look='main')
        iconDir = self.asset_context.path.icon().abs_path()
        iconName = self.asset_context.icon_name(ext='.png')
        iconPath = '%s/%s' % (iconDir, iconName)
        previewPath = path if path else iconPath if os.path.exists(iconPath) else ''
        self.preview(previewPath) if previewPath else self.no_preview()

    def set_playblast_preview(self): 
        path = self.asset_context.path.name().abs_path()
        mov = self.find_latest_playblast(path)

        self.media_display_widget.mediaplayer.clear()
        self.media_display_widget.clear_view()
        self.preview(mov[0]) if mov else self.no_preview()

    def find_latest_playblast(self, path, latest=True): 
        movExts = ['.mov']
        latests = []

        step = self.get_selected_step()['short_name']
        process = 'main'

        stepPath = '%s/%s' % (path, step)

        mediaPath = '%s/%s/output' % (stepPath, process)
        if os.path.exists(mediaPath): 
            files = file_utils.list_file(mediaPath)

            if files: 
                if any(os.path.splitext(a)[-1] in movExts for a in files): 
                    if latest: 
                        versions = [re.findall(r'v\d{3}', a)[0] for a in files if re.findall(r'v\d{3}', a)]
                        if versions:
                            highest = sorted(versions)[-1]
                            pickFiles = [a for a in files if highest in a]
                            pickFile = pickFiles[0] if pickFiles else files[-1]
                            mov = '%s/%s' % (mediaPath, pickFile)
                            latests.append(mov)
                    else: 
                        latests += ['%s/%s' % (mediaPath, a) for a in files if os.path.splitext(a)[-1] in movExts]

        return latests

    def fetch_notes(self):
        project = self.get_selected_project()
        task = self.get_selected_task()
        self.notes = self.sg.fetch_notes(project, task, self.radioChecked)
        self.init_note_list()

    def fetch_publish_workspace(self):
        self.workspace_widget.update_input(self.asset_context.copy())

    def fetch_status(self):
        if self.asset_context:
            self.status_widget.refresh(entity=self.asset_context.copy())
            self.status_widget.refresh_clicked()

    def get_my_account(self):
        return getpass.getuser()

    def get_selected_group(self):
        index = self.ui.group_comboBox.currentIndex()
        group_selected = self.ui.group_comboBox.itemData(index, QtCore.Qt.UserRole)
        return group_selected

    def get_selected_user(self):
        index = self.ui.user_comboBox.currentIndex()
        user_selected = self.ui.user_comboBox.itemData(index, QtCore.Qt.UserRole)
        return user_selected

    def get_selected_project(self):
        index = self.ui.project_comboBox.currentIndex()
        project_selected = self.ui.project_comboBox.itemData(index, QtCore.Qt.UserRole)
        return project_selected

    def get_selected_entity(self):
        entity_selected = self.ui.entity_comboBox.currentText()
        self._current_entity = entity_selected
        entity = entity_mapper[entity_selected]
        return entity

    def get_selected_step(self):
        index = self.ui.step_comboBox.currentIndex()
        step_selected = self.ui.step_comboBox.itemData(index, QtCore.Qt.UserRole)
        return step_selected

    def get_selected_task(self):
        task_selected = self.ui.task_listWidget.currentItem().data(QtCore.Qt.UserRole)
        return task_selected

    def get_current_task(self):
        current_task_index = self.ui.task_listWidget.currentRow()
        return current_task_index

    def get_selected_note(self):
        note_selected = self.ui.note_listWidget.currentItem().data(QtCore.Qt.UserRole)
        return note_selected

    def get_selected_app(self):
        app_selected = self.ui.app_comboBox.currentText()
        return app_selected

    def get_status_widget(self):
        status_data = []
        for task in self.tasks:
            status_text = task['sg_status_list']
            found_status = False
            if len(status_data) == 0:
                data = {
                    'display': status_text,
                    'count': 1
                }
                status_data.append(data)
                continue
            for item in status_data:
                if item["display"] == status_text:
                    found_status = True
                    item['count'] += 1
                    break
            if not found_status:
                data = {
                    'display': status_text,
                    'count': 1
                }
                status_data.append(data)
        return status_data

    def get_task_filter_widget(self):
        tasks_filter = []
        for task in self.tasks:
            task_content = task['content']
            found_task = False
            if len(tasks_filter) == 0:
                data = {
                    'display': task_content,
                    'count': 1
                }
                tasks_filter.append(data)
                continue
            for item in tasks_filter:
                if item["display"] == task_content:
                    found_task = True
                    item['count'] += 1
                    break
            if not found_task:
                data = {
                    'display': task_content,
                    'count': 1
                }
                tasks_filter.append(data)
        return tasks_filter

    def get_tag_task_filter_widget(self):
        tag_tasks_filter = []
        for task in self.tasks:
            tags = task['tags']
            for tag in tags:
                tag_task_content = tag['name']
                found_task = False
                if len(tag_tasks_filter) == 0:
                    data = {
                        'display': tag_task_content,
                        'count': 1
                    }
                    tag_tasks_filter.append(data)
                    continue
                for item in tag_tasks_filter:
                    if item["display"] == tag_task_content:
                        found_task = True
                        item['count'] += 1
                        break
                if not found_task:
                    data = {
                        'display': tag_task_content,
                        'count': 1
                    }
                    tag_tasks_filter.append(data)
        return tag_tasks_filter

    def get_type_filter_widget(self):
        types_filter = []
        entity_selected = self.get_selected_entity()
        for asset_type in self.tasks:
            if entity_selected == "Asset":
                type_name = asset_type['entity.Asset.sg_asset_type']
            elif entity_selected == "Shot":
                type_name = asset_type['entity.Shot.sg_episode.Scene.code']

            found_task = False
            if len(types_filter) == 0:
                data = {
                    'display': type_name,
                    'count': 1
                }
                types_filter.append(data)
                continue
            for item in types_filter:
                if item["display"] == type_name:
                    found_task = True
                    item['count'] += 1
                    break
            if not found_task:
                data = {
                    'display': type_name,
                    'count': 1
                }
                types_filter.append(data)
        return types_filter

    def get_options_data(self):
        group_selected = self.get_selected_group()
        user_selected = self.get_selected_user()
        project_selected = self.get_selected_project()
        entity_selected = self.get_selected_entity()
        step_selected = self.get_selected_step()

        data = {
            'group': group_selected,
            'user': user_selected,
            'project': project_selected,
            'entity': entity_selected,
            'step': step_selected
        }

        return data

    def get_process_path(self):
        context = context_info.Context()
        project = self.get_selected_project()
        entity = self.ui.entity_comboBox.currentText()
        step = self.get_selected_step()
        task = self.get_selected_task()
        entity_selected = self.get_selected_entity()

        if entity_selected == "Asset":
            task_key = 'entity.Asset.sg_asset_type'
        elif entity_selected == "Shot":
            task_key = 'entity.Shot.sg_episode.Scene.code'

        context.update(
            project=project['name'],
            entityType=entity,
            entityGrp=self.task_selected[task_key],
            # entityParent=self.task_selected['entity.Asset.sg_mainasset'],
            entity=self.task_selected['entity']['name'],
            step=step['short_name']
        )
        self.asset_context = context_info.ContextPathInfo(context=context)
        self.asset_context.context.update(task=task.get('content'))
        browse_process = os.path.split(self.asset_context.path.process().abs_path())[0]
        return browse_process

    def get_app_path(self):
        process_selected = self.ui.process_comboBox.currentText()
        # print("process_selected: ", process_selected)
        if process_selected:
            self.asset_context.context.update(process=process_selected, version="v001")
            appPath = self.asset_context.path.app().abs_path()
            browseAppPath = os.path.split(appPath)[0]
            return browseAppPath
        else:
            return None

    def get_work_space_path(self):
        app_selected = self.ui.app_comboBox.currentText()
        self.asset_context.context.update(app=app_selected)
        workspacePath = self.asset_context.path.workspace().abs_path()
        return workspacePath

    def get_selected_work(self):
        work_selected = self.file_widget.listWidget.currentItem().text()
        return work_selected

    def get_selected_write_note_department(self):
        index = self.ui.write_note_department_box.currentIndex()
        department_selected = self.ui.write_note_department_box.itemData(index, QtCore.Qt.UserRole)
        return department_selected

    def get_write_note_message(self):
        msg_comment = self.ui.write_note_message_box.toPlainText()
        return msg_comment

    def get_write_note_subject(self):
        subject = self.ui.subject_box.toPlainText()
        return subject

    def get_selected_write_note_task(self):
        index = self.ui.write_note_tasks_box.currentIndex()
        task_selected = self.ui.write_note_tasks_box.itemData(index, QtCore.Qt.UserRole)
        return task_selected

    def get_selected_order_options(self):
        index = self.ui.order_options_combobox.currentIndex()
        order_option_selected = self.ui.order_options_combobox.itemData(index, QtCore.Qt.UserRole)
        return order_option_selected

    def set_list_users(self):
        group_selected = self.get_selected_group()
        self.users = self.sg.get_list_users_by_group(group_selected)

    def set_list_groups(self):
        self.groups = self.sg.get_list_groups()

    def set_list_projects(self):
        self.projects = self.sg.get_list_project_by_user()

    def set_list_steps(self):
        entity_selected = self.get_selected_entity()
        self.steps = self.sg.get_list_step(entity_selected)

    def set_default_user(self):
        my_account = self.get_my_account()
        user_detail = self.sg.get_list_users_by_acc(my_account)
        if user_detail: 
            current_user_group = user_detail.get('groups', [])[0]

            if current_user_group: 
                index = self.ui.group_comboBox.findText(current_user_group['name'], QtCore.Qt.MatchFixedString)
                if index >= 0:
                    self.ui.group_comboBox.setCurrentIndex(index)
                index = self.ui.user_comboBox.findText(user_detail['name'], QtCore.Qt.MatchFixedString)
                if index >= 0:
                    self.ui.user_comboBox.setCurrentIndex(index)

    def set_default_radio_btn(self):
        self.radioChecked = "task"
        self.ui.task_radioButton.setChecked(True)

    def set_tasks_list_widget(self, tasks):
        sortBy = self.ui.sortBy_comboBox.currentText()
        # print tasks
        if sortBy == 'Due Date':
            sorted_tasks = sorted(tasks, key=lambda k: k['due_date'] if k['due_date'] else '9999999999')
        elif sortBy == 'Name':
            sorted_tasks = sorted(tasks, key=lambda k: k['entity']['name'])
        # print sorted_tasks
        entity = self.get_selected_entity()
        taskKey = 'entity.Asset.sg_asset_type' if entity == 'Asset' else 'entity.Shot.sg_sequence_code' if entity == 'Shot' else ''
        for row, task in enumerate(sorted_tasks):
            if task['sg_status_list'] == 'apr':
                task['due_date'] = None
            myQCustomQWidget = QCustomQWidget()
            myQCustomQWidget.setTextUp(task['entity']['name'])
            myQCustomQWidget.setTextDown(task['content'])
            myQCustomQWidget.setTextDown2(task[taskKey])
            myQCustomQWidget.setStartDateLabel(task['start_date'])
            myQCustomQWidget.setDueDateLabel(task['due_date'])
            myQCustomQWidget.setDurationLabel(task['due_date'])
            myQCustomQWidget.setIcon(task['sg_status_list'])

            widget_item = QtWidgets.QListWidgetItem(self.ui.task_listWidget)
            widget_item.setSizeHint(myQCustomQWidget.sizeHint())
            if row % 2 == 0:
                widget_item.setBackground(QtGui.QColor('#696969'))

            widget_item.setData(QtCore.Qt.UserRole, task)
            self.ui.task_listWidget.addItem(widget_item)
            self.ui.task_listWidget.setItemWidget(widget_item, myQCustomQWidget)

    # def set_default_project(self):
    #     project_selected = self.get_selected_project()
    #     entity_selected = self.ui.entity_comboBox.currentText()
    #     step_selected = self.get_selected_step()
    #     user_selected = self.get_selected_user()
    #     file_path = "{PROJECT_SETTING_PATH}/project_setting.json".format(PROJECT_SETTING_PATH=PROJECT_SETTING_PATH)
    #     if not os.path.exists(PROJECT_SETTING_PATH):
    #         os.makedirs(PROJECT_SETTING_PATH)
    #         open(file_path, 'w').close()
    #     if os.stat(file_path).st_size == 0:
    #         with open(file_path, 'w+') as outfile:
    #             data = {
    #                 user_selected['name']: {
    #                     'project': project_selected['name'],
    #                     'entity': entity_selected,
    #                     'step': step_selected
    #                 }
    #             }
    #             json.dump(data, outfile, indent=4)
    #     else:
    #         with open(file_path, 'r+') as outfile:
    #             data_loaded = json.load(outfile)
    #             if user_selected['name'] in data_loaded:
    #                 project_index = self.ui.project_comboBox.findText(
    #                     data_loaded[user_selected['name']]['project'],
    #                     QtCore.Qt.MatchFixedString
    #                 )
    #                 entity_index = self.ui.entity_comboBox.findText(
    #                     data_loaded[user_selected['name']]['entity'],
    #                     QtCore.Qt.MatchFixedString
    #                 )
    #                 if project_index >= 0:
    #                     self.ui.project_comboBox.setCurrentIndex(project_index)
    #                 if entity_index >= 0:
    #                     self.ui.entity_comboBox.setCurrentIndex(entity_index)

    #                 step_index = self.ui.step_comboBox.findText(
    #                     data_loaded[user_selected['name']]['step']['code'],
    #                     QtCore.Qt.MatchFixedString
    #                 )
    #                 if step_index >= 0:
    #                     self.ui.step_comboBox.setCurrentIndex(step_index)
    #             else:
    #                 data = {
    #                     user_selected['name']: {
    #                         'project': project_selected['name'],
    #                         'entity': entity_selected,
    #                         'step': step_selected
    #                     }
    #                 }
    #                 data_loaded.update(data)
    #                 outfile.seek(0)
    #                 json.dump(data_loaded, outfile, indent=4)
    #                 outfile.truncate()

    def set_write_note_tasks_combobox(self):
        department_selected = self.get_selected_write_note_department()
        options_data = self.get_options_data()
        task = self.get_selected_task()
        tasks = self.sg.get_tasks_write_note(department_selected, options_data, task)
        self.init_write_note_tasks(tasks)

    def set_app_default(self):
        pass
        # # step_text = self.ui.step_comboBox.currentText()
        # if config.isNuke:
        #     self.runningApp = "nuke"

    def select_task(self, current_task_index):
        self.ui.task_listWidget.setCurrentRow(current_task_index)
        # task.setSelected(self, True)

    def show_add_dialog_process(self):
        # self.dialog_process = DialogAddProcess(self)
        self.dialog_process = DialogAddProcessTemplate(self, self.asset_context)

    def group_combobox_changed(self):
        self.set_list_users()
        self.init_users_combobox()
        self.mapping_step_by_group()

    def user_combobox_changed(self):
        pass
        # self.set_list_projects()
        # self.init_projects_combobox()

    def project_combobox_changed(self):
        project = self.ui.project_comboBox.currentText()
        self._current_project = project
        self.projectInfo = project_info.ProjectInfo(project=project)
        self.projectInfo.set_project_env()

    def entity_combobox_changed(self):
        self.set_list_steps()
        self.init_step_combobox()

    def step_combobox_changed(self):
        step_selected = self.ui.step_comboBox.currentText()
        self._current_step = step_selected
        # self.ui.task_listWidget.clear()
        # self.ui.statusFilter_listWidget.clear()
        # self.ui.taskFilter_listWidget.clear()
        # self.ui.typeFilter_listWidget.clear()

    def process_combobox_changed(self):
        self.init_app_combobox()

    def app_combobox_changed(self):
        self.init_work_list()

    def radio_notes_changed(self, args):
        if self.ui.task_radioButton.isChecked() == True:
            self.radioChecked = "task"
        else:
            self.radioChecked = "asset"
        self.fetch_notes()

    def tab_changed(self, index):
        """ if tab changed, button command changed """
        self.current_tab = self.ui.tabWidget.tabText(index)
        self.ui.submit_btn.setEnabled(True)

        if self.current_tab == self.tab_work:
            if self.asset_context:
                self.init_work_list()

        elif self.current_tab == self.tab_publish:
            self.ui.submit_btn.setText('Checkout as work version')
            self.ui.save_pushButton.setEnabled(False)

        elif self.current_tab == self.tab_inputs:
            self.ui.submit_btn.setEnabled(False)
            self.ui.save_pushButton.setEnabled(False)
            if self.asset_context:
                self.input_widget.update_input(self.asset_context.copy())

        elif self.current_tab == self.tab_build:
            if self.asset_context:
                self.build_widget.update_input(self.asset_context.copy())

        elif self.current_tab == self.tab_status:
            if self.asset_context:
                self.status_widget.refresh(self.asset_context.copy())

    def write_note_department_change(self):
        self.set_write_note_tasks_combobox()

    def write_note_attachment_changed(self, files):
        self.write_note_attachment = []
        self.write_note_attachment = files
        self.write_note_attachment_box.clear()
        for row, file in enumerate(self.write_note_attachment):
            self.write_note_attachment_box.addItem(file)

    def search_task_change(self):
        self.search_text = self.ui.searchEdit.text()
        # self.search_text = text
        self.process_filter_tasks()

    def note_double_clicked(self):
        self.loading_start()
        QtCore.QCoreApplication.processEvents()
        note = self.get_selected_note()
        options_data = self.get_options_data()
        task = self.get_selected_task()
        self.open_note_window(note, options_data, task)
        self.loading_stop()

    def open_note_window(self, note, options_data, task):
        self.note_window = NoteStandAlone(self, note, options_data, task, self.sg)
        # self.note_window.show()

    def filter_tasks(self, filter_tasks):
        entity_selected = self.get_selected_entity()
        tasks_filtered = []
        for task in self.tasks:
            if entity_selected == "Asset":
                type_name = task['entity.Asset.sg_asset_type']
            elif entity_selected == "Shot":
                type_name = task['entity.Shot.sg_episode.Scene.code']
            if task['sg_status_list'] in filter_tasks and task['content'] in filter_tasks and type_name in filter_tasks:
                tags = task['tags']
                if tags:
                    for tag in tags:
                        tagName = tag['name']
                        if tagName in filter_tasks:
                            tasks_filtered.append(task)
                            break
                else:
                    tasks_filtered.append(task)
        # tasks_filtered = list(filter(lambda task: task['sg_status_list'] in filter_tasks and task['content'] in filter_tasks and type_name in filter_tasks, self.tasks))
        self.init_list_tasks_filtered_widget(tasks_filtered)

    def process_filter_tasks(self):
        current_status_filtered_item = self.ui.statusFilter_listWidget.currentItem().data(QtCore.Qt.UserRole)
        current_task_filtered_item = self.ui.taskFilter_listWidget.currentItem().data(QtCore.Qt.UserRole)
        current_tag_task_filtered_item = self.ui.tagTaskFilter_listWidget.currentItem().data(QtCore.Qt.UserRole)
        current_type_filtered_item = self.ui.typeFilter_listWidget.currentItem().data(QtCore.Qt.UserRole)

        if current_status_filtered_item['display'] == "Show All":
            status_filtered_item = list(map(lambda status: status['code'], self.status_list))
        else:
            status_filtered_item = [current_status_filtered_item['display']]

        if current_task_filtered_item['display'] == "Show All":
            tasks_filter = self.get_task_filter_widget()
            task_filtered_item = list(map(lambda task: task['display'], tasks_filter))
        else:
            task_filtered_item = [current_task_filtered_item['display']]

        if current_tag_task_filtered_item['display'] == "Show All":
            tag_tasks_filter = self.get_tag_task_filter_widget()
            tag_task_filtered_item = list(map(lambda task: task['display'], tag_tasks_filter))
        else:
            tag_task_filtered_item = [current_tag_task_filtered_item['display']]

        if current_type_filtered_item['display'] == "Show All":
            types_filter = self.get_type_filter_widget()
            type_filtered_item = list(map(lambda asset_type: asset_type['display'], types_filter))
        else:
            type_filtered_item = [current_type_filtered_item['display']]

        filter_items = [status_filtered_item, task_filtered_item ,tag_task_filtered_item , type_filtered_item]
        flat_filter_items = [item for sublist in filter_items for item in sublist]

        self.filter_tasks(flat_filter_items)

    def task_changed(self):
        self.loading_start()
        self.set_default_radio_btn()
        QtCore.QCoreApplication.processEvents()
        try:
            self.task_selected = self.get_selected_task()
            self.step_selected = self.get_selected_step()
            self.set_app_default()
            self.fetch_notes()
            self.init_list_process()
            self.fetch_status()
            self.fetch_publish_workspace()
            self.init_write_note_components()
            self.init_order_by_options()
            self.show_preview()
        except Exception as e:
            tb = sys.exc_info()[2]
            print e
            # print "line number : %s", tb.tb_lineno
            self.file_widget.listWidget.clear()
            widget_item = QtWidgets.QListWidgetItem(self.file_widget.listWidget)
            widget_item.setText(str(e))
            self.file_widget.listWidget.addItem(widget_item)
        finally:
            self.loading_stop()

    def onchange_filter_user_checkbox(self, val):
        if self.ui.checkBox_filterUser.isChecked():
            self.ui.user_comboBox.setEnabled(True)
            self.ui.group_comboBox.setEnabled(True)
        else:
            self.ui.user_comboBox.setEnabled(False)
            self.ui.group_comboBox.setEnabled(False)
        self.checked_filter_user = val

    def onchange_overide_filename(self, val):
        if self.ui.overide_checkbox.isChecked():
            self.is_check_override = True
            work_file_selected = self.get_selected_work()
            work_file_name_split = work_file_selected.split(".")
            self.ui.override_filename_box.setEnabled(True)
            self.ui.version_work_txt.setText(".".join(work_file_name_split[1:]))
            self.ui.override_filename_box.setText(work_file_name_split[0])
        else:
            self.ui.override_filename_box.setEnabled(False)
            self.ui.version_work_txt.setText("")

    def order_options_combobox_changed(self):
        order_option_selected = self.get_selected_order_options()
        self.task_manager_model.set_selected_order_by_options(order_option_selected)
        self.init_work_list()

    def set_default_order_option(self):
        step = self.get_selected_step()
        if step.get("code") == "Rig":
            order_option_index = self.ui.order_options_combobox.findText(
                "version",
                QtCore.Qt.MatchFixedString
            )
            if order_option_index >= 0:
                self.ui.order_options_combobox.setCurrentIndex(order_option_index)

    def fetch_tasks(self):
        # self.note_model.test_asset(self.task_selected, step_selected)
        try:
            self.loading_start()
            self.ui.showTask_pushButton.setEnabled(False)
            QtCore.QCoreApplication.processEvents()
            data = self.get_options_data()
            if self.valid_data_show_tasks():
                if self.ui.checkBox_filterUser.isChecked():
                    self.tasks = self.sg.get_tasks_list(data)
                    self.init_list_tasks_widget()
                    self.init_list_status_filter()
                    self.init_list_task_filter()
                    self.init_list_tag_task_filter()
                    self.init_list_type_filter()
                else:
                    self.tasks = self.sg.get_tasks_list_no_filter_user(data)
                    self.init_list_tasks_widget()
                    self.init_list_status_filter()
                    self.init_list_task_filter()
                    self.init_list_tag_task_filter()
                    self.init_list_type_filter()
            else:
                self.show_warning_msg()
        except Exception as e:
            self.loading_stop()
            print e
        finally:
            project_selected = self.get_selected_project()
            user_selected = self.get_selected_user()
            entity_selected = self.ui.entity_comboBox.currentText()
            step_selected = self.get_selected_step()
            self.task_manager_model.save_default_project(
                project_selected,
                user_selected,
                entity_selected,
                step_selected
            )
            self.loading_stop()
            self.ui.showTask_pushButton.setEnabled(True)

    def auto_process(self, taskEntity, stepEntity):
        syncTaskProcess = self.sg.get_task_process_sync()
        step = stepEntity.get('code')
        taskName = taskEntity.get('content')
        process = taskName
        defaultApps = ['maya']
        print 'step', step, syncTaskProcess

        if step in syncTaskProcess:
            stepPath = self.asset_context.path.step().abs_path()
            processPath = '%s/%s' % (stepPath, process)

            for app in defaultApps:
                dirPath = '%s/%s' % (processPath, app)
                if not os.path.exists(dirPath):
                    os.makedirs(dirPath)
                    logger.debug('process created "%s"' % process)

            return process

    def submitted(self):
        """ button signal """
        # if tab mode
        if self.current_tab == self.tab_work:
            if len(self.workspace_dir) == 0:
                self.create_new_file()
            elif len(self.workspace_dir) != 0:
                self.open_file()
            else:
                print 'cannot submit'

        if self.current_tab == self.tab_publish:
            self.open_as()

    def submit_write_note(self):
        self.ui.write_note_submit_btn.setText('Uploading...')
        QtCore.QCoreApplication.processEvents()
        subject = self.get_write_note_subject()
        msg = self.get_write_note_message()
        department = self.get_selected_write_note_department()
        task = self.get_selected_write_note_task()
        current_task_index = self.get_current_task()
        options_data = self.get_options_data()
        try:
            if msg == "" or department is None or task is None:
                QtWidgets.QMessageBox.warning(self, "Message", "Please input data!")
                self.ui.write_note_submit_btn.setText('Submit Note')
                return
            else:
                self.note_model.submit_write_note(
                    subject,
                    msg,
                    department,
                    task,
                    options_data,
                    self.write_note_attachment
                )
                self.fetch_tasks()
                self.select_task(current_task_index)
                self.fetch_notes()
                QtWidgets.QMessageBox.information(
                    self,
                    "Message",
                    "Upload comment successfully."
                )
            self.ui.write_note_submit_btn.setText('Submit Note')
        except Exception as e:
            QtWidgets.QMessageBox.critical(self, "Message", "Error:" + str(e) + ". Please contact admin.")
            self.ui.write_note_submit_btn.setText('Submit Note')

    def save_as(self):
        try:
            # app_selected = self.get_selected_app()
            workspace_path = self.asset_context.path.workspace().abs_path()
            version = file_utils.calculate_version(file_utils.list_file(workspace_path))
            self.asset_context.context.update(version=version)
            if not self.is_check_override:
                file_name = self.asset_context.work_name(user=True)
            else:
                workfile_name = self.asset_context.work_name(user=True)
                override_name = self.ui.override_filename_box.text()
                file_name = self.task_manager_model.get_override_filename(workfile_name, override_name)
            saveFileName = '%s/%s' % (workspace_path, file_name)
            if saveFileName:
                if config.isMaya:
                    import maya.cmds as mc
                    # save as
                    mc.file(rename=saveFileName)
                    mc.file(save=True, f=True, typ='mayaAscii')
                    self.sg.update_ip_condition(self.task_selected)
                elif config.isNuke:
                    print "Save nuke file..."
                    import nuke
                    nuke.scriptSaveAs(filename=saveFileName, overwrite=-1)

                self.init_work_list()
        except Exception as e:
            print "Cannot save file", str(e)
            QtWidgets.QMessageBox.critical(self, "Message", "Error:" + str(e) + ". Please contact admin.")
        finally:
            task_selected = self.get_selected_task()
            os.environ["task"] = str(task_selected["content"])
            # current_task_index = self.get_current_task()
            # self.task_manager_model.set_in_progress_task(task_selected)
            # self.fetch_tasks()
            # self.select_task(current_task_index)

    def valid_data_show_tasks(self):
        group_text = self.ui.group_comboBox.currentText()
        user_text = self.ui.user_comboBox.currentText()
        project_text = self.ui.project_comboBox.currentText()
        entity_text = self.ui.entity_comboBox.currentText()
        step_text = self.ui.step_comboBox.currentText()

        if group_text != "" and user_text != "" and user_text != "Not Found" and project_text != "" and entity_text != "" and step_text != "":
            return True
        else:
            return False

    def show_warning_msg(self):
        # self.ui.msg_show_task.setVisible(True)
        self.ui.msg_show_task.setStyleSheet('color: Yellow')
        self.ui.msg_show_task.setText("Information missing.")

    def loading_start(self):
        # self.ui.msg_show_task.setVisible(True)
        self.ui.msg_show_task.setText("Loading ...")
        # self.gif_loading.start()

    def loading_stop(self):
        self.ui.msg_show_task.setText("")
        # self.ui.msg_show_task.setVisible(False)
        # self.gif_loading.stop()

    def create_new_file(self):
        try:
            file_name = self.asset_context.work_name(user=True)
            workspace_path = self.asset_context.path.workspace().abs_path()
            project = self.get_selected_project()
            app_selected = self.get_selected_app()
            if app_selected == "maya":

                # TEMPLATE_NEW_FILE = '{core_path}/core/rf_template/{project_name}/asset/ma.ma'.format(
                #     core_path=os.environ.get('RFSCRIPT'),
                #     project_name=project['name']
                # )
                # ----- generate new file
                temp_fp, temp_path = tempfile.mkstemp(suffix='.ma')
                os.close(temp_fp)
                TEMPLATE_NEW_FILE = dcc_hook.generate_new_file(path=temp_path, project=project['name'])
                # print TEMPLATE_NEW_FILE
            elif app_selected == "nuke":
                TEMPLATE_NEW_FILE = '{core_path}/core/rf_template/{project_name}/asset/template.nk'.format(
                    core_path=os.environ.get('RFSCRIPT'),
                    project_name=project['name']
                )
            dest_file = "{workspace_path}/{file_name}".format(workspace_path=workspace_path, file_name=file_name)
            if not os.path.exists(TEMPLATE_NEW_FILE):
                TEMPLATE_NEW_FILE = TEMPLATE_NEW_FILE.replace(project['name'], 'default')
            copy2(TEMPLATE_NEW_FILE, dest_file)
            self.init_work_list()
            print("Create file {} successfully.".format(dest_file))
        except Exception as e:
            print e
        finally:
            task_selected = self.get_selected_task()
            current_task_index = self.get_current_task()
            self.task_manager_model.set_in_progress_task(task_selected)
            self.open_file()
            os.environ["task"] = str(task_selected["content"])
            # self.fetch_tasks()
            # self.select_task(current_task_index)

    def open_file(self):
        title = 'Opening File'
        message = 'Pls Ans, You opening file for working or checking ?'
        # result = QtWidgets.QMessageBox.question(self, title, message, QtWidgets.QMessageBox.Yes, QtWidgets.QMessageBox.No)
        box = QtWidgets.QMessageBox()
        box.setIcon(QtWidgets.QMessageBox.Question)
        box.setWindowTitle('Open File')
        box.setText('Are You opening file for Work or Check ?')
        box.setStandardButtons(QtWidgets.QMessageBox.Yes|QtWidgets.QMessageBox.No)
        buttonY = box.button(QtWidgets.QMessageBox.Yes)
        buttonY.setText('Work')
        buttonN = box.button(QtWidgets.QMessageBox.No)
        buttonN.setText('Check')
        result = box.exec_()
        if result == QtWidgets.QMessageBox.Yes:
            self.sg.update_ip_condition(self.task_selected)
        work_selected = self.get_selected_work()
        app_selected = self.get_selected_app()
        loadNoReference = self.ui.loadNoReference_checkBox.isChecked()
        loadString = 'all' if not loadNoReference else 'none'
        workspace_path = self.asset_context.path.workspace().abs_path()
        dest_file = "{workspace_path}/{work_selected}".format(workspace_path=workspace_path, work_selected=work_selected)
        try:
            if config.isMaya:
                import maya.cmds as mc
                from rftool import preload
                print 'open file with Maya.'
                MAYA_PATH = 'C:\\Program Files\\Autodesk\\Maya2018\\bin\\maya.exe'
                mayaExts = {'.ma':'mayaAscii', '.mb':'mayaBinary'}
                openArgs = {}
                dst_fn, dst_ext = os.path.splitext(dest_file)
                if dst_ext in mayaExts:
                    openArgs['typ'] = mayaExts[dst_ext]

                # save_confirm = False
                unknown_file = mc.file(loc=True, q=True)
                sceneModify = mc.file(q=True, mf=True)

                if unknown_file == "unknown":
                    mc.file(dest_file, o=True, f=True, loadReferenceDepth=loadString, **openArgs)
                    self.asset_context.write_env()
                    entity = context_info.ContextPathInfo(path=dest_file)
                    preload.check(entity)
                    return
                else:
                    if sceneModify:
                        confirm = mc.confirmDialog(t="Confirm", m="Do you want to save file ?", button = ["Save", "No"], cancelButton = "No", dismissString='Abort')
                        if confirm == "Abort":
                            return
                        if confirm == "Save":
                            mc.file(save=True, f=True)

                    mc.file(dest_file, o=True, f=True, loadReferenceDepth=loadString, **openArgs)
                    self.asset_context.write_env()
                    # preload
                    entity = context_info.ContextPathInfo(path=dest_file)
                    preload.check(entity)
            elif config.isNuke:
                print "Open nuke file..."
                import nuke
                nuke.scriptOpen(dest_file)
            # else:
            #     print 'open file standalone: {}'.format(dest_file)
            #     if app_selected == "maya":
            #         subprocess.Popen([MAYA_PATH, dest_file], stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
            #     if app_selected == "nuke":
            #         subprocess.Popen([config.Software.data['Nuke11']['path']])

        except Exception as e:
            print "Cannot open file", str(e)
            QtWidgets.QMessageBox.critical(self, "Message", "Error:" + str(e) + ". Please contact admin.")
            return
        finally:
            task_selected = self.get_selected_task()
            os.environ["task"] = str(task_selected["content"])
            # current_task_index = self.get_current_task()
            # self.task_manager_model.set_in_progress_task(task_selected)
            # self.fetch_tasks()
            # self.select_task(current_task_index)

    def open_as(self):
        """ open as workfile """
        # selected path
        path = self.workspace_widget.listWidget.selected_file()

        if config.isMaya and path:
            title = 'Open Publish File'
            message = 'Checkout as your work file?'
            result = QtWidgets.QMessageBox.question(self, title, message, QtWidgets.QMessageBox.Yes, QtWidgets.QMessageBox.No)

            if result == QtWidgets.QMessageBox.Yes:
                import maya.cmds as mc

                # get work file name
                work_path = self.get_work_space_path()
                new_version = file_utils.calculate_version(file_utils.list_file(work_path))
                self.asset_context.context.update(version=new_version)
                new_file = self.asset_context.work_name(user=True)
                workfile = '%s/%s' % (work_path, new_file)

                dest_file = file_utils.copy(path, workfile)
                mc.file(dest_file, o=True, f=True)
                self.asset_context.write_env()
                # set shotgun status
                status = 'ip'
                self.sg.update_task_status(self.task_selected, status)
                # print self.task_selected
                # print self.step_selected

    def mapping_step_by_group(self):
        if len(self.steps) != 0:
            mapper_group_step = {
                'Animator': 'Animation',
                'art': 'Design',
                'comper': 'Comp',
                'fx': 'FX',
                'Layout': 'Layout',
                'lighter': 'Light',
                'modeler': 'Model',
                'rigger': 'Rig',
                'setdressor': 'SetDress',
                'sim': 'Sim',
                'groomer': 'Groom',
                'tech': 'TechAnim',
                'qc': 'AssetQc', 
            }
            group_selected = self.get_selected_group()
            if group_selected['code'] in mapper_group_step:
                map_text = mapper_group_step[group_selected['code']]
                index = self.ui.step_comboBox.findText(map_text, QtCore.Qt.MatchFixedString)
                if index >= 0:
                    self.ui.step_comboBox.setCurrentIndex(index)

    def mapping_status(self, status_code):
        for status in self.status_list:
            if status['code'] == status_code:
                return status

    def decode_text(self, txt):
        if txt is None:
            txt = "-"
        return txt.decode('utf8')

    def app_process(self):
        self.set_list_groups()
        self.init_group_combobox()
        self.set_list_users()
        self.init_users_combobox()
        self.set_list_projects()
        self.init_projects_combobox()
        self.init_signals()
        self.set_default_user()
        self.init_entity_combobox()
        self.mapping_step_by_group()
        # self.set_default_project()
        self.read_settings()

    def read_settings(self):
        # project
        project = self.settings.value("project")
        if project:
            project_index = self.ui.project_comboBox.findText(project, QtCore.Qt.MatchFixedString)
            if project_index >= 0:
                self.ui.project_comboBox.setCurrentIndex(project_index)

        # entity
        entity = self.settings.value("entity")
        if entity:
            entity_index = self.ui.entity_comboBox.findText(entity, QtCore.Qt.MatchFixedString)
            if entity_index >= 0:
                self.ui.entity_comboBox.setCurrentIndex(entity_index)

        # step
        step = self.settings.value("step")
        if step:
            step_index = self.ui.step_comboBox.findText(step, QtCore.Qt.MatchFixedString)
            if step_index >= 0:
                self.ui.step_comboBox.setCurrentIndex(step_index)

    def write_settings(self):
        self.settings.setValue("project", self._current_project)
        self.settings.setValue("entity", self._current_entity)
        self.settings.setValue("step", self._current_step)

    def closeEvent(self, event):
        print '{} is closing...'.format(uiName)
        self.write_settings()

def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = StandAlone(maya_win.getMayaWindow())
        return myApp

    if config.isNuke: 
        from rftool.utils.ui import nuke_win 
        logger.info('Run in Nuke\n')
        myApp = StandAlone(nuke_win._nuke_main_window())
        return myApp

    else:
        logger.info('Run in standalone\n')
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        myApp = StandAlone()
        stylesheet.set_default(app)
        sys.exit(app.exec_())


if __name__ == '__main__':
    show()
