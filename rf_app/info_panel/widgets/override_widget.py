# -*- coding: utf-8 -*-
import os 
import sys 
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from Qt import QtCore
from Qt import QtGui 
from Qt import QtWidgets

modulePath = sys.modules[__name__].__file__
moduleDir  = os.path.dirname(modulePath)

class Image: 
    backIcon =  '%s/icons/back.png' % os.path.split(moduleDir)[0]


class Color: 
    default = 'text-align: left; color: rgb(180, 220, 240);'

class OverrideWidget(QtWidgets.QWidget):
    """docstring for OverrideWidget"""
    def __init__(self, parent=None):
        super(OverrideWidget, self).__init__(parent)
        self.parent = parent
        self.layout = QtWidgets.QHBoxLayout()
        self.button = QtWidgets.QPushButton()
        self.button.setFlat(True)
        font = QtGui.QFont()
        font.setPointSize(8)
        self.button.setFont(font)

        self.spacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.layout.addWidget(self.button)
        self.layout.addItem(self.spacer)
        self.layout.setSpacing(0)
        self.layout.setContentsMargins(0, 0, 0, 0)

        self.layout.setStretch(0, 0)
        self.layout.setStretch(1, 1)

        self.setLayout(self.layout)

    def set_text(self, text): 
        self.button.setText(text)

    def set_override(self, entityName): 
        self.button.setText('( %s )' % entityName)
        self.button.setIcon(QtGui.QIcon(Image.backIcon))
        self.button.setStyleSheet(Color.default)
        self.button.setLayoutDirection(QtCore.Qt.RightToLeft)

    def clear(self): 
        self.button.setText('')
        self.button.setIcon(QtGui.QIcon(''))

