# -*- coding: utf-8 -*-
import os 
import sys 
from Qt import QtCore
from Qt import QtGui 
from Qt import QtWidgets
from functools import partial
from collections import OrderedDict 

from .. import tasks_config
reload(tasks_config)


class StepCheckBoxUi(QtWidgets.QWidget):
    """docstring for Ui"""
    def __init__(self, parent=None):
        super(StepCheckBoxUi, self).__init__(parent)
        self.layout = QtWidgets.QGridLayout()
        self.statusWidgetDict = OrderedDict()
        self.setLayout(self.layout)


class StepCheckBox(StepCheckBoxUi):
    """docstring for StepComboBox"""
    def __init__(self):
        super(StepCheckBox, self).__init__()

    def add_ui(self,results):
        for i in reversed(range(self.layout.count())): 
            self.layout.itemAt(i).widget().deleteLater()

        stepList = []
        resultList = []
        for taskEntity in results[::-1]:
            stepEntity = taskEntity.get('step')
            department = stepEntity.get('name') if stepEntity else 'No Department'

            type = taskEntity.get('entity').get('type')
            if type == 'Shot':
                stepConfig = tasks_config.Tasks.sceneTasks
            elif type == 'Asset':
                stepConfig = tasks_config.Tasks.assetTasks

            if not department in stepList and department in stepConfig:
                stepList.append(department)

        for step in stepConfig:
            if step in stepList:
                resultList.append(step)

        self.statusWidgetDict = OrderedDict()
        column = 1
        row = 0
        for label in resultList:
            if column == 5:
                row+=1
                column=1
            checkBox = QtWidgets.QCheckBox(label)
            checkBox.stateChanged.connect(partial(self.set_filter_step))
            self.layout.addWidget(checkBox, row, column, 1, 1)
            self.statusWidgetDict[label] = checkBox
            # if label in note_config.Default.statusDefault: 
            #     checkBox.setChecked(True)

            column+=1
        return self.statusWidgetDict

    def set_filter_step(self, *args):
        checkList = []
        for label, checkBox in self.statusWidgetDict.iteritems():
            if checkBox.isChecked():
                checkList.append(checkBox)
        return checkList



