import os
import sys
from urllib import urlopen

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

modulePath = sys.modules[__name__].__file__
moduleDir  = os.path.dirname(modulePath)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

class Image: 
    separator = '%s/icons/separator.png' % os.path.split(moduleDir)[0]
    img = '%s/icons/img.png' % os.path.split(moduleDir)[0]
    loading = '%s/icons/loading60' % os.path.split(moduleDir)[0]

class Color: 
    date = 'color: rgb(100, 140, 200);'
    link = 'color: rgb(100, 200, 100);'
    version = 'color: rgb(220, 160, 100)'
    button = 'background-color: rgb(40, 40, 40)'
    status = 'color: rgb(40, 40, 40);'





class NoteItem(QtWidgets.QWidget):
    """docstring for NoteItem"""
    def __init__(self, parent=None):
        super(NoteItem, self).__init__(parent=parent)
        self.set_ui()


    def set_ui(self):
        self.layout = QtWidgets.QVBoxLayout()
        self.itemHorizontalLayout = QtWidgets.QHBoxLayout()
        self.sideInfoLayout = QtWidgets.QVBoxLayout()
        self.infoGridLayout = QtWidgets.QGridLayout()

        # add widgets 
        self.userIcon = QtWidgets.QLabel()
        self.userIcon.setAlignment(QtCore.Qt.AlignCenter)
        self.userName = QtWidgets.QLabel()
        self.userName.setAlignment(QtCore.Qt.AlignCenter)
        font = QtGui.QFont()
        font.setBold(True)
        self.userName.setFont(font)
        self.commentLayout = QtWidgets.QVBoxLayout()
        self.date = QtWidgets.QLabel()
        self.date.setStyleSheet(Color.date)
        font = QtGui.QFont()
        font.setBold(True)
        self.date.setFont(font)

        # task 
        self.stepsLabel = QtWidgets.QLabel()
        self.stepsLabel.setStyleSheet(Color.link)
        font = QtGui.QFont()
        font.setBold(True)
        self.stepsLabel.setFont(font)

        self.versionLabel = QtWidgets.QLabel()
        self.versionLabel.setStyleSheet(Color.version)
        font = QtGui.QFont()
        font.setBold(True)
        self.versionLabel.setFont(font)

        self.commentBody = QtWidgets.QPlainTextEdit()
        self.commentBody.setMaximumSize(QtCore.QSize(260, 100))
        self.commentBody.setFrameShadow(QtWidgets.QFrame.Plain)
        self.commentBody.setFrameShape(QtWidgets.QFrame.HLine)
        self.commentBody.setEnabled(True)
        self.commentSpacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.imageGridLayout = QtWidgets.QGridLayout()

        self.sideSpacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.line = QtWidgets.QLabel()
        self.line.setPixmap(QtGui.QPixmap(Image.separator))
        self.line.setAlignment(QtCore.Qt.AlignRight)

        # status 
        self.subLayout = QtWidgets.QGridLayout()
        self.statusLabel = QtWidgets.QLabel('Status')
        self.status = QtWidgets.QLabel()
        # self.status.setFont(font)
        self.status.setAlignment(QtCore.Qt.AlignCenter)
        self.typeLabel = QtWidgets.QLabel('Type')
        self.noteType = QtWidgets.QLabel()
        self.noteType.setAlignment(QtCore.Qt.AlignCenter)
        self.noteType.setFont(font)

        # add to layouts 

        # self.subLayout.addWidget(self.statusLabel, 0, 0, 1, 1)
        self.subLayout.addWidget(self.status, 1, 1, 1, 1)
        # self.subLayout.addWidget(self.typeLabel, 1, 0, 1, 1)
        self.subLayout.addWidget(self.noteType, 0, 1, 1, 1)

        # main grid layout
        self.sideInfoLayout.addLayout(self.infoGridLayout)
        self.sideInfoLayout.addItem(self.sideSpacer)

        self.infoGridLayout.addWidget(self.userIcon, 0, 0, 1, 1)
        self.infoGridLayout.addWidget(self.userName, 1, 0, 1, 1)
        self.infoGridLayout.addLayout(self.subLayout, 2, 0, 1, 1)
        # self.infoGridLayout.addWidget(self.commentLabel, 3, 0, 1, 1)

        # adjust ratio 
        self.sideInfoLayout.setStretch(0, 0)
        self.sideInfoLayout.setStretch(1, 1)

        self.subLayout.setColumnStretch(0, 0)
        self.subLayout.setColumnStretch(1, 1)

        self.infoGridLayout.setColumnStretch(0, 0)
        self.infoGridLayout.setColumnStretch(1, 1)

        self.itemHorizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.imageGridLayout.setContentsMargins(8, 8, 0, 8)

        self.layout.setSpacing(0)
        self.itemHorizontalLayout.setSpacing(0)
        self.commentLayout.setSpacing(0)

        self.commentLayout.setStretch(0, 0)
        self.commentLayout.setStretch(1, 0)
        self.commentLayout.setStretch(2, 0)
        self.commentLayout.setStretch(3, 1)

        self.commentLayout.addWidget(self.date)
        self.commentLayout.addWidget(self.stepsLabel)
        self.commentLayout.addWidget(self.versionLabel)
        self.commentLayout.addWidget(self.commentBody)
        self.commentLayout.addLayout(self.imageGridLayout)
        self.commentLayout.addItem(self.commentSpacer)

        self.itemHorizontalLayout.addLayout(self.sideInfoLayout)
        self.itemHorizontalLayout.addLayout(self.commentLayout)

        self.layout.addLayout(self.itemHorizontalLayout)
        self.layout.addWidget(self.line)
        self.setLayout(self.layout)

        # setStyleSheet
        # self.textShot.setFont(QtGui.QFont("Arial", 9, QtGui.QFont.Bold))
        # self.textFrameRange.setFont(QtGui.QFont("Arial", 8))


    def set_date(self, datetime): 
        text = datetime.strftime('%Y-%b-%d %H:%M.%S')
        self.date.setText(text)

    def set_comment_size(self, size): 
        self.commentBody.setMaximumSize(QtCore.QSize(260, size))

    def set_comment(self, text): 
        self.commentBody.setPlainText(text)
        self.resize_text_content()

    def set_user_icon(self, imagePath): 
        self.userIcon.setPixmap(QtGui.QPixmap(imagePath).scaled(60, 60, QtCore.Qt.KeepAspectRatio))

    def set_name(self, name): 
        self.userName.setText(name)

    def set_link(self, steps, tasks): 
        step = '; '.join(steps)
        task = '; '.join(tasks)
        taskLimit = []
        for i, task in enumerate(tasks): 
            if i % 3 == 0 and not i == 0: 
                taskLimit.append('\n%s' % task)
            else: 
                taskLimit.append(task)
        task = '; '.join(taskLimit)

        message = '%s   [ %s ]' % (step, task)
        if not step and not task: 
            message = 'All Departments'
        self.stepsLabel.setText(message)

    def set_versions(self, versions): 
        text = '; '.join(versions) if versions else ' - '
        self.versionLabel.setText(text)

    def set_status(self, status, bgColor=''): 
        color = ''
        if bgColor: 
            color = '%s;%s' % (Color.status, bgColor)
        self.status.setText(status)
        self.status.setStyleSheet(color)

    def set_type(self, text): 
        self.noteType.setText(text)

    def set_type_color(self, bgColor): 
        color = '%s;%s' % (Color.status, bgColor)
        self.noteType.setStyleSheet(color)

    def resize_text_content(self): 
        lines = self.commentBody.document().blockCount()
        if lines > 6: 
            mul = lines - 5
            height = 100 + (mul * 10)
            self.commentBody.setMaximumSize(QtCore.QSize(260, height))
        else: 
            mul = lines
            height = 50 * lines
            self.commentBody.setMaximumSize(QtCore.QSize(260, height))

    def add_image(self, imgIndex, data=None, imgPath=''): 
        button = QtWidgets.QPushButton()
        button.setStyleSheet(Color.button)
        # data = urlopen(imgUrl).read()
        if data: 
            icon = QtGui.QPixmap()
            icon.loadFromData(data)
            button.setIcon(icon)
        if imgPath: 
            icon = QtGui.QIcon(imgPath)
            button.setIcon(icon)


        button.setIconSize(QtCore.QSize(100, 100))
        button.setMaximumSize(100, 100)
        button.setMinimumSize(100, 100)
        self.imageGridLayout.addWidget(button, 0, imgIndex, 1, 1) 
        return button

    def add_attachments(self, imgDatas): 
        i = 0 
        for data in imgDatas: 
            self.add_image(i, data)
            i+=1 




        # font = self.commentBody.document().defaultFont()    # or another font if you change it
        # fontMetrics = QtGui.QFontMetrics(font)      # a QFontMetrics based on our font
        # textSize = fontMetrics.size(0, text)

        # textWidth = textSize.width() + 30       # constant may need to be tweaked
        # textHeight = textSize.height() + 30     # constant may need to be tweaked

        # self.commentBody.setMinimumSize(textWidth, textHeight)  # good if you want to insert this into a layout
        # self.commentBody.resize(textWidth, textHeight)

    # def set_text_frame(self, text):
    #     self.textFrameRange.setText(text)

    # def set_text_status(self, message):
    #     self.textstatus.setText(message)

    # def set_shot_color(self, colors):
    #     self.textShot.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))

    # def set_range_color(self, colors):
    #     self.textFrameRange.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))

    # def set_status_color(self, colors):
    #     self.textstatus.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))

    # def get_text_shot(self):
    #     return self.textShot.text()