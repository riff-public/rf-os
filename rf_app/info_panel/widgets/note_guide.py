# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:\Dropbox\script_server\core\rf_app\info_panel\widgets\note_guide.ui'
#
# Created: Tue May 19 18:48:03 2020
#      by: pyside2-uic  running on PySide2 2.0.0~alpha0
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(458, 373)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.mainLayout = QtWidgets.QGridLayout()
        self.mainLayout.setObjectName("mainLayout")
        self.comment_label = QtWidgets.QLabel(self.centralwidget)
        self.comment_label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTop|QtCore.Qt.AlignTrailing)
        self.comment_label.setObjectName("comment_label")
        self.mainLayout.addWidget(self.comment_label, 3, 0, 1, 1)
        self.img1 = QtWidgets.QLabel(self.centralwidget)
        self.img1.setObjectName("img1")
        self.mainLayout.addWidget(self.img1, 4, 1, 1, 1)
        self.img2 = QtWidgets.QLabel(self.centralwidget)
        self.img2.setObjectName("img2")
        self.mainLayout.addWidget(self.img2, 5, 1, 1, 1)
        self.user_label = QtWidgets.QLabel(self.centralwidget)
        self.user_label.setObjectName("user_label")
        self.mainLayout.addWidget(self.user_label, 1, 0, 1, 1)
        self.userIcon_label = QtWidgets.QLabel(self.centralwidget)
        self.userIcon_label.setObjectName("userIcon_label")
        self.mainLayout.addWidget(self.userIcon_label, 0, 0, 1, 1)
        self.gridLayout_4 = QtWidgets.QGridLayout()
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.status_label = QtWidgets.QLabel(self.centralwidget)
        self.status_label.setObjectName("status_label")
        self.gridLayout_4.addWidget(self.status_label, 0, 0, 1, 1)
        self.status = QtWidgets.QLabel(self.centralwidget)
        self.status.setObjectName("status")
        self.gridLayout_4.addWidget(self.status, 0, 1, 1, 1)
        self.type_label = QtWidgets.QLabel(self.centralwidget)
        self.type_label.setObjectName("type_label")
        self.gridLayout_4.addWidget(self.type_label, 1, 0, 1, 1)
        self.type = QtWidgets.QLabel(self.centralwidget)
        self.type.setObjectName("type")
        self.gridLayout_4.addWidget(self.type, 1, 1, 1, 1)
        self.mainLayout.addLayout(self.gridLayout_4, 2, 0, 1, 1)
        self.plainTextEdit = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.plainTextEdit.setObjectName("plainTextEdit")
        self.mainLayout.addWidget(self.plainTextEdit, 3, 1, 1, 1)
        self.mainLayout.setColumnStretch(0, 1)
        self.verticalLayout.addLayout(self.mainLayout)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 458, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtWidgets.QApplication.translate("MainWindow", "MainWindow", None, -1))
        self.comment_label.setText(QtWidgets.QApplication.translate("MainWindow", "Comment:", None, -1))
        self.img1.setText(QtWidgets.QApplication.translate("MainWindow", "img1", None, -1))
        self.img2.setText(QtWidgets.QApplication.translate("MainWindow", "img2", None, -1))
        self.user_label.setText(QtWidgets.QApplication.translate("MainWindow", "user name", None, -1))
        self.userIcon_label.setText(QtWidgets.QApplication.translate("MainWindow", "user icon", None, -1))
        self.status_label.setText(QtWidgets.QApplication.translate("MainWindow", "status", None, -1))
        self.status.setText(QtWidgets.QApplication.translate("MainWindow", "Open", None, -1))
        self.type_label.setText(QtWidgets.QApplication.translate("MainWindow", "type", None, -1))
        self.type.setText(QtWidgets.QApplication.translate("MainWindow", "Fix", None, -1))

