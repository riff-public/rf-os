# -*- coding: utf-8 -*-
import os 
import sys 
from Qt import QtCore
from Qt import QtGui 
from Qt import QtWidgets


class TaskComboBoxUi(QtWidgets.QWidget):
    """docstring for Ui"""
    def __init__(self, parent=None):
        super(TaskComboBoxUi, self).__init__(parent)
        self.layout = QtWidgets.QHBoxLayout()
        # title 
        self.stepLabel = QtWidgets.QLabel('Task')
        self.comboBox = QtWidgets.QComboBox()

        self.layout.addWidget(self.stepLabel)
        self.layout.addWidget(self.comboBox)

        self.layout.setStretch(0, 1)
        self.layout.setStretch(1, 4)

        self.layout.setSpacing(4)
        self.layout.setContentsMargins(0, 0, 0, 0)

        self.setLayout(self.layout)


class TaskComboBox(TaskComboBoxUi):
    """docstring for TaskComboBox"""
    def __init__(self):
        super(TaskComboBox, self).__init__()


    def refresh(self, entity, sg=None): 
        """ init step list """ 
        if not sg: 
            from rf_utils.sg import sg_process 
            sg = sg_process.sg 
        
        