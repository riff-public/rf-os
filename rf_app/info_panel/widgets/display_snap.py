from Qt import QtWidgets
from Qt import QtCore
from Qt import QtGui

class CustomListWidget(QtWidgets.QListWidget):
    deleted = QtCore.Signal(object)
    def __init__(self, parent=None):
        super(CustomListWidget, self).__init__(parent)

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Delete:
            self._del_item()

    def _del_item(self):
        for item in self.selectedItems():
            self.takeItem(self.row(item))
            self.deleted.emit(item)

class DisplaySnap(QtWidgets.QWidget):
    """docstring for DisplaySnap"""
    def __init__(self, parent=None):
        super(DisplaySnap, self).__init__(parent=parent)
        self.layout = QtWidgets.QHBoxLayout()
        self.displayWidget = CustomListWidget()
        self.displayWidget.setViewMode(QtWidgets.QListView.IconMode)
        self.displayWidget.setIconSize(QtCore.QSize(64, 64))
        self.layout.addWidget(self.displayWidget)

        self.layout.setSpacing(0)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

    def add_item(self, path): 
        item = QtWidgets.QListWidgetItem(self.displayWidget)
        iconPath = path
        iconWidget = QtGui.QIcon()
        iconWidget.addPixmap(QtGui.QPixmap(iconPath), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        item.setIcon(iconWidget)
        item.setData(QtCore.Qt.UserRole, path)
        return item 

    def get_images(self): 
        items = [self.displayWidget.item(a).data(QtCore.Qt.UserRole) for a in range(self.displayWidget.count())]
        return items

    def clear(self): 
        self.displayWidget.clear()


        
