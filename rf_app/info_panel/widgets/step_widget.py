# -*- coding: utf-8 -*-
import os 
import sys 
from Qt import QtCore
from Qt import QtGui 
from Qt import QtWidgets


class StepComboBoxUi(QtWidgets.QWidget):
    """docstring for Ui"""
    currentIndexChanged = QtCore.Signal(dict)
    def __init__(self, parent=None):
        super(StepComboBoxUi, self).__init__(parent)
        self.layout = QtWidgets.QHBoxLayout()
        # title 
        self.stepLabel = QtWidgets.QLabel('Department')
        self.comboBox = QtWidgets.QComboBox()

        self.layout.addWidget(self.stepLabel)
        self.layout.addWidget(self.comboBox)

        self.layout.setStretch(0, 1)
        self.layout.setStretch(1, 4)

        self.layout.setSpacing(4)
        self.layout.setContentsMargins(0, 0, 0, 0)

        self.setLayout(self.layout)


class StepComboBox(StepComboBoxUi):
    """docstring for StepComboBox"""
    def __init__(self):
        super(StepComboBox, self).__init__()

    def refresh(self, entity): 
        """ init step list """ 
        steps = []
        if entity.entity_type == 'asset': 
            steps = entity.projectInfo.step.list_asset()
        if entity.entity_type == 'scene': 
            steps = entity.projectInfo.step.list_scene()

        self.comboBox.blockSignals(True)
        self.comboBox.clear()
        
        activeIndex = 0 
        for i, step in enumerate(steps): 
            self.comboBox.addItem(step)
            sgStep = entity.projectInfo.step.sg_step(entity.entity_type, step)
            self.comboBox.setItemData(i, (step, sgStep), QtCore.Qt.UserRole)

            if step == entity.step: 
                activeIndex = i

        self.comboBox.blockSignals(False)
        self.sync_current(entity)

    def sync_current(self, entity): 
        """ sync current step to entity """ 
        steps = [self.comboBox.itemData(a, QtCore.Qt.UserRole)[0] for a in range(self.comboBox.count())]
        if entity.step in steps: 
            activeIndex = steps.index(entity.step)
            self.comboBox.setCurrentIndex(activeIndex)
            
            # if index is 0, emit signals 
            if activeIndex == 0: 
                self.comboBox.currentIndexChanged.emit(True)
        else: 
            # if no step to sync 
            self.comboBox.currentIndexChanged.emit(True)

        self.currentIndexChanged.emit(True)
