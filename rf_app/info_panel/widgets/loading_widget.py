import os
import sys
from urllib import urlopen

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

modulePath = sys.modules[__name__].__file__
moduleDir  = os.path.dirname(modulePath)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

class Icon: 
    image1 = '%s/icons/loading60.gif' % os.path.split(moduleDir)[0]
    image2 = '%s/icons/loading32.gif' % os.path.split(moduleDir)[0]


class LoadingCircle(QtWidgets.QWidget):
    """docstring for LoadingCircle"""
    def __init__(self, image=1, parent=None):
        super(LoadingCircle, self).__init__(parent=parent)
        self.layout = QtWidgets.QVBoxLayout()
        self.label = QtWidgets.QLabel()
        icon = Icon.image1 
        if image == 2: 
            icon = Icon.image2

        self.movie = QtGui.QMovie(icon)
        self.label.setMovie(self.movie)
        self.layout.addWidget(self.label)
        self.layout.setSpacing(0)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)
        self.loading = False 

    def start(self): 
    	self.label.setVisible(True)
    	self.movie.start()
        self.loading = True

    def stop(self): 
    	# self.movie.stop()
    	self.label.setVisible(False)
        self.loading = False 
