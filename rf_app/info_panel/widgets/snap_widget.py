import os
import sys
from datetime import datetime

from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCore

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

modulePath = sys.modules[__name__].__file__
moduleDir  = os.path.dirname(modulePath)

class DimScreen(QtWidgets.QSplashScreen):
    snapped = QtCore.Signal(str)
    """ snap screen shot with rubber band """
    """ darken the screen by making splashScreen """
    # app = QtWidgets.QApplication.instance() -> for maya
    # app = QtWidgets.QApplication(sys.argv) -> for standalone

    def __init__(self, isMaya):
        """"""
        if isMaya:
            app = QtWidgets.QApplication.instance()
        else:
            app = QtWidgets.QApplication.instance()
            if not app:
                app = QtWidgets.QApplication(sys.argv)

        screenGeo = app.desktop().screenGeometry()
        width = screenGeo.width()
        height = screenGeo.height()
        fillPix = QtGui.QPixmap(width, height)
        fillPix.fill(QtGui.QColor(1,1,1))

        super(DimScreen, self).__init__(fillPix)
        self.havePressed = False
        self.origin = QtCore.QPoint(0,0)
        self.end = QtCore.QPoint(0,0)
        self.rubberBand = None


        self.setWindowState(QtCore.Qt.WindowFullScreen)
        #self.setBackgroundRole(QtWidgets.QPalette.Dark)
        self.setWindowOpacity(0.4)


    def mousePressEvent(self, event):
        self.havePressed = True
        self.origin = event.pos()

        if not self.rubberBand:
            self.rubberBand = QtWidgets.QRubberBand(QtWidgets.QRubberBand.Rectangle, self)
        self.rubberBand.setGeometry(QtCore.QRect(self.origin, QtCore.QSize()))
        self.rubberBand.show()

    def mouseMoveEvent(self, event):
        self.rubberBand.setGeometry(QtCore.QRect(self.origin, event.pos()).normalized())

    def mouseReleaseEvent(self, event):
        self.rubberBand.hide()
        if self.havePressed == True:
            self.end = event.pos()
            self.hide()

            output = self.capture()
        QtWidgets.QApplication.restoreOverrideCursor()

    def capture(self):
        outputFile = self.get_output()
        if not os.path.exists(os.path.dirname(outputFile)):
            os.makedirs(os.path.dirname(outputFile))

        QtGui.QPixmap.grabWindow(QtWidgets.QApplication.desktop().winId(), self.origin.x(), self.origin.y(), self.end.x()-self.origin.x(), self.end.y()-self.origin.y()).save(outputFile, 'png')
        self.snapped.emit(outputFile)
        # logger.info(outputFile)
        return outputFile

    def get_output(self):
        filename = datetime.now().strftime("%y-%m-%d_%H-%M-%S")
        outputPath = '%s/tmpCapture/capture_%s.png' % (os.environ.get('TEMP'), filename)
        return outputPath


class SnapButton(QtWidgets.QWidget):
    """docstring for SnapButton"""
    captured = QtCore.Signal(str)
    precaptured = QtCore.Signal(bool)
    def __init__(self, captureCommand=None, parent=None):
        super(SnapButton, self).__init__(parent=parent)
        self.captureCommand = captureCommand
        self.w = 1280
        self.h = 1024
        self.allLayout = QtWidgets.QVBoxLayout()

        self.button = QtWidgets.QPushButton()

        self.allLayout.addWidget(self.button)
        self.allLayout.setSpacing(0)
        self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.allLayout)

        self.preCapScreen = DimScreen(isMaya=True)
        self.preCapScreen.snapped.connect(self.emit_path)

        self.init_signals()

    def init_signals(self):
        self.button.clicked.connect(self.capture)

    def capture(self):
        self.precaptured.emit(True)
        if not self.captureCommand:
            self.preCapScreen.show()
            QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.CrossCursor)

        else:
            result = self.captureCommand(self.w, self.h)
            self.captured.emit(result)

    def emit_path(self, path):
        self.captured.emit(path)

    def set_icon(self, iconPath):
        if os.path.exists(iconPath):
            iconWidget = QtGui.QIcon()
            iconWidget.addPixmap(QtGui.QPixmap(iconPath),QtGui.QIcon.Normal,QtGui.QIcon.Off)
            self.button.setIcon(iconWidget)
            self.button.setIconSize(QtCore.QSize(30, 30))


# display 

class CustomListWidget(QtWidgets.QListWidget):
    deleted = QtCore.Signal(object)
    def __init__(self, parent=None):
        super(CustomListWidget, self).__init__(parent)

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Delete:
            self._del_item()

    def _del_item(self):
        for item in self.selectedItems():
            self.takeItem(self.row(item))
            self.deleted.emit(item)

class DisplaySnap(QtWidgets.QWidget):
    """docstring for DisplaySnap"""
    selected = QtCore.Signal(object)
    def __init__(self, parent=None):
        super(DisplaySnap, self).__init__(parent=parent)
        self.layout = QtWidgets.QHBoxLayout()
        self.displayWidget = CustomListWidget()
        self.displayWidget.setViewMode(QtWidgets.QListView.IconMode)
        self.displayWidget.setIconSize(QtCore.QSize(64, 64))
        self.layout.addWidget(self.displayWidget)

        self.layout.setSpacing(0)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)
        self.displayWidget.itemClicked.connect(self.item_selected)

    def add_item(self, path, preview): 
        item = QtWidgets.QListWidgetItem(self.displayWidget)
        iconPath = preview
        iconWidget = QtGui.QIcon()
        iconWidget.addPixmap(QtGui.QPixmap(iconPath), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        item.setIcon(iconWidget)
        item.setData(QtCore.Qt.UserRole, [path, preview])
        return item 

    def get_images(self): 
        items = [self.displayWidget.item(a).data(QtCore.Qt.UserRole) for a in range(self.displayWidget.count())]
        paths = [path for path, preview in items]
        return paths

    def clear(self): 
        self.displayWidget.clear()

    def item_selected(self, item): 
        self.selected.emit(item.data(QtCore.Qt.UserRole))



class Config:
    movPreview = '%s/icons/etc/mov_preview_icon.png' % moduleDir
    etcPreview = '%s/icons/etc/etc_preview_icon.png' % moduleDir
    imgExt = ['.jpg', '.png', '.tif', '.gif']
    movExt = ['.mov', '.avi', '.mp4']


class DropUrlList(QtWidgets.QListWidget):
    """ subclass QLineEdit for dragdrop events """
    dropped = QtCore.Signal(list)
    multipleDropped = QtCore.Signal(list)

    def __init__(self, parent=None):
        super(DropUrlList, self).__init__(parent)
        self.setAcceptDrops(True)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls:
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            links = []
            for url in event.mimeData().urls():
                links.append(str(url.toLocalFile()))
            self.dropped.emit(links[-1])
            self.multipleDropped.emit(links)
        else:
            event.ignore()

# drop preview 

class DropUrlLabel(QtWidgets.QLabel):
    """ subclass QLineEdit for dragdrop events """
    dropped = QtCore.Signal(list)
    multipleDropped = QtCore.Signal(list)
    def __init__(self, parent=None):
        super(DropUrlLabel, self).__init__(parent)
        self.setAcceptDrops(True)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls:
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            links = []
            for url in event.mimeData().urls():
                links.append(str(url.toLocalFile()))
            self.dropped.emit(links[-1])
            self.multipleDropped.emit(links)
        else:
            event.ignore()


class Display(QtWidgets.QWidget) :
    dropped = QtCore.Signal(object)
    def __init__(self) :
        super(Display, self).__init__()

        self.allLayout = QtWidgets.QVBoxLayout()
        self.display = DropUrlLabel()
        self.allLayout.addWidget(self.display)

        self.allLayout.setSpacing(0)
        self.allLayout.setContentsMargins(2, 2, 2, 2)
        self.setLayout(self.allLayout)
        self.display.setAlignment(QtCore.Qt.AlignCenter)
        self.currentPath = ''

        # self.display.dropped.connect(self.call_back)
        self.display.multipleDropped.connect(self.call_backs)
        self.customExt = dict()
        self.autoPreview = True

    def set_label(self, text):
        self.display.setText(text)


    def call_back(self, path):
        self.set_display(path)
        self.dropped.emit(path)

    def call_backs(self, paths): 
        for path in paths: 
            preview = self.get_preview_image(path)
            if self.autoPreview: 
                self.set_display(path, preview)
            self.dropped.emit([path, preview])

    def set_display(self, path, preview='', width=None, height=None):
        self.currentPath = path
        preview = path if not preview else preview
        width = self.display.frameGeometry().width() if not width else width
        height = self.display.frameGeometry().height() if not height else height
        return self.display.setPixmap(QtGui.QPixmap(preview).scaled(width, height, QtCore.Qt.KeepAspectRatio))

    def thumbnail_file(self): 
        filename = datetime.now().strftime("%y-%m-%d_%H-%M-%S")
        outputPath = '%s/tmpCapture/capture_%s.jpg' % (os.environ.get('TEMP'), filename)
        return outputPath

    def set_style(self, style):
        self.setStyleSheet(style)

    def clear(self):
        self.display.clear()

    def get_preview_image(self, path):
        name, ext = os.path.splitext(path)
        ext = ext.lower()
        if ext in Config.imgExt:
            return path
        elif ext in Config.movExt:
            return self.generate_video_preview(path)
        elif ext in self.customExt:
            return self.customExt[ext]
        else:
            return Config.etcPreview

    def generate_video_preview(self, path): 
        from rf_utils import media_lib
        dst = self.thumbnail_file()
        dst = media_lib.mov_thumbnail(src=path, ext='.jpg', dst=dst)
        return dst


class Color: 
    projectLabel = 'color: rgb(220, 220, 220);'
    entityLabel = 'color: rgb(180, 220, 240);'
    grpLabel = 'color: rgb(160, 160, 160);'
    submitButton = 'background-color: rgb(80, 160, 240);'
    snapButton = 'background-color: rgb(72, 72, 72);'
    addInputButton = 'color: rgb(200, 200, 200); background-color: rgb(72, 72, 72);'
    dropBG = 'background-color: rgb(100, 100, 100);'
    clearButton = 'background-color: rgb(160, 100, 100);'


class Default: 
    refreshIcon = '%s/icons/refresh.png' % os.path.split(moduleDir)[0]
    captureIcon = '%s/icons/snap2.png' % os.path.split(moduleDir)[0]
    dropFileIcon = '%s/icons/drop_file.png' % os.path.split(moduleDir)[0]
    clearIcon = '%s/icons/clear.png' % os.path.split(moduleDir)[0]
    multipleIcon = '%s/icons/multiple_files.png' % os.path.split(moduleDir)[0]
    noTask = 'No Task'


class Ui(QtWidgets.QWidget):
    """docstring for Ui"""
    def __init__(self, parent=None):
        super(Ui, self).__init__(parent=parent)
        self.layout = QtWidgets.QVBoxLayout()

        # display 
        self.previewDisplay = Display()

        self.controlLayout = QtWidgets.QHBoxLayout()
        self.addInputButton = QtWidgets.QPushButton('+')
        self.snapButton = SnapButton()
        self.multipleCheckBox = QtWidgets.QPushButton()
        self.multipleCheckBox.setCheckable(True)
        self.clearButton = QtWidgets.QPushButton()
        self.spacer = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.fileStackWidget = DisplaySnap()

        self.controlLayout.addWidget(self.addInputButton)
        self.controlLayout.addWidget(self.snapButton)
        self.controlLayout.addWidget(self.multipleCheckBox)
        self.controlLayout.addItem(self.spacer)
        self.controlLayout.addWidget(self.clearButton)

        self.layout.addWidget(self.previewDisplay)
        self.layout.addWidget(self.fileStackWidget)
        self.layout.addLayout(self.controlLayout)

        self.setLayout(self.layout)
        self.set_default()

    def set_preview_display(self, path, preview=''): 
        self.previewDisplay.set_display(path, preview, 460, 360)

    def set_default(self): 
        self.set_preview_display(Default.dropFileIcon)
        self.previewDisplay.set_style(Color.dropBG)
        self.previewDisplay.setMaximumSize(QtCore.QSize(360, 240))

        # snap button 
        self.snapButton.button.setIcon(QtGui.QPixmap(Default.captureIcon))
        self.snapButton.button.setIconSize(QtCore.QSize(50, 50))
        self.snapButton.button.setStyleSheet(Color.snapButton)
        self.snapButton.setMaximumSize(QtCore.QSize(40, 30))

        # self.addInputButton.setFlat(True)
        self.addInputButton.setMaximumSize(QtCore.QSize(40, 30))
        self.addInputButton.setStyleSheet(Color.addInputButton)

        font = QtGui.QFont()
        font.setBold(True)
        font.setPointSize(11)
        self.addInputButton.setFont(font)

        self.clearButton.setMaximumSize(QtCore.QSize(40, 30))
        self.clearButton.setIcon(QtGui.QPixmap(Default.clearIcon))
        self.clearButton.setIconSize(QtCore.QSize(32, 32))
        self.clearButton.setStyleSheet(Color.clearButton)

        self.multipleCheckBox.setMaximumSize(QtCore.QSize(40, 30))
        self.multipleCheckBox.setIcon(QtGui.QPixmap(Default.multipleIcon))
        self.multipleCheckBox.setIconSize(QtCore.QSize(32, 32))
        self.multipleCheckBox.setStyleSheet(Color.addInputButton)

        self.fileStackWidget.setMaximumSize(QtCore.QSize(360, 50))


class Widget(Ui):
    """docstring for Widget"""
    message = QtCore.Signal(dict)
    captured = QtCore.Signal(str)
    def __init__(self, parent=None):
        super(Widget, self).__init__(parent=parent)
        self.previewDisplay.autoPreview = False
        self.init_signals()

    def init_signals(self): 
        self.snapButton.captured.connect(self.snapped)
        self.clearButton.clicked.connect(self.clear)
        self.multipleCheckBox.clicked.connect(self.multiple_checked)
        self.fileStackWidget.displayWidget.deleted.connect(self.preview_deleted)
        self.fileStackWidget.selected.connect(self.preview_selected)
        self.previewDisplay.dropped.connect(self.dropped_file)

    def snapped(self, path): 
        if self.allow_multiple_files(path): 
            self.set_preview_display(path)
            self.fileStackWidget.add_item(path, path)
            logger.debug('User snapped a screen "%s"' % path)
            self.captured.emit(path)

    def dropped_file(self, result): 
        path, preview = result 
        if self.allow_multiple_files(path): 
            self.set_preview_display(path, preview)
            self.fileStackWidget.add_item(path, preview)
            logger.debug('user dropped file %s' % path)

    def clear(self): 
        self.set_preview_display(Default.dropFileIcon)
        self.fileStackWidget.clear()
        self.message.emit({'message': 'Clear media list', 'type': 'info'})

    def multiple_checked(self): 
        state = self.multipleCheckBox.isChecked()
        self.fileStackWidget.setEnabled(state)

    def preview_deleted(self): 
        if self.fileStackWidget.displayWidget.count() == 0: 
            self.clear()

    def preview_selected(self, result): 
        path, preview = result
        self.set_preview_display(preview)

    def files(self): 
        return self.fileStackWidget.get_images()

    def allow_multiple_files(self, newFile): 
        basename, fileExt = os.path.splitext(newFile)
        files = self.files()
        exts = list(set([str(os.path.splitext(a)[-1]) for a in files]))

        # check if this is movie 
        if fileExt in Config.movExt: 
            if len(exts) >= 1: 
                message = 'Only 1 media allowed'
                logger.error(message)
                self.message.emit({'message': message, 'type': 'error'})
                return False 
        else: 
            exts.append(fileExt)
            if any(a in exts for a in Config.movExt): 
                message = 'Only 1 media allowed'
                logger.error(message)
                self.message.emit({'message': message, 'type': 'error'})
                return False 
            if len(list(set(exts))) > 1: 
                message = 'Cannot mixed file type "%s"' % exts
                logger.error(message)
                self.message.emit({'message': message, 'type': 'error'})
                return False 

        message = 'add %s' % os.path.basename(newFile)
        self.message.emit({'message': message, 'type': 'info'})
        return True 

