import os
import sys

from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCore

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
moduleDir = os.path.dirname(sys.modules[__name__].__file__)


class Config:
    movPreview = '%s/icons/etc/mov_preview_icon.png' % moduleDir
    etcPreview = '%s/icons/etc/etc_preview_icon.png' % moduleDir
    imgExt = ['.jpg', '.png', '.tif', '.gif']
    movExt = ['.mov']


class DropUrlList(QtWidgets.QListWidget):
    """ subclass QLineEdit for dragdrop events """
    dropped = QtCore.Signal(list)
    multipleDropped = QtCore.Signal(list)

    def __init__(self, parent=None):
        super(DropUrlList, self).__init__(parent)
        self.setAcceptDrops(True)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls:
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            links = []
            for url in event.mimeData().urls():
                links.append(str(url.toLocalFile()))
            self.dropped.emit(links[-1])
            self.multipleDropped.emit(links)
        else:
            event.ignore()


class DropUrlLabel(QtWidgets.QLabel):
    """ subclass QLineEdit for dragdrop events """
    dropped = QtCore.Signal(list)
    multipleDropped = QtCore.Signal(list)
    def __init__(self, parent=None):
        super(DropUrlLabel, self).__init__(parent)
        self.setAcceptDrops(True)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls:
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            links = []
            for url in event.mimeData().urls():
                links.append(str(url.toLocalFile()))
            self.dropped.emit(links[-1])
            self.multipleDropped.emit(links)
        else:
            event.ignore()


class Display(QtWidgets.QWidget) :
    dropped = QtCore.Signal(str)
    def __init__(self) :
        super(Display, self).__init__()

        self.allLayout = QtWidgets.QVBoxLayout()
        self.display = DropUrlLabel()
        self.allLayout.addWidget(self.display)

        self.allLayout.setSpacing(0)
        self.allLayout.setContentsMargins(2, 2, 2, 2)
        self.setLayout(self.allLayout)
        self.display.setAlignment(QtCore.Qt.AlignCenter)
        self.currentPath = ''

        self.display.dropped.connect(self.call_back)
        self.customExt = dict()

    def set_label(self, text):
        self.display.setText(text)

    def call_back(self, path):
        self.set_display(path)
        self.dropped.emit(path)

    def set_display(self, path, width=None, height=None):
        self.currentPath = path
        preview = self.get_preview_image(path)
        width = self.display.frameGeometry().width() if not width else width
        height = self.display.frameGeometry().height() if not height else height
        return self.display.setPixmap(QtGui.QPixmap(preview).scaled(width, height, QtCore.Qt.KeepAspectRatio))

    def set_style(self, style):
        self.setStyleSheet(style)

    def clear(self):
        self.display.clear()

    def get_preview_image(self, path):
        name, ext = os.path.splitext(path)

        if ext in Config.imgExt:
            return path
        elif ext in Config.movExt:
            return Config.movPreview
        elif ext in self.customExt:
            return self.customExt[ext]
        else:
            return Config.etcPreview
