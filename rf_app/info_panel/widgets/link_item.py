import os
import sys
from urllib import urlopen

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

modulePath = sys.modules[__name__].__file__
moduleDir  = os.path.dirname(modulePath)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

class Image: 
    separator = '%s/icons/separator.png' % os.path.split(moduleDir)[0]
    versionDefault = '%s/icons/sg_version.png' % os.path.split(moduleDir)[0]
    loading = '%s/icons/loading60' % os.path.split(moduleDir)[0]
    dirIcon = '%s/icons/dir.png' % os.path.split(moduleDir)[0]
    rvIcon = '%s/icons/rv.png' % os.path.split(moduleDir)[0]
    sgIcon = '%s/icons/sg.png' % os.path.split(moduleDir)[0]


class Color: 
    date = 'color: rgb(100, 140, 200);'
    name = 'color: rgb(220, 220, 220);'
    latest = 'color: rgb(240, 200, 100);'
    entityType = 'color: rgb(100, 200, 100);'
    user = 'color: rgb(220, 160, 100)'
    description = 'color: rgb(100, 100, 100)'
    button = 'background-color: rgb(40, 40, 40)'
    status = 'color: rgb(40, 40, 40);'


class LinkItem(QtWidgets.QWidget):
    """docstring for LinkItem"""
    def __init__(self, parent=None):
        super(LinkItem, self).__init__(parent=parent)
        self.parent = parent

        self.set_ui()


    def set_ui(self):
        self.layout = QtWidgets.QVBoxLayout()
        self.subLayout = QtWidgets.QHBoxLayout()
        # add widgets 
        # version Layout 
        self.linkLayout = QtWidgets.QVBoxLayout()
        self.linkButton = self.add_icon_widget(size=[100, 100], iconPath=Image.versionDefault)
        self.status = QtWidgets.QLabel()

        self.linkLayout.addWidget(self.linkButton)
        self.linkLayout.addWidget(self.status)
        self.linkLayout.setStretch(0, 1)
        self.linkLayout.setStretch(1, 0)
        self.linkLayout.setSpacing(0)
        self.linkLayout.setContentsMargins(0, 0, 0, 0)

        # details layout 
        self.detailLayout = QtWidgets.QGridLayout()
        self.name = self.label_widget(10, True, Color.name)
        self.type = self.label_widget(0, True, Color.entityType)

        self.descriptionLabel = self.label_widget(0, True, Color.description)
        self.descriptionLabel.setText('Description : ')
        self.description = self.description_widget()
        self.spacer = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)


        self.detailLayout.setSpacing(0)
        self.detailLayout.addWidget(self.name, 0, 0, 1, 1)
        self.detailLayout.addWidget(self.type, 1, 0, 1, 1)
        self.detailLayout.addWidget(self.descriptionLabel, 2, 0, 1, 1)
        self.detailLayout.addWidget(self.description, 3, 0, 1, 1)
        self.detailLayout.addItem(self.spacer, 4, 0, 1, 1)

        self.subLayout.addLayout(self.linkLayout)
        self.subLayout.addLayout(self.detailLayout)

        self.subLayout.setStretch(0, 1)
        self.subLayout.setStretch(1, 4)

        # add line 
        self.line = QtWidgets.QLabel()
        self.line.setPixmap(QtGui.QPixmap(Image.separator))
        self.line.setAlignment(QtCore.Qt.AlignRight)

        self.layout.addLayout(self.subLayout)
        self.layout.addWidget(self.line)

        self.layout.setStretch(0, 0)
        self.layout.setStretch(1, 0)

        self.setLayout(self.layout)

    def add_icon_widget(self, size=[64, 64], iconPath='', setFlat=False): 
        widget = QtWidgets.QPushButton()
        widget.setMinimumSize(QtCore.QSize(size[0], size[1]))
        widget.setMaximumSize(QtCore.QSize(size[0], size[1]))
        widget.setStyleSheet(Color.button)

        widget.setFlat(True) if setFlat else None
        
        icon = QtGui.QPixmap(iconPath)
        widget.setIcon(icon)
        widget.setIconSize(QtCore.QSize(100, 100))

        return widget 

    def set_icon(self, path): 
        icon = QtGui.QIcon(path)
        self.linkButton.setIcon(icon)
        self.linkButton.setIconSize(QtCore.QSize(100, 100))

    def add_image(self, imgIndex, data=None, imgPath=''): 
        button = QtWidgets.QPushButton()
        button.setStyleSheet(Color.button)
        # data = urlopen(imgUrl).read()
        if data: 
            icon = QtGui.QPixmap()
            icon.loadFromData(data)
            button.setIcon(icon)
        if imgPath: 
            icon = QtGui.QIcon(imgPath)
            button.setIcon(icon)


        button.setIconSize(QtCore.QSize(100, 100))
        button.setMaximumSize(100, 100)
        button.setMinimumSize(100, 100)
        self.imageGridLayout.addWidget(button, 0, imgIndex, 1, 1) 
        return button

    def label_widget(self, size=0, bold=False, styleSheet=''): 
        label = QtWidgets.QLabel()
        font = QtGui.QFont()
        font.setBold(True) if bold else None 
        font.setPointSize(size) if size else None
        label.setFont(font)
        label.setStyleSheet(styleSheet)
        return label 

    def description_widget(self): 
        widget = QtWidgets.QLabel()
        # widget.setMaximumSize(QtCore.QSize(260, 50))
        # widget.setFrameShadow(QtWidgets.QFrame.Plain)
        # widget.setFrameShape(QtWidgets.QFrame.HLine)
        # widget.setEnabled(True)
        return widget

    def set_name(self, text): 
        self.name.setText(text)

    def set_type(self, text): 
        self.type.setText(text)

    def set_description(self, text): 
        self.description.setPlainText(text)

    def set_date(self, datetime): 
        text = datetime.strftime('%Y-%b-%d %H:%M.%S')
        self.date.setText(text)

        # setStyleSheet
        # self.textShot.setFont(QtGui.QFont("Arial", 9, QtGui.QFont.Bold))
        # self.textFrameRange.setFont(QtGui.QFont("Arial", 8))


    