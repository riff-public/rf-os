from PySide2 import QtCore, QtWidgets 
import nuke 
from nukescripts import panels
import main_ui
reload(main_ui)
import ssl 
ssl._create_default_https_context = ssl._create_unverified_context


def show(): 
    pane = nuke.getPaneFor("Properties.1")
    panels.registerWidgetAsPanel('nuke_dock.main_ui.MainUi', 'RF Panel',"rfpanel.1", True).addToPane(pane)
