# -*- coding: utf-8 -*-
import os 
import sys 
import time 
import subprocess
from collections import OrderedDict
from datetime import datetime 
from functools import partial
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from Qt import QtCore
from Qt import QtGui 
from Qt import QtWidgets

import rf_config as config
from rf_utils import thread_pool
from ..widgets import step_widget
from ..widgets import loading_widget
from ..widgets import version_item
from .. import cmd_utils
from .. import note_config
from .. import sg_request
from rf_utils import user_info
reload(user_info)
reload(note_config)
reload(cmd_utils)
reload(loading_widget)
reload(version_item)
# reload(sg_request)
reload(step_widget)
reload(thread_pool)

modulePath = sys.modules[__name__].__file__
moduleDir  = os.path.dirname(modulePath)


class Default: 
    userIcon = '%s/icons/user.png' % os.path.split(moduleDir)[0]
    refreshIcon = '%s/icons/refresh.png' % os.path.split(moduleDir)[0]
    noTask = 'No Task'
    noStep = 'No Department'
    noUser = 'No User'


class Ui(QtWidgets.QWidget):
    """docstring for Ui"""
    def __init__(self, thread=None, parent=None):
        super(Ui, self).__init__(parent)
        self.firstTimeActivation = True
        self.startTime = datetime.now()
        self.items = []
        self.versions = []
        self.taskDict = OrderedDict()

        self.downloadCount = 0
        self.downloadMovieCount = 0
         # setup thread 
        self.threadpool = QtCore.QThreadPool() if not thread else thread

        self.setup_ui()
        self.set_default()
        self.init_signals()

    def setup_ui(self): 
        self.layout = QtWidgets.QVBoxLayout()
        # department filters (input)
        self.titleLayout = QtWidgets.QGridLayout()
        self.stepComboBox = step_widget.StepComboBox()
        self.titleSpacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        # refresh button 
        self.refreshButton = QtWidgets.QPushButton()
        self.refreshButton.setMaximumSize(32, 32)
        self.refreshButton.setIcon(QtGui.QIcon(Default.refreshIcon))
        self.refreshButton.setFlat(True)

        self.titleLayout.addWidget(self.stepComboBox, 0, 0, 1, 1)
        self.titleLayout.addItem(self.titleSpacer, 0, 1, 1, 1)
        self.titleLayout.addWidget(self.refreshButton, 0, 2, 1, 1)

        self.taskLayout = QtWidgets.QHBoxLayout()
        self.taskLabel = QtWidgets.QLabel('Task')
        self.taskComboBox = QtWidgets.QComboBox()
        self.taskCheckBox = QtWidgets.QCheckBox()
        self.spacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.latestCheckBox = QtWidgets.QCheckBox('Latest only')


        self.taskLayout.addWidget(self.taskLabel)
        self.taskLayout.addWidget(self.taskComboBox)
        self.taskLayout.addWidget(self.taskCheckBox)
        self.taskLayout.addItem(self.spacer)
        self.taskLayout.addWidget(self.latestCheckBox)

        self.taskLayout.setStretch(0, 1)
        self.taskLayout.setStretch(1, 2)
        self.taskLayout.setStretch(2, 0)
        self.taskLayout.setStretch(3, 4)
        self.taskLayout.setStretch(4, 0)

        self.titleLayout.addLayout(self.taskLayout, 1, 0, 1, 3)
        # self.titleLayout.addWidget(self.taskCheckBox, 1, 1, 1, 1)


        self.titleLayout.setColumnStretch(0, 2)
        self.titleLayout.setColumnStretch(1, 2)
        self.titleLayout.setColumnStretch(2, 0)

        # version display widget 
        self.versionDisplayWidget = QtWidgets.QListWidget()

        # status 
        self.progressLayout = QtWidgets.QHBoxLayout()
        self.progressLabel = QtWidgets.QLabel()
        self.loadingStatus = loading_widget.LoadingCircle(image=2)
        self.summaryLabel = QtWidgets.QLabel()

        self.progressLayout.addWidget(self.progressLabel)
        self.progressLayout.addWidget(self.loadingStatus)
        # self.progressLayout.addWidget(self.showAllCheckBox)


        self.spacer = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.layout.addLayout(self.titleLayout)
        self.layout.addWidget(self.versionDisplayWidget)
        self.layout.addLayout(self.progressLayout)
        # self.layout.addWidget(self.summaryLabel)
        # self.layout.addItem(self.spacer)

        self.layout.setStretch(0, 0)
        self.layout.setStretch(1, 1)
        self.layout.setStretch(2, 0)
        self.layout.setStretch(3, 0)
        self.layout.setStretch(4, 0)

        self.setLayout(self.layout)
        self.setWindowTitle('Versions')

    def set_default(self): 
        self.taskCheckBox.setChecked(True)


    def init_signals(self): 
        self.refreshButton.clicked.connect(self.refresh_clicked)
        self.stepComboBox.comboBox.currentIndexChanged.connect(self.set_task_comboBox)
        self.taskComboBox.currentIndexChanged.connect(self.filter_versions)
        self.taskCheckBox.stateChanged.connect(self.task_checked)
        self.latestCheckBox.stateChanged.connect(self.filter_versions)

    def refresh(self, entity): 
        """ ui tab start here """ 
        logger.debug('Refreshing %s ...' % self.windowTitle())
        self.entity = entity

        # init ui 
        if entity.valid: 
            if self.firstTimeActivation: 
                self.firstTimeActivation = False

                self.setup_static_info(entity)
                self.setup_dynamic_info()
        else: 
            self.set_progress_status('Not in the pipeline. Stop loading', 'info', True)

    def setup_static_info(self, entity): 
        self.stepComboBox.refresh(entity)

    def setup_dynamic_info(self): 
        # list versions 
        self.list_versions()

    def refresh_clicked(self): 
        # clear cache 
        sg_request.versionCaches = dict()
        # list versions 
        self.list_versions()

    def list_versions(self): 
        self.set_progress_status('Fetching versions ...', 'info')
        self.items = []
        worker = thread_pool.Worker(self.fetch_versions)
        worker.signals.result.connect(self.fetch_versions_finished)
        self.threadpool.start(worker)

    def fetch_versions(self): 
        cmd_utils.sleep()
        versions = sg_request.fetch_versions(self.entity.project, self.entity.entity_type, self.entity.name)
        return versions 

    def fetch_versions_finished(self, results): 
        self.versions = results
        self.latestVersionDict = self.latest_version()

        self.set_task_comboBox()
        self.display_versions(results)
        self.filter_versions()
        self.set_progress_status('Finished', 'info', True)

        # start worker 
        if results: 
            self.set_progress_status('Downloading media ...')
            self.fetch_media_thread(results)


    def fetch_media_thread(self, versionEntities): 
        worker = thread_pool.Worker(self.fetch_preview_media, versionEntities[::-1])
        self.workerMediaSignals = worker.signals 
        worker.signals.loopResult.connect(self.fetch_preview_media_finished)
        self.threadpool.start(worker)


    def fetch_preview_media(self, versionEntities): 
        cmd_utils.sleep()
        imageDict = dict()
        for versionEntity in versionEntities: 
            version = Version(versionEntity)
            versionDict = dict()
            versionId = version.id()
            url = version.image()
            if version.versionEntity.get('sg_uploaded_movie'): 
                dst = self.version_path(version.versionEntity, thumbnail=True)

                if url: 
                    if not os.path.exists(dst): 
                        self.downloadCount+=1 
                        self.set_progress_status('Downloading %s items ...' % self.downloadCount)
                        logger.debug(dst)
                        sg_request.download_thumbnail(url, dst)

                    # else: 
                    #     self.set_progress_status('Using cache %s' % dst, 'debug', False)

                    versionDict = {'id': versionId, 'preview': dst}
                    self.workerMediaSignals.loopResult.emit(versionDict)
                    imageDict[versionId] = versionDict

        return imageDict


    def fetch_preview_media_finished(self, imageDict): 
        """ update thumbnail """ 
        self.update_thumbnails(imageDict)

        if self.threadpool.activeThreadCount() == 0: 
            self.set_progress_status('Finished', 'info', True)

    def update_thumbnails(self, imageDict): 
        """ update thumbnail """ 
        versionId = imageDict.get('id')
        path = imageDict.get('preview')
        for item in self.items: 
            version = item.data(QtCore.Qt.UserRole)
            itemId = version.get('id')

            if itemId == versionId: 
                itemWidget = self.versionDisplayWidget.itemWidget(item)
                itemWidget.set_icon(path) if path else None
                QtWidgets.QApplication.processEvents()

    def set_task_comboBox(self): 
        # get step 
        if self.versions: 
            result = self.stepComboBox.comboBox.itemData(self.stepComboBox.comboBox.currentIndex(), QtCore.Qt.UserRole)
            step, sgStep = result if result else ('', '')
            tasks = []

            for version in self.versions: 
                v = Version(version)
                itemTaskName = v.task()
                itemStep = v.step()
                if step.lower() == itemStep.lower(): 
                    tasks.append(itemTaskName)

            # add items 
            self.taskComboBox.blockSignals(True)
            self.taskComboBox.clear()
            taskNames = list(set(sorted(tasks))) if tasks else [Default.noTask]
            for task in taskNames: 
                self.taskComboBox.addItem(task)
            self.taskComboBox.blockSignals(False)
            self.taskComboBox.currentIndexChanged.emit(True)


    def display_versions(self, versions): 
        self.versionDisplayWidget.clear()

        for version in versions[::-1]: 
            item = self.add_version_item(version)
            self.items.append(item)

        if not versions: 
            self.versionDisplayWidget.addItem('-- No Version --')

    def add_version_item(self, versionEntity): 
        item = QtWidgets.QListWidgetItem(self.versionDisplayWidget)
        item.setData(QtCore.Qt.UserRole, versionEntity)
        item.setFlags(item.flags() & ~QtCore.Qt.ItemIsSelectable)
        note_config.set_tool_tip(item, versionEntity)

        version = Version(versionEntity)

        versionName = version.name()
        taskName = version.task()
        latestVersion = self.latestVersionDict[taskName]

        versionItem = version_item.VersionItem()
        versionItem.set_date(version.created_at())
        versionItem.set_version(version.name())
        versionItem.set_task(taskName)
        versionItem.set_user(version.user())
        versionItem.set_description(version.description())
        versionItem.set_latest() if versionName == latestVersion else None

        mediaPath = version.movie_path() or ''
        versionItem.versionButton.clicked.connect(partial(self.versionButton_clicked, versionEntity))
        versionItem.dirButton.clicked.connect(partial(self.open_dir, mediaPath))
        versionItem.rvButton.clicked.connect(partial(self.open_in_rv, mediaPath))
        versionItem.sgButton.clicked.connect(partial(self.open_in_sg, versionEntity.get('id')))

        if not os.path.exists(os.path.dirname(mediaPath)): 
            versionItem.dirButton.setEnabled(False)
            versionItem.rvButton.setEnabled(False)

        item.setSizeHint(versionItem.sizeHint())
        self.versionDisplayWidget.addItem(item)
        self.versionDisplayWidget.setItemWidget(item, versionItem)

        return item

    def versionButton_clicked(self, versionEntity): 
        """ download and open in rv """ 
        worker = thread_pool.Worker(partial(self.download_version, versionEntity))
        worker.signals.result.connect(self.download_version_finished)
        self.threadpool.start(worker)
        self.downloadMovieCount+= 1
        self.set_progress_status('Downloading %s items' % (self.downloadMovieCount))


    def download_version(self, versionEntity): 
        cmd_utils.sleep()
        dst = ''
        attachmentEntity = versionEntity.get('sg_uploaded_movie')
        if attachmentEntity: 
            dst = self.version_path(versionEntity)
            if not os.path.exists(dst): 
                dst = sg_request.download_attachment(attachmentEntity, dst)
        return dst 

    def download_version_finished(self, path): 
        """ view in player """ 
        self.downloadMovieCount-=1 
        if os.path.exists(path): 
            os.startfile(path)
            logger.debug('Opened %s' % path)
        self.set_progress_status('Download version Finished', 'debug', True)

    def open_dir(self, path): 
        cmd_utils.open_dir(path)

    def open_in_rv(self, path): 
        cmd_utils.open_in_rv(path)

    def open_in_sg(self, versionId): 
        # cmd_utils.open_in_rv(path)
        cmd_utils.open_rv_screeningRoom([versionId])

    def task_checked(self, value): 
        self.taskComboBox.setEnabled(True) if value else self.taskComboBox.setEnabled(False)
        self.filter_versions()

    def filter_versions(self): 
        step = ''
        sgStep = ''
        result = self.stepComboBox.comboBox.itemData(self.stepComboBox.comboBox.currentIndex(), QtCore.Qt.UserRole)
        if result: 
            step, sgStep = result
        taskName = str(self.taskComboBox.currentText())

        if self.items: 
            for item in self.items: 
                versionEntity = item.data(QtCore.Qt.UserRole)
                version = Version(versionEntity)
                versionName = version.name()
                itemStep = version.step()
                itemTask = version.task()
                latestVersion = self.latestVersionDict[itemTask]

                # not check mean pass all step tasks 
                if not self.taskCheckBox.isChecked(): 
                    itemTask = taskName
                
                if step == itemStep.lower() and taskName == itemTask: 
                    hidden = False 
                else: 
                    hidden = True 

                if self.latestCheckBox.isChecked(): 
                    if not hidden: 
                        if not versionName == latestVersion: 
                            hidden = True

                # print hidden, '(%s %s), (%s %s)' % (step, itemStep.lower(), taskName, itemTask)
                self.versionDisplayWidget.setItemHidden(item, hidden)


    def latest_version(self): 
        """ get latest """ 
        taskDict = dict()
        groupDict = dict()

        for version in self.versions: 
            datetime = version.get('created_at')
            taskEntity = version.get('sg_task')
            taskName = taskEntity.get('name') if taskEntity else Default.noTask
            time = datetime.strftime('%Y-%m-%d %H:%M.%S')
            taskDict[time] = version

        for time in sorted(taskDict.keys())[::-1]: 
            version = taskDict[time]
            versionName = version.get('code')
            taskEntity = version.get('sg_task')
            taskName = taskEntity.get('name') if taskEntity else Default.noTask
            
            if not taskName in groupDict.keys(): 
                groupDict[taskName] = versionName
                continue
            # else: 
            #     groupDict[taskName].append(versionName)

        return groupDict

        

    def set_progress_status(self, text, level='debug', finished=False): 
        if level == 'debug': 
            logger.debug(text)
        if level == 'info': 
            logger.info(text)
        if level == 'warning': 
            logger.warning(text)

        if not finished: 
            if not self.loadingStatus.loading: 
                self.loadingStatus.start()
            self.progressLabel.setText(text)
        else: 
            self.loadingStatus.stop()
            self.finishTime = round((float((datetime.now() - self.startTime).seconds)), 2)
            logger.debug('Finished in %s' % self.finishTime)
            self.progressLabel.setText('%s in %s secs' % (text, self.finishTime))


    def version_path(self, versionEntity, thumbnail=False): 
        """ get attachment path """ 
        dst = note_config.version_path(self.entity.copy(), versionEntity, thumbnail)
        return dst

        # versionId = versionEntity.get('id')
        # name = versionEntity.get('code')
        # entity = self.entity.copy()
        # entity.context.update(step='_data', process='sgVersion')
        # path = entity.path.process().abs_path()
        # filename = versionEntity["sg_uploaded_movie"]["name"]
        # if thumbnail: 
        #     filename = '%s_thumbnamil.jpg' % os.path.splitext(filename)[0]
        # dst = '%s/%s' % (path, filename)
        # return dst 



class Version(object):
    """docstring for Version"""
    def __init__(self, versionEntity):
        super(Version, self).__init__()
        self.versionEntity = versionEntity
        self.attr = ['user', 'entity', 'sg_task', 'sg_task.Task.step', 'sg_status_list', 'sg_uploaded_movie', 'image', 'sg_path_to_movie', 'code', 'description', 'created_at']

    def task(self): 
        taskEntity = self.versionEntity.get('sg_task')
        taskName = taskEntity.get('name') if taskEntity else Default.noTask 
        return taskName

    def step(self): 
        stepEntity = self.versionEntity.get('sg_task.Task.step')
        department = stepEntity.get('name') if stepEntity else Default.noStep
        return department 

    def status(self): 
        return self.versionEntity.get('sg_status_list')

    def name(self): 
        return self.versionEntity.get('code')

    def id(self): 
        return self.versionEntity.get('id')

    def image(self): 
        return self.versionEntity.get('image')

    def uploaded_movie(self): 
        return self.versionEntity.get('sg_uploaded_movie')

    def entity(self): 
        return self.versionEntity.get('entity')

    def created_at(self): 
        return self.versionEntity.get('created_at')

    def user(self): 
        userEntity = self.versionEntity.get('user')
        name = userEntity.get('name') if userEntity else Default.noUser
        return name

    def movie_path(self): 
        path = self.versionEntity.get('sg_path_to_movie')
        return path 

    def description(self): 
        text = self.versionEntity.get('description') or '-'
        return text
        
