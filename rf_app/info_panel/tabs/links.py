# -*- coding: utf-8 -*-
import os 
import sys 
import subprocess
from collections import OrderedDict
from datetime import datetime 
from functools import partial
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from Qt import QtCore
from Qt import QtGui 
from Qt import QtWidgets

from rf_utils import thread_pool
from ..widgets import step_widget
from ..widgets import link_item
from ..widgets import loading_widget
from ..widgets import snap_widget
from ..widgets import display_snap
from .. import note_config
from .. import sg_request
from rf_utils import user_info
from rf_utils.context import context_info
from .. import cmd_utils
reload(cmd_utils)
reload(user_info)
reload(note_config)
reload(snap_widget)
reload(loading_widget)
reload(display_snap)
reload(sg_request)

reload(step_widget)
reload(link_item)

modulePath = sys.modules[__name__].__file__
moduleDir  = os.path.dirname(modulePath)

class Default: 
    refreshIcon = '%s/icons/refresh.png' % os.path.split(moduleDir)[0]


class Color: 
    projectLabel = 'color: rgb(220, 220, 220);'
    entityLabel = 'color: rgb(180, 220, 240);'
    grpLabel = 'color: rgb(160, 160, 160);'
    submitButton = 'background-color: rgb(80, 160, 240);'
    snapButton = 'background-color: rgb(200, 200, 200);'


class Ui(QtWidgets.QWidget):
    """docstring for Ui"""
    def __init__(self, thread=None, parent=None):
        super(Ui, self).__init__(parent)
        self.parent = parent
        self.firstTimeActivation = True
        self.items = []

        # setup thread 
        self.threadpool = QtCore.QThreadPool() if not thread else thread
        self.setup_ui()
        self.startTime = datetime.now()

         # setup thread 
        self.threadpool = QtCore.QThreadPool() if not thread else thread

    def setup_ui(self): 
        self.layout = QtWidgets.QVBoxLayout()
        # title layout
        self.titleLayout = QtWidgets.QHBoxLayout()
        self.titleSpacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.searchLabel = QtWidgets.QLabel('Search : ')
        self.searchLineEdit = QtWidgets.QLineEdit()

        self.refreshButton = QtWidgets.QPushButton()
        self.refreshButton.setMaximumSize(32, 32)
        self.refreshButton.setIcon(QtGui.QIcon(Default.refreshIcon))
        self.refreshButton.setFlat(True)

        self.titleLayout.addWidget(self.searchLabel)
        self.titleLayout.addWidget(self.searchLineEdit)
        self.titleLayout.addItem(self.titleSpacer)
        self.titleLayout.addWidget(self.refreshButton)

        self.titleLayout.setStretch(0, 0)
        self.titleLayout.setStretch(1, 1)
        self.titleLayout.setStretch(1, 2)
        self.titleLayout.setStretch(1, 0)

        self.displayLayout = QtWidgets.QVBoxLayout()
        self.linkDisplayWidget = QtWidgets.QListWidget()
        self.displayLayout.addWidget(self.linkDisplayWidget)

        # status 
        self.progressLayout = QtWidgets.QHBoxLayout()
        self.progressLabel = QtWidgets.QLabel()
        self.loadingStatus = loading_widget.LoadingCircle(image=2)
        self.summaryLabel = QtWidgets.QLabel()

        self.progressLayout.addWidget(self.progressLabel)
        self.progressLayout.addWidget(self.loadingStatus)

        self.layout.addLayout(self.titleLayout)
        self.layout.addLayout(self.displayLayout)
        self.layout.addLayout(self.progressLayout)


        self.setLayout(self.layout)
        self.setWindowTitle('Links')
        self.init_signals()

    def init_signals(self): 
        self.linkDisplayWidget.itemDoubleClicked.connect(self.item_clicked)

    def refresh(self, entity): 
        """ ui tab start here """ 
        logger.debug('Refreshing %s ...' % self.windowTitle())
        self.entity = entity

        # init ui 
        if entity.valid: 
            if self.firstTimeActivation: 
                self.firstTimeActivation = False
                self.setup_dynamic_info()
        else: 
            self.lock_ui()
            self.set_progress_status('Not in the pipeline. Stop loading', 'info', True)


    def setup_dynamic_info(self): 
        self.list_links()

    def list_links(self): 
        # fetch links 
        self.items = []
        self.startTime = datetime.now()
        self.set_progress_status('Fetch links ...')
        worker = thread_pool.Worker(self.fetch_links)
        worker.signals.result.connect(self.fetch_links_finished)
        self.threadpool.start(worker)


    def fetch_links(self): 
        cmd_utils.sleep()
        results = sg_request.fetch_links(self.entity.project, self.entity.entity_type, self.entity.name) 
        return results 

    def fetch_links_finished(self, results): 
        self.set_progress_status('Fetch link finished', 'debug', True)
        self.display_links(results)

        # fetch icons 
        self.fetch_media_thread(results)

    def display_links(self, sgEntities): 
        self.linkDisplayWidget.clear()

        for sgEntity in sgEntities: 
            item = self.add_item(sgEntity)
            self.items.append(item)

        if not sgEntities: 
            self.linkDisplayWidget.addItem('-- No Information --')

    def add_item(self, sgEntity): 
        item = QtWidgets.QListWidgetItem(self.linkDisplayWidget)
        item.setData(QtCore.Qt.UserRole, sgEntity)
        # item.setFlags(item.flags() & ~QtCore.Qt.ItemIsSelectable)
        note_config.set_tool_tip(item, sgEntity)

        linkWidget = link_item.LinkItem()
        linkWidget.set_name(sgEntity.get('code'))
        assetType = sgEntity.get('sg_asset_type')
        sequence = sgEntity.get('sg_sequence_code')
        linkWidget.set_type(assetType or sequence)
        
        item.setSizeHint(linkWidget.sizeHint())
        self.linkDisplayWidget.addItem(item)
        self.linkDisplayWidget.setItemWidget(item, linkWidget)

        return item

    def fetch_media_thread(self, sgEntities): 
        worker = thread_pool.Worker(partial(self.fetch_media, sgEntities))
        worker.signals.loopResult.connect(self.fetch_media_finished)
        self.workerMediaSignals = worker.signals 
        self.threadpool.start(worker)

    def fetch_media(self, sgEntities): 
        cmd_utils.sleep()
        for sgEntity in sgEntities: 
            imageDict = dict()
            dst = self.link_path(sgEntity, thumbnail=True)
            print dst 

            if not os.path.exists(dst): 
                print '%s download' % dst
                url = sgEntity.get('image')
                sg_request.download_thumbnail(url, dst)
            imageDict = {'id': sgEntity.get('id'), 'preview': dst}
            self.workerMediaSignals.loopResult.emit(imageDict)
        return True

    def fetch_media_finished(self, imageDict): 
        # update thumbnails 
        self.update_thumbnail(imageDict)

    def update_thumbnail(self, imageDict): 
        """ update thumbnail """ 
        entityId = imageDict.get('id')
        path = imageDict.get('preview')
        for item in self.items: 
            sgEntity = item.data(QtCore.Qt.UserRole)
            itemId = sgEntity.get('id')

            if itemId == entityId: 
                itemWidget = self.linkDisplayWidget.itemWidget(item)
                itemWidget.set_icon(path) if path else None
                QtWidgets.QApplication.processEvents()

    def item_clicked(self, item): 
        entity = item.data(QtCore.Qt.UserRole)
        context = context_info.Context()
        context.use_sg(sg_request.sg, entity.get('project').get('name'), sg_request.Config.entityTypeMap.get(entity.get('type')), entityName=entity.get('code'))
        linkEntity = context_info.ContextPathInfo(context=context)
        self.parent.initialize_context(linkEntity)
        self.parent.tabWidget.setCurrentIndex(1)

    def link_path(self, sgEntity, thumbnail=False): 
        """ get attachment path """ 
        dst = note_config.link_path(self.entity.copy(), sgEntity, thumbnail)
        return dst 
        # name = sgEntity.get('code')
        # entity = self.entity.copy()
        # entity.context.update(step='_data', process='sgEntity')
        # path = entity.path.process().abs_path()
        # filename = name
        # if thumbnail: 
        #     filename = '%s_thumbnail.jpg' % os.path.splitext(name)[0]
        # dst = '%s/%s' % (path, filename)
        # return dst 

    def set_progress_status(self, text, level='debug', finished=False): 
        if level == 'debug': 
            logger.debug(text)
        if level == 'info': 
            logger.info(text)
        if level == 'warning': 
            logger.warning(text)

        if not finished: 
            if not self.loadingStatus.loading: 
                self.loadingStatus.start()
            self.progressLabel.setText(text)
        else: 
            self.loadingStatus.stop()
            self.finishTime = round((float((datetime.now() - self.startTime).seconds)), 2)
            logger.debug('Finished in %s' % self.finishTime)
            self.progressLabel.setText('%s in %s secs' % (text, self.finishTime))

