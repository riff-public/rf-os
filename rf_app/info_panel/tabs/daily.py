# -*- coding: utf-8 -*-
import os 
import sys 
import subprocess
from collections import OrderedDict
from datetime import datetime 
from functools import partial
import time
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from Qt import QtCore
from Qt import QtGui 
from Qt import QtWidgets

from rf_utils import thread_pool
from ..widgets import step_widget
from ..widgets import note_item
from ..widgets import loading_widget
from ..widgets import snap_widget
from ..widgets import display_snap
from ..widgets import display_widget
from .. import note_config
from .. import sg_request
from rf_utils import user_info
from rf_utils import publish_info
from .. import daily_cmd
reload(daily_cmd)
reload(user_info)
reload(note_config)
reload(snap_widget)
reload(loading_widget)
reload(display_snap)
reload(sg_request)
reload(display_widget)

reload(step_widget)
reload(note_item)

modulePath = sys.modules[__name__].__file__
moduleDir  = os.path.dirname(modulePath)


class Color: 
    projectLabel = 'color: rgb(220, 220, 220);'
    entityLabel = 'color: rgb(180, 220, 240);'
    grpLabel = 'color: rgb(160, 160, 160);'
    submitButton = 'background-color: rgb(80, 160, 240);'
    snapButton = 'background-color: rgb(72, 72, 72);'
    addInputButton = 'color: rgb(200, 200, 200); background-color: rgb(72, 72, 72);'
    dropBG = 'background-color: rgb(100, 100, 100);'
    clearButton = 'background-color: rgb(160, 100, 100);'
    errorStatus = 'color: rgb(240, 240, 240); background-color: rgb(200, 20, 20);'
    submitButton = 'background-color: rgb(76, 76, 76);'
    submitComplete = 'background-color: rgb(76, 140, 76);'


class Default: 
    refreshIcon = '%s/icons/refresh.png' % os.path.split(moduleDir)[0]
    captureIcon = '%s/icons/snap2.png' % os.path.split(moduleDir)[0]
    dropFileIcon = '%s/icons/drop_file.png' % os.path.split(moduleDir)[0]
    clearIcon = '%s/icons/clear.png' % os.path.split(moduleDir)[0]
    dailyIcon = '%s/icons/daily.png' % os.path.split(moduleDir)[0]
    noTask = 'No Task'


class Ui(QtWidgets.QWidget):
    """docstring for Ui"""
    def __init__(self, thread=None, parent=None):
        super(Ui, self).__init__(parent)
        self.parent = parent
        self.firstTimeActivation = True
        self.startTime = datetime.now()
        self.threadpool = QtCore.QThreadPool() if not thread else thread
        self.setup_ui()
        self.set_default()
        self.init_signals()

    def setup_ui(self): 
        self.layout = QtWidgets.QVBoxLayout()
        # title layout
        # contain step comboBox and refresh icon 
        self.titleLayout = QtWidgets.QHBoxLayout()
        self.titleSpacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.stepComboBox = step_widget.StepComboBox()
        self.taskLabel = QtWidgets.QLabel('Task ')
        self.taskComboBox = QtWidgets.QComboBox()

        # refresh button 
        self.refreshButton = QtWidgets.QPushButton()
        self.refreshButton.setMaximumSize(32, 32)
        self.refreshButton.setIcon(QtGui.QIcon(Default.refreshIcon))
        self.refreshButton.setFlat(True)

        self.titleLayout.addWidget(self.stepComboBox)
        self.titleLayout.addWidget(self.taskLabel)
        self.titleLayout.addWidget(self.taskComboBox)
        self.titleLayout.addItem(self.titleSpacer)
        self.titleLayout.addWidget(self.refreshButton)

        self.titleLayout.setStretch(0, 2)
        self.titleLayout.setStretch(1, 0)
        self.titleLayout.setStretch(2, 1)
        self.titleLayout.setStretch(3, 0)
        self.titleLayout.setStretch(4, 0)

        # preview area 
        self.previewLayout = QtWidgets.QVBoxLayout()
        self.previewWidget = snap_widget.Widget()

        self.previewLayout.addWidget(self.previewWidget)
        # self.previewLayout.addLayout(self.controlLayout)

        # description
        self.descriptionLayout = QtWidgets.QVBoxLayout()
        self.descriptionLabel = QtWidgets.QLabel('Description: ')
        self.description = QtWidgets.QPlainTextEdit()
        self.descriptionLayout.addWidget(self.descriptionLabel)
        self.descriptionLayout.addWidget(self.description)


        # progres status 
        self.progressLayout = QtWidgets.QHBoxLayout()
        self.progressLabel = QtWidgets.QLabel()
        self.loadingStatus = loading_widget.LoadingCircle(image=2)

        self.progressLayout.addWidget(self.progressLabel)
        self.progressLayout.addWidget(self.loadingStatus)

        self.submitButton = QtWidgets.QPushButton('Submit Daily')


        # spacer 
        self.spacer = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.layout.addLayout(self.titleLayout)
        self.layout.addLayout(self.previewLayout)
        self.layout.addLayout(self.descriptionLayout)
        self.layout.addItem(self.spacer)
        self.layout.addWidget(self.submitButton)
        self.layout.addLayout(self.progressLayout)

        self.layout.setStretch(0, 0)
        self.layout.setStretch(1, 1)
        self.layout.setStretch(2, 3)
        self.layout.setStretch(3, 0)

        self.setLayout(self.layout)

    def set_default(self): 
        # preview widget 
        self.submitButton.setMinimumSize(QtCore.QSize(30, 40))
        iconWidget = QtGui.QIcon()
        iconWidget.addPixmap(QtGui.QPixmap(Default.dailyIcon),QtGui.QIcon.Normal,QtGui.QIcon.Off)
        self.submitButton.setIcon(iconWidget)
        self.submitButton.setIconSize(QtCore.QSize(32, 32))
        # self.submitButton.setFlat(True)
        # self.submitButton.setStyleSheet(Color.submitButton)
        return 

    def refresh(self, entity): 
        """ ui tab start here """ 
        logger.debug('Refreshing %s ...' % self.windowTitle())
        self.entity = entity
        # self.set_progress_status('Test', 'info', True)

        # init ui 
        if entity.valid: 
            if self.firstTimeActivation: 
                self.firstTimeActivation = False
                self.setup_static_info(entity)
        else: 
            self.set_progress_status('Not in the pipeline. Stop loading', 'info', True)


    def setup_static_info(self, entity):
        self.stepComboBox.refresh(entity) 

    def init_signals(self): 
        self.refreshButton.clicked.connect(self.refresh_clicked)
        self.stepComboBox.comboBox.currentIndexChanged.connect(self.set_task_comboBox)
        self.previewWidget.message.connect(self.widget_message)
        self.submitButton.clicked.connect(self.submit_daily)
        self.previewWidget.captured.connect(self.captured)


    def refresh_clicked(self): 
        self.refresh(self.entity)

    def set_task_comboBox(self): 
        """ set task based on input step """ 
        # set no task 
        self.taskComboBox.clear()
        self.taskComboBox.addItem(Default.noTask)

        # if new tasks found, it will override no task label 
        worker = thread_pool.Worker(self.fetch_tasks)
        worker.signals.result.connect(self.fetch_tasks_finished)
        self.threadpool.start(worker)

    def fetch_tasks(self): 
        """ fetch task from sg_request """ 
        result = self.stepComboBox.comboBox.itemData(self.stepComboBox.comboBox.currentIndex(), QtCore.Qt.UserRole)
        if result: 
            step, sgStep = result
            return sg_request.fetch_tasks(self.entity.project, self.entity.entity_type, self.entity.name, step)
        return []

    def fetch_tasks_finished(self, tasks): 
        self.display_tasks(tasks)


    def display_tasks(self, tasks): 
        """ set display given data """ 
        if tasks: 
            self.taskComboBox.blockSignals(True)
            self.taskComboBox.clear()

            for i, task in enumerate(tasks): 
                self.taskComboBox.addItem(task.get('content'))
                self.taskComboBox.setItemData(i, task, QtCore.Qt.UserRole)

            self.taskComboBox.currentIndexChanged.emit(True)
            self.taskComboBox.blockSignals(False)

    def captured(self, path): 
        """ snap clicked """ 
        self.submitButton.setStyleSheet('')
        self.submitButton.setText('Submit Daily')
        # self.previewWidget.set_display(path, 460, 360)

    def submit_daily(self): 
        """ submit daily """
        files = self.previewWidget.files()
        
        # current task 
        taskEntity = self.taskComboBox.itemData(self.taskComboBox.currentIndex(), QtCore.Qt.UserRole)
        description = self.description.toPlainText()

        if description: 
            if files: 
                # submit to shotgun
                self.set_progress_status('Submitting daily ...')
                worker = thread_pool.Worker(self.submit_daily_thread, taskEntity, files,  description)
                worker.signals.result.connect(self.submit_finished)
                self.threadpool.start(worker)
                self.submitButton.setEnabled(False)
            else: 
                self.set_progress_status('No media', 'error', True, styleSheet=Color.errorStatus)

        else: 
            self.set_progress_status('Description missing', 'error', True, styleSheet=Color.errorStatus)


    def submit_daily_thread(self, taskEntity, files, description): 
        # submit to server 
        entity = self.entity.copy()
        # prep data 
        regInfo = publish_info.RegisterInfo(entity)
        publishVersion = regInfo.register_version() or 'v001'
        res = taskEntity.get('sg_resolution') or daily_cmd.guess_res(taskEntity)

        # update context 
        entity.context.update(publishVersion=publishVersion, res=res)

        # submit process 
        self.set_progress_status('Copy files ...')
        publishImgs = daily_cmd.submit(entity.copy(), taskEntity, files)

        self.set_progress_status('Upload to shotgun ...')
        result = sg_request.publish_daily(entity.copy(), taskEntity, publishImgs, description)
        return result 


    def submit_finished(self, result): 
        """ call when submit finished """ 
        status, info = result 

        if status: 
            self.submitButton.setEnabled(True)
            self.submitButton.setStyleSheet(Color.submitComplete)
            self.previewWidget.clear()
            self.submitButton.setText('Submit Completed')
            self.description.clear()

            self.set_progress_status('Finished', level='info', finished=True, styleSheet='')

        else: 
            self.submitButton.setText('Error Submit')
            self.submitButton.setStyleSheet(Color.errorStatus)
            self.set_progress_status('Error submit', level='error', finished=True, styleSheet=Color.errorStatus)
            logger.error(info)


    def widget_message(self, infoDict): 
        text = infoDict['message']
        mesgType = infoDict['type']
        color = Color.errorStatus if mesgType == 'error' else ''
        self.set_progress_status(text, level=mesgType, finished=True, styleSheet=color)



    def set_progress_status(self, text, level='debug', finished=False, styleSheet=''): 
        if level == 'debug': 
            logger.debug(text)
        if level == 'info': 
            logger.info(text)
        if level == 'warning': 
            logger.warning(text)
        if level == 'error': 
            logger.error(text)

        self.progressLabel.setStyleSheet(styleSheet)

        if not finished: 
            if not self.loadingStatus.loading: 
                self.loadingStatus.start()
            self.progressLabel.setText(text)
        else: 
            self.loadingStatus.stop()
            self.finishTime = round((float((datetime.now() - self.startTime).seconds)), 2)
            logger.debug('Finished in %s' % self.finishTime)
            self.progressLabel.setText('%s (Finished in %s secs)' % (text, self.finishTime))

        QtWidgets.QApplication.processEvents()


