# -*- coding: utf-8 -*-
import os 
import sys 
import subprocess
from collections import OrderedDict
from datetime import datetime 
from functools import partial
import time
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from Qt import QtCore
from Qt import QtGui 
from Qt import QtWidgets

from rf_utils import thread_pool
from ..widgets import step_widget
from ..widgets import note_item
from ..widgets import loading_widget
from ..widgets import snap_widget
from ..widgets import display_snap
from .. import note_config
from .. import sg_request
from rf_utils import user_info
from .. import cmd_utils
reload(cmd_utils)
reload(user_info)
reload(note_config)
reload(snap_widget)
reload(loading_widget)
reload(display_snap)
reload(sg_request)

reload(step_widget)
reload(note_item)

modulePath = sys.modules[__name__].__file__
moduleDir  = os.path.dirname(modulePath)

class Color: 
    projectLabel = 'color: rgb(220, 220, 220);'
    entityLabel = 'color: rgb(180, 220, 240);'
    grpLabel = 'color: rgb(160, 160, 160);'
    submitButton = 'background-color: rgb(80, 160, 240);'
    snapButton = 'background-color: rgb(200, 200, 200);'


class Default: 
    userIcon = '%s/icons/user.png' % os.path.split(moduleDir)[0]
    refreshIcon = '%s/icons/refresh.png' % os.path.split(moduleDir)[0]
    captureIcon = '%s/icons/snap.png' % os.path.split(moduleDir)[0]


class Ui(QtWidgets.QWidget):
    """docstring for Ui"""
    def __init__(self, thread=None, parent=None):
        super(Ui, self).__init__(parent)
        self.parent = parent
        self.filterCheckBoxWidgets = dict()
        self.staticSetup = False
        self.noteCaches = dict()
        self.attachmentCaches = dict()
        self.downloadCount = 0
        self.items = []
        self.firstTimeActivation = True
        self.startTime = datetime.now()
        self.setup_ui()

         # setup thread 
        self.threadpool = QtCore.QThreadPool() if not thread else thread

    def setup_ui(self): 
        self.layout = QtWidgets.QVBoxLayout()
        # title layout
        # contain step comboBox and refresh icon 
        self.titleLayout = QtWidgets.QHBoxLayout()
        self.stepComboBox = step_widget.StepComboBox()
        self.titleSpacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        # refresh button 
        self.refreshButton = QtWidgets.QPushButton()
        self.refreshButton.setMaximumSize(32, 32)
        self.refreshButton.setIcon(QtGui.QIcon(Default.refreshIcon))
        self.refreshButton.setFlat(True)

        self.titleLayout.addWidget(self.stepComboBox)
        self.titleLayout.addItem(self.titleSpacer)
        self.titleLayout.addWidget(self.refreshButton)

        self.titleLayout.setStretch(0, 2)
        self.titleLayout.setStretch(1, 2)
        self.titleLayout.setStretch(2, 0)

        self.summaryLabel = QtWidgets.QLabel()



        # --------------------------------------------------------------------

        # descriptions area 
        self.descriptionLayout = QtWidgets.QVBoxLayout()
        self.descriptionLabel = QtWidgets.QLabel('Descriptions')
        self.descriptionWidget = QtWidgets.QPlainTextEdit()
        self.descriptionLayout.addWidget(self.descriptionLabel)
        self.descriptionLayout.addWidget(self.descriptionWidget)

        # display comment widget 
        self.displayLayout = QtWidgets.QVBoxLayout()

        self.displayWidget = QtWidgets.QListWidget()
        self.displayWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.showAllCheckBox = QtWidgets.QCheckBox('Show all Notes')
        self.showAllCheckBox.setLayoutDirection(QtCore.Qt.RightToLeft)

        self.progressLayout = QtWidgets.QHBoxLayout()
        self.progressLabel = QtWidgets.QLabel()
        self.loadingStatus = loading_widget.LoadingCircle(image=2)

        self.progressLayout.addWidget(self.progressLabel)
        self.progressLayout.addWidget(self.loadingStatus)
        self.progressLayout.addWidget(self.showAllCheckBox)

        self.displayLayout.addWidget(self.displayWidget)
        self.displayLayout.addWidget(self.summaryLabel)
        self.displayLayout.addLayout(self.progressLayout)

        # --------------------------------------------------------------------

        # main spacer 
        self.noteSpacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.layout.addLayout(self.titleLayout)
        # self.layout.addLayout(self.commentLayout)
        self.layout.addLayout(self.descriptionLayout)
        self.layout.addLayout(self.displayLayout)
        self.layout.addItem(self.noteSpacer)

        self.layout.setStretch(0, 0)
        self.layout.setStretch(1, 0)
        self.layout.setStretch(2, 4)
        self.layout.setStretch(3, 0)

        self.setLayout(self.layout)
        self.setWindowTitle('Notes')

        self.init_signals()




    def init_signals(self): 
        # change department trigger filter items 
        self.stepComboBox.comboBox.currentIndexChanged.connect(self.set_filter_items)
        # show all notes trigger filter item 
        self.showAllCheckBox.stateChanged.connect(self.set_filter_items)
        # refresh button trigger dynamic attribute reload and clear cache 
        self.refreshButton.clicked.connect(self.refresh_clicked)
        # right click menu connect 
        self.displayWidget.customContextMenuRequested.connect(self.show_note_options)

    def refresh(self, entity): 
        """ ui tab start here """ 
        logger.debug('Refreshing %s ...' % self.windowTitle())
        self.entity = entity

        # init ui 
        if entity.valid: 
            self.fit_items()
            if self.firstTimeActivation: 
                self.firstTimeActivation = False
                self.setup_static_info(entity)
                self.setup_dynamic_info()
        else: 
            self.lock_ui()
            self.set_progress_status('Not in the pipeline. Stop loading', 'info', True)

    def lock_ui(self): 
        self.submitButton.setEnabled(False)
        self.snapButton.setEnabled(False)

    def refresh_clicked(self): 
        count = self.threadpool.activeThreadCount()
        if count == 0:
            self.clear_caches()
            self.setup_dynamic_info()
        else: 
            logger.debug('Thread is running %s. Cannot refresh.' % count)


    def clear_caches(self): 
        """ clear note caches """ 
        self.noteCaches = dict()

    def setup_static_info(self, entity): 
        if not self.staticSetup: 
            # set department 
            self.stepComboBox.refresh(entity)
            # set default 
            self.staticSetup = True 
        else: 
            self.stepComboBox.refresh(entity)

    def setup_dynamic_info(self): 
        self.list_notes()



    def list_notes(self): 
        # list comments 
        # fetching from shotgun 
        self.items = []
        self.duration = []
        self.startTime = datetime.now()

        self.set_progress_status('Loading from Shotgun...')
        self.set_loading()

        # start thread 
        self.set_progress_status('Fetching notes ...')
        worker = thread_pool.Worker(self.fetch_note)
        worker.signals.result.connect(self.fetch_note_finished)
        self.threadpool.start(worker)

    def fetch_note(self): 
        """ query notes from shotgun """ 
        # initiate sleep for more stable operation 
        cmd_utils.sleep()
        if not self.entity.name in self.noteCaches.keys(): 
            logger.debug('Connecting shotgun ...')
            notes = sg_request.fetch_notes_information(self.entity.project, self.entity.name, brief=True)
            self.noteCaches[self.entity.name] = notes
        else: 
            logger.debug('Using caches')

        return self.noteCaches[self.entity.name]

    def fetch_note_finished(self, noteDict): 
        """ thread callback """ 
        notes = [v.get('note') for k, v in noteDict.iteritems()]
        logger.debug('Set display')
        self.display_notes(noteDict)
        self.set_progress_status('Finished', 'info', True)
        
        # fetch attachments in thread 
        self.set_progress_status('Fetching attachment ...')
        worker = thread_pool.Worker(partial(self.fetch_attachments, notes))
        worker.signals.result.connect(self.fetch_attachments_finished)
        self.threadpool.start(worker)


    def fetch_attachments(self, notes): 
        """ query attachment entity from attachment id to get 'image' attribute """ 
        cmd_utils.sleep()
        attachmentDict = dict()
        for note in notes: 
            attachEntities = []
            attachments = note.get('attachments')
            if attachments: 
                for attachment in attachments: 
                    entity = sg_request.get_attachment(attachment.get('id'))
                    if entity: 
                        # find thumbnail first 
                        imgPath = self.attachment_path(entity, thumbnail=True)
                        imgData = None
                        if not os.path.exists(imgPath): 
                            url = entity.get('image')
                            if url: 
                                sg_request.download_thumbnail(url, imgPath)


                        attachEntities.append({'imgData': imgData, 'entity': entity, 'path': imgPath})
            attachmentDict[note['id']] = attachEntities
        return attachmentDict


    def fetch_attachments_finished(self, attachments): 
        """ attachment downloaded and update to ui """ 
        self.update_attachment(attachments)
        self.set_progress_status('Finished', 'info', True)

    def update_attachment(self, attachmentDict): 
        """ add download attachment to ui """ 
        items = self.items

        for item in items: 
            noteDict = item.data(QtCore.Qt.UserRole)
            if noteDict: 
                noteEntity = noteDict.get('note')
                attachments = attachmentDict.get(noteEntity.get('id'))
                widget = self.displayWidget.itemWidget(item)
                
                if attachments: 
                    for index, data in enumerate(attachments): 
                        imgPath = data.get('path')
                        imgData = data.get('imgData')
                        attachEntity = data.get('entity')
                        if os.path.exists(imgPath): 
                            button = widget.add_image(index, data=None, imgPath=imgPath)
                        else: 
                            button = widget.add_image(index, data=imgData, imgPath='')
                            # logger.debug('queue download %s' % attachEntity.get('name'))
                            # self.attachment_clicked(attachEntity, openFile=False)
                        
                        button.clicked.connect(partial(self.attachment_clicked, attachEntity))

                    QtWidgets.QApplication.processEvents()
                    item.setSizeHint(widget.sizeHint())

    def fetch_attachment(self, note): 
        imgPaths = []
        attachments = note.get('attachments')
        if attachments: 
            for attachment in attachments: 
                entity = sg_request.get_attachment(attachment.get('id'))
                if entity: 
                    # find thumbnail first 
                    imgPath = self.attachment_path(entity, thumbnail=True)
                    if not os.path.exists(imgPath): 
                        url = entity.get('image')
                        sg_request.download_thumbnail(url, imgPath)
                    imgPaths.append(imgPath)

        return (note.get('id'), imgPaths)


    def fetch_attachment_finished(result): 
        noteId, imgPaths = result
        for item in self.items: 
            data = item.data(QtCore.Qt.UserRole)
            note = data['note']
            if noteId == note.get('id'): 
                for index, imgPath in enumerate(imgPaths): 
                    button = widget.add_image(index, imgPath=imgPath)
                    button.clicked.connect(partial(self.attachment_clicked, attachEntity))

                QtWidgets.QApplication.processEvents()
                item.setSizeHint(widget.sizeHint())

    def download_attachment(self, attachEntity, dst, openFile=True): 
        """ download attachment """ 
        self.downloadCount += 1 
        self.set_progress_status('Downloading %s images' % self.downloadCount)
        result = sg_request.download_attachment(attachEntity, dst)
        return (result, attachEntity, openFile)

    def download_finished(self, result): 
        path, attachEntity, openFile = result
        path = path.replace('/', '\\')
        self.downloadCount -=1 
        
        # open file 
        if openFile: 
            os.startfile(path)
        self.set_progress_status('Finished download images', 'debug', True)
        
    def attachment_clicked(self, attachEntity, openFile=True): 
        dst = self.attachment_path(attachEntity)
        path = os.path.dirname(dst)

        if not os.path.exists(path): 
            os.makedirs(path)

        if not os.path.exists(dst): 
            # thread 
            worker = thread_pool.Worker(partial(self.download_attachment, attachEntity, dst, openFile))
            worker.signals.result.connect(self.download_finished)
            worker.signals.error.connect(self.collect_error)
            self.threadpool.start(worker)

        else: 
            if openFile: 
                os.startfile(dst)

    def attachment_path(self, attachmentEntity, thumbnail=False): 
        """ get attachment path """ 
        dst = note_config.attachment_path(self.entity.copy(), attachmentEntity, thumbnail)
        return dst 


    def display_notes(self, noteDict): 
        """ display notes """  
        self.displayWidget.clear()
        if not noteDict: 
            self.displayWidget.addItem('-- No Briefs --')

        for note in noteDict.keys()[::-1]: 
            item = self.add_item(noteDict[note])
            self.items.append(item)

        self.set_filter_items()


    def add_item(self, noteInfo): 
        """ add item information """
        noteEntity = noteInfo.get('note')
        noteEntity = noteInfo.get('note')
        steps = list(set([a.get('name') for a in noteInfo.get('steps')]))
        attachments = [a.get('name') for a in noteInfo.get('attachments')]
        versions = [a.get('code') for a in noteInfo.get('versions')] if noteInfo.get('versions') else []
        tasks = list(set([a.get('name') for a in noteInfo.get('tasks')] if noteInfo.get('tasks') else []))
        colorStatus = note_config.Color.statusMap.get(noteEntity.get('sg_status_list'))
        noteType = noteEntity.get('sg_note_type')
        userIcon = noteEntity.get('user').get('path') or Default.userIcon
        if not noteType: 
            noteType = note_config.Default.noteTypeDefault # Fix

        infoDict = {'comment': noteEntity.get('content'), 
                    'date': noteEntity.get('created_at'), 
                    'userName': noteEntity.get('user').get('name'), 
                    'status': noteEntity.get('sg_status_list'), 
                    'noteType': noteType, 
                    'steps': steps, 
                    'tasks': tasks, 
                    'versions': versions, 
                    'userIcon': userIcon
                    }


        item = QtWidgets.QListWidgetItem(self.displayWidget)
        item.setData(QtCore.Qt.UserRole, noteInfo)
        item.setFlags(item.flags() & ~QtCore.Qt.ItemIsSelectable)
        note_config.set_tool_tip(item, noteEntity)

        noteItem = note_item.NoteItem()
        self.set_note_item(infoDict, noteItem)

        item.setSizeHint(noteItem.sizeHint())
        self.displayWidget.addItem(item)
        self.displayWidget.setItemWidget(item, noteItem)

        return item

    def set_note_item(self, infoDict, widgetItem): 
        comment = infoDict.get('comment')
        date = infoDict.get('date')
        name = infoDict.get('userName')
        status = infoDict.get('status')
        noteType = infoDict.get('noteType')
        steps = infoDict.get('steps', [])
        tasks = infoDict.get('tasks', [])
        versions = infoDict.get('versions')
        userIcon = infoDict.get('userIcon')

        # calculate color 
        colorStatus = note_config.Color.statusMap.get(status)
        # map nice name
        displayStatus = note_config.Note.displayMap.get(status)

        widgetItem.set_comment(comment) if comment else None
        widgetItem.set_date(date) if date else None 
        widgetItem.set_name(name) if name else None
        widgetItem.set_status(displayStatus) if displayStatus else None 

        widgetItem.set_type(noteType or '-') if noteType else None
        widgetItem.set_type_color(colorStatus) if colorStatus else None 
        widgetItem.set_link(steps, tasks) if steps or tasks else None 
        widgetItem.set_versions(versions) if versions else None 
        widgetItem.set_user_icon(userIcon) if userIcon else None 

    def submit_note(self): 
        content = self.commentTextEdit.toPlainText()
        attachments = self.snapListWidget.get_images()

        if content or attachments: 
            self.set_progress_status('Submiting note ...', 'debug')
            worker = thread_pool.Worker(partial(self.create_note, content, attachments))
            worker.signals.result.connect(self.submit_note_finished)
            self.threadpool.start(worker)
            self.commentTextEdit.setEnabled(False)
            self.submitButton.setEnabled(False)
        else: 
            self.set_progress_status('Please write note before submit', 'warning', True)

    def create_note(self, content, attachments): 
        cmd_utils.sleep()
        user = self.user.name()
        noteType = str(self.noteTypeComboBox.currentText())
        step, sgStep = self.stepComboBox.comboBox.itemData(self.stepComboBox.comboBox.currentIndex(), QtCore.Qt.UserRole)
        subject = '[%s] submitted a note on "%s" through RF Panel' % (user, step)
        status = note_config.Note.typeMap[noteType]

        result = sg_request.create_note(project=self.entity.project, entityType=self.entity.entity_type, 
                                        entityName=self.entity.name, 
                                        subject=subject, 
                                        content=content, 
                                        step=sgStep, 
                                        user=self.user.sg_user(), 
                                        noteType=noteType, 
                                        status=status)
        
        if attachments: 
            self.set_progress_status('Uploading %s attachments ...' % len(attachments))
            attachmentResults = sg_request.upload_attachments(result.get('id'), attachments)
        return result

    def submit_note_finished(self, result): 
        logger.debug(result)
        self.commentTextEdit.setEnabled(True)
        self.submitButton.setEnabled(True)
        self.commentTextEdit.clear()
        self.snapListWidget.clear()
        self.snapListWidget.setVisible(False)
        self.set_progress_status('Submit success', 'info', True)

        self.clear_caches()
        self.list_notes()

    def display_captured_image(self, imagePath): 
        """ display snapped image """
        self.snapListWidget.add_item(imagePath)
        self.snapListWidget.setVisible(True)

    def preview_deleted(self): 
        """ if preview all deleted, hide widget """
        if self.snapListWidget.displayWidget.count() == 0: 
            self.snapListWidget.setVisible(False)

    def show_note_options(self, pos): 
        """ display status options """ 
        item = self.displayWidget.currentItem()
        menu = QtWidgets.QMenu(self)
        for status in note_config.Note.menuChoices: 
            sgStatus = note_config.Note.statusMap.get(status)
            label = 'Set %s' % status
            statusMenu = menu.addAction(label)
            statusMenu.triggered.connect(partial(self.status_triggered, item, sgStatus))
        
        menu.popup(self.displayWidget.mapToGlobal(pos))


    def status_triggered(self, item, status): 
        """ when status menu clicked """
        self.set_progress_status('Updating status ...')
        data = item.data(QtCore.Qt.UserRole)
        note = data.get('note')
        noteId = note.get('id')
        user = self.user.sg_user()
        worker = thread_pool.Worker(partial(self.update_note, item, noteId, status, user))
        worker.signals.result.connect(self.update_note_finished)
        self.threadpool.start(worker)


    def update_note(self, item, noteId, status, userEntity): 
        result = sg_request.update_note(noteId, status, userEntity)
        return (item, result)

    def update_note_finished(self, result): 
        item, noteEntity = result
        status = noteEntity.get('sg_status_list')

        itemWidget = self.displayWidget.itemWidget(item)
        infoDict = {'status': status}
        self.set_note_item(infoDict, itemWidget)
        # itemWidget.set_status(status, colorStatus)
        self.set_progress_status('Finished', 'debug', True)


    def set_filter_items(self, *args): 
        data = self.stepComboBox.comboBox.itemData(self.stepComboBox.comboBox.currentIndex(), QtCore.Qt.UserRole)
        step = ''
        if data: 
            step, sgStep = data 

        # collect all data as tags 
        # 1. collect steps, 
        # 2. collect status 
        # 3. collect noteType 
        count = 0 
        for item in self.items: 
            data = item.data(QtCore.Qt.UserRole)
            noteEntity = data.get('note')
            steps = [a.get('name').lower() for a in data.get('steps')]
            # print tags
            hidden = True 
            show = False 
            if step in steps: 
                show = True 

            else: 
                if not steps: 
                    show = True 

            if show: 
                hidden = False

            # set item 
            self.displayWidget.setItemHidden(item, hidden)

            # set size 
            QtWidgets.QApplication.processEvents()
            itemWidget = self.displayWidget.itemWidget(item)
            item.setSizeHint(itemWidget.sizeHint()) 
            if not hidden: 
                count+=1 
        
        self.set_summary(count)

    def fit_items(self): 
        items = [self.displayWidget.item(a) for a in range(self.displayWidget.count())]

        if items: 
            for item in items: 
                itemWidget = self.displayWidget.itemWidget(item)
                if itemWidget: 
                    item.setSizeHint(itemWidget.sizeHint()) 

            QtWidgets.QApplication.processEvents()


    def set_summary(self, count): 
        message = '%s comments' % count 
        self.summaryLabel.setText(message)




    # def set_filter_items(self, *args): 
    #     data = self.stepComboBox.comboBox.itemData(self.stepComboBox.comboBox.currentIndex(), QtCore.Qt.UserRole)
    #     if data: 
    #         step, sgStep = data 
    #         for item in self.items: 
    #             noteDict = item.data(QtCore.Qt.UserRole)
    #             steps = [a.get('name').lower() for a in noteDict.get('steps')]
    #             hidden = True 
    #             if not steps: 
    #                 # this mean show
    #                 hidden = False 
    #             else: 
    #                 if step.lower() in steps: 
    #                     hidden = False 

    #             if self.showAllCheckBox.isChecked(): 
    #                 hidden = False 

    #             self.displayWidget.setItemHidden(item, hidden)
                
    #             # set size 
    #             QtWidgets.QApplication.processEvents()
    #             itemWidget = self.displayWidget.itemWidget(item)
    #             item.setSizeHint(itemWidget.sizeHint())
    def lock_tabs(self, state): 
        tabs = self.parent.tabWidget.count()
        index = self.parent.tabWidget.currentIndex()

        for i in range(tabs): 
            if not i == index: 
                self.parent.tabWidget.setTabEnabled(i, state)


    def set_loading(self): 
        widget = loading_widget.LoadingCircle()
        widget.start()
        item = QtWidgets.QListWidgetItem(self.displayWidget)
        item.setSizeHint(widget.sizeHint())
        item.setFlags(item.flags() & ~QtCore.Qt.ItemIsSelectable)

        self.displayWidget.addItem(item)
        self.displayWidget.setItemWidget(item, widget)

    def collect_error(self, error): 
        logger.warning(error)

    def set_progress_status(self, text, level='debug', finished=False): 
        if level == 'debug': 
            logger.debug(text)
        if level == 'info': 
            logger.info(text)
        if level == 'warning': 
            logger.warning(text)

        if not finished: 
            if not self.loadingStatus.loading: 
                self.loadingStatus.start()
            self.progressLabel.setText(text)
        else: 
            self.loadingStatus.stop()
            self.finishTime = round((float((datetime.now() - self.startTime).seconds)), 2)
            logger.debug('Finished in %s' % self.finishTime)
            self.progressLabel.setText('%s in %s secs' % (text, self.finishTime))
