# -*- coding: utf-8 -*-
import os 
import sys 
import subprocess
from collections import OrderedDict
from datetime import datetime 
from functools import partial
import time
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from Qt import QtCore
from Qt import QtGui 
from Qt import QtWidgets

from rf_utils import thread_pool
from ..widgets import step_widget
from ..widgets import note_item
from ..widgets import loading_widget
from ..widgets import snap_widget
from ..widgets import display_snap
from .. import note_config
from .. import sg_request
from rf_utils import user_info
from .. import cmd_utils
reload(cmd_utils)
reload(user_info)
reload(note_config)
reload(snap_widget)
reload(loading_widget)
reload(display_snap)
reload(sg_request)

reload(step_widget)
reload(note_item)

modulePath = sys.modules[__name__].__file__
moduleDir  = os.path.dirname(modulePath)

class Color: 
    projectLabel = 'color: rgb(220, 220, 220);'
    entityLabel = 'color: rgb(180, 220, 240);'
    grpLabel = 'color: rgb(160, 160, 160);'
    submitButton = 'background-color: rgb(80, 160, 240);'
    snapButton = 'background-color: rgb(200, 200, 200);'


class Default: 
    refreshIcon = '%s/icons/refresh.png' % os.path.split(moduleDir)[0]
    captureIcon = '%s/icons/snap.png' % os.path.split(moduleDir)[0]


class Ui(QtWidgets.QWidget):
    """docstring for Ui"""
    def __init__(self, thread=None, parent=None):
        super(Ui, self).__init__(parent)
        self.parent = parent
        self.firstTimeActivation = True
        self.startTime = datetime.now()
        self.setup_ui()

    def setup_ui(self): 
        self.layout = QtWidgets.QVBoxLayout()
        # title layout
        # contain step comboBox and refresh icon 
        self.titleLayout = QtWidgets.QHBoxLayout()
        self.titleSpacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        # refresh button 
        self.refreshButton = QtWidgets.QPushButton()
        self.refreshButton.setMaximumSize(32, 32)
        self.refreshButton.setIcon(QtGui.QIcon(Default.refreshIcon))
        self.refreshButton.setFlat(True)

        self.titleLayout.addItem(self.titleSpacer)
        self.titleLayout.addWidget(self.refreshButton)

        self.titleLayout.setStretch(0, 2)
        self.titleLayout.setStretch(1, 0)


        # progres status 
        self.progressLayout = QtWidgets.QHBoxLayout()
        self.progressLabel = QtWidgets.QLabel()
        self.loadingStatus = loading_widget.LoadingCircle(image=2)

        self.progressLayout.addWidget(self.progressLabel)
        self.progressLayout.addWidget(self.loadingStatus)

        # spacer 
        self.spacer = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.layout.addLayout(self.titleLayout)
        self.layout.addItem(self.spacer)
        self.layout.addLayout(self.progressLayout)

        self.layout.setStretch(0, 0)
        self.layout.setStretch(1, 1)
        self.layout.setStretch(2, 0)

        self.setLayout(self.layout)

    def refresh(self, entity): 
        """ ui tab start here """ 
        logger.debug('Refreshing %s ...' % self.windowTitle())
        self.entity = entity
        self.set_progress_status('Test', 'info', True)

        # init ui 
        if entity.valid: 
            if self.firstTimeActivation: 
                self.firstTimeActivation = False
                self.setup_static_info()
        else: 
            self.set_progress_status('Not in the pipeline. Stop loading', 'info', True)


    def setup_static_info(self): 
        pass 


    def set_progress_status(self, text, level='debug', finished=False): 
        if level == 'debug': 
            logger.debug(text)
        if level == 'info': 
            logger.info(text)
        if level == 'warning': 
            logger.warning(text)

        if not finished: 
            if not self.loadingStatus.loading: 
                self.loadingStatus.start()
            self.progressLabel.setText(text)
        else: 
            self.loadingStatus.stop()
            self.finishTime = round((float((datetime.now() - self.startTime).seconds)), 2)
            logger.debug('Finished in %s' % self.finishTime)
            self.progressLabel.setText('%s in %s secs' % (text, self.finishTime))


