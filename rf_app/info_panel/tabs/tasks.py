# -*- coding: utf-8 -*-
import os 
import sys 
import subprocess
from collections import OrderedDict
from datetime import datetime 
from functools import partial
import time
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from Qt import QtCore
from Qt import QtGui 
from Qt import QtWidgets

from rf_utils import thread_pool
from ..widgets import step_checkbox
from ..widgets import note_item
from ..widgets import loading_widget
from ..widgets import snap_widget
from ..widgets import display_snap
from .. import note_config
from .. import sg_request
from rf_utils.sg import sg_process
from rf_utils import user_info
from .. import cmd_utils
# from rf_utils.widget import status_widget
from rf_utils.widget.status_widget import Icon

from rf_shotgun_event_daemon.src.plugins import status_update
reload(status_update)
from rf_utils.sg import sg_process
from rf_utils import project_info
from rf_utils.pipeline.notification import noti_module_cliq
reload(noti_module_cliq)

# reload(status_widget)
reload(cmd_utils)
reload(user_info)
reload(note_config)
reload(snap_widget)
reload(loading_widget)
reload(display_snap)
reload(sg_request)

reload(step_checkbox)
reload(note_item)

modulePath = sys.modules[__name__].__file__
moduleDir  = os.path.dirname(modulePath)

class Color: 
    projectLabel = 'color: rgb(220, 220, 220);'
    entityLabel = 'color: rgb(180, 220, 240);'
    grpLabel = 'color: rgb(160, 160, 160);'
    submitButton = 'background-color: rgb(80, 160, 240);'
    snapButton = 'background-color: rgb(200, 200, 200);'


class Default: 
    separator = '%s/icons/separator.png' % os.path.split(moduleDir)[0]
    refreshIcon = '%s/icons/refresh.png' % os.path.split(moduleDir)[0]
    captureIcon = '%s/icons/snap.png' % os.path.split(moduleDir)[0]
    userIcon = '%s/icons/user.png' % os.path.split(moduleDir)[0]
    noTask = 'No Task'
    noStep = 'No Department'
    noUser = 'No User'

class Config:
    asset = 'Asset'
    shot = 'Shot'
    mode = {asset: 'asset', shot: 'scene'}


class Ui(QtWidgets.QWidget):
    """docstring for Ui"""
    def __init__(self, thread=None, parent=None):
        super(Ui, self).__init__(parent)
        self.parent = parent
        self.firstTimeActivation = True
        self.startTime = datetime.now()
        self.user = user_info.User()
        self.entity = None

        self.threadpool = QtCore.QThreadPool() if not thread else thread
        self.threadpool.setMaxThreadCount(1)

        self.stepList = []

        self.setup_ui()
        self.init_signals()

    def setup_ui(self): 
        self.layout = QtWidgets.QVBoxLayout()
        # title layout
        # contain step comboBox and refresh icon 
        self.titleLayout = QtWidgets.QHBoxLayout()
        self.titleSpacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        # refresh button 
        self.refreshButton = QtWidgets.QPushButton()
        self.refreshButton.setMaximumSize(32, 32)
        self.refreshButton.setIcon(QtGui.QIcon(Default.refreshIcon))
        self.refreshButton.setFlat(True)

        self.titleLayout.addItem(self.titleSpacer)
        self.titleLayout.addWidget(self.refreshButton)

        self.titleLayout.setStretch(0, 2)
        self.titleLayout.setStretch(1, 0)

        # task display widget 
        self.taskDisplayWidget = QtWidgets.QListWidget()

        self.stepCheckBox = step_checkbox.StepCheckBox()


        # progres status 
        self.progressLayout = QtWidgets.QHBoxLayout()
        self.progressLabel = QtWidgets.QLabel()
        self.loadingStatus = loading_widget.LoadingCircle(image=2)

        self.progressLayout.addWidget(self.progressLabel)
        self.progressLayout.addWidget(self.loadingStatus)

        # spacer 
        self.spacer = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.layout.addLayout(self.titleLayout)
        self.layout.addWidget(self.taskDisplayWidget)
        self.layout.addWidget(self.stepCheckBox)
        self.layout.addLayout(self.progressLayout)

        self.layout.setStretch(0, 0)
        self.layout.setStretch(1, 1)
        self.layout.setStretch(2, 0)

        self.setLayout(self.layout)

    def init_signals(self):
        self.refreshButton.clicked.connect(self.refresh_clicked)

    def refresh_clicked(self): 
        # clear cache 
        if self.entity.user != self.entity.context.noContext:
            self.user = user_info.User(nickname=self.entity.user)
        sg_request.allTaskCaches = dict()
        # list versions 
        self.list_tasks()

    def setup_dynamic_info(self): 
        # list tasks 
        self.list_tasks()

    def set_progress_status(self, text, level='debug', finished=False): 
        if level == 'debug': 
            logger.debug(text)
        elif level == 'info': 
            logger.info(text)
        elif level == 'warning': 
            logger.warning(text)

        if not finished: 
            if not self.loadingStatus.loading: 
                self.loadingStatus.start()
            self.startTime = datetime.now()
            self.progressLabel.setText(text)
        else: 
            self.loadingStatus.stop()
            self.finishTime = (datetime.now() - self.startTime).total_seconds()
            logger.debug('Finished in %s' % self.finishTime)
            self.progressLabel.setText('%s in %s secs' % (text, self.finishTime))

    def refresh(self, entity): 
        """ ui tab start here """ 
        logger.debug('Refreshing %s ...' % self.windowTitle())
        self.entity = entity

        # init ui 
        if entity.valid: 
            if self.firstTimeActivation: 
                self.firstTimeActivation = False
                self.setup_dynamic_info()
        else: 
            self.set_progress_status('Not in the pipeline. Stop loading', 'info', True)

    def list_tasks(self): 
        self.set_progress_status('Fetching tasks ...', 'info')
        worker = thread_pool.Worker(self.fetch_tasks)
        worker.signals.result.connect(self.fetch_tasks_finished)
        self.threadpool.start(worker)

    def fetch_tasks(self): 
        cmd_utils.sleep()
        
        # refresh display
        sg_request.clear_caches(cacheType='taskStep')  # need to clear cache first

        tasks = sg_request.fetch_tasks(self.entity.project, self.entity.entity_type, self.entity.name)
        return tasks 

    def fetch_tasks_finished(self, results): 
        self.tasks = results
        # print self.tasks
        # get previous checked checkbox
        previous_cb = []
        for cb in self.stepList:
            previous_cb.append(cb.text())

        allCheckBox = self.stepCheckBox.add_ui(results)
        for label, checkBox in allCheckBox.iteritems():
            checkBox.stateChanged.connect(partial(self.display_tasks, results))
            for cb in previous_cb:
                if label == cb:
                    checkBox.setChecked(True)

        self.display_tasks(results)
        self.set_progress_status('Finished', 'info', True)

    def display_tasks(self, tasks, *args): 
        self.taskDisplayWidget.clear()

        taskDict = {}
        for taskEntity in tasks[::-1]:
            taskList = []
            task = Task(taskEntity)
            # self.task = taskEntity # have multiple taskentity

            taskStep = task.step()

            if taskStep in taskDict:
                taskList = taskDict.get(taskStep)
            taskList.append(taskEntity)
            taskDict[taskStep] = taskList

        self.stepList = self.stepCheckBox.set_filter_step()

        for checkBox in self.stepList:
            step = checkBox.text()
            if step in taskDict:
                self.task_item(step, taskDict[step])

        if not tasks: 
            self.taskDisplayWidget.addItem('-- No Task --')

    def task_item(self, step, taskEntityList):
        # taskFrame = QtWidgets.QFrame()
        # taskLayout = QtWidgets.QVBoxLayout()

        stepTitle = QtWidgets.QLabel('%s' % step)
        newfont = QtGui.QFont("Times", 12, QtGui.QFont.Bold) 
        stepTitle.setFont(newfont)
        stepTitle.setFixedSize(100, 32)

        item = QtWidgets.QListWidgetItem(self.taskDisplayWidget)
        self.taskDisplayWidget.addItem(item)
        self.taskDisplayWidget.setItemWidget(item,stepTitle)
        item.setData(QtCore.Qt.UserRole,stepTitle)
        item.setSizeHint(stepTitle.sizeHint())

        assignedList = self.set_assigned_list_by_step(taskEntityList)

        for taskEntity in reversed(taskEntityList):
            task = Task(taskEntity)
            status = task.status()
            taskName = task.task()
            assigned = task.assigned()
            assignedStr = ', '.join(assigned)
            task.assignedList = assignedList
            nice_status = Icon.statusMap[status]['display']
            buttonTxt = '{}    -    ( {} )'.format(taskName, assignedStr)
            # print buttonTxt
            taskButton = QtWidgets.QPushButton(buttonTxt)
            taskButton.setMaximumSize(500, 32)
            # taskButton.setIcon(QtGui.QIcon(Default.refreshIcon))
            # get icon path
            icon = QtGui.QIcon('{0}/{1}'.format(Icon.path, Icon.statusMap[status]['icon']))
            taskButton.setIcon(icon)

            taskButton.setStyleSheet("QPushButton { text-align: left; }")
            taskButton.setFlat(True)
            taskButton.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
            # taskButton.clicked.connect(self.show_status_menu)

            item = QtWidgets.QListWidgetItem(self.taskDisplayWidget)
            self.taskDisplayWidget.addItem(item)
            self.taskDisplayWidget.setItemWidget(item,taskButton)
            item.setData(QtCore.Qt.UserRole,task)
            item.setSizeHint(taskButton.sizeHint())

            taskButton.customContextMenuRequested.connect(partial(self.show_status_menu, taskButton, item))

        line = QtWidgets.QLabel()
        line.setPixmap(QtGui.QPixmap(Default.separator))
        line.setAlignment(QtCore.Qt.AlignLeft)
        item = QtWidgets.QListWidgetItem(self.taskDisplayWidget)
        self.taskDisplayWidget.addItem(item)
        self.taskDisplayWidget.setItemWidget(item,line)
        item.setSizeHint(line.sizeHint())

    def set_assigned_list_by_step(self, taskEntityList):
        assignedList = []
        for taskEntity in reversed(taskEntityList):
            task = Task(taskEntity)
            assinged = task.assigned()
            for person in assinged:
                if not person in assignedList:
                    assignedList.append(person)
        return assignedList

    def show_status_menu(self, button ,item, pos):
        # statusList = ['wtg' , 'rdy', 'ip', 'rev', 'fix', 'hld', 'p_apr', 'apr', 'omt']
        task = item.data(QtCore.Qt.UserRole)
        if self.user.is_producer() or self.user.is_admin() or self.user.is_supervisor() or self.user.sg_user()['name']  in task.assignedList:  # only allow menu for set status to producer or admin only
            statusList = Icon.statusMap.keys()
            menu = QtWidgets.QMenu(self)

            # task = item.data(QtCore.Qt.UserRole)
            status = task.status()

            for status in statusList:
                statusMenu = menu.addAction(Icon.statusMap[status]['display'])
                # statusMenu.setIcon(QtGui.QIcon(Default.refreshIcon))
                icon = QtGui.QIcon('{0}/{1}'.format(Icon.path, Icon.statusMap[status]['icon']))
                statusMenu.setIcon(icon)
                statusMenu.triggered.connect(partial(self.update_status, button, task, status))

            menu.popup(button.mapToGlobal(pos))

    def update_status(self, button, task, newStatus, *args):
        if self.user.is_producer() or self.user.is_admin() or self.user.is_supervisor() or self.user.sg_user()['name']  in task.assignedList:
            taskId = task.id()
            # set SG status
            sg_process.set_task_status(taskId, newStatus)

            # # refresh display
            # sg_request.clear_caches(cacheType='taskStep')  # need to clear cache first

            taskEntity = sg_process.get_task_from_id(taskId)
            project_entity = taskEntity['project']
            proj = project_info.ProjectInfo(project=project_entity['name'])
            tasks_trigger_table = proj.task.dependency(Config.mode[taskEntity['entity']['type']])

            description = ''
            userEntity =  self.user.sg_user()

            taskResult = []

            if taskEntity['step']['name'] == 'Design':
                if newStatus == 'rev':
                    taskResult = [taskEntity]
                elif newStatus == 'apr' or newStatus == 'fix' : # newStatus == 'apr'
                    triggerStep = 'Model'
                    triggerTaskName = 'Model'
                    taskResult = [sg_process.get_one_task_by_step(taskEntity['entity'], triggerStep, triggerTaskName)]
                    taskResult.append(taskEntity)


            else: 
                if newStatus == 'rev':
                    taskResult = [taskEntity]

                elif newStatus == 'apr' or newStatus == 'fix' or newStatus == 'p_aprv': # newStatus == 'apr'
                    taskResult = status_update.update_dependency_event_api(tasks_trigger_table, taskEntity)
                    taskResult.append(taskEntity)
                    taskname = taskEntity.get('content')
                    if taskname == 'rigUv' or taskname == 'rig':
                        anim = {'step':{'name':'anim'}, 'task_assignees':[{'name':'-'}], 'project':taskEntity['project'], 'sg_status_list':'def' }
                        taskResult.append(anim)

            emails = noti_module_cliq.get_notification_data(taskResult, taskEntity, description, userEntity)

            imgurl = taskEntity['image']
            if not imgurl:
                imgurl = 'https://previewengine-accl.zoho.com/image/WD/54mxc9836fa8a51d545eab56f79f735e2029c?width=2046&height=1536'
            urlImage = [imgurl]

            self.list_tasks()

            noti_module_cliq.send_notification(emails, taskEntity, urlImage=urlImage)

            # TODO: trigger next department 
            # TODO: if new status is aprv, notify user by bot

    def add_task_item(self, taskEntity): 
        item = QtWidgets.QListWidgetItem(self.taskDisplayWidget)
        item.setData(QtCore.Qt.UserRole, taskEntity)
        item.setFlags(item.flags() & ~QtCore.Qt.ItemIsSelectable)
        note_config.set_tool_tip(item, taskEntity)

        task = Task(taskEntity)

        taskName = task.task()
        taskStep = task.step()
        taskStatus = task.status()

        return item

class Task(object):
    """docstring for Task"""
    def __init__(self, taskEntity):
        super(Task, self).__init__()
        self.taskEntity = taskEntity
        self.attr = ['user', 'entity', 'sg_task', 'sg_task.Task.step', 'sg_status_list', 'sg_uploaded_movie', 'image', 'sg_path_to_movie', 'code', 'description', 'created_at']
        self.assignedList = []

    def task(self): 
        return self.taskEntity.get('content')

    def id(self):
        return self.taskEntity.get('id')

    def assigned(self):
        assigned = self.taskEntity.get('task_assignees')
        return [a['name'] for a in assigned] 

    def step(self): 
        stepEntity = self.taskEntity.get('step')
        department = stepEntity.get('name') if stepEntity else Default.noStep
        return department 

    def status(self): 
        return self.taskEntity.get('sg_status_list')