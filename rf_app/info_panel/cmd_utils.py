import os 
import sys
import subprocess
import note_config
reload(note_config)
import time 

def create_thumbnail(path, dst, sizeW=100, keepRatio=True): 
    try: 
        from PIL_Maya import Image
    except ImportError: 
        from PIL import Image
    size = sizeW, sizeW
    im = Image.open(path)
    im.thumbnail(size, Image.ANTIALIAS)
    im.save(dst, "PNG")


def open_dir(path): 
    dirname = os.path.dirname(path).replace('/', '\\')
    if os.path.exists(dirname): 
        subprocess.Popen(r'explorer /select,"%s"' % dirname)

def open_in_rv(path): 
    rvPath = note_config.Software.rv
    if not os.path.exists(rvPath):
        root, end = rvPath.split('RV-')
        version = [a for a in os.listdir(root) if 'RV-' in a]
        if version:
            rvPath = '%s%s/%s' % (root, version[0], '/'.join(end.split('/')[1:]))
        else:
            # dialog.MessageBox.error('Error', 'No RV installed. Use default player')
            os.startfile(path)
            return 

    commands = [rvPath]
    commands += [path]
    subprocess.Popen(commands)


def open_rv_screeningRoom(versions): 
    openShotgunVersions('https://riffanimation.shotgunstudio.com', versions)


def openShotgunVersions(url, versions):
    import webbrowser
    command = """ -sendEvent 'gma-play-entity' '{"protocol_version":1,"server":"%s","type":"Version","ids":[%s]}""" % (url, ','.join([str(i) for i in versions]))
    baked = "rvlink://baked/%s" % (command.encode("hex"))
    webbrowser.open(baked)


def sleep(): 
    time.sleep(0.05)


def submit(): 
    pass 






