# v.0.0.1 polytag switcher
_title = 'Riff SG Panel'
_version = 'v.0.0.1'
_des = 'beta'
uiName = 'RFPanelUI'

#Import python modules
import sys
import os
import getpass
import logging

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
import rf_config as config

# framework modules
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import main_ui
reload(main_ui)



class RFPanel(QtWidgets.QMainWindow):
    def __init__(self, entity=None, parent=None):
        #Setup Window
        super(RFPanel, self).__init__(parent)

        # ui read
        self.layout = QtWidgets.QVBoxLayout()
        self.ui = main_ui.MainUi(entity=entity)
        self.layout.addWidget(self.ui)
        self.setLayout(self.layout)

        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)

        # title
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.resize(1200, 750)


def show(entity=None):
    logger.info('Run in standalone\n')
    app = QtWidgets.QApplication.instance()
    if not app:
        app = QtWidgets.QApplication(sys.argv)
    myApp = RFPanel(entity)
    myApp.show()
    stylesheet.set_default(app)
    sys.exit(app.exec_())


if __name__ == '__main__':
    show()