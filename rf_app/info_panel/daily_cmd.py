import os 
import sys 
import shutil
from rf_utils import daily


def submit(entity, taskEntity, files): 
    """ submit daily folder and shotgun """ 
    entity.context.update(publishVersion=entity.subversion)


    # process raw images 
    # make hero 
    name, ext = os.path.splitext(files[0])
    heroName = entity.publish_name(ext=ext, hero=True)
    outputHeroImgPath = entity.path.output_hero_img().abs_path()
    
    # version 
    filename = entity.publish_name(ext=ext, hero=False)
    outputImgPath = entity.path.outputImg().abs_path()

    print 'outputHeroImgPath', outputHeroImgPath
    print 'outputImgPath', outputImgPath

    # copy to hero 
    status, heroImgs = covert_to_sequences(outputHeroImgPath, heroName, files)
    # copy to version 
    status, versionImgs = covert_to_sequences(outputImgPath, filename, files)

    # submit daily folder 
    dailyResult = daily.submit(entity, heroImgs)

    return versionImgs


def guess_res(taskEntity): 
    taskName = taskEntity.get('content')
    names = taskName.split('_')
    if len(names) > 1: 
        res = names[-1]
    else: 
        res = 'md'
    return res 


def covert_to_sequences(dstDir, filename, imgs):
    filename, ext = os.path.splitext(filename)
    outputs = []
    for i, img in enumerate(imgs):
        seqName = '%s.%04d%s' % (filename, i+1, ext)
        dst = '%s/%s' % (dstDir, seqName)
        if not os.path.exists(dstDir):
            os.makedirs(dstDir)
        shutil.copy2(img, dst)

        if os.path.exists(dst):
            outputs.append(dst)

    return (True, outputs) if len(imgs) == len(outputs) else (False, outputs)



def submit_daily(entity): 
    from rf_utils import publish_info 
    reload(publish_info)
    from rf_utils import daily
    # this set publish version
    regInfo = publish_info.PreRegister(entity)
    entity.context.update(publishVersion=entity.subversion)

    outputImgPath = asset.path.outputImg().abs_path()
    outputHeroImgPath = asset.path.output_hero_img().abs_path()

    # collect imgs
    imgs = self.collect_snap(asset, outputImgPath)
    heroImgs = self.collect_snap(asset, outputHeroImgPath, hero=False)
    dailyResult = daily.submit(asset, heroImgs)

    # restore
    asset.context.update(publishVersion=publishVersion)

    # create version
    versionResult = sg_hook.publish_daily(asset, taskEntity, status, imgs, userEntity, description)
    taskResult = sg_hook.update_task(asset, taskEntity, status)


def publish_daily(entity, files): 


    
    filename = entity.publish_name(ext=ext, hero=hero)

    if len(files) == 1: 
        pass 
    elif len(files) > 1: 
        status, outputs = cmd_utils.covert_to_sequences(dst, filename, files)

    imgs = self.collect_snap(asset, outputImgPath)
    heroImgs = self.collect_snap(asset, outputHeroImgPath, hero=False)
    dailyResult = daily.submit(asset, heroImgs)