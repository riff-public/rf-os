# -*- coding: utf-8 -*-
import os 
import sys 
import logging
import rf_config
from functools import partial 
from collections import OrderedDict
from Qt import QtCore
from Qt import QtGui 
from Qt import QtWidgets
from rf_utils import icon
from rf_utils import project_info
import tabs
from rf_utils.context import context_info
from widgets import override_widget
reload(override_widget)

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

modulePath = sys.modules[__name__].__file__
moduleDir  = os.path.dirname(modulePath)

class Color: 
    projectLabel = 'color: rgb(220, 220, 220);'
    entityLabel = 'color: rgb(180, 220, 240);'
    grpLabel = 'color: rgb(160, 160, 160);'
    entityOverrideWidget = 'text-align: left; color: rgb(180, 220, 240);'
    override = 'color: rgb(240, 100, 100);'


class Default: 
    """ name of tabs module """ 
    activeTab = 'daily'
    allowedTabs = ['brief', 'tasks', 'notes', 'versions', 'links', 'daily']
    tabNameMap = {'versions': 'Published'}


class Config: 
    entityLinkMap = {'asset': 'Shots', 'scene': 'Assets'}


class MainUi(QtWidgets.QWidget):
    """docstring for MainUi"""
    def __init__(self, entity=None, parent=None):
        super(MainUi, self).__init__(parent)

        self.setup_ui()
        self.entity = self.initialize_context(entity)
        self.init_signals()


    def setup_ui(self): 
        # center layout 
        self.layout = QtWidgets.QVBoxLayout()
        self.scrollArea = QtWidgets.QScrollArea()
        self.scrollWidget = QtWidgets.QWidget()
        self.scrollArea.setWidget(self.scrollWidget)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setFrameShape(QtWidgets.QFrame.VLine)

        # title 
        self.titleLayout = self.add_title()

        # separator 
        self.line = QtWidgets.QFrame()
        self.line.setFrameShape(QtWidgets.QFrame.VLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Raised)

        # info layout 
        self.infoLayout = QtWidgets.QVBoxLayout()
        self.tabWidget = self.add_tab_widget()
        self.infoLayout.addWidget(self.tabWidget)

        # separator 
        self.line2 = QtWidgets.QFrame()
        self.line2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line2.setFrameShadow(QtWidgets.QFrame.Sunken)


        # spacer 
        self.spacerItem = QtWidgets.QSpacerItem(20, 460, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)

        # add to main layout 
        self.layout.addLayout(self.titleLayout)
        self.layout.addWidget(self.line2)
        self.layout.addLayout(self.infoLayout)

        self.setLayout(self.layout)


    def add_title(self): 
        self.logo = QtWidgets.QLabel()
        self.logo.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))

        self.titleLayout = QtWidgets.QHBoxLayout()
        self.titleLayout.addWidget(self.logo)

        # project 
        self.projectLayout = QtWidgets.QVBoxLayout()

        self.projectLabel = QtWidgets.QLabel('Unknown Project')
        font = QtGui.QFont()
        font.setPointSize(16)
        self.projectLabel.setFont(font)
        self.projectLabel.setStyleSheet(Color.projectLabel)

        self.entityLabel = QtWidgets.QLabel('noAsset')
        font = QtGui.QFont()
        font.setPointSize(12)
        self.entityLabel.setFont(font)
        self.entityLabel.setStyleSheet(Color.entityLabel)

        self.entityOverrideWidget = override_widget.OverrideWidget()

        self.grpLabel = QtWidgets.QLabel('type')
        font = QtGui.QFont()
        font.setPointSize(8)
        self.grpLabel.setFont(font)
        self.grpLabel.setStyleSheet(Color.grpLabel)


        self.titleLayout.addLayout(self.projectLayout)
        self.projectLayout.addWidget(self.projectLabel)
        self.projectLayout.addWidget(self.grpLabel)
        self.projectLayout.addWidget(self.entityLabel)
        self.projectLayout.addWidget(self.entityOverrideWidget)

        self.projectLayout.setSpacing(0)
        self.projectLayout.setContentsMargins(0, 0, 0, 0)

        self.titleLayout.setStretch(0, 0)
        self.titleLayout.setStretch(1, 1)
        return self.titleLayout


    def add_tab_widget(self): 
        """ looping for each tabs modules and compare to allowedTab setting, 
            add to tab widgets """ 
        self.tabWidget = QtWidgets.QTabWidget()
        modules = tabs.get_module()
        activeIndex = 0 
        self.tabModules = dict()
        self.threadpool = QtCore.QThreadPool()
        self.threadpool.setMaxThreadCount(1)
        tabNames = []

        for tabName in Default.allowedTabs: 

            for index, name in enumerate(modules): 
                title = Default.tabNameMap.get(name) or name.capitalize()
                if name == tabName: 
                    func = tabs.get_func(name)
                    reload(func)
                    widget = func.Ui(thread=self.threadpool, parent=self)
                    self.tabWidget.addTab(widget, title)
                    if hasattr(widget, 'refresh'): 
                        widget.refresh
                    if name == Default.activeTab: 
                        activeIndex = len(self.tabModules.keys())
                    
                    self.tabModules[name] = widget
        # active tab 
        self.tabWidget.setCurrentIndex(activeIndex)
        return self.tabWidget


    def init_signals(self): 
        # signal tab 
        self.tabWidget.currentChanged.connect(self.tab_clicked)
        # signal restore 
        self.entityOverrideWidget.button.clicked.connect(self.restore)

    def setup_default(self): 
        """ default value for ui """ 
        self.set_links_tab()

    def set_links_tab(self): 
        possibleTabNames = [v for k, v in Config.entityLinkMap.iteritems()] + ['Links']
        for i in range(self.tabWidget.count()): 
            tabName = self.tabWidget.tabText(i)
            if tabName in possibleTabNames: 
                self.tabWidget.setTabText(i, Config.entityLinkMap.get(self.entity.entity_type) or 'Links')


    def tab_clicked(self, index): 
        """ signal when click tab """ 
        self.refresh(self.entity)

    def restore(self): 
        self.initialize_context()

    
    def refresh(self, entity): 
        """ activate ui logic """ 
        widget = self.tabWidget.currentWidget()
        widget.refresh(self.entity)


    def initialize_context(self, entity=None): 
        """ setup context info """       
        from rf_utils.context import context_info  
        self.entity = entity
        self.reset_tab_caches()

        if not entity: 
            self.entity = context_info.ContextPathInfo()
        self.set_title(self.entity)
        self.refresh(self.entity)
        self.setup_default()
        return self.entity 

    def reset_tab_caches(self): 
        """ clear first time activate each tab """ 
        if self.tabModules: 
            for tabName, widget in self.tabModules.iteritems(): 
                if hasattr(widget, 'firstTimeActivation'): 
                    widget.firstTimeActivation = True 


    def set_title(self, entity): 
        project = 'Unknown Project'
        entityType = 'no type'
        name = 'noAsset' 
        override = False 

        if entity.valid: 
            project = entity.project 
            entityType = entity.entity_type
            name = entity.name

            if rf_config.isMaya: 
                localEntity = context_info.ContextPathInfo()
                if localEntity.valid: 
                    project = entity.project 
                    name = entity.name 
                    entityType = entity.type
                    if not entity.name == localEntity.name: 
                        override = True


        self.projectLabel.setText(project)
        self.entityLabel.setText(name)
        self.grpLabel.setText(entityType)

        if override: 
            self.entityLabel.setText('%s (override)' % name)
            self.entityLabel.setStyleSheet(Color.override)
            self.entityOverrideWidget.set_override('%s' % localEntity.name)
            logger.debug('Override with %s' % entity)

        else: 
            self.entityLabel.setText('%s' % name)
            self.entityLabel.setStyleSheet(Color.entityLabel)
            self.entityOverrideWidget.clear()
            logger.debug('initialized with scene context %s' % entity)

