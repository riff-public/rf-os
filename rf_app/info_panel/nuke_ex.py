from PySide2 import QtGui, QtCore, QtWidgets 
import nuke 
from nukescripts import panels

class PanelTest(QtWidgets.QWidget):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.setLayout(QtWidgets.QVBoxLayout())
        self.myTable    = QtWidgets.QTableWidget()
        self.myTable.header = ['Date', 'Files', 'Size', 'Path' ]
        self.myTable.size = [ 75, 375, 85, 600 ]
        self.myTable.setColumnCount(len(self.myTable.header))
        self.myTable.setHorizontalHeaderLabels(self.myTable.header)
        self.myTable.setSelectionMode(QtWidgets.QTableView.ExtendedSelection)
        self.myTable.setSelectionBehavior(QtWidgets.QTableView.SelectRows)
        self.myTable.setSortingEnabled(1)
        self.myTable.sortByColumn(1, QtCore.Qt.DescendingOrder)
        self.myTable.setAlternatingRowColors(True)

        self.myTable.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.myTable.setRowCount(50)
        self.layout().addWidget(self.myTable)

        self.myTable.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding))
        self.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding))

        def closeEvent():
            print 'Closed PanelTest'

class mainPanel(QtWidgets.QWidget):
    def __init__(self):
        QtWidgets.QWidget.__init__(self)
        self.testBtn = QtWidgets.QPushButton()
        self.setLayout( QtWidgets.QVBoxLayout() )
        self.layout().addWidget( self.testBtn )
        self.testBtn.clicked.connect(self.createPanel)
        self.setSizePolicy( QtWidgets.QSizePolicy( QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding))

    def closeEvent():
        print 'Closed mainPanel'

    def createPanel(self):
        pane = nuke.getPaneFor("example.test.panel")
        print pane
        panels.registerWidgetAsPanel('PanelTest', 'PanelTest',"example.test.panel", True).addToPane(pane)

mp = mainPanel()
mp.createPanel()