import os 
import sys 
from rf_utils.sg import sg_process
from rf_utils import user_info
from urllib import urlopen
import urllib
from collections import OrderedDict
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

sg = sg_process.sg
projectCaches = dict()
versionIdCaches = dict()
versionCaches = dict()
taskCaches = dict()
userCaches = []
entityCaches = dict()
taskStepCaches = dict()


class DataField: 
    project = ['name', 'id']
    note = ['note_links', 'created_at', 'updated_at', 'content', 'subject', 'sg_note_type', 'tasks', 'user', 'sg_status_list', 'attachments', 'sg_modified_by', 'user.HumanUser.image']
    attachment = ['image', 'filename']
    entity = ['code', 'id', 'project']
    user = ['sg_localuser', 'sg_ad_account', 'name', 'department', 'groups', 'image']
    version = ['user', 'entity', 'sg_task', 'sg_task.Task.step', 'sg_status_list', 'sg_uploaded_movie', 'image', 'sg_path_to_movie', 'code', 'description', 'created_at']
    asset = ['shots', 'sg_asset_type', 'code', 'id', 'image', 'project']
    shot = ['assets', 'code', 'id', 'sg_sequence_code', 'image', 'project']
    task = ['content', 'id', 'step', 'sg_status_list', 'task_assignees', 'project', 'sg_resolution', 'entity']
    entityMap = {'asset': shot, 'scene': asset}

class Config: 
    sgEntity = {'asset': 'Asset', 'scene': 'Shot'}
    sgSearchMap = {'asset': 'Shot', 'scene': 'Asset'}
    attrMap = {'asset': 'assets', 'scene': 'shots'}
    entityTypeMap = {'Asset': 'asset', 'Shot': 'scene'}
        

def clear_caches(cacheType=''): 
    """ clear caches """ 
    if cacheType == 'project': 
        global projectCaches
        projectCaches = dict()
    elif cacheType == 'taskStep':
        global taskStepCaches
        taskStepCaches = dict()

def fetch_users(): 
    """ fetch all users """ 
    global userCaches
    if not userCaches: 
        filters = []
        userEntities = sg.find('HumanUser', filters, DataField.user)
        userCaches = userEntities
    return userCaches


def get_user_image(userId):
    allUser = fetch_users()
    userEntities = [a for a in allUser if a.get('id') == userId]
    if userEntities: 
        userEntity = userEntities[0]
        user = user_info.User()
        user.userEntity = userEntity
        return user.get_thumbnail()


def fetch_notes(projectName, entityName): 
    """ get note from selected project and entity name """ 
    project = get_project(projectName)
    filters = [['project', 'is', project], ['note_links', 'name_is', entityName]]
    notes = sg.find('Note', filters, DataField.note)
    return notes 


def fetch_notes_information(projectName, entityName, brief=False): 
    global versionIdCaches
    global taskCaches
    noteDict = OrderedDict()
    if not brief: 
        notes = fetch_notes(projectName, entityName)
    else: 
        notes = fetch_brief_notes(projectName, entityName)

    for note in notes: 
        entities = note.get('note_links')
        steps = []
        tasks = []
        versions = []
        for entity in entities: 
            if entity.get('type') == 'Version': 
                if not entity.get('id') in versionIdCaches.keys(): 
                    versionEntity = sg.find_one('Version', [['id', 'is', entity.get('id')]], ['sg_task', 'sg_task.Task.step', 'code'])          
                    versionIdCaches[versionEntity.get('id')] = versionEntity
                versionEntity = versionIdCaches[entity.get('id')]
                step = versionEntity.get('sg_task.Task.step')
                steps.append(step)
                task = versionEntity.get('sg_task')
                tasks.append(task)
                versions.append(versionEntity)
            if entity.get('type') == 'Task': 
                tasks.append(entity)
                if not entity.get('id') in taskCaches.keys(): 
                    taskEntity = sg.find_one('Task', [['id', 'is', entity.get('id')]], ['step', 'code'])
                    taskCaches[taskEntity['id']] = taskEntity
                taskEntity = taskCaches[entity['id']]
                step = taskEntity.get('step')
                steps.append(step)  

        userImage = get_user_image(note.get('user').get('id'))
        note['user']['path'] = userImage
        
        noteDict[note['id']] = {'note': note, 'steps': steps, 'attachments': note.get('attachments'), 'tasks': tasks, 'versions': versions}

    return noteDict


def fetch_brief_notes(projectName, entityName): 
    """ get note from selected project and entity name """ 
    project = get_project(projectName)
    filters = [['project', 'is', project], ['note_links', 'name_is', entityName], ['sg_note_type', 'is', 'Brief']]
    notes = sg.find('Note', filters, DataField.note)
    return notes 


def fetch_versions(project, entityType, entityName): 
    """ find version linked to Asset or Shot """ 
    import time 
    global versionCaches
    if not entityName in versionCaches.keys(): 
        logger.debug('Fetch version from shotgun')
        projectEntity = get_project(project)
        entity = get_entity(project, Config.sgEntity.get(entityType), entityName)
        filters = [['project', 'is', projectEntity], ['entity', 'is', entity]]
        versions = sg.find('Version', filters, DataField.version)
        versionCaches[entityName] = versions 
    return versionCaches[entityName]

def fetch_links(project, entityType, entityName): 
    projectEntity = get_project(project)
    entity = get_entity(project, Config.sgEntity.get(entityType), entityName)

    # if asset, find Shot which has links to asset 
    # if scene, find Asset which has links to shot 
    filters = [['project', 'is', projectEntity], [Config.attrMap.get(entityType), 'is', entity]]
    results = sg.find(Config.sgSearchMap.get(entityType), filters, DataField.entityMap[entityType])
    return results 


def fetch_tasks(project, entityType, entityName, step=''): 
    # key cache 
    key = '%s-%s-%s' % (project, entityName, step)
    if key in taskStepCaches.keys(): 
        tasks = taskStepCaches[key]

    else: 
        # fetch tasks
        projectEntity = get_project(project)
        entity = sg.find_one(Config.sgEntity[entityType], [['project', 'is', projectEntity], ['code', 'is', entityName]], DataField.entity)
        filters = [['project', 'is', projectEntity], ['entity', 'is', entity]]
        filters.append(['step.Step.code', 'is', step]) if step else None 
        tasks = sg.find('Task', filters, DataField.task)

        # set caches 
        taskStepCaches[key] = tasks
    return tasks 



def get_attachment(id): 
    return sg.find_one('Attachment', [['id', 'is', id]], DataField.attachment)


def download_attachment(attachmentEntity, dst): 
    return sg.download_attachment(attachmentEntity, file_path=dst)


def upload_attachments(noteId, files): 
    results = []
    for file in files: 
        path = file.replace('/', '\\')
        result = sg.upload('Note', noteId, path, 'attachments')
        results.append(result)
    return results 


def publish_daily(entity, taskEntity, files, description): 
    """ wrapper submit daily to sg """ 
    from rf_app.publish.asset import sg_hook
    print 'version ...', entity.context.publishVersion
    status = 'rev'
    user = user_info.User()
    userEntity = user.sg_user()
    try: 
        versionResult = sg_hook.publish_daily(entity, taskEntity, status, files, userEntity, description)
        taskResult = sg_hook.update_task(entity, taskEntity, status)
        return True, versionResult
    except Exception as e: 
        return False, e


def create_note(project, entityType, entityName, subject, content, step, user, noteType, status): 
    """ create notes """ 
    projectEntity = get_project(project)
    entity = sg.find_one(Config.sgEntity[entityType], [['project', 'is', projectEntity], ['code', 'is', entityName]], DataField.entity)
    tasks = sg.find('Task', [['project', 'is', projectEntity], ['entity', 'is', entity], ['step.Step.code', 'is', step]], ['content', 'id'])
    noteLinks = [entity] + tasks if tasks else [entity]
    data = {'project': projectEntity, 
            'note_links': noteLinks, 
            'subject': subject, 
            'content': content, 
            'user': user, 
            'sg_note_type': noteType, 
            'sg_status_list': status
            }
    return sg.create('Note', data)


def update_note(noteId, status, userEntity): 
    """ update note """ 
    data = {'sg_status_list': status, 'sg_modified_by': userEntity}
    return sg.update('Note', noteId, data)

def get_image(url): 
    data = urlopen(url).read()
    return data


def download_thumbnail(url, dst): 
    dirname = os.path.dirname(dst)
    if not os.path.exists(dirname): 
        os.makedirs(dirname)

    urllib.urlretrieve(url, dst)

    if os.path.exists(dst): 
        return dst 


def get_project(projectName): 
    global projectCaches
    if not projectName in projectCaches.keys(): 
        projectEntity = sg.find_one('Project', [['name', 'is', projectName]], DataField.project)
        projectCaches[projectName] = projectEntity
    return projectCaches[projectName]


def get_entity(projectName, entityType, entityName): 
    global entityCaches
    if not entityName in entityCaches.keys(): 
        projectEntity = get_project(projectName)
        filters = [['project', 'is', projectEntity], ['code', 'is', entityName]]
        entity = sg.find_one(entityType, filters, DataField.entity)
        entityCaches[entityName] = entity 
    return entityCaches[entityName]
        
