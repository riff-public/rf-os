# Copyright 2017 Autodesk, Inc. All rights reserved.
#
# Use of this software is subject to the terms of the Autodesk license
# agreement provided at the time of installation or download, or which
# otherwise accompanies this software in either electronic or hard copy form.

# -*- coding: utf-8 -*-
import os 
import sys 
from collections import OrderedDict
from maya.app.general.mayaMixin import MayaQWidgetDockableMixin
from maya import OpenMayaUI as omui
import maya.cmds as mc 
from Qt import QtCore
from Qt import QtGui 
from Qt import QtWidgets
from rf_utils import icon
from rf_utils import project_info
import main_ui
reload(main_ui)

customMixinWindow = None
uiName = 'infoPanel'
workspaceControlName = '{}WorkspaceControl'.format(uiName)
dockUi = None


class SGInfoDockWidget(MayaQWidgetDockableMixin, QtWidgets.QWidget):
    def __init__(self, entity=None, parent=None):
        super(SGInfoDockWidget, self).__init__(parent=parent)

        # center layout 
        self.layout = QtWidgets.QVBoxLayout()
        self.mainWidget = main_ui.MainUi(entity=entity)
        self.layout.addWidget(self.mainWidget)
        self.setLayout(self.layout)
        self.setWindowTitle('RF SG Pipeline')
    
 
def SGInfoDockWidgetUIScript(entity=None, restore=False):
    global customMixinWindow

    ''' When the control is restoring, the workspace control has already been created and
      all that needs to be done is restoring its UI.
    '''
    if restore == True:
        # Grab the created workspace control with the following.
        restoredControl = omui.MQtUtil.getCurrentParent()

    if customMixinWindow is None:
        # Create a custom mixin widget for the first time
        customMixinWindow = SGInfoDockWidget(entity=entity)     
        customMixinWindow.setObjectName(uiName)
      
    if restore == True:
        # Add custom mixin widget to the workspace control
        mixinPtr = omui.MQtUtil.findControl(customMixinWindow.objectName())
        omui.MQtUtil.addWidgetToMayaLayout(long(mixinPtr), long(restoredControl))
    else:
        # Create a workspace control for the mixin widget by passing all the needed parameters. See workspaceControl command documentation for all available flags.
        customMixinWindow.show(dockable=True, area='right', floating=False, height=900, width=300, uiScript='SGInfoDockWidgetUIScript(entity=entity, restore=True)')
        mc.workspaceControl(workspaceControlName, e=True, ttc=["AttributeEditor", -1], wp="preferred", mw=200, initialWidth=200, l=customMixinWindow.windowTitle())

    return customMixinWindow



def show(entity=None, restore=False):
    global dockUi
    if not restore: 
        if mc.workspaceControl(workspaceControlName, ex=True): 
            mc.deleteUI(workspaceControlName)
        dockUi = SGInfoDockWidgetUIScript(entity=entity)
    else: 
        if hasattr(dockUi, 'show'): 
            dockUi.show()
            dockUi.mainWidget.initialize_context(entity)
        else: 
            return show(entity, False)
    return dockUi

