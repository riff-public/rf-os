import os 
import sys 
import rf_config as config 

class Tasks: 
    assetTasks = ['Design','Keyvis','Model','Rig','Texture','Groom','Lookdev','Sim']
    sceneTasks = ['Layout','Anim', 'Colorscript', 'SetDress','FinalCam','Sim','TechAnim','Qc','FX','Light','Comp']