import os 
import sys 
import rf_config as config 

class Software: 
    rv = config.Software.rv

class NoteType: 
    reply = 'Reply'
    brief = 'Brief'
    fix = 'Fix'
    request = 'Request'


class Status: 
    opn = 'opn'
    ip = 'ip'
    cmpt = 'cmpt'
    clsd = 'clsd'


class StatusDisplay: 
    opn = 'Open'
    ip = 'In Progress'
    cmpt = 'Complete'
    clsd = 'Closed'


class CheckBoxFilter: 
    statuses = {StatusDisplay.opn: Status.opn, StatusDisplay.ip: Status.ip, StatusDisplay.cmpt: Status.cmpt, StatusDisplay.clsd: Status.clsd}
    types = [NoteType.reply, NoteType.brief, NoteType.fix, NoteType.request]


class Note: 
    noteType = [NoteType.reply, NoteType.brief, NoteType.fix, NoteType.request]
    statuses = [StatusDisplay.opn, StatusDisplay.cmpt, StatusDisplay.clsd]
    statusMap = {StatusDisplay.opn: Status.opn, StatusDisplay.ip: Status.ip, StatusDisplay.cmpt: Status.cmpt, StatusDisplay.clsd: Status.clsd}
    displayMap = {Status.opn: StatusDisplay.opn, Status.ip: StatusDisplay.ip, Status.cmpt: StatusDisplay.cmpt, Status.clsd: StatusDisplay.clsd}
    typeMap = {NoteType.reply: Status.clsd, NoteType.brief: Status.clsd, NoteType.fix: Status.opn, NoteType.request: Status.opn}
    menuChoices = [StatusDisplay.opn, StatusDisplay.ip, StatusDisplay.cmpt]


class Color: 
    opn = 'background-color: rgb(240, 60, 60)'
    ip = 'background-color: rgb(220, 180, 60)'
    cmpt = 'background-color: rgb(100, 140, 220)'
    clsd = 'background-color: rgb(160, 160, 160)'
    statusMap = {Status.opn: opn, Status.ip: ip, Status.cmpt: cmpt, Status.clsd: clsd}

 
class Default: 
    noteTypeDefault = NoteType.fix
    statusFilters = [Status.opn, Status.ip, Status.cmpt, Status.clsd]
    typeDefault = [NoteType.reply, NoteType.brief, NoteType.fix, NoteType.request]
    statusDefault = [StatusDisplay.opn, StatusDisplay.ip, StatusDisplay.cmpt, StatusDisplay.clsd]


def attachment_path(entity, attachmentEntity, thumbnail=False): 
    """ get attachment path """ 
    name = unicode(attachmentEntity.get('filename'), 'utf-8')
    entity.context.update(step='_data', process='sgNote')
    path = entity.path.process().abs_path()
    filename = name
    if thumbnail: 
        filename = '%s_thumbnail.jpg' % os.path.splitext(name)[0]
    dst = '%s/%s' % (path, filename)
    return dst 


def version_path(entity, versionEntity, thumbnail=False): 
    """ get attachment path """ 
    if versionEntity: 
        versionId = versionEntity.get('id')
        name = versionEntity.get('code')
        entity.context.update(step='_data', process='sgVersion')
        path = entity.path.process().abs_path()
        filename = versionEntity["sg_uploaded_movie"].get('name')
        if thumbnail: 
            filename = '%s_thumbnamil.jpg' % os.path.splitext(filename)[0]
        dst = '%s/%s' % (path, filename)
    return dst 

def link_path(entity, sgEntity, thumbnail=False): 
    """ get attachment path """ 
    name = unicode(sgEntity.get('code'), 'utf-8')
    entity.context.update(step='_data', process='sgEntity')
    path = entity.path.process().abs_path()
    filename = name
    if thumbnail: 
        filename = '%s_thumbnail.jpg' % os.path.splitext(name)[0]
    dst = '%s/%s' % (path, filename)
    return dst 


def get_ext(url):
    """Return the filename extension from url, or ''."""
    from urlparse import urlparse
    parsed = urlparse(url)
    root, ext = os.path.splitext(parsed.path)
    return ext

def set_tool_tip(widget, infoDict): 
    strInfo = str()
    lines = []

    for k, v in infoDict.iteritems(): 
        if 'image' in k or 'movie' in k or 'attachment' in k: 
            if len(str(v)) > 60: 
                v = '%s ...' % str(v)[0: 60]
        line = '%s: %s' % (k, v)
        lines.append(line)
    strInfo = '\n'.join(lines)
    widget.setToolTip(strInfo)
