import os
import sys
import traceback
from collections import OrderedDict

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


class AsmUi(QtWidgets.QWidget):
    status = QtCore.Signal(str)
    def __init__(self, parent=None) :
        super(AsmUi, self).__init__(parent)
        self.ui()
        
    def ui(self): 
        self.layout = QtWidgets.QVBoxLayout()
        self.treeAsm = QtWidgets.QTreeWidget()

        self.buttonLayout = QtWidgets.QHBoxLayout()
        # buttons 
        self.showAllButton = QtWidgets.QPushButton('Show Set Member')
        self.customButton = QtWidgets.QPushButton('Show only selection')

        self.buttonLayout.addWidget(self.showAllButton)
        self.buttonLayout.addWidget(self.customButton)

        self.layout.addWidget(self.treeAsm)
        self.layout.addLayout(self.buttonLayout)
        self.setLayout(self.layout)


class BuildWidget(QtWidgets.QWidget):
    status = QtCore.Signal(str)
    def __init__(self, parent=None) :
        super(BuildWidget, self).__init__(parent)
        self.ui()
        
    def ui(self): 
        self.layout = QtWidgets.QHBoxLayout()
        self.comboBox = QtWidgets.QComboBox()
        self.layout.addWidget(self.comboBox)
        self.setLayout(self.layout)
        self.layout.setContentsMargins(1, 1, 1, 1)


class ButtonWidget(QtWidgets.QWidget):
    status = QtCore.Signal(str)
    def __init__(self, parent=None) :
        super(ButtonWidget, self).__init__(parent)
        self.ui()
        
    def ui(self): 
        self.layout = QtWidgets.QHBoxLayout()
        self.button = QtWidgets.QPushButton()
        self.layout.addWidget(self.button)
        self.setLayout(self.layout)
        self.layout.setContentsMargins(1, 1, 1, 1)


class LookWidget(QtWidgets.QWidget):
    status = QtCore.Signal(str)
    lookChanged = QtCore.Signal(str)
    def __init__(self, parent=None) :
        super(LookWidget, self).__init__(parent)
        self.ui()
        
    def ui(self): 
        self.layout = QtWidgets.QHBoxLayout()
        self.comboBox = QtWidgets.QComboBox()
        self.layout.addWidget(self.comboBox)
        self.setLayout(self.layout)
        self.layout.setContentsMargins(1, 1, 1, 1)

    def init_signals(self): 
        self.comboBox.currentIndexChanged.connect(self.look_changed)

    def look_changed(self): 
        look = self.comboBox.currentText()
        lookChanged.emit(look)

