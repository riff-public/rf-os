# v.0.0.1 polytag switcher
_title = 'Asm Utils'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'AsmUtilsUI'

#Import python modules
import sys
import os
import getpass
from datetime import datetime
import logging
from functools import partial
from collections import OrderedDict

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
import rf_config as config

# framework modules
from rf_utils import log_utils

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import ui
reload(ui)
from rf_utils.context import context_info
import maya.cmds as mc 
from rf_app.asm import asm_lib
from rf_utils import register_entity
from rf_utils import thread_pool

class Color: 
    normal = [200, 200, 200]
    missing = [240, 100, 100]

class Icon: 
    gpu = '%s/icons/gpu_icon.png' % moduleDir
    abc = '%s/icons/alembic_icon.png' % moduleDir
    rs = '%s/icons/rs_icon.png' % moduleDir
    rig = '%s/icons/rig_icon.png' % moduleDir

class AsmTool(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(AsmTool, self).__init__(parent)

        # ui read
        self.ui = ui.AsmUi(parent)
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)

        # title
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.resize(800, 400)

        # titles 
        self.assetLabel = 'Assets'
        self.buildLabel = 'Build asset'
        self.actionLabel = 'Action'
        self.removeLabel = 'Remove'
        self.lookLabel = 'Look'

        self.headers = [self.assetLabel, self.buildLabel, self.actionLabel, self.removeLabel, self.lookLabel, '']
        self.init_functions()
        self.init_signals()


    def init_functions(self): 
        self.itemWidgetDict = dict()
        self.threadpool = QtCore.QThreadPool()
        self.threadpool.setMaxThreadCount(1)

        self.entity = context_info.ContextPathInfo()
        self.set_ui()
        

    def init_signals(self): 
        self.ui.showAllButton.clicked.connect(self.init_process)
        self.ui.customButton.clicked.connect(self.list_custom)
        self.ui.treeAsm.itemClicked.connect(self.select_item)

    def init_process(self): 
        projects = self.list_set()
        project = projects[0] if projects else os.environ.get('ACTIVE_PROJECT')
        worker = thread_pool.Worker(self.fetch_sgprocess, project)
        worker.signals.result.connect(self.fetch_finished)
        self.threadpool.start(worker)

    def fetch_sgprocess(self, project): 
        # call shotgun for look 
        from rf_utils.sg import sg_utils
        sg = sg_utils.sg 
        print 'fetch fetch_sgprocess', project
        projectFilter = ['project.Project.name', 'is', project]
        stepFilter = ['step.Step.code', 'is', 'Texture']
        filters = [projectFilter, stepFilter]
        fields = ['content', 'sg_name', 'entity']
        tasks = sg.find('Task', filters, fields)
        return tasks

    def fetch_finished(self, result): 
        self.lookDict = result
        self.update_look_ui(result)

    def list_set(self): 
        return self.list_asms('default')

    def list_custom(self): 
        cmd = asm_lib.UiCmd()
        sels, asms = cmd.list_selection()
        self.list_asms('custom', asms)

    def update_look_ui(self, lookDict): 
        print lookDict


    def set_ui(self): 
        # resize to fit content 
        header = QtWidgets.QTreeWidgetItem(self.headers)
        self.ui.treeAsm.setHeaderItem(header)
        self.ui.treeAsm.setColumnWidth(self.headers.index(self.assetLabel), 220)
        self.ui.treeAsm.setColumnWidth(self.headers.index(self.buildLabel), 260)
        self.ui.treeAsm.setColumnWidth(self.headers.index(self.actionLabel), 100)
        self.ui.treeAsm.setColumnWidth(self.headers.index(self.removeLabel), 100)
        self.ui.treeAsm.setColumnWidth(self.headers.index(self.lookLabel), 100)

    def list_asms(self, mode='default', selections=None): 
        # expect Set_Grp and ShotDress_Grp 
        grps = list_asm_grp(self.entity)
        setGrp = self.entity.projectInfo.asset.set_grp()
        shotdressGrp = self.entity.projectInfo.asset.shotdress_grp()

        self.ui.treeAsm.clear()

        # root = self.ui.treeAsm.invisibleRootItem()
        root = self.ui.treeAsm.invisibleRootItem()
        self.ui.treeAsm.addTopLevelItem(root)
        projects = []

        # rootItem.setText(1, 'Test')
        # root2 = self.add_tree_item(root, ['ShotDress_Grp'])
        if mode == 'default': 
            if mc.objExists(setGrp): 
                rootItem = self.add_tree_item(root, [setGrp])
                asmRoots = asm_lib.find_set_root(setGrp)
                self.ui.treeAsm.expandItem(rootItem)

                for i, rootAsm in enumerate(asmRoots):
                    loc = asm_lib.MayaRepresentation(rootAsm)
                    asmPath = loc.asset_description()
                    rootAsset = context_info.ContextPathInfo(path=asmPath)
                    projects.append(rootAsset.project)
                    asmItem = self.add_tree_item(rootItem, [loc.shortname])
                    asmItem.setData(0, QtCore.Qt.UserRole, loc)
                    self.ui.treeAsm.expandItem(asmItem)

                    asmLoc = asm_lib.Asm(rootAsm)
                    childs = asmLoc.list_asm()
                    assetDict = self.group_assets(childs)

                    self.set_asset_items(asmItem, assetDict)

                # shotdress 
                shotDressItem = self.add_tree_item(root, [shotdressGrp])
                self.ui.treeAsm.expandItem(shotDressItem)

                shotdress = asm_lib.Asm(shotdressGrp)
                shotdressChilds = shotdress.list_asm()
                shotdressDict = self.group_assets(shotdressChilds)
                self.set_asset_items(shotDressItem, shotdressDict)
            else: 
                rootItem = self.add_tree_item(root, ['No Set_Grp'])

        # custom 
        if mode == 'custom': 
            if selections: 
                customItem = self.add_tree_item(root, ['Custom Selection'])
                self.ui.treeAsm.expandItem(customItem)
                customSelectionDict = self.group_assets(selections)
                self.set_asset_items(customItem, customSelectionDict)

        return list(set(projects))
    
    def set_asset_items(self, parentItem, assetDict): 
        assetRow = 0 
            
        for assetPath, childs in assetDict.iteritems(): 
            childAsms = [asm_lib.MayaRepresentation(a) for a in childs]
            asset = context_info.ContextPathInfo(path=assetPath)
            assetItem = self.add_tree_item(parentItem, [asset.name])
            assetItem.setData(0, QtCore.Qt.UserRole, [childAsms, asset])
            
            # add widgets
            # comboBox items 
            optionWidget = ui.BuildWidget(self.ui.treeAsm)
            self.ui.treeAsm.setItemWidget(assetItem, self.headers.index(self.buildLabel), optionWidget)
            self.add_file_list(optionWidget.comboBox, assetPath)
            assetItem.setSizeHint(self.headers.index(self.buildLabel), optionWidget.sizeHint())
            
            # build button 
            buildWidget = ui.ButtonWidget(self.ui.treeAsm)
            buildWidget.button.setText('Build All')
            buildWidget.button.clicked.connect(partial(self.build, assetItem, optionWidget))
            self.ui.treeAsm.setItemWidget(assetItem, self.headers.index(self.actionLabel), buildWidget)

            # remove button
            removeWidget = ui.ButtonWidget(self.ui.treeAsm)
            removeWidget.button.setText('Remove All')
            removeWidget.button.clicked.connect(partial(self.remove, assetItem, optionWidget))
            self.ui.treeAsm.setItemWidget(assetItem, self.headers.index(self.removeLabel), removeWidget)


            for row, childAsm in enumerate(childAsms): 
                childItem = self.add_tree_item(assetItem, [childAsm.shortname])
                childItem.setData(0, QtCore.Qt.UserRole, [[childAsm], asset])

                # build button 
                buildSubWidget = ui.ButtonWidget(self.ui.treeAsm)
                buildSubWidget.button.setText('Build')
                buildSubWidget.button.clicked.connect(partial(self.build, childItem, optionWidget))
                self.ui.treeAsm.setItemWidget(childItem, self.headers.index(self.actionLabel), buildSubWidget)

                # remove button
                removeSubWidget = ui.ButtonWidget(self.ui.treeAsm)
                removeSubWidget.button.setText('Remove')
                removeSubWidget.button.clicked.connect(partial(self.remove, childItem, optionWidget))
                self.ui.treeAsm.setItemWidget(childItem, self.headers.index(self.removeLabel), removeSubWidget)

                # look comboBox 
                # lookWidget = ui.LookWidget(self.ui.treeAsm)
                # lookWidget.lookChanged.connect(partial(self.change_look, childItem, optionWidget))
                # self.ui.treeAsm.setItemWidget(childItem, self.headers.index(self.lookLabel), lookWidget)

            assetRow += 1


    def add_tree_item(self, root, contents=[]): 
        return QtWidgets.QTreeWidgetItem(root, contents)

    def select_item(self, item, column): 
        data = item.data(0, QtCore.Qt.UserRole)
        childs = data[0]
        mc.select(childs)

    def build(self, item, optionWidget): 
        itemData = item.data(0, QtCore.Qt.UserRole)
        asms = itemData[0]
        asset = itemData[1]

        data = optionWidget.comboBox.itemData(optionWidget.comboBox.currentIndex(), QtCore.Qt.UserRole)
        switchFile = data[0]
        buildType = data[1]
        custom_switch(asms, switchFile, buildType)

    def remove(self, item, optionWidget): 
        itemData = item.data(0, QtCore.Qt.UserRole)
        asms = itemData[0]
        asset = itemData[1]

        locs = [a.longname for a in asms]
        mc.select(locs)
        cmd = asm_lib.UiCmd()
        cmd.remove()

    def change_look(self, item, optionWidget): 
        itemData = item.data(0, QtCore.Qt.UserRole)
        asms = itemData[0]
        asset = itemData[1]
        print asset


    def group_assets(self, childs): 
        # group childs to asset 
        assetDict = OrderedDict()
        for child in childs: 
            childAsm = asm_lib.MayaRepresentation(child)
            path = childAsm.asset_description()
            if not path in assetDict.keys(): 
                assetDict[path] = [child]
            else: 
                assetDict[path].append(child)

        return assetDict

    def add_file_list(self, comboBox, descritionPath): 
        files = list_hero_files(descritionPath)
        proxyfiles = list_rsproxy_files(descritionPath)
        rigfiles = list_rig_files(descritionPath)

        gpuIcon = QtGui.QIcon()
        gpuIcon.addPixmap(QtGui.QPixmap(Icon.gpu), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        abcIcon = QtGui.QIcon()
        abcIcon.addPixmap(QtGui.QPixmap(Icon.abc), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        rsIcon = QtGui.QIcon()
        rsIcon.addPixmap(QtGui.QPixmap(Icon.rs), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        rigIcon = QtGui.QIcon()
        rigIcon.addPixmap(QtGui.QPixmap(Icon.rs), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        

        row = 0 
        for file in files: 
            color = Color.normal if os.path.exists(file) else Color.missing
            display = '%s' % (os.path.basename(file))
            comboBox.addItem(display)
            comboBox.setItemData(row, [file, 'gpu'], QtCore.Qt.UserRole)
            comboBox.setItemIcon(row, gpuIcon)
            comboBox.setItemData(row, QtGui.QColor(color[0], color[1], color[2]), QtCore.Qt.TextColorRole)
            row += 1

        for file in files: 
            color = Color.normal if os.path.exists(file) else Color.missing
            display = '%s' % (os.path.basename(file))
            comboBox.addItem(display)
            comboBox.setItemData(row, [file, 'cache'], QtCore.Qt.UserRole)
            comboBox.setItemIcon(row, abcIcon)
            comboBox.setItemData(row, QtGui.QColor(color[0], color[1], color[2]), QtCore.Qt.TextColorRole)
            row += 1

        for file in proxyfiles: 
            color = Color.normal if os.path.exists(file) else Color.missing
            display = '%s' % (os.path.basename(file))
            comboBox.addItem(display)
            comboBox.setItemData(row, [file, 'rsproxy'], QtCore.Qt.UserRole)
            comboBox.setItemIcon(row, rsIcon)
            comboBox.setItemData(row, QtGui.QColor(color[0], color[1], color[2]), QtCore.Qt.TextColorRole)
            row += 1

        for file in rigfiles: 
            color = Color.normal if os.path.exists(file) else Color.missing
            display = '%s' % (os.path.basename(file))
            comboBox.addItem(display)
            comboBox.setItemData(row, [file, 'rig'], QtCore.Qt.UserRole)
            comboBox.setItemIcon(row, rsIcon)
            comboBox.setItemData(row, QtGui.QColor(color[0], color[1], color[2]), QtCore.Qt.TextColorRole)
            row += 1



        

    def find_switch_assets(self, asset, descritionPath): 
        files = list_hero_files(descritionPath)
        proxyfiles = list_rsproxy_files(descritionPath)




def show(): 
    from rftool.utils.ui import maya_win
    logger.info('Run in Maya\n')
    maya_win.deleteUI(uiName)
    myApp = AsmTool(maya_win.getMayaWindow())
    myApp.show()
    return myApp

def list_asm_grp(entity): 
    setGrp = entity.projectInfo.asset.set_grp()
    shotDressGrp = entity.projectInfo.asset.shotdress_grp()
    grps = [a for a in [setGrp, shotDressGrp] if mc.objExists(a)]
    return grps 


def custom_switch(asms, switchFile, buildType, *args): 
    locs = [a.longname for a in asms]
    mc.select(locs)

    cmd = asm_lib.UiCmd()
    if buildType == 'cache': 
        cmd.switch_cache(overrideFile=switchFile)
    if buildType == 'gpu': 
        cmd.switch_gpu(overrideFile=switchFile)
    if buildType == 'rsproxy': 
        cmd.switch_rsproxy(overrideFile=switchFile)

def list_hero_files(descritionPath):
    caches = []
    reg = register_entity.RegisterInfo(descritionPath)
    if descritionPath: 
        for res in ['md', 'pr']: 
            cacheFiles = reg.list.caches(res=res, hero=True)
            cacheFiles += reg.list.gpus(res=res, hero=True)
            for file in cacheFiles: 
                if not file in caches: 
                    caches.append(file)

    return caches

def list_rsproxy_files(descritionPath): 
    files = []
    reg = register_entity.RegisterInfo(descritionPath)
    if descritionPath: 
        for res in ['md', 'pr']: 
            rsproxys = reg.list.files(key='rsproxy', res=res, hero=True) 
            for file in rsproxys: 
                if not file in files: 
                    files.append(file)

    return files


def list_rig_files(descritionPath): 
    files = []
    reg = register_entity.RegisterInfo(descritionPath)
    if descritionPath: 
        for res in ['md', 'pr']: 
            rigs = reg.list.files(key='rig', res=res, hero=True) 
            for file in rigs: 
                if not file in files: 
                    files.append(file)

    return files

