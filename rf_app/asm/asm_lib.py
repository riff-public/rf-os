uiName = 'AsmUtils'
_title = 'Asm Utilities'
_version = 'v.0.0.1'
_description = 'wip'

import os
import sys
import time
from datetime import datetime
from collections import OrderedDict
from functools import partial
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import asm_maya
reload(asm_maya)
from rf_utils.context import context_info
from rf_utils import register_entity
import maya.cmds as mc
from rf_utils import file_utils
from rf_utils import custom_exception
reload(file_utils)
reload(register_entity)

sys.setrecursionlimit(1500)

def loc_representation(name):
    count = asm_maya.name_count(name)
    locName = representation_name(name, count+1)
    locName = asm_maya.create_locator(locName)
    return locName


def representation_name(name, number):
    """ get rep name """
    return '%s_%03d' % (name, number)


class ConfigAttr:
    asmLocator = 'asmLocator'
    root = 'root'
    hidden = 'hidden'
    variant = 'variant'
    cache = 'cache'
    setAsm = 'setAsm'
    description = 'description'
    look = 'look'
    geo = 'geo'
    childPos = 'childPos'
    overrideData = 'overrideData'
    asset = 'asset'
    position = 'position'
    abcOffset = 'abc_offset'
    abcSpeed = 'abc_speed'
    display = 'display'
    setdate = 'setdate'
    overridedate = 'overridedate'
    originalVariant = 'originalVariant'


class DataAttr:
    root = 'root'
    setAsm = 'setAsm'
    shortName = 'shortName'
    nodeType = 'nodeType'
    assetDescription = 'assetDescription'
    overrideData = 'overrideData'
    cache = 'cache'
    parent = 'parent'
    shape = 'shape'
    topRootLn = 'topRootLn'
    topRoot = 'topRoot'
    position = 'position'
    local = 'local'
    offset = 'offset'
    hidden = 'hidden'
    look = 'look'
    geo = 'geo'
    childPos = 'childPos'

def add_description(locName, root=False, asmPath='', adPath='', look='main', geo='main', abcOffset=0, abcSpeed=1, display='', variant=False):
    asm_maya.add_attr(locName, ConfigAttr.asmLocator, True, asm_maya.Attr.checkBox)
    asm_maya.add_attr(locName, ConfigAttr.root, root, asm_maya.Attr.checkBox)
    asm_maya.add_attr(locName, ConfigAttr.variant, variant, asm_maya.Attr.checkBox)
    asm_maya.add_attr(locName, ConfigAttr.hidden, root, asm_maya.Attr.checkBox)
    asm_maya.add_attr(locName, ConfigAttr.cache, root, asm_maya.Attr.checkBox)
    asm_maya.add_attr(locName, ConfigAttr.setAsm, asmPath, asm_maya.Attr.string)
    asm_maya.add_attr(locName, ConfigAttr.description, adPath, asm_maya.Attr.string)
    asm_maya.add_attr(locName, ConfigAttr.look, look, asm_maya.Attr.string)
    asm_maya.add_attr(locName, ConfigAttr.geo, geo, asm_maya.Attr.string)
    asm_maya.add_attr(locName, ConfigAttr.asset, '', asm_maya.Attr.message)
    asm_maya.add_attr(locName, ConfigAttr.abcOffset, abcOffset, asm_maya.Attr.double)
    asm_maya.add_attr(locName, ConfigAttr.abcSpeed, abcSpeed, asm_maya.Attr.double)
    asm_maya.add_attr(locName, ConfigAttr.display, display, asm_maya.Attr.string)


class Config:
    nodeName = 'asm'
    transform = 'transform'
    setdress = 'setdress'
    shot = 'shot'

class Representation(object):
    """docstring for Representation"""
    def __init__(self, name='', create=False):
        super(Representation, self).__init__()
        self.name = self.create(name) if create else name
        self.preferName = name
        self.nameClash = True if not name == self.name else False
        self.project = ''
        self.data = None
        self.cacheRegInfo = dict()

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name

    def representation_format(self, name, number):
        return '%s_%03d' % (name, number)

    def create(self, name, asLocator=True):
        name = asm_maya.create_locator(name) if asLocator else asm_maya.create_group(name)
        add_description(name)
        return name

    def set(self, gpuPath, entity=None):
        """ create loc """
        if not entity:
            entity = context_info.ContextPathInfo(path=gpuPath)
            entity.context.update(look='main', res='md')

        # init data
        self.entity = entity
        self.gpuPath = gpuPath
        self.regInfo = register_entity.Register(entity)
        add_description(self.name, asmPath='', adPath=self.regInfo.hero_config_path(), look=self.entity.look)
        return self.name

    def set_values(self, dictValue):
        adPath = dictValue[DataAttr.assetDescription]
        look = dictValue[DataAttr.look]
        isRoot = dictValue[DataAttr.root]
        setAsm = dictValue[DataAttr.setAsm]
        add_description(self.name, root=isRoot, asmPath=setAsm, adPath=adPath, look=look)

    def set_value(self, key, value): 
        validKeys = [ConfigAttr.setAsm, ConfigAttr.description, ConfigAttr.look, ConfigAttr.geo]
        if key in validKeys: 
            asm_maya.add_attr(self.name, key, value, asm_maya.Attr.string)
        else: 
            logger.warning('"%s" is not a valid key' % key)

    def set_variant_value(self, gpuPath, entity=None): 
        """ create loc """
        if not entity:
            entity = context_info.ContextPathInfo(path=gpuPath)
            entity.context.update(look='main', res='md')

        # init data
        regInfo = register_entity.Register(entity)
        adPath = regInfo.hero_config_path()
        originalDescription = self.asset_description()

        # add new variant ad path 
        asm_maya.add_attr(self.name, ConfigAttr.description, adPath, asm_maya.Attr.string)
        if not self.is_variant(): 
            self.set_original_variant(originalDescription)
            self.set_variant(True)

    def restore_variant(self): 
        adPath = self.get_original_variant()
        asm_maya.add_attr(self.name, ConfigAttr.description, adPath, asm_maya.Attr.string)
        asm_maya.add_attr(self.name, ConfigAttr.originalVariant, '', asm_maya.Attr.string)
        self.set_variant(False)

    def asm_name(self, name):
        count = asm_maya.name_count(name)
        name = self.representation_format(name, count + 1)
        return name

    def is_root(self):
        return asm_maya.get_attr(self.name, ConfigAttr.root)

    def is_hidden(self):
        return asm_maya.get_attr(self.name, ConfigAttr.hidden)

    def is_variant(self): 
        return asm_maya.get_attr(self.name, ConfigAttr.variant)

    def is_cache(self):
        return asm_maya.get_attr(self.name, ConfigAttr.cache)

    def get_set_date(self): 
        return asm_maya.get_attr(self.name, ConfigAttr.setdate)

    def get_override_date(self): 
        return asm_maya.get_attr(self.name, ConfigAttr.overridedate)

    def get_original_variant(self): 
        return asm_maya.get_attr(self.name, ConfigAttr.originalVariant)

    @property
    def asm_data(self):
        return asm_maya.get_attr(self.name, ConfigAttr.setAsm)

    def asset_description(self):
        return asm_maya.get_attr(self.name, ConfigAttr.description)

    def override_data(self):
        return asm_maya.get_attr(self.name, ConfigAttr.overrideData)

    @property
    def get_data(self):
        self.data = file_utils.ymlLoader(self.asset_description()) if not self.data else self.data
        return self.data

    @property
    def look(self):
        return asm_maya.get_attr(self.name, ConfigAttr.look)

    def is_asm(self):
        name = self.name
        return asm_maya.get_attr(name, ConfigAttr.asmLocator)

    def set_description(self, path):
        asm_maya.add_attr(self.name, ConfigAttr.description, path, asm_maya.Attr.string)

    def set_asm_description(self, path):
        asm_maya.add_attr(self.name, ConfigAttr.setAsm, path, asm_maya.Attr.string)

    def set_override(self, path):
        asm_maya.add_attr(self.name, ConfigAttr.overrideData, path, asm_maya.Attr.string)

    def set_root(self, state):
        asm_maya.add_attr(self.name, ConfigAttr.root, state, type=asm_maya.Attr.checkBox)

    def set_cache(self, state):
        asm_maya.set_attr(self.name, DataAttr.cache, state)

    def set_variant(self, state): 
        asm_maya.set_attr(self.name, ConfigAttr.variant, state)

    def set_asm(self, state):
        asm_maya.set_attr(self.name, ConfigAttr.asmLocator, state)

    def set_look(self, look):
        asm_maya.add_attr(self.name, ConfigAttr.look, look, asm_maya.Attr.string)

    def set_date_modify(self, date): 
        asm_maya.add_attr(self.name, ConfigAttr.setdate, date, asm_maya.Attr.string)

    def override_date_modify(self, date): 
        asm_maya.add_attr(self.name, ConfigAttr.overridedate, date, asm_maya.Attr.string)

    def set_geo(self, geo):
        asm_maya.add_attr(self.name, ConfigAttr.geo, geo, asm_maya.Attr.string)

    def set_original_variant(self, path):
        asm_maya.add_attr(self.name, ConfigAttr.originalVariant, path, asm_maya.Attr.string)

    def get_value(self, key): 
        return asm_maya.get_attr(self.name, key)

    @property
    def asmlocator_attr(self): 
        return '%s.%s' % (self.name, ConfigAttr.asmLocator)

    @property 
    def root_attr(self): 
        return '%s.%s' % (self.name, ConfigAttr.root)

    @property
    def hidden_attr(self): 
        return '%s.%s' % (self.name, ConfigAttr.hidden)

    @property 
    def cache_attr(self): 
        return '%s.%s' % (self.name, ConfigAttr.cache)

    @property
    def variant_attr(self): 
        return '%s.%s' % (self.name, ConfigAttr.variant)

    @property 
    def setasm_attr(self): 
        return '%s.%s' % (self.name, ConfigAttr.setAsm)

    @property 
    def description_attr(self): 
        return '%s.%s' % (self.name, ConfigAttr.description)

    @property 
    def look_attr(self): 
        return '%s.%s' % (self.name, ConfigAttr.look)

    @property 
    def geo_attr(self): 
        return '%s.%s' % (self.name, ConfigAttr.geo)

    @property 
    def setdate_attr(self): 
        return '%s.%s' % (self.name, ConfigAttr.setdate)

    @property
    def overridedate_attr(self):
        return '%s.%s' % (self.name, ConfigAttr.overridedate)

    @property 
    def asset_attr(self): 
        return '%s.%s' % (self.name, ConfigAttr.asset)

    @property 
    def abc_offset_attr(self): 
        return '%s.%s' % (self.name, ConfigAttr.abcOffset)

    @property 
    def abc_speed_attr(self): 
        return '%s.%s' % (self.name, ConfigAttr.abcSpeed)

    @property 
    def display_attr(self): 
        return '%s.%s' % (self.name, ConfigAttr.display)


class MayaRepresentation(Representation):
    """docstring for MayaRepresentation"""
    def __init__(self, name='', create=False):
        super(MayaRepresentation, self).__init__(name=name, create=create)
        import maya.cmds as mc
        self.mc = mc
        self.zeroMatrix = [1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0]
        self.rootCtrl = 'AllMover_Ctrl'
        self.switchRigGrp = 'Switch_Grp'

    def is_rep(self):
        """ check if this is locator """
        if self.is_asm():
            shapes = self.mc.listRelatives(self.name, s=True, f=True)
            isRep = self.mc.objectType(shapes[0], isType=asm_maya.Asm.repType) if shapes else False
            return isRep

    def is_group(self):
        shapes = mc.listRelatives(self.name, s=True)
        return True if not shapes else False

    def position(self, world=True):
        return self.mc.xform(self.name, q=True, ws=world, m=True)

    @property
    def child(self):
        """ return transform child """
        # expect to have only 1 child under transform
        childs = self.mc.listRelatives(self.name, c=True, type='transform', f=True)
        # child must not be asm locator
        childs = [a for a in childs if not MayaRepresentation(a).is_asm() and mc.objectType(a) == 'transform'] if childs else []
        return ChildNode(childs[0]) if childs else ''

    @property
    def gpu_path(self):
        if self.child: 
            if mc.listRelatives(self.child, s=True, f=True): 
                return mc.getAttr('%s.cacheFileName' % mc.listRelatives(self.child, s=True, f=True)[0])

    @property
    def ctrl(self):
        """ find AllMover_Ctrl """
        # disable this because rig doesn't parent to asm locator but use message to link instead
        # nodes = self.mc.ls(self.name, dag=True, l=True)
        nodes = self.mc.ls(self.connected_asset(), dag=True, l=True)
        nodes = [a for a in nodes if self.mc.objectType(a, isType='transform')]
        rootCtrl = [a for a in nodes if self.rootCtrl in a.split('|')[-1]]
        return rootCtrl[0] if rootCtrl else ''

    def ctrl_pos(self, world=True):
        return self.mc.xform(self.ctrl, q=True, ws=world, m=True) if self.ctrl else None


    @property
    def child_offset(self):
        """ return xform world space position if child local xform not == 0, 0, 0 """
        if self.ctrl:
            localPos = self.mc.xform(self.ctrl, q=True, m=True)
            worldPos = self.mc.xform(self.ctrl, q=True, ws=True, m=True)
            return worldPos if not localPos == self.zeroMatrix else self.zeroMatrix

        if self.child:
            localPos = self.mc.xform(self.child, q=True, m=True)
            worldPos = self.mc.xform(self.child, q=True, ws=True, m=True)
            return worldPos if not localPos == self.zeroMatrix else self.zeroMatrix
            return self.zeroMatrix

    @property
    def shape(self):
        shape = self.mc.listRelatives(self.name, s=True, f=True)
        return shape[0] if shape else ''

    @property
    def parent(self):
        parents = self.mc.listRelatives(self.name, p=True, f=True)
        return parents[0] if parents else ''

    @property
    def shortname(self):
        name = self.mc.ls(self.name)
        return name[0].split('|')[-1] if name else ''
        # return name[0] if name else ''

    @property
    def longname(self):
        name = self.mc.ls(self.name, l=True)
        return name[0] if name else ''

    @property
    def visibility(self):
        return self.mc.getAttr('%s.visibility' % self.child)

    # def is_hidden(self):
    #     return not self.visibility

    def is_free(self):
        """ check if locator is already built member """
        if self.is_rep():
            return False if self.child else True
        return False

    def is_lock(self):
        return self.mc.lockNode(self.name, q=True, l=True)

    def is_pos_lock(self):
        return asm_maya.is_lock(self.name)

    def set_cache(self, state):
        isLock = self.is_lock()
        isPosLock = self.is_pos_lock()
        self.lock_node(False) if isLock else None
        self.lock_pos(False) if isPosLock else None
        asm_maya.set_attr(self.name, DataAttr.cache, state)
        self.lock_pos(True) if isPosLock else None
        self.lock_node(True) if isLock else None


    def set_message(self, obj): 
        self.lock_node(False)
        asm_maya.lock_transform([self.name], False)
        asm_maya.set_message(self.name, ConfigAttr.asset, obj)

    @property
    def node_type(self):
        if self.is_asm():
            return Config.nodeName
        return self.mc.objectType(self.name)

    @property
    def node_order(self):
        return asm_maya.outliner_order(self.name)


    @property
    def shape_type(self):
        return self.mc.objectType(self.shape)

    def set_hidden(self, state, lock=True):
        isLock = self.is_lock()
        isPosLock = self.is_pos_lock()
        self.lock_node(False) if isLock else None
        self.lock_pos(False) if isPosLock else None
        asm_maya.set_attr(self.name, DataAttr.hidden, state)
        if lock:
            self.lock_pos(True) if isPosLock else None
            self.lock_node(True) if isLock else None

    def set_parent(self, parent, resolveNameClash=False):
        if self.mc.objExists(parent):
            currentParents = self.mc.listRelatives(self.name, p=True, f=True)
            currentParent = currentParents[0] if currentParents else ''
            if not parent in currentParent :
                self.lock_node(False)
                self.name = self.mc.parent(self.name, parent)[0]
            if resolveNameClash and self.nameClash:
                self.lock_node(False)
                self.name = self.mc.rename(self.name, self.preferName)


    def snap_ctrl(self): 
        rig = self.connected_asset()
        if rig: 
            namespace = self.mc.referenceQuery(rig[0], ns=True)
            rootCtrl = '%s:%s' % (namespace, self.rootCtrl)
            # self.mc.delete(self.mc.parentConstraint(self.name, rootCtrl))
            # scale 
            # scaleAttr = 'scaleY'
            # scaleY = self.mc.getAttr('%s.%s' % (self.name, scaleAttr))
            # self.mc.setAttr('%s.%s' % (rootCtrl, scaleAttr), scaleY)

            # change the logic here, some asset use all 3 scale axis.
            asm_maya.snap(rootCtrl, self.name)
        else: 
            logger.warning('No message connected')

    def connected_asset(self): 
        return asm_maya.get_message(self.name, ConfigAttr.asset)

    def connected_message(self): 
        return asm_maya.get_message(self.name, 'message')

    def set_order(self, order):
        self.mc.reorder(self.name, f=True)
        self.mc.reorder(self.name, r=order)

    def set_pos(self, pos):
        self.mc.xform(self.name, ws=True, m=pos)

    def reset_pos(self): 
        self.mc.xform(self.name, ws=True, m=self.zeroMatrix)

    def set_child_pos(self, pos, ws=True):
        pos = self.zeroMatrix if not pos else pos
        if self.ctrl:
            self.set_ctrl_pos(pos, ws=ws)

        elif self.child:
            keys = self.mc.keyframe(self.child, q=True, n=True)
            self.mc.delete(keys) if keys else None
            self.mc.xform(self.child, ws=ws, m=pos)

    def set_ctrl_pos(self, pos, ws=True):
        self.mc.xform(self.ctrl, ws=ws, m=pos)

    def set_vis(self, state):
        """ set vis of child transform """
        if self.child:
            attr = '%s.visibility' % (self.child)
            isLock = self.mc.getAttr(attr, l=True)
            self.mc.setAttr(attr, l=False) if isLock else None
            self.mc.setAttr(attr, state) if self.mc.objExists(attr) else None
            self.mc.setAttr(attr, l=True) if isLock else None
        self.set_hidden(not state)

    def reset_local_pos(self):
        self.mc.xform(self.name, m=self.zeroMatrix)

    def rename(self, name):
        self.name = self.mc.rename(self.name, name)

    def remove(self):
        self.lock_node(False)
        if self.child:
            self.mc.lockNode(self.child, l=False)
        self.mc.delete(self.name)

    def remove_child(self, ref=True):
        if self.child:
            self.mc.lockNode(self.child, l=False)
            asm_maya.remove(self.child, ref=ref)
        if self.connected_asset(): 
            asm_maya.remove(self.connected_asset()[0], ref=ref)

    def lock_node(self, state):
        if self.mc.objExists(self.name): 
            self.mc.lockNode(self.name, l=state) if not self.mc.referenceQuery(self.name, inr=True) else None
            self.mc.lockNode(self.child, l=state) if self.child else None

    def lock_pos(self, state):
        asm_maya.lock_transform([self.name], state)

    def select(self):
        self.mc.select(self.name)

    def linked_cache(self): 
        data = alembic_data()
        abcNode = data.get(self.longname)

        if abcNode: 
            abcSpeed = '%s.%s' % (abcNode, 'speed')
            abcOffset = '%s.%s' % (abcNode, 'offset')
            abcCycle = '%s.%s' % (abcNode, 'cycleType')
            
            if not self.mc.objExists(self.abc_speed_attr): 
                asm_maya.add_attr(self.name, ConfigAttr.abcSpeed, 0, asm_maya.Attr.double)
            if not self.mc.objExists(self.abc_offset_attr): 
                asm_maya.add_attr(self.name, ConfigAttr.abcOffset, 0, asm_maya.Attr.double)

            asm_maya.connect_attr(self.abc_speed_attr, abcSpeed)
            asm_maya.connect_attr(self.abc_offset_attr, abcOffset)
            self.mc.setAttr(abcCycle, 1)


    def build(self, buildType, res='md', mode='', overrideFile='', proxyMode=0, instance=True):
        """ build representation """
        # set dress mode will lock Gpu and release loc
        # shot mode will lock loc and release Gpu
        if self.is_free() and not self.is_hidden() :
            hidden = self.is_hidden()
            regFile = self.asset_description()
            reg = self.register_info(regFile)
            self.lock_pos(True) if mode == Config.shot else None
            process = self.get_value(ConfigAttr.geo) or 'main'

            if buildType == 'gpu':
                # gpuInfo = reg.get.gpu(process='main', res=res)
                gpuInfo = guess_process_file(reg, process=process, res=res, filetype='gpu')

                if gpuInfo:
                    gpuFile = gpuInfo[register_entity.Config.heroFile]
                    if overrideFile: 
                        gpuFile = overrideFile
                        geo = get_geo(overrideFile)
                        self.set_geo(geo)

                    gpu = build_gpu(gpuFile, self.name, self.shortname, hidden=hidden, mode='')

                #     gpu = asm_maya.create_gpu(self.shortname, gpuFile)
                #     if gpu:
                #         gpuObject = MayaRepresentation(gpu)
                #         gpuObject.set_parent(self.name)
                #         gpuObject.reset_local_pos()

                #         gpuObject.lock_pos(True) if mode == Config.setdress else None
                #         gpuObject.lock_node(True) if mode == Config.shot else None
                #         gpuObject.set_vis(not hidden)
                #         self.set_cache(False)
                #         return True
                    if not gpu:
                        logger.warning('Cannot built. Gpu path not exists %s' % gpuFile)

                        # try again
                        if res == 'md':
                            build(buildType, res='pr')
                            return True

                else:
                    logger.debug('Cannot build. No Gpu or asset no published. Missing %s' % regFile)
                    # try again
                    if res == 'md':
                        logger.debug('"md" not register, try to build "proxy"')
                        self.build(buildType, res='pr', mode=mode)
                        return True

            if buildType == 'rig':
                rigInfo = reg.get.rig(process='main', res=res)
                currentRig = self.connected_asset()

                if not currentRig: 
                    if rigInfo:
                        entity = context_info.ContextPathInfo(path=regFile)
                        entity.context.update(res=res, look=self.look, step='rig')
                        rigFile = rigInfo[register_entity.Config.heroFile]
                        if overrideFile: 
                            rigFile = overrideFile
                            geo = get_geo(overrideFile)
                            self.set_geo(geo)

                        rig = asm_maya.create_reference_rig(entity, self.shortname, rigFile, True, self.switchRigGrp)
                        if rig:
                            rigObject = MayaRepresentation(rig[0])
                            # rigObject.set_parent(self.name)
                            # rigObject.reset_local_pos()
                            self.set_message(rig[0])
                            self.set_cache(True)
                            self.snap_ctrl()
                            rigObject.set_message(self.name)
                            return True
                else: 
                    logger.warning('%s already connected' % currentRig)

            if buildType == 'cache': 
                # cacheInfo = reg.get.cache(process='main', res=res)
                cacheInfo = guess_process_file(reg, process=process, res=res, filetype='cache')

                entity = context_info.ContextPathInfo(path=regFile)
                entity.context.update(res=res, look=self.look, step='model')
                cacheFile = None 
                if cacheInfo:
                    cacheFile = cacheInfo[register_entity.Config.heroFile]
                if overrideFile: 
                    cacheFile = overrideFile
                    geo = get_geo(overrideFile)
                    self.set_geo(geo)

                if cacheFile: 
                    cache = build_cache(entity, cacheFile, self.name, self.shortname, material=True)
                    self.linked_cache()

                    # cache = asm_maya.create_reference_rig(entity, self.shortname, rigFile, True)
                    # if cache:
                    #     cacheObject = MayaRepresentation(cache[0])
                    #     cacheObject.set_parent(self.name)
                    #     cacheObject.reset_local_pos()
                    #     self.set_cache(True)



            if buildType == 'rsproxy':
                # proxyInfo = reg.get.rsproxy(look=self.look, res=res)
                proxyInfo = guess_process_file(reg, process=process, res=res, filetype='rsproxy')
                gpuInfo = guess_process_file(reg, process=process, res=res, filetype='gpu')
                entity = context_info.ContextPathInfo(path=regFile)
                entity.context.update(res=res, look=self.look, step='lookdev')

                if gpuInfo:
                    gpuPreviewPath = gpuInfo[register_entity.Config.heroFile]
                assetName = reg.get.asset_name()

                if proxyInfo:
                    proxyFile = proxyInfo[register_entity.Config.heroFile]
                    if overrideFile: 
                        proxyFile = overrideFile
                    if instance: 
                        renderProxy = asm_maya.create_proxy_instance(self.shortname, assetName, proxyFile, displayMode=proxyMode, gpuPreviewPath=gpuPreviewPath)
                    else: 
                        renderProxy = asm_maya.create_render_proxy(self.shortname, proxyFile, displayMode=proxyMode, gpuPreviewPath=gpuPreviewPath)

                    if renderProxy:
                        rsNode, shape, placeHolder = renderProxy
                        renderProxyObject = MayaRepresentation(placeHolder)
                        renderProxyObject.set_parent(self.name)
                        renderProxyObject.reset_local_pos()
                        asm_maya.override_instance_material(rsNode, entity)
                        return True

    def register_info(self, regFile):
        asset = context_info.ContextPathInfo(path=regFile)
        if regFile in self.cacheRegInfo.keys():
            return self.cacheRegInfo[regFile]

        # use Register to read unpublished data file
        reg = register_entity.Register(asset)
        self.cacheRegInfo[regFile] = reg
        return reg


class Asm(object):
    """docstring for Asm"""
    def __init__(self, root, path='', adPath=''):
        super(Asm, self).__init__()
        self.root = root
        self.adPath = adPath
        self.asm = MayaRepresentation(self.root)
        self.path = path if path else self.asm.asm_data

    def create(self, build=True, mode=''):
        """ main method for create """
        lockDict = self.lock_state()
        self.lock(False, all=True)
        self.lock_pos(False, all=True)
        message = 'Cannot create. "%s" not exists' % self.path
        create(self.root, self.path, adPath=self.adPath) if os.path.exists(self.path) else logger.warning(message)
        self.build() if build else None
        # self.lock_state(lockDict) if not mode else self.lock_mode(mode)# relock

    def update(self, build=True, mode=''):
        """ update method """
        lockDict = self.lock_state()
        self.lock(False, all=True)
        self.lock_pos(False, all=True)
        message = 'Cannot update. "%s" or "%s" not exists' % (self.root, self.path)
        create(self.root, self.path)  if mc.objExists(self.root) and os.path.exists(self.path) else logger.warning(message)
        self.build() if build else None
        self.lock_state(lockDict) if not mode else self.lock_mode(mode)# relock

    def build(self, buildType='gpu', res='md'):
        lockDict = self.lock_state()
        build(self.root, buildType=buildType, res=res)
        self.lock_state(lockDict)

    def lock_mode(self, mode=''):
        """ default = not lock
            set = free asm lock child
            shot = lock asm free child """
        if mode == '':
            self.lock(False, all=True)
            self.lock_pos(False, all=True)
        if mode == 'set':
            self.lock(False, all=True)
            self.lock_pos(True, child=True)
            self.lock_pos(False, asm=True)
        if mode == 'shot':
            self.lock(False, all=True)
            self.lock_pos(False, child=True)
            self.lock_pos(True, asm=True)
            self.lock(True, all=True)

    def lock(self, state, asm=False, child=False, all=False):
        childs = self.get_childs(asm=asm, child=child, all=all)
        for child in childs:
            MayaRepresentation(child).lock_node(state)

    def lock_pos(self, state, asm=False, child=False, all=False):
        childs = self.get_childs(asm=asm, child=child, all=all)
        transformChilds = [a for a in childs if mc.objectType(a, isType='transform')]
        for child in transformChilds:
            MayaRepresentation(child).lock_pos(state)

    def set_hidden(self, state):
        lockDict = self.lock_state()
        childs = self.get_childs(asm=True)
        for child in childs:
            rep = MayaRepresentation(child)
            rep.set_vis(not state)
        self.lock_state(lockDict)

    def remove(self, ref=True):
        self.lock(False, child=True)
        asms = self.get_childs(asm=True)
        for asm in asms:
            MayaRepresentation(asm).remove_child(ref=ref)


    def remove_root(self):
        self.lock(False, all=True)
        asm_maya.remove(self.root)

    def get_childs(self, asm=False, child=False, all=False):
        if asm:
            return self.list_asm()
        if child:
            return self.list_childs()
        if all:
            return self.list_all()

    def lock_state(self, lockDict=None):
        members = self.list_all()
        if not lockDict:
            lockDict = OrderedDict()
            for member in members:
                lockDict[member] = MayaRepresentation(member).is_lock()
        else:
            for member, state in lockDict.iteritems():
                if mc.objExists(member):
                    state = state[0] if state else False
                    MayaRepresentation(member).lock_node(state)

        return lockDict

    def get_set_time(self): 
        if self.asm.is_root(): 
            return self.asm.get_set_time()

    def list_asm(self):
        """ list only locators """
        locs = [a for a in list_asm_childs(self.root) if not MayaRepresentation(a).is_group()]
        return locs

    def list_childs(self):
        """ list childs under asm locator """
        asms = self.list_asm()
        childs = []

        for asm in asms:
            members = mc.listRelatives(asm, c=True, type='transform', f=True)
            childs = childs + members if members else childs
        return childs

    def list_all(self):
        nodes = mc.ls(self.root, dag=True, l=True)
        transform = [a for a in nodes if mc.objectType(a, isType='transform')]
        return transform

    def list_assets(self): 
        locs = self.get_childs(asm=True)
        assets = []

        for loc in locs: 
            loc = MayaRepresentation(loc)
            desPath = loc.asset_description()
            asset = context_info.ContextPathInfo(path=desPath)
            if not asset.name in assets: 
                assets.append(asset.name)

        return assets


class ChildNode(object):
    """docstring for ChildNode"""
    def __init__(self, name):
        super(ChildNode, self).__init__()
        self.name = name

    def __repr__(self):
        return self.name

    def __str__(self):
        return self.name

    def shortname(self):
        return self.name.split('|')[-1]

    def is_gpu(self):
        shape = mc.listRelatives(self.name, s=True)
        return mc.objectType(shape[0], isType='gpuCache') if shape else False

    def is_rsproxy(self):
        nodes = mc.listHistory(self.name)
        rsProxyNode = [a for a in nodes if mc.objectType(a, isType='RedshiftProxyMesh')] if nodes else []
        return True if rsProxyNode else False

    @property
    def child(self):
        nodes = mc.listRelatives(self.name)
        nodes = [a for a in nodes if mc.objectType(a, isType='transform')] if nodes else []
        return nodes[0] if nodes else ''

    def is_free(self):
        return True if not self.child else False

    def remove(self):
        mc.delete(self.child) if self.child else None

    def attach(self, obj):
        return mc.parent(obj, self.child) if self.child else None


def build_gpu(gpuFile, name, shortname, hidden=False, mode=''): 
    # name = locator long name 
    # shortname = locator shortname
    gpu = asm_maya.create_gpu(shortname, gpuFile)
    if gpu:
        gpuObject = MayaRepresentation(gpu)
        gpuObject.set_parent(name)
        gpuObject.reset_local_pos()

        gpuObject.lock_pos(True) if mode == Config.setdress else None
        gpuObject.lock_node(True) if mode == Config.shot else None
        gpuObject.set_vis(not hidden)
        asm_maya.set_attr(name, DataAttr.cache, False)
        return True
    else:
        logger.warning('Cannot built. Gpu path not exists %s' % gpuFile)

    return gpu

            # # try again
            # if res == 'md':
            #     build(buildType, res='pr')
            #     return True

    # else:
    #     logger.debug('Cannot build. No Gpu or asset no published. Missing %s' % regFile)
    #     # try again
    #     if res == 'md':
    #         logger.debug('"md" not register, try to build "proxy"')
    #         self.build(buildType, res='pr', mode=mode)
    #         return True


def list_set(entity=None):
    setGrp = 'Set_Grp'
    if entity: 
        setGrp = entity.projectInfo.asset.set_grp() or 'Set_Grp'
    asms = []
    if mc.objExists(setGrp):
        sets = mc.listRelatives(setGrp, c=True)
        if sets: 
            for asset in sets:
                asm = MayaRepresentation(asset)
                if asm.is_root():
                    asms.append(asset)
    return asms


def build_cache(entity, rigFile, name, shortname, material=True): 
    cache = asm_maya.create_reference_rig(entity, shortname, rigFile, material)
    if cache:
        cacheObject = MayaRepresentation(cache[0])
        cacheObject.set_parent(name)
        cacheObject.reset_local_pos()
        # self.set_cache(True)
        asm_maya.set_attr(name, DataAttr.cache, True)
    return cache


def asm_name(name):
    """ get rep name """
    # name_001
    count = asm_maya.name_count(name)
    return '%s_%03d' % (name, count + 1)

def export(root, path, mode='normal', selection=False):
    """ export root to data file """
    data = OrderedDict()
    currentSels = mc.ls(sl=True, dag=True, l=True)
    currentSelGrps = [MayaRepresentation(a).parent for a in currentSels]

    if mc.objExists(root):
        rootLn = mc.ls(root, l=True)[0]
        rootSn = mc.ls(root)[0]

        # list all hierarchy
        allChilds = mc.ls(root, dag=True, l=True)
        asmChilds = filter_childs(allChilds)
        # if copy, do not filter for root upstream. just export data
        if mode == 'copy':
            asmChilds = filter_basic(allChilds)

        for asm in asmChilds:
            childDict = OrderedDict()
            asmObject = MayaRepresentation(asm)
            name = relative_longname(asm, rootLn)
            isRoot = True if asm == rootLn else asmObject.is_root()
            setAsm = asmObject.asm_data
            hidden = asmObject.is_hidden()

            if selection: 
                if asmObject.node_type == 'asm': 
                    if asm not in currentSels: 
                        continue

            if asm == rootLn:
                continue

            childDict['root'] = isRoot or False
            childDict['setAsm'] = setAsm
            childDict['shortName'] = asmObject.shortname
            childDict['nodeType'] = asmObject.node_type
            childDict['order'] = asmObject.node_order
            childDict['assetDescription'] = asmObject.asset_description()
            childDict['parent'] = relative_longname(asmObject.parent, rootLn)
            childDict['position'] = asmObject.position()
            childDict['local'] = asmObject.position(False)
            childDict['offset'] = asmObject.child_offset
            childDict['hidden'] = hidden if hidden else False
            childDict['look'] = asmObject.look
            childDict['geo'] = asmObject.look
            data[name] = childDict

    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))
    return file_dumper(path, data)

def paste(root, path, asLocator=True):
    root = asm_maya.root_name(root, '')
    rootSn = asm_maya.shortname(root)
    rawData = file_loader(path)
    data = add_root_data(root, rawData)
    remove_asm(root, data)

    # create root
    rootObject = MayaRepresentation(root)
    if not asm_maya.object_exists(root):
        # as locator 
        if asLocator: 
            rootObject = MayaRepresentation(rootSn, True)
        # as group
        else: 
            rootObject = MayaRepresentation(rootSn)
            rootObject.create(rootSn, asLocator=False)

        rootObject.set_asm_description(path)

    for node, valueDict in data.iteritems():
        # extract values
        buildNode = create_node(root, node, data)


def create(root, path, parent='', pos=[], adPath=''):
    editRoot = asm_maya.EditMode(root)
    start = datetime.now()

    with editRoot: 
        root = asm_maya.root_name(root, parent)
        rootSn = asm_maya.shortname(root)
        rawData = file_loader(path)
        data = add_root_data(root, rawData)
        mfiletime = filetime(path)

        # remove excessive asm
        remove_asm(root, data)

        # create root
        rootObject = MayaRepresentation(root)
        if not asm_maya.object_exists(root):
            rootObject = MayaRepresentation(rootSn, True)
            rootObject.set_parent(parent)

        rootObject.set_pos(pos) if pos else None
        rootObject.lock_node(False)
        rootObject.lock_pos(False)
        rootObject.set_asm_description(path)
        rootObject.set_description(adPath)
        rootObject.set_root(True)
        rootObject.set_date_modify(mfiletime)

        # detect current transform 
        rootPos = rootObject.position()
        rootObject.reset_pos()

        for node, valueDict in data.iteritems():
            # extract values
            s = datetime.now()
            buildNode = create_node(root, node, data)

        rootObject.set_pos(rootPos)

def create_node(root, key, data):
    name = key # rel long name
    shortName = data[key]['shortName']
    nodeType = data[key]['nodeType']
    parent = data[key]['parent']
    isRoot = data[key]['root']
    asmPath = data[key]['setAsm']
    order = data[key]['order']

    pos = data[key]['position']
    localPos = data[key]['local']
    offsetPos = data[key]['offset']
    hidden = data[key]['hidden']
    look = data[key]['look']
    nameClash = False
    isAsm = True

    buildNode = MayaRepresentation(name)
    nodeType = Config.nodeName if isRoot else nodeType

    if isRoot:
        create(shortName, asmPath, parent=parent, pos=pos)

    if not asm_maya.object_exists(name):
        if nodeType == Config.nodeName:
            # instance
            buildNode = MayaRepresentation(shortName, True)
            buildNode.set_values(data[key])


        if nodeType == Config.transform:
            groupName = asm_maya.create_group(shortName)
            buildNode = MayaRepresentation(groupName)
            nameClash = True if not shortName == groupName else False
            isAsm = False

    if parent:
        if not asm_maya.object_exists(parent):
            parentNode = create_node(root, parent, data)
            parent = parentNode.name

        buildNode.set_parent(parent, True)
        buildNode.rename(shortName) if nameClash else None

    # check child
    # find ctrl first
    childOffset = buildNode.child_offset if buildNode.child else None
    # set xform
    buildNode.set_asm(isAsm)
    buildNode.set_pos(pos)
    buildNode.set_child_pos(childOffset) if not childOffset == buildNode.zeroMatrix and not childOffset == None else None
    buildNode.set_order(order)
    
    # test bring back hidden 
    buildNode.set_hidden(hidden, False)

    return buildNode


def build(root, buildType='gpu', res='md', viewport=False):
    """ build through heirarchy """
    # allChilds = mc.ls(root, dag=True, l=True)
    # transformChilds = [a for a in allChilds if mc.objectType(a, isType='transform')]
    allChilds = list_asm_childs(root)
    if viewport: 
        allChilds = select_viewport_asm(timeline=True, sample=5)

    for child in allChilds:
        build_loc(child, buildType, res)

def build_loc(asmLocator, buildType='gpu', res='md'): 
    loc = MayaRepresentation(asmLocator)
    if not loc.is_root() and loc.is_rep():
        if not loc.is_hidden() and not loc.is_cache() and not loc.connected_asset():
            loc.build(buildType, res)
            loc.lock_node(False)

def remove(root, viewport=False, inverse=False): 
    allChilds = list_asm_childs(root) 
    allChilds = clear_naming(allChilds)
    if viewport: 
        childs = select_viewport_asm(timeline=True, sample=5)
        childs = clear_naming(childs)
        if inverse: 
            allChilds = [a for a in allChilds if not a in childs]
        else: 
            allChilds = childs
    for child in allChilds:
        loc = MayaRepresentation(child)
        loc.remove_child()

def clear_naming(list_obj):
    if list_obj:
        for ix, obj in enumerate(list_obj):
            if obj:
                if obj[0] == '|':
                    new_name = obj[1:]
                    list_obj[ix] = new_name
    return list_obj
   
def export_override(root, path):
    """ export override data """
    data = OrderedDict()
    rep = MayaRepresentation(root)
    rep.lock_node(False)
    rep.set_override(path)
    # rep.lock_node(True)
    asmRoot = Asm(root)
    asms = asmRoot.get_childs(asm=True)

    # root override pos 
    info = OrderedDict()
    loc = MayaRepresentation(root)
    key = '%s' % (root)
    info[ConfigAttr.position] = loc.position()
    data[key] = info

    for asm in asms:
        info = OrderedDict()
        loc = MayaRepresentation(asm)
        key = '%s%s' % (root, asm.split(root)[-1])
        # childPos = loc.child_offset
        info[ConfigAttr.childPos] = loc.child_offset
        info[ConfigAttr.hidden] = loc.is_hidden()
        info[ConfigAttr.cache] = loc.is_cache()
        info[ConfigAttr.description] = loc.asset_description()
        info[ConfigAttr.look] = loc.look
        info[ConfigAttr.variant] = loc.is_variant()
        info[ConfigAttr.originalVariant] = loc.get_original_variant()

        data[key] = info

    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))

    return file_dumper(path, data)


def import_override(root, path, build=False, force=True, run=True):
    """ import override data """
    start = datetime.now()

    if is_override_new(root, path) or force: 
        if not run: 
            logger.debug('import override preview update result = True')
            return True
        data = file_loader(path)
        rep = MayaRepresentation(root)
        rep.lock_node(False)
        rep.set_override(path)
        mtime = filetime(path)
        rep.override_date_modify(mtime)

        for asm, data in data.iteritems():
            loopStart = datetime.now()
            childPos = data.get(ConfigAttr.childPos)
            hidden = data.get(ConfigAttr.hidden) or False
            cache = data.get(ConfigAttr.cache)
            description = data.get(ConfigAttr.description)
            look = data.get(ConfigAttr.look)
            position = data.get(ConfigAttr.position)
            variant = data.get(ConfigAttr.variant)
            originalVariant = data.get(ConfigAttr.originalVariant)

            if mc.objExists(asm):
                rep = MayaRepresentation(asm)
                if position: 
                    rep.set_pos(position)
                # if cache: 
                #     rep.set_cache(cache)

                # if hidden: 
                hide = False if hidden or cache else True
                rep.set_hidden(hidden)
                rep.set_cache(cache) if cache else rep.set_cache(False)
                # set variant 
                if variant: 
                    rep.set_description(description)
                    rep.set_variant(variant)
                    rep.set_original_variant(originalVariant)

                    if rep.child: 
                        if rep.child.is_gpu(): 
                            rep.remove_child(ref=False)
                            rep.build('gpu') 

                # remove cache GPU 
                MayaRepresentation(asm).remove_child(ref=False) if cache else None
                # this hide child GPU 
                rep.set_vis(hide)
                # rep.set_look(look)
                # rep.set_description(description)
                if build: 
                    buildType = 'gpu'
                    MayaRepresentation(asm).build(buildType)
                    
                if childPos: 
                    if not rep.ctrl: # skip update if rep connected to a ctrl 
                        if not childPos == rep.zeroMatrix:
                            rep.set_child_pos(childPos)
                        else: 
                            rep.set_child_pos(rep.zeroMatrix, ws=False)

        logger.info('Finished updating override')
    else: 
        logger.info('No override changes. Skip update override')
        return False 


def remove_asm(root, data):
    """ remove obj that is not in the data keys """
    allChilds = mc.ls(root, dag=True, l=True)[1:]
    asmChilds = filter_remove_child(allChilds)
    asmChilds = [a for a in asmChilds if MayaRepresentation(a).is_asm()]
    removeChilds = [a for a in asmChilds if not a in data.keys()]

    if removeChilds:
        # logger.info('%s objects will be removed' % (len(removeChilds)))
        for child in removeChilds[::-1]:
            asm = MayaRepresentation(child)
            asm.remove()
        logger.info('%s objects removed' % (len(removeChilds)))


def add_root_data(root, data):
    # root can be long name |setA|setB
    newRootSn = asm_maya.shortname(root)
    newRootDict = OrderedDict()

    for key, value in data.iteritems():
        parent = value[DataAttr.parent]
        newKey = asm_maya.format_longname([root, key])
        newParent = asm_maya.format_longname([root, parent]) if parent else None

        # replace parent to root if parent == root
        newRootDict[newKey] = value
        if not parent:
            newParent = root
        newRootDict[newKey][DataAttr.parent] = newParent

    return newRootDict

def find_root(data):
    root = [k for k, v in data.iteritems() if v[DataAttr.root]]
    return root[0] if root else ''

def filter_childs(objList):
    """ filter only asm and group """
    valid = []
    for obj in objList:
        if mc.objectType(obj, isType='transform'):
            loc = Representation(obj)
            if loc.is_asm() and valid_asm(obj):
                valid.append(obj)

            else:
                if is_group(obj) and valid_group(obj):
                    valid.append(obj)

    return valid

def filter_basic(objList):
    """ filter only asm and group """
    valid = []
    for obj in objList:
        if mc.objectType(obj, isType='transform'):
            loc = Representation(obj)
            if loc.is_asm():
                valid.append(obj)

            else:
                if is_group(obj) and not is_ref(obj):
                    valid.append(obj)

    return valid

def filter_remove_child(objList):
    """ this will find childs that belongs to the given data scope """
    valid = []
    skipPrefix = []
    transformList = [a for a in objList if mc.objectType(a, isType='transform')]

    for obj in transformList:
        loc = Representation(obj)
        skipPrefix.append(obj) if loc.is_root() and loc.is_asm() else None

    for obj in transformList:
        skip = any(a == obj[:len(a)] for a in skipPrefix)
        if not skip:
            valid.append(obj)

    return valid


def list_asm_childs(root):
    """ filter only asm and group """
    objList = mc.ls(root, dag=True, l=True)
    valid = []
    for obj in objList:
        if mc.objectType(obj, isType='transform'):
            loc = Representation(obj)
            if loc.is_asm() and not loc.is_root():
                valid.append(obj)

    return valid

def find_asm_root(obj):
    longName = mc.ls(obj, l=True)
    elems = longName[0].split('|')
    # find upper parents
    upperNodes = [('|').join(elems[1:-i]) for i in range(len(elems))]
    upperNodes = [a for a in upperNodes if a] if upperNodes else []
    for node in upperNodes:
        # check if more than 1 object, make it absolute name
        if len (mc.ls(node)) > 1: 
            node = '|{}'.format(node)
        if MayaRepresentation(node).is_asm():
            return node

def find_asm_message(obj): 
    """ find top group of the selection and see its message connection """ 
    longName = mc.ls(obj, l=True)
    elems = longName[0].split('|')
    # find upper parents
    # upperNodes = [('|').join(elems[1:-i]) for i in range(len(elems))]
    upperNodes = []

    for i in range(len(elems)): 
        node = ('|').join(elems[1:-i])
        if i == 0: 
            node = ('|').join(elems[1:])
        upperNodes.append(node)

    upperNodes = [a for a in upperNodes if a] if upperNodes else []

    for node in upperNodes:
        asmLoc = MayaRepresentation(node).connected_message()
        if asmLoc: 
            if MayaRepresentation(asmLoc[0]).is_asm():
                return asmLoc[0]


def is_group(obj):
    return True if not mc.listRelatives(obj, s=True) else False

def is_ref(obj): 
    return True if mc.referenceQuery(obj, inr=True) else False 

def valid_asm(obj):
    """ check if this asm is another root. if yes do not include this to data """
    # if this object is root, it is valid without iterate upstream
    loc = Representation(obj)
    if loc.is_root():
        return True

    # if this is normal object, check for upstream
    parents = list_upstream_transform(obj)
    return True if not [a for a in parents if Representation(a).is_root()] else False


def valid_group(group):
    """ check if this group is not under asm locator """
    parents = list_upstream_transform(group)
    return True if not [a for a in parents if Representation(a).is_asm()] else False


def list_upstream_transform(ply):
    """ find all upstream transform """
    ln = mc.ls(ply, l=True)
    elems = ln[0].split('|') if ln else ''
    parents = []
    for i, each in enumerate(elems):
        names = elems[1:i+1]
        if names:
            parents.append('|%s' % ('|').join(names))
    return parents


def relative_longname(name, removeRoot=''):
    """ remove | and remove given root to make name relative """
    # |root|group|treeA -> group|treeA
    if name:
        if removeRoot:
            name = remove_root(name, removeRoot)
        if name:
            if '|' in name[0]:
                return name[1:]
            return name


def remove_root(name, root):
    """ remove root from name """
    if name == root:
        return ''
    if root == name[:len(root)]:
        return name[len(root):]


def collect_asset_description(asset, includeHidden=True):
    setGrp = asset.projectInfo.asset.set_grp() if asset else 'Set_Grp'
    childs = list_asm_childs(setGrp)
    childs = [a for a in childs if not MayaRepresentation(a).is_hidden()] if not includeHidden else childs
    assetDict = sg_asset_dict(childs)
    return assetDict

def collect_shotdress_description(asset, includeHidden=True): 
    shotDressGrp = asset.projectInfo.asset.shotdress_grp() if asset else 'ShotDress_Grp'
    childs = list_asm_childs(shotDressGrp)
    childs = [a for a in childs if not MayaRepresentation(a).is_hidden()] if not includeHidden else childs
    assetDict = sg_asset_dict(childs)
    return assetDict

def check_asset_descriptions(paths): 
    failed = list()
    for path in paths: 
        reg = register_entity.RegisterInfo(path)
        if not isinstance(reg.asset_id, int): 
            failed.append(path)

    return failed

def sg_asset_dict(asms): 
    paths = list(set([MayaRepresentation(a).asset_description() for a in asms]))
    assetDict = OrderedDict()
    '''
    failed_list = check_asset_descriptions(paths)

    if failed_list: 
        error_message = ('\n').join(failed_list) 
        raise custom_exception.PipelineError(error_message)
    '''

    for path in paths:
        if path:
            reg = register_entity.RegisterInfo(path)
            try: 
                assetDict[reg.asset_name] = {'name': reg.asset_name, 'project': reg.project, 'id': int(reg.asset_id)}
            except TypeError: 
                message = ('{} error, {}, {}. Republished this asset will fix'.format(path, reg.asset_name, reg.asset_id))
                raise custom_exception.PipelineError(message)

    return assetDict

def collect_set_description(asset):
    assetDict = OrderedDict()
    setGrp = asset.projectInfo.asset.set_grp() if asset else 'Set_Grp'
    if mc.objExists(setGrp):
        roots = mc.listRelatives(setGrp)

        if roots:
            paths = [MayaRepresentation(a).asset_description() for a in roots if MayaRepresentation(a).is_root()]

            for path in paths:
                reg = register_entity.RegisterInfo(path)
                assetDict[reg.asset_name] = {'name': reg.asset_name, 'project': reg.project, 'id': reg.asset_id}

    return assetDict


# complete build exported set
def build_set(asset, namespace='', buildAsset=True, mode='', force=True, run=True):
    startTime = datetime.now()
    setGrp = asset.projectInfo.asset.set_grp()
    heroDir = asset.path.hero().abs_path()
    output = asset.output_name(outputKey='set', hero=True)
    filename = '%s/%s' % (heroDir, output)

    regInfo = register_entity.Register(asset)
    adPath = regInfo.hero_config_path()

    if not mc.objExists(setGrp):
        setGrp = mc.group(em=True, n=setGrp)

    childs = mc.listRelatives(setGrp, c=True, type='transform')
    existing = [a for a in childs if MayaRepresentation(a).is_asm()] if childs else []
    zone = namespace if namespace else '%s_asm' % asset.name
    #zone = '%s_asm' % asset.name
    # if update, put root with |set_grp|root, if new just create |root outside and parent later
    root = '%s|%s' % (setGrp, zone) if zone in existing else zone

    if is_file_new(root, filename) or force: 
        if run: 
            asm = Asm(root, filename, adPath)
            asm.create(build=buildAsset)
            logger.info('Finished Build set %s' % (datetime.now() - startTime))

            parent = mc.listRelatives(zone, p=True)
            if not parent:
                mc.parent(zone, setGrp)
            else:
                if not parent[0] == setGrp:
                    mc.parent(zone, setGrp)
        else: 
            return True 

    else: 
        logger.info('No changes. Skip update set "%s" "%s"' % (root, filename))
        return False

    # asm.lock_mode(mode=mode)

    logger.info('Finished in %s' % (datetime.now() - startTime))
    return asm

def update(entity=None, build=False, mode='', force=True, run=True):
    """ update all asm under set group """
    from rftool.utils import maya_utils
    statuses = []
    setGrp = entity.projectInfo.entity.set_grp() if entity else 'Set_Grp'
    
    if mc.objExists(setGrp):
        with maya_utils.ParentToWorld([setGrp]): 
            childs = mc.listRelatives(setGrp, c=True, type='transform') or []
            roots = [a for a in childs if MayaRepresentation(a).is_root()]
            
            if roots:
                for root in roots:
                    asmPath = MayaRepresentation(root).asm_data
                    asset = context_info.ContextPathInfo(path=asmPath)
                    asset.context.update(step='set', process='main')
                    rootName = '%s_asm' % asset.name
                    name = '%s|%s' % (setGrp, rootName)
                    asm = Asm(name)

                    if is_file_new(root) or force: 
                        if run: 
                            asm.update(build=build, mode=mode)
                            logger.info('Finished update set "%s"' % root)
                        else: 
                            statuses.append(True)
                    else: 
                        logger.info('No changes. Skip update on "%s"' % root)
                        statuses.append(False)

    return statuses


def update_selected(build=True, mode=''):
    """ update selected asm root """
    sels = mc.ls(sl=True, l=True)
    sels = [a[1:] for a in sels]
    asms = [a for a in sels if MayaRepresentation(a).is_root] if sels else []

    for asm in asms:
        asmPath = MayaRepresentation(asm).asm_data
        asset = context_info.ContextPathInfo(path=asmPath)
        asset.context.update(step='set', process='main')
        asm = Asm(asm)
        asm.update(build=build, mode=mode)

    mc.select(sels)

def build_select():
    sels = mc.ls(sl=True)
    for sel in sels:
        Asm(sel).build()

def find_set_root(grp='Set_Grp'):
    """ find root under define group """
    childs = mc.listRelatives(grp, c=True, type='transform', f=True)
    childsRelName = [a[1:] for a in childs] if childs else []
    roots = [a for a in childsRelName if MayaRepresentation(a).is_root]
    return roots

def select_viewport_asm(startFrame=None, endFrame=None, timeline=False, sample=10, expand=0): 
    from rftool.utils import maya_utils
    # timeline 
    if timeline: 
        startFrame = mc.playbackOptions(q=True, min=True)
        endFrame = mc.playbackOptions(q=True, max=True)
    # sample 
    maya_utils.select_object_range(int(startFrame), int(endFrame), step=sample, expand=expand)
    roots = find_selection_root()
    if roots: 
        roots = [a for a in roots if a]
        mc.select(roots) if roots else None
        return roots 

def optimized_asm():
    hiddenAsms = guess_hidden()

    for asm in hiddenAsms: 
        set_hidden(asm)

def guess_hidden(): 
    asms = select_viewport_asm(timeline=True)
    allAsms = list_asms()
    hiddenAsms = [a for a in allAsms if not a in asms]
    # remove root 
    hiddenAsms = [a for a in hiddenAsms if not MayaRepresentation(a).is_root()]
    return hiddenAsms 

def set_hidden(obj): 
    loc = MayaRepresentation(obj)
    loc.set_hidden(True)
    loc.remove_child()

def list_asms(): 
    locs = [mc.listRelatives(a, p=True, f=True)[0] for a in mc.ls(type='locator')]
    if locs: 
        asms = [a[1:] for a in locs if MayaRepresentation(a).is_asm()]
        return asms
    return []

def find_selection_root(): 
    sels = mc.ls(sl=True, l=True)

    roots = []
    if sels: 
        for sel in sels: 
            loc = MayaRepresentation(sel)
            if loc.is_asm(): 
                roots.append(sel) if not sel in roots else None 
            else:         
                root = find_asm_root(sel)
                roots.append(root) if not root in roots else None 
    
    return roots 
    # list_asm_childs

def sweep_hide_asm(root): 
    asms = list_asm_childs(root)
    hideAsms = []

    for asm in asms: 
        checkList = get_parents(asm)
        hideObjs = []
        for a in checkList:
            if a:
                checkVis = mc.getAttr('%s.visibility' % a)
                if not checkVis:
                    hideObjs.append(a)
        #hideObjs = [a for a in checkList if not mc.getAttr('%s.visibility' % a)]
        if MayaRepresentation(asm).child:
            childHidden = not mc.getAttr('%s.visibility' % MayaRepresentation(asm).child)
        else:
            childHidden = False

        if hideObjs or childHidden:
            if not mc.getAttr('%s.hidden' % asm):
                hideAsms.append(asm)
        
    return hideAsms

def convert_vis_to_hidden(root): 
    asms = sweep_hide_asm(root)

    if asms: 
        for asm in asms: 
            MayaRepresentation(asm).set_hidden(True)

def remove_hidden_child(root): 
    asms = list_asm_childs(root)

    for asm in asms: 
        loc = MayaRepresentation(asm)
        loc.remove_child() if loc.is_hidden() else None


def get_parents(obj): 
    ln = mc.ls(obj, l=True)
    elems = ln[0].split('|')
    parents = [ln[0]]
    
    for i in range(len(elems))[::-1]: 
        tempList = elems[0: i]
        if tempList: 
            upperParent = ('|').join(tempList)
            parents.append(upperParent)

    return parents

def get_geo(path): 
    """ guess process name by gpu and alembic name """ 
    scene = context_info.ContextPathInfo(path=path)
    basename = os.path.basename(path)
    outputList = scene.guess_outputKey(basename)
    if outputList: 
        outputKey, step = outputList[0]
        scene.context.update(step=step)
        scene.extract_name(basename, outputKey=outputKey, force=True)
        process = scene.process
        return process if not process == 'gpu' else 'main'

    logger.warning('Cannot find process from file name "main" return')
    return 'main'

def guess_process_file(reg, process, res, filetype='gpu'): 
    """ this is hard coded  where loop and find process name from model and anim """ 
    if filetype == 'gpu': 
        if process == 'main': 
            return reg.get.gpu(process='main', res=res)
        else: 
            fileInfo = reg.get.list_step_data_key(step='anim', process=process, dataKey='cache', res=res)
            return fileInfo

    if filetype == 'cache':
        if process == 'main': 
            return reg.get.cache(process='main', res=res)
        else: 
            fileInfo = reg.get.list_step_data_key(step='anim', process=process, dataKey='cache', res=res)
            return fileInfo

    if filetype == 'rsproxy': 
        if process == 'main': 
            return reg.get.rsproxy(look='main', res=res)
        else: 
            fileInfo = reg.get.list_step_data_key(step='anim', process=process, dataKey='rsproxy', res=res)
            return fileInfo


def alembic_data(): 
    abcs = mc.ls(type='AlembicNode')
    abcInfo = dict()

    for abc in abcs: 
        nodes = mc.listConnections(abc, d=True, s=False)
        if nodes:
            validNodes = [a for a in nodes if mc.objectType(a) in ['transform', 'mesh']]
            asmLoc = []
            
            for node in validNodes: 
                upNodes = list_upstream_transform(node)
                if upNodes: 
                    asmLoc = [a for a in upNodes if MayaRepresentation(a).is_asm()]
                    if asmLoc: 
                        abcInfo[asmLoc[0]] = abc 
                        break 

    return abcInfo


def cleanup(func): 
    def wrapper(*args, **kwargs): 
        result = func(*args, **kwargs)
        asm_maya.clean_instance()
        return result 
    return wrapper


class UI(object):
    """docstring for UI"""
    def __init__(self):
        super(UI, self).__init__()
        # window size width x height
        from functools import partial
        self.partial = partial
        self.w = 200
        self.h = 820

    def show(self):
        self.window = mc.window(uiName, t='%s - %s - %s' % (_title, _version, _description))
        self.layout = mc.columnLayout(adj=4, rs=4)
        mc.text(l='Asm Utilities', align='center')
        mc.button(l='Refresh Scene', h=30, bgc=(0.6, 1, 0.6), c=self.refresh_scene)
        mc.button(l='Refresh/Update Set', h=30, bgc=(0.8, 1, 0.8), c=self.update_selected)
        mc.button(l='Remove set', h=30, bgc=(1, 0.6, 0.6), c=self.remove_set)
        mc.separator()
        mc.text(l='Build')
        mc.button(l='Build GPU - proxy', h=30, bgc=(0.8, 1, 0.8), c=self.partial(self.build, 'pr'))
        mc.button(l='Build GPU - medium', h=30, bgc=(0.7, 1, 0.7), c=self.partial(self.build, 'md'))
        mc.button(l='Remove', h=30, bgc=(1, 0.6, 0.6), c=self.remove)
        mc.separator()
        mc.text(l='Show / Hide')
        mc.button(l='Show', h=30, c=self.show_asm, bgc=(0.9, 0.9, 0.9))
        mc.button(l='Hide', h=30, c=self.hide_asm, bgc=(0.6, 0.6, 0.6))
        mc.button(l='Reset position', h=30, bgc=(1, 0.6, 0.6), c=self.reset_override)
        mc.separator()
        mc.text(l='Switch')
        mc.rowLayout(nc=2, adj=True, columnAttach=[2,'both',50])
        mc.radioCollection('res')
        mc.radioButton('pr', label='pr')
        mc.radioButton('md', label='md', select=True)
        mc.setParent('..')
        mc.button(l='Rig', h=30, bgc=(1, 1, 0.6), c=partial(self.switch_rig, ''))
        mc.button(l='Cache', h=30, bgc=(1, 1, 0.6), c=partial(self.switch_cache, ''))
        mc.button(l='Gpu', h=30, bgc=(0.8, 1, 0.8), c=partial(self.switch_gpu, ''))
        mc.button(l='RsProxy', h=30, bgc=(0.6, 1, 0.6), c=partial(self.switch_rsproxy, '', 1))
        mc.button(l='RsProxy Inst', h=30, bgc=(0.6, 1, 0.6), c=partial(self.switch_rsproxy, '', 1, True))
        mc.button(l='RsProxy Box', h=30, bgc=(0.6, 1, 0.6), c=partial(self.switch_rsproxy, '', 0))
        mc.separator()
        mc.text(l='Selection')
        mc.button(l='Select Empty Rep', h=30, c=self.select_empty_rep)
        mc.button(l='Select Hidden', h=30, c=self.select_hidden_rep)
        mc.separator()
        mc.text(l='Set')
        mc.button(l='Show all', h=30, c=self.show_asm_all)
        mc.button(l='Show Only viewport', h=30, c=self.show_asm_viewport)

        mc.showWindow()
        mc.window(uiName, e=True, wh=[self.w, self.h])

    def refresh_scene(self, *args):
        update(entity=None, build=True, mode='shot')

    def update_selected(self, *args):
        update_selected(True, mode='shot')

    def remove_set(self, *args):
        roots = self.get_root()
        for root in roots:
            if MayaRepresentation(root).is_root():
                Asm(root).remove_root()

    def build(self, res, *args):
        """ build below """
        sels, asms = self.list_selection()
        for asm in asms:
            root = Asm(asm)
            root.build(buildType='gpu', res=res)
            root.lock_mode('shot')
        mc.select(sels)

    def remove(self, *args):
        """ remove below """
        sels, asms = self.list_selection()
        for asm in asms:
            Asm(asm).remove(ref=False)
        mc.select([a for a in sels if mc.objExists(a)])

    def remove_rep(self, *args):
        """ remove locator """
        sels, asms = self.list_selection()
        for asm in asms:
            MayaRepresentation(asm).remove()

    def show_asm(self, *args):
        sels, asms = self.list_selection()
        for asm in asms:
            Asm(asm).set_hidden(False)
        mc.select(sels)

    def hide_asm(self, *args):
        sels, asms = self.list_selection()
        for asm in asms:
            Asm(asm).set_hidden(True)
        mc.select(sels)

    def reset_override(self, *args):
        sels, asms = self.list_selection()
        for asm in asms:
            rep = MayaRepresentation(asm)
            rep.set_child_pos(rep.zeroMatrix, ws=False)
        mc.select(sels)

    @cleanup
    def switch_rig(self, res='md', overrideFile='', *args):
        from rf_maya.rftool.utils import pipeline_utils
        from Qt import QtWidgets
        if not res:
            res = mc.radioCollection('res', q=True, select=True)
        sels, asms = self.list_selection()

        for asm in asms:
            rep = MayaRepresentation(asm)
            path = rep.asset_description()
            asset = context_info.ContextPathInfo(path=path)
            asset.context.update(step='rig', process='main', res=res)
            rigDir = asset.path.hero().abs_path()
            rigFile = asset.output_name(outputKey='rig', hero=True, ext='.mb')
            rigPath = '%s/%s' %(rigDir, rigFile)

            # find model ABC
            asset.context.update(step='model', process='main', res=res)
            publishPath = asset.path.output_hero().abs_path()
            dataFile = asset.output(outputKey='cache')
            abcPath = '%s/%s' % (publishPath, dataFile)
            
            autoGenerated = pipeline_utils.read_autoGeneratedRig_data(entity=asset)
            # rig does not exists, ask user to generate
            if not os.path.exists(rigPath) or autoGenerated:
                dial = QtWidgets.QMessageBox()
                dial.setText('Simple rig_%s for %s can be generated.\n\nAuto generate rig?' %(res, asset.name))
                dial.setWindowTitle("Auto generate rig")
                dial.setIcon(QtWidgets.QMessageBox.Question)
                yes_button = dial.addButton('Yes', QtWidgets.QMessageBox.ButtonRole.YesRole)
                dial.addButton('No', QtWidgets.QMessageBox.ButtonRole.RejectRole)
                dial.exec_()

                if dial.clickedButton() == yes_button:
                    # print abcPath, os.path.exists(abcPath)
                    if not os.path.exists(abcPath):
                        mc.warning('Model %s for %s not found!\nCannot generate rig.' %(res, asset.name))
                        continue

                    # get environments for the project and use them in mayapy
                    logger.info('Creating environments for mayapy...')
                    version = mc.about(v=True)
                    project = asset.project
                    logger.info('Project: %s' %(project))
                    logger.info('Version: %s' %(version))
                    department = 'Py'

                    # merge environments with current environments
                    envStr = asset.projectInfo.get_project_env(dcc='maya', dccVersion=version, department=department)

                    import subprocess
                    from rf_maya.rftool.utils import maya_utils
                    from rf_app.propit2 import propit_cmd
                    reload(propit_cmd)
                    # auto generate rig
                    mayapy = maya_utils.pyinterpreter()
                    mc.waitCursor(st=True)
                    genRigProcess = subprocess.Popen([mayapy, propit_cmd.__file__, '-i', abcPath, '-r', res], 
                                       stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, env=envStr)
                    out, err = genRigProcess.communicate()
                    mc.waitCursor(st=False)

                    # set asset data autoGenerateRig to True
                    pipeline_utils.write_autoGeneratedRig_data(value=True, entity=asset)

            if os.path.exists(rigPath):
                if not rep.ctrl:
                    if not rep.connected_asset(): 
                        prevPos = rep.child_offset
                        rep.remove_child()
                        rep.build('rig', res=res, overrideFile=overrideFile)
                        if prevPos:
                            rep.set_ctrl_pos(prevPos) if not prevPos == rep.zeroMatrix else None
                    else: 
                        logger.warning('Rig already connected')
                else:
                    logger.warning('Rig already attached')

    @cleanup
    def switch_cache(self, res='md', overrideFile='', *args):
        if not res:
            res = mc.radioCollection('res', q=True, select=True)
        sels, asms = self.list_selection()
        for asm in asms:
            rep = MayaRepresentation(asm)
            prevPos = rep.child_offset
            if not overrideFile:
                path = rep.asset_description()
                asset = context_info.ContextPathInfo(path=path)
                reg = register_entity.Register(asset)
                cacheInfo = guess_process_file(reg, process='main', res=res, filetype='cache')
                if not cacheInfo:
                    mc.warning( "%s is does not have md" % asm)
                    # break
            rep.remove_child()
            rep.lock_node(False)
            rep.build('cache', res=res, mode='shot', overrideFile=overrideFile)
            if prevPos:
                rep.set_child_pos(prevPos) if not prevPos == rep.zeroMatrix else None

    @cleanup
    def switch_gpu(self, res='md', overrideFile='', *args):
        if not res:
            res = mc.radioCollection('res', q=True, select=True)
        sels, asms = self.list_selection()
        for asm in asms:
            rep = MayaRepresentation(asm)
            prevPos = rep.child_offset
            rep.remove_child()
            rep.lock_node(False)
            rep.build('gpu', res=res, mode='shot', overrideFile=overrideFile)
            if prevPos:
                rep.set_child_pos(prevPos) if not prevPos == rep.zeroMatrix else None

    @cleanup
    def switch_rsproxy(self, overrideFile='', mode=1, instance=False, *args):
        # disable logging #
        import logging
        logger = logging.getLogger()
        logger.setLevel('INFO')
        
        sels, asms = self.list_selection()
        res = 'md'
        # res = mc.radioCollection('res', q=True, select=True)
        for asm in asms:
            rep = MayaRepresentation(asm)
            prevPos = rep.child_offset
            if not overrideFile:
                path = rep.asset_description()
                asset = context_info.ContextPathInfo(path=path)
                reg = register_entity.Register(asset)
                cacheInfo = guess_process_file(reg, process='main', res=res, filetype='rsproxy')
                if not cacheInfo:
                    mc.warning( "%s do not have rsproxy" % asm)
                    break
            rep.remove_child()
            rep.lock_node(False)
            rep.build('rsproxy', res='md', proxyMode=mode, overrideFile=overrideFile, instance=instance)
            if prevPos:
                rep.set_child_pos(prevPos) if not prevPos == rep.zeroMatrix else None


    def list_selection(self):
        sels = mc.ls(sl=True)
        asms = [a for a in sels if MayaRepresentation(a).is_asm()]

        # group assume that it is valid
        groups = [a for a in sels if MayaRepresentation(a).is_group()]
        # convert group (cache group) to asm locator
        groups = list(set([find_asm_root(a) for a in groups if find_asm_root(a)]))
        # non asm mean asset parent under asm locator
        nonAsms = [a for a in sels if not MayaRepresentation(a).is_asm() and not MayaRepresentation(a).is_group()]
        # find parent incase of gpu selected
        parentAsms = list(set([find_asm_root(a) for a in nonAsms]))
        mesgAsms = list(set([find_asm_message(a) for a in nonAsms]))
        if not mesgAsms: 
            mesgAsms = list(set([find_asm_message(a) for a in groups]))
        asms = asms + parentAsms + groups + mesgAsms
        asms = [a for a in asms if a]

        return sels, list(set(asms))

    def select_empty_rep(self, *args):
        roots = self.get_root()
        freeLoc = []
        for root in roots:
            childs = Asm(root).get_childs(asm=True)
            free = [a for a in childs if MayaRepresentation(a).is_free()]
            freeLoc = freeLoc + free
        mc.select(freeLoc) if freeLoc else None

    def select_hidden_rep(self, *args):
        roots = self.get_root()
        hiddenLoc = []
        for root in roots:
            childs = Asm(root).get_childs(asm=True)
            hidden = [a for a in childs if MayaRepresentation(a).is_hidden()]
            hiddenLoc = hiddenLoc + hidden
        mc.select(hiddenLoc) if hiddenLoc else None

    def get_root(self):
        sels, asms = self.list_selection()
        roots = find_set_root() if not sels else asms
        return roots

    def show_asm_viewport(self, *args):
        build('Set_Grp', buildType='rsproxy', res='md', viewport=False)
        remove('Set_Grp', viewport=True, inverse=True )

    def show_asm_all(self, *args):
        build('Set_Grp', buildType='rsproxy', res='md', viewport=False)


def checktime(func, *args, **kwargs): 
    from datetime import datetime 
    def wrapper(*args, **kwargs): 
        start = datetime.now()
        value = func(*args, **kwargs)
        return value 
    return wrapper


def file_loader(path): 
    # choice for json
    try: 
        data = file_utils.json_loader(path)        
        logger.debug('json loaded %s...' % path)
    except ValueError: 
        data = file_utils.ymlLoader(path)
        logger.debug('yml loaded %s...' % path)
    return data 

def file_dumper(path, data): 
    name, ext = os.path.splitext(path)
    ymlFile = '%s.yml' % name

    # dump json 
    file_utils.json_dumper(path, data)
    logger.debug('finished write json %s' % path)

    # dump yaml 
    file_utils.ymlDumper(ymlFile, data, None)
    logger.debug('finished write yml backup %s' % ymlFile)
    return True

def filetime(path): 
    if os.path.exists(path): 
        mtime = os.path.getmtime(path)
        display = '%s;%s' % (time.ctime(mtime), mtime)
        return display
    return None

def display_to_mtime(display): 
    digitCheck = display.split(';')[-1].replace('.', '')
    if digitCheck.isdigit(): 
        return float(display.split(';')[-1])
    return 0

def is_file_new(root, path=''): 
    asm = MayaRepresentation(root)
    path = asm.asm_data if not path else path 
    currentMtime = display_to_mtime(asm.get_set_date())
    fileMtime = display_to_mtime(filetime(path))

    if fileMtime == currentMtime: 
        return False
    return True
    # get_override_date

def is_override_new(root, path): 
    asm = MayaRepresentation(root)
    currentMtime = display_to_mtime(asm.get_override_date())
    fileMtime = display_to_mtime(filetime(path))

    if fileMtime == currentMtime: 
        return False
    return True


class UiCmd(UI):
    """docstring for UiCmd"""
    def __init__(self):
        super(UiCmd, self).__init__()


def ui():
    def deleteUI(ui):
        if mc.window(ui, exists=True):
            mc.deleteUI(ui)
            deleteUI(ui)

    deleteUI(uiName)
    app = UI()
    app.show()

""" 
Copy exact locator 
Group name 'ShotDress' 
select ShotDress 
export('ShotDress', 'E:/temp/box.yml')
paste('ShotDress', 'E:/temp/box.yml', False)

Regular export 
This will create new instance as locator 
export('ShotDress', 'E:/temp/box.yml')
create('ShotDress1', 'E:/temp/box.yml')
create('ShotDress2', 'E:/temp/box.yml')

Copy paste 
export('ShotDress', 'E:/temp/box.yml')
paste('ShotDress1', 'E:/temp/box.yml')

# build / remove viewport 
build only visible in camera
asm_lib.build('ShotDress_Grp', viewport=True)

remove what's not visible in camera
asm_lib.remove('ShotDress_Grp', viewport=True, inverse=True)

convert hide visibility to asm hidden 
asm_lib.convert_vis_to_hidden('ShotDress_Grp')

# variant switcher 
# switch variant
sels, asms = asm_lib.UiCmd().list_selection()
loc = asm_lib.MayaRepresentation(asms[0])
# variant path 
gpuPath = 'P:/Bikey/asset/publ/prop/bushGrassC/hero/bushGrassC_mdl_gpu_md.hero.abc'
loc.set_variant_value(gpuPath)
asm_lib.UiCmd().switch_gpu()


# restore original variant
sels, asms = asm_lib.UiCmd().list_selection()
loc = asm_lib.MayaRepresentation(asms[0])
loc.restore_variant()
asm_lib.UiCmd().switch_gpu()

"""
