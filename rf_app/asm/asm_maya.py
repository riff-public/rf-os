import os
import sys
import maya.cmds as mc
import maya.mel as mm
from rftool.utils import maya_utils
from rf_maya.lib import maya_file
reload(maya_file)
from rf_utils.context import context_info
import pymel.core as pm
from collections import OrderedDict

class Attr:
    string = 'string'
    checkBox = 'bool'
    message = 'message'
    double = 'double'

class Asm:
    repType = 'locator'


def check_gpu_plugin():
    plugin = 'gpuCache.mll'
    if not mc.pluginInfo(plugin, q=True, l=True):
        mc.loadPlugin(plugin, qt=True)


def create_locator(name):
    locName = mc.spaceLocator(n=name)
    locName = locName[0] if locName else ''
    dupNames = mc.ls(locName)
    # check if more than 1 object for this name
    if len(dupNames) > 1:
        locName = dupNames[0]
    return locName


def name_count(name):
    # format name_000
    strCount = len(name) + 4
    objs = mc.ls('%s_*' % name, type='transform')
    count = len([a for a in objs if len(a) == strCount])
    return count


def get_sequence_name(name):
    mc.ls('%s_*' % name, type='transform')


def add_attr(obj, attr, value, type='string'):
    objAttr = '%s.%s' % (obj, attr)

    # check lock
    lockState = mc.lockNode(obj, q=True)
    mc.lockNode(obj, l=False) if lockState == [True] else None

    if not mc.objExists(objAttr):
        if type == Attr.string:
            mc.addAttr(obj, ln=attr, dt='string')
        if type == Attr.checkBox:
            mc.addAttr(obj, ln=attr, at='bool')
        if type == Attr.message:
            mc.addAttr(obj, ln=attr, at='message')
        if type == Attr.double: 
            mc.addAttr(obj, ln=attr, at='double')


        mc.setAttr(objAttr, e=True, keyable=False, channelBox=True)
    mc.setAttr(objAttr, value, type='string') if type == Attr.string else None
    mc.setAttr(objAttr, value) if type == Attr.checkBox else None

    # relock
    mc.lockNode(obj, l=True) if lockState == True else None
    return objAttr


def get_attr(obj, attr):
    objAttr = '%s.%s' % (obj, attr)
    if mc.objExists(objAttr):
        return mc.getAttr(objAttr)
    return ''

def set_attr(obj, attr, state):
    objAttr = '%s.%s' % (obj, attr)
    if pm.objExists(objAttr):
        objAttr = pm.PyNode(objAttr)
        relock = False 

        if objAttr.isLocked(): 
            objAttr.unlock()
            relock = True 
        
        pm.setAttr(objAttr, state)
        objAttr.lock() if relock else None 

def set_message(obj, attr, value): 
    objAttr = '%s.%s' % (obj, attr)
    if not mc.objExists(objAttr): 
        add_attr(obj, attr, '', type='message')
    mc.connectAttr('%s.message' % value, objAttr, f=True)

def get_message(obj, attr): 
    target = '%s.%s' % (obj, attr)
    if mc.objExists(target): 
        # add_attr(obj, attr, '', type='message')
        return mc.listConnections(target)


def snap(source, target): 
    source = pm.PyNode(source)
    target = pm.PyNode(target)
    pm.delete(pm.parentConstraint(target, source))

    if source.scaleX.isLocked(): 
        source.scaleX.unlock()
    if source.scaleY.isLocked(): 
        source.scaleY.unlock()
    if source.scaleZ.isLocked(): 
        source.scaleZ.unlock()
    scaleValueX = target.getAttr('scaleX')
    scaleValueY = target.getAttr('scaleY')
    scaleValueZ = target.getAttr('scaleZ')
    try:
        source.setAttr('scaleX', scaleValueX)
    except:
        pass
    try:
        source.setAttr('scaleY', scaleValueY)
    except:
        pass
    try:
        source.setAttr('scaleZ', scaleValueZ)
    except:
        pass


def object_exists(obj):
    return mc.objExists(obj)


def object_type(obj):
    return mc.objectType(obj)


def create_group(name):
    return mc.group(em=True, n=name)


def format_longname(nameList):
    # if first element has | keep it
    keepLn = True if nameList[0][0] == '|' else False
    longname = '|'.join([strip_ln_string(a) for a in nameList])
    return '|%s' % longname if keepLn else longname


def strip_ln_string(name):
    name = name[1:] if name[0] == '|' else name
    name = name[:-1] if name[-1] == '|' else name
    return name


def shortname(longname):
    return longname.split('|')[-1]


def root_name(root, parent):
    if not parent:
        return '|%s' % root
    return format_longname([parent, root])


def outliner_order(ply):
    ln = mc.ls(ply, l=True)[0]
    parent = mc.listRelatives(ply, p=True, f=True)
    if parent:
        childs = mc.listRelatives(parent, c=True, f=True)
    if not parent:
        childs = mc.ls(assemblies=True, l=True)

    # print parent, childs

    return childs.index(ln) or 0


def parent_order(child, parent, order=0):
    result = child
    if parent:
        childParent = mc.listRelatives(child, p=True)
        if childParent:
            if not childParent[0] == parent:
                result = mc.parent(child, parent)
        else:
            result = mc.parent(child, parent)

    mc.reorder(result, f=True)
    mc.reorder(result, r=order)
    return result


def lock_transform(objs, state):
    for obj in objs:
        if not mc.referenceQuery(obj, inr=True):
            attrs = mc.listAttr(obj, k=True)
            for attr in attrs:
                mc.setAttr('%s.%s' % (obj, attr), l=state)

def is_lock(obj):
    attrs = mc.listAttr(obj, k=True)
    return any([mc.getAttr('%s.%s' % (obj, a), l=True) for a in attrs])


def create_gpu(name, path):
    check_gpu_plugin()
    if os.path.exists(path):
        return maya_utils.create_gpu(name, path)

def create_reference(name, path):
    currentNodes = mc.ls(assemblies=True)
    maya_utils.create_reference(name, path, force=False)
    return [a for a in mc.ls(assemblies=True) if not a in currentNodes]

def create_reference_rig(entity, name, path, material=True, group=''):
    currentNodes = mc.ls(assemblies=True)
    maya_file.create_reference(entity, name, path, material=material)
    node = [a for a in mc.ls(assemblies=True) if not a in currentNodes]
    if group: 
        group = mc.group(em=True, n=group) if not mc.objExists(group) else group 
        mc.parent(node, group)
    return node 

def create_render_proxy(name, path, displayMode=0, displayPercentage=10, gpuPreviewPath=None):
    # naming padding asset_001_001
    formatName = '%s_001' % name
    newName = maya_utils.get_name_sequencial(formatName)
    # check extension for proxy type 
    name, ext = os.path.splitext(path)
    if ext == '.rs': 
        from rftool.render.redshift import rs_utils
        return rs_utils.create_proxy(nodeName=newName, proxyPath=path, displayMode=displayMode, displayPercentage=displayPercentage, force=True, placeHolderSuffix=False, gpuPreviewPath=gpuPreviewPath)
    if ext == '.ass': 
        from rftool.render.arnold import arnold_utils
        return arnold_utils.create_aiStandIn(nodeName=newName, proxyPath=path, displayMode=displayMode, force=True, placeHolderSuffix=False)


def create_proxy_instance(instanceName, assetName, path, displayMode=0, displayPercentage=10, gpuPreviewPath=None): 
    from rftool.render.redshift import rs_utils
    assetProxyGrp = 'AssetInstance_Grp'
    formatName = '%s_001' % instanceName
    newName = maya_utils.get_name_sequencial(formatName)
    
    if not pm.objExists(assetProxyGrp): 
        pm.group(n=assetProxyGrp, em=True)
    
    if pm.getAttr('{}.visibility'.format(assetProxyGrp)): 
        pm.setAttr('{}.visibility'.format(assetProxyGrp), 0)
    
    if not pm.getAttr('{}.visibility'.format(assetProxyGrp), lock=True): 
        pm.setAttr('{}.visibility'.format(assetProxyGrp), lock=True)

    originalProxy = get_proxy_instance(path, assetProxyGrp)
        # create original 
    if not originalProxy: 
        node, shape, originalProxy = rs_utils.create_proxy(nodeName=assetName, proxyPath=path, displayMode=displayMode, displayPercentage=displayPercentage, force=True, placeHolderSuffix=False, gpuPreviewPath=gpuPreviewPath)
        pm.parent(originalProxy, assetProxyGrp)
    else: 
        #print('proxy found', originalProxy)
        node = rs_utils.get_rsproxy_node(originalProxy.name())
        shape = originalProxy.getShape()

    instance = pm.duplicate(originalProxy, n=newName, instanceLeaf=True)[0]
    #print('instance', instance)
    instance.setParent(w=True)
    return node, shape, instance.name()


def get_proxy_instance(path, assetProxyGrp='AssetInstance_Grp'): 
    from rftool.render.redshift import rs_utils
    mc.select(cl=True)
    objects = pm.listRelatives(assetProxyGrp, ad=True, type='transform')
    originalProxy = ''
    if objects: 
        for obj in objects: 
            rsNode = rs_utils.get_rsproxy_node(obj.name())
            if rsNode: 
                proxyPath = pm.getAttr('{}.fileName'.format(rsNode))
                if path == proxyPath: 
                    originalProxy = obj 
                    node = rsNode

    return originalProxy


def clean_instance(): 
    from rftool.render.redshift import rs_utils
    assetProxyGrp = 'AssetInstance_Grp'
    if pm.objExists(assetProxyGrp): 
        objects = pm.listRelatives(assetProxyGrp, ad=True, type='transform')

        for obj in objects: 
            rsNode = rs_utils.get_rsproxy_node(obj.name())
            if rsNode: 
                # len == 1 mean no instance anymore 
                if len(pm.listRelatives(obj.getShape(), allParents=True)) == 1: 
                    pm.delete(obj)

        objects = pm.listRelatives(assetProxyGrp, ad=True, type='transform')
        if not objects: 
            pm.delete(assetProxyGrp)


def override_instance_material(node, entity): 
    from rftool.shade import shade_utils
    reload(shade_utils)
    shadeNamespace = entity.mtr_namespace
    # defalut material
    mtrFile = entity.library(context_info.Lib.mtr)
    mtr = '%s/%s' % (entity.path.hero().abs_path(), mtrFile)
    mtrRel = '%s/%s' % (entity.path.hero(), mtrFile)
    shade_utils.apply_ref_shade(shadeFile=mtrRel, shadeNamespace=shadeNamespace, reloadRef=False, debug=False)
    mc.setAttr('{}.materialMode'.format(node), 2)
    mc.setAttr('{}.nameMatchPrefix'.format(node), '{}:'.format(shadeNamespace), type='string')


def remove(obj, ref=True):
    if not mc.referenceQuery(obj, inr=True):
        unlock_child(obj)
        mc.delete(obj)
    else:
        if ref:
            path = mc.referenceQuery(obj, f=True)
            mc.file(path, rr=True)

def unlock_child(obj):
    childs = mc.ls(obj, dag=True, l=True)
    for child in childs:
        mc.lockNode(child, l=False)


def has_key(obj): 
    curves = mc.keyframe(obj, q=True, n=True)
    return True if curves else False 

def get_curves(obj): 
    curves = mc.keyframe(obj, q=True, n=True)
    return curves


def connect_attr(srcAttr, dstAttr, keepValue=False): 
    if keepValue: 
        dstValue = mc.getAttr(dstAttr)
        mc.setAttr(srcAttr, dstValue)
    if not mc.isConnected(srcAttr, dstAttr): 
        mc.connectAttr(srcAttr, dstAttr, f=True)


def add_locator_shape(transform): 
    shape_name = '{}Shape'.format(transform)
    node = pm.PyNode(transform)
    if not node.getShape(): 
        loc_shape = pm.createNode('locator', n=shape_name)
        pm.parent(loc_shape, node, s=True, r=True)


class EditMode(): 
    """ release lock and restore previous state """ 
    def __init__(self, root): 
        self.root = root 
        self.lockDict = self.get_state()

    def get_state(self): 
        lockDict = OrderedDict()
        if pm.objExists(self.root): 
            targets = pm.listRelatives(self.root, ad=True, type='transform', f=True)

            for target in targets: 
                lockDict[target.longName()] = pm.lockNode(target, q=True)[0]

        return lockDict

    def unlock(self): 
        """ get childs under root """ 
        if pm.objExists(self.root): 
            targets = pm.listRelatives(self.root, ad=True)

            for target in targets: 
                # unlock if lock 
                pm.lockNode(target, l=False) if pm.lockNode(target, q=True)[0] else None

    def restore(self): 
        """ restore state """ 
        if self.lockDict: 
            for obj, state in self.lockDict.iteritems(): 
                pm.lockNode(obj, l=True) if state else None

    def __enter__(self): 
        self.unlock()

    def __exit__(self, *args): 
        self.restore()

