# v.0.0.1 library wip
_title = 'Set Utilities'
_version = 'v.0.0.1'
_des = 'beta'
ui_name = 'RFASMSwitch'

#Import python modules
import sys
import os
import logging
import getpass
import json

module_dir = os.path.dirname(sys.modules[__name__].__file__)
app_name = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

import maya.cmds as mc
import maya.mel as mm
import maya.OpenMayaUI as mui

# initial env setup
user = '%s-%s' % (os.environ.get('RFUSER'), getpass.getuser())

from rf_utils import log_utils

# set logger
log_file = log_utils.name(app_name, user=user)
logger = log_utils.init_logger(log_file)
logger.setLevel(logging.INFO)

from rftool.utils.ui import load
from rftool.utils import maya_utils

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui


ui_file = '%s/ui.ui' % module_dir
form_class, base_class = load.load_ui_type(ui_file)


class RFAsmUi(base_class):

    def __init__(self, parent=None):
        #Setup Window
        super(RFAsmUi, self).__init__(parent)
        # ui read
        self.ui = form_class()
        self.ui.setupUi(self)
        self.setWindowTitle('%s %s - %s' % (_title, _version, _des))
        self.setObjectName(ui_name)

        self.set_icons()

    def set_icons(self): 
        


# If inside Maya open Maya GUI
def getMayaWindow():
    ptr = mui.MQtUtil.mainWindow()
    return wrapInstance(long(ptr), QtWidgets.QWidget)


def show():
    deleteUI(ui_name)
    app = RFAsmUi(getMayaWindow())
    app.show()
    return app


def deleteUI(ui):
    if mc.window(ui, exists=True):
        mc.deleteUI(ui)
        deleteUI(ui)
