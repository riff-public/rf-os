# v.0.0.1 polytag switcher
_title = 'Bake App'
_version = 'v.0.0.1'
_des = 'Wip'
uiName = 'BakeUI'

#Import python modules
import sys
import os
import logging
import getpass
from functools import partial
import traceback
from collections import OrderedDict
import pymel.core as pmc
import os
import json
import pickle
import math
import subprocess
import tempfile
moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
import rf_config as config
reload(config)
# framework modules
from rf_utils import log_utils
reload(log_utils)
from rf_utils.ui import load
reload(load)
from rf_utils.ui import stylesheet
reload(stylesheet)

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui
from rf_utils.widget import project_widget
reload(project_widget)
from rf_utils.sg import sg_utils
reload(sg_utils)
from rf_utils import file_utils as fu
reload(fu)
from rf_utils.context import context_info
reload(context_info)
from rf_utils.pipeline import file_info 
reload(file_info)
from rf_utils.pipeline import create_asset
reload(create_asset)
from rf_utils import admin
reload(admin)
from rf_utils.sg import sg_process
reload(sg_process)
# maya & module function 
import pymel.core as pmc
import maya.cmds as mc
import maya.mel as mel

import sourceHik as hik
reload(hik)

from rf_app.mocap.function.mocapData import ioData as iod
reload(iod)

from rf_app.mocap.function.mocapData import poseData as posd
reload(posd)

from rf_app.mocap.function.mocapData import ctrlData as ctrld
reload(ctrld)

from rf_app.mocap.function.mocapData import mocapData
reload(mocapData)

from rf_app.mocap.function.mocapData import hikList as hikl
reload(hikl)

from rf_app.mocap.function import fkiksnap
reload(fkiksnap)

import rf_app.mocap.function.hikDef as hikd
reload(hikd)



from rf_maya.rftool.shade import shade_utils
reload(shade_utils)

class Bake(QtWidgets.QMainWindow):
	def __init__(self, parent=None):
		#Setup Window
		super(Bake, self).__init__(parent)

		# ui read
		uiFile = '%s/bake_app.ui' % moduleDir
		self.ui = load.setup_ui_maya(uiFile, parent)
		# set size 
		self.w = 500
		self.h = 600 

		self.setCentralWidget(self.ui)
		self.setObjectName(uiName)
		self.resize(self.w, self.h)
		self.setWindowTitle('%s %s %s' % (_title, _version, _des))
		#self.ui.exListWidget.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
		self.asset_list = ['']
		self.action_folder_list = []
		#self.bake_anim_list = []
		self.imProjComboBox = project_widget.SGProjectComboBox(sg=sg_utils.sg, layout=QtWidgets.QHBoxLayout())
		self.imProjComboBox.allLayout.setStretch(1, 1)
		self.imProjComboBox.label.setText('Project : ')

		self.projectWidget = project_widget.SGProjectComboBox(sg=sg_utils.sg, layout=QtWidgets.QHBoxLayout())
		self.projectWidget.allLayout.setStretch(1, 1)
		self.projectWidget.label.setText('  1. Project : ')
		# #spacerItem = QtWidgets.QSpacerItem(10, 10, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
		# #self.projectLayout.addItem(spacerItem)
		# self.projectWidget.projectComboBox.setCurrentText(self.mocap_char_project)
		# self.ui.asset_name.setCurrentText(self.mocap_char_asset)
		self.all_mocap = []
		self.asset_mocap_dict = {}
		self.asset_mocap_dict['all'] = []
		self.shot_mocap = []
		self.show_mocap = []
		self.search_char = ''
		self.search_name = ''
		self.init_import_function()
		self.init_signal()

#===========================================================================================================#
	#import function
	def init_import_function(self):
		self.ui.horizontalLayout_9.addWidget(self.imProjComboBox,0)
		self.imProjComboBox.projectComboBox.activated.connect(self.import_get_project)
		self.import_get_project()
		#self.change_animList()
		#self.update_show_list()
		self.ui.imAnimType.activated.connect(self.update_show_list)
		self.ui.imAssetComboBox.activated.connect(self.update_show_list)
		self.ui.search_name.returnPressed.connect(self.update_show_list)
		#self.add_button()
		#self.date_list_cbb()
		#self.obj_listWidget()
		self.ui.imListWidget.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
		self.ui.imListWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.ui.imListWidget.customContextMenuRequested.connect(self.show_entity_menu)

		self.ui.importButton.clicked.connect(self.import_hik)
		
		
		if not mc.pluginInfo('mayaHIK',loaded=1,q=1):
			mc.loadPlugin('mayaHIK')
		mc.help(pm=1)
		mel.eval('HIKCharacterControlsTool')

	def open_explorer(self, path):
		#print path
		#print 
		#subprocess.Popen('explorer ,"{}"'.format(os.path.dirname(path)))
		os.startfile(os.path.dirname(path))
	def show_entity_menu(self, pos):
		menu = QtWidgets.QMenu(self)
		#menu.addItem()
		path = self.ui.imListWidget.selectedItems()[-1]
		file_path =  path.toolTip()
		if file_path and os.path.isfile(file_path):
			explorerMenu = menu.addAction('Open in Explorer')
			explorerMenu.triggered.connect(partial(self.open_explorer, file_path))
			menu.popup(self.ui.imListWidget.mapToGlobal(pos))
		else:
			explorerMenu = menu.addAction('This file is from old version.')
			#explorerMenu.triggered.connect(partial(self.open_explorer, path))
			menu.popup(self.ui.imListWidget.mapToGlobal(pos))			
		


	def import_get_project(self):
		# change self.project f om project from comboBox
		self.im_project = self.imProjComboBox.projectComboBox.currentText()
		context = context_info.Context()
		context.update(project= self.im_project, entityType='asset', entityGrp ='char')
		asset_char = context_info.ContextPathInfo(context = context)
		# after change project it will change asset in combo box
		try: 
			filter_asset_list = fu.listFolder("{}".format(asset_char.path.scheme(key='publishAssetTypePath').abs_path()))
			#print filter_asset_list
			
			if filter_asset_list:
				#print 'asset_list :{}'.format(filter_asset_list)
				# filter asset that have mocap data
				check_context = context_info.Context()
				check_context.use_sg(sg_utils.sg, self.im_project, 'asset')
				check_asset = context_info.ContextPathInfo(context = check_context)	


				asset_list = ['all']
				self.asset_mocap_dict['all'] = []
				#print filter_asset_list
				for asset in filter_asset_list:
					#print asset
					if asset == 'mocap':
						print 'check asset : {}'.format(asset)
					else:
						#print '1'
						check_asset.context.update(step='mocap', process='', res='', entity=asset,entityGrp ='char')
						check_mocap = check_asset.path.scheme(key='publishProcessPath').abs_path()
						mocap_data_path = os.path.join("{}".format(check_mocap),'data')
						mocap_anim_path = os.path.join("{}".format(check_mocap),'raw')
						#print '2'
						mocap_fbx_data_path = os.path.join(check_mocap,'mocap_data.json')
						#print mocap_fbx_data_path
						if os.path.isfile(mocap_fbx_data_path):
							with open(mocap_fbx_data_path,'r') as f:
								fbx_data = json.load(f)
						else:
							fbx_data = ''			#print path
						#print 'mocap_data_path : {}'.format(mocap_data_path)
						if os.path.isdir(mocap_data_path):
							#print 'mocap data {} : Exists'.format(asset)
							#print asset
							asset_list.append(asset)
							self.asset_mocap_dict[asset] = []
						#print '3'
						if os.path.isdir(mocap_anim_path):
							asset_mocap_list = fu.listFile(mocap_anim_path)
							#print '4'
							if asset_mocap_list:
								for asset_mocap_file in asset_mocap_list:
									if '.mb' in asset_mocap_file:
										try:
											if fbx_data and fbx_data.has_key(asset_mocap_file):
												#print 'case1'
												self.asset_mocap_dict[asset].append([asset_mocap_file,os.path.join(mocap_anim_path,asset_mocap_file),fbx_data[asset_mocap_file]])
												self.asset_mocap_dict['all'].append([asset_mocap_file,os.path.join(mocap_anim_path,asset_mocap_file),fbx_data[asset_mocap_file]])
											else:
												#print 'case2'
												#print 'asset'
												self.asset_mocap_dict[asset].append([asset_mocap_file,os.path.join(mocap_anim_path,asset_mocap_file),''])
												#print all
												self.asset_mocap_dict['all'].append([asset_mocap_file,os.path.join(mocap_anim_path,asset_mocap_file),''])
										except Exception as err:
											print err
								#for asset in asset_mocap_list:
									#self.asset_mocap.append()
							else:
								pass

				self.ui.imAssetComboBox.clear()
				self.ui.imAssetComboBox.addItems(asset_list)
				#print asset_list
				
			else:
				asset_list = []
			
		except Exception as e:
			print e
			#asset_list = []
			

		#print asset_list
		self.ui.imAssetComboBox.clear()
		self.ui.imAssetComboBox.addItems(asset_list)
		self.change_animList()
		self.update_show_list()
		if not asset_list:
			mc.confirmDialog(m = '"{}" cannot do motion capture right now.'.format(self.im_project))

	def change_animList(self, *args):
		get_proj =  self.imProjComboBox.projectComboBox.currentText()
		shot_list = [sg_shot['code'] for sg_shot in sg_process.get_all_shots(get_proj)]
		shot_context = context_info.Context()
		shot_context.update(project=get_proj , entityType='scene')
		scene = context_info.ContextPathInfo(context=shot_context)
		self.shot_mocap = []
		#print shot_list
		for shot in shot_list:
			scene.context.update(entity = shot)
			scene.update_scene_context()
			shot_publ_path = scene.path.scheme(key='publishShotPath').abs_path()
			path = os.path.join(shot_publ_path,'mocap','raw')
			mocap_fbx_data_path = os.path.join(shot_publ_path,'mocap','mocap_data.json')
			#print mocap_fbx_data_path
			if os.path.isfile(mocap_fbx_data_path):
				with open(mocap_fbx_data_path,'r') as f:
					fbx_data = json.load(f)
			else:
				fbx_data = ''			#print path

			if os.path.isdir(path):
				mocap_file_list = fu.listFile(path)
				
				if mocap_file_list:
					for mocap_file in mocap_file_list:
						if '.mb' in mocap_file:
							if fbx_data and fbx_data.has_key(mocap_file):
								self.shot_mocap.append([mocap_file,os.path.join(path,mocap_file),fbx_data[mocap_file]])
							else:
								self.shot_mocap.append([mocap_file,os.path.join(path,mocap_file),''])
				else:
					continue
			else:
				continue

		# for shot in shot_list:
		# 	os.path.isdir(os.path.join)
		self.all_mocap = self.asset_mocap_dict['all'][:]+self.shot_mocap[:]

	def update_show_list(self):
		char_filter = self.ui.imAssetComboBox.currentText()
		search_filter = self.ui.search_name.text()
		animType = self.ui.imAnimType.currentText()
		#print animType
		
		if char_filter == 'all':
			char_filter =''
		self.show_mocap= []
		

		if animType == 'all':
			tmp_mocap_list = self.all_mocap[:]
		elif animType =='asset':
			tmp_mocap_list = self.asset_mocap_dict['all'][:]
		elif animType =='shot':
			tmp_mocap_list = self.shot_mocap[:]

		#print tmp_mocap_list
		if char_filter and search_filter:
			#print 'case1'
			for files in tmp_mocap_list:
				if char_filter in files[0] and search_filter in files[0]:
					self.show_mocap.append(files)

		elif char_filter and not search_filter:
			#print 'case2'
			for files in tmp_mocap_list:
				if char_filter in files[0]:
					self.show_mocap.append(files)
					
		elif not char_filter and search_filter:
			#print 'case3'
			for files in tmp_mocap_list:
				if search_filter in files[0]:
					self.show_mocap.append(files)
					
		elif not char_filter and not search_filter:
			#print 'case4'
			self.show_mocap = tmp_mocap_list[:]
		
		self.show_import_widget()


	def show_import_widget(self):
		self.ui.imListWidget.clear()
		# self.ui.imListWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		# self.ui.imListWidget.customContextMenuRequested.connect(self.show_explorer_menu)
		for action in self.show_mocap:
			#print action
			item = QtWidgets.QListWidgetItem(self.ui.imListWidget)
			item.setText(action[0])
			item.setData(QtCore.Qt.UserRole, action[1])
			item.setToolTip( action[-1])
        

	def import_hik(self,*args):
		item_path_list = self.ui.imListWidget.selectedItems()   
		for item in item_path_list:
			source_file = item.data(QtCore.Qt.UserRole)
			if '\\' in source_file:
				file_name = source_file.split('\\')[-1].split('.')[0]
			elif '/' in source_file:
				file_name = source_file.split('/')[-1].split('.')[0]

			self.ref_ns = str(file_name)
			
			for letter in [' ','-','+','*']:
				if letter in self.ref_ns:
					self.ref_ns = self.ref_ns.replace(letter,'_')

			mc.file(source_file, ns=self.ref_ns, r=1) 

#==============================================================================================================#
	
	# export function   

	def init_signal(self):
		self.ui.horizontalLayout_2.addWidget(self.projectWidget,0)
		self.ui.exAddButton.clicked.connect(self.add_widgets)
		self.ui.exDelButton.clicked.connect(self.del_widgets)
		self.projectWidget.projectComboBox.activated.connect(self.get_project)
		self.ui.fromSceneButton.clicked.connect(self.auto_create_task)
		self.ui.exAssetComboBox.activated.connect(self.get_asset_mocap_path)
		self.ui.bakeButton.clicked.connect(self.do_all)
		self.ui.bakeSelButton.clicked.connect(self.do_sel)
		self.ui.hikButton.clicked.connect(self.create_char)
		# self.ui.resComboBox.activated.connect(self.select_resolution)
		# self.ui.switchButton.clicked.connect(self.switch_resolution)
		self.ui.exListWidget.itemClicked.connect(self.swap_hik_source)
		#self.ui.exListWidget.customContextMenuRequested.connect(self.show_entity_menu)
		self.ui.mulSel_checkBox.stateChanged.connect(self.selection_mode)
		self.ui.loadSceneButton.clicked.connect(self.load_from_scene)
		self.ui.customCheck.stateChanged.connect(self.custom_check)
		self.get_project()
		try:
			self.get_asset_mocap_path()
		except:
			pass

	def loadDict(self,path):
		if os.path.exists(path):
			with open(path, 'r') as f:
				data = json.load(f)
				return data

	def add_widgets(self,animLayer = None, meta_data = None):
		if mc.objExists('hik_ctrl'):
			item = QtWidgets.QListWidgetItem(self.ui.exListWidget)
			if animLayer == None or meta_data == None:
				track_obj = track_item(self)
				self.ui.exListWidget.addItem(item)
				self.ui.exListWidget.setItemWidget(item,track_obj)
				item.setData(QtCore.Qt.UserRole,track_obj)
				item.setSizeHint(track_obj.sizeHint())
			else:
				track_obj = track_item.load_metaData(self,animLayer,meta_data)
				self.ui.exListWidget.addItem(item)
				self.ui.exListWidget.setItemWidget(item,track_obj)
				item.setData(QtCore.Qt.UserRole,track_obj)
				item.setSizeHint(track_obj.sizeHint())
		else:
			mc.confirmDialog(m='Please create humanIk Character first.')
		#track_obj.create_anim_layer()

		#self.sqShot_active()
		#self.track_obj.animComboBox.currentTextChanged.connect(self.sqShot_active)
		return track_obj

	def del_widgets(self):
		#self.ui.exListWidget.takeItem()
		#print self.ui.exListWidget.currentRow()
		widgetItems = self.ui.exListWidget.selectedItems()
		for item in widgetItems:
			mc.delete(item.data(QtCore.Qt.UserRole).anim_layer)
			idx = self.ui.exListWidget.row(item)
			self.ui.exListWidget.takeItem(idx)

	def selection_mode(self):
		
		if self.ui.mulSel_checkBox.isChecked():
			self.ui.exListWidget.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
			self.ui.exListWidget.itemClicked.disconnect()

		elif not self.ui.mulSel_checkBox.isChecked():
			self.ui.exListWidget.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
			
			self.ui.exListWidget.itemClicked.connect(self.swap_hik_source)	
	
	def get_project(self):
		# change self.project f om project from comboBox
		self.project = self.projectWidget.projectComboBox.currentText()
		context = context_info.Context()
		context.update(project= self.project, entityType='asset', entityGrp ='char')
		asset_char = context_info.ContextPathInfo(context = context)
		# after change project it will change asset in combo box
		try: 
			filter_asset_list = fu.listFolder("{}".format(asset_char.path.scheme(key='publishAssetTypePath').abs_path()))
			#print filter_asset_list
			
			if filter_asset_list:
				# filter asset that have mocap data
				check_context = context_info.Context()

				check_context.use_sg(sg_utils.sg, self.project, 'asset')
				check_asset = context_info.ContextPathInfo(context = check_context)	

				asset_list = []
				
				for asset in filter_asset_list:
					if asset == 'extra':
						continue
					elif asset == 'mocap':
						continue
					else:
						check_asset.context.update(step='mocap', process='', res='', entity=asset,entityGrp ='char')
						check_mocap = check_asset.path.scheme(key='publishProcessPath').abs_path()

						if os.path.isdir(os.path.join("{}".format(check_mocap),'data')):
							asset_list.append(asset)
			else:
				asset_list = []
		
		except Exception as e:
			asset_list = []

		self.ui.exAssetComboBox.clear()
		self.ui.exAssetComboBox.addItems(asset_list)

		if not asset_list:
			mc.confirmDialog(m = '"{}" cannot do motion capture right now.'.format(self.project))
			

	def get_asset_mocap_path(self):
		self.project = self.projectWidget.projectComboBox.currentText()
		self.asset = self.ui.exAssetComboBox.currentText()
		self.context = context_info.Context()
		self.context.use_sg(sg_utils.sg, self.project, 'asset', entityName = self.asset)
		self.asset_context = context_info.ContextPathInfo(context = self.context)
		self.asset_context.context.update(step='rig',process = 'main',res='md',app='maya',look='main')

		self.heroPath = self.asset_context.path.hero().abs_path()
		self.shd_file = self.asset_context.output_name(outputKey = 'materialRig',hero=True)
		self.shd_path = '{}/{}'.format(self.heroPath,self.shd_file)

		self.asset_context.context.update(step='rig',process = 'main',res='pv',app='maya')
		self.pv_file = self.asset_context.output_name(outputKey = 'rig',hero=True)
		self.pv_path = '{}/{}'.format(self.heroPath,self.pv_file).replace('.mb','.ma')
		
		self.asset_context.context.update(step='rig',process = 'main',res='pr',app='maya')
		self.pr_file = self.asset_context.output_name(outputKey = 'rig',hero=True)
		self.pr_path = '{}/{}'.format(self.heroPath,self.pr_file)

		self.asset_context.context.update(step='rig',process = 'main',res='md',app='maya')
		self.md_file =self.asset_context.output_name(outputKey = 'rig',hero=True)
		self.md_path = '{}/{}'.format(self.heroPath,self.md_file)

		self.asset_context.context.update(process='ctrl')
		self.task_publishFile = self.asset_context.path.scheme(key='publishHeroOutputPath').abs_path()
		self.ctrl_file = self.asset_context.publish_name(hero=True)
		self.ctrl_path = '{}/{}'.format(self.task_publishFile,self.ctrl_file)
		
		self.all_mover_name = self.asset_context.projectInfo.asset.get('allMoverCtrl')

		self.asset_context.context.update(step='mocap', process='', res='', app='')
		self.asset_mocap_path = self.asset_context.path.scheme(key='publishProcessPath').abs_path()

		self.asset_data_path = "{}/data".format(self.asset_mocap_path)
		self.asset_anim_path = "{}/anim".format(self.asset_mocap_path)

		if not os.path.isdir(self.asset_anim_path):
			admin.makedirs(self.asset_anim_path)

		self.bindPose_data_path = os.path.join(self.asset_data_path,'bindPose').replace('\\','/')
		self.tpose_data_path = os.path.join(self.asset_data_path,'tpose').replace('\\','/')
		self.hik_data_path = os.path.join(self.asset_data_path,'char_list').replace('\\','/')
		self.fkiksnap_data_path = os.path.join(self.asset_data_path,'fkikDict').replace('\\','/')

		if os.path.isfile(self.md_path):
			self.rig_path = self.md_path
		
		elif os.path.isfile(self.pr_path):
			self.rig_path = self.pr_path

		elif os.path.isfile(self.pv_path):
			self.rig_path = self.pv_path

		# elif os.path.isfile(self.ctrl_path):
		# 	self.rig_path = self.ctrl_path
		

		
		self.ns = self.asset + ':'
		self.char_name = self.asset+'_mocap'

		if os.path.isfile(self.hik_data_path) and os.path.isfile(self.fkiksnap_data_path):
			self.hik_char_data =  self.loadDict(self.hik_data_path)
			self.fkIk_dict = self.loadDict(self.fkiksnap_data_path)
			self.get_all_ctrl() 
		
		else:
			mc.confirmDialog(m = 'This asset not ready for motion capture.')

	def get_all_ctrl(self):
		self.ctrl_list = []
		
		for part in self.hik_char_data:
			if part[3]:
				self.ctrl_list.append(part[3])

		for key in self.fkIk_dict.keys():
			part_dict = self.fkIk_dict[key]
			for ctrl in part_dict['fk_ctrl']:
				if ctrl not in self.ctrl_list:
					self.ctrl_list.append(ctrl)

			for ctrl  in part_dict['ik_ctrl']:
				if ctrl not in self.ctrl_list:
					self.ctrl_list.append(ctrl)
		
		for num in range(len(self.ctrl_list)):
			self.ctrl_list[num] = self.ns + self.ctrl_list[num]

	def create_char(self):
		#self.get_asset_mocap_path()
		# check ctrl reference
		if not mc.objExists(self.ns[:-1]+'RN'):
			# if ctrl rig not in scene yet
			# ref one
			mc.file(self.rig_path ,r=1 ,ns = self.asset)
			#print self.shd_path
			if os.path.isfile(self.shd_path):
				#print 'shd is True'
				#mc.file(self.shd_file, ns = self.asset+'_shd')
				shade_utils.apply_ref_shade(self.shd_path,self.asset+'_shd',self.asset)


		# check human ik if humanIk not create yet
		if not mc.objExists(self.char_name):
			self.create_hik_character()
		else:
			mc.delete(self.char_name)
			self.create_hik_character()
		self.create_hik_ctrl()
		

	def create_hik_character(self):
		posd.read_pose(self.bindPose_data_path, self.ns, '', '')
		posd.read_pose(self.tpose_data_path, self.ns, '', '')
		hikd.create_char(self.ns, self.char_name, self.hik_char_data)
		hikd.create_hik_custom_rig(self.ns, self.char_name,self.hik_char_data)


	def create_hik_ctrl(self):
		IK_attr_list = ['leftArmIK','rigtArmIK','leftLegIK','rightLegIK']
		if not pmc.objExists('hik_ctrl'):
			self.hik_ctrl = pmc.circle(ch=0,r=10,nry=1,nrz=0,nrx=0)[0]
			self.hik_ctrl.rename('hik_ctrl')
			for trans in 'trs':
				for ax in 'xyz':
					self.hik_ctrl.attr('{}{}'.format(trans,ax)).lock()
					self.hik_ctrl.attr('{}{}'.format(trans,ax)).set(k=0)
			self.hik_ctrl.v.lock()
			self.hik_ctrl.v.set(k=0)
			add_lock_attr(self.hik_ctrl ,attr_name="hikIkATTR",)

			for attr in IK_attr_list:
				add_attr(self.hik_ctrl,attr_name=attr,k=1,dv=0,max=1,min=0)

			for attr in ['leftLegIK','rightLegIK']:
				mc.setAttr('{}.{}'.format(self.hik_ctrl,attr),1)
		else:
			self.hik_ctrl = pmc.PyNode('hik_ctrl')

		attr_list = [["ReachActorLeftWrist","ReachActorLeftWristRotation"],
					["ReachActorRightWrist","ReachActorRightWristRotation"],
					["ReachActorLeftAnkle","ReachActorLeftAnkleRotationRotation"],
					["ReachActorRightAnkle","ReachActorRightAnkleRotation"]]

		self.hik_prop_node = pmc.PyNode(self.char_name).propertyState.connections(s=1,d=0,p=0)
		for idx,hik_attrs in enumerate(attr_list):
			for hik_attr in hik_attrs:
				if not self.hik_prop_node[0].attr(hik_attr).connections(s=1,d=0,p=1):
					self.hik_ctrl.attr(IK_attr_list[idx])  >> self.hik_prop_node[0].attr(hik_attr)
	

	def export_mocap_anim(self,source,all_mover,start,end,path,file_name):
		# back to bind Pose for preparing fk/ik snap function
		# bake anim key
		handle,tmp_path = tempfile.mkstemp(suffix='.ma')
		os.close(handle)
		temp_dir = os.path.dirname(tmp_path)
		temp_file = os.path.basename(tmp_path)
		output_path = os.path.join(path,file_name)
		posd.read_pose(self.bindPose_data_path, self.ns, '', '')
		self.fkIk_dict = fkiksnap.add_dup_ik_ctrl(self.ns, self.fkIk_dict)
		posd.read_pose(self.tpose_data_path, self.ns, '', '')
		temp_output_path = hik.bake_hik_range(source ,self.char_name, all_mover, start , end, self.ctrl_list, temp_dir , temp_file,self.fkIk_dict)
		admin.copyfile(temp_output_path, output_path)
		os.remove(temp_output_path)
		#print output_path
		#print path
		#print file_name
		#print 'do :{}'.format(output_path)
		return output_path

	def clear_dup_ik_ctrl(self,fkIk_dict ={}):
		for key in self.fkIk_dict.keys():
			mc.delete(self.fkIk_dict[key]['dup_ik_ctrl'])
			self.fkIk_dict[key].pop('dup_ik_ctrl',None)
			#print self.fkIk_dict.keys()
	
	def do_all(self):
		self.do_task(all=1)

	def do_sel(self):
		self.do_task(all=0)

	def do_task(self,all = 1):
		all_mover = '{}{}'.format(self.ns,self.all_mover_name)
		if not mc.objExists(all_mover):
			mc.confirmDialog(m='please select allmover.')
			return			
		ref_node = self.ns[:-1]+'RN'


		# if self.rig_path != self.ctrl_path:
		# 	if os.path.isfile(self.ctrl_path):
		# 		mc.file(self.ctrl_path,loadReference = ref_node)
		# 	elif os.path.isfile(self.pv_path):
		# 		if self.rig_path != self.pv_path:
		# 			mc.file(self.ctrl_path,loadReference = ref_node)

		# else:
		# 	pass

		#hik.set_hik_source(self.char_name)
		if all:
			times = range(self.ui.exListWidget.count())
			item_list = []
			for idx in times:
				item_list.append(self.ui.exListWidget.item(idx))
		else:
			item_list = self.ui.exListWidget.selectedItems()

		
		retargeter = mel.eval('RetargeterGetName( "{}" )'.format(self.char_name))
		#print retargeter
		mel.eval('RetargeterDisconnect( "{}" );'.format(retargeter))
		mel.eval('HIKCharacterControlsTool ;')


		self.create_hik_character()
		mc.refresh(suspend=1)
		
		for item in item_list:
			task = item.data(QtCore.Qt.UserRole)
			task_data = self.setAnimData(task)
			
			# mute all layer before bake each task
			for layer in mc.ls(type='animLayer'):
				mel.eval('animLayerMuteCallBack  "{}" "1";'.format(layer))
				mc.animLayer(layer,e=1,sel=0)

			# mute only working layer
			mel.eval('animLayerMuteCallBack  "{}" "0";'.format(task.anim_layer))
			mc.animLayer(task.anim_layer,e=1,sel=1)

			# do bake each task
			if task_data['animType'] == 'shot':
				file_name = task_data['source'].split(':')[0]+'.ma'
				export_path = os.path.join(task_data['path'],'mocap',self.asset)


			elif task_data['animType'] == 'asset':
				file_name = task_data['source'].split(':')[0]+'.ma'
				export_path = os.path.join(self.asset_anim_path)

			print export_path

			if not os.path.isdir(export_path):
				'craete Export_path : {} '.format(export_path)
				result = admin.makedirs(export_path)
				if result:
					pass
				else:
					try:
						os.mkdir(export_path)
					except Exception as e:
						print e

			temp_path = self.export_mocap_anim(task_data['source'],all_mover ,float(task_data['startEnd'][0]) ,float(task_data['startEnd'][1]),export_path,file_name)
			
			#admin.copyfile(temp_path , os.path.join(export_path, file_name))
			
			anim_crv = mc.keyframe(self.ctrl_list, q=True, n=1)
			mc.delete(anim_crv)
			self.clear_dup_ik_ctrl(self.fkIk_dict)
		
		mc.refresh(suspend=0)
		mc.confirmDialog(m='bake mocap animation complete')


	def auto_create_task(self):
		self.ui.exListWidget.clear()
		hik_node = hik.sourceHikNode()
		hik_key = hik_node.keys()
		hik_key.sort()
		
		task_context = context_info.Context()
		for name in hik_key :
			frame = hik_node[name]

			item = self.add_widgets()
			item.sourceComboBox.setCurrentText(name)
			item.startLine.setText(str(frame[0]))
			item.endLine.setText(str(frame[1]))
			
			hik_ns = pmc.PyNode(name).namespace()[:-1]
			splitList = hik_ns.split('_')[0]
			print hik_ns
			file_path = mc.ls('{}:*'.format(hik_ns),assemblies=1)[0]
			#file_path = hik_ns+':Reference'
			if mc.objExists(file_path+'.fbxPath'):
				file_path = mc.getAttr(file_path+'.fbxPath')
			else:
				file_path = 'This motion capture data is from old version.'

			item.sourceFileLabel.setText(file_path)
			
			if len(splitList) >= 4:
				shotName = '_'.join(name.split('_')[:4])
				task_context.use_sg(sg_utils.sg, self.project,'scene',entityName=shotName)
				scene_context = context_info.ContextPathInfo(context=task_context)
				sq = scene_context.episode

			else:
				sq =''
				shotName = ''

			if item.sqComboBox.findText(sq) != -1:
				sqIdx  = item.sqComboBox.findText(sq)
				
				item.sqComboBox.setCurrentIndex (sqIdx)
				item.add_shot()
				
				shotIdx = item.shotComboBox.findText(shotName)
				
				item.shotComboBox.setCurrentText (shotName)
			else:
				item.animComboBox.setCurrentText('asset')
				item.sqShot_active()

			item.task_metaData()

	def swap_hik_source(self):
		#print self.ui.exListWidget.count()
		if self.ui.exListWidget.count():
			cur_task = self.ui.exListWidget.currentItem()
			task = cur_task.data(QtCore.Qt.UserRole)
			task_data = self.setAnimData(task)
			source = task_data['source']
			hikd.set_hik_source(self.char_name,source)

		for layer in mc.ls(type='animLayer'):
			mel.eval('animLayerMuteCallBack  "{}" "1";'.format(layer))
			mc.animLayer(layer,e=1,sel=0)

		mel.eval('animLayerMuteCallBack  "{}" "0";'.format(task.anim_layer))
		mc.animLayer(task.anim_layer,e=1,sel=1)

	def setAnimData(self,track_obj):
		source_data = track_obj.sourceComboBox.currentText()
		sourceFile_data = track_obj.sourceFileLabel.text()
		animType_data = track_obj.animComboBox.currentText()
		sequence_data = track_obj.sqComboBox.currentText()
		shot_data = track_obj.shotComboBox.currentText()
		startEnd_data = [track_obj.startLine.text(), track_obj.endLine.text()]
		shot_path = track_obj.shotComboBox.itemData(track_obj.shotComboBox.currentIndex())
		obj_data = {"source":source_data,"sourceFile":sourceFile_data ,"animType":animType_data, "sequence":sequence_data, "shot":shot_data, "startEnd":startEnd_data, "path":shot_path}
		return obj_data


	def load_from_scene(self):
		self.ui.exListWidget.clear()
		animLayer = mc.ls(type='animLayer',ro=0)
		animLayer.remove('BaseAnimation')
		for layer in animLayer:
			metaData = pickle.loads(mc.getAttr(layer+'.taskData'))
			track_obj = self.add_widgets(layer,metaData)

	def custom_check(self):
		if not self.ui.customCheck.isChecked():
			self.ui.exAddButton.setDisabled(1)
			self.ui.exDelButton.setDisabled(1)
			self.ui.loadSceneButton.setDisabled(1)
		else:
			self.ui.exAddButton.setEnabled(1)
			self.ui.exDelButton.setEnabled(1)
			self.ui.loadSceneButton.setEnabled(1)			

class ComboBoxNoWheel(QtWidgets.QComboBox):
	def wheelEvent (self, event):
		event.ignore()

class track_item(QtWidgets.QFrame):
	def __init__(self,ui,animLayer =None):
		self.ui = ui
		super(track_item, self).__init__()
		self.setFrameShape(QtWidgets.QFrame.Box)
		self.setFrameShadow(QtWidgets.QFrame.Sunken)
		self.setLineWidth(2)
		self.setColor("lightGreen")

		widgetLayout = QtWidgets.QVBoxLayout()
		spacerItem = QtWidgets.QSpacerItem(15, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

		sourceLayout =  QtWidgets.QHBoxLayout()
		sourceLabel =  QtWidgets.QLabel("Source : ")
		self.sourceComboBox = ComboBoxNoWheel()
		sourceLayout.addWidget(sourceLabel)
		sourceLayout.addWidget(self.sourceComboBox)
		sourceLayout.setStretch(1,1)
		sourceLayout.setStretch(2,1)

		sourceFileLayout =  QtWidgets.QHBoxLayout()
		sourceFileTopic =  QtWidgets.QLabel("Source File: ")
		self.sourceFileLabel = QtWidgets.QLabel() 
		sourceFileLayout.addWidget(sourceFileTopic)
		sourceFileLayout.addWidget(self.sourceFileLabel)
		sourceFileLayout.setStretch(1,1)
		sourceFileLayout.setStretch(2,1)


		animLayout =  QtWidgets.QHBoxLayout()
		animLabel =  QtWidgets.QLabel("AnimType : ")
		self.animComboBox = ComboBoxNoWheel()
		animLayout.addWidget(animLabel)
		animLayout.addWidget(self.animComboBox)
		animLayout.setStretch(1,1)
		animLayout.setStretch(2,1)

		sqLayout =  QtWidgets.QHBoxLayout()
		self.sqLabel =  QtWidgets.QLabel("Episode : ")
		self.sqComboBox = ComboBoxNoWheel()
		sqLayout.addWidget(self.sqLabel)
		sqLayout.addWidget(self.sqComboBox)

		#shotLayout =  QtWidgets.QHBoxLayout()
		self.shotLabel =  QtWidgets.QLabel("Shot : ")
		self.shotComboBox = ComboBoxNoWheel()
		sqLayout.addWidget(self.shotLabel)
		sqLayout.addWidget(self.shotComboBox)
		
		sqLayout.setStretch(1,1)
		sqLayout.setStretch(2,0)
		sqLayout.setStretch(3,1)

		timeLayout = QtWidgets.QHBoxLayout()
		startLabel = QtWidgets.QLabel("Start : ")
		self.startLine = QtWidgets.QLineEdit()
		endLabel = QtWidgets.QLabel("End : ")
		self.endLine = QtWidgets.QLineEdit()
		timeLayout.addWidget(startLabel)
		timeLayout.addWidget(self.startLine)
		timeLayout.addWidget(endLabel)
		timeLayout.addWidget(self.endLine)

		# lockLayout = QtWidgets.QHBoxLayout()
		# self.autoFrame = QtWidgets.QCheckBox("Auto Fill")
		# lockLayout.addWidget(self.autoFrame)
		# self.lockWidget = QtWidgets.QCheckBox("Lock settting")
		# lockLayout.addWidget(self.lockWidget)
		# self.autoFrame.setChecked(1)

		widgetLayout.addLayout(sourceLayout)
		widgetLayout.addLayout(sourceFileLayout)
		widgetLayout.addLayout(animLayout)
		widgetLayout.addLayout(sqLayout)
		#widgetLayout.addLayout(shotLayout)
		widgetLayout.addLayout(timeLayout)
		#widgetLayout.addLayout(lockLayout)
		#widgetLayout.addWidget(line)
		widgetLayout.addStretch()
		self.ui.projectWidget.projectComboBox.activated.connect(self.change_project)
		if animLayer == None:
			self.create_anim_layer()
		else:
			
			self.anim_layer = animLayer

		self.track_item_init()
		self.setLayout(widgetLayout)

	def track_item_init(self):
		self.hikSource()
		self.sqShot_active()
		self.animComboBox.activated.connect(self.sqShot_active)
		self.add_sequence()
		self.animComboBox.activated.connect(self.add_sequence)
		self.sqComboBox.activated.connect(self.add_shot)
		self.startLine.textChanged.connect(self.task_metaData)
		self.endLine.textChanged.connect(self.task_metaData)
		#self.lockWidget.toggled.connect(self.disableQFrame)
		self.createStartEnd()
		self.sourceComboBox.activated.connect(self.hik_set)

	def hikSource(self):
		hik_node = hik.sourceHikNode()
		
		get_hik = []
		for name in hik_node.iterkeys():
			get_hik.append(name)
		get_hik.sort()
		self.sourceComboBox.addItems(get_hik)
		animCbb = ["shot", "asset"]
		self.animComboBox.addItems(animCbb)
	
	def hik_set(self):
		hikName = self.sourceComboBox.currentText()
		ns = ':'.join(hikName.split(':')[:-1])
		ref_jnt = mc.ls('{}:*'.format(ns),assemblies=1)[0]
		#ref_jnt = ns+':Reference'

		if mc.objExists(ref_jnt+'.file_path'):
			file_path = mc.getAttr(ref_jnt+'.file_path')
		else:
			file_path = 'This motion capture data is from old version.' 

		self.sourceFileLabel.setText(file_path)
		self.createStartEnd()


	def sqShot_active(self):
		mode = self.animComboBox.currentText()
		if mode =='asset':
			self.hide_disable_widget(self.sqComboBox)
			self.hide_disable_widget(self.shotComboBox)
			self.hide_disable_widget(self.sqLabel)
			self.hide_disable_widget(self.shotLabel)
			self.sqComboBox.clear()
			self.shotComboBox.clear()

		elif mode =='shot':
			self.show_enable_widget(self.sqComboBox)
			self.show_enable_widget(self.shotComboBox)
			self.show_enable_widget(self.sqLabel)
			self.show_enable_widget(self.shotLabel)

		self.task_metaData()

	def hide_disable_widget(self,qtWidget):
		qtWidget.setDisabled(1)

	def show_enable_widget(self,qtWidget):
		qtWidget.setEnabled(1)

	def add_sequence(self):
		if self.animComboBox.currentText() == "asset":
			self.sqComboBox.clear()
		elif self.animComboBox.currentText() == "shot":
			self.sqComboBox.clear()
			get_proj = self.ui.projectWidget.projectComboBox.currentText()
			shot_context = context_info.Context()
			shot_context.update(project=get_proj , entityType='scene')
			scene = context_info.ContextPathInfo(context=shot_context)
			self.sq_list = fu.listFolder(scene.path.publish().abs_path())
			#print self.sq_list
			self.sqComboBox.addItems(self.sq_list)

		self.add_shot()
		self.task_metaData()

	def add_shot(self):
		if self.animComboBox.currentText() == "asset":
			self.shotComboBox.clear()
		elif self.animComboBox.currentText() == "shot":
			self.shotComboBox.clear()
			get_proj = self.ui.projectWidget.projectComboBox.currentText()
			get_sq = self.sqComboBox.currentText()
			
			sq_context = context_info.Context()
			sq_context.update(project=get_proj , entityType='scene',entityGrp = get_sq)
			get_sq = context_info.ContextPathInfo(context=sq_context)
			get_shot = fu.listFolder(get_sq.path.scheme(key='publishEpisodePath').abs_path())
			self.shotComboBox.addItems(get_shot)

			for idx,shot in enumerate(get_shot):
				get_sq.context.update(entity = shot)
				path = get_sq.path.scheme(key = 'publishShotPath').abs_path()
				#print path
				self.shotComboBox.setItemData(idx,path,QtCore.Qt.UserRole)

		self.task_metaData()

	def createStartEnd(self):
		proj = self.sourceComboBox.currentText()
		print proj
		if proj:
			#name = proj.replace('Character1','Reference')
			ns = proj.split(':')[0]
			name = mc.ls('{}:*'.format(ns),assemblies=1)[0]
			startF =  math.ceil(mc.findKeyframe(name, w="first"))
			endF =  math.ceil(mc.findKeyframe(name, w="last"))

			self.startLine.setText(str(startF))
			self.endLine.setText(str(endF))
		self.task_metaData()

	def change_project(self):
		self.add_sequence()

	def disableQFrame(self):
		is_check = self.lockWidget.isChecked()
		
		if is_check == True:
			self.sourceComboBox.setDisabled(1)
			self.animComboBox.setDisabled(1)
			self.sqComboBox.setDisabled(1)
			self.shotComboBox.setDisabled(1)
			self.startLine.setDisabled(1)
			self.endLine.setDisabled(1)
			
		elif is_check == False:
			mode = self.animComboBox.currentText()
			if mode =='asset':
				self.sourceComboBox.setEnabled(1)
				self.animComboBox.setEnabled(1)
				self.sqComboBox.setDisabled(1)
				self.sqComboBox.setDisabled(1)
				self.startLine.setEnabled(1)
				self.endLine.setEnabled(1)
			elif mode =='shot':
				self.sourceComboBox.setEnabled(1)
				self.animComboBox.setEnabled(1)
				self.sqComboBox.setEnabled(1)
				self.shotComboBox.setEnabled(1)
				self.startLine.setEnabled(1)
				self.endLine.setEnabled(1)

	def setColor(self, color):
		pal = self.palette()
		pal.setColor(QtGui.QPalette.Window, QtGui.QColor(color))
		self.setPalette(pal)

	def set_source(self):
		source = sourceComboBox.currentText()
		hikd.set_hik_source(self.ui.char_name, source)


	def create_anim_layer(self):
		mc.select('hik_ctrl')
		self.anim_layer = mc.animLayer(aso =1 ,o=1,m=1)
		mc.select(d=1)

	def task_metaData(self):
		data = pickle.dumps(self.ui.setAnimData(self))
		if not mc.objExists(self.anim_layer+'.taskData'):
			mc.addAttr(self.anim_layer,ln='taskData', dt='string')
		mc.setAttr(self.anim_layer+'.taskData',data,type ='string')
		
	def str_sel_comboBox(self,comboBox,str_sel):
		idx = comboBox.findText(str_sel)
		#print idx
		
		if idx != -1:
			comboBox.setCurrentIndex(idx)
			#comboBox.completer()
			return True
		else:
			return False

	@classmethod
	def load_metaData(cls,ui,anim_layer,meta_data):
		task_obj = cls(ui,anim_layer)
		#print meta_data
		task_obj.str_sel_comboBox(task_obj.sourceComboBox,meta_data['source'])
		task_obj.str_sel_comboBox(task_obj.animComboBox,meta_data['animType'])
		task_obj.sqShot_active()
		if meta_data['animType'] == 'shot': 
			task_obj.add_sequence()
			task_obj.str_sel_comboBox(task_obj.sqComboBox,meta_data['sequence'])
			task_obj.add_shot()
			task_obj.str_sel_comboBox(task_obj.shotComboBox,meta_data['shot'])

		task_obj.startLine.setText(meta_data['startEnd'][0])
		task_obj.endLine.setText(meta_data['startEnd'][1])

		return task_obj



def show(mode='default'):
	if config.isMaya:
		from rftool.utils.ui import maya_win
		logger.info('Run in Maya\n')
		maya_win.deleteUI(uiName)
		myApp = Bake(maya_win.getMayaWindow())
		myApp.show()
		return myApp

if __name__ == '__main__':
	show()

def add_attr(obj,attr_name = '' ,**kwargs):
	obj = pmc.PyNode(obj)
	if not pmc.objExists('{}.{}'.format(obj.name,attr_name)):
		obj.addAttr(attr_name,**kwargs)

def add_lock_attr(obj,attr_name = '',**kwargs):
	obj = pmc.PyNode(obj)
	if not pmc.objExists('{}.{}'.format(obj.name,attr_name)):
		add_attr(obj,attr_name,k=1,dv=0)
		obj.attr(attr_name).lock()