# v.0.0.1 polytag switcher
_title = 'Template App'
_version = 'v.0.0.1'
_des = 'Wip'
uiName = 'mocap library'

#Import python modules
import sys
import os
import logging
import getpass
import traceback
from collections import OrderedDict
import pymel.core as pmc
import os
import json
import pickle

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
import rf_config as config

# framework modules
from rf_utils import log_utils
from rf_utils.ui import load
reload(load)
from rf_utils.ui import stylesheet

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui
from rf_utils.widget import project_widget
from rf_utils.sg import sg_utils
from rf_utils import file_utils as fu
from rf_utils.context import context_info
from rf_utils.pipeline import file_info 
reload(file_info)

# maya & module function 
import pymel.core as pmc
import maya.cmds as mc
import maya.mel as mel


from rf_app.mocap.function.mocapData import ioData as iod
reload(iod)

from rf_app.mocap.function.mocapData import poseData as posd
reload(posd)

from rf_app.mocap.function.mocapData import ctrlData as ctrld
reload(ctrld)

from rf_app.mocap.function.mocapData import mocapData
reload(mocapData)

from rf_app.mocap.function.mocapData import hikList as hikl
reload(hikl)

from rf_app.mocap.function import fkiksnap
reload(fkiksnap)

import rf_app.mocap.function.hikDef as hikd
reload(hikd)

from rf_app.mocap.cutMocap import sourceHik
reload(sourceHik)

from rf_app.mocap.function import animData
reload(animData)

if not mc.pluginInfo('mayaHIK',loaded=1,q=1):
    mc.loadPlugin('mayaHIK')
mel.eval('HIKCharacterControlsTool')

class MocapLibrary(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(MocapLibrary, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        self.ui = load.setup_ui_maya(uiFile, parent)
        # set size 
        self.w = 500
        self.h = 600 

        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.resize(self.w, self.h)
        self.setWindowTitle('%s %s %s' % (_title, _version, _des))

        self.asset_list = ['']
        self.current_action_list = []
        self.projectWidget = project_widget.SGProjectComboBox(sg=sg_utils.sg, layout=QtWidgets.QHBoxLayout())
        self.projectWidget.setContentsMargins(0, 0,0, 0)
        self.projectWidget.allLayout.setStretch(1, 1)
        self.projectWidget.label.setText('1. Project:                 ')
        self.projectWidget.projectComboBox.setDisabled(1)
        self.projectWidget.projectComboBox.setStyleSheet( ":disabled {color: #BFFA37;}")
        #spacerItem = QtWidgets.QSpacerItem(10, 10, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.ui.asset_name.setStyleSheet( ":disabled {color: #BFFA37;}")
        #self.projectLayout.addItem(spacerItem)

        self.init_signals()
        self.projectWidget.projectComboBox.setCurrentText(self.mocap_char_project)
        self.ui.asset_name.setCurrentText(self.mocap_char_asset)
        self.confirm_dialog = AddWindow(self ,  convertUi = self)
        
        self.context = context_info.Context()
        self.context.use_sg(sg_utils.sg, self.project, 'asset', entityName = self.asset)
        self.asset_context = context_info.ContextPathInfo(context = self.context)
        self.all_mover_name = self.asset_context.projectInfo.asset.get('allMoverCtrl')
        self.get_scene_name()
    
    def init_signals(self): 
        """ connect signal widgets """ 
        self.ui.verticalLayout_3.addWidget(self.projectWidget,0)
        self.projectWidget.projectComboBox.activated.connect(self.get_project)
        self.ui.asset_name.currentTextChanged.connect(self.get_asset)
        self.ui.anim_type.activated.connect(self.get_type)
        self.ui.sequence_name.activated.connect(self.get_shot)
        #self.ui.search_name.returnPressed.connect(self.get_type)
        self.ui.search_name.textChanged.connect(self.get_type)
        self.ui.shot_name.activated.connect(self.get_anim_file)
        #self.ui.shot_name.activated.connect(self.filter_action_list)
        self.get_character()
        self.get_project()
        self.get_asset()
        #self.get_type()
        #self.get_shot()
        #self.ui.bake_button.clicked.connect(self.test_print)
        #self.ui.shot_name.currentIndexChanged.connect(self.shot_name_changed)
        #self.ui.listWidget.currentItemChanged.connect(self.action_selected)
        self.ui.change_button.clicked.connect(self.change_char)
        self.ui.bake_button.clicked.connect(self.import_anim_Dialog)

    def get_scene_name(self):
        path = pmc.system.sceneName().__str__()
        if path:
            self.ui.anim_type.setCurrentText('shot')
            self.get_type()
            scene = context_info.ContextPathInfo(path=path)
            self.ui.search_name.setText(scene.name)
        else:
            self.ui.anim_type.setCurrentText('asset')
            self.get_type()



    def get_project(self):
        # change self.project from project from comboBox
        self.project = self.projectWidget.projectComboBox.currentText()
        context = context_info.Context()
        context.update(project= self.project, entityType='asset', entityGrp ='char')
        asset_char = context_info.ContextPathInfo(context = context)
        # after change project it will change asset in combo box
        self.asset_list = []
        try: 
            if os.path.isdir(asset_char.path.scheme(key='publishAssetTypePath').abs_path()):
                filter_asset_list = fu.listFolder("{}".format(asset_char.path.scheme(key='publishAssetTypePath').abs_path()))

                shot_context = context_info.Context()
                shot_context.update(project = self.project , entityType = 'scene')
                scene = context_info.ContextPathInfo(context = shot_context)
                self.shot_path = scene.path.publish().abs_path()
                self.sq_list = fu.listFolder(scene.path.publish().abs_path())

                if filter_asset_list:
                    # filter asset that have mocap data
                    check_context = context_info.Context()
                    check_context.use_sg(sg_utils.sg, self.project, 'asset')
                    check_asset = context_info.ContextPathInfo(context = check_context) 
                    
                    
                    for asset in filter_asset_list:
                        check_asset.context.update(step='mocap', process='', res='', entity=asset,entityGrp = 'char')
                        check_mocap = check_asset.path.scheme(key='publishProcessPath').abs_path()
                        #print check_mocap
                        #if os.path.isdir(os.path.join("P:/%s/asset/publ/char/"%(self.project),asset,'mocap','data')):
                        if os.path.isdir(os.path.join("{}".format(check_mocap),'data')):
                            self.asset_list.append(asset)
                    self.ui.asset_name.clear()
                    self.ui.asset_name.addItems(self.asset_list)
                else:
                    self.asset_list = []
            else:
                self.asset_list = []
        except Exception as e:
            self.asset_list = []

        self.ui.asset_name.clear()
        self.ui.asset_name.addItems(self.asset_list)
        
    def get_asset(self):
        # change self.asset from asset commboBox
        self.asset = self.ui.asset_name.currentText()
        
        if self.asset:
            self.get_asset_path()
            self.get_type()
        else:
            self.ui.shot.setDisabled(1)
            self.ui.shot_name.setDisabled(1)
            self.ui.sequence.setDisabled(1)
            self.ui.sequence_name.setDisabled(1)
            self.ui.listWidget.clear()

    def get_asset_path(self):
        context = context_info.Context()
        context.use_sg(sg_utils.sg, self.project, 'asset')
        asset_context = context_info.ContextPathInfo(context = context)
        asset_context.context.update(step='mocap', process='', res='', entity=self.asset,entityGrp = 'char')
        self.asset_mocap_path = asset_context.path.scheme(key='publishProcessPath').abs_path()

        #self.shot_path = r"P:/%s/scene/publ"%(self.project)
        self.asset_anim_path = "{}/anim".format(self.asset_mocap_path)
        print self.asset_mocap_path
        print self.asset_anim_path

    def get_type(self):
        self.action_path_list = []
        self.types = self.ui.anim_type.currentText()
        # create action_path_list and update action list widget
        if self.types == 'asset':
            # self.ui.shot.hide()
            # self.ui.shot_name.hide()
            # self.ui.sequence.hide()
            # self.ui.sequence_name.hide()
            self.ui.shot.setDisabled(1)
            self.ui.shot_name.setDisabled(1)
            self.ui.sequence.setDisabled(1)
            self.ui.sequence_name.setDisabled(1)
            #self.action_path_list.append(self.asset_anim_path)
            self.action_file_list = []
            for action_file in fu.listFile(self.asset_anim_path):
                self.action_file_list.append([action_file, os.path.join(self.asset_anim_path,action_file).replace('\\','/')])
            
            self.filter_action_list()
        
        elif self.types =='shot':
            self.ui.shot.setEnabled(1)
            self.ui.shot_name.setEnabled(1)
            self.ui.sequence.setEnabled(1)
            self.ui.sequence_name.setEnabled(1)
            self.get_sequence()

    def get_sequence(self):
        self.ui.sequence_name.clear()
        sq_list = fu.listFolder(self.shot_path)
        self.ui.sequence_name.addItem('all')
        all_sq_list = []
        self.ui.sequence_name.addItems(sq_list)
        num = 1
        
        for sq in sq_list:
            sq_data = os.path.join(self.shot_path,sq).replace('\\','/')
            all_sq_list.append(sq_data)
            self.ui.sequence_name.setItemData(num,[sq_data],QtCore.Qt.UserRole)
            num+=1
        
        self.ui.sequence_name.setItemData(0,all_sq_list,QtCore.Qt.UserRole)
        #print all_sq_list
        #self.ui.sequence_name.activated.connect(self.test_print)   
        self.get_shot()

    def get_shot(self):
        self.ui.shot_name.clear()
        sq_list = self.ui.sequence_name.currentData()
        item_shot_list = []
        self.ui.shot_name.addItem('all')
        

        shot_path_list = []
        for sq_path in sq_list:
            shot_list = fu.listFolder(sq_path)
            for shot in shot_list:
                item_shot_list.append(shot)
                shot_path_list.append(os.path.join(sq_path,shot))
        num=1

        all_shot_list = []
    
        for shot_num,shot in enumerate(item_shot_list):
            shot_data = os.path.join(shot_path_list[shot_num])
            if 'mocap' in fu.listFolder(shot_data):
                mocap_path = os.path.join(shot_data,'mocap').replace('\\','/')
                if self.asset in fu.listFolder(mocap_path):
                    mocap_shot_asset = os.path.join(mocap_path,self.asset).replace('\\','/')
                    if fu.listFile(mocap_shot_asset):
                        all_shot_list.append(shot_data)
                        self.ui.shot_name.addItem(shot)
                        shot_data_list =  [shot_data]
                        self.ui.shot_name.setItemData(num,shot_data_list,QtCore.Qt.UserRole)
                        num+=1
        
        self.ui.shot_name.setItemData(0,all_shot_list,QtCore.Qt.UserRole)
        
        #self.ui.sequence_name.activated.connect(self.test_print)                
        self.get_anim_file()

    def get_anim_file(self):
        # get all action file from self.action_path_list
        shot_data_list = self.ui.shot_name.currentData()
        #print shot_data_list
        self.action_file_list = []
        

        for shot_path in shot_data_list:
            shot_mocap_path = os.path.join(shot_path,'mocap',self.asset).replace('\\','/')
            files = fu.listFile(shot_mocap_path)
            
            for action_file in files:
                action_data = os.path.join(shot_mocap_path,action_file).replace('\\','/')
                self.action_file_list.append([action_file,action_data])
        
        self.filter_action_list()
        
        

    def filter_action_list(self):
        self.filter = self.ui.search_name.text()
        self.current_action_list = []
        #print self.filter
        if self.action_file_list:
            for action in self.action_file_list:
                if not self.filter:
                    self.current_action_list.append(action)
                else:
                    if self.filter in action[0]:
                        self.current_action_list.append(action)
        
        self.show_anim_file()

 
    def show_anim_file(self):
        self.ui.listWidget.clear()
        for action in self.current_action_list:
            item = QtWidgets.QListWidgetItem(self.ui.listWidget)
            item.setText(action[0])
            item.setData(QtCore.Qt.UserRole, action[1])


    def test_print(self):
        item = self.ui.listWidget.currentItem()
        #print item.data(QtCore.Qt.UserRole)
 

    def get_project_asset(self,path):
        project = path.split('/')[1]
        asset = path.split('/')[5]
        return project,asset

    def get_character(self):
        sel_list = pmc.selected()
        if sel_list:
            character = pmc.selected()[0]
        else:
            mc.confirmDialog(m='please select character.')
            return

        self.sel = character
        ref_file = character.referenceFile().path
        f = file_info.File(ref_file)
        path = f.redirect()
        #print path
        self.mocap_char_project,self.mocap_char_asset = self.get_project_asset(path)

        self.ns = character.namespace()
        
        proj_idx = [self.projectWidget.projectComboBox.itemText(i) for i in range(self.projectWidget.projectComboBox.count())].index(self.mocap_char_project)
        self.projectWidget.projectComboBox.setCurrentIndex(proj_idx)
        self.get_project()
        
        if self.mocap_char_asset in self.asset_list:
            asset_idx = [self.ui.asset_name.itemText(i) for i in range(self.ui.asset_name.count())].index(self.mocap_char_asset)
            self.ui.asset_name.setCurrentIndex(asset_idx)
            self.get_asset()
        else:
            mc.confirmDialog(m='There is no baked mocap for "{}" yet'.format(self.mocap_char_asset))
            self.get_asset()
            return
 

    def import_anim_Dialog(self):
        # get ma file from select action widget 
        self.import_path = self.ui.listWidget.currentItem().data(QtCore.Qt.UserRole)
        #self.confirm_dialog.show()
        all_mover = self.ns + self.all_mover_name
        time_offset = mc.currentTime(q=1)
        self.import_char_anim(all_mover, self.import_path , time_offset)
    
    def import_char_anim(self,all_mover,path,time_offset):
        sourceHik.import_anim(all_mover ,path,time_offset)
        #sourceHik.test()


    def change_char(self):
        self.get_character()
        self.get_project()
        self.get_asset()        
        self.projectWidget.projectComboBox.setCurrentText(self.mocap_char_project)
        self.ui.asset_name.setCurrentText(self.mocap_char_asset)
        
        self.context.use_sg(sg_utils.sg, self.project, 'asset', entityName = self.asset)
        self.asset_context = context_info.ContextPathInfo(context = self.context)
        self.all_mover_name = self.asset_context.projectInfo.asset.get('allMoverCtrl')


class AddWindow(QtWidgets.QDialog):
    def __init__(self, parent=None, convertUi = ''):
        super(AddWindow, self).__init__(parent)
        text = QtWidgets.QLabel("Please Select All_Mover Ctrl of character.")
        self.okButton = QtWidgets.QPushButton("OK")
        self.convertUi = convertUi
        addLayout = QtWidgets.QHBoxLayout()
        addLayout.addWidget(text)
        addLayout.addWidget(self.okButton)

        self.resize(250, 50)
        self.setLayout(addLayout)
        
        self.okButton.clicked.connect(self.closeAddWindow)
        
    def closeAddWindow(self):
        all_mover = self.convertUi.ns + self.convertUi.all_mover_name
        if all_mover:
            #all_mover = all_mover[0].name()
            print all_mover
            time_offset = mc.currentTime(q=1)
            self.convertUi.import_char_anim(all_mover,self.convertUi.import_path,time_offset)
            #sourceHik.import_anim(all_mover,self.convertUi.import_path,time_offset)
        else:
            mc.confirmDialog('please select All_Mover dumbass')
        
        self.close() 
           
def show(mode='default'):
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = MocapLibrary(maya_win.getMayaWindow())
        myApp.show()
        return myApp


if __name__ == '__main__':
    show()


# ##################### ruin of script ##################################
#    def get_type(self):
#         # get type to filter file from asset or scene
#         self.action_path_list = []
#         self.types = self.ui.anim_type.currentText()

#         # each type will give different action path list to find action
#         # asset type will show only loop animation action of that character
#         if self.types == 'asset':
#             self.ui.shot.hide()
#             self.ui.shot_name.hide()
#             self.action_path_list.append(self.asset_anim_path.replace('\\','/'))
#             #print self.action_path_list
#             self.action_from_assetNShot()
        
#         # all shot type will show all animation that character has in every shot in this project
#         elif self.types == 'all shot':
#             self.ui.shot.show()
#             self.ui.shot_name.show()
            
#             # get all sq folder in this project
#             sq_path_list = []
#             self.shot_name_list = []
#             self.ui.shot_name.clear()
#             self.ui.shot_name.setDisabled(1)
#             self.ui.shot_name.addItem('all')
            
#             i = 1 
#             for sq_name in fu.listFolder(self.shot_path):
#                 #print sq_name
#                 sq_path_list = ( os.path.join(self.shot_path,sq_name))
#                 #print sq_path_list
#                 if sq_path_list:
#                 # get all shot from sq folder
#                     #for sq_folder in sq_path_list:
#                     #print 'sqFolder:%s'%sq_folder
#                     # get action from specific asset in shot folder
#                     for shot_name in fu.listFolder(sq_path_list):
#                         #print 'shotName:%s'%shot_name
#                         shot_path_list = os.path.join(sq_path_list,shot_name,self.asset)
#                         #print 'shot_path_list:%s'%shot_path_list
#                         if os.path.isdir(shot_path_list):
#                             self.ui.shot_name.addItem(shot_name)
#                             self.ui.shot_name.setItemData(i, shot_path_list.replace('\\','/'), QtCore.Qt.UserRole)
#                             self.action_path_list.append(shot_path_list.replace('\\','/'))
#                             i+=1
#                             #print '%s  :  %s  :  %s'%(sq_folder , shot_name,self.action_path_list)
#                 #print self.action_path_list
#             self.ui.shot_name.setItemData(0, self.action_path_list, QtCore.Qt.UserRole)
#             self.ui.shot_name.setEnabled(1)
#             self.shot_name_changed()

#         # shot will show only animation that character has in current shot
#         elif self.types == 'shot':
#             self.ui.shot.hide()
#             self.ui.shot_name.hide()
#             shot_path_list = os.path.join(self.cur_shot_publ_path,self.asset)
#             self.action_path_list.append(shot_path_list.replace('\\','/'))
#             #print self.action_path_list
#             self.action_from_assetNShot()
#         #self.get_action_list()
        
    
#     def action_from_assetNShot(self):
#         #shot = self.ui.shot_name.currentText()
#         currentPath = self.action_path_list
#         self.search = self.ui.search_name.displayText().lower()
#         actions = []
#         #print currentPath
#         if currentPath:
#             for path in currentPath:
#                 #print path
#                 actions.append(path) 
#         self.ui.listWidget.clear()
        
#         if actions:
#             for path in actions:
#                if fu.listFolder(path):
#                     for action in fu.listFolder(path): 
#                         if self.search:
#                             if self.search in action.lower():
#                                 item = QtWidgets.QListWidgetItem(self.ui.listWidget)
#                                 item.setText(action)
#                                 item.setData(QtCore.Qt.UserRole, '%s/%s' % (path, action))
#                         else:
#                             item = QtWidgets.QListWidgetItem(self.ui.listWidget)
#                             item.setText(action)
#                             item.setData(QtCore.Qt.UserRole, '%s/%s' % (path, action))  

#     def shot_name_changed(self): 
#         """ list action and fill to action widget """ 
#         index = self.ui.shot_name.currentIndex()
#         currentPath = self.ui.shot_name.itemData(index, QtCore.Qt.UserRole)
#         self.search = self.ui.search_name.displayText().lower()
#         actions = []
#         if index == 0: 
#             if currentPath != None:
#                 for path in currentPath: 
#                     actions.append(path)
#                 self.ui.listWidget.clear()
                
#                 if actions:
#                     for path in actions:
#                         if fu.listFolder(path):
#                             for action in fu.listFolder(path): 
#                                 if self.search:
#                                     if self.search in action.lower():
#                                         item = QtWidgets.QListWidgetItem(self.ui.listWidget)
#                                         item.setText(action)
#                                         item.setData(QtCore.Qt.UserRole, '%s/%s' % (path, action))
#                                 else:
#                                     item = QtWidgets.QListWidgetItem(self.ui.listWidget)
#                                     item.setText(action)
#                                     item.setData(QtCore.Qt.UserRole, '%s/%s' % (path, action))  
#         else: 
#             actions = fu.listFolder(currentPath)

#             self.ui.listWidget.clear()
#             for path in currentPath:
#                 if actions:
#                     for action in actions: 
#                         if self.search:
#                             if self.search in action.lower():
#                                 item = QtWidgets.QListWidgetItem(self.ui.listWidget)
#                                 item.setText(action)
#                                 item.setData(QtCore.Qt.UserRole, '%s/%s' % (currentPath, action))
#                         else:
#                             item = QtWidgets.QListWidgetItem(self.ui.listWidget)
#                             item.setText(action)
#                             item.setData(QtCore.Qt.UserRole, '%s/%s' % (currentPath, action))                       


#     def action_selected(self): 
#         """ what happen when click action """ 
#         item = self.ui.listWidget.currentItem()
#         self.fbx_file_name =  item.text()
#         if item:
#             data = item.data(QtCore.Qt.UserRole)
#             self.action_folder_path = data


#     def get_action_list(self):
#         # query action folder from all arugement
#         self.action_folder_name_list = []
#         self.action_folder_path_list = []
#         for path in self.action_path_list:
#             action_folder_list = fu.listFile(path)
#             if action_folder_list:
#                 for action_folder in action_folder_list:
#                     self.action_folder_name_list.append(action_folder)
#                     self.action_folder_path_list.append(os.path.join(path,action_folder).replace('\\','/'))
    

#     def get_shot(self):
#         scene_name = context_info.ContextPathInfo()
#         if scene_name.valid:
#             self.shot = scene_name.name
#             shot_list = scene_name.path.name().split('/')[1:6]
#             shot_list[2] = 'publ'
#             self.cur_shot_publ_path = 'P:/%s'%('/'.join(shot_list))
#             self.ui.anim_type.setCurrentIndex(1)
#             self.get_type()
#         else:
#             self.shot =''
#             self.sq = ''
#             self.ui.anim_type.clear()
#             self.ui.anim_type.addItems(['asset','shot'])
#             self.ui.anim_type.setCurrentIndex(1)
#             self.get_type()