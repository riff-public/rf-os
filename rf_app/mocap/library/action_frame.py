# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'action_frame.ui'
#
# Created: Fri Apr 17 11:13:57 2020
#      by: PyQt4 UI code generator 4.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_action_list(object):
    def setupUi(self, action_list):
        action_list.setObjectName(_fromUtf8("action_list"))
        action_list.resize(755, 228)
        self.horizontalLayout_3 = QtGui.QHBoxLayout(action_list)
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.action_all_layout = QtGui.QHBoxLayout()
        self.action_all_layout.setObjectName(_fromUtf8("action_all_layout"))
        self.thumb_pic = QtGui.QGraphicsView(action_list)
        self.thumb_pic.setObjectName(_fromUtf8("thumb_pic"))
        self.action_all_layout.addWidget(self.thumb_pic)
        spacerItem = QtGui.QSpacerItem(20, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.action_all_layout.addItem(spacerItem)
        self.action_layout = QtGui.QGridLayout()
        self.action_layout.setObjectName(_fromUtf8("action_layout"))
        self.des_text = QtGui.QLabel(action_list)
        self.des_text.setObjectName(_fromUtf8("des_text"))
        self.action_layout.addWidget(self.des_text, 2, 1, 1, 1)
        self.frame_text = QtGui.QLabel(action_list)
        self.frame_text.setObjectName(_fromUtf8("frame_text"))
        self.action_layout.addWidget(self.frame_text, 1, 1, 1, 1)
        self.frame_label = QtGui.QLabel(action_list)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.frame_label.setFont(font)
        self.frame_label.setObjectName(_fromUtf8("frame_label"))
        self.action_layout.addWidget(self.frame_label, 1, 0, 1, 1)
        self.des_label = QtGui.QLabel(action_list)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.des_label.setFont(font)
        self.des_label.setObjectName(_fromUtf8("des_label"))
        self.action_layout.addWidget(self.des_label, 2, 0, 1, 1)
        self.name_text = QtGui.QLabel(action_list)
        self.name_text.setObjectName(_fromUtf8("name_text"))
        self.action_layout.addWidget(self.name_text, 0, 1, 1, 1)
        self.name_label = QtGui.QLabel(action_list)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.name_label.setFont(font)
        self.name_label.setObjectName(_fromUtf8("name_label"))
        self.action_layout.addWidget(self.name_label, 0, 0, 1, 1)
        self.action_layout.setColumnStretch(1, 1)
        self.action_layout.setRowStretch(0, 1)
        self.action_layout.setRowStretch(1, 1)
        self.action_layout.setRowStretch(2, 2)
        self.action_all_layout.addLayout(self.action_layout)
        self.action_all_layout.setStretch(2, 1)
        self.horizontalLayout_3.addLayout(self.action_all_layout)

        self.retranslateUi(action_list)
        QtCore.QMetaObject.connectSlotsByName(action_list)

    def retranslateUi(self, action_list):
        action_list.setWindowTitle(_translate("action_list", "Form", None))
        self.des_text.setText(_translate("action_list", "TextLabel", None))
        self.frame_text.setText(_translate("action_list", "TextLabel", None))
        self.frame_label.setText(_translate("action_list", "frame :", None))
        self.des_label.setText(_translate("action_list", "description :", None))
        self.name_text.setText(_translate("action_list", "TextLabel", None))
        self.name_label.setText(_translate("action_list", "name :", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    action_list = QtGui.QWidget()
    ui = Ui_action_list()
    ui.setupUi(action_list)
    action_list.show()
    sys.exit(app.exec_())

