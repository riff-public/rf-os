#fkIkDict = {}
import maya.OpenMaya as om
import maya.cmds as mc
import maya.OpenMayaAnim as oma
import copy
import pymel.core as pmc

def get_polevector(start, mid, end, offsetPoleVec=2):
	x = lambda vec, loc : mc.move(vec.x, vec.y, vec.z, loc)
	pos = mc.xform(start, q=1, ws=1, t=True)
	a = om.MVector(pos[0], pos[1], pos[2])
	pos2 = mc.xform(end, q=1, ws=1, t=True)
	b= om.MVector(pos2[0], pos2[1], pos2[2])
	pos3 = mc.xform(mid, q=1, ws=1, t=True)
	c = om.MVector(pos3[0], pos3[1], pos3[2])
	i = b-a
	d= i* .5
	e = a+d
	f = c-e
	g = f *2
	h = e+g
	return (h.x, h.y, h.z)

def ik_to_fk( fk_ctrl_list , skin_joint_list):
	for num in range(len(skin_joint_list)):
		mat = mc.xform(skin_joint_list[num], q=1, ws=1, m=1)
		mc.xform(fk_ctrl_list[num] , ws=1, m=mat)
	# if fk_default:
	# 	mc.setAttr('%s.%s'%(ctrl,attr),0)
	# elif fk_default:
	# 	mc.setAttr('%s.%s'%(ctrl,attr),1)
	
def fk_to_ik( ik_ctrl_list, skin_joint_list ,ctrl, attr ,fk_default):
	# get tip
	skin_ra = mc.xform(skin_joint_list[-1], q=1, ra=1)
	ik_ra = mc.xform(ik_ctrl_list[-1], ra=1, q=1)
	tip_pv_dif = ( skin_ra[0]-ik_ra[0] , skin_ra[1]-ik_ra[1], skin_ra[2]- ik_ra[2])

	mc.matchTransform(ik_ctrl_list[-1],skin_joint_list[-1])
	mc.rotate(-tip_pv_dif[0], -tip_pv_dif[1], -tip_pv_dif[2], ik_ctrl_list[-1], r=1)
	
	#get base
	base_mat = mc.xform(skin_joint_list[0],ws=1,piv=1,q=1)
	skin_ra = mc.xform(skin_joint_list[0],q=1,ra=1)
	ik_ra = mc.xform(ik_ctrl_list[0],ra=1,q=1)
	
	base_pv_dif = (skin_ra[0]-ik_ra[0] ,skin_ra[1]-ik_ra[1], skin_ra[2]-ik_ra[2])
	mc.matchTransform(ik_ctrl_list[0],skin_joint_list[0])
	mc.rotate(-base_pv_dif[0], -base_pv_dif[1], -base_pv_dif[2], ik_ctrl_list[0], r=1)
	mc.xform(ik_ctrl_list[0],ws=1,piv= (base_mat[0],base_mat[1],base_mat[2]))

	# ge pole vector
	t = get_polevector(skin_joint_list[0], skin_joint_list[1], skin_joint_list[2])
	mc.xform(ik_ctrl_list[1],ws=1,t=t)


def create_fkik_snap_dict(swicth_ctrl = '', attr = 'fkIk', fk_list = [],ik_list = 
						[],skin_list = [], fk_at = 0):
	snap_dict = {}

	snap_dict["ctrl"] = swicth_ctrl
	snap_dict["attr"] = attr
	snap_dict["fk_ctrl"] = fk_list
	snap_dict["ik_ctrl"] = ik_list
	snap_dict["skin_jnt"] = skin_list
	snap_dict["fk_at"] = fk_at

	return snap_dict

def key_anim_crv(anim_crv,time,value):
	sel = om.MSelectionList()
	sel.add('pCube1_translateX')
	mObj = om.MObject()
	sel.getDependNode(0,mObj)
	animCrv = oma.MFnAnimCurve(mObj)
	animCrv.addKeyframe(om.MTime(time),value)

def collect_fkik_dict(key_list= [],dict_list = []):
	if len(key_list) == len(dict_list):
		fkik_dict = {}
		for key in key_list:
			fkik_dict[key] = dict_list[key]
	return fkik_dict


def add_dup_ik_ctrl(ns,fkIk_dict):
	fkIk_dict_copy = copy.deepcopy(fkIk_dict)
	
	for fkIk_key in fkIk_dict.keys():
		part_dict = fkIk_dict[fkIk_key]
		part_dict_copy = fkIk_dict_copy[fkIk_key]
		part_dict['dup_ik_ctrl'] = []

		for num,fk_ctrl in enumerate(part_dict_copy['fk_ctrl']):
			part_dict_copy['fk_ctrl'][num] = ('%s%s'%(ns,fk_ctrl))
		
		for num,ik_ctrl in enumerate(part_dict_copy['ik_ctrl']):
			part_dict_copy['ik_ctrl'][num] = ('%s%s'%(ns,ik_ctrl))
			part_dict['dup_ik_ctrl'].append(mc.duplicate('%s%s'%(ns,ik_ctrl),po=1)[0])
	
		for obj in part_dict['dup_ik_ctrl']:
			pmc.PyNode(obj).t.unlock()
			pmc.PyNode(obj).r.unlock()
			pmc.PyNode(obj).s.unlock()
		
			for attr in 'tx','ty','tz','rx','ry','rz','sx','sy','sz','v':
				pmc.PyNode(obj).attr(attr).unlock()
				
			pmc.PyNode(obj).v.set(1)
			pmc.PyNode(obj).setParent(w=1)
	
	for fkIk_key in fkIk_dict.keys():
		part_dict = fkIk_dict[fkIk_key]
		part_dict_copy = fkIk_dict_copy[fkIk_key]
		for num in range(len(part_dict['dup_ik_ctrl'])):
			pmc.parentConstraint( part_dict_copy['fk_ctrl'][num] , part_dict['dup_ik_ctrl'][num],mo=1)

	return fkIk_dict

def fk_to_Ik_all(ns, fkIk_dict,frame_range,animLayer= ''):
	fkIk_dict = copy.deepcopy(fkIk_dict)
	all_ik_ctrl =[]
	all_dup_ctrl = []

	kf = []
	for fkIk_key in fkIk_dict.keys():
		part_dict = fkIk_dict[fkIk_key]
		ns_fk_ctrl =[]
		ns_ik_ctrl =[]
		ns_skin_jnt =[]
		part_dict['ctrl'] = '%s%s'%(ns,part_dict['ctrl'])
		#part_dict['dup_ik_ctrl'] = []
		
		for num,fk_ctrl in enumerate(part_dict['fk_ctrl']):
			part_dict['fk_ctrl'][num] = ('%s%s'%(ns,fk_ctrl))
			
		for num,ik_ctrl in enumerate(part_dict['ik_ctrl']):
			part_dict['ik_ctrl'][num] = ('%s%s'%(ns,ik_ctrl))
			#part_dict['dup_ik_ctrl'].append(mc.duplicate('%s%s'%(ns,ik_ctrl),po=1)[0])

		for num,skin_jnt in enumerate(part_dict['skin_jnt']):
			part_dict['skin_jnt'][num] = ('%s%s'%(ns,skin_jnt))

		#print 'part dict : %s' %(part_dict['dup_ik_ctrl'])

		kf = set(kf) | set(mc.keyframe(part_dict['fk_ctrl'],q=1,tc=1))
		kf = sorted(kf)

		all_ik_ctrl.extend(part_dict['ik_ctrl'])
		all_dup_ctrl.extend(part_dict['dup_ik_ctrl'])
	
	for keyFrame in kf:
		#print keyFrame

		#print frame_range[0]
		#print frame_range[1]
		#print frame_range
		# print 'keyFrame : %s' %(keyFrame)
		# print 'keyFrame >= start : %s'% (keyFrame >= frame_range[0])
		# print 'keyFrame <= start : %s'% (keyFrame <= frame_range[1])
		if (keyFrame >= frame_range[0]) and (keyFrame <= frame_range[1]):
			#print ' do keyFrame :  %s'%keyFrame
			mc.currentTime(keyFrame,update=1)
			for fkIk_key in fkIk_dict.keys():
				mc.matchTransform( fkIk_dict[fkIk_key]['ik_ctrl'][0] , fkIk_dict[fkIk_key]['dup_ik_ctrl'][0])
				mc.matchTransform( fkIk_dict[fkIk_key]['ik_ctrl'][-1] , fkIk_dict[fkIk_key]['dup_ik_ctrl'][-1])
				#fk_to_ik(fkIk_dict[fkIk_key]['ik_ctrl'], fkIk_dict[fkIk_key]['skin_jnt'], fkIk_dict[fkIk_key]['ctrl'], fkIk_dict[fkIk_key]['attr'],fkIk_dict[fkIk_key]['fk_default'])
				# ge pole vector
				t = get_polevector(fkIk_dict[fkIk_key]['skin_jnt'][0], fkIk_dict[fkIk_key]['skin_jnt'][1], fkIk_dict[fkIk_key]['skin_jnt'][2])
				mc.xform(fkIk_dict[fkIk_key]['ik_ctrl'][1],ws=1,t=t)		
			if animLayer:
				mc.setKeyframe(all_ik_ctrl,at = ['translate','rotate'],s=0,al = animLayer)
			else:
				mc.setKeyframe(all_ik_ctrl,at = ['translate','rotate'],s=0)
		else:
			print 'something wrong'
	#mc.delete(all_dup_ctrl)



# def ik_to_fk_all(ns, fkIk_dict):
# 	fkIk_dict = copy.deepcopy(fkIk_dict)
# 	all_fk_ctrl =[]


# 	kf = []
# 	for fkIk_key in fkIk_dict.keys():
# 		part_dict = fkIk_dict[fkIk_key]
		
# 		for num,fk_ctrl in enumerate(part_dict['fk_ctrl']):
# 			part_dict['fk_ctrl'][num] = ('%s%s'%(ns,fk_ctrl))
# 			all_fk_ctrl.append('%s%s'%(ns,fk_ctrl))
# 		for num,ik_ctrl in enumerate(part_dict['ik_ctrl']):
# 			part_dict['ik_ctrl'][num] = ('%s%s'%(ns,ik_ctrl))
# 			#part_dict['dup_ik_ctrl'].append(mc.duplicate('%s%s'%(ns,ik_ctrl),po=1)[0])

# 		for num,skin_jnt in enumerate(part_dict['skin_jnt']):
# 			part_dict['skin_jnt'][num] = ('%s%s'%(ns,skin_jnt))

# 		kf = set(kf) | set(mc.keyframe(part_dict['ik_ctrl'],q=1,tc=1))
# 		kf = sorted(kf)
	
# 	for keyFrame in kf:
# 		mc.currentTime(keyFrame,update=1)
# 		for fkIk_key in fkIk_dict.keys():
# 			if 'Arm' in fkIk_key:
# 				fk_ctrl_list = par_dict[fkIk_key]['fk_ctrl']
# 				skin_joint_list = par_dict[fkIk_key]['skin_jnt']	
# 				ik_to_fk(fk_ctrl_list,skin_joint_list)
# 				mc.setKeyframe(all_fk_ctrl,s=0)
	
