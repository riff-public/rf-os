import pymel.core as pmc
import maya.cmds as mc
import maya.mel as mm
import os
import json
import pickle

from mocapData import ioData as iod
reload(iod)

from mocapData import poseData as posd
reload(posd)

from mocapData import ctrlData as ctrld
reload(ctrld)

from mocapData import mocapData
reload(mocapData)

from mocapData import hikList as hikl
reload(hikl)

#from mocapData import char_list
#reload(char_list)

if not mc.pluginInfo('mayaHIK',loaded=1,q=1):
	mc.loadPlugin('mayaHIK')

class mocapRig(object):
	def __init__(self):
		self.winName = 'mocapRig'
		self.ns = ''
		self.joint_scroll_data_list =  []
		self.ctrl_scroll_data_list = []
	
	def adjColumn(self,*args):
		tabIdx = mc.tabLayout('tmpSetupTab',q=1,sti=1)
		
		if tabIdx ==1:
			mc.tabLayout('tmpSetupTab',e=1, w=580)
			mc.columnLayout('mainCol',e=1, w=580)
			mc.window(self.winName,e=1, w=580)
		
		elif tabIdx ==2:
			mc.tabLayout('tmpSetupTab',e=1, w=500)
			mc.columnLayout('mainCol',e=1, w=500)
			mc.window(self.winName,e=1, w=500)

	def get_ns(self,*args):
		self.ns = pmc.selected()[0].namespace()
		mc.textField('ns',e=1,tx=self.ns,bgc=[.5,1,.5])
		self.fld_path = mocapData.get_project_from_scene()
		self.tPose_file_name = 'tPose'
		self.char_list_name = 'char_list'
		self.proj, self.asset = mocapData.get_project_from_scene()
		self.fld_path = mocapData.get_mocap_folder(self.proj, self.asset)
		self.tPose_path = os.path.join(self.fld_path, self.tPose_file_name)
		self.char_path = os.path.join(self.fld_path, self.char_list_name)
		self.load_char_list()
		self.convert_list_to_scroll()
	

	def get_folder(self,*args):
		data_folder = mc.fileDialog2(ds=1,cap= 'Select Mocap Data File.',fileMode=3)
		if data_folder:
			check_folder(data_folder[0])
	

	def check_folder(self, folder_path=''):
		mc.textField('file_text',e=1,tx = folder_path)
		if os.path.isdir(folder_path):
			mc.textField('file_text',e=1 , bgc = [.5,1,.5])
		else:
			mc.textField('file_text',e=1,bgc=[1,.5,.5])


	# char list function
	# load char list function
	def load_char_list(self,*args):
		if os.path.exists(self.char_path):
			with open(self.char_path, 'r') as f:
				self.char_list = json.load(f)
		else:
			with open(os.path.join(os.path.dirname(__file__),'mocapData','char_list'),'r') as f:
				self.char_list = json.load(f)
		
		self.convert_list_to_scroll()

	# load default char list function
	def load_default_char_list(self,*args):
		with open(os.path.join(os.path.dirname(__file__),'mocapData','char_list'),'r') as f:
			self.char_list = json.load(f)
		self.convert_list_to_scroll()

	def load_from_file(self,*args):
		data = mc.fileDialog2(ds=1,cap= 'Select Mocap Data File.',fileMode=1)
		if data:
			with open(data[0],'r') as f:
				self.char_list = json.load(f)
			self.convert_list_to_scroll()

	# save char list function 
	def save_char_list(self):
		self.convert_scroll_to_list()
		if not os.path.isdir(self.fld_path):
			os.mkdir(self.fld_path)
		
		with open(self.char_path,'w') as f:
			json.dump(self.char_list, f)

	# scrollist function
	def convert_list_to_scroll(self):
		self.joint_scroll_data_list =  []
		self.ctrl_scroll_data_list = []

		for part_data in self.char_list:
			part = part_data[0]
			joint_data = part_data[2]
			ctrl_data = part_data[3]
			self.joint_scroll_data_list.append('{}  :   {}'.format(part,joint_data))
			self.ctrl_scroll_data_list.append('{}  :   {}'.format(part,ctrl_data))

		mc.textScrollList('jnt_scroll', e=1, ra=1)
		mc.textScrollList('jnt_scroll', e=1, a = self.joint_scroll_data_list)

		mc.textScrollList('ctrl_scroll', e=1, ra=1)
		mc.textScrollList('ctrl_scroll', e=1, a = self.ctrl_scroll_data_list)

	def convert_scroll_to_list(self,*args):
		self.joint_scroll_data_list = mc.textScrollList('jnt_scroll', q=1, ai=1)
		self.ctrl_scroll_data_list = mc.textScrollList('ctrl_scroll', q=1, ai=1)
		
		for num_process in range(len(self.joint_scroll_data_list)):
			self.char_list[num_process][2]= self.joint_scroll_data_list[num_process].split('  :   ')[-1]
			self.char_list[num_process][3]= self.ctrl_scroll_data_list[num_process].split('  :   ')[-1]

		self.convert_list_to_scroll()
	
	# replace function
	def edit_obj_from_scroll(self, obj , text_scroll):
		#replace select idx
		idxs = mc.textScrollList(text_scroll, q=1, sii=1)
		if idxs:
			for idx in idxs:
				if text_scroll == 'jnt_scroll':
					self.char_list[idx-1][2] = obj

				if text_scroll == 'ctrl_scroll':
					self.char_list[idx-1][3] = obj

			self.convert_list_to_scroll()
		mc.textScrollList(text_scroll,e=1,sii=idx)

	def remove_obj_from_scroll(self, text_scroll):
		self.edit_obj_from_scroll('' , text_scroll)

	def edit_joint_scroll(self,*args):
		sel_obj = pmc.ls(sl=True)
		if sel_obj:
			obj = sel_obj[0].stripNamespace().__str__()
			self.edit_obj_from_scroll(obj , 'jnt_scroll')

	def edit_ctrl_scroll(self,*args):
		sel_obj = pmc.ls(sl=True)
		if sel_obj:
			obj = sel_obj[0].stripNamespace().__str__()
			self.edit_obj_from_scroll(obj , 'ctrl_scroll')	

	def remove_joint_scroll(self,*args):
		self.remove_obj_from_scroll('jnt_scroll')

	def remove_ctrl_scroll(self,*args):
		self.remove_obj_from_scroll('ctrl_scroll')	

	# use replace function as ''
	def write_tPose(self,*args):
		obj_str = mc.textField('tpose_text_field',q=1,tx=1)
		self.obj_list = obj_str.split(',')
		mocapData.write_tpose(self.proj, self.asset ,self.tPose_file_name, mc.ls(self.obj_list))
		print 'write tPose complete'

	def read_tPose(self,*args):
		mocapData.read_tpose(self.proj, self.asset, self.ns)
		print 'read tPose complete'

	def check_file(self,*args):
		mc.textField('file_field',e=1,tx=self.ns,bgc=[.5,1,.5])

	def find_obj(self,text_scroll):
		strs  = mc.textScrollList(text_scroll, q=1,si = 1 )
		if strs:
			obj = strs[0].split('  :   ')[-1] 
			pmc.select('{}{}'.format(self.ns,obj))

	def find_joint(self,*args):
		self.find_obj('jnt_scroll')

	def find_ctrl(self,*args):
		self.find_obj('ctrl_scroll')


	# UI Layout Part
	def show_ui(self):
		if mc.window(self.winName,exists=1):
			mc.deleteUI(self.winName)
		
		ui = mc.window(self.winName,title = 'mocapRig',w=600)
		# main layout
		mainLayout = mc.columnLayout('mainCol',cal='center',co=['both',15],w=600,adj=1)
		nsColumn = mc.columnLayout('nsColumn',cal='center',adj=1)
		mc.separator(h=10, style ='none')
		mc.text('Please fill namespace of character.')
		ns_text_field =mc.textField('ns',w=200,ed=0,bgc=[1,.5,.5],ip = 0)
		mc.separator(h=5,style ='none')
		mc.button('getNS',l='get namespace' , c= self.get_ns)
		mc.separator(h=5,style ='none')
		
		# file_field =mc.textField('file_text',w=200,ed=0,ip = 0,bgc=[1,.5,.5])
		# mc.separator(h=5,style ='none')
		# mc.button('browse',l='browse mocap folder' , c= self.get_folder)
		mc.setParent(mainLayout)
		mc.separator(h=14, style ='none')
		tab_layout = mc.tabLayout('tmpSetupTab', p=mainLayout, cr=1, w=560 , sc=self.adjColumn)

		# first layout
		mc.setParent(tab_layout)
		mocap_tab = mc.columnLayout('mocap_tab', cal='center', co=['both',15], w=570, adj=1)
		mocap_row_layout = mc.rowLayout(numberOfColumns=7)
		#mc.separator(w = 45,h=12, style='none')

		# # part column
		#part_column = mc.columnLayout('part_column', cal='center', w=200, adj=1)
		# mc.separator(h=6, style='none')
		# mc.text('part_ctrl', l='Part')
		# mc.separator(h=3, style='none')
		# part_add_rem_column = mc.columnLayout('part_add_rem_column', cal='center', adj=1)
		# part_add_rem_row = mc.rowLayout('part_add_rem_row', nc=2, w=180)
		# mc.button('part_replace_button', l='replace', w=100)
		# mc.button('part_remove_button', l='remove', w=100)
		# mc.setParent(part_column)
		# mc.separator(h=3, style ='none')
		# mc.textScrollList('part_scroll', w=180, h=300, allowMultiSelection=1, shi=2)
		# mc.separator(h=6, style='none')
		# mc.button('part_find', l='Find', w=100)
		
		# part_order_column = mc.columnLayout('part_order_column',cal='center',adj=1)
		# part_order_row = mc.rowLayout('part_order_row', nc=2, w=200)
		# mc.setParent(part_column)
		# mc.separator(h=3, style='none')
		# mc.setParent(part_column)
		# mc.setParent(mocap_row_layout)
		# mc.columnLayout('swapColumn2', cal='center', w=28, adj=1, co=['both',1])
		# mc.setParent(mocap_row_layout)
		
		# jnt_column
		jnt_column = mc.columnLayout('jnt_column', cal='center', w=250, adj=1)
		mc.separator(h=6, style ='none')
		mc.text('jnt_text', l='Joint')
		mc.separator(h=3, style ='none')
		jnt_add_rem_column = mc.columnLayout('jnt_add_rem_column', cal='center', adj=1)
		jnt_add_rem_row = mc.rowLayout('jnt_add_rem_row', nc=2, w=180)
		mc.button('jnt_replace_button', l='replace', w=125 , c= self.edit_joint_scroll)
		mc.button('jnt_remove_button', l='remove', w=125 , c= self.remove_joint_scroll)
		mc.setParent(jnt_column)
		mc.separator(h=3, style ='none')
		mc.textScrollList('jnt_scroll', w=250, h=300, allowMultiSelection=1, shi=1 )
		mc.separator(h=6, style='none')
		mc.button('jnt_find',l='Find', w=100 , c= self.find_joint)

		jnt_order_column = mc.columnLayout('jnt_order_column', cal='center', adj=1)
		jnt_order_row = mc.rowLayout('jnt_order_row', nc=2, w=200)
		mc.setParent(jnt_column)
		mc.separator(h=3, style='none')
		mc.setParent(jnt_column)
		mc.setParent(mocap_row_layout)
		mc.separator(w=30, style='none',h=30)

		# ctrl_column
		mc.setParent(mocap_row_layout)
		ctrl_column = mc.columnLayout('ctrl_column', cal='center', w=250, adj=1)
		mc.separator(h=6, style ='none')
		mc.text('ctrl_text', l='Ctrl')
		mc.separator(h=3, style ='none')
		ctrl_add_rem_column = mc.columnLayout('ctrl_add_rem_column', cal='center', adj=1)
		ctrl_add_reomve_row = mc.rowLayout('ctrl_add_reomve_row', nc=2, w=180)
		mc.button('ctrl_replace_button', l='replace', w=125, c= self.edit_ctrl_scroll)
		mc.button('ctrl_remove_button', l='remove', w=125, c = self.remove_ctrl_scroll)
		mc.setParent(ctrl_column)
		mc.separator(h=3,style ='none')
		mc.textScrollList('ctrl_scroll',w=250,h=300,allowMultiSelection=1,shi=1 )
		mc.separator(h=6, style='none')
		mc.button('ctrl_find',l='Find',w=100 , c = self.find_ctrl)

		ctrl_column = mc.columnLayout('ctrl_column',cal='center',adj=1)
		ctrl_row = mc.rowLayout('ctrl_row',nc=2,w=200)
		mc.setParent(ctrl_column)
		mc.separator(h=3,style='none')
		mc.setParent(ctrl_column)
		# Foot
		mc.setParent(mocap_row_layout)
		mc.separator(h=8,style='none')
		mc.setParent('mocap_tab')
		mc.separator(w=25,style='none')
		mc.separator(h=10,style ='singleDash',w=200)
		mc.separator(h=6,style ='none')

		resetRow = mc.rowLayout('resetRow',nc=10)
		mc.nodeIconButton('hik_load_file',l='load from file',w=120,h=50, st =  "iconAndTextVertical" ,  i = 'openScript.png' ,c = self.load_from_file )	
		
		mc.separator(style ='none', w = 10)
		mc.nodeIconButton('hik_default',l='default template',w=120,h=50 , st =  "iconAndTextVertical" ,  i = 'openScript.png' , c = self.load_default_char_list)

		mc.separator(style ='none', w = 10)
		mc.nodeIconButton('hik_map_load',l='load by asset',w=120,h=50 , st =  "iconAndTextVertical" ,  i = 'openScript.png', c=self.load_char_list)

		mc.separator(style ='none', w = 10)
		mc.nodeIconButton('hik_map_write',l='save',w=120,h=50 , st =  "iconAndTextVertical" ,  i = 'save.png', c= self.save_char_list)

		mc.setParent(mocap_tab)
		mc.separator(h=12,style ='none')


		# TPoseTab
		mc.setParent(tab_layout)
		tPose_tab = mc.columnLayout('tpose_tab', cal='center', co=['both',15], w=300, adj=1)
		tpose_column = mc.columnLayout('tpose_column', cal='center', w=200, adj=1)
		mc.separator(h=5,style ='none')
		mc.text('Type suffix of ctrl to write tPose Data.')
		mc.separator(h=5,style ='none')
		tpose_text_field =mc.textField('tpose_text_field', tx='*:*_Ctrl, *_Ctrl' , w=200, ip = 0)
		mc.separator(h=5,style ='none')
		mc.separator(h=6, style='none')
		mc.separator(style ='none', w = 10)
		mc.nodeIconButton('hik_write_tpose',l='write tPose',w=120,h=60 ,  st =  "iconAndTextVertical" ,i='poseEditor.png', c = self.write_tPose)

		mc.separator(style ='none',h =3 ,w = 10)
		mc.nodeIconButton('hik_read_tpose',l='read tPose',w=120,h=60 ,  st =  "iconAndTextVertical" ,i='poseEditor.png' , c = self.read_tPose)

		mc.setParent(mainLayout)
		mc.separator(h=20,style ='none')


		# fkik_snap
		 





		mc.showWindow(self.winName)

# import mocapRigUI
# reload(mocapRigUI)

# mocapRigUI.mocapRig().show_ui()