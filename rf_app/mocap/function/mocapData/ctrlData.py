import math
import pymel.core as pmc
import maya.cmds as mc
import maya.mel as mm
import os
import json
import pickle

import ioData as iod
reload(iod)
# import crvFunc as cf
# reload(cf)
# write ctrl attr part

fld_path = iod.get_data_folder('rigData','ctrl','ctrl_attr')

def create_attr_dict_list(obj = ''):
	# creat write_obj list for loop
	write_obj = [obj]
	dict_list = []
	shapes = pmc.listRelatives(obj, shapes = True)
	# create exception attribute
	exception = [ 'controlPoints.xValue' ,
				  'controlPoints.yValue' ,
				  'controlPoints.zValue' ,
				  'colorSet.clamped' ,
				  'colorSet.representation' ]
	
	# append shape to write_obj list
	for shape in shapes:
		write_obj.append(shape)
	# create empty dict to collect dag attr data
	aDct = {}
	# loop each node to collect attr
	for each in write_obj :
		# create empty dict for collect attribute data
		aDctAttr = {}
		# assign obj as pynode and list attribute for query
		each_obj = pmc.PyNode(each)
		attrs = pmc.listAttr(each, keyable = True, s = True, unlocked = True)
		# check name space / assign as name space key
		nm = each_obj.stripNamespace().__str__()
		aDctAttr['namespace'] = each_obj.namespace()
		# check attribute before query
		if attrs :
			
			# create dict data / attribute as key / query as value
			for attr in attrs :
				if attr not in exception :
					aDctAttr[attr] = each_obj.attr(attr).get()
			
			# assign obj name as key					
			aDct[nm] = aDctAttr

	# append both dict to list
		dict_list.append(aDct)
	return dict_list


def write_attr(obj_list, file_path = ''):
	attr_list = []
	# loop obj_list
	for obj in obj_list:
		# create obj dict list
		dict_list = create_attr_dict_list(obj)
		# append to attr_list
		attr_list.append(dict_list)
	# write attr_list to file
	iod.write_json_data(attr_list,file_path)


def multiple_write_attr(obj_list, fld_path = fld_path):
	# get path file
	if obj_list:
		# loop obj_list
		for obj in obj_list:
			# get obj name
			nm = pmc.PyNode(obj).stripNamespace().__str__()
			# ctrl name
			file_name = nm+'_attr'
			file_path = os.path.join(fld_path,file_name)
			write_attr([obj], file_path)

def pack_write_attr(obj_list, file_name='ctrl_attr'):
	
	if obj_list:
		file_name
		file_path = os.path.join(fld_path,file_name)
		write_attr(obj_list, file_path)	

# read ctrl attr part
def read_attr_dict_list(data_list = [] ,ns = None ,search_for ='', replace_as =''):
	if data_list:
		for dict_list in data_list:
			for dag in dict_list:
				for key in dag.keys():
					if ns == None:
						if 'namespace' in dag[key].keys():
							obj_name = '{}{}'.format(dag[key]['namespace'],key)
						else:
							obj_name = key
					elif ns != None:
						if not ns:
							obj_name = key

						elif ns[-1] == ':':
							obj_name = '{}{}'.format(ns,key)
						else:
							obj_name = '{}:{}'.format(ns,key)
					
					for attr in dag[key]:
						if attr != 'namespace':
							if pmc.objExists('{}.{}'.format(obj_name,attr)):
								if not pmc.getAttr('{}.{}'.format(obj_name, attr), l=1):

									try:
										pmc.setAttr('{}.{}'.format(obj_name,attr),dag[key][attr])
									except:
										pass 


def read_attr(file_path = '', ns = None, search_for = '', replace_as = ''):
	data_list = iod.read_json_data(file_path)
	read_attr_dict_list(data_list,ns)

def read_attr_obj(obj_list = [] ,ns = '' ,search_for = '', replace_as = '' , folder_path = fld_path):
	for obj in obj_list:
		file_name = iod.get_obj_file_name(obj,'attr')
		#folder_path = iod.get_data_folder('rigData','ctrl','ctrl_attr')
		file_path = os.path.join(folder_path,file_name)
		read_attr(file_path, ns, search_for, replace_as)

# # write ctrl shape
# def create_crv_dict_list(obj = []):
# 	# crate empty list
# 	crv_shape_list = []
# 	# get crv shape dict from obj_list
# 	# get crv dict from obj
# 	crv_dict = {}
# 	crv_shape_dict = {}
# 	# get namespace
# 	each_obj = pmc.PyNode(obj)
# 	nm = each_obj.stripNamespace().__str__()
# 	# assign key from namespace / data to crv_shape_dict 
# 	crv_shape_dict['namespace'] = each_obj.namespace()
# 	crv_shape_dict['data'] = cf.get_shape(obj)

# 	# assign key from object name / data to crv_dict
# 	crv_dict[nm] = crv_shape_dict
# 	#crv_shape_list.append(crv_dict)

# 	return crv_dict

# def write_crv_shape(obj_list, file_path = ''):
# 	crv_list = []
# 	# loop obj_list
# 	for obj in obj_list:
# 		# create obj dict list
# 		dict_list = create_crv_dict_list(obj)
# 		# append to crv_list
# 		crv_list.append(dict_list)
# 	# write crv_list to file
# 	iod.write_json_data(crv_list,file_path)
	

# def multiple_write_crv_shape(obj_list , fld_path = fld_path):
# 	# get path file
# 	#fld_path = iod.get_data_folder('rigData','ctrl','ctrl_shape')
# 	if obj_list:
# 		# loop obj_list
# 		for obj in obj_list:
# 			# get obj name
# 			nm = pmc.PyNode(obj).stripNamespace().__str__()
# 			# ctrl name
# 			file_name = nm+'_ctrl'
# 			file_path = os.path.join(fld_path,file_name)
# 			write_crv_shape([obj], file_path)

# def pack_write_crv_shape(obj_list, file_name='ctrl_attr', fld_path = fld_path):
# 	#fld_path = iod.get_data_folder('rigData','ctrl','ctrl_shape')
# 	if obj_list:
# 		file_path = os.path.join(fld_path,file_name)
# 		write_attr(obj_list, file_path)	


# def read_crv_dict_list(data_list= [], ns =None):
# 	if data_list:
# 		for dict_list in data_list:
# 			for obj in dict_list.keys():
# 				if ns == None:
# 					if 'namespace' in dict_list[obj].keys():
# 						obj_name = '{}{}'.format(dict_list[obj]['namespace'],obj)
# 					else:
# 						obj_name = key
# 				elif ns:
# 					if ns[-1] == ':':
# 						obj_name = '{}{}'.format(ns,obj)
# 					else:
# 						obj_name = '{}:{}'.format(ns,obj)        				
	
# 			cf.set_shape_base(obj_name,dict_list[obj]['data'])


# def read_crv_shape(file_path = '', ns = None, search_for = '', replace_as = ''):
# 	data_list = iod.read_json_data(file_path)
# 	read_crv_dict_list(data_list,ns)

# def read_crv_obj(obj_list = [] , ns =None , search_for='' , replace_as = '', fld_path = fld_path):
# 	for obj in obj_list:
# 		file_name = iod.get_obj_file_name(obj,'ctrl')
# 		#folder_path = iod.get_data_folder('rigData','ctrl','ctrl_shape')
# 		file_path = os.path.join(folder_path,file_name)
# 		read_crv_shape(file_path, ns, search_for, replace_as)
