import math
import pymel.core as pmc
import maya.cmds as mc
import maya.mel as mm
import os
import json
import pickle

import ioData as iod
reload(iod)

import ctrlData
reload(ctrlData)

fld_path = iod.get_data_folder('rigData','pose')

def write_pose(obj_list, file_name='default_pose', fld_path = fld_path):
	#fld_path = iod.get_data_folder('rigData','pose')
	if obj_list:
		file_path = os.path.join(fld_path,file_name)
		ctrlData.pack_write_attr(obj_list, file_path)

def read_pose(file_path = '', ns = None , search_for = '', replace_as = ''):
	data_list = iod.read_json_data(file_path)
	ctrlData.read_attr_dict_list(data_list, ns , search_for, replace_as)
