# v.0.0.1 polytag switcher
_title = 'Convert App'
_version = 'v.0.0.1'
_des = 'Wip'
uiName = 'ConvertUI'

#Import python modules
import sys
import os
import logging
import getpass
import traceback
from collections import OrderedDict
import pymel.core as pmc
import os
import json
import pickle
import subprocess

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
import rf_config as config

# framework modules
from rf_utils import log_utils
from rf_utils.ui import load
reload(load)
from rf_utils.ui import stylesheet

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui
from rf_utils.widget import project_widget
from rf_utils.sg import sg_utils
from rf_utils import file_utils as fu
from rf_utils.context import context_info
from rf_utils.pipeline import file_info 
reload(file_info)

# maya & module function 
import pymel.core as pmc
import maya.cmds as mc
import maya.mel as mel

from rf_app.mocap.function import fbxDef
reload(fbxDef)

class AddWindow(QtWidgets.QDialog):

    def __init__(self, parent=None, convertUi = ''):
        super(AddWindow, self).__init__(parent)

        self.input_dir = QtWidgets.QLineEdit()
        self.okButton = QtWidgets.QPushButton("OK")
        self.convertUi = convertUi
        addLayout = QtWidgets.QHBoxLayout()
        addLayout.addWidget(self.input_dir)
        addLayout.addWidget(self.okButton)

        self.resize(250, 50)
        self.setLayout(addLayout)
        
        self.okButton.clicked.connect(self.closeAddWindow)
        
    def closeAddWindow(self):
        dir_name = self.input_dir.text()
        if dir_name:
            os.mkdir(r"P:\Library\Mocap\%s"%dir_name)
            self.convertUi.proj_list_cbb()
            self.close()

class ConvertFile(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(ConvertFile, self).__init__(parent)

        # ui read
        uiFile = '%s/convert_file.ui' % moduleDir
        self.ui = load.setup_ui_maya(uiFile, parent)
        # set size 
        self.w = 500
        self.h = 600 

        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.resize(self.w, self.h)
        self.setWindowTitle('%s %s %s' % (_title, _version, _des))

        self.asset_list = ['']
        self.action_folder_list = []
        # self.projectWidget = project_widget.SGProjectComboBox(sg=sg_utils.sg, layout=QtWidgets.QHBoxLayout())
        # self.projectWidget.setContentsMargins(0, 0, 3, 0)
        # #spacerItem = QtWidgets.QSpacerItem(10, 10, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        # #self.projectLayout.addItem(spacerItem)

        # self.projectWidget.projectComboBox.setCurrentText(self.mocap_char_project)
        # self.ui.asset_name.setCurrentText(self.mocap_char_asset)
        self.addWindow = AddWindow(None ,  convertUi = self)
        self.init_function()

    def init_function(self):
        self.proj_list_cbb()
        #self.add_button()
        self.date_list_cbb()
        self.obj_listWidget()
        #self.bakeSel_button()
        #self.bakeAll_button()
        
        self.ui.anim_listBox.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
        self.ui.anim_listBox.currentItemChanged.connect(self.print_value)
        self.ui.bakeAll_butt.clicked.connect(self.bakeAll_button)
        self.ui.bakeSel_butt.clicked.connect(self.bakeSel_button)
        

    def proj_list_cbb(self):
        self.ui.proj_list.clear()
        self.proj_ls = os.listdir(r"P:\Library\Mocap")
        self.ui.proj_list.addItems(self.proj_ls)
    #-------------------------------------------------

    def add_button(self):
        self.ui.addButton.clicked.connect(self.showAddWindow)
        
    def showAddWindow(self, *args):
        self.addWindow.show()
    #-------------------------------------------------

    def date_list_cbb(self):
        self.change_dateList()
        self.ui.proj_list.currentTextChanged.connect(self.change_dateList)

    def change_dateList(self, *args):
        self.ui.date_list.clear()
        get_proj_list =  self.ui.proj_list.currentText()
        get_date = os.listdir(r"P:\Library\Mocap\%s"%get_proj_list)
        self.ui.date_list.addItems(get_date)
    #--------------------------------------------------------------

    def obj_listWidget(self):
        self.change_animList()
        self.ui.date_list.currentTextChanged.connect(self.change_animList)

    def change_animList(self, *args):
        self.ui.anim_listBox.clear()
        get_proj_list =  self.ui.proj_list.currentText()
        get_date_list = self.ui.date_list.currentText()
        self.date_path = r"P:\Library\Mocap\%s\%s"%(get_proj_list, get_date_list)
        self.ma_path = os.path.join(self.date_path,'maya')
        self.fbx_path = os.path.join(self.date_path,'fbx')
        try:
            get_anim = os.listdir(self.fbx_path)

            if not os.path.isdir(self.ma_path):
                os.mkdir(self.ma_path)
            #print self.ui.anim_listBox.addItems(get_anim)
            #print type(self.ui.anim_listBox)
            #count = 1
            self.all_anim_data = []
            for path in get_anim:
                if 'fbx' in path.split('.')[-1]:
                    item = QtWidgets.QListWidgetItem(self.ui.anim_listBox)
                    item.setText(path)
                    item.setData(QtCore.Qt.UserRole, os.path.join(self.fbx_path,path).replace('\\','/'))
                    
                    self.all_anim_data.append(os.path.join(self.fbx_path,path).replace('\\','/'))
                    #item.setText(action)
                    #item.setData(QtCore.Qt.UserRole, '%s/%s' % (currentPath, action))                    
                    

                    #self.ui.anim_listBox.setText(path)
                    #self.ui.anim_listBox.setData( QtCore.Qt.UserRole,os.path.join(self.date_path,path).replace('\\','/'),QtCore.Qt.UserRole)
                    #count +=1
        except:
            self.all_anim_data = []
    #----------------------------------------------------------------------------------

    def bakeSel_button(self):
        item_list = self.ui.anim_listBox.selectedItems()
        path_list = []
        for item in item_list:
            path_list.append(item.data(QtCore.Qt.UserRole))
        self.bake_fbx(path_list)
        mc.confirmDialog(m='bake process complete')
        
    
    def bakeAll_button(self):
        self.all_anim_data
        self.bake_fbx(self.all_anim_data)
        mc.confirmDialog(m='bake process complete')

    def bake_fbx(self,path_list):
        python_path = r'C:\Program Files\Autodesk\Maya2018\bin\mayapy.exe'
        batchPath = os.path.join(moduleDir,'fbxMa.py')

        for fbx_path in path_list:
            proc_obj  = subprocess.Popen([python_path, batchPath, fbx_path, self.ma_path], 
                stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        out, err = proc_obj.communicate()
        print('out -> %s' % out.decode())
        print('err -> %s' % err.decode())


    def print_value(self):
        #idx = self.ui.anim_listBox.currentIndex()
        #print 'idx : %s'%(idx)
        #value = self.ui.anim_listBox.itemData(idx,QtCore.Qt.UserRole)
        item = self.ui.anim_listBox.currentItem()
        data = item.data(QtCore.Qt.UserRole)
   

def show(mode='default'):
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = ConvertFile(maya_win.getMayaWindow())
        myApp.show()
        return myApp


if __name__ == '__main__':
    show()
