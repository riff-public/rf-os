# v.0.0.1 polytag switcher
_title = 'Convert App'
_version = 'v.0.0.1'
_des = 'Wip'
uiName = 'ConvertUI'

#Import python modules
import sys
import os
import logging
import getpass
import traceback
from collections import OrderedDict
import pymel.core as pmc
import os
import json
import pickle
import subprocess
import re
import tempfile
from rf_utils import project_info

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
import rf_config as config
reload(config)
# framework modules
from rf_utils import log_utils
reload(log_utils)
from rf_utils.ui import load
reload(load)
from rf_utils.ui import stylesheet
reload(stylesheet)

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui
from rf_utils.widget import project_widget
reload(project_widget)
from rf_utils.sg import sg_utils
reload(sg_utils)
from rf_utils import file_utils as fu
reload(fu)
from rf_utils.context import context_info
reload(context_info)
from rf_utils.pipeline import file_info 
reload(file_info)
from rf_utils.pipeline import create_asset
reload(create_asset)
from rf_utils import admin
reload(admin)
from rf_utils.sg import sg_process
reload(sg_process)
# maya & module function 
import pymel.core as pmc
import maya.cmds as mc
import maya.mel as mel

from rf_app.mocap.function import fbxDef
reload(fbxDef)

def create_folder(path):
    if '\\' in path:
        path = path.replace('\\','/')
    print 'path: %s'%path

    for level in range(len(path.split('/'))):
        #print level
        path_check = '/'.join(path.split('/')[:level+1]).replace('\\','/')
        print path_check
        if not os.path.isdir(path_check):
            #print 'print create folder'
            os.mkdir(path_check)

    return path_check

class AddWindow(QtWidgets.QDialog):

    def __init__(self, parent=None, convertUi = ''):
        super(AddWindow, self).__init__(parent)

        self.input_dir = QtWidgets.QLineEdit()
        self.okButton = QtWidgets.QPushButton("OK")
        self.convertUi = convertUi
        addLayout = QtWidgets.QHBoxLayout()
        addLayout.addWidget(self.input_dir)
        addLayout.addWidget(self.okButton)

        self.resize(250, 50)
        self.setLayout(addLayout)
        
        self.okButton.clicked.connect(self.closeAddWindow)
        
    def closeAddWindow(self):
        dir_name = self.input_dir.text()
        if dir_name:
            os.mkdir(r"P:\Library\Mocap\%s"%dir_name)
            self.convertUi.proj_list_cbb()
            self.close()

class ConvertFile(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(ConvertFile, self).__init__(parent)

        # ui read
        uiFile = '%s/convert_file.ui' % moduleDir
        self.ui = load.setup_ui_maya(uiFile, parent)
        # set size 
        self.w = 500
        self.h = 600 

        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.resize(self.w, self.h)
        self.setWindowTitle('%s %s %s' % (_title, _version, _des))

        self.asset_list = ['']
        self.action_folder_list = []
        # self.projectWidget = project_widget.SGProjectComboBox(sg=sg_utils.sg, layout=QtWidgets.QHBoxLayout())
        # self.projectWidget.setContentsMargins(0, 0, 3, 0)
        # #spacerItem = QtWidgets.QSpacerItem(10, 10, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        # #self.projectLayout.addItem(spacerItem)

        # self.projectWidget.projectComboBox.setCurrentText(self.mocap_char_project)
        # self.ui.asset_name.setCurrentText(self.mocap_char_asset)
        self.addWindow = AddWindow(None ,  convertUi = self)
        self.init_function()

    def init_function(self):
        self.proj_list_cbb()
        #self.add_button()
        self.date_list_cbb()
        self.obj_listWidget()
        #self.bakeSel_button()
        #self.bakeAll_button()
        
        self.ui.anim_listBox.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
        #self.ui.anim_listBox.currentItemChanged.connect(self.print_value)
        self.ui.bakeAll_butt.clicked.connect(self.bakeAll_button)
        self.ui.bakeSel_butt.clicked.connect(self.bakeSel_button)
        

    def proj_list_cbb(self):
        self.ui.proj_list.clear()
        self.proj_ls = os.listdir(r"P:\Library\Mocap")
        self.ui.proj_list.addItems(self.proj_ls)
    #-------------------------------------------------

    def add_button(self):
        self.ui.addButton.clicked.connect(self.showAddWindow)
        
    def showAddWindow(self, *args):
        self.addWindow.show()
    #-------------------------------------------------

    def date_list_cbb(self):
        self.change_dateList()
        self.ui.proj_list.currentTextChanged.connect(self.change_dateList)

    def change_dateList(self, *args):
        self.ui.date_list.clear()
        get_proj_list =  self.ui.proj_list.currentText()
        self.project = get_proj_list
        get_date = os.listdir(r"P:\Library\Mocap\%s"%get_proj_list)
        self.ui.date_list.addItems(get_date)
        self.sg_shot = sg_process.get_all_shots(get_proj_list)
    #--------------------------------------------------------------

    def obj_listWidget(self):
        self.change_animList()
        self.ui.date_list.currentTextChanged.connect(self.change_animList)

    def change_animList(self, *args):
        self.ui.anim_listBox.clear()
        get_proj_list =  self.ui.proj_list.currentText()
        get_date_list = self.ui.date_list.currentText()
        self.date_path = r"P:\Library\Mocap\%s\%s"%(get_proj_list, get_date_list)
        #self.ma_path = os.path.join(self.date_path,'maya')
        self.fbx_path = os.path.join(self.date_path,'fbx')
        
        try:
            get_anim = os.listdir(self.fbx_path)

            # if not os.path.isdir(self.ma_path):
            #     os.mkdir(self.ma_path)
            #print self.ui.anim_listBox.addItems(get_anim)
            #print type(self.ui.anim_listBox)
            #count = 1
            self.all_anim_data = []
            for path in get_anim:
                if 'fbx' in path.split('.')[-1]:
                    item = QtWidgets.QListWidgetItem(self.ui.anim_listBox)
                    item.setText(path)
                    item.setData(QtCore.Qt.UserRole, os.path.join(self.fbx_path,path).replace('\\','/'))
                    
                    self.all_anim_data.append(os.path.join(self.fbx_path,path).replace('\\','/'))
                    #item.setText(action)
                    #item.setData(QtCore.Qt.UserRole, '%s/%s' % (currentPath, action))                    
                    

                    #self.ui.anim_listBox.setText(path)
                    #self.ui.anim_listBox.setData( QtCore.Qt.UserRole,os.path.join(self.date_path,path).replace('\\','/'),QtCore.Qt.UserRole)
                    #count +=1
        except:
            self.all_anim_data = []
    #----------------------------------------------------------------------------------

    def bakeSel_button(self):
        item_list = self.ui.anim_listBox.selectedItems()
        path_list = []
        for item in item_list:
            path_list.append(item.data(QtCore.Qt.UserRole))
        self.bake_fbx(path_list)
        mc.confirmDialog(m='bake process complete')
        
    
    def bakeAll_button(self):
        #self.all_anim_data
        self.bake_fbx(self.all_anim_data)
        mc.confirmDialog(m='bake process complete')

    def bake_fbx(self,path_list):
        proj = project_info.ProjectInfo(self.project)
        python_path = proj.software.mayapy()
        #python_path = r'C:\Program Files\Autodesk\Maya2020\bin\mayapy.exe'
        batchPath = os.path.join(moduleDir,'fbxMa.py')


        sc_context = context_info.Context()
        sc_context.update(project = self.project, entityType = 'scene')             
        scene_context = context_info.ContextPathInfo(context=sc_context)

        as_context = create_asset.get_context(self.project,'extra','char','all')
        asset_context = context_info.ContextPathInfo(context=as_context)
        asset_context.context.update(step ='mocap')
        version = mc.about(v=True)
        department = 'Py'
        env = asset_context.projectInfo.get_project_env(dcc='maya', dccVersion=version, department=department)
        non_found = []
        

        for fbx_path in path_list:
            splitList = fbx_path.split('/')
            #print splitList
            mb_file = splitList[-1].replace('.fbx','.mb')
            file_split = mb_file.split('_')
            
            raw_shot_list = []
            if len(file_split) >= 4:
                proj = file_split[0]
                ep = file_split[1]
                sq = file_split[2]
                ver = file_split[-1]

                asset = file_split[-2]
                shot_list = file_split[3:-2]
                action = ''


                shot_check = re.search('.*s[0-9][0-9][0-9][0-9]_',mb_file)
                action_check = mb_file.split(shot_check.group())[-1].split('_')
                if len(action_check) == 2:
                    asset = action_check[0]
                    action = ''
                    shot_list = file_split[3:-2]

                elif len(action_check) ==3:
                    asset = action_check[0]
                    action = '{}_'.format(action_check[1])
                    shot_list = file_split[3:-3]

                for shot in shot_list:
                    for shots in self.sg_shot:
                        if re.search("{}_{}_{}_{}".format(proj,ep,sq,shot),shots['code']):

                            shot_name = shots["code"]
                            scene_context.context.update(entity = shot_name)
                            scene_context.update_scene_context()
                            shot_path =  scene_context.path.scheme(key='publishShotPath').abs_path()
                            raw_shot_path = os.path.join(shot_path,'mocap','raw').replace('\\','/')
                            raw_file_name = '{}_{}_{}{}'.format(shot_name,asset,action,ver)
                            print '{} : {}'.format(raw_shot_path,raw_file_name)
                            raw_shot_list.append([raw_shot_path,raw_file_name,fbx_path])
                            if splitList[-1] in non_found:
                                non_found.remove(splitList[-1])
                            break

                        elif re.search("{}_{}_{}[a-z]_{}".format(proj,ep,sq,shot),shots['code']):
                            shot_name = shots["code"]
                            scene_context.context.update(entity = shot_name)
                            scene_context.update_scene_context()
                            shot_path =  scene_context.path.scheme(key='publishShotPath').abs_path()
                            raw_shot_path = os.path.join(shot_path,'mocap','raw').replace('\\','/')
                            raw_file_name = '{}_{}_{}{}'.format(shot_name,asset,action,ver)
                            print '{} : {}'.format(raw_shot_path,raw_file_name)
                            raw_shot_list.append([raw_shot_path,raw_file_name,fbx_path])

                            if splitList[-1] in non_found:
                                non_found.remove(splitList[-1])
                            break

                        else:
                            if splitList[-1] not in non_found:
                                non_found.append(splitList[-1])
                            continue

            else:
                asset_name = sg_process.get_one_asset(self.project,file_split[0])
                if asset_name:
                    asset_name = asset_name['code']
                else:
                    asset_name = 'extra'
                print asset_name
                #task_context.use_sg(sg_utils.sg, self.project, 'asset', entityName = asset_name)
                asset_context.context.update(entity =asset_name)
                asset_publ_path = asset_context.path.scheme(key='publishStepPath').abs_path()
                raw_shot_path = os.path.join(asset_publ_path,'raw').replace('\\','/')
                raw_shot_list.append([raw_shot_path, mb_file,fbx_path])

            done =0
            return_file = ''
            print 'raw_shot_list : '
            print raw_shot_list

            handle,temp_path = tempfile.mkstemp(suffix='.mb')
            os.close(handle)
            temp_dir = os.path.dirname(temp_path)
            temp_file = os.path.basename(temp_path)

            for raw_shot_path in raw_shot_list:
                print 'raw_shot_path : '
                print raw_shot_path
                if not os.path.isdir(raw_shot_path[0]):
                    #print 'create_folder at {}'.format(raw_shot_path)
                    admin.makedirs(raw_shot_path[0])
                else:
                    print raw_shot_path                    
                #mb_path = os.path.join(raw_shot_path)
                #print mb_path
                
                data_dict = [raw_shot_path[1],raw_shot_path[-1]]
                #print data_dict
                #print os.path.join('/'.join(raw_shot_path[0].split('/')[:-1]),'mocap_data.json')
                self.write_mocapData(data_dict,os.path.join('/'.join(raw_shot_path[0].split('/')[:-1]),'mocap_data.json'))



                # print ma_path
                if not done:

                    #print temp_dir
                    #print temp_file
                    proc_obj  = subprocess.Popen([python_path, batchPath, fbx_path, temp_dir,temp_file], 
                        stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True,env=env)
                    out, err = proc_obj.communicate()
                    print('out -> %s' % out.decode())
                    print('err -> %s' % err.decode())
                    #return_file = proc_obj.CompletedProcess()
                    #return_file = os.path.join(temp_path,raw_shot_path[1])
                    #
                    print 'copy from{} to {}'.format(temp_path, os.path.join(raw_shot_path[0],raw_shot_path[1]))
                    admin.copyfile(temp_path, os.path.join(raw_shot_path[0],raw_shot_path[1]))
                    done = 1
                else:
                    #print return_file ,':to:',os.path.join(raw_shot_path[0],raw_shot_path[1])
                    print 'copy from{} to {}'.format(temp_path, os.path.join(raw_shot_path[0],raw_shot_path[1]))
                    admin.copyfile(temp_path, os.path.join(raw_shot_path[0],raw_shot_path[1]))

            os.remove(temp_path)

        if non_found:
            mc.confirmDialog(m = '\n'.join(non_found)+'\n does not match the shot name.')


    def check_extra_folder(self,*args):
        extra_asset = create_asset.get_context(self.project,'extra','char','all')
        asset_context = context_info.ContextPathInfo(context = extra_asset)
        asset_context.context.update(step ='mocap')

        self.extra_asset_work = asset_context.path.scheme(key='workAssetNamePath').abs_path()
        self.extra_asset_path = asset_context.path.scheme(key='publishStepPath').abs_path()
        self.extra_asset_data_path = os.path.join(self.extra_asset_path,'data','raw').replace('\\','/')

        if os.path.isdir(self.extra_asset_work):
            return True
        else:
            create_asset.create(str(self.project), 'mocap', 'char', assetSubtype='main', mainAsset='', tags='', template='default', setEntity=[], link=True) 
            admin.makedirs(self.extra_asset_data_path)
                 

    def print_value(self):
        #idx = self.ui.anim_listBox.currentIndex()
        #print 'idx : %s'%(idx)
        #value = self.ui.anim_listBox.itemData(idx,QtCore.Qt.UserRole)
        item = self.ui.anim_listBox.currentItem()
        data = item.data(QtCore.Qt.UserRole)
   
    def write_mocapData(self,info,file):
        if os.path.isfile(file):
            with open(file,'r') as f:
                data_dict = json.load(f)
        else:
            data_dict = {}

        data_dict[info[0]] = info[1]

        with open(file,'w') as f:
            json.dump(data_dict,f)




def show(mode='default'):
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = ConvertFile(maya_win.getMayaWindow())
        myApp.show()
        return myApp


if __name__ == '__main__':
    show()
