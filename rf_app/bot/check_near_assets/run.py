import sys
import os 
import argparse
import time
import logging
from collections import OrderedDict

import pymel.core as pm 
import maya.cmds as mc 

from rf_utils import log_utils
from rf_maya.rftool.utils import objectsInCameraView as oic
reload(oic)

UI_NAME = 'check_near_assets'
pkg_dir = os.path.dirname(__file__)
DEFAULT_FILE_LIST = '%s\\file_lists.txt' %pkg_dir
# DEFAULT_OUTPUT_DIR = '%s\\outputs' %pkg_dir
DEFAULT_OUTPUT_DIR = '%s\\logs\\%s' %(os.environ.get('RFSCRIPT'), UI_NAME)
LOG_LEVEL = logging.INFO
GEO_GRP_NAME = 'Geo_Grp'
CHECK_STEP = 5

def setup_parser(title):
    # create the parser
    parser = argparse.ArgumentParser(description=title, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-f', dest='file_list', type=str, help='Path to text file containing file paths', default=DEFAULT_FILE_LIST)
    parser.add_argument('-s', dest='scenes', nargs='+', help='Paths to maya scene file', default='')
    parser.add_argument('-j', dest='job_names', nargs='+', help='Name of the job', default=[])
    parser.add_argument('-g', dest='generate_summary_log', type=bool, help='Generate summary log', default=False)
    parser.add_argument('-o', dest='output_log', type=str, help='Path to output directory', default=DEFAULT_OUTPUT_DIR)
    return parser

def generate_summary_log(log_dir):
    sum_logger = logging.getLogger(__name__)
    sum_logger.addHandler(logging.NullHandler())

    results = {}  # {assetName: (distance, ns, shot, path)}
    for f in os.listdir(log_dir):
        if 'process' in f or 'summary' in f:
            continue
        
        fp = '%s/%s' %(log_dir, f)
        sum_logger.info('Reading: %s' %(fp))
        lines = []
        with open(fp, 'r') as fh:
            lines = fh.readlines()
        shotName, ext = os.path.splitext(f)
        for line in lines:
            le = line.split()
            assetName = le[0]
            ns = le[1]
            dist = le[2]
            path = le[3]

            if assetName not in results:
                results[assetName] = (dist, ns, shotName, path)
            else:
                old_dist = results[assetName][0]
                if float(dist) < float(old_dist):
                    results[assetName] = (dist, ns, shotName, path)

    sorted_dists = sorted(results.items(), key=lambda x: float(x[1][0]))
    resTexts = ['{:<25s} {:<20s} {:<32s} {:<12s} {:^32s}'.format('ASSET', 'DISTANCE', 'NAMESPACE', 'SHOT', 'PATH'), 
                '='*150]
    for assetName, data in sorted_dists:
        dist = data[0]
        ns = data[1]
        shotName = data[2]
        path = data[3]
        resTexts.append('{:<25s} {:<20s} {:<32s} {:<12s} {:<32s}'.format(assetName, str(dist), ns, shotName, path))
    
    summary_log_path = '%s/summary.log' %(log_dir)
    with open(summary_log_path, 'w') as f:
        f.write('\n'.join(resTexts))

def main(files, output_dir, job_names=[], generate_summary_log=False):
    output_dir = output_dir.replace('\\', '/')
    # create log
    log_dir = ''
    if not job_names:
        timestr = time.strftime("%Y%m%d-%H%M%S")
        log_folder = '%s.%s' %(UI_NAME, timestr)
        log_dir = '%s/%s' %(output_dir, log_folder)
        process_log_file_path = '%s/process.log' %(log_dir)
    else:
        log_folder = job_names[0]
        log_dir = '%s/%s' %(output_dir, log_folder)
        process_log_file_path = '%s/process_%s.log' %(log_dir, job_names[1])
    
    if not os.path.exists(log_dir):
        logger.debug('# Log dir created: %s' %log_dir)
        os.makedirs(log_dir)
    
    fh1 = logging.FileHandler(process_log_file_path)
    fh1.setLevel(LOG_LEVEL)
    logger.addHandler(fh1)

    # read the path list
    logger.info('# Starting %s' %UI_NAME)
    logger.info('# Files are...')
    logger.info('\n'.join(files))

    num_files = len(files)
    for i, fp in enumerate(files):
        if os.path.exists(fp) and os.path.isfile(fp):
            logger.info('=========================')
            logger.info('>> Starting (%s/%s): %s' %((i+1), num_files, fp))
        else:
            continue

        # open file
        logger.info('Openning scene...')
        st = time.time()
        try:
            pm.openFile(fp, f=True)
        except:
            logger.error('Cannot open: %s' %fp)
            continue
        logger.info('Scene opened.')

        # get cam
        logger.info('Getting shot node...')
        shotNodes = pm.ls(type='shot')
        if not shotNodes or len(shotNodes) != 1:
            logger.error('Cannot find shot node!')
            continue
        shotName = shotNodes[0].nodeName()
        logger.info('Shot node: %s' %shotName)

        logger.info('Getting camera...')
        camName = '%s_cam' %(shotName)
        if len(pm.ls(camName)) != 1:
            logger.error('Cannot find camera: %s' %camName)
            continue

        cam = pm.PyNode(camName)
        if not cam.getShape(type='camera'):
            logger.error('Invalid camera: %s' %cam)
            continue
        logger.info('Camera: %s' %camName)

        # run check 
        logger.info('Start getting asset distances...')
        try: 
            results = oic.get_asset_distance_from_cam(cam, 
                    geoGrpName=GEO_GRP_NAME, 
                    step=CHECK_STEP,
                    onlyInCamera=True, 
                    printResult=False)
        except:
            logger.error('Failed to get distance from camera')
            continue

        logger.info('Finished getting asset distances.')

        # write result
        logger.info('Reordering results...')
        asset_dict = OrderedDict()
        for ref, dist in results.iteritems():
            ns = ref.namespace
            data = (ns, dist)
            path = ref.path
            if path not in asset_dict:
                asset_dict[path] = data
            else:
                old_dist = asset_dict[path][1]
                if dist < old_dist:
                    asset_dict[path] = data
        logger.info('Results reordered.')

        logger.info('Writing log...')
        log_file_path = '%s/%s.log' %(log_dir, shotName)
        logger.info('Scene log path: %s' %log_file_path)
        resTexts = []
        for path, data in asset_dict.iteritems():
            # try to get type, asset name
            pathSplits = path.split('/')
            if len(pathSplits) >= 6:
                asset_name = '%s/%s' %(pathSplits[4], pathSplits[5])
            else:
                asset_name = os.path.basename(path)

            ns = data[0]
            dist = data[1]
            resTexts.append('{:<25s} {:<32s} {:<20s} {:<32s}'.format(asset_name, ns, str(dist), path))
        
        with open(log_file_path, 'w') as f:
            f.write('\n'.join(resTexts))

        logger.info('Write log complete.')
        duration = time.time() - st
        logger.info('>> Finished in %s s: %s' %(duration, fp))

    # generate summary log
    if generate_summary_log:
        logger.info('Generating summary...')
        generate_summary_log(log_dir)
        logger.info('Summay log done.')
    logger.info('>> Check complete.')

if __name__ == "__main__":
    parser = setup_parser('Check for assets that get near the camera in a list of shots.')
    params = parser.parse_args()    

    files = []
    if params.scenes:
        files = params.scenes
    elif params.file_list:
        with open(params.file_list, 'r') as f:
            files = [l.rstrip() for l in f.readlines()]
    if files:
        files = [fp.replace('\\', '/') for fp in files]
        main(files=files, 
            output_dir=params.output_log,
            job_names=params.job_names,
            generate_summary_log=params.generate_summary_log
            )

'''
"C:/Program Files/Autodesk/Maya2018/bin/mayapy.exe" "D:/dev/core/rf_app/bot/check_near_assets/run.py"


'''