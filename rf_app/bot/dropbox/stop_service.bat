@echo off
set service_path="%~dp0\service.py"
set service_name=RiffDropboxService

SC QUERY %service_name% > NUL
IF ERRORLEVEL 1060 GOTO END ELSE GOTO CHECK_STOP

:CHECK_STOP
SC QUERY %service_name% | find "RUNNING" > NUL
IF ERRORLEVEL 0 (
	python %service_path% stop
)

:UNINSTALL
python %service_path% remove

:END
echo Done.
PAUSE