import os
import sys
SCRIPT_PATH = os.path.join(os.environ['RFSCRIPT'], 'core')
if not SCRIPT_PATH in sys.path:
    sys.path.append(SCRIPT_PATH)

import logging
logging.basicConfig(filename='log.log', level=logging.DEBUG)
logger = logging.getLogger(__name__)

if not len(logger.handlers):
    sh = logging.StreamHandler()
    sh.setLevel(logging.DEBUG)
    logger.addHandler(sh)

import time
import json
import tempfile

# import win32service
# import win32serviceutil

from rf_utils import install_location
# from rf_utils import dropbox_utils
# reload(dropbox_utils)
import config
reload(config)

def get_unique_file_name(directory, name, ext):
    num = 1
    while True:
        fn = '%s%s%s' %(name, str(num), ext)
        fp = os.path.join(directory,fn)
        fp = os.path.normpath(fp)
        if os.path.exists(fp):
            num += 1
        else:
            return fp

def submit_request(data, name='', wait_limit=1000):
    # get dropbox path
    dbRoot = install_location.get_dropbox_root()
    req_dir = ''
    log_dir = ''
    if dbRoot:
        req_dir = os.path.join(dbRoot, config.REQUEST_DIR)
        log_dir = os.path.join(req_dir, config.LOG_DIR)
    if not os.path.exists(req_dir):
        logger.error('No request directory found: %s' %req_dir)
        return
    if not os.path.exists(log_dir):
        logger.error('No log directory found: %s' %log_dir)
        return
    logger.info('Request directory: %s' %req_dir)
    logger.info('Log directory: %s' %log_dir)
    # check if service is running
    # service_name = service.RiffDropboxService._svc_name_
    # logger.info('Checking service status: %s' %service_name)
    # service_status = None
    # try:
    #     service_status = win32serviceutil.QueryServiceStatus(service_name)
    # except win32serviceutil.pywintypes.error:
    #     logger.error('Service does not exists: %s' %service_name)
    #     return

    # if service_status[1] != win32service.SERVICE_RUNNING and not debug:
    #     logger.error('Service is not running: %s' %service_name)
    #     return

    # add result temp path to the data
    tmp_file = next(tempfile._get_candidate_names())
    tmp_path = os.path.join(log_dir, tmp_file)
    logger.info('Result file: %s' %tmp_file)
    data['result_file'] = tmp_file

    unique_req_name = get_unique_file_name(directory=req_dir, name=name, ext=config.REQUEST_EXT)
    req_path = os.path.join(req_dir, unique_req_name)
    # with tempfile.NamedTemporaryFile(prefix='%s.' %name, suffix='.request', dir=req_dir, delete=False) as f:
        
    # # write request to temp
    # tmp_req_f = tempfile.NamedTemporaryFile(prefix='%s.' %unique_req_name, suffix='.request', delete=False)
    # tmp_req_path = tmp_req_f.name
    # logger.info('Temp request file: %s' %tmp_req_path)
    # with tmp_req_f as f:
    with open(req_path, 'w') as f:
        json.dump(data, f)

    # upload request to Dropbox
    # db_req_path = '/%s/%s' %(config.REQUEST_DIR, unique_req_name)
    # logger.info('Uploading to dropbox: %s' %db_req_path)
    # dropbox_utils.upload(local_path=tmp_req_path, dropbox_path=db_req_path)

    # need a first sleep to at least wait for the service to pick it up
    time.sleep(config.SERVICE_REFRESH_DURATION)

    # wait for the service result
    result = None
    st = time.time()
    time_taken = 0.0
    # downloaded = False
    try:
        while True:
            time_taken = time.time() - st
            if time_taken >= wait_limit:
                logger.error('Time out waiting for service to finish!')
                break
            if os.path.exists(tmp_path) and os.access(tmp_path, os.R_OK):
                # time.sleep(config.RESULT_REFRESH_DURATION)
                try:
                    with open(tmp_path, 'r') as f:
                        try:
                            result = json.load(f)
                        except Exception, e:
                            logger.error(e)
                            logger.error('Invalid service result: %s' %(res))
                except IOError:
                    # if not downloaded:
                    #     db_tmp_path = '/%s/%s/%s' %(config.REQUEST_DIR, config.LOG_DIR, tmp_file)
                    #     logger.info('Downloading from dropbox: %s' %db_tmp_path)
                    #     dl_result = dropbox_utils.download(dropbox_path=db_tmp_path, local_path=tmp_path)
                    #     if dl_result:
                    #         downloaded = True
                    continue
                os.remove(tmp_path)
                break
            else:
                time.sleep(config.RESULT_REFRESH_DURATION)
    except KeyboardInterrupt:
        pass
    logger.info('Service result: %s (took %s sec)' %(result, time_taken))
    return result

'''
from rf_app.bot.dropbox import submit
reload(submit)

# -----------------------------------------------
# example for submitting a copyfile request
# actions are function names and argruments 
# taken from rf_app/bot/dropbox actions.py
data = {"user": "Nu", 
        "actions": [["path_exists", "//riff-data/production/riff_production/sevenchickmovie"]]
        }

# submit a request
result = submit.submit_request(data=data, name='TestQueryScm')

print result
# [{u'action': u'copyfile', u'result': None, u'success': True}]
# result is a list(ordered by actions) of dictionary :
    # action - function name that has been run
    # result - the return result of the action
    # success - boolean telling if the action encounter an error or not 

'''