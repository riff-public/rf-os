@echo off
set service_path="%~dp0\service.py"
set service_name=RiffDropboxService

SC QUERY %service_name% > NUL
IF ERRORLEVEL 1060 GOTO INSTALL

:UPDATE
python %service_path% update
python %service_path% restart
GOTO END

:INSTALL
python %service_path% --username "%USERDOMAIN%\%USERNAME%" --password "" install

:START
python %service_path% start

:END
echo Done.
PAUSE