import os
import sys
import shutil

import logging
logger = logging.getLogger('service')

from rf_utils import install_location
from rf_utils import dropbox_utils

def copyfile(source, destination):
    source = os.path.normpath(source)
    destination = os.path.normpath(destination)
    des_dir_name = os.path.dirname(destination)
    if not os.path.exists(des_dir_name):
        logger.debug('Creating directory: %s' %des_dir_name)
        os.makedirs(des_dir_name)

    if not os.path.isfile(source):
        msg = 'Source does not exists: %s' %(source)
        logger.error(msg)
        raise RuntimeError(msg)

    logger.debug('Copying: %s ---> %s' %(source, destination))
    shutil.copyfile(source, destination)
    logger.debug('Copied: %s ---> %s' %(source, destination))

def copydir(source, destination):
    if not os.path.isdir(source):
        msg = 'Source does not exists: %s' %(source)
        logger.error(msg)
        raise RuntimeError(msg)

    for root, dirs, files in os.walk(source):
        if not os.path.isdir(root):
            os.makedirs(root)

        for file in files:
            rel_path = root.replace(source, '').lstrip(os.sep)
            dest_path = os.path.join(destination, rel_path)

            if not os.path.isdir(dest_path):
                logger.debug('Creating directory: %s' %dest_path)
                os.makedirs(dest_path)

            source_fp = os.path.join(root, file)
            des_fp = os.path.join(dest_path, file)
            logger.debug('Copying: %s ---> %s' %(source_fp, des_fp))
            shutil.copyfile(source_fp, des_fp)
            logger.debug('Copied: %s ---> %s' %(source_fp, des_fp))

def get_dropbox_root():
    dbRoot = install_location.get_dropbox_root()
    logger.debug('Dropbox root: %s' %dbRoot)
    return dbRoot

def copy_file_from_dropbox(dropbox_src, destination):
    ''' Copy a file from Dropbox local source to local/network destination '''
    dbRoot = get_dropbox_root()
    dropbox_src = dropbox_src.strip('/')
    dropbox_src = dropbox_src.strip('\\')
    db_src = os.path.join(dbRoot, dropbox_src)
    logger.debug('Dropbox file source: %s' %db_src)

    copyfile(source=db_src, destination=destination)

def download_file_from_dropbox(dropbox_src, destination):
    logger.debug('Downloading from dropbox: %s' %dropbox_src)
    dl_result = dropbox_utils.download(dropbox_path=dropbox_src, local_path=destination)
    return dl_result

def copy_file_to_dropbox(source, dropbox_destination):
    ''' Copy a file from local/network source to local Dropbox destination '''
    dbRoot = get_dropbox_root()
    dropbox_destination = dropbox_destination.strip('/')
    dropbox_destination = dropbox_destination.strip('\\')
    db_des = os.path.join(dbRoot, dropbox_destination)
    db_des = os.path.normpath(db_des)
    logger.debug('Dropbox file destination: %s' %db_des)

    req_dir = os.path.join(dbRoot, 'requests')
    db_des_dir = os.path.dirname(db_des)
    if req_dir == db_des_dir:
        msg = 'Cannot copy to Dropbox requests folder!'
        logger.error(msg)
        raise RuntimeError(msg)
        
    copyfile(source=source, destination=db_des)

def copy_dir_from_dropbox(dropbox_src, destination):
    ''' Copy a file from Dropbox local source to local/network destination '''
    dbRoot = get_dropbox_root()
    dropbox_src = dropbox_src.strip('/')
    dropbox_src = dropbox_src.strip('\\')
    db_src = os.path.join(dbRoot, dropbox_src)
    logger.debug('Dropbox dir source: %s' %db_src)

    copydir(source=db_src, destination=destination)

def copy_dir_to_dropbox(source, dropbox_destination):
    ''' Copy a file from local/network source to local Dropbox destination '''
    dbRoot = get_dropbox_root()
    dropbox_destination = dropbox_destination.strip('/')
    dropbox_destination = dropbox_destination.strip('\\')
    db_des = os.path.join(dbRoot, dropbox_destination)
    db_des = os.path.normpath(db_des)
    logger.debug('Dropbox dir destination: %s' %db_des)

    req_dir = os.path.join(dbRoot, 'requests')
    db_des_dir = os.path.dirname(db_des)
    if req_dir == db_des_dir:
        msg = 'Cannot copy to Dropbox requests folder!'
        logger.error(msg)
        raise RuntimeError(msg)
        
    copydir(source=source, destination=db_des)

def path_exists(path):
    path = os.path.normpath(path)
    logger.debug('Checking existance: %s' %path)
    result = os.path.exists(r'%s' %(path))
    return result