import os
import sys
SCRIPT_PATH = os.path.join(os.environ['RFSCRIPT'], 'core')
if not SCRIPT_PATH in sys.path:
    sys.path.append(SCRIPT_PATH)

import logging
import time
import json
import shutil
import re
from collections import OrderedDict
from datetime import datetime

from rf_utils.win_service_utils import SMWinservice
from rf_utils import install_location

import config
# reload(config)
import actions

# setup logger
logging.basicConfig(filename='log.log', level=logging.DEBUG)
logger = logging.getLogger(__name__)
formatter = logging.Formatter('%(asctime)s: %(message)s')
sh = logging.StreamHandler()
sh.setLevel(logging.INFO)
sh.setFormatter(formatter)
logger.addHandler(sh)

class RiffDropboxService(SMWinservice):
    _svc_name_ = "RiffDropboxService"
    _svc_display_name_ = "Riff dropbox service"
    _svc_description_ = "Riff's studio service for Dropbox file transfer."

    def start(self):
        dbRoot = install_location.get_dropbox_root()
        self.req_dir = ''
        self.done_dir = ''
        self.log_dir = ''
        if dbRoot:
            self.req_dir = os.path.join(dbRoot, config.REQUEST_DIR)
            self.done_dir = os.path.join(self.req_dir, config.DONE_DIR)
            self.log_dir = os.path.join(self.req_dir, config.LOG_DIR)

        if not os.path.exists(self.req_dir) or not os.path.exists(self.done_dir) or not os.path.exists(self.log_dir):
            self.isrunning = False
        else:
            self.isrunning = True

    def stop(self):
        self.isrunning = False

    def main(self):
        domain = os.environ['USERDOMAIN']
        service_account = os.environ['USERNAME']

        while self.isrunning:
            reload(actions)
            for fl in os.listdir(self.req_dir):
                fn, ext = os.path.splitext(fl)
                fp = os.path.join(self.req_dir, fl)

                # skip if it's not file with .request extionsion
                if not os.path.isfile(fp) or ext != config.REQUEST_EXT:
                    continue

                # read request on to RAM
                requst_data = str()
                with open(fp, 'r') as f:
                    request_data = f.read()
                # remove request
                os.remove(fp)
                logger.info('%s\\%s is processing: %s' %(domain, service_account, fl))

                # get request new id
                new_id = self.get_new_id()
                logger.info('Service ID: %s' %new_id)

                # new log
                log_fn = '%s.%s%s' %(new_id, fn, config.LOG_EXT)
                log_path = os.path.join(self.log_dir, log_fn)
                self.new_log(log_path)

                # re-write request with new id to done folder
                done_fn = '%s.%s%s' %(new_id, fn, config.REQUEST_EXT)
                done_path = os.path.join(self.done_dir, done_fn)
                with open(done_path, 'w') as wf:
                    wf.write(request_data)
                
                 # log header
                logger.debug('================================================')
                logger.debug('Running from: %s\\%s' %(domain, service_account))
                logger.info('Starting: %s' %(done_path))

                # do some actions...
                user, result_file, action_lists = self.parse_request(path=done_path)
                logger.info('From user: %s' %user)
                logger.info('Result file: %s' %result_file)
                logger.debug('Actions: %s' %action_lists)
                service_results = []

                if action_lists:
                    for action in action_lists:
                        func = None
                        try:
                            exec('func = actions.%s' %action[0])
                        except:
                            msg = 'Failed to load function: %s' %action[0]
                            logger.error(msg)
                            service_results.append({'action': action[0], 'success':False, 'result':msg})
                            break

                        args = action[1:]
                        argStr = ', '.join(args)
                        logger.debug('func: %s' %func)
                        logger.debug('args: %s' %args)
                        logger.info('Starting action: %s(%s)' %(action[0], argStr))
                        start_time = time.time()
                        try:
                            result = func(*args)
                            if result:
                                logger.info('Function return: %s' %(result))
                            
                            total_time = time.time() - start_time
                            logger.info('Success: %s(%s) - duration time: %s' %(action[0], argStr, total_time))
                            service_results.append({'action': action[0], 'success':True, 'result':result})
                        except Exception, e:
                            logger.error(str(e))
                            logger.error('Action failed: %s' %action)
                            service_results.append({'action': action[0], 'success':False, 'result':str(e)})
                            break

                    logger.debug('Actions done: %s' %done_path)
                else:
                    logger.error('Invalid request: %s' %done_path)
                
                if result_file:
                    result_path = os.path.join(self.log_dir, result_file)
                    self.write_service_result(data=service_results, path=result_path)

                logger.debug('================================================')
                self.close_log()

            time.sleep(config.SERVICE_REFRESH_DURATION)


    def get_new_id(self):
        requests = sorted([f for f in os.listdir(self.done_dir) if os.path.isfile(os.path.join(self.done_dir, f)) and f.endswith(config.REQUEST_EXT)], reverse=True) 
        id_str = str(1).zfill(config.ID_PADDING)
        for req in requests:
            id_match = re.match(r'^[0-9]{%s}' %config.ID_PADDING, req)
            if id_match:
                new_id = int(id_match.group()) + 1
                id_str = str(new_id).zfill(config.ID_PADDING)
                return id_str
        return id_str

    def new_log(self, log_path):
        baseName = os.path.basename(log_path)
        name, ext = os.path.splitext(baseName)

        fh = logging.FileHandler(log_path)
        fh.setLevel(logging.DEBUG)
        fh.setFormatter(formatter)
        logger.addHandler(fh)
        
    def close_log(self):
        handlers = logger.handlers[:]
        for handler in handlers:
            if isinstance(handler, logging.FileHandler):
                handler.close()
                logger.removeHandler(handler)

    def parse_request(self, path):
        data = {}
        with open(path, 'r') as f:
            try:
                data = json.load(f)
            except ValueError, e:
                return

        # get user
        user = 'unknown'
        if 'user' in data:
            user = data['user']
        else:
            logger.warning('No user specified in: %s' %path)

        # get result path
        result_file = ''
        if 'result_file' in data:
            result_file = data['result_file']
        else:
            logger.warning('No result_file found in: %s' %path)

        # get actions
        actions = []
        if 'actions' in data:
            actions = data['actions']
        else:
            logger.error('No action found in: %s' %path)
            return

        return user, result_file, actions

    def write_service_result(self, data, path):
        logger.info('Writing service result: %s %s' %(data, path))
        with open(path, 'w') as f:
            json.dump(data, f)


if __name__ == '__main__':
    RiffDropboxService.parse_command_line()



