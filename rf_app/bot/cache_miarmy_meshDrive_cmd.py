import sys
import os 
import argparse

import pymel.core as pm 
import maya.cmds as mc 

from rf_utils.miarmy import miarmy_utils
reload(miarmy_utils)


import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


def setup_parser(title):
    # create the parser
    parser = argparse.ArgumentParser(description=title, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-s', dest='scene', type=str, help='The path to the source scene to be cached', required=True)
    parser.add_argument('-d', dest='destinationPath', type=str, help='The path to the destination alembic', required=True)
    parser.add_argument('-a', dest='agents', nargs='+', help='Agent node names to cache, default will cache all', default=[])
    return parser

def main(mayaScene, destinationPath, agents=[]): 
    """ available process 
    """
    # open scene 
    try: 
        result = mc.file(mayaScene, o=True, f=True, prompt=False)
        logger.debug('Open scene completed')
    except Exception as e: 
        logger.error('Scene has opened with error')
        logger.error(e)

    res = miarmy_utils.do_export_abc_from_agents(abcPath=destinationPath, agents=agents)
    logger.info(res)
    logger.info('** Finished caching Miarmy meshDrive to ABC.')

if __name__ == "__main__":
    # print sys.argv
    logger.info('## Start Cache process')
    parser = setup_parser('Export alembic cache from Miarmy meshDrive')
    params = parser.parse_args()    
    main(mayaScene=params.scene, destinationPath=params.destinationPath, agents=params.agents)
    
'''
example:
"C:/Program Files/Autodesk/Maya2018/bin/mayapy.exe" D:/dev/core/rf_app/bot/cache_miarmy_meshDrive_cmd.py -s "D:/__playground/test_miarmy001.ma" -d "D:/__playground/test_meshDrive/md_001.abc"

'''