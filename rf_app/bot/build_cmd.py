uiName = 'CacheQueue' 
import sys
import os 
import argparse
import getpass

import pymel.core as pm 
import maya.cmds as mc 

from rf_utils.context import context_info
from rf_utils import file_utils
from rf_app.publish.scene import app
import rf_app.publish.scene.export as export
reload(app)
from rftool.utils import export_yeti_cache
from rf_utils import log_utils
from rf_utils.pipeline import shot_data


import logging
uiName = 'AutoBuild'

user = '%s-%s' % (os.environ['RFUSER'], getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)


def setup_parser(title):
    # create the parser
    parser = argparse.ArgumentParser(description=title, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-s', dest='scene', type=str, help='The path to the source scene to be built', required=True)
    parser.add_argument('-step', dest='step', type=str, help='Department to cache to build', default='techanim')
    return parser


def main(mayaScene, step='techanim', buildFilter=[], muteFilter=[]): 
    from rf_app.scene.builder.model import scene_builder_model
    reload(scene_builder_model)
    from rftool.fix import moveSceneToOrigin
    reload(moveSceneToOrigin)
    from rf_utils.pipeline import file_name_prefix
    reload(file_name_prefix)

    # new scene 
    scene = context_info.ContextPathInfo(path=mayaScene)
    scene.context.update(step=step)
    newScene = build_scene_name(scene)

    mc.file(new=True, f=True)
    result = save_file(newScene)
    logger.info('** Build new scene %s' % newScene)

    # normal build 
    scene_builder_model.build(buildType='cache', filterList=True, buildFilter=buildFilter, muteFilter=muteFilter)
    ws_data_path = shot_data.get_shot_data_path(entity=scene)
    offset = [0.0, 0.0, 0.0]
    
    if os.path.exists(ws_data_path): 
        data = file_utils.ymlLoader(ws_data_path)
        offset = data[moveSceneToOrigin.CAM_DIST_KEY]
        offset = [round(offset[0], 2)*-1, round(offset[1], 2)*-1, round(offset[2], 2)*-1]
    
    # attach groom (offset) 
    moveSceneToOrigin.main(offset)
    
    # add name prefix
    file_name_prefix.file_name_prefix_playblast()

    result = save_file(newScene)
    qcFile = copy_to_qc(result)
    logger.info('** Build scene and copy to: "%s"' % qcFile)

    # submit_playblast_job(qcFile)
    # logger.info('** Submit scene to render farm: "%s"' % qcFile)


    logger.info('** Save result %s' % newScene)
    logger.info('** Build Complete!!\n')
    return result

def build_outsource(mayaScene, step='light', buildFilter=[], muteFilter=[]): 
    from rf_app.scene.builder.model import scene_builder_model
    reload(scene_builder_model)
    from rftool.fix import moveSceneToOrigin
    reload(moveSceneToOrigin)
    from rf_utils.pipeline import file_name_prefix
    reload(file_name_prefix)

    # new scene 
    scene = context_info.ContextPathInfo(path=mayaScene)
    scene.context.update(step=step, process='outsource')
    newScene = build_scene_name(scene)

    mc.file(new=True, f=True)
    result = save_file(newScene)
    logger.info('** Build new scene %s' % newScene)

    # normal build 
    scene_builder_model.build(buildType='cache', filterList=True, buildFilter=buildFilter, muteFilter=muteFilter)
    # ws_data_path = shot_data.get_shot_data_path(entity=scene)
    # offset = [0.0, 0.0, 0.0]
    
    # if os.path.exists(ws_data_path): 
    #     data = file_utils.ymlLoader(ws_data_path)
    #     offset = data[moveSceneToOrigin.CAM_DIST_KEY]
    #     offset = [round(offset[0], 2)*-1, round(offset[1], 2)*-1, round(offset[2], 2)*-1]
    
    # # attach groom (offset) 
    # moveSceneToOrigin.main(offset)
    
    # add name prefix
    file_name_prefix.file_name_prefix_playblast()

    result = save_file(newScene)
    # qcFile = copy_to_qc(result)
    # logger.info('** Build scene and copy to: "%s"' % qcFile)

    # submit_playblast_job(qcFile)
    # logger.info('** Submit scene to render farm: "%s"' % qcFile)


    logger.info('** Save result %s' % newScene)
    logger.info('** Build Complete!!\n')
    return result


def build_scene_name(scene): 
    path = scene.path.workspace().abs_path()
    workfile = scene.work_name()
    elem = '%s_autoBuild' % workfile.split('.')[0]
    filename = '%s.%s' % (elem, '.'.join(workfile.split('.')[1:]))
    return '%s/%s' % (path, filename)


def save_file(filepath):
    if not os.path.exists(os.path.dirname(filepath)): 
        os.makedirs(os.path.dirname(filepath))
    name, ext = os.path.splitext(filepath)
    filetype = 'mayaAscii' if ext == '.ma' else 'mayaBinary' if ext == '.mb' else ''
    mc.file(rename=filepath)
    result = mc.file(save=True, f=True, type=filetype)
    return result

def copy_to_qc(path):
    srcPath = path
    scene = context_info.ContextPathInfo(path=path)
    scene.context.update(step='anim', process='qc')
    path = scene.path.workspace().abs_path()
    workfile = scene.work_name()
    elem = '%s_autoBuild' % workfile.split('.')[0]
    filename = '%s.%s' % (elem, '.'.join(workfile.split('.')[1:]))
    qcFile = '%s/%s' % (path, filename)
    if not os.path.exists(path): 
        os.makedirs(path)

    if os.path.exists(path): 
        file_utils.copy(srcPath, qcFile)

    return qcFile


# def submit_playblast_job(mayaFile): 
#     from rftool.scene.playblast import utils
#     from rftool.scene.playblast import hook
#     task_name = 'cache'
#     scene = context_info.ContextPathInfo(path=mayaFile)
#     shotName = scene.name_code
#     camera = hook.camera_info(shotName)
#     startFrame, endFrame, seqStartFrame, seqEndFrame = hook.shot_info(shotName)

#     utils.playblast_to_queue(scene, startFrame, endFrame, camera, shotName, task_name=task_name, sg_upload=True)


if __name__ == "__main__":
    # print sys.argv
    logger.info('** Start Build process')
    parser = setup_parser('Build params')
    params = parser.parse_args()    
    main(mayaScene=params.scene, step=params.step)
    
"""
example 
# default build
from rf_app.bot import build_cmd
reload(build_cmd)
build_cmd.main('P:/projectName/scene/work/ep01/pr_ep01_q0040_s0010/techanim/main/maya/pr_ep01_q0040_s0010_techanim_main.v001.TA.ma')
"""

"""
from rf_utils.deadline import cache_all
cmds = ["O:/Pipeline/core/rf_app/bot/cache_cmd.py", '-s', "P:/projectName/scene/work/ep01/pr_ep01_q0010_s0020/anim/main/maya/pr_ep01_q0010_s0020_anim_main.v005.TA.ma"]
cache_all.process_send_to_farm(cmds)
"""

# P:/projectName/scene/work/ep01/pr_ep01_q0040_s0010/anim/main/maya/pr_ep01_q0040_s0010_anim_main.v001.TA.ma