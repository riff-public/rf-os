import sys, os
import json

import maya.cmds as mc
import maya.mel as mel
import pymel.core as pm
from collections import OrderedDict

import argparse
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

# from rf_utils.context import context_info
from rftool.utils import export_yeti_cache
reload(export_yeti_cache)
from rf_utils import file_utils

path = 'P:/SevenChickMovie/_data/misc/yeti_cache.yml'

def setup_parser(title):
    # create the parser
    parser = argparse.ArgumentParser(description=title, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-s', dest='scene', type=str, help='The path to the source scene to be cached', required=True)
    parser.add_argument('-l', dest='log', type=str, help='The path to temp log file for result', default='')
    parser.add_argument('-n', dest='namespaces', nargs='+', help='The namespaces of asset to be cached', default=[])
    return parser


if __name__ == "__main__":
    print ':::::: Export Yeti Cache ::::::'
    logger.info(':::::: Export Yeti Cache ::::::')

    parser = setup_parser('Export Yeti Cache')
    params = parser.parse_args()
    
    data = file_utils.ymlLoader(path) if os.path.exists(path) else OrderedDict()
    data[params.scene] = False 
    file_utils.ymlDumper(path, data)

    export_yeti_cache.main(scene=params.scene, 
        namespaces=params.namespaces, 
        log=params.log)

    data = file_utils.ymlLoader(path) if os.path.exists(path) else OrderedDict()
    data[params.scene] = True 
    file_utils.ymlDumper(path, data)



'''
"C:/Program Files/Autodesk/Maya2017/bin/mayapy.exe" "D:/dev/core/rf_maya/rftool/utils/export_yeti_cache.py" -s "P:/projectName/scene/work/ep01/pr_ep01_q0050_s0110/anim/main/maya/pr_ep01_q0050_s0110_anim_main.v001.Nu.ma" -n "canis_001_Tech"
'''