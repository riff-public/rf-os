from rf_maya.lib import playblast_lib
from rf_utils.context import context_info
reload(context_info)
from rf_utils.sg import sg_process
reload(sg_process)
from rf_utils import register_shot
reload(register_shot)

import sys
import os 
import argparse
from datetime import datetime 

import pymel.core as pm 
import maya.cmds as mc 

from rf_utils.context import context_info
from rf_utils.sg import sg_process 

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

def setup_parser(title):
    # create the parser
    # modules = list_options()
    # moduleHelp = ('\\n').join(modules)
    parser = argparse.ArgumentParser(description=title, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-s', dest='scene', type=str, help='The path to the source scene to be cached', required=True)
    # parser.add_argument('-step', dest='step', type=str, help='Department to cache. If not specify, "anim" is default')
    # parser.add_argument('-n', dest='namespaces', nargs='+', help='The namespace to export. If not specify, all namespaces will be exported.', default=[])
    # parser.add_argument('-m', dest='modules', nargs='+', help='Modules name to run. available modules are %s' % moduleHelp, default=[])
    # parser.add_argument('-a', dest='assets', nargs='+', help='The asset to export. If not specify, all assets will be exported.', default=[])
    # parser.add_argument('-c', dest='camera', nargs='+', help='The shot camera.', default=[])
    # parser.add_argument('-fcl', dest='focalLength', nargs='+', help='focalLength of this camera.', default=[])
    return parser

# def list_options(): 
#     return export.cache_module_data().keys()

def main(mayaScene):
	# path = mc.file(q=True, loc=True)
	path = mayaScene
	result = mc.file(path, o=True, f=True, prompt=False)
	scene = context_info.ContextPathInfo(path=path)
	projName = scene.project
	shotName = scene.name #'scm_act1_q0010a_s0010'
	duration = sg_process.get_shot_duration(projName, shotName)
	startFrame = scene.projectInfo.render.start_frame()
	endFrame = startFrame + (duration-1)
	reg = register_shot.Register(scene)
	camShot = reg.get.camera()
	camera = '%s:camshotShape' % camShot[0]
	outputPath = 'E:/test/test_render_farm' # change
	outputName = 'test_render' # change
	#playblast_lib.add_preview_light_playblast()
	#sn = mc.file(q=True,loc=True)
	#fileName = r'P:\SevenChickMovie\scene\work\act1\scm_act1_q0010a_s0010\anim\main\maya\scm_act1_q0010a_s0010_anim_split.v004.ma'
	#mc.file(fileName,o=True,f=True)
	#mc.render(batch=True)
	#cam = mc.camera()
	#mc.render( cam[0], x=768, y=576, b=True)
	mc.setAttr("defaultRenderGlobals.currentRenderer", "mayaHardware", type="string") # <<< set renderer # change
	mc.workspace(fr=("images",outputPath)) # <<< set output path
	mc.setAttr ("defaultRenderGlobals.animation", 1) # <<< set animation on
	mc.setAttr ("defaultRenderGlobals.animationRange", 0) # <<< set animation range to Render Settings
	mc.setAttr ("defaultRenderGlobals.startFrame", startFrame) # <<< get st frame
	mc.setAttr ("defaultRenderGlobals.endFrame", endFrame) # <<< get end frame
	mc.setAttr ("defaultRenderGlobals.outFormatControl", 0) # <<< set out format to as output format
	mc.setAttr ("defaultRenderGlobals.putFrameBeforeExt", 1) # <<< set format to name.####.ext
	mc.setAttr ("defaultRenderGlobals.imageFormat", 7) # <<< .ext 7=.iff ex:(.iff, .exr, .jpg)\  # change
	mc.setAttr ("defaultRenderGlobals.imageFilePrefix", outputName, type='string') # <<< set output fileName
	mc.setAttr ("perspShape.renderable", 0)
	mc.setAttr ("frontShape.renderable", 0)
	mc.setAttr ("sideShape.renderable", 0)
	mc.setAttr ("topShape.renderable", 0)
	mc.setAttr ("%s.renderable" % camera, 1)# <<< get camera name
	#set time stFrame endFrame
	#set .ext
	#playblast_lib.add_preview_light_playblast()
	mc.render(camera, batch=True)

	#mc.file(q=True,sn=True)

	#camera_ = mc.modelPanel('modelPanel4',cam=True,q=True)
	#camera_ = 's0010_cam:camshot'
	#print mc.nodeType('s0010_cam:camshotShape')

	#print mc.listRelatives('s0010_cam:camshot')


# def set_sg_status(scene, task, status): 
#     """ set status for anim and tech anim to check if auto cache and autobuild works """ 
#     # find cache and tech anim task 

#     # find task 
#     filters = [['project.Project.name', 'is', scene.project], ['entity', 'is', scene.context.sgEntity], ['content', 'is', task]]
#     fields = ['content', 'id']
#     taskEntity = sg_process.sg.find_one('Task', filters, fields)

#     # update task 
#     data = {'sg_status_list': status}
#     sg_process.sg.update('Task', taskEntity['id'], data)


if __name__ == "__main__":
    # print sys.argv
    logger.info('## Start Render process')
    parser = setup_parser('Render params')
    params = parser.parse_args()    
    main(mayaScene=params.scene)