uiName = 'CacheQueue' 
import sys
import os 
import argparse
from datetime import datetime 

import pymel.core as pm 
import maya.cmds as mc 

from rf_utils.context import context_info
from rf_utils import file_utils
from rf_app.publish.scene import app
import rf_app.publish.scene.export as export
reload(app)
from rf_utils.context import context_info
from rftool.utils import export_yeti_cache
from rf_utils.sg import sg_process 

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


class Modules: 
    default = ['camera',
         # 'alembic_cache',
         'cache_single',
         'export_grp',
         'extracam_grp',
         'export_set',
         'instance_dress',
         'set_asset_list',
         'shotdress',
         'shot_workspace',
         'yeti_bg',
         'miarmy_rsproxy']

class Preset: 
    stepMap = {'anim': {'task':'anim', 'step':'anim', 'process':'main', 'mod': Modules.default}, 
                'default': {'task':'anim', 'step':'anim', 'process':'main', 'mod': Modules.default}}

def setup_parser(title):
    # create the parser
    modules = list_options()
    moduleHelp = ('\\n').join(modules)
    parser = argparse.ArgumentParser(description=title, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-s', dest='scene', type=str, help='The path to the source scene to be cached', required=True)
    parser.add_argument('-step', dest='step', type=str, help='Department to cache. If not specify, "anim" is default')
    parser.add_argument('-build', dest='build', type=str, help='status to cache.')
    parser.add_argument('-user', dest='user', type=str, help='User who sent jobs')
    parser.add_argument('-n', dest='namespaces', nargs='+', help='The namespace to export. If not specify, all namespaces will be exported.', default=[])
    parser.add_argument('-m', dest='modules', nargs='+', help='Modules name to run. available modules are %s' % moduleHelp, default=[])
    parser.add_argument('-a', dest='assets', nargs='+', help='The asset to export. If not specify, all assets will be exported.', default=[])
    parser.add_argument('-c', dest='camera', nargs='+', help='The shot camera.', default=[])
    parser.add_argument('-fcl', dest='focalLength', nargs='+', help='focalLength of this camera.', default=[])
    return parser



def main(mayaScene, step='anim', namespaces=[], modules=[], assets=[], camera=[], focalLength=[], user=None, build=None): 
    """ available process 
        ['camera',
         'workspace',
         'cache',
         'alembic_cache',
         'anim_mov',
         'auto_build',
         'cache_single',
         'export_grp',
         'export_set',
         'instance_dress',
         'mov',
         'set_asset_list',
         'set_status',
         'shotdress',
         'shot_workspace',
         'yeti_bg',
         'yeti_cache',
         'yeti_cache_bg',
         'yeti_groom']
    """
    # open scene 
    start = datetime.now()
    try: 
        result = mc.file(mayaScene, o=True, f=True, prompt=False)
        logger.debug('Open scene completed')
    except Exception as e: 
        logger.error('Scene has opened with error')
        logger.error(e)

    # setup export data 
    config = Preset.stepMap.get('anim', Preset.stepMap.get('default'))
    scene = context_info.ContextPathInfo(path=mayaScene)
    modules = config['mod'] if not modules else modules

    scene.context.use_sg(sg_process.sg, scene.project, entityType='scene', entityName=scene.name)
    if not scene.step or not scene.process:
        scene.context.update(task=config['task'], step=config['step'], process=config['process'])

    # exportData = app.get_export_dict(scene, scene.name_code, ['camera', 'alembic_cache', 'shotdress'])

    # detect yeti 
    cacheYeti = True if 'yeti_bg' in modules else False 
    modules.remove('yeti_bg') if cacheYeti else None

    print 'cacheYeti ============', cacheYeti

    exportData = app.get_export_dict(scene, scene.name_code, modules)
    filterExportData = []
    filterExportData, exportNs = filter_namespaces(exportData, namespaces, assets) if namespaces or assets else (exportData, [])

    for item in filterExportData: 
        display = item.get('display')
        funcType = item.get('type')
        args = item.get('args')
        logger.info('Item to export: %s' % display)



    logger.info('Start caching ...')

    if scene.step == config['step'] and eval(build):
        set_sg_status(scene, 'cache', 'ip')

    result = app.do_export(scene, itemDatas=filterExportData, uiMode=False, publish=True, user=user)
    print '---- Finished cached in %s ----' % (datetime.now() - start)

    if result: 
        successItems, failedItems = result
        if not failedItems: 
            logger.info('Export Finished!!')

        else: 
            logger.warning('Finished with error')
            logger.info(failedItems)

    if cacheYeti: 
        exportNs = [a for a in exportNs if '_Tech' in a] 
        print 'exportNs', exportNs
        logger.info('** Caching yeti ...')
        export_yeti_cache.main(scene=mayaScene, namespaces=exportNs)
        print '---- Finished cached yeti in %s ----' % (datetime.now() - start)
        # else: 
        #     logger.info('No Tech namespace, Skip yeti cache')

    # else: 
    #     if not namespaces: 
    #         logger.info('** Caching yeti ...')
    #         export_yeti_cache.main(scene=mayaScene, namespaces=namespaces)
    #         print '---- Finished cached yeti in %s ----' % (datetime.now() - start)
    
    # set status 
    if scene.step == config['step'] and eval(build):
        set_sg_status(scene, 'cache', 'rev')
    logger.info('** Finished caching Yeti!!')
    buildResult = '***  Not Build ****'

    if scene.step == config['step'] and eval(build):
        logger.info('Build ...')
        import build_cmd
        reload(build_cmd)
        
        try: 
            buildResult = build_cmd.main(mayaScene,muteFilter=['ref_data','fx_cache'])
        except Exception as e: 
            buildResult = '***  Not Build ****'
            logger.error('Build failed')
        
        # set status 
        set_sg_status(scene, 'techAnim', 'rev')
    logger.info('Build finished %s' % buildResult)
    finish = datetime.now() - start
    print '======== Finished all process in %s ==========' % finish


def filter_namespaces(exportData, namespaces, assets=[]): 
    filterExportData = []
    exportNs = []

    for item in exportData: 
        display = item.get('display')
        funcType = item.get('type')
        args = item.get('args')

        if funcType in ['cache', 'camera', 'miarmy_rscache', 'set', 'import_data', 'asm']: 
            funcDicts = args[0]

            for exportType, funcDict in item.get('args')[0].iteritems():
                namespace = funcDict['namespace']
                # funcType = funcDict['funcType']
                # dataType = funcDict['dataType']
                # outputNs = funcDict['outputNs']
                # outputKey = funcDict['outputKey']
                # schemaKey = funcDict['schemaKey']
                # node = funcDict.get('node', '')
                # key = funcDict.get('key', namespace)
                # ref = funcDict.get('ref', '')
                # exportNamespace = funcDict.get('exportNs', '')
                # outputHeroKey = funcDict.get('outputHeroKey', '')
                # publish = funcDict.get('publish', False)

                if namespaces: 
                    add = True if namespace in namespaces else False 
                if assets: 
                    add = True if any([True for a in assets if a in namespace]) else False
                if add: 
                    filterExportData.append(item) if not item in filterExportData else None
                    exportNs.append(namespace)

        # else: 
        #     filterExportData.append(item) if not item in filterExportData else None

    return filterExportData, exportNs

def list_options(): 
    return export.cache_module_data().keys()


def set_sg_status(scene, task, status): 
    """ set status for anim and tech anim to check if auto cache and autobuild works """ 
    # find cache and tech anim task 

    # find task 
    filters = [['project.Project.name', 'is', scene.project], ['entity', 'is', scene.context.sgEntity], ['content', 'is', task]]
    fields = ['content', 'id']
    taskEntity = sg_process.sg.find_one('Task', filters, fields)

    # update task 
    data = {'sg_status_list': status}
    sg_process.sg.update('Task', taskEntity['id'], data)


if __name__ == "__main__":
    # print sys.argv
    logger.info('## Start Cache process')
    parser = setup_parser('Cache params')
    params = parser.parse_args()    
    main(mayaScene=params.scene, step=params.step, namespaces=params.namespaces, modules=params.modules, assets=params.assets, camera=params.camera, focalLength=params.focalLength, user=params.user, build=params.build)
    
"""
example 
# default cache 
mayapy.exe cache_cmd -s "P:/projectName/scene/work/ep01/pr_ep01_q0010_s0020/anim/main/maya/pr_ep01_q0010_s0020_anim_main.v005.TA.ma" 
# department cache 
mayapy.exe cache_cmd -s "P:/projectName/scene/work/ep01/pr_ep01_q0010_s0020/anim/main/maya/pr_ep01_q0010_s0020_anim_main.v005.TA.ma" -step "anim" 
# specific namespace cache 
mayapy.exe cache_cmd -s "P:/projectName/scene/work/ep01/pr_ep01_q0010_s0020/anim/main/maya/pr_ep01_q0010_s0020_anim_main.v005.TA.ma" -n "arthur_001" "canis_001" -m "cache_single" 
"""

"""
from rf_utils.deadline import cache_all
cmds = ["O:/Pipeline/core/rf_app/bot/cache_cmd.py", '-s', "P:/projectName/scene/work/ep01/pr_ep01_q0010_s0020/anim/main/maya/pr_ep01_q0010_s0020_anim_main.v005.TA.ma"]
cache_all.process_send_to_farm(cmds)
"""

# mayapy.exe cache_cmd -s "P:/projectName/scene/work/ep01/pr_ep01_q0040_s0010/anim/main/maya/pr_ep01_q0040_s0010_anim_main.v001.TA.ma" 