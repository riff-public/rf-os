uiName = 'CacheQueue' 
import sys
import os 
import argparse
from datetime import datetime 

import pymel.core as pm 
import maya.cmds as mc 

from rf_utils.context import context_info
from rf_utils import file_utils
from rf_app.publish.scene import app
import rf_app.publish.scene.export as export
reload(app)
from rf_utils.context import context_info
from rftool.utils import export_yeti_cache
from rf_utils.sg import sg_process 
from rf_utils.miarmy import miarmy_utils
reload(miarmy_utils)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


class Modules: 
    default = ['miarmy_rsproxy']

class Preset: 
    stepMap = {'crowd': {'task':'crowd', 'step':'crowd', 'process':'main', 'mod': Modules.default}, 
                'default': {'task':'crowd', 'step':'crowd', 'process':'main', 'mod': Modules.default}}

def setup_parser(title):
    # create the parser
    modules = list_options()
    moduleHelp = ('\\n').join(modules)
    parser = argparse.ArgumentParser(description=title, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-s', dest='scene', type=str, help='The path to the source scene to be cached', required=True)
    parser.add_argument('-step', dest='step', type=str, help='Department to cache. If not specify, "anim" is default')
    parser.add_argument('-n', dest='namespaces', nargs='+', help='The namespace to export. If not specify, all namespaces will be exported.', default=[])
    parser.add_argument('-m', dest='modules', nargs='+', help='Modules name to run. available modules are %s' % moduleHelp, default=[])
    parser.add_argument('-a', dest='assets', nargs='+', help='The asset to export. If not specify, all assets will be exported.', default=[])
    parser.add_argument('-c', dest='camera', nargs='+', help='The shot camera.', default=[])
    parser.add_argument('-fcl', dest='focalLength', nargs='+', help='focalLength of this camera.', default=[])
    return parser


def main(mayaScene): 
    """ available process 
    """
    # open scene 
    start = datetime.now()
    try: 
        result = mc.file(mayaScene, o=True, f=True, prompt=False)
        logger.debug('Open scene completed')
    except Exception as e: 
        logger.error('Scene has opened with error')
        logger.error(e)

    # prep scene 
    miarmy_utils.prep_cache()

    # setup export data 
    config = Preset.stepMap.get('crowd', Preset.stepMap.get('default'))
    scene = context_info.ContextPathInfo(path=mayaScene)
    modules = Modules.default

    scene.context.use_sg(sg_process.sg, scene.project, entityType='scene', entityName=scene.name)
    scene.context.update(task=config['task'], step=config['step'], process=config['process'])

    # exportData = app.get_export_dict(scene, scene.name_code, ['camera', 'alembic_cache', 'shotdress'])

    exportData = app.get_export_dict(scene, scene.name_code, modules)
    filterExportData = exportData

    logger.info('Start caching ...')
    set_sg_status(scene, 'crowd', 'ip')
    result = app.do_export(scene, itemDatas=filterExportData, uiMode=False, publish=True)
    print '---- Finished cached in %s ----' % (datetime.now() - start)


    # set status 
    set_sg_status(scene, 'crowd', 'rev')
    logger.info('** Finished caching Yeti!!')


def list_options(): 
    return export.cache_module_data().keys()


def set_sg_status(scene, task, status): 
    """ set status for anim and tech anim to check if auto cache and autobuild works """ 
    # find cache and tech anim task 

    # find task 
    filters = [['project.Project.name', 'is', scene.project], ['entity', 'is', scene.context.sgEntity], ['content', 'is', task]]
    fields = ['content', 'id']
    taskEntity = sg_process.sg.find_one('Task', filters, fields)

    # update task 
    if taskEntity: 
        data = {'sg_status_list': status}
        sg_process.sg.update('Task', taskEntity['id'], data)


if __name__ == "__main__":
    # print sys.argv
    logger.info('## Start Cache process')
    parser = setup_parser('Cache params')
    params = parser.parse_args()    
    main(mayaScene=params.scene)
    
"""
example 
# default cache 
mayapy.exe cache_cmd -s "P:/projectName/scene/work/ep01/pr_ep01_q0010_s0020/anim/main/maya/pr_ep01_q0010_s0020_anim_main.v005.TA.ma" 
# department cache 
mayapy.exe cache_cmd -s "P:/projectName/scene/work/ep01/pr_ep01_q0010_s0020/anim/main/maya/pr_ep01_q0010_s0020_anim_main.v005.TA.ma" -step "anim" 
# specific namespace cache 
mayapy.exe cache_cmd -s "P:/projectName/scene/work/ep01/pr_ep01_q0010_s0020/anim/main/maya/pr_ep01_q0010_s0020_anim_main.v005.TA.ma" -n "arthur_001" "canis_001" -m "cache_single" 
"""

"""
from rf_utils.deadline import cache_all
cmds = ["O:/Pipeline/core/rf_app/bot/cache_cmd.py", '-s', "P:/projectName/scene/work/ep01/pr_ep01_q0010_s0020/anim/main/maya/pr_ep01_q0010_s0020_anim_main.v005.TA.ma"]
cache_all.process_send_to_farm(cmds)
"""

# mayapy.exe cache_cmd -s "P:/projectName/scene/work/ep01/pr_ep01_q0040_s0010/anim/main/maya/pr_ep01_q0040_s0010_anim_main.v001.TA.ma" 