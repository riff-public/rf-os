_title = 'Asset Loader UI'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'AssetLoaderUI'

#Import python modules
import sys
import os 
import json 
import getpass
import re

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# # config file 
# config_file = '%s/local_config.json' % moduleDir
# with open(config_file) as configData: 
#     localConfig = json.load(configData)

# SCRIPT = localConfig['SCRIPT']
# ROOT = os.environ.get(SCRIPT)
# core = localConfig['CORE']

# # import config
# sys.path.append('%s/%s' % (ROOT, core))
import rf_config as config

# framework modules 
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet
from rf_utils import icon
from rf_utils import file_utils
from rf_utils.widget import media_widget
reload(media_widget)

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import ui 
reload(ui)

import manager
reload(manager)

from rf_utils.context import context_info
reload(context_info)

from functools import partial
import maya.OpenMaya as om

from rf_maya.contextMenu.dag import switch_ref_menu

w = 1150
h = 680
iconSize = QtCore.QSize(115,115)
maxIconSize = QtCore.QSize(170,170)
widgetSize = QtCore.QSize(119,137)
iconBgColor = QtGui.QColor(128, 128, 128)
fonts = QtGui.QFont("Times", 10, QtGui.QFont.Bold)
bgColor = '#283747'
bgWarnColor = '#9B4040'
assetWidgetStyleSheet = "QListWidget:item:selected {background-color:rgb(130,252,123);height: 40px;border: 4px solid rgb(130,252,123);color:black;}"
mainPath = '%s/core' % os.environ['RFSCRIPT']
iconCamera = '%s/icons/camera.open.svg' % mainPath

class AssetUI(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        #Setup Window
        super(AssetUI, self).__init__(parent)

        self.types = ['char', 'prop', 'others']
        self.ui = ui.Ui()
        self.setCentralWidget(self.ui)

        self.allAssetData = {}
        self.list_item = []

        self.get_asset_data_from_scene()
        self.sort_asset_data()
        self.create_asset_widgets()
        self.add_asset_widgets()

        self.add_shot_detail()
        self.set_shot_detail()

        self.setObjectName(uiName)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.resize(w,h)
        self.setWindowIcon(QtGui.QIcon("%s/icons/shelves/assetLoader.png"%mainPath)); 

        self.connect_signal()
        self.start_api()

    def get_asset_data_from_scene(self):
        assetManager = manager.Manager()
        self.allAssetData = assetManager.list_all_ref()

    def sort_asset_data(self):
        self.splitAsset = {}
        for assetName, assetData in self.allAssetData.iteritems():
            typ = assetData.assetType if assetData.assetType else 'others'
            if not typ in self.types:
                typ = 'others'
            if not typ in self.splitAsset:
                self.splitAsset[typ] = [assetName]
            else:
                assetList = self.splitAsset[typ]
                assetList.append(assetName)
                assetList.sort()
                self.splitAsset[typ] = assetList

    def create_asset_widgets(self):
        self.ui.assetCharListWidget.clear()
        self.ui.assetPropListWidget.clear()
        self.ui.assetOthersListWidget.clear()
        self.charList = []
        self.propList = []
        self.othersList = []
        for assetType, assetList in self.splitAsset.iteritems():
            for assetName in assetList:
                search_key = self.ui.searchText.text()
                if search_key:
                    if not search_key in assetName:
                        continue

                asset = self.allAssetData[assetName]

                if assetType != 'others':
                    assetEntity = context_info.ContextPathInfo(path = asset.refPath)
                    assetEntity.context.update(step='lookdev', process='main')
                    iconDir = assetEntity.path.scheme(key='outputHeroImgPath').abs_path()
                    try:
                        iconName = os.listdir(iconDir)[0]
                        iconPath = '%s/%s' % (iconDir, iconName)
                    except:
                        assetEntity.context.update(step='model', process='main')
                        iconDir = assetEntity.path.scheme(key='outputHeroImgPath').abs_path()
                        try:
                            iconName = os.listdir(iconDir)[0]
                            iconPath = '%s/%s' % (iconDir, iconName)
                        except:
                            iconPath = icon.nopreview
                else:
                    iconPath = icon.nopreview
                    for ns in asset.copies:
                        if ns.endswith('_cam'):
                            iconPath = iconCamera

                if assetType == 'char':
                    item = AssetItem()
                    self.charList.append(item)
                elif assetType == 'prop':
                    item = AssetItem()
                    self.ui.assetPropListWidget.setItemWidget(item, item.checkBox)
                    self.propList.append(item)
                elif assetType == 'others':
                    item = AssetItem()
                    self.ui.assetOthersListWidget.setItemWidget(item, item.checkBox)
                    self.othersList.append(item)

                item.setText('    %s (%s)'%(assetName,len(asset.copies)))
                item.setFont(fonts)
                item.setTextAlignment(QtCore.Qt.AlignLeft);
                item.checkBox.setStyleSheet("QCheckBox::indicator { width: 15px; height: 15px;}")
                for ns, path in asset.copies.iteritems():
                    state = asset.check_load_state(path)
                    if state == True:
                        state = QtCore.Qt.Checked
                        break
                    elif state == False:
                        state = QtCore.Qt.Unchecked
                item.checkBox.setCheckState(state)


                pixMap = QtGui.QPixmap(iconPath)
                # scale the pixmap to match max icon size
                scaledPixMap = pixMap.scaled(maxIconSize, QtCore.Qt.KeepAspectRatio)

                # create a canvas pixmap, filled with transparent
                canvasPixmap = QtGui.QPixmap(maxIconSize.width(), maxIconSize.height())
                canvasPixmap.fill(iconBgColor)
                
                # paint the scaledPixmap on top of canvasPixmap
                painter = QtGui.QPainter(canvasPixmap)
                painter.setRenderHints(QtGui.QPainter.Antialiasing | QtGui.QPainter.SmoothPixmapTransform)

                # find draw point
                drawPoint = QtCore.QPoint(0, 0)
                width = scaledPixMap.width()
                height = scaledPixMap.height()
                if width != height:
                    if width > height:
                        drawPoint = QtCore.QPoint(0, (canvasPixmap.height() - height) * 0.5)
                    elif width < height:
                        drawPoint = QtCore.QPoint((canvasPixmap.width() - width) * 0.5, 0)
                painter.drawPixmap(drawPoint, scaledPixMap)
                # painter.setCompositionMode(QtGui.QPainter.CompositionMode_SourceOver)
                painter.end()

                iconWidget = QtGui.QIcon()
                iconWidget.addPixmap(canvasPixmap, QtGui.QIcon.Normal, QtGui.QIcon.Off)
                item.setIcon(iconWidget)
                item.setData(QtCore.Qt.UserRole,asset)
                item.setSizeHint(widgetSize)
                item.setBackgroundColor(bgColor)
                item.checkBox.stateChanged.connect(partial(self.check,item))
                toolTipList = []
                toolTipText = ''
                for ns, path in asset.copies.iteritems():
                    fn = path.split('/')[-1]
                    if '{' in fn:
                        fn = fn.split('{')[0]
                    if not fn in toolTipList:
                        toolTipList.append(fn)

                for fn in toolTipList:
                    if assetType != 'others':
                        if not '_md' in fn:
                             item.setBackgroundColor(bgWarnColor)
                    toolTipText += '%s'%fn
                    if fn != toolTipList[-1]:
                        toolTipText += '\n'


                item.setToolTip(toolTipText)

            self.ui.assetCharListWidget.setIconSize(iconSize)
            self.ui.assetPropListWidget.setIconSize(iconSize)
            self.ui.assetOthersListWidget.setIconSize(iconSize)
            self.ui.assetCharListWidget.setStyleSheet(assetWidgetStyleSheet);
            self.ui.assetPropListWidget.setStyleSheet(assetWidgetStyleSheet);
            self.ui.assetOthersListWidget.setStyleSheet(assetWidgetStyleSheet);

    def add_asset_widgets(self):
        for index in xrange(self.ui.assetCharListWidget.count()):
            item = self.ui.assetCharListWidget.item(index)
            item.setHidden(True)
        for index in xrange(self.ui.assetPropListWidget.count()):
            item = self.ui.assetPropListWidget.item(index)
            item.setHidden(True)
        for index in xrange(self.ui.assetOthersListWidget.count()):
            item = self.ui.assetOthersListWidget.item(index)
            item.setHidden(True)

        search_key = self.ui.searchText.text()
        for item in self.charList:
            if search_key:
                asset = item.data(QtCore.Qt.UserRole)
                if not search_key in asset.assetName:
                    continue
            self.ui.assetCharListWidget.addItem(item)
            self.ui.assetCharListWidget.setItemWidget(item, item.checkBox)
            item.setHidden(False)
        for item in self.propList:
            if search_key:
                asset = item.data(QtCore.Qt.UserRole)
                if not search_key in asset.assetName:
                    continue
            self.ui.assetPropListWidget.addItem(item)
            self.ui.assetPropListWidget.setItemWidget(item, item.checkBox)
            item.setHidden(False)
        for item in self.othersList:
            if search_key:
                asset = item.data(QtCore.Qt.UserRole)
                if not search_key in asset.assetName:
                    continue
            self.ui.assetOthersListWidget.addItem(item)
            self.ui.assetOthersListWidget.setItemWidget(item, item.checkBox)
            item.setHidden(False)

    def connect_signal(self):
        self.ui.assetCharListWidget.itemSelectionChanged.connect(self.item_sel_changed)
        self.ui.assetPropListWidget.itemSelectionChanged.connect(self.item_sel_changed)
        self.ui.assetOthersListWidget.itemSelectionChanged.connect(self.item_sel_changed)

        self.ui.assetCharListWidget.mousePressSignal.connect(self.item_select)
        self.ui.assetPropListWidget.mousePressSignal.connect(self.item_select)
        self.ui.assetOthersListWidget.mousePressSignal.connect(self.item_select)
        self.ui.log_widget.itemClicked.connect(self.tree_check)
        self.ui.searchText.textChanged.connect(self.add_asset_widgets)
        self.ui.sliderWidget.valueChanged.connect(self.set_icon_size)

        self.ui.log_widget.itemSelectionChanged.connect(self.tree_select)

        self.ui.assetCharListWidget.customContextMenuRequested.connect(self.show_switch_menu)
        self.ui.assetCharListWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.ui.assetPropListWidget.customContextMenuRequested.connect(self.show_switch_menu)
        self.ui.assetPropListWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.ui.assetOthersListWidget.customContextMenuRequested.connect(self.show_switch_menu)
        self.ui.assetOthersListWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)

    def set_icon_size(self):
        value = self.ui.sliderWidget.value()
        for index in xrange(self.ui.assetCharListWidget.count()):
            item = self.ui.assetCharListWidget.item(index)
            item.setSizeHint(QtCore.QSize(value+4,value+22))
        for index in xrange(self.ui.assetPropListWidget.count()):
            item = self.ui.assetPropListWidget.item(index)
            item.setSizeHint(QtCore.QSize(value+4,value+22))
        for index in xrange(self.ui.assetOthersListWidget.count()):
            item = self.ui.assetOthersListWidget.item(index)
            item.setSizeHint(QtCore.QSize(value+4,value+22))
        self.ui.assetCharListWidget.setIconSize(QtCore.QSize(value,value))
        self.ui.assetPropListWidget.setIconSize(QtCore.QSize(value,value))
        self.ui.assetOthersListWidget.setIconSize(QtCore.QSize(value,value))

    def closeEvent(self, event):
        try:
            self.remove_api()
        except Exception, e:
            pass

    def remove_api(self):
        om.MMessage.removeCallback(self.afterLoadRef)
        om.MMessage.removeCallback(self.afterUnloadRef)
        om.MMessage.removeCallback(self.afterOpen)
        om.MMessage.removeCallback(self.afterNew)
        om.MMessage.removeCallback(self.afterCreateRef)
        om.MMessage.removeCallback(self.afterRemoveRef)

    def start_api(self):
        self.afterLoadRef = om.MSceneMessage.addCallback(om.MSceneMessage.kAfterLoadReference , self.refresh_reference)
        self.afterUnloadRef = om.MSceneMessage.addCallback(om.MSceneMessage.kAfterUnloadReference , self.refresh_reference)
        self.afterOpen = om.MSceneMessage.addCallback(om.MSceneMessage.kAfterOpen , self.refresh_ui)
        self.afterNew = om.MSceneMessage.addCallback(om.MSceneMessage.kAfterNew , self.refresh_ui)
        self.afterCreateRef = om.MSceneMessage.addCallback(om.MSceneMessage.kAfterCreateReference , self.refresh_reference)
        self.afterRemoveRef = om.MSceneMessage.addCallback(om.MSceneMessage.kAfterRemoveReference , self.refresh_reference)

    def refresh_reference(self, *args):
        selected = self.get_selection()
        self.get_asset_data_from_scene()
        self.sort_asset_data()
        self.create_asset_widgets()
        self.add_asset_widgets()
        self.keep_selection(selected)

    def get_selection(self):
        selectedList = []
        for item in self.list_item:
            asset = item.data(QtCore.Qt.UserRole)
            selectedList.append(asset.assetName)
        return selectedList

    def keep_selection(self, selected):
        for index in xrange(self.ui.assetCharListWidget.count()):
            item = self.ui.assetCharListWidget.item(index)
            asset = item.data(QtCore.Qt.UserRole)
            for selItem in selected:
                if asset.assetName == selItem:
                    item.setSelected(True)
        for index in xrange(self.ui.assetPropListWidget.count()):
            item = self.ui.assetPropListWidget.item(index)
            asset = item.data(QtCore.Qt.UserRole)
            for selItem in selected:
                if asset.assetName == selItem:
                    item.setSelected(True)
        for index in xrange(self.ui.assetOthersListWidget.count()):
            item = self.ui.assetOthersListWidget.item(index)
            asset = item.data(QtCore.Qt.UserRole)
            for selItem in selected:
                if asset.assetName == selItem:
                    item.setSelected(True)

    def refresh_ui(self, *args):
        self.get_asset_data_from_scene()
        self.sort_asset_data()
        self.create_asset_widgets()
        self.add_asset_widgets()
        self.set_shot_detail()

    def item_select(self, *args):
        modifiers = QtWidgets.QApplication.keyboardModifiers()
        self.focusedWidget = args[0]
        for widget in (self.ui.assetCharListWidget, self.ui.assetPropListWidget, self.ui.assetOthersListWidget):
            if widget != self.focusedWidget and not modifiers:
                widget.clearSelection()

    def show_switch_menu(self, pos):
        menu = QtWidgets.QMenu(self)
        # switch_ref_menu.list_ref()
        currentItem = self.focusedWidget.currentItem()
        if currentItem:
            asset = currentItem.data(QtCore.Qt.UserRole)
            for ns, path in asset.copies.iteritems():
                entity = context_info.ContextPathInfo(path=path)
                keyGrp = entity.projectInfo.asset.geo_grp() or 'Geo_Grp'
                rigGrp = ns + ':' + keyGrp
            files = switch_ref_menu.list_ref(rigGrp)
            for fn in files:
                name = fn.split('/')[-1]
                rigMenu = menu.addAction(name)
                rigMenu.triggered.connect(partial(self.switch_asset, fn, asset))
            menu.popup(self.focusedWidget.mapToGlobal(pos))

    def switch_asset(self, rigFile, asset):
        self.remove_api()
        for ns, path in asset.copies.iteritems():
            asset.switch(rigFile, path)
        self.refresh_reference()
        self.start_api()

    def item_sel_changed(self):
        self.list_item = []
        self.list_item += self.ui.assetCharListWidget.selectedItems()
        self.list_item += self.ui.assetPropListWidget.selectedItems()
        self.list_item += self.ui.assetOthersListWidget.selectedItems()
        self.add_asset_detail()

    def check(self, clickedItem, *args):
        self.remove_api()
        state = clickedItem.checkBox.checkState()
        if not [i for i in self.list_item if i is clickedItem]:
            asset = clickedItem.data(QtCore.Qt.UserRole)
            for ns, path in asset.copies.iteritems():
                if state == QtCore.Qt.Checked:
                    asset.load_reference_with_matte([ns])
                elif state == QtCore.Qt.Unchecked:
                    asset.unload_reference_with_matte([ns])
        self.start_api()
        # clickedItem.setSelected(True)

        for item in self.list_item:
            item.checkBox.setCheckState(state)
            for treeItem in item.treeItemList:
                treeItem.setCheckState(0,state)
        self.tree_check()

    def tree_select(self):
        selItems =  self.ui.log_widget.selectedItems()
        nsList = []
        asset = None
        for sel in selItems:
            asset = sel.data(0,QtCore.Qt.UserRole)
            ns = sel.text(0)
            nsList.append(ns)
        if asset:
            asset.select(nsList)

    def tree_check(self):
        self.remove_api()
        for item in self.list_item:
            check = False
            for i in (range(item.treeItemHead.childCount())):
                state = item.treeItemHead.child(i).checkState(0)
                if state == QtCore.Qt.Checked:
                    asset = item.treeItemHead.child(i).data(0,QtCore.Qt.UserRole)
                    ns = item.treeItemHead.child(i).text(0)
                    asset.load_reference_with_matte([ns])

                    item.checkBox.blockSignals(True)
                    item.checkBox.setCheckState(QtCore.Qt.Checked)
                    item.checkBox.blockSignals(False)
                    check = True

                elif state == QtCore.Qt.Unchecked:
                    asset = item.treeItemHead.child(i).data(0,QtCore.Qt.UserRole)
                    ns = item.treeItemHead.child(i).text(0)
                    asset.unload_reference_with_matte([ns])

                if check == False:  
                    item.checkBox.blockSignals(True)
                    item.checkBox.setCheckState(QtCore.Qt.Unchecked)
                    item.checkBox.blockSignals(False)
        self.start_api()

    def add_shot_detail(self):
        self.frame = QtWidgets.QFrame()
        self.frame.setFrameShape(QtWidgets.QFrame.Box)
        self.frame.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.frame_layout = QtWidgets.QVBoxLayout(self.frame)
        self.preview_label = media_widget.PreviewMedia(self.frame)
        self.frame_layout.addWidget(self.preview_label)

        self.projLabel = QtWidgets.QLabel()
        self.shotLabel = QtWidgets.QLabel()

        self.ui.infoLayout.addWidget(self.frame)
        self.ui.infoLayout.addWidget(self.projLabel)
        self.ui.infoLayout.addWidget(self.shotLabel)

        self.ui.infoLayout.addWidget(self.ui.log_widget)

    def set_shot_detail(self):
        try:
            shotEntity = context_info.ContextPathInfo()
            project = shotEntity.project
            shot = shotEntity.name
            path = shotEntity.path.name().abs_path()
            self.movs = self.find_latest_playblast(path)
            if not self.movs:
                self.movs = [icon.nopreview]
        except:
            self.movs = [icon.nopreview]
            project = ''
            shot = ''


        self.projLabel.setText('Project : %s'%project)
        self.shotLabel.setText('Shot : %s'%shot)
        self.preview_label._loadMedia([self.movs[0]])


    def add_asset_detail(self):
        root = self.ui.log_widget.invisibleRootItem()
        child_count = root.childCount()
        for i in range(child_count):
            self.ui.log_widget.takeTopLevelItem(0)

        for item in self.list_item:
            asset = item.data(QtCore.Qt.UserRole)

            treeItemHead = item.treeItemHead
            self.ui.log_widget.addTopLevelItem(treeItemHead)
            treeItemHead.setText(0,asset.assetName)
            treeItemHead.setExpanded(True)
            # clear all child Before create New
            for i in reversed(range(treeItemHead.childCount())):
                treeItemHead.removeChild(treeItemHead.child(i))

            # clear treeItemList before create new
            item.treeItemList = []
            for ns, path in asset.copies.iteritems():
                treeItem = item.create_treeItem()
                treeItem.setFlags(treeItem.flags() | QtCore.Qt.ItemIsUserCheckable)
                treeItem.setText(0,ns)
                treeItem.setData(0,QtCore.Qt.UserRole,asset)
                # if not '_md' in path:
                #     treeItem.setBackgroundColor(0,bgWarnColor)
                state = asset.check_load_state(path)
                if state == True:
                    state = QtCore.Qt.Checked
                elif state == False:
                    state = QtCore.Qt.Unchecked
                treeItem.setCheckState(0, state)
                treeItem.setFont(0, fonts)

            for treeItem in item.treeItemList:
                treeItemHead.addChild(treeItem)

    def find_latest_playblast(self, path, latest=True): 
        movExts = ['.mov']
        latests = []
        steps = file_utils.list_folder(path)

        for step in steps: 
            stepPath = '%s/%s' % (path, step)
            processes = file_utils.list_folder(stepPath)

            for process in processes: 
                mediaPath = '%s/%s/output' % (stepPath, process)
                if os.path.exists(mediaPath): 
                    files = file_utils.list_file(mediaPath)

                    if files: 
                        if any(os.path.splitext(a)[-1] in movExts for a in files): 
                            if latest: 
                                versions = [re.findall(r'v\d{3}', a)[0] for a in files if re.findall(r'v\d{3}', a)]
                                highest = sorted(versions)[-1]
                                pickFiles = [a for a in files if highest in a]
                                pickFile = pickFiles[0] if pickFiles else files[-1]
                                mov = '%s/%s' % (mediaPath, pickFile)
                                latests.append(mov)
                            else: 
                                latests += ['%s/%s' % (mediaPath, a) for a in files if os.path.splitext(a)[-1] in movExts]

        return latests

class AssetItem(QtWidgets.QListWidgetItem):
    def __init__(self, parent=None):
        super(AssetItem, self).__init__(parent)
        self.checkBox = QtWidgets.QCheckBox()
        self.treeItemHead = QtWidgets.QTreeWidgetItem()
        self.treeItemList = []

    def create_treeItem(self):
        treeItem = QtWidgets.QTreeWidgetItem()
        self.treeItemList.append(treeItem)
        self.treeItemList.sort()
        return treeItem
            
def show():
    global assetLoaderApp
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        uiName = 'AssetLoaderAppUI'
        # maya_win.deleteUI(uiName)
        try:
            assetLoaderApp.close()
        except Exception, e:
            pass

        assetLoaderApp = AssetUI(maya_win.getMayaWindow())
        assetLoaderApp.show()
        return assetLoaderApp

    else: 
        logger.info('Run in AssetUI\n')
        app = QtWidgets.QApplication.instance()
        if not app: 
            app = QtWidgets.QApplication(sys.argv)
        assetLoaderApp = AssetUI()
        assetLoaderApp.show()
        stylesheet.set_default(app)
        sys.exit(app.exec_())

if __name__ == '__main__': 
    show()
