from rf_utils.context import context_info
import maya.cmds as mc
import pymel.core as pm
import core
reload(core)

class Manager():
    def __init__(self):
        self.allAssetData = {} # {assetName : Asset()}

    def list_all_ref(self):

        refs = pm.listReferences()

        for ref in refs:
            namespace = ref.namespace
            pathWoCopy = ref.path
            pathCopy = ref.withCopyNumber()
            assetEntity = context_info.ContextPathInfo(path = pathCopy)
            status = ref.isLoaded()
            assetName = assetEntity.name
            if not assetName:
                assetName = namespace
            # check instance
            if assetName in self.allAssetData:
                # use exists instance
                assetData = self.allAssetData[assetName]
            else:
                # new instance
                assetData = core.Asset()
                assetData.assetName = assetName
                # assetData.assetEntity = assetEntity
                assetData.assetType = assetEntity.type

            # isMtr
            if 'mtr' in pathCopy:
                # shaders
                assetData.shaders.append(pathCopy)
            else:
                # copies
                assetData.copies[namespace] = pathCopy
                assetData.refPath = pathWoCopy
                assetData.loadStatus = status

            if assetData.copies:
                self.allAssetData[assetName] = assetData

        return self.allAssetData