from Qt import QtWidgets
from Qt import QtCore
from Qt import QtGui

class Ui(QtWidgets.QWidget):
    """docstring for Ui"""
    def __init__(self, parent=None):
        super(Ui, self).__init__(parent)
        self.mainLayout = QtWidgets.QHBoxLayout()

        self.setLayout(self.mainLayout)

        self.mainSplitter = QtWidgets.QSplitter()
        self.mainSplitter.setOrientation(QtCore.Qt.Horizontal)
        self.mainSplitter.resize(200, 200)

        self.assetSplitWidget = QtWidgets.QWidget(self.mainSplitter)
        self.infoSplitWidget = QtWidgets.QWidget(self.mainSplitter)

        self.mainSplitter.setSizes([125,75])

        self.assetLayout = QtWidgets.QVBoxLayout(self.assetSplitWidget)
        self.infoLayout = QtWidgets.QVBoxLayout(self.infoSplitWidget)

        self.mainLayout.addWidget(self.mainSplitter)

        self.add_search_widgets()
        self.add_asset_widgets()
        self.add_slider_wigets()
        self.add_asset_tree()

    def add_search_widgets(self):
    	
        self.searchLayout = QtWidgets.QHBoxLayout()
        self.searchLabel = QtWidgets.QLabel('Search')
        self.searchText = QtWidgets.QLineEdit()
        self.searchText.setPlaceholderText('<asset name>')

        self.searchLayout.addWidget(self.searchLabel)
        self.searchLayout.addWidget(self.searchText)

        self.assetLayout.addLayout(self.searchLayout)

    def add_slider_wigets(self):

        self.sliderWidget = QtWidgets.QSlider()
        self.sliderWidget.setOrientation(QtCore.Qt.Horizontal)

        self.assetLayout.addWidget(self.sliderWidget)

        self.sliderWidget.setMinimum(60)
        self.sliderWidget.setMaximum(170)
        self.sliderWidget.setSingleStep(10)
        self.sliderWidget.setValue(115)

    def add_asset_widgets(self):
        self.assetSplitter = QtWidgets.QSplitter()
        self.assetSplitter.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.assetSplitter.setFrameShadow(QtWidgets.QFrame.Raised)
        self.assetSplitter.setMidLineWidth(1)
        self.assetSplitter.setOrientation(QtCore.Qt.Vertical)
        self.assetSplitter.setChildrenCollapsible(True)

        font = QtGui.QFont()
        font.setPointSize(12)

        self.groupBoxChar = QtWidgets.QGroupBox(self.assetSplitter)
        self.groupBoxChar.setFont(font)
        self.groupBoxChar.setTitle(QtWidgets.QApplication.translate("MainWindow", 'Char', None, -1))

        self.aseetCharVerticalLayout = QtWidgets.QVBoxLayout(self.groupBoxChar)

        self.assetCharListWidget = ImportListWidget()
        self.assetCharListWidget.setViewMode(QtWidgets.QListView.IconMode)
        self.assetCharListWidget.setMovement(QtWidgets.QListView.Static)
        self.assetCharListWidget.setResizeMode(QtWidgets.QListView.Adjust)
        self.assetCharListWidget.setSelectionMode(QtWidgets.QListView.ExtendedSelection)

        self.aseetCharVerticalLayout.addWidget(self.assetCharListWidget)
        # self.assetLayout.addWidget(self.assetSplitter)


        self.groupBoxProp = QtWidgets.QGroupBox(self.assetSplitter)
        self.groupBoxProp.setFont(font)
        self.groupBoxProp.setTitle(QtWidgets.QApplication.translate("MainWindow", 'Prop', None, -1))

        self.aseetPropVerticalLayout = QtWidgets.QVBoxLayout(self.groupBoxProp)

        self.assetPropListWidget = ImportListWidget()
        self.assetPropListWidget.setViewMode(QtWidgets.QListView.IconMode)
        self.assetPropListWidget.setMovement(QtWidgets.QListView.Static)
        self.assetPropListWidget.setResizeMode(QtWidgets.QListView.Adjust)
        self.assetPropListWidget.setSelectionMode(QtWidgets.QListView.ExtendedSelection)

        self.aseetPropVerticalLayout.addWidget(self.assetPropListWidget)
        # self.assetLayout.addWidget(self.assetSplitter)

        self.groupBoxOthers = QtWidgets.QGroupBox(self.assetSplitter)
        self.groupBoxOthers.setFont(font)
        self.groupBoxOthers.setTitle(QtWidgets.QApplication.translate("MainWindow", 'Others', None, -1))

        self.aseetOthersVerticalLayout = QtWidgets.QVBoxLayout(self.groupBoxOthers)

        self.assetOthersListWidget = ImportListWidget()
        self.assetOthersListWidget.setViewMode(QtWidgets.QListView.IconMode)
        self.assetOthersListWidget.setMovement(QtWidgets.QListView.Static)
        self.assetOthersListWidget.setResizeMode(QtWidgets.QListView.Adjust)
        self.assetOthersListWidget.setSelectionMode(QtWidgets.QListView.ExtendedSelection)

        self.aseetOthersVerticalLayout.addWidget(self.assetOthersListWidget)

        self.assetSplitter.resize(300, 300)
        self.assetSplitter.setSizes([190,190,20])

        self.assetLayout.addWidget(self.assetSplitter)

    def add_asset_tree(self):
        self.log_widget = QtWidgets.QTreeWidget()
        self.log_widget.setHeaderItem(QtWidgets.QTreeWidgetItem(["Asset Namespace"]))
        file_tree_palette = QtGui.QPalette()
        file_tree_palette.setColor(QtGui.QPalette.Window, QtGui.QColor(255, 255, 255))
        file_tree_palette.setColor(QtGui.QPalette.Base, QtGui.QColor(30, 30, 30))
        file_tree_palette.setColor(QtGui.QPalette.Highlight, QtGui.QColor(93, 93, 93))
        self.log_widget.setPalette(file_tree_palette)
        self.log_widget.setSelectionMode(QtWidgets.QListView.ExtendedSelection)

class ImportListWidget(QtWidgets.QListWidget):
    ''' Sub-class QTableWidget for blocking object select signal when right-click '''
    mousePressSignal = QtCore.Signal(object)
    def __init__(self, parent=None):
        super(ImportListWidget, self).__init__(parent)

    def mousePressEvent(self, event):
        self.mousePressSignal.emit(self)
        super(ImportListWidget, self).mousePressEvent(event)