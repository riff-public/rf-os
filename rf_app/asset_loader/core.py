from rf_utils.context import context_info
reload(context_info)
import maya.cmds as mc

class BaseRef(object):
    def __init__(self):
        self.copies = {} # {namespace : pathW/Copy}
        self.refPath = ''
        self.loadStatus = False

    def load_reference(self, namespace=[]):
        if not namespace:
            namespace = self.copies.keys()
        for ns in namespace:
            path = self.copies[ns]
            if not mc.referenceQuery(path, isLoaded=True):
                mc.file(path,loadReference=False)

    def unload_reference(self, namespace=[]):
        if not namespace:
            namespace = self.copies.keys()
        for ns in namespace:
            path = self.copies[ns]
            if mc.referenceQuery(path, isLoaded=True):
                mc.file(path,unloadReference=False)

    def check_load_state(self, path=''):
        return mc.referenceQuery(path, isLoaded=True)

    def select(self, namespace=[]):
        mc.select(cl=True)
        for ns in namespace:
            path = self.copies[ns]
            entity = context_info.ContextPathInfo(path=path)
            keyGrp = entity.projectInfo.asset.geo_grp() or 'Geo_Grp'
            obj = ns + ':' + keyGrp
            if len(mc.ls(obj)) == 1:
                mc.select(obj, tgl=True)

    def switch(self, rigFile, path):
        rnNode = mc.referenceQuery(path, referenceNode=True)
        mc.file(rigFile, loadReference=rnNode, prompt=False)


class Asset(BaseRef):
    def __init__(self):
        super(Asset,self).__init__()
        
        self.shaders = []
        self.assetName = ''
        self.assetType = ''

    def load_reference_with_matte(self, namespace=[]):
        self.load_reference(namespace=namespace)
        self.check_optimize_materials()

    def unload_reference_with_matte(self, namespace=[]):
        self.unload_reference(namespace=namespace)
        self.check_optimize_materials()

    def check_optimize_materials(self):
        for namespace, path in self.copies.iteritems():
            if mc.referenceQuery(path, isLoaded=True):
                self.load_materials()
                break
        else:
            self.unload_materials()

    def load_materials(self):
        if self.shaders:
            for shade in self.shaders:
                if not mc.referenceQuery(shade, isLoaded=True):
                    mc.file(shade,loadReference=False)

    def unload_materials(self):
        if self.shaders:
            for shade in self.shaders:
                if mc.referenceQuery(shade, isLoaded=True):
                    mc.file(shade,unloadReference=False)