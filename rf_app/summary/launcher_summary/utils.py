from rf_utils import file_utils
from datetime import datetime
import os
from rf_utils.sg import sg_process
sg = sg_process.sg

pathtest = 'O:/Pipeline/logs/RFLauncherLogs/Tor.json'
root = 'O:/Pipeline/logs/RFLauncherLogs'
files = os.listdir(root)

def users():
    userdict = dict()
    filters = []
    field = ['department.Department.name', 'sg_name']
    result = sg.find('HumanUser', filters, field)

    for user in result:
        name = user.get('sg_name')
        step = user['department.Department.name']

        if name not in userdict.keys():
            userdict[name] = step

    return userdict

def load_logs():
    user_dict = dict()
    for name in files:
        n = name.split('.')[0]
        fileName = os.path.join(root, name)
        logs = file_utils.json_loader(fileName)
        user_dict[n] = logs

    return user_dict

def logs_step_data(logs, user_dict):
    result = dict()
    stepLogs = dict()
    tool_dict = dict()
    dateLog = dict()

    for log in logs:
        user = log

        for dt_string in logs[log]:
            department = logs[log][dt_string]['department']
            project = logs[log][dt_string]['project']
            tool = logs[log][dt_string]['dcc']
            dt = datetime.strptime(dt_string, "%d/%m/%Y %H:%M:%S")
            numYear = dt.year
            numMonth = dt.month
            numDay = dt.day
            time = dt.strftime("%Y/%m/%d %H:%M:%S")
            step = user_dict[user] if user in user_dict.keys() else 'None'
            data = {'project':project, 'department':department, 'dcc':tool, 'user':user, 'time':time, 'step':step}

            if step not in stepLogs.keys():
                stepLogs[step] = {user:{dt:data}}
            else:
                if user not in stepLogs[step].keys():
                    stepLogs[step][user] = {dt:data}
                else:
                    if dt not in stepLogs[step][user].keys():
                        stepLogs[step][user][dt] = data

            if numYear not in dateLog.keys():
                dateLog[numYear] = {numMonth:[numDay]}
            else:
                if numMonth not in dateLog[numYear].keys():
                    dateLog[numYear][numMonth] = [numDay]
                else:
                    if numDay not in dateLog[numYear][numMonth]:
                        dateLog[numYear][numMonth].append(numDay)

            # tool by step
            if step not in tool_dict.keys():
                tool_dict[step] = [tool]
            else:
                if tool not in tool_dict[step]:
                    tool_dict[step].append(tool)

            # all tool
            if 'ALL' not in tool_dict.keys():
                tool_dict['ALL'] = [tool]
            else:
                if tool not in tool_dict['ALL']:
                    tool_dict['ALL'].append(tool)

    result = {'date':dateLog, 'log':stepLogs, 'tool':tool_dict}

    return result