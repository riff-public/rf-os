from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import sys
import os
import time
import subprocess
sys.path.append('{}/core'.format(os.environ['RFSCRIPT']))

import calendar
from datetime import datetime
from rf_utils.ui import stylesheet
from rf_app.summary.launcher_summary import utils
uiName = 'LauncherSummary'
config_step = ['Art','Model','Groom','Lookdev','Rig','Layout','Anim','Sim','Lighting','FX','Comp','Edit','Realtime','Pipeline','IP Management','Production', 'HR', 'None']
TEXT_SEL = ' - select - '
_NUM = 1
FIXED = 1
TODAY = datetime.today().date()

class config:
    img_app = '%s/core/rf_launcher_apps/Tool/Launcher Summary/LauncherSummary.png' % os.environ.get('RFSCRIPT')

class App(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(App, self).__init__(parent=parent)

        self.set_ui()
        self.fresh()
        self.init_signal()

    def set_ui(self):
        self.widget = QtWidgets.QWidget()
        self.main = QtWidgets.QVBoxLayout(self.widget)

        self.projectLayout = QtWidgets.QHBoxLayout()
        self.combobox_project = QtWidgets.QComboBox()
        self.projectSpacer = QtWidgets.QSpacerItem(20, 20)
        self.label_project = QtWidgets.QLabel("Project")
        self.button_refresh = QtWidgets.QPushButton(" refresh ")
        self.projectLayout.addWidget(self.label_project)
        self.projectLayout.addWidget(self.combobox_project)
        self.projectLayout.addItem(self.projectSpacer)
        self.projectLayout.addWidget(self.button_refresh)

        self.projectLayout.setStretch(0, 0)
        self.projectLayout.setStretch(1, 0)
        self.projectLayout.setStretch(2, 2)
        self.projectLayout.setStretch(3, 0)

        self.optionLayout = QtWidgets.QHBoxLayout()
        self.label_user = QtWidgets.QLabel("user")
        self.label_department = QtWidgets.QLabel("department")
        self.checkbox_user = QtWidgets.QCheckBox()
        self.checkbox_department = QtWidgets.QCheckBox()
        self.combobox_option = QtWidgets.QComboBox()
        self.optionSpacer = QtWidgets.QSpacerItem(20, 20)

        self.optionLayout.addWidget(self.label_user)
        self.optionLayout.addWidget(self.checkbox_user)
        self.optionLayout.addWidget(self.label_department)
        self.optionLayout.addWidget(self.checkbox_department)
        self.optionLayout.addWidget(self.combobox_option)
        self.combobox_option.setFixedWidth(150)
        self.optionLayout.addItem(self.optionSpacer)

        self.optionLayout.setStretch(0, 0)
        self.optionLayout.setStretch(1, 0)
        self.optionLayout.setStretch(2, 0)
        self.optionLayout.setStretch(3, 0)
        self.optionLayout.setStretch(4, 0)
        self.optionLayout.setStretch(5, 2)

        self.dateLayout = QtWidgets.QHBoxLayout()
        self.label_date = QtWidgets.QLabel("Date:")
        self.first_calendar = QtWidgets.QCalendarWidget()
        self.first_calendar.setVerticalHeaderFormat(QtWidgets.QCalendarWidget.NoVerticalHeader)
        self.first_dateEdit = QtWidgets.QDateEdit()
        self.first_dateEdit.setVisible(True)
        self.first_dateEdit.setDisplayFormat('dd MMM yyyy')
        self.first_dateEdit.setCalendarPopup(True)
        self.first_dateEdit.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.first_dateEdit.setCalendarWidget(self.first_calendar)
        self.first_dateEdit.setDate(QtCore.QDate(TODAY.year, TODAY.month, 1))
        self.label_line = QtWidgets.QLabel("-")
        self.end_calendar = QtWidgets.QCalendarWidget()
        self.end_calendar.setVerticalHeaderFormat(QtWidgets.QCalendarWidget.NoVerticalHeader)
        self.end_dateEdit = QtWidgets.QDateEdit()
        self.end_dateEdit.setVisible(True)
        self.end_dateEdit.setDisplayFormat('dd MMM yyyy')
        self.end_dateEdit.setCalendarPopup(True)
        self.end_dateEdit.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.end_dateEdit.setCalendarWidget(self.end_calendar)
        self.end_dateEdit.setDate(QtCore.QDate(TODAY.year, TODAY.month, TODAY.day))
        self.button_submit = QtWidgets.QPushButton("  Submit  ")
        self.button_submit.setFixedWidth(120)
        self.dateSpacer = QtWidgets.QSpacerItem(20, 20)

        self.dateLayout.addWidget(self.label_date)
        self.dateLayout.addWidget(self.first_dateEdit)
        self.dateLayout.addWidget(self.label_line)
        self.dateLayout.addWidget(self.end_dateEdit)
        self.dateLayout.addWidget(self.button_submit)
        self.dateLayout.addItem(self.dateSpacer)

        self.dateLayout.setStretch(0, 0)
        self.dateLayout.setStretch(1, 0)
        self.dateLayout.setStretch(2, 0)
        self.dateLayout.setStretch(3, 0)
        self.dateLayout.setStretch(4, 1)
        self.dateLayout.setStretch(5, 2)

        self.tableWidget = QtWidgets.QTableWidget()
        self.tableWidget.setStyleSheet("background-color: rgb(45, 45, 45);")
        self.tableWidget.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.usertableWidget = QtWidgets.QTableWidget()
        self.usertableWidget.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.usertableWidget.setVisible(False)
        self.usertableWidget.setFixedWidth(280)
        self.usertableWidget.setStyleSheet("QToolTip {%s%s%s} QTableWidget{%s}"
                                            %("color:black;","background-color: #E5FCC2;" ,"border: 0px;","background-color: rgb(45, 45, 45);"))


        self.tree_widget = QtWidgets.QTreeWidget()
        self.usertableLayout = QtWidgets.QHBoxLayout()
        self.tableLayout = QtWidgets.QVBoxLayout()

        self.usertableLayout.addLayout(self.tableLayout)
        self.usertableLayout.addWidget(self.usertableWidget)
        self.tableLayout.addWidget(self.tableWidget)
        self.tableLayout.addWidget(self.tree_widget)

        self.usertableLayout.setStretch(0, 2)
        self.usertableLayout.setStretch(1, 0)
        self.tableLayout.setStretch(0, 2)
        self.tableLayout.setStretch(1, 1)

        self.main.addLayout(self.projectLayout)
        self.main.addLayout(self.optionLayout)
        self.main.addLayout(self.dateLayout)
        self.main.addLayout(self.usertableLayout)

        self.main.setStretch(0, 0)
        self.main.setStretch(1, 0)
        self.main.setStretch(2, 0)
        self.main.setStretch(3, 2)

        self.setCentralWidget(self.widget)
        self.setMinimumSize(1000, 750)
        self.setWindowTitle(uiName)
        self.setWindowIcon(QtGui.QIcon(config.img_app))

    def init_signal(self):
        self.checkbox_department.stateChanged.connect(self.swicth_checkbox_department)
        self.checkbox_user.stateChanged.connect(self.swicth_checkbox_user)
        self.button_submit.clicked.connect(self.switch_option_data)
        self.button_refresh.clicked.connect(self.fresh)

        self.tableWidget.itemClicked.connect(self.set_user_day_tablewidget)
        self.tableWidget.cellClicked.connect(self.set_close_usertable)

    def set_close_usertable(self, row, col):
        item = self.tableWidget.item(row, col)
        if not item:
            self.usertableWidget.clear()
            self.usertableWidget.setVisible(False)

    def fresh(self):
        self.logs = utils.load_logs()
        self.user_dict = utils.users()
        self.result = utils.logs_step_data(self.logs, self.user_dict)
        self.dateLogs = self.result['date']
        self.stepLogs = self.result['log']
        self.toolList = self.result['tool']
        self.statusOpenTable = False
        self.switch_option_data()

    def set_user(self):
        userList = list()
        self.combobox_option.clear()
        self.combobox_option.addItem(TEXT_SEL)
        for step in self.stepLogs:
            for user in self.stepLogs[step]:
                userList.append(user)

        for name in sorted(userList):
            self.combobox_option.addItem(name)

    def set_department(self):
        self.combobox_option.clear()
        self.combobox_option.addItem(TEXT_SEL)
        for step in config_step:
            self.combobox_option.addItem(step)

    def swicth_checkbox_user(self):
        if self.checkbox_user.isChecked():
            self.checkbox_department.setChecked(False)
        self.switch_option_comboobx()

    def swicth_checkbox_department(self):
        if self.checkbox_department.isChecked():
            self.checkbox_user.setChecked(False)
        self.switch_option_comboobx()

    def switch_option_comboobx(self):
        if self.checkbox_department.isChecked():
            self.set_department()
            self.first_dateEdit.setDisplayFormat('dd MMM yyyy')
            self.end_dateEdit.setVisible(True)
            self.label_line.setVisible(True)
            self.statusOpenTable = False
            self.usertableWidget.setVisible(False)
        elif self.checkbox_user.isChecked():
            self.set_user()
            self.first_dateEdit.setDisplayFormat('MMM yyyy')
            self.end_dateEdit.setVisible(False)
            self.label_line.setVisible(False)
        else:
            self.statusOpenTable = False
            self.combobox_option.clear()

    def switch_option_data(self):
        string_first_date = str(self.first_dateEdit.date().toPython())
        first_date = datetime.strptime(string_first_date, "%Y-%m-%d")
        string_end_date = str(self.end_dateEdit.date().toPython())
        end_date = datetime.strptime(string_end_date, "%Y-%m-%d")

        if self.checkbox_department.isChecked() and self.combobox_option.currentText() != TEXT_SEL:
            step = self.combobox_option.currentText()
            userList = sorted(self.stepLogs[step])
            toolList = sorted(self.toolList[step])
            self.set_step_tablewidget(step=step, userList=userList, toolList=toolList, first_date=first_date, end_date=end_date)
            self.set_step_treewidget(stepList=[step], first_date=first_date, end_date=end_date)

        elif self.checkbox_user.isChecked() and self.combobox_option.currentText() != TEXT_SEL:
            user = self.combobox_option.currentText()
            step = self.user_dict[user]
            date = first_date

            toolList = list()
            for dt in self.stepLogs[step][user]:
                tool = self.stepLogs[step][user][dt]['dcc']
                if first_date.month ==  dt.month and first_date.year ==  dt.year:
                    if tool not in toolList:
                        toolList.append(tool)

            toolList = sorted(toolList)
            self.set_user_month_tablewidget(step=step, user=user, toolList=toolList, date=first_date)
            self.set_user_treewidget(step=step, user=user, date=date)

        else:
            toolList = sorted(self.toolList['ALL'])
            self.set_default_tablewidget(stepList=config_step, toolList=toolList, first_date=first_date, end_date=end_date)
            self.set_step_treewidget(stepList=config_step, first_date=first_date, end_date=end_date)

    def set_default_tablewidget(self, stepList=None, toolList=None, first_date=None, end_date=None):
        self.tableWidget.clear()
        #setup table
        self.tableWidget.setColumnCount(len(stepList))
        self.tableWidget.setRowCount(len(toolList))
        self.tableWidget.setHorizontalHeaderLabels(stepList)
        self.tableWidget.setVerticalHeaderLabels(toolList)
        self.tableWidget.horizontalHeader().setDefaultSectionSize(68)

        col_pos = {step:i for i, step in enumerate(stepList)}
        row_pos = {tool:i for i, tool in enumerate(toolList)}

        for step in config_step:
            for user in self.stepLogs[step]:
                for dt in self.stepLogs[step][user]:

                    tool = self.stepLogs[step][user][dt]['dcc']
                    date = datetime.strptime(dt.strftime("%Y-%m-%d"), "%Y-%m-%d")

                    col = col_pos[step]
                    row = row_pos[tool]

                    if first_date <=  date and end_date >= date:
                        value = int(self.tableWidget.item(row, col).text()) if self.tableWidget.item(row, col) else 0
                        value += FIXED

                        item = self.tableWidget.item(row, col)
                        item = QtWidgets.QTableWidgetItem(str(value))
                        item.setTextAlignment(QtCore.Qt.AlignCenter)
                        item.setForeground(QtCore.Qt.white)
                        item.setBackground(QtGui.QColor(70, 70, 70))
                        self.tableWidget.setItem(row, col, item)

    def set_step_tablewidget(self, step=None, userList=None, toolList=None, first_date=None, end_date=None):
        self.tableWidget.clear()
        #setup table
        self.tableWidget.setColumnCount(len(userList))
        self.tableWidget.setRowCount(len(toolList))
        self.tableWidget.setHorizontalHeaderLabels(userList)
        self.tableWidget.setVerticalHeaderLabels(toolList)
        self.tableWidget.horizontalHeader().setDefaultSectionSize(68)

        col_pos = {user:i for i, user in enumerate(userList)}
        row_pos = {tool:i for i, tool in enumerate(toolList)}

        for user in self.stepLogs[step]:
            for dt in self.stepLogs[step][user]:

                tool = self.stepLogs[step][user][dt]['dcc']
                date = datetime.strptime(dt.strftime("%Y-%m-%d"), "%Y-%m-%d")

                col = col_pos[user]
                row = row_pos[tool]

                if first_date <=  date and end_date >= date:
                    value = int(self.tableWidget.item(row, col).text()) if self.tableWidget.item(row, col) else 0
                    value += FIXED

                    item = self.tableWidget.item(row, col)
                    item = QtWidgets.QTableWidgetItem(str(value))
                    item.setTextAlignment(QtCore.Qt.AlignCenter)
                    item.setForeground(QtCore.Qt.white)
                    item.setBackground(QtGui.QColor(70, 70, 70))
                    self.tableWidget.setItem(row, col, item)

    def set_user_month_tablewidget(self, step=None, user=None, toolList=None, date=None):
        self.tableWidget.clear()
        self.statusOpenTable = True
        self.usertableWidget.setVisible(False)
        #setup table
        weekday, numdays = calendar.monthrange(date.year, date.month)
        daysList = [str(i+1) for i in range(numdays)]

        self.tableWidget.setColumnCount(numdays)
        self.tableWidget.setRowCount(len(toolList))
        self.tableWidget.setHorizontalHeaderLabels(daysList)
        self.tableWidget.setVerticalHeaderLabels(toolList)
        self.tableWidget.horizontalHeader().setDefaultSectionSize(20)

        row_pos = {tool:i for i, tool in enumerate(toolList)}

        for dt in self.stepLogs[step][user]:
            tool = self.stepLogs[step][user][dt]['dcc']

            if date.month ==  dt.month and date.year ==  dt.year:
                col = dt.day - 1
                row = row_pos[tool]

                value = int(self.tableWidget.item(row, col).text()) if self.tableWidget.item(row, col) else 0
                value += FIXED

                item = self.tableWidget.item(row, col)
                item = QtWidgets.QTableWidgetItem(str(value))
                data = dt
                item.setData(QtCore.Qt.UserRole, data)
                item.setTextAlignment(QtCore.Qt.AlignCenter)
                item.setForeground(QtCore.Qt.white)
                item.setBackground(QtGui.QColor(70, 70, 70))
                self.tableWidget.setItem(row, col, item)

    def set_step_treewidget(self, stepList=None, user=None, first_date=None, end_date=None):
        self.tree_widget.clear()
        #setup tree widget
        title = ['date time', 'toolName', 'user',  'project', 'department']
        self.tree_widget.setHeaderLabels(title)

        items = list()
        time_dict = dict()

        for step in stepList:
            for user in self.stepLogs[step]:
                for dt in self.stepLogs[step][user]:
                    time = self.stepLogs[step][user][dt]['time']
                    tool = self.stepLogs[step][user][dt]['dcc']
                    project = self.stepLogs[step][user][dt]['project']
                    department = self.stepLogs[step][user][dt]['department']
                    date = datetime.strptime(dt.strftime("%Y-%m-%d"), "%Y-%m-%d")
                    data = [time, tool, user, project, department]

                    if first_date <=  date and end_date >= date:
                        time_dict[dt] = data


        for i, time in enumerate(sorted(time_dict.keys(), reverse=True)):
            data = time_dict[time]
            item = QtWidgets.QTreeWidgetItem(data)
            items.append(item)

        self.tree_widget.insertTopLevelItems(0, items)

    def set_user_treewidget(self, step=None, user=None, date=None):
        self.tree_widget.clear()
        #setup tree widget
        title = ['date time', 'toolName', 'user',  'project', 'department']
        self.tree_widget.setHeaderLabels(title)

        items = list()
        time_dict = dict()
        for dt in self.stepLogs[step][user]:
            time = self.stepLogs[step][user][dt]['time']
            tool = self.stepLogs[step][user][dt]['dcc']
            project = self.stepLogs[step][user][dt]['project']
            department = self.stepLogs[step][user][dt]['department']
            data = [time, tool, user, project, department]

            if date.year ==  dt.year and date.month <=  dt.month:
                time_dict[dt] = data

        for i, time in enumerate(sorted(time_dict.keys(), reverse=True)):
            data = time_dict[time]
            item = QtWidgets.QTreeWidgetItem(data)
            items.append(item)

        self.tree_widget.insertTopLevelItems(0, items)

    def set_user_day_tablewidget(self, item):
        self.usertableWidget.clear()
        if self.statusOpenTable:
            self.usertableWidget.setVisible(True)

            user = self.combobox_option.currentText()
            step = self.user_dict[user]
            date = item.data(QtCore.Qt.UserRole)
            toolList = list()

            for dt in self.stepLogs[step][user]:
                if dt.year == date.year and dt.month == date.month and dt.day == date.day:
                    tool = self.stepLogs[step][user][dt]['dcc']
                    if tool not in toolList:
                        toolList.append(tool)

            hourList = ['%d:00'%(i) for i in range(24)]
            col_pos = {tool:i for i, tool in enumerate(sorted(toolList))}
            nameNone = [" " for i in toolList]

            self.usertableWidget.setColumnCount(len(toolList))
            self.usertableWidget.setHorizontalHeaderLabels(nameNone)
            self.usertableWidget.setRowCount(len(hourList))
            self.usertableWidget.setVerticalHeaderLabels(hourList)
            self.usertableWidget.horizontalHeader().setDefaultSectionSize(10)
            self.usertableWidget.verticalHeader().setDefaultSectionSize(3)
                    
            for dt in self.stepLogs[step][user]:
                if dt.year == date.year and dt.month == date.month and dt.day == date.day:
                    tool = self.stepLogs[step][user][dt]['dcc']
                    time = self.stepLogs[step][user][dt]['time']
                    row = dt.hour
                    col = col_pos[tool]

                    item = self.usertableWidget.item(row, col)
                    if not item:
                        item = QtWidgets.QTableWidgetItem('')
                        text = '%s, %s'%(tool, time)
                        item.setToolTip(text)
                        item.setTextAlignment(QtCore.Qt.AlignCenter)
                        item.setForeground(QtCore.Qt.white)
                        item.setBackground(QtGui.QColor(157, 224, 173))
                        self.usertableWidget.setItem(row, col, item)


def show():
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        myApp = App()
        stylesheet.set_default(app)
        myApp.show()
        sys.exit(app.exec_())

if __name__ == '__main__':
    show()