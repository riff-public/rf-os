# v.0.0.1 polytag switcher
_title = 'ExportLayout'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'ExportLayoutUI'
# dev 
#Import python modules
import sys
import os 
import json 
import getpass

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# config file 
config_file = '%s/local_config.json' % moduleDir
with open(config_file) as configData: 
    localConfig = json.load(configData)

SCRIPT = localConfig['SCRIPT']
ROOT = os.environ.get(SCRIPT)
core = localConfig['CORE']

# import config
sys.path.append('%s/%s' % (ROOT, core))
import rf_config as config

# framework modules 
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet
from rftool.utils import maya_utils
from rf_utils import icon 
from rf_utils import file_utils
from rf_utils.pipeline import user_pref




user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui



import maya.cmds as mc


class ExportLayout(QtWidgets.QMainWindow):


    def __init__(self, parent=None):
        #Setup Window
        super(ExportLayout, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        if config.isMaya: 
            self.ui = load.setup_ui_maya(uiFile, parent)
        else: 
            self.ui = load.setup_ui(uiFile, self)
        
        self.ui.show()
        self.ui.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.display_shot()

    def show_file_name(self):
        return mc.file(q=True, sn=True)

    def display_shot(self):
        shots = mc.ls(sl=True)
        for ix in shots:
            startTime =  mc.shot(ix, st=True, q=True)
            endTime =  mc.shot(ix, et=True, q=True)
            duration = mc.shot(ix, sd=True, q=True)
            showShot = '%s %s: %s %s' % (ix,startTime, endTime, duration)
            item = QtWidgets.QListWidgetItem(self.ui.shotName_listView)
            item.setText(showShot)

    def check_stated_selectedItem(self):
        if nonSelected_checkBox.isCheck() == True :
            pass
        elif selected_checkBox.isCheck() == True :
            pass



def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = ExportLayout(maya_win.getMayaWindow())
        return myApp

    else: 
        logger.info('Run in Standalone\n')
        app = QtWidgets.QApplication.instance()
        if not app: 
            app = QtWidgets.QApplication(sys.argv)
        myApp = ExportLayout()
        stylesheet.set_default(app)
        sys.exit(app.exec_())

if __name__ == '__main__': 
    show()


