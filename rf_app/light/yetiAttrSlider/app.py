import sys, os
from collections import defaultdict
import json

import pymel.core as pm
import maya.OpenMaya as om

from Qt import wrapInstance, QtGui, QtCore, QtWidgets
from rftool.utils.ui import maya_win
from rf_utils.ui import load as loadUi
reload(loadUi)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

_uiName = 'yetiAttrSliderWin'
_version = 'beta'
moduleDir = os.path.dirname(sys.modules[__name__].__file__)

pluginName = 'pgYetiMaya.mll'
try:
    if not pm.pluginInfo(pluginName, q=True, l=True):
        pm.loadPlugin(pluginName, qt=True)
except:
    logger.error('Failed to load %s plugin!' %(pluginName))

# load UI
uiFile = '%s\\ui.ui' %moduleDir
form_class, base_class = loadUi.loadUiType(uiFile)

'''
listWidget.scrollToItem(item, QtGui.QAbstractItemView.PositionAtTop)
'''
class YetiAttrSlider(base_class, form_class):
    def __init__(self, parent):
        super(YetiAttrSlider, self).__init__(parent)
        self.setupUi(self)

        # win title
        self.setWindowTitle(QtWidgets.QApplication.translate("yetiAttrSliderWin", 
                                                    "Yeti Attribute Slider - v.%s" %_version, None))

        # action
        self.frameSelected_shortcut = QtWidgets.QShortcut(QtGui.QKeySequence("F"), self.treeWidget, self.frameSelected)

        # connects
        self.treeWidget.itemSelectionChanged.connect(self.selectFromList)

        self.viewportDensity_slider.valueChanged.connect(lambda : self.sliderChanged('viewportDensity'))
        self.viewportWidth_slider.valueChanged.connect(lambda : self.sliderChanged('viewportWidth'))
        self.renderDensity_slider.valueChanged.connect(lambda : self.sliderChanged('renderDensity'))
        self.renderWidth_slider.valueChanged.connect(lambda : self.sliderChanged('renderWidth'))

        self.viewportDensity_spinBox.editingFinished.connect(lambda : self.spinBoxChanged('viewportDensity'))
        self.viewportWidth_spinBox.editingFinished.connect(lambda : self.spinBoxChanged('viewportWidth'))
        self.renderDensity_spinBox.editingFinished.connect(lambda : self.spinBoxChanged('renderDensity'))
        self.viewportWidth_spinBox.editingFinished.connect(lambda : self.spinBoxChanged('renderWidth'))

        self.refresh_pushButton.clicked.connect(self.populate)
        self.set_pushButton.clicked.connect(self.set)
        self.show()

        # class vars
        self.mapData = {}
        self.scriptJobs = []
        self.lastSelItem = None
        
        # default method calls
        self.setupScriptJob()
        self.populate()

    def frameSelected(self):
        
        if self.lastSelItem:
            self.treeWidget.scrollToItem(self.lastSelItem, QtWidgets.QAbstractItemView.PositionAtTop)

    def setupScriptJob(self):
        selChangedJob = pm.scriptJob(e=('SelectionChanged', self.yetiSelected))
        self.scriptJobs = [selChangedJob]
        # print selChangedJob

    def closeEvent(self, event):
        for jid in self.scriptJobs:
            if pm.scriptJob(ex=jid):
                pm.scriptJob(kill=jid, force=True)
        # print 'closeEvent'

    def yetiSelected(self):
        msel = om.MSelectionList()
        om.MGlobal.getActiveSelectionList(msel)
        selIt = om.MItSelectionList(msel)
        selNodes = []
        while not selIt.isDone():
            mobj = om.MObject()
            selIt.getDependNode(mobj)
            apiType = mobj.apiType()
            if apiType == 110:  # it's a transform
                mdag = om.MDagPath()
                selIt.getDagPath(mdag)
                try:
                    mdag.extendToShape()
                except:
                    selIt.next()
                    continue
                node = mdag.node()
                if node.apiType() == 706:  # it's pgYetiMaya type
                    name = mdag.partialPathName()
                    pyNode = pm.PyNode(name)
                    selNodes.append(pyNode)
            selIt.next()

        
        self.treeWidget.itemSelectionChanged.disconnect()
        
        for uiObj, node in self.mapData.iteritems():
            # uiObj.setSelected(False)
            if node in selNodes:
                uiObj.setSelected(True)
                self.lastSelItem = uiObj
            else:
                uiObj.setSelected(False)

        # if currItem:
        # self.treeWidget.setCurrentItem(currItem)
        self.treeWidget.itemSelectionChanged.connect(self.selectFromList)

    def sliderChanged(self, name):
        exec('value = self.%s_slider.value()' %name)
        exec('self.%s_spinBox.setValue(%s)' %(name, value))

    def spinBoxChanged(self, name):
        exec('value = self.%s_spinBox.value()' %name)
        exec('self.%s_slider.setValue(%s)' %(name, value))

    def selectFromList(self):
        # print 'i'
        toSels = []
        self.treeWidget.itemSelectionChanged.disconnect()
        for i in xrange(self.treeWidget.topLevelItemCount()):
            topItem = self.treeWidget.topLevelItem(i)
            selAll = False
            if topItem.isSelected():
                topItem.setSelected(False)
                selAll = True
            for c in xrange(topItem.childCount()):
                child = topItem.child(c)
                if selAll:
                    child.setSelected(True)
                    self.lastSelItem = child
                if child.isSelected():
                    toSels.append(self.mapData[child].getParent())
        if toSels:
            pm.select(toSels, r=True)
        self.treeWidget.itemSelectionChanged.connect(self.selectFromList)

    def populate(self):
        self.treeWidget.clear()

        yetiNodes = pm.ls(type='pgYetiMaya')
        data = defaultdict(list)
        for yn in yetiNodes:
            ns = yn.namespace()
            data[ns].append(yn)

        self.mapData = {}
        for ns, nodes in data.iteritems():
            if not ns:
                ns = ':(root)'

            # top item
            topItem = QtWidgets.QTreeWidgetItem()
            topItem.setText(0, ns)
            # topItem.setFlags(topItem.flags() & ~QtCore.Qt.ItemIsSelectable)

            self.treeWidget.addTopLevelItem(topItem)

            for i, node in enumerate(nodes):
                name = node.getParent().shortName().replace(ns, '')
                # child item
                item = QtWidgets.QTreeWidgetItem()
                item.setText(0, name)
                item.setFlags(item.flags() & ~QtCore.Qt.ItemIsUserCheckable)
                topItem.insertChild(i, item)
                self.addDefaultNote(node)
                self.mapData[item] = node

            topItem.setExpanded(True)

    def addDefaultNote(self, node):
        if not node.hasAttr('notes'):
            pm.addAttr(node, ln='notes', dt='string')

        current_note = node.notes.get()
        if not current_note:
            viewportDensity = node.viewportDensity.get()
            viewportWidth = node.viewportWidth.get()
            renderDensity = node.renderDensity.get()
            renderWidth = node.renderWidth.get()
            value_dict = {'viewportDensity': viewportDensity,
                        'viewportWidth': viewportWidth,
                        'renderDensity': renderDensity,
                        'renderWidth': renderWidth}
            note = {'defaultAttributes':value_dict}
            s_note = json.dumps(note)
            node.notes.set(s_note)

    def set(self):
        for i in xrange(self.treeWidget.topLevelItemCount()):
            topItem = self.treeWidget.topLevelItem(i)
            for c in xrange(topItem.childCount()):
                child = topItem.child(c)
                if child.isSelected():
                    node = self.mapData[child]
                    noteValues = {}
                    try:
                        noteValues = json.loads(node.notes.get())['defaultAttributes']
                    except Exception, e:
                        print e
                        continue
                    if noteValues:
                        for attr, default_value in noteValues.iteritems():
                            nodeAttr = node.attr(attr)
                            curr_value = nodeAttr.get()
                            exec('percentage = self.%s_spinBox.value()' %attr)
                            new_value = default_value * percentage * 0.01
                            # print new_value
                            if new_value != curr_value:
                                nodeAttr.set(new_value)

def show():
    try:
        maya_win.deleteUI(_uiName)
    except Exception, e:
        print e 
    
    myApp = YetiAttrSlider(parent=maya_win.getMayaWindow())

    return myApp

'''

from rf_app.light.yetiAttrSlider import app
reload(app)
app.show()

'''