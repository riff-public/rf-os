import os 
import sys
import logging 
import collections

import pymel.core as pm
import maya.cmds as mc
import maya.mel as mel

from rf_utils import file_utils


# logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

moduleDir = os.path.dirname(sys.modules[__name__].__file__)


class Config: 
    configDir = '%s/%s' % (moduleDir.replace('\\', '/'), '_config')
    configName = 'renderlayer_setup.yml'

def config_file(project): 
    configFile = '%s/%s/%s' % (Config.configDir, project, Config.configName)
    return configFile

def create_grp(name):
    if not pm.objExists(name):
        return pm.group(em=True, name=name)
    else:
        grps = pm.ls(name, type='transform')
        return grps[0]

def create_nested_grps(d, parent=None):
    for key, value in d.iteritems():
        grp = create_grp(key)
        if isinstance(value, collections.OrderedDict):
            create_nested_grps(value, parent=grp)
        
        if parent:
            pm.parent(grp, parent)
    
def create_render_layers(project='default'):
    config_path = config_file(project)
    data = file_utils.ymlLoader(config_path)

    # create groups
    create_nested_grps(data['GROUPS'])
    # set group colors
    if 'COLORS' in data:
        for obj in data['COLORS']:
            if not pm.objExists(obj):
                logger.warning('Node does not exist: %s' %obj)
            else:
                colors = data['COLORS'][obj]

                node = pm.PyNode(obj)
                node.useOutlinerColor.set(True)
                node.outlinerColor.set(colors)

    # create rsMatte
    for name in data['MATTE']:
        ls = pm.ls(name, type='RedshiftMatteParameters')
        if ls:
            node = ls[0]
        else:
            node = pm.createNode('RedshiftMatteParameters', n=name)
        if 'MEMBERS' in data['MATTE'][name] and data['MATTE'][name]['MEMBERS']:
            members = data['MATTE'][name]['MEMBERS']
            existing_members = []
            for m in members:
                if not pm.objExists(m):
                    logger.warning('Node does not exist: %s' %m)
                else:
                    existing_members.append(m)
            if existing_members:
                pm.sets(node, add=existing_members)

        if 'SETATTR' in data['MATTE'][name] and data['MATTE'][name]['SETATTR']:
            attrs = data['MATTE'][name]['SETATTR']
            for attr in attrs:
                splits = attr.split(' ')
                attr_name = splits[0]
                value = splits[1]
                exec('node.attr("%s").set(%s)' %(attr_name, value))

    # create rsVisibility
    for name in data['VISIBILITY']:
        ls = pm.ls(name, type='RedshiftVisibility')
        if ls:
            node = ls[0]
        else:
            node = pm.createNode('RedshiftVisibility', n=name)
        if 'MEMBERS' in data['VISIBILITY'][name] and data['VISIBILITY'][name]['MEMBERS']:
            members = data['VISIBILITY'][name]['MEMBERS']
            existing_members = []
            for m in members:
                if not pm.objExists(m):
                    logger.warning('Node does not exist: %s' %m)
                else:
                    existing_members.append(m)
            if existing_members:
                pm.sets(node, add=existing_members)
    for name in data['MESHPARAMETERS']:
        ls = pm.ls(name, type='RedshiftMeshParameters')
        if ls:
            node = ls[0]
        else:
            node = pm.createNode('RedshiftMeshParameters', n=name)
        if 'MEMBERS' in data['MESHPARAMETERS'][name] and data['MESHPARAMETERS'][name]['MEMBERS']:
            members = data['MESHPARAMETERS'][name]['MEMBERS']
            existing_members = []
            for m in members:
                if not pm.objExists(m):
                    logger.warning('Node does not exist: %s' %m)
                else:
                    existing_members.append(m)
            if existing_members:
                pm.sets(node, add=existing_members)

    # create renderLayers
    existing_layers = mc.ls(type='renderLayer')
    for name in data['RENDER_LAYERS']:
        if name in existing_layers:
            layer = pm.PyNode(name)
        else:
            layer = pm.createRenderLayer(n=name)
        if 'MEMBERS' in data['RENDER_LAYERS'][name] and data['RENDER_LAYERS'][name]['MEMBERS']:
            members = data['RENDER_LAYERS'][name]['MEMBERS']
            existing_members = []
            for m in members:
                if not pm.objExists(m):
                    logger.warning('Node does not exist: %s' %m)
                else:
                    existing_members.append(m)
            if existing_members:
                layer.addMembers(existing_members)

        if 'OVERRIDE' in data['RENDER_LAYERS'][name] and data['RENDER_LAYERS'][name]['OVERRIDE']:
            overrides = data['RENDER_LAYERS'][name]['OVERRIDE']
            i = 0
            for str_value in overrides:
                splits = str_value.split(' ')
                attr_name = splits[0]
                value = splits[1]

                if pm.objExists(attr_name):
                    pm.editRenderLayerAdjustment(attr_name, layer=layer)
                    exec('layer.adjustments[i].value.set(%s)' %value)
                    i += 1
                else:
                    logger.warning('Attribute does not exist: %s' %attr_name)