import os
import sys

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_utils import icon


class HiddenGeoUI(QtWidgets.QWidget):
   def __init__(self, parent=None):
        super(HiddenGeoUI, self).__init__(parent)
        
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
       
        self.verticalLayout = QtWidgets.QVBoxLayout()
        
        self.listWidget = QtWidgets.QListWidget()
        
        self.verticalLayout.addWidget(self.listWidget)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
       
        self.show_pushButton = QtWidgets.QPushButton("Show")
       
        self.horizontalLayout.addWidget(self.show_pushButton)
        self.hide_pushButton = QtWidgets.QPushButton("Hide")
        self.horizontalLayout.addWidget(self.hide_pushButton)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.verticalLayout_2.addLayout(self.verticalLayout)

        self.centralwidget = QtWidgets.QHBoxLayout()
        self.centralwidget.addLayout(self.verticalLayout_2)
        self.setLayout(self.centralwidget)