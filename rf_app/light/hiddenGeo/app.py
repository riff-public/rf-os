_title = 'Riff HiddenGeo'
_version = 'v.0.0.1'
_des = 'beta'
uiName = 'RFHiddenGeo'

#Import python modules
import sys
import os
import getpass
import json
import logging

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# import config
import rf_config as config
from rf_utils import log_utils
# sg

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import ui
import re 
import maya.cmds as mc
from rftool.utils import maya_utils
from rf_utils.context import context_info
import pymel.core as pm 
from rf_utils import register_shot
from functools import partial 
from rf_maya.rftool.utils import abc_utils
from rf_utils.context import context_info
from rf_maya.rftool.utils import pipeline_utils
from rf_maya.lib import shot_lib
import pymel.core as pm
from functools import partial


class RFHiddenGeo(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #SetUp Window
        super(RFHiddenGeo, self).__init__(parent)
        self.ui = ui.HiddenGeoUI(parent)
        self.setObjectName(uiName)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.setCentralWidget(self.ui)
        self.resize(300, 300)
        self.init_ui()
        self.__init_signal()

    def init_ui(self):
    	self.list_geo()

    def __init_signal(self):
        self.ui.show_pushButton.clicked.connect(partial(self.show_hidden, hidden=False ))
        self.ui.hide_pushButton.clicked.connect(partial(self.show_hidden, hidden=True ))

    def list_geo(self): 
        geo_grp = mc.ls('*:Geo_Grp')
        for geo in geo_grp:
            self.add_item(geo)
            

    def add_item(self, geo):
        namespace = geo.split(':Geo_Grp')[0]
        ref_path = mc.getAttr('%s.refPath'%geo) if mc.objExists('%s.refPath'%geo) else None
        item = QtWidgets.QListWidgetItem()
        item.setText(namespace)
        item.setData(QtCore.Qt.UserRole, ref_path)
        self.ui.listWidget.addItem(item)

    def show_hidden(self,hidden=False):
        cur_item = self.ui.listWidget.currentItem()
        namespace = cur_item.text()
        ref_path = cur_item.data(QtCore.Qt.UserRole)
        list_geo = self.get_hidden_geo_data(ref_path)
        self.edit_hidden_geo(namespace, list_geo, hidden=hidden )


    def get_hidden_geo_data(self, ref_path):
        asset = context_info.ContextPathInfo(path= ref_path)
        geos = pipeline_utils.read_hiddenGeo_data(entity =asset)
        return geos
            
    def edit_hidden_geo(self, namespace, geo_list, hidden = False):
   
        shot_lib.hidden_geo(namespace, geo_list, hidden=hidden)

def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = RFHiddenGeo(maya_win.getMayaWindow())
        myApp.show()
        return myApp
if __name__ == '__main__':
    show()