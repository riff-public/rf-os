# v.0.0.1 polytag switcher
_title = 'Riff Light Tool'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'RFLightTool'
uiUtName = 'RFLightUtils'

#Import python modules
import sys
import os
import getpass
import logging
from collections import OrderedDict
from functools import partial

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
import rf_config as config

if config.isMaya:
    import maya.cmds as mc
    import maya.mel as mm

# framework modules
from rf_utils import log_utils
from rf_utils import file_utils
from rf_utils.context import context_info
from rftool.render.redshift import rs_utils
from rf_utils.deadline import render_queue
from rftool.utils import pipeline_utils
from rf_app.light import aov_setup
from rf_app.light import render_preset

from rftool.utils import objectsInCameraView as ov
from rf_maya.lib import sequencer_lib
from rftool.utils import maya_utils
from rf_utils import time_utils

reload(ov)

reload(render_queue)
reload(context_info)
reload(rs_utils)
reload(pipeline_utils)
reload(render_preset)
reload(aov_setup)

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

# import ui
# thread


class Config: 
    layerSetup = 'P:/SevenChickMovie/asset/publ/_global/light/hero/ShotPipeLine/shotLightingWorkFlow.ma'

class Ui(QtWidgets.QWidget):
    def __init__(self, parent=None) :
        super(Ui, self).__init__(parent)

        self.allLayout = QtWidgets.QVBoxLayout()
        self.label = QtWidgets.QLabel('Light Tools %s' % _version)
        self.allLayout.addWidget(self.label)

        self.subLayout = QtWidgets.QHBoxLayout()
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.subLayout.addItem(spacerItem1)
        self.allLayout.addLayout(self.subLayout)

        self.input_pushButton = QtWidgets.QPushButton('Show Cache Inputs')
        self.input_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.input_pushButton)

        self.line1 = self.line(self.allLayout)

        presetLabel = QtWidgets.QLabel('Render Setting Preset')
        self.presetLayout = QtWidgets.QVBoxLayout()
        self.presetLabel = QtWidgets.QLabel('No preset found')
        self.presetLayout.addWidget(presetLabel)
        self.presetLayout.addWidget(self.presetLabel)
        self.allLayout.addLayout(self.presetLayout)

        self.line2 = self.line(self.allLayout)

        self.layerSetting_pushButton = QtWidgets.QPushButton('Import layer')
        self.allLayout.addWidget(self.layerSetting_pushButton)

        self.layerSetup_pushButton = QtWidgets.QPushButton('Import layer (Render Setup)')
        self.allLayout.addWidget(self.layerSetup_pushButton)

        self.aov_pushButton = QtWidgets.QPushButton('Aov Setup')
        self.allLayout.addWidget(self.aov_pushButton)

        self.create_tree_collecions_pushButton = QtWidgets.QPushButton("Create Tree Collections")
        self.allLayout.addWidget(self.create_tree_collecions_pushButton)

        self.line3 = self.line(self.allLayout)

        self.matteId_pushButton = QtWidgets.QPushButton('Import Matte ID')
        self.matteId_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.matteId_pushButton)

        self.smoothId_pushButton = QtWidgets.QPushButton('Import Smooth info')
        self.smoothId_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.smoothId_pushButton)

        self.set_filename_prefix_pushButton = QtWidgets.QPushButton('Set Filename Prefix')
        self.set_filename_prefix_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.set_filename_prefix_pushButton)

        self.select_unseen_geo_pushButton = QtWidgets.QPushButton('Select Unseen Geo')
        self.select_unseen_geo_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.select_unseen_geo_pushButton)

        self.convert_image_plane_pushButton = QtWidgets.QPushButton('Convert Image Plane To Mesh')
        self.convert_image_plane_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.convert_image_plane_pushButton)

        self.switch_shade_pushButton = QtWidgets.QPushButton('Switch Shade to LDV')
        self.switch_shade_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.switch_shade_pushButton)

        self.switch_shade_part2_pushButton = QtWidgets.QPushButton('Switch Shade to LDV Part2')
        self.switch_shade_part2_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.switch_shade_part2_pushButton)

        self.lightImprtCA_pushButton = QtWidgets.QPushButton('Light LDV CA')
        self.lightImprtCA_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.lightImprtCA_pushButton)

        self.line4 = self.line(self.allLayout)

        utLabel = QtWidgets.QLabel('Utilities Tools')
        self.allLayout.addWidget(utLabel)

        self.utils_pushButton = QtWidgets.QPushButton('Utilities')
        self.utils_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.utils_pushButton)

        self.cameraOverscan_pushButton = QtWidgets.QPushButton('Camera Overscan')
        self.cameraOverscan_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.cameraOverscan_pushButton)

        self.submit_pushButton = QtWidgets.QPushButton('Send to Deadline')
        self.submit_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.submit_pushButton)


        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.allLayout.addItem(spacerItem)

        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(2, 2, 2, 2)

        self.allLayout.setStretch(0, 0)
        self.allLayout.setStretch(1, 0)
        self.allLayout.setStretch(2, 0)
        self.allLayout.setStretch(3, 0)
        self.allLayout.setStretch(5, 2)

        self.setLayout(self.allLayout)
        self.set_default()

    def line(self, layout, direction='h', parent=None):
        """ line """
        line = QtWidgets.QFrame(parent)
        line.setFrameShape(QtWidgets.QFrame.HLine) if direction == 'h' else line.setFrameShape(QtWidgets.QFrame.VLine)
        line.setFrameShadow(QtWidgets.QFrame.Sunken)
        layout.addWidget(line)
        return line

    def set_default(self): 
        pass 
        # self.stillPreset_pushButton.setStyleSheet('background-color: rgb(220, 220, 100); color: rgb(40, 40, 40)')
        # self.animPreset_pushButton.setStyleSheet('background-color: rgb(220, 180, 100); color: rgb(40, 40, 40)')
        # self.renderPreset_pushButton.setStyleSheet('background-color: rgb(100, 220, 100); color: rgb(40, 40, 40)')
        # self.save_setting_pushButton.setStyleSheet('background-color: rgb(100, 200, 100); color: rgb(40, 40, 40)')
        # self.utils_pushButton.setStyleSheet('background-color: rgb(100, 200, 100); color: rgb(40, 40, 40)')


class Utils_Ui(QtWidgets.QWidget):
    def __init__(self, parent=None) :
        super(Utils_Ui, self).__init__(parent)
        self.allLayout = QtWidgets.QVBoxLayout()
        self.label = QtWidgets.QLabel('Light Utils %s' % _version)
        self.allLayout.addWidget(self.label)

        self.move_origin_pushButton = QtWidgets.QPushButton('Move to Origin')
        self.move_origin_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.move_origin_pushButton)

        self.line = self.line(self.allLayout)

        self.save_setting_pushButton = QtWidgets.QPushButton('Save Render Setting')
        self.save_setting_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.save_setting_pushButton)

        self.apply_setting_pushButton = QtWidgets.QPushButton('Apply Render Setting')
        self.apply_setting_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.apply_setting_pushButton)

        self.setLayout(self.allLayout)

    def line(self, layout, direction='h', parent=None):
        """ line """
        line = QtWidgets.QFrame(parent)
        line.setFrameShape(QtWidgets.QFrame.HLine) if direction == 'h' else line.setFrameShape(QtWidgets.QFrame.VLine)
        line.setFrameShadow(QtWidgets.QFrame.Sunken)
        layout.addWidget(line)
        return line


class UtilsTool(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(UtilsTool, self).__init__(parent)

        # ui read
        self.parent = parent
        self.ui = Utils_Ui(parent)
        self.setCentralWidget(self.ui)
        self.setObjectName(uiUtName)

        self.init_signals()

    def init_signals(self): 
        self.ui.move_origin_pushButton.clicked.connect(self.run_move_origin)
        self.ui.save_setting_pushButton.clicked.connect(partial(self.run_save_setting, 'Export'))
        self.ui.apply_setting_pushButton.clicked.connect(partial(self.run_save_setting, 'Import'))

    def run_move_origin(self): 
        from rftool.fix import moveSceneToOrigin
        reload(moveSceneToOrigin)
        moveSceneToOrigin.main()

    def run_save_setting(self, mode=''): 
        from rf_maya.rftool.utils import renderlayer_utils
        reload(renderlayer_utils)
        if mode == 'Export': 
            path = renderlayer_utils.export_renderlayer_adjustments()
        elif mode == 'Import': 
            path = renderlayer_utils.import_renderlayer_adjustments()


class LightTool(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(LightTool, self).__init__(parent)

        # ui read
        self.parent = parent
        self.ui = Ui(parent)
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)

        # title
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.resize(200, 200)

        self.init_functions()
        self.init_signals()
        self.set_tool_tips()

    def init_functions(self):
        self.ui.submit_pushButton.setVisible(False) 
        self.list_preset_buttons()

    def init_signals(self):
        self.ui.input_pushButton.clicked.connect(self.show_cache_input)
        self.ui.matteId_pushButton.clicked.connect(self.import_matteId)
        self.ui.smoothId_pushButton.clicked.connect(self.import_smooth_info)
        self.ui.submit_pushButton.clicked.connect(self.submit_deadline)
        self.ui.set_filename_prefix_pushButton.clicked.connect(self.set_filename_prefix)
        self.ui.select_unseen_geo_pushButton.clicked.connect(self.select_unseen_geo)
        self.ui.layerSetting_pushButton.clicked.connect(self.run_layer_setup)
        self.ui.layerSetup_pushButton.clicked.connect(self.run_import_layer_setup)
        self.ui.aov_pushButton.clicked.connect(self.run_aov_setup)
        self.ui.create_tree_collecions_pushButton.clicked.connect(self.run_create_tree_collecions)
        self.ui.utils_pushButton.clicked.connect(self.run_utils)
        self.ui.cameraOverscan_pushButton.clicked.connect(self.run_overscan)
        self.ui.convert_image_plane_pushButton.clicked.connect(self.covert_image_plane)
        self.ui.switch_shade_pushButton.clicked.connect(self.switch_shade_ldv)
        self.ui.lightImprtCA_pushButton.clicked.connect(self.lightImprtCA)

    def set_tool_tips(self): 
        self.ui.aov_pushButton.setToolTip('Config file : %s' % aov_setup.config_file())
        self.ui.layerSetting_pushButton.setToolTip('Maya file : %s' % Config.layerSetup)

    def get_reference_list(self, entity): 
        refs = mc.file(q=True, r=True)
        keyGrp = entity.projectInfo.asset.geo_grp() or 'Geo_Grp'
        paths = {}

        for ref in refs: 
            namespace = mc.referenceQuery(ref, ns=True)
            path = mc.referenceQuery(ref, f=True)
            targetGeo = '%s:%s' % (namespace[1:], keyGrp)

            if mc.objExists(targetGeo):
                allGeos = []
                allChildren = mc.listRelatives(targetGeo, ad=True, pa=True, type='transform')
                if allChildren:
                    allGeos = [c for c in allChildren if mc.listRelatives(c, s=True, ni=True, pa=True)]
                paths[path] = allGeos

        return paths 

    def select_unseen_geo(self):
        entity = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))
        namespaces = []
        shotName = entity.name_code
        refPaths = self.get_reference_list(entity)
        # lenRef = len(refPaths)

        if mc.objExists(shotName) and refPaths: 
            startFrame, endFrame, seqStartFrame, seqEndFrame = sequencer_lib.shot_info(shotName)
            cam = mc.shot(shotName, q=True, cc=True)
            
            if mc.objectType(cam) == 'camera': 
                cam = mc.listRelatives(cam, p=True)[0]

            with time_utils.Duration(): 
                seen_objects = ov.getUnseenReferenceNodes_selection(cam, 
                        refs=refPaths.keys(), timeRange=[startFrame, endFrame], step=3,
                        returnRefNode=False)
                unseen_objects = []
                for path, geos in refPaths.iteritems():
                    for geo in geos:
                        if geo not in seen_objects:
                            unseen_objects.append(geo)
                if unseen_objects:
                    mc.select(unseen_objects, r=True)

    def covert_image_plane(self):
        from rf_maya.rftool.model.materialCreator import core
        reload(core)

        planeName = 'imagePlane_Geo'
        cam = mc.ls(sl=True)[0]
        shape = mc.listRelatives(cam, children=True)[0]
        img = mc.listRelatives(shape, ad=True, type='imagePlane')
        if img:
            img = img[0]
        if mc.objExists(planeName):
            mc.delete(planeName)
        mc.polyPlane(name = planeName, w=1 ,h=1 ,sx=1 ,sy=1 ,ax=[0,0,1] ,cuv=2 ,ch=1)
        mc.parent( planeName, cam, r=True)
        express = "%s.translateZ = %s.depth * -1;" % (planeName, img)
        express += "\npolyPlane -e -width (abs(%s.boundingBoxMinX) + abs(%s.boundingBoxMaxX)) %s;"% (img, img, planeName)
        express += "\npolyPlane -e -height (abs(%s.boundingBoxMinY) + abs(%s.boundingBoxMaxY)) %s;"% (img, img, planeName)
        mc.expression(s=express)
        
        planeShade = "imagePlane_mtr"  
        shd = mc.shadingNode("surfaceShader" , name=planeShade, asShader=True)
        shdSG = mc.sets(name='%sSG' % shd, empty=True, renderable=True, noSurfaceShader=True)
        mc.connectAttr('%s.outColor' % shd, '%s.surfaceShader' % shdSG)
        fileShd = "imageFile"
        MatCreate = core.MaterialCreator()
        imgShd = MatCreate.create_file('fileShd')
        mc.connectAttr('%s.outColor' % imgShd, '%s.outColor' % shd)
        fn = mc.getAttr('%s.imageName' % img)
        try:
            startFrame = int(fn.split('.')[-2])
        except:
            startFrame = mc.playbackOptions( q=True,min=True )
        mc.connectAttr("%s.imageName"%img, "%s.fileTextureName"%imgShd)
        mc.connectAttr("%s.frameOffset"%img, "%s.frameOffset"%imgShd)
        mc.connectAttr("%s.useFrameExtension"%img, "%s.useFrameExtension"%imgShd)
        express = "%s.startCycleExtension = %s + %s.frameOffset;" % (imgShd,startFrame,img)
        express += "\n%s.endCycleExtension = %s + %s.frameOffset + %s.frameCache;" % (imgShd,startFrame,img,img)
        mc.setAttr('%s.useHardwareTextureCycling'%imgShd , 1)
        mc.expression(s=express)
        mc.sets(planeName ,e=True, forceElement=shdSG )
        mc.select(imgShd.nodeName())

    def switch_shade_ldv(self):
        from rf_maya.contextMenu.dag import shade_menu
        reload(shade_menu)
        sels = mc.ls(sl=True)
        shade_menu.multiple_switch(shadeKey='_ldv_',selection=sels)

    def switch_shade_ldv_part2(self):
        from rf_maya.contextMenu.dag import shade_menu
        reload(shade_menu)
        sels = mc.ls(sl=True)
        shade_menu.multiple_switch(shadeKey='_ldv_',selection=sels)
        shade_menu.multiple_switch(shadeKey='_ldv_part02_',selection=sels)

    def list_preset_buttons(self): 
        entity = context_info.ContextPathInfo(path=mc.file(q=True, loc=True))
        project = entity.project if entity.project else None
        presets = render_preset.list_preset(project)
        configFiles = render_preset.list_config(project)

        self.buttons = dict()

        if presets: 
            for i, preset in enumerate(presets): 
                color = 100 
                button = QtWidgets.QPushButton(preset)
                self.ui.presetLayout.addWidget(button)
                button.clicked.connect(partial(render_preset.apply_config, project, preset))
                self.buttons[preset] = button

                button.setStyleSheet('background-color: rgb(220, 200, %s); color: rgb(40, 40, 40)' % (color + (i * 40)))
                button.setToolTip('Config file: %s' % configFiles[i])

            self.ui.presetLabel.setVisible(False)


    def show_cache_input(self): 
        from rf_app.scene.builder import app
        reload(app)
        app.show() 

    def import_matteId(self): 
        from rf_app.lookdev.import_matte import import_matte
        reload(import_matte)
        sels = mc.ls(sl=True) or None
        selection = True  if sels else False 
        if not selection: 
            result = mc.confirmDialog( title='Confirm', message='Apply MatteId to all assets?', button=['Yes','No'])
            if result == 'No': 
                return 
              
        assetDict = pipeline_utils.list_assets(sels=sels)
        for path, namespaces in assetDict.iteritems(): 
            print path
            smPlys = []
            asset = context_info.ContextPathInfo(path=path)
            import_matte.import_puzz_matte(asset, namespaces)
                

    def import_smooth_info(self): 
        sels = mc.ls(sl=True) or None
        selection = True  if sels else False 

        if not selection: 
            result = mc.confirmDialog( title='Confirm', message='Apply smooth to all assets?', button=['Yes','No'])
            if result == 'No': 
                return 
              
        assetDict = pipeline_utils.list_assets(sels=sels)
        for path, namespaces in assetDict.iteritems(): 
            smPlys = []
            asset = context_info.ContextPathInfo(path=path)
            if not asset.type == 'mattepaint':
                smoothFile = rs_utils.smooth_data_file(asset)
                plys = file_utils.ymlLoader(smoothFile)
                nodeName = '%s_meshParameter' % asset.name

                if plys: 
                    for namespace in namespaces: 
                        smPlys += ['%s:%s' % (namespace, a) for a in plys]

                    smPlys = [a for a in smPlys if mc.objExists(a)]
                    rs_utils.add_smooth_ply(nodeName, smPlys)

                    mc.setAttr ("%s.screenSpaceAdaptive" % nodeName, 1) # fix smooth UV

    def submit_deadline(self): 
        # set output prefix 
        # send to deadline 
        from rf_maya.lib import sequencer_lib
        scene = context_info.ContextPathInfo()
        shot_name = scene.name_code
        workspaceOutput = scene.path.work_output().abs_path()
        audioMov = scene.publish_name(ext='.mov')
        filename = scene.publish_name(ext='.png').replace('.png', ".####.png")
        dst_path = os.path.join(workspaceOutput, audioMov).replace('\\', '/')
        cam_ = '%s_cam'% shot_name
        startFrame, endFrame, seqStartFrame, seqEndFrame = sequencer_lib.shot_info(scene.name_code)
        render_queue.render(scene.path.name().abs_path(), int(startFrame), int(endFrame), cam_, dst_path, shot_name, filename)


    def run_layer_setup(self): 
        # import maya.cmds as mc 
        # return mc.file(Config.layerSetup, i=True, type='mayaAscii', options='v=0', pr=True, loadReferenceDepth='all') 
        from rf_app.light import renderlayer_setup as rls
        reload(rls)
        rls.create_render_layers()

    def run_import_layer_setup(self): 
        import maya.cmds as mc 
        import maya.app.renderSetup.model.renderSetup as renderSetup
        import json
        entity = context_info.ContextPathInfo(path=mc.file(q=True, loc=True))
        project = entity.project if entity.project else None
        layerSetup = 'P:/%s/rnd/light/Lighting_Template/Hero' % (project)
        if os.path.exists(layerSetup):
            # import ma file
            layerSetupMa = layerSetup + '/%s.ma'%(project)
            mc.file(layerSetupMa, i=True, type='mayaAscii', options='v=0', pr=True, loadReferenceDepth='all') 

            # import json file for render setup
            layerSetupJson =  layerSetup + '/RenderLayer.json'
            with open(layerSetupJson, "r") as file:
                renderSetup.instance().decode(json.load(file), renderSetup.DECODE_AND_OVERWRITE, None)
                
    def run_aov_setup(self): 
        aov_setup.create_maya_aov(project='default') 

    def run_create_tree_collecions(self):
        from rf_app.light import createTreeCollections
        reload(createTreeCollections)
        createTreeCollections.runUI(parent=self.ui)

    def lightImprtCA(self):
        # tawan code #
        from rf_app.light import ca_importLight
        reload(ca_importLight)

    def run_utils(self): 
        from rftool.utils.ui import maya_win
        maya_win.deleteUI(uiUtName)
        ui = UtilsTool(maya_win.getMayaWindow())
        ui.show()

    def set_filename_prefix(self):
        from rf_utils.pipeline import file_name_prefix
        reload(file_name_prefix)
        scene = context_info.ContextPathInfo()
        file_name_prefix.file_name_prefix2(scene.step)

    def run_overscan(self):
        from rf_maya.rftool.light.utils import camera_overscan
        reload(camera_overscan)
        camera_overscan.runUI(parent=self.ui)

def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        maya_win.deleteUI(uiName)
        myApp = LightTool(maya_win.getMayaWindow())
        myApp.show()
        return myApp
