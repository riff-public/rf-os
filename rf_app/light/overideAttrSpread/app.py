_title = 'Riff overideAttrSpread'
_version = 'v.0.0.1'
_des = 'beta'
uiName = 'RFoverideAttrSpread'

#Import python modules
import sys
import os
import getpass
import json
import logging

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# import config
import rf_config as config
from rf_utils import log_utils
# sg

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import ui
import re 
import maya.cmds as mc
from rftool.utils import maya_utils
from rf_utils.context import context_info
import pymel.core as pm 
from rf_utils import register_shot
from functools import partial 
from rf_maya.rftool.utils import abc_utils
from rf_utils.context import context_info
from rf_maya.rftool.utils import pipeline_utils
from rf_maya.lib import shot_lib
import pymel.core as pm
from functools import partial


class RFoverideAttrSpread(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #SetUp Window
        super(RFoverideAttrSpread, self).__init__(parent)
        self.ui = ui.OverideAttrSpreadUI(parent)
        self.setObjectName(uiName)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.setCentralWidget(self.ui)
        self.resize(300, 300)
        self.init_ui()
        self.__init_signal()

    def init_ui(self):
        render_layers = {mc.getAttr( i + ".displayOrder") : i for i in mc.ls(type='renderLayer')}
        self.renderLayerSorted = []
        for num , layer in render_layers.iteritems():
            self.renderLayerSorted.append(layer)
        for layer in self.renderLayerSorted:
            self.ui.comboBox.addItem(layer)
        self.refreshList()

        self.ui.listWidget.setSelectionMode(self.ui.listWidget.ExtendedSelection)

    def __init_signal(self):
        #self.show_pushButton.clicked.connect(self.test)
        self.ui.comboBox.currentIndexChanged.connect(self.refreshList)
        self.ui.set_pushButton.clicked.connect(self.setOveride)
        self.ui.remove_pushButton.clicked.connect(self.removeOveride)

    def refreshList(self):
        self.ui.listWidget.clear()
        currentLayer = self.ui.comboBox.currentText()
        mc.editRenderLayerGlobals( currentRenderLayer = currentLayer)

        self.idList = self.getPuzzleMatte()
        self.overideList = self.getOveride(currentLayer)

        for matte in self.idList:
            item = self.ui.add_widget_item(text = matte)        # get ID in renderLayer
            item.setTextColor(QtCore.Qt.gray)
            for override in self.overideList:                   # Check Overide Id
                if override.split('.')[0] == matte:
                    item.setCheckState(QtCore.Qt.Checked)
                    item.setTextColor(QtCore.Qt.green)

    def setOveride(self):
        currentLayer = self.ui.comboBox.currentText()
        selectedItem = self.ui.listWidget.selectedItems()
        for item in selectedItem:
            item.setCheckState(QtCore.Qt.Checked)
            item.setTextColor(QtCore.Qt.green)
            text = item.text()
            mc.editRenderLayerAdjustment( '%s.enabled' % text, layer=currentLayer )
            mc.setAttr('%s.enabled' % text, 1)

    def removeOveride(self):
        currentLayer = self.ui.comboBox.currentText()
        selectedItem = self.ui.listWidget.selectedItems()
        for item in selectedItem:
            item.setCheckState(QtCore.Qt.Unchecked)
            item.setTextColor(QtCore.Qt.gray)
            text = item.text()
            mc.editRenderLayerAdjustment( '%s.enabled' % text, layer=currentLayer, remove=True)
            mc.setAttr('%s.enabled' % text, 0)



    def getPuzzleMatte(self):
        aovList = mc.ls(type='RedshiftAOV')
        idList = []
        for aov in aovList:
            if '_ID_' in aov:
                idList.append(aov)
        return idList

    def getOveride(self, selectLayer):
        overideList = mc.editRenderLayerAdjustment(selectLayer, q=True, layer=True)
        return overideList
            

def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = RFoverideAttrSpread(maya_win.getMayaWindow())
        myApp.show()
        return myApp
if __name__ == '__main__':
    show()