import os
import sys

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_utils import icon


class OverideAttrSpreadUI(QtWidgets.QWidget):
	def __init__(self, parent=None):
		super(OverideAttrSpreadUI, self).__init__(parent)

		self.verticalLayout_2 = QtWidgets.QVBoxLayout()

		self.verticalLayout = QtWidgets.QVBoxLayout()

		self.comboBox = QtWidgets.QComboBox()

		self.listWidget = QtWidgets.QListWidget()
		self.verticalLayout.addWidget(self.comboBox)
		self.verticalLayout.addWidget(self.listWidget)
		self.horizontalLayout = QtWidgets.QHBoxLayout()

		self.set_pushButton = QtWidgets.QPushButton("Set Overide")

		self.horizontalLayout.addWidget(self.set_pushButton)
		self.remove_pushButton = QtWidgets.QPushButton("Remove Overide")
		self.horizontalLayout.addWidget(self.remove_pushButton)
		self.verticalLayout.addLayout(self.horizontalLayout)
		self.verticalLayout_2.addLayout(self.verticalLayout)

		self.centralwidget = QtWidgets.QHBoxLayout()
		self.centralwidget.addLayout(self.verticalLayout_2)
		self.setLayout(self.centralwidget)

	def add_widget_item(self, text=''):
		item = QtWidgets.QListWidgetItem()
		item.setCheckState(QtCore.Qt.Unchecked)
		item.setText(text)
		self.listWidget.addItem(item)
		return item