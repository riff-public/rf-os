import os 
import sys 
from rf_utils import file_utils
import logging 

# logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

# module name 
moduleDir = os.path.dirname(sys.modules[__name__].__file__)


class Config: 
	configDir = '%s/%s' % (moduleDir.replace('\\', '/'), '_config')


def list_config(project): 
	root = '%s/%s' % (Config.configDir, project)
	if not os.path.exists(root): 
		root = '%s/default' % (Config.configDir)
		logger.warning('Project not exists "%s"' % project)

	files = file_utils.list_file(root)
	return ['%s/%s' % (root, a) for a in files]

def list_preset(project): 
	skipFiles = ['aov_setup.yml', 'renderlayer_setup.yml']
	files = list_config(project)
	if files: 
		return [os.path.splitext(os.path.basename(a))[0] for a in files if not os.path.basename(a) in skipFiles]
	return []

def apply_config(project, presetName): 
	files = list_config(project)

	if files: 
		for file in files: 
			path, ext = os.path.splitext(file)
			filename = os.path.basename(path)

			if presetName in filename: 
				# apply preset
				summary = apply_preset(file, checkOnly=True)
				result = app_dialog(summary) 
				
				# apply if result == True 
				if result: 
					apply_preset(file)
					return 

		logger.warning('Preset "%s" not found' % presetName)
	else: 
		logger.warning('No preset found')


def apply_preset(presetFile, checkOnly=False): 
	data = file_utils.ymlLoader(presetFile)
	summary = []
	defaultData = data.get('default')

	if defaultData: 
		apply_maya_preset(defaultData, checkOnly=checkOnly)

	# find node 
	return apply_maya_preset(data, checkOnly=checkOnly)


def apply_maya_preset(data, checkOnly=False): 
	import maya.cmds as mc 
	import pymel.core as pm 
	summary = []
	# find node 
	nodes = data.get('node', [])

	if nodes: 
		for nodeName in nodes: 
			unpackData = data['node'][nodeName]

			for attrData in unpackData: 
				attr = attrData['attr']
				value = attrData['value']
				dataType = attrData['type']
				nodeAttr = '%s.%s' % (nodeName, attr)

				if pm.objExists(nodeAttr): 
					currentValue = pm.getAttr(nodeAttr)
					# padding
					paddingDecimal = len(str(value).split('.')[-1]) if '.' in str(value) else 0
					# print 'padding', paddingDecimal, currentValue
					# round up value 
					paddingValue = round(currentValue, paddingDecimal) if paddingDecimal else currentValue

					if not paddingValue == value: 
						message = '"%s" %s -> %s' % (nodeAttr, currentValue, value)
						summary.append(message)

					if not checkOnly: 
						pm.setAttr(nodeAttr, value)
				else: 
					logger.warning('Attr not exists "%s"' % nodeAttr)

	return summary


def app_dialog(summary): 
	import maya.cmds as mc 
	import pymel.core as pm 

	# convert to string 
	if not summary: 
		return True 
	message = 'Value on attributes below will be changed\n%s' % '\n'.join(summary)
	title = 'Confirm Value Changed'
	result = mc.confirmDialog( title=title, message=message, button=['Apply','Cancel'])
	return True if result == 'Apply' else False



