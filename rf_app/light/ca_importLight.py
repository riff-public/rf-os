import os
import maya.cmds as cmds
import maya.app.renderSetup.model.renderSetup as renderSetup
import json


def import_light(light_file_path):
    if os.path.exists(light_file_path):
        cmds.file(light_file_path, i=True)
        print('Light imported!')
    else:
        print('File does not exist!')
       

def import_render_setup(rs_file_path):
    if os.path.exists(rs_file_path):
        with open(rs_file_path, "r") as file:
            renderSetup.instance().decode(json.load(file), renderSetup.DECODE_AND_OVERWRITE, None)
        print('Render Setup imported!')
    else:
        print('File does not exist!')
        


def browse_file_path(text_field):
    file_path = cmds.fileDialog2(fileMode=1, caption="Select a file", okCaption="Select")[0]
    cmds.textField(text_field, edit=True, text=file_path)


def combined_function(light_file_path, rs_file_path):
    import_light(light_file_path)
    import_render_setup(rs_file_path)



window = cmds.window(title="Light Select", widthHeight=(300, 250), resizeToFitChildren=False, sizeable=False, bgc=[0.1, 0.1, 0.1])
layout = cmds.columnLayout(parent=window, adjustableColumn=True)

cmds.separator(height=14, style='none')
cmds.text(parent=layout, label="Light Import", font="boldLabelFont")
cmds.separator(height=14, style='none')


light_file_text = cmds.textField(parent=layout, width=280, text="P:/CA/rnd/lookdev/LookDev_Template/CA_LDV.ma")
rs_file_text = cmds.textField(parent=layout, width=280, text="P:/CA/rnd/lookdev/LookDev_Template/CA_LDV.json")




cmds.separator(height=12, style='none')
cmds.button(label="Import", command=lambda x: combined_function(cmds.textField(light_file_text, query=True, text=True),
                                                                 cmds.textField(rs_file_text, query=True, text=True)),
            bgc=[0,1,0], height=50)

cmds.separator(height=14, style='none')
cmds.text(parent=layout, label="LDV Template", font="boldLabelFont")
cmds.separator(height=14, style='none')



cmds.separator(height=12, style='none')
cmds.text(label="0.0.1   ", align="right", font="smallPlainLabelFont")
cmds.separator(height=10, style='none')


cmds.showWindow(window)
