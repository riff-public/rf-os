import os 
import sys 
from rf_utils import file_utils
import logging 
import maya.cmds as mc
# logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

# module name 
moduleDir = os.path.dirname(sys.modules[__name__].__file__)


class Config: 
	configDir = '%s/%s' % (moduleDir.replace('\\', '/'), '_config')
	aovName = 'aov_setup.yml'


def config_file(project='SevenChickMovie'): 
	configFile = '%s/%s/%s' % (Config.configDir, project, Config.aovName)
	return configFile 


def read_config(project): 
	file = config_file(project)
	print file 
	data = file_utils.ymlLoader(file)
	return data 

def create_maya_aov(project, renderer='redshift', multipleInstance=False): 
	""" multipleInstance = True, allow more than 1 aov type -> puzzleMatte """ 
	from rftool.render.redshift import rs_utils
	reload(rs_utils)
	data = read_config(project)
	aovData = data.get(renderer)

	print aovData
	if aovData: 
		nodeType = aovData['nodeType']
		aovTypes = aovData['aovType']

		for aov in aovTypes: 


			if not multipleInstance: 
				# check for node type first 
				create = True  if not aov in rs_utils.list_aov_type() else False 
			else: 
				create = True 
			if create: 
				print aov['aovName']
				result = rs_utils.create_aov(aov['aovName'])
				logger.info('Create %s: %s' % (aov, result))
				if aov['attr']:
					for attr, value in aov['attr'].iteritems():
						if attr == 'rename':
							result = mc.rename(result, value)
							mc.setAttr('%s.name'%(result), value, type= 'string')
						else:
							print '%s.%s'%(result, attr),'test'
							print value,'est'
							setAttr_type('%s.%s'%(result, attr), str(value))
	# # rs_utils.update_aov_list()


def setAttr_type(attr, value):
	type = mc.getAttr(attr,type=True)
	if type == 'string':
		mc.setAttr('%s'%(attr), value, type= 'string')
	else:
		print attr, value
		mc.setAttr('%s'%(attr), int(value))