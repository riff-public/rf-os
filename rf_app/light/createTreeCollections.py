from PySide2 import QtWidgets, QtCore, QtGui
from shiboken2 import wrapInstance
import pymel.core as pm
import maya.cmds as cmds

import maya.app.renderSetup.model.renderSetup as renderSetup
import maya.app.renderSetup.model.collection as collection
import maya.api.OpenMaya as OpenMaya

import os
import sys
import json

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

from rf_utils.ui.flatUi import main_ui
reload(main_ui)

class CreateTreeCollections_ui(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(CreateTreeCollections_ui, self).__init__(parent)


        self.allRenderLayers = self.getRenderLayers()

        self.__top_layout = QtWidgets.QGridLayout(self)
        self.__build_ui()
        self.__connect_signals()

    def __build_ui(self):
        layer_label = QtWidgets.QLabel("Select Layer", self)
        self.layer_cbb = QtWidgets.QComboBox(self)
        self.layer_cbb.addItems(self.allRenderLayers.keys())

        self.allOfThem_rad = QtWidgets.QRadioButton("All of them", self)
        self.allOfThem_rad.setChecked(True)
        self.selected_rad = QtWidgets.QRadioButton("Selected", self)

        self.create_btn = QtWidgets.QPushButton("Create Layer Collections", self)
        self.create_btn.setObjectName("blueBtn")

        self.__top_layout.addWidget(layer_label, 0, 0, 1, 1)
        self.__top_layout.addWidget(self.layer_cbb, 0, 1, 1, 1)
        self.__top_layout.addWidget(QHLine(), 1, 0, 1, 2)
        self.__top_layout.addWidget(self.allOfThem_rad, 2, 0, 1, 1)
        self.__top_layout.addWidget(self.selected_rad, 2, 1, 1, 1)
        self.__top_layout.addWidget(QHLine(), 3, 0, 1, 2)
        self.__top_layout.addWidget(self.create_btn, 4, 0, 1, 2)


    def __connect_signals(self):
        self.create_btn.clicked.connect(self.create)


    def create(self):
        layerName = self.layer_cbb.currentText()
        selection = self.selected_rad.isChecked()

        layer = self.allRenderLayers[layerName]
        layerType = self.find_layerType(["Light_Stylize", "Line", "Texture_Add"], layerName)

        if not layerType:
            return cmds.warning("Please select one of these layer [Light_Stylize", "Line", "Texture_Add]")

        #print layerName, layerType, layer
        result = self.createTreeCollections(layer, layerType, selection)

        if result:
            msg = QtWidgets.QMessageBox(self)
            msg.setWindowTitle("Result")
            msg.setText("Done")
            msg.setStandardButtons(msg.Ok)
            msg.exec_()
        else:
            cmds.warning("Something went wrong... Try to select again.")


    def getRenderLayers(self):
        rs = renderSetup.instance()
        allRenderLayers = rs.getRenderLayers()

        renderLayers = {i.name(): i for i in allRenderLayers}

        return renderLayers


    def find_layerType(self, layerTypeList, layer):
        for i in range(len(layerTypeList)):
            if layerTypeList[i] in layer:
                return layerTypeList[i]

        return layerTypeList[-1]

    def getObjectsData(self):
        d = {}
        objs = cmds.ls(sl=True)
        
        for obj in objs:
            if cmds.objectType(obj) != "transform":
                continue

            assetName = obj.split("_")[0]
            pattern = "{}*:Geo_Grp".format(assetName)
            
            if assetName not in d:
                d[assetName] = {"pattern": pattern,
                                "selection": [],
                                "shadingGroups": []}

            if assetName in obj:
                n = obj.split(":")[0] + ":Geo_Grp"
                d[assetName]["selection"].append(n)

            sgNs = pm.PyNode(obj).shadingGroups()[0].namespace()
            sgs = cmds.ls("{}*".format(sgNs), type="shadingEngine")
            d[assetName]["shadingGroups"] = sgs
            
        return d

    def refShader(self, shaderPath):

        shader = shaderPath.split("/")[-1].split(".ma")[0]
        rnNode = shader + "RN"
        
        if not cmds.objExists(rnNode):
            cmds.file(shaderPath, r=True, mergeNamespacesOnClash=True, ns=shader)
        
        shader = "{}:{}_shd".format(shader, shader)
        
        return shader

    
    def createFileTexture(self, fileTextureName, p2dName):
        tex = pm.shadingNode('file', name=fileTextureName, asTexture=True, isColorManaged=True)
        if not pm.objExists(p2dName):
            pm.shadingNode('place2dTexture', name=p2dName, asUtility=True)
        p2d = pm.PyNode(p2dName)
        tex.filterType.set(0)
        pm.connectAttr(p2d.outUV, tex.uvCoord)
        pm.connectAttr(p2d.outUvFilterSize, tex.uvFilterSize)
        pm.connectAttr(p2d.vertexCameraOne, tex.vertexCameraOne)
        pm.connectAttr(p2d.vertexUvOne, tex.vertexUvOne)
        pm.connectAttr(p2d.vertexUvThree, tex.vertexUvThree)
        pm.connectAttr(p2d.vertexUvTwo, tex.vertexUvTwo)
        pm.connectAttr(p2d.coverage, tex.coverage)
        pm.connectAttr(p2d.mirrorU, tex.mirrorU)
        pm.connectAttr(p2d.mirrorV, tex.mirrorV)
        pm.connectAttr(p2d.noiseUV, tex.noiseUV)
        pm.connectAttr(p2d.offset, tex.offset)
        pm.connectAttr(p2d.repeatUV, tex.repeatUV)
        pm.connectAttr(p2d.rotateFrame, tex.rotateFrame)
        pm.connectAttr(p2d.rotateUV, tex.rotateUV)
        pm.connectAttr(p2d.stagger, tex.stagger)
        pm.connectAttr(p2d.translateFrame, tex.translateFrame)
        pm.connectAttr(p2d.wrapU, tex.wrapU)
        pm.connectAttr(p2d.wrapV, tex.wrapV)
        return tex.name()

    def createTreeCollections(self, layer, layerType, selection):
        objs = cmds.ls(sl=True)
        if not objs:
            return cmds.warning("Please select object")
        
            
        collections = {c.name(): c for c in layer.getCollections()}
        
        data = self.getObjectsData()

        if not data:
            return False
        
        for asset, v in data.iteritems():
            colName = asset + "_" + layerType
            
            # delete if existing #
            if colName in collections.keys():
                collection.delete(collections[colName])

            col = layer.createCollection(colName)
             
            if selection:
                col.getSelector().staticSelection.set(v["selection"])
                col.getSelector().setPattern("")
            else:
                col.getSelector().staticSelection.set([])
                col.getSelector().setPattern(v["pattern"])
                
            # create new material #
            if layerType == "Light_Stylize":
                rsMat = cmds.shadingNode("RedshiftMaterial", asShader=True)
                pm.setAttr("{}.diffuse_color".format(rsMat), 1.33567, 1.334577, 1.333802)
                pm.setAttr("{}.refl_weight".format(rsMat), 0)
                rsSprite = cmds.shadingNode("RedshiftSprite", asShader=True)
                cmds.connectAttr("{}.outColor".format(rsMat), "{}.input.".format(rsSprite), force=True)
                nodeToSet = rsMat, rsSprite
                attr = "tex0"
                
            elif layerType == "Line":
                outlineShd = self.refShader("P:/Bikey/rnd/light/Light_Template/shade/outline.ma")
                fileNode = self.createFileTexture("file", "fileP2d")
                surfaceShader = cmds.shadingNode("surfaceShader", asShader=True)
                pm.setAttr("{}.outColor".format(surfaceShader), 1, 1, 1)
                cmds.connectAttr("{}.outColor".format(fileNode), "{}.outMatteOpacity".format(surfaceShader), force=True)
                nodeToSet = surfaceShader, fileNode, outlineShd
                attr = "fileTextureName"
                
            elif layerType == "Texture_Add":
                rsMat = self.refShader("P:/Bikey/rnd/light/Light_Template/shade/texture_add.ma")
                rsSprite = cmds.shadingNode("RedshiftSprite", asShader=True)
                cmds.connectAttr("{}.outColor".format(rsMat), "{}.input.".format(rsSprite), force=True)
                nodeToSet = rsMat, rsSprite
                attr = "tex0"


            for sg in v["shadingGroups"]:
                mat = cmds.listConnections("{}.surfaceShader".format(sg))[0]
                matType = cmds.nodeType(mat)
        
                if matType == "RedshiftSprite":
                    colName = "sprite_sg_" + layerType
                    spriteFile = cmds.getAttr("{}.tex0".format(mat))
                    cmds.setAttr("{}.{}".format(nodeToSet[1], attr), spriteFile, type="string")
                    
                    if layerType == "Line":
                        connectToSg = nodeToSet[0]
                    else:
                        connectToSg = nodeToSet[1]
                else:
                    colName = "normal_sg_"  + layerType
                    if layerType == "Line":
                        connectToSg = nodeToSet[2]
                    else:
                        connectToSg = nodeToSet[0]
                        
                    
                existingCol = {c.name(): c for c in col.getCollections()}
                
                if colName in existingCol:
                    collection.delete(existingCol[colName])
            
                override = col.createOverride("", OpenMaya.MTypeId(0x58000386))
                parentCol = override.parent()
                parentCol.setName(colName)
                parentCol.getSelector().staticSelection.set([sg])
                parentCol.getSelector().setPattern("")
                override.setShader(connectToSg)

        return True



class QHLine(QtWidgets.QFrame):
    """docstring for QHLine"""
    def __init__(self):
        super(QHLine, self).__init__()
        self.setFrameShape(QtWidgets.QFrame.HLine)
        self.setFrameShadow(QtWidgets.QFrame.Sunken)
        


def runUI(ui_name="CreateTreeCollections_ui", size=(300, 175), title="Create Tree Collections", parent=None):
    if pm.window(ui_name, exists=True):
        pm.deleteUI(ui_name)
        
    win = main_ui.MainWindow(ui_name=ui_name, size=size, title=title, widget=CreateTreeCollections_ui(), parent=parent)
    win.show()
    