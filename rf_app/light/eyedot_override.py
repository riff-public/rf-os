import os
import glob
from functools import partial

from Qt import QtWidgets
from Qt import QtCore
from Qt import QtGui

import pymel.core as pm
import maya.OpenMaya as om

from rf_utils.context import context_info

SHD_NAME = 'eyeDot_Shd'
FILE_NAME = 'eyeDot_File'

class ScrollMessageBox(QtWidgets.QMessageBox):
    def __init__(self, title, message, ls=[], *args, **kwargs):
        QtWidgets.QMessageBox.__init__(self, *args, **kwargs)
        self.setWindowTitle(title)

        self.content = QtWidgets.QWidget(self)
        self.vLayout = QtWidgets.QVBoxLayout(self.content)


        self.label = QtWidgets.QLabel(message)
        self.vLayout.addWidget(self.label)

        self.list_widget = QtWidgets.QListWidget()
        self.vLayout.addWidget(self.list_widget)

        self.hLayout = QtWidgets.QHBoxLayout()
        self.vLayout.addLayout(self.hLayout)
        self.l_radioButton = QtWidgets.QRadioButton('L')
        self.l_radioButton.setChecked(True)
        self.hLayout.addWidget(self.l_radioButton)
        self.r_radioButton = QtWidgets.QRadioButton('R')
        self.r_radioButton.setChecked(False)
        self.hLayout.addWidget(self.r_radioButton)

        h = 100
        if ls:
            for l in ls:
                self.list_widget.addItem(QtWidgets.QListWidgetItem(l))
                h += 15
            self.list_widget.setCurrentRow(0)
            if h > 500:
                h = 500

        self.content.setMinimumSize(150, h)

        self.layout().addWidget(self.content, 0, 0, 1, self.layout().columnCount())
        self.yes_button = self.addButton('Override', QtWidgets.QMessageBox.ButtonRole.YesRole)
        self.no_button = self.addButton('Cancel', QtWidgets.QMessageBox.ButtonRole.RejectRole)

    @staticmethod
    def show(title, message, ls, parent=None):
        dialog = ScrollMessageBox(title=title, message=message, ls=ls, parent=parent)
        result = dialog.exec_()
        return dialog

def override_shd_eyeDot(entity=None, geos=[]):
    if not entity:
        entity = context_info.ContextPathInfo(path=str(pm.sceneName()))

    # ----- get eye dot texture path
    eyeDotTextureDir = entity.projectInfo.asset.global_asset(key='eyeDotTextureDir')
    eyeDotTextureDir = eyeDotTextureDir.replace('/', '\\')
    if not os.path.exists(eyeDotTextureDir):
        om.MGlobal.displayError('Eyedot directory does not exist: %s' %eyeDotTextureDir)
        return

    eyeDotShdPath = entity.projectInfo.asset.global_asset(key='eyeDotShd')
    eyeDotShdPath = eyeDotShdPath.replace('/', '\\')
    if not os.path.exists(eyeDotShdPath):
        om.MGlobal.displayError('Eyedot shader file does not exists: %s' %eyeDotShdPath)
        return

    if not geos:
        geos = [s for s in pm.selected(type='transform') if s.getShape(type='mesh', ni=True)]
        if not geos:
            om.MGlobal.displayError('Select some eye cornea!')
            return

    if pm.nt.RenderLayer.currentLayer() == pm.nt.RenderLayer.defaultRenderLayer():
        om.MGlobal.displayError('Current render layer must not be the default layer.')
        return

    # ----- find available textures
    fols = {}
    for f in os.listdir(eyeDotTextureDir):
        fol_path = os.path.join(eyeDotTextureDir, f)
        if os.path.isdir(fol_path):
            contents = os.listdir(fol_path)
            # find L and R texture
            l_paths = glob.glob('%s_L.*' %os.path.join(fol_path, f))
            r_paths = glob.glob('%s_R.*' %os.path.join(fol_path, f))
            if l_paths and r_paths:
                fols[f] = (l_paths[0], r_paths[0])

    # ----- popup the UI
    dial = ScrollMessageBox.show(title='Override Shader', 
                        message='Select eye dot texture.', 
                        ls=fols)
    
    tex_name = ''
    side_name = ''
    tex_img = ''
    if dial.clickedButton() == dial.yes_button:
        tex_name = str(dial.list_widget.currentItem().text())
        textures = fols[tex_name]
        if dial.l_radioButton.isChecked():
            side_name = 'L'
            tex_img = textures[0]
        else:
            side_name = 'R'
            tex_img = textures[1]
    else:
        return

    # ----- bring in the shader if it does not exists
    ns = 'eyeDotShd_%s_%s' %(tex_name, side_name)
    lsShds = pm.ls('%s:%s' %(ns, SHD_NAME))
    ref = None
    if not lsShds:
        ref = pm.createReference(eyeDotShdPath, ns=ns)
        shds = pm.ls('%s:%s' %(ns, SHD_NAME))
        if not shds:
            om.MGlobal.displayError('Cannot find in reference: %s:%s' %(ns, SHD_NAME))
            ref.remove()
            return
        shd = shds[0]
    else:
        shd = lsShds[0]
        ref_path = pm.referenceQuery(shd, f=True)
        ref = pm.FileReference(ref_path)

    # ----- set texture path
    files = pm.ls('%s:%s' %(ns, FILE_NAME))
    if not files:
        om.MGlobal.displayError('Cannot find in reference: %s:%s' %(ns, FILE_NAME))
        ref.remove()
        return

    file_node = files[0]
    file_node.fileTextureName.set(tex_img)

    # ----- assign shader
    pm.select(geos, r=True)
    pm.hyperShade(assign=shd)

