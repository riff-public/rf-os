#!/usr/bin/env python
# -- coding: utf-8 --

import os 
import sys 
import random
import lucidity
import getpass
from rf_utils.pipeline.notification import noti_module_cliq as noti
from rf_utils import user_info
from rf_utils.context import context_info

default_recipients = ['TA', 'Pia']


def get_message(): 
	message = '{receiver}คะ \n{user} มีการ unlock {entity_type} {step} {name} ที่ publish ไปแล้วค่ะ'
	message += '\nอาจเกิดการแก้ที่กระทบแผนกอื่น'
	message += '\nไฟล์ที่เปิด {filename} '
	message += '\nฝาก{receiver} เช็คหน่อยนะคะ ขอบคุณค่ะ'
	return message


def run(filename): 
	entity = context_info.ContextPathInfo(path=filename)

	# breacher 
	user = user_info.User()
	sgUser = user.sg_user()
	localname = sgUser['sg_local_name']
	email = sgUser.get('email')
	sender = [localname, email]

	# trigger 
	if not user.is_admin(): 
		if entity.entity_type == 'scene': 
			producer = entity.projectInfo.team.producer(entity.entity_type, entity.step)
			userinfo = user_info.SGUser()
			recipients = list()

			default_recipients.extend(producer)

			for person in default_recipients:
				sg_person = userinfo.get_user_entity(person)
				receiver_email = sg_person.get('email')
				receiver_name = sg_person['sg_local_name']
				recipients.append([receiver_name, receiver_email])

			for receiver_name, receiver_email in recipients: 
				# receiver_email = 'chanon.v@riff-studio.com'
				message = get_message()
				template = lucidity.Template('Message', message)
				message = template.format({
					'user': localname, 
					'receiver': receiver_name, 
					'entity_type': entity.entity_type, 
					'name': entity.name, 
					'step': entity.step, 
					'filename': filename})

				noti.main(botName='jengchanbot', message=message, emails=[receiver_email])
