import os
import sys
import traceback
from collections import OrderedDict

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils import icon
from rf_utils.widget import project_widget
from rf_utils.widget import file_comboBox
from rf_utils.widget import sg_entity_widget
from rf_utils.sg import sg_utils


class SGManagerCoreUI(QtWidgets.QWidget):
    status = QtCore.Signal(str)
    def __init__(self, parent=None) :
        super(SGManagerCoreUI, self).__init__(parent)

        # main layout
        self.allLayout = QtWidgets.QVBoxLayout()

        self.add_category_widget()

        self.add_library_widget()
        # self.add_workspace_widget()
        self.setLayout(self.allLayout)
        self.set_default()


    def add_line(self):
        """ set line to layout """
        self.lineHeader = self.line(self.allLayout)

        # 2 cols layout - library, workspace
    def add_category_widget(self):
        self.categoryLayout = QtWidgets.QHBoxLayout()
        self.allLayout.addLayout(self.categoryLayout)

    def add_library_widget(self):
        """ entity listwidget """
        # layout
        self.libraryLayout = QtWidgets.QVBoxLayout()
        self.categoryLayout.addLayout(self.libraryLayout)
        self.entityListLayout = QtWidgets.QHBoxLayout()


        # widgets
        self.add_navigation_widget()
        self.line(self.libraryLayout)
        self.add_search_widget()
        self.add_entity_widget()
        self.add_button_widget()
        self.add_status_widget()
        self.set_navigation_layout()

    def add_navigation_widget(self):
        # nav
        self.navigationLayout = QtWidgets.QHBoxLayout()
        self.libraryLayout.addLayout(self.navigationLayout)
        self.add_project_widget()
        self.add_mode()
        self.add_type_episode_widget()
        self.line(self.navigationLayout, 'v')
        self.add_step_widget(self.navigationLayout)
        # self.add_process_widget(self.navigationLayout)
        # self.add_workspace_checkbox()

        # add spacer
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.navigationLayout.addItem(spacerItem)

    def add_search_widget(self):
        self.searchLayout = QtWidgets.QHBoxLayout()
        self.libraryLayout.addLayout(self.searchLayout)

        self.searchLabel = QtWidgets.QLabel('Search :')

        self.searchLineEdit = QtWidgets.QLineEdit()
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.searchLayout.addWidget(self.searchLabel)
        self.searchLayout.addWidget(self.searchLineEdit)
        self.searchLayout.addItem(spacerItem)

    def add_entity_widget(self):
        self.libraryLayout.addLayout(self.entityListLayout)
        self.entityWidget = sg_entity_widget.EntityListWidget()
        self.entityWidget.listWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.entityWidget.set_icon_mode()
        self.entityListLayout.addWidget(self.entityWidget)

        self.entityListLayout.setStretch(0, 1)
        self.entityListLayout.setStretch(1, 4)

    def add_button_widget(self):
        self.buttonLayout = QtWidgets.QHBoxLayout()
        self.allLayout.addLayout(self.buttonLayout)

        self.packButton = QtWidgets.QPushButton('Pack to Dropbox')
        self.syncButton = QtWidgets.QPushButton('Sync Back to Server')

        self.buttonLayout.addWidget(self.packButton)
        self.buttonLayout.addWidget(self.syncButton)

        self.packButton.setMinimumSize(QtCore.QSize(0, 30))
        self.syncButton.setMinimumSize(QtCore.QSize(0, 30))

        self.buttonLayout.setStretch(0, 2)
        self.buttonLayout.setStretch(1, 2)

    def add_status_widget(self):
        self.statusLayout = QtWidgets.QHBoxLayout()
        self.allLayout.addLayout(self.statusLayout)

        self.statusLabel = QtWidgets.QLabel('Status : ')

        self.statusLayout.addWidget(self.statusLabel)



    def add_project_widget(self):
        # add project

        self.projectWidget = project_widget.SGProjectComboBox(sg=sg_utils.sg, layout=QtWidgets.QHBoxLayout())
        self.navigationLayout.addWidget(self.projectWidget)

        # layout
        self.projectWidget.projectComboBox.setMinimumSize(QtCore.QSize(140, 0))


    def add_mode(self):
        self.modeLabel = QtWidgets.QLabel('Mode :')
        self.modeComboBox = QtWidgets.QComboBox()
        self.modeLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)

        self.navigationLayout.addWidget(self.modeLabel)
        self.navigationLayout.addWidget(self.modeComboBox)

    def add_type_episode_widget(self):
        self.typeEpisodeLabel = QtWidgets.QLabel('Type :')
        self.typeEpisodeComboBox = QtWidgets.QComboBox()
        # self.typeEpisodeCheckBox.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.navigationLayout.addWidget(self.typeEpisodeLabel)
        self.navigationLayout.addWidget(self.typeEpisodeComboBox)

    def add_workspace_checkbox(self):
        # add checkBox
        self.workspaceCheckBox = QtWidgets.QCheckBox('Workspace')
        self.navigationLayout.addWidget(self.workspaceCheckBox)

    def add_step_widget(self, layout):
        self.stepWidget = file_comboBox.FileComboBox(layout=QtWidgets.QHBoxLayout())
        self.stepWidget.set_label('Department : ')
        # self.stepWidget.label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        layout.addWidget(self.stepWidget)
        self.stepWidget.comboBox.setMinimumSize(QtCore.QSize(100, 0))


    def line(self, layout, direction='h', parent=None):
        """ line """
        line = QtWidgets.QFrame(parent)
        line.setFrameShape(QtWidgets.QFrame.HLine) if direction == 'h' else line.setFrameShape(QtWidgets.QFrame.VLine)
        line.setFrameShadow(QtWidgets.QFrame.Sunken)
        layout.addWidget(line)
        return line

    def set_default(self):
        # self.libraryButton.setVisible(False)
        self.entityWidget.set_icon_size([10, 10])

        self.typeEpisodeComboBox.setEnabled(True)


    def set_navigation_layout(self):
        self.navigationLayout.setSpacing(6)

        self.navigationLayout.setStretch(0, 1) # proj label
        self.navigationLayout.setStretch(1, 1) # proj
        self.navigationLayout.setStretch(2, 2) # mode label
        self.navigationLayout.setStretch(3, 1) # mode
        self.navigationLayout.setStretch(4, 2) # type label
        self.navigationLayout.setStretch(5, 1) # type
        self.navigationLayout.setStretch(6, 2) # proc label


class SGManagerUI(SGManagerCoreUI):
    """docstring for SGManagerUI"""
    def __init__(self, parent=None):
        super(SGManagerUI, self).__init__(parent=parent)
        # self.init_signals()

    def filter1_changed(self, value):
        state = True if value else False
        self.typeEpisodeComboBox.setEnabled(state)

    def name_enable(self, value):
        state = True if value else False
        self.previewNameWidget.setEnabled(state)



