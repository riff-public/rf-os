# v.0.0.1 polytag switcher
_title = 'Riff Pack Dropbox'
_version = 'v.0.0.6'
_des = 'Miarmy Oa'
uiName = 'PackDropbox'

#Import python modules
import sys
import os
import getpass
from datetime import datetime
import logging
from functools import partial
from collections import OrderedDict

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
import rf_config as config

# framework modules
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet
from rf_utils.widget import dialog

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import ui
reload(ui)
# reload(ui.auto_build_widget)
# thread
from rf_utils.sg import sg_thread
import utils
reload(utils)
from rf_utils import icon
from rf_utils.sg import sg_utils
# from rf_utils.sg import sg_process
from rf_utils.pipeline import user_pref
from rf_utils.context import context_info
context_info.sg = sg_utils.sg

from rf_utils import install_location
import shutil

from rf_app.sceneCollector_scm import utils as sc_utils
reload(sc_utils)

class Config:
    asset = 'asset'
    scene = 'scene'
    mode = {asset: 'Asset', scene: 'Shot'}
    allTag = 'All tag'
    expiredOption = {'1. default': 300, '2. 1 Hour': 3600}


class RFSGManager(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(RFSGManager, self).__init__(parent)

        # ui read
        self.ui = ui.SGManagerUI(parent)
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)

        # title
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.resize(800, 650)

        # declare context
        self.entity = None
        self.uiEntity = None

        self.cachesData = {Config.asset: dict(), Config.scene: dict()}

        # prefs
        self.pref = user_pref.ToolSetting(uiName)
        self.prefData = self.pref.read()

        # self.init_menu()
        self.init_ui()
        self.init_signals()
        self.init_functions()
        # self.set_default()

    def init_ui(self):
        """ initial ui values """
        # self.add_status_bar()
        self.ui.modeComboBox.addItems(sorted(Config.mode.keys()))
        # set prev selection
        index = sorted(Config.mode.keys()).index(self.prefData['entityType']) if self.prefData.get('entityType') else 0
        self.ui.modeComboBox.setCurrentIndex(index)
        self.entityType = str(self.ui.modeComboBox.currentText())

    def init_ui_entity(self, entity):
        """ init ui entity """
        self.uiEntity = entity
        self.uiEntity.context.update(entityType=self.entityType)

    def init_signals(self):
        """ init ui signals """
        self.ui.projectWidget.projectChanged.connect(self.project_changed)
        self.ui.modeComboBox.currentIndexChanged.connect(self.mode_changed)

        # filters
        self.ui.typeEpisodeComboBox.currentIndexChanged.connect(self.filter1_changed)

        # search
        self.ui.searchLineEdit.textChanged.connect(partial(self.show_filtered_entity, 'search'))

        # entity signals
        self.ui.entityWidget.selected.connect(self.entity_selected)
        self.ui.stepWidget.currentIndexChanged.connect(self.step_changed)

        # button
        self.ui.packButton.clicked.connect(self.packToDb)
        self.ui.syncButton.clicked.connect(self.syncBack)

    def init_functions(self):
        """ start functions """
        self.ui_customized()
        self.set_ui_context()
        self.fetch_data()

    def ui_customized(self):
        self.ui.stepWidget.sortedGuide = ['model', 'rig', 'gloom', 'texture', 'lookdev', 'set', 'layout', 'anim', 'finalcam', 'setdress', 'sim', 'fx', 'techanim', 'light', 'comp']
        self.ui.stepWidget.skipList = ['textures', 'colorscript', 'keyvis', 'storyboard']

        self.ui.entityWidget.set_text_mode()

    def project_changed(self):
        """ project changed """
        self.fetch_data()
        self.set_ui_context()
        self.save_pref()

    def fetch_data(self):
        """ get data from shotgun """
        # get ui values
        projectEntity = self.ui.projectWidget.current_item()
        sgEntityType = Config.mode[self.entityType]
        project = self.ui.projectWidget.current_project()

        # # loading bar
        # self.loading_display()

        # if no cache
        if not self.ui.projectWidget.current_project() in self.cachesData[self.entityType].keys():
            # fetch sg data thread
            self.sgThread = sg_thread.GetEntity(projectEntity, sgEntityType)
            self.sgThread.value.connect(self.populate_entity)
            self.sgThread.finished.connect(self.fetch_finished)
            self.sgThread.start()
            self.lock_ui(True)

        # use cache
        else:
            sortedData = self.cachesData[self.entityType][self.ui.projectWidget.current_project()]
            self.populate_entity([sortedData[a] for a in sortedData])

    def fetch_finished(self):
        self.lock_ui(False)

    def lock_ui(self, state):
        self.ui.projectWidget.projectComboBox.setEnabled(not state)
        self.ui.modeComboBox.setEnabled(not state)

    def populate_entity(self, sgEntities):
        """ poplulate filters and entities """
        # add cache
        self.sortedEntities = sg_utils.ordered_entity_dict(sgEntities)
        # set caches
        self.cachesData[self.entityType][self.ui.projectWidget.current_project()] = self.sortedEntities
        self.relationData = self.find_relation_data(sgEntities)

        # set filter1
        self.populate_filter1(self.relationData.keys())

    def find_relation_data(self, sgEntities):
        mode = self.ui.modeComboBox.currentText()
        if mode == Config.asset:
            data = sg_utils.find_attr_relation(sgEntities, 'sg_asset_type', 'sg_subtype')
        if mode == Config.scene:
            data = sg_utils.find_attr_relation(sgEntities, 'sg_episode.Scene.code', 'sg_sequence_code')

        return data

    def populate_filter1(self, data):
        self.ui.typeEpisodeComboBox.blockSignals(True)

        self.ui.typeEpisodeComboBox.clear()
        self.ui.typeEpisodeComboBox.addItems(data)
        # prev selection
        lastSel = self.prefData.get('typeEpisode')
        status = self.prefData.get('typeEpisodeStatus')

        self.ui.typeEpisodeComboBox.setCurrentIndex(data.index(lastSel)) if lastSel in data else None

        self.ui.typeEpisodeComboBox.blockSignals(False)
        self.filter1_changed()


    def set_ui_context(self):
        # set global entity for ui use
        project = self.ui.projectWidget.current_project()
        self.uiEntityThread = sg_thread.GetContext(project)
        self.uiEntityThread.value.connect(self.init_ui_entity)
        self.uiEntityThread.start()

    def mode_changed(self):
        """ asset or scene changed """
        self.entityType = str(self.ui.modeComboBox.currentText())
        self.rename_label(self.entityType)
        self.fetch_data()

    def rename_label(self, mode):
        """ set label to match mode """
        if mode == Config.asset:
            typeEpisodeText = 'Type : '

        if mode == Config.scene:
            typeEpisodeText = 'Episode : '

        self.ui.typeEpisodeLabel.setText(typeEpisodeText)

    def save_pref(self):
        project = self.ui.projectWidget.current_project()
        entityType = self.entityType
        typeEpisode = str(self.ui.typeEpisodeComboBox.currentText())
        step = self.ui.stepWidget.current_text()

        prefDict = OrderedDict()
        prefDict['project'] = project
        prefDict['entityType'] = entityType or str()
        prefDict['typeEpisode'] = typeEpisode or str()
        prefDict['step'] = step or str()
        prefDict['entity'] = self.ui.entityWidget.current_text()

        self.pref.write(prefDict)

    def step_changed(self, path):
        step = self.ui.stepWidget.current_text()
        self.entity.context.update(step=step)
        # self.set_process(self.entity)
        self.save_pref()

    # def process_changed(self, path):
    #     process = self.ui.processWidget.current_text()
    #     self.entity.context.update(process=process)
    #     # self.set_app(self.entity)
    #     # self.save_pref()

    def filter1_changed(self):
        filter1 = str(self.ui.typeEpisodeComboBox.currentText())
        self.show_filtered_entity()

    def show_filtered_entity(self, *args):
        # self.populate_tags()
        self.calculate_list()

    def get_filter_entity(self):
        filter1Value = str(self.ui.typeEpisodeComboBox.currentText())
        filter2Value = ''

        filter1 = (filter1Value, self.get_filter1_attr())
        filter2 = (filter2Value, self.get_filter2_attr())
        # filters
        sgEntities = sg_utils.filter_entities([self.sortedEntities[a] for a in self.sortedEntities], filter1, filter2)
        return sgEntities

    def get_filter1_attr(self):
        mode = self.ui.modeComboBox.currentText()
        if mode == Config.asset:
            return 'sg_asset_type'
        if mode == Config.scene:
            return 'sg_episode.Scene.code'

    def get_filter2_attr(self):
        mode = self.ui.modeComboBox.currentText()
        if mode == Config.asset:
            return 'sg_subtype'
        if mode == Config.scene:
            return 'sg_sequence_code'

    def calculate_list(self, *args):
        """ filtering entities. refresh ui here """
        sgEntities = self.get_filter_entity()

        sgEntities = self.filter_search(sgEntities)
        self.list_entities(sgEntities)

    def list_entities(self, sgEntities):
        self.ui.entityWidget.clear()
        self.ui.entityWidget.add_items(sgEntities, widget=False)
        self.ui.stepWidget.clear()
        # self.ui.processWidget.clear()

        # sel prev selection
        lastSel = self.prefData.get('entity')
        self.ui.entityWidget.set_current(lastSel, signal=True) if not lastSel == None else None

    def filter_search(self, sgEntities):
        key = str(self.ui.searchLineEdit.text())
        return [a for a in sgEntities if key.lower() in a['code'].lower()] if key else sgEntities

    def block_nav_signals(self, state):
        self.ui.stepWidget.blockSignals(state)

    def entity_selected(self, sgEntity):
        """ entity selected signal """
        # clear step, process, app
        self.block_nav_signals(True)
        self.ui.stepWidget.clear()
        self.block_nav_signals(False)

        # browse step
        # no thread
        entity = sg_thread.update_entity(sgEntity)
        self.set_step(entity)


        return
        # entityType = Config.mode[self.ui.modeComboBox.currentText()]
        # self.stepThread = sg_thread.UpdateEntity(sgEntity)
        # self.stepThread.value.connect(self.set_step)
        # self.stepThread.start()

    def set_step(self, entity):
        # make entity global
        self.entity = entity
        # self.entity.context.update(app=self.app)

        browseStepPath = os.path.split(entity.path.step().abs_path())[0]

        if os.path.exists(browseStepPath):
            # add ui
            self.ui.stepWidget.clear()
            self.ui.stepWidget.list_items(browseStepPath)

            lastSel = self.prefData.get('step')
            self.ui.stepWidget.set_current(lastSel) if not lastSel == None else None

    def packToDb(self):
        # import hashlib
        # sgEntity = self.ui.entityWidget.current_item()
        self.ui.statusLabel.setText('Status : Loading . . .')

        publishPath = os.path.split(self.entity.path.hero().abs_path())[0]
        # print publishPath
        workspacePath = os.path.split(self.entity.path.process().abs_path())[0]
        # print workspacePath
        #workspaceProcess = os.listdir(workspacePath)
        collects = []
        wsFiles = []
        app = 'maya'
        entityType =self.entity.get_value('entityType')
        dstRoot = install_location.get_dropbox_root()
        if entityType == 'scene':
            self.entity.context.update(process = 'main', app = app)
            workspacePath = self.entity.path.workspace().abs_path()

        for root, dirs, files in os.walk(workspacePath):
            currentDir = os.path.split(root)[-1]
            if currentDir == app:
                if files: 
                    fileDict = dict()
                    for file in files:
                        if file.split('.')[-1] == 'ma' or file.split('.')[-1] == 'mb':
                            fullPath = '%s/%s' % (root.replace('\\', '/'), file)
                            ft = os.path.getmtime(fullPath)
                            fileDict[ft] = fullPath

                    if fileDict: 
                        latestFile = fileDict.get(sorted(fileDict.keys())[-1])
                        collects.append(latestFile)

        if entityType == 'asset':
            for root, dirs, files in os.walk(publishPath):
                if files:
                    for file in files:
                        fullPath = '%s/%s' % (root.replace('\\', '/'), file)
                        collects.append(fullPath)
            # print collects
            if collects: 
                wsFiles += collects

            if wsFiles:
                for i, src in enumerate(wsFiles): 
                    path = src.replace(':', '')
                    dst = '%s/%s' % (dstRoot, path)
                    print '%s/%s copy %s' % (i+1, len(wsFiles), dst)
                    self.ui.statusLabel.setText('Status : %s/%s copy %s' % (i+1, len(wsFiles), dst))
                    dirname = os.path.dirname(dst)
                    
                    if not os.path.exists(dirname): 
                        os.makedirs(dirname)
                
                    shutil.copy2(src, dst)
                title = 'Copy complete'
                message = 'Finished Copy_to_DB'
                dialog.MessageBox.success(title, message)
                self.ui.statusLabel.setText('Status : Completed ! ! !')

            else:
                self.ui.statusLabel.setText('Status : No Files to copy - -')

                title = 'Failed !!'
                message = 'No Files to copy'
                dialog.MessageBox.warning(title, message)
                self.ui.statusLabel.setText('Status : Failed ! ! !')

        elif entityType == 'scene':
            from rf_app.sceneCollector_scm import sceneCollecter_batch
            reload(sceneCollecter_batch)

            collectAsset = []
            for c in collects:
                data = sc_utils.get_asset(c, batch=True, mode='all', version='2018')
                allitems = data.get('files')
                for item in allitems:
                    collectAsset.append(item)

            if collects: 
                wsFiles += collects
                wsFiles += collectAsset
            if wsFiles:
                for i, src in enumerate(wsFiles): 
                    path = src.replace(':', '')
                    dst = '%s/%s' % (dstRoot, path)
                    print '%s/%s copy %s' % (i+1, len(wsFiles), dst)
                    self.ui.statusLabel.setText('Status : %s/%s copy %s' % (i+1, len(wsFiles), dst))
                    dirname = os.path.dirname(dst)
                    
                    if not os.path.exists(dirname): 
                        os.makedirs(dirname)
                
                    shutil.copy2(src, dst)

                title = 'Copy complete'
                message = 'Finished Copy_to_DB'
                dialog.MessageBox.success(title, message)
                self.ui.statusLabel.setText('Status : Completed ! ! !')

            else:
                self.ui.statusLabel.setText('Status : No Files to copy - -')

                title = 'Failed !!'
                message = 'No Files to copy'
                dialog.MessageBox.warning(title, message)
                self.ui.statusLabel.setText('Status : Failed ! ! !')


    def syncBack(self):
        self.ui.statusLabel.setText('Status : Loading . . .')

        workspacePath = os.path.split(self.entity.path.process().abs_path())[0]
        DBRoot = install_location.get_dropbox_root()
        app = 'maya'
        entityType =self.entity.get_value('entityType')
        if entityType == 'scene':
            self.entity.context.update(process = 'main', app = app)
            workspacePath = self.entity.path.workspace().abs_path()
        workspacePath = workspacePath.replace(':', '')
        DBWorkspacePath = os.path.join(DBRoot, workspacePath)
        collects = []
        wsFiles = []

        for root, dirs, files in os.walk(DBWorkspacePath):
            currentDir = os.path.split(root)[-1]
            if currentDir == app:
                if files: 
                    fileDict = dict()
                    for file in files:
                        if file.split('.')[-1] == 'ma' or file.split('.')[-1] == 'mb':
                            fullPath = '%s/%s' % (root.replace('\\', '/'), file)
                            ft = os.path.getmtime(fullPath)
                            fileDict[ft] = fullPath

                    if fileDict: 
                        latestFile = fileDict.get(sorted(fileDict.keys())[-1])
                        collects.append(latestFile)

        step = self.entity.get_value('step')

        if step == 'groom' or step == 'texture':
            if step == 'groom':
                app = 'texture'
                self.entity.context.update( process = 'main' , app = app)
            elif step == 'texture':
                app = 'images'
                self.entity.context.update( process = 'main' , app = app)

            workspacePath = self.entity.path.workspace().abs_path()
            workspacePath = workspacePath.replace(':', '')
            DBWorkspacePath = os.path.join(DBRoot, workspacePath)

            for root, dirs, files in os.walk(DBWorkspacePath):
                if files:
                    for file in files:
                        fullPath = '%s/%s' % (root.replace('\\', '/'), file)
                        collects.append(fullPath)


        if collects: 
            wsFiles += collects

        if wsFiles:
            for i, src in enumerate(wsFiles):
                srcNorm = os.path.normpath(src)
                dst = srcNorm.replace(DBRoot+'\\', '')
                drive = dst.split(os.sep)[0]
                dst = dst.replace(drive, '%s:'%drive)
                user = dst.split('.')[-2]
                dst = dst.replace(user,user+'-WFH')
                print '%s/%s copy %s' % (i+1, len(wsFiles), dst)
                self.ui.statusLabel.setText('Status : %s/%s copy %s' % (i+1, len(wsFiles), dst))

                dirname = os.path.dirname(dst)
                
                if not os.path.exists(dirname): 
                    os.makedirs(dirname)
            
                shutil.copy2(src, dst)

            title = 'Copy complete'
            message = 'Finished Copy_to_Local'
            dialog.MessageBox.success(title, message)
            self.ui.statusLabel.setText('Status : Completed ! ! !')
        else:
            self.ui.statusLabel.setText('Status : No Files to copy - -')

            title = 'Failed !!'
            message = 'No Files to copy'
            dialog.MessageBox.warning(title, message)
            self.ui.statusLabel.setText('Status : Failed ! ! !')



def show(mode='default'):
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        if mode == 'default':
            maya_win.deleteUI(uiName)
            myApp = RFSGManager(maya_win.getMayaWindow())
        if mode == 'asset':
            maya_win.deleteUI('%sAsset' % uiName)
            myApp = AssetMode(maya_win.getMayaWindow())
        if mode == 'shot':
            maya_win.deleteUI('%sShot' % uiName)
            myApp = ShotMode(maya_win.getMayaWindow())
        myApp.show()
        return myApp
    else:
        logger.info('Run in standalone\n')
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        if mode == 'default':
            myApp = RFSGManager()
        if mode == 'asset':
            myApp = AssetMode()
        if mode == 'shot':
            myApp = ShotMode()
        myApp.show()
        stylesheet.set_default(app)
        sys.exit(app.exec_())


if __name__ == '__main__':
    show()
